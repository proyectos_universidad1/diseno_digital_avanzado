/*
Rutinas CORDIC para el cálculo de y=sin(theta),x=cos(theta)
Alexander López Parrado (2023)

-32768 <=theta<=32767

Para el caso de los �ngulos
-32768=> -180 °
 32767=> +180 °

Para el caso de los resultados del seno y del coseno
-32768 => -1
 32767 => +1
*/

#include <stdint.h>
#include <math.h>
#include <malloc.h>
#include "cordic.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846 /* pi */
#endif

/*Función para generar las tablas*/
/*Esta función es proveida únicamente para depuración*/
void cordicGenTable(int32_t *table)
{

    uint8_t i;
    double aux1;

    for (i = 0; i < 24; i++)
    {
        aux1 = atan(1.0 / (double)(1L << i)) / M_PI;
        aux1 = aux1 * (double)(8388608);
        table[i] = (int32_t)(floor(aux1));
    }
}

/*Función núcleo para rotación*/
void rotationKernel(int32_t *x, int32_t *y, int32_t *z, int32_t *atanTable)
{

    int32_t x_k, x_k_1, y_k, y_k_1;
    int32_t z_k, z_k_1;
    uint8_t k;

    y_k = *y;
    z_k = *z;
    x_k = *x;

    for (k = 0; k < 24; k++)
    {

        if (z_k < 0)
        {

            x_k_1 = x_k + (y_k >> k);
            y_k_1 = y_k - (x_k >> k);
            z_k_1 = z_k + (atanTable[k]);
        }
        else
        {

            x_k_1 = x_k - (y_k >> k);
            y_k_1 = y_k + (x_k >> k);
            z_k_1 = z_k - (atanTable[k]);
        }

        x_k = x_k_1;
        y_k = y_k_1;
        z_k = z_k_1;
        
        printf("k: %d,x:%d, y:%d, z: %d\n", k, x_k, y_k, z_k);
    }

    *x = x_k;
    *y = y_k;
    *z = z_k;
}

/*Cálculo del coseno y el seno*/
void cordicCosSin(int32_t angle, int32_t *c, int32_t *s, int32_t *atanTable)
{

    int32_t x, y;
    int32_t z;
    uint8_t rot_flag;

    x = 8388607;
    y = 0,
    z = angle;

    rot_flag = ((angle > 4194304) || (angle < (-4194304)));

    /*Se remapea a ángulos en el I y IV cuadrante, rotación de pi radianes*/
    /*I -> III, II->IV*/

    if (rot_flag)
    {
        z = (z + 8388608); // se refleja el ángulo


        if (z &(0x800000)) // se verifica el bit de signo a 24 bits
            z = (z & 0x00FFFFFFL) | 0xFF000000L;
            
        else
            z = (z & 0x00FFFFFFL);
            
    }

    rotationKernel(&x, &y, &z, atanTable);

    *s = y;
    *c = x;

    /*Se refleja de nuevo*/
    if (rot_flag)
    {
        *c = -x;
        *s = -y;
    }
}

/*Función para modo de vectorización de Cordic*/
void vectoring_mode(int32_t *x, int32_t *y, int32_t *z, int32_t *atanTable)
{
    int32_t x_k, x_k_1, y_k, y_k_1;
    int32_t z_k, z_k_1;
    uint8_t k;

    y_k = *y;
    z_k = *z;
    x_k = *x;

    for (k = 0; k < 24; k++)
    {

        if (y_k > 0)
        {

            x_k_1 = x_k + (y_k >> k);
            y_k_1 = y_k - (x_k >> k);
            z_k_1 = z_k + (atanTable[k]);
        }
        else
        {

            x_k_1 = x_k - (y_k >> k);
            y_k_1 = y_k + (x_k >> k);
            z_k_1 = z_k - (atanTable[k]);
        }

        x_k = x_k_1;
        y_k = y_k_1;
        z_k = z_k_1;
    }

    *x = x_k;
    *y = y_k;
    *z = z_k;
}

/*Función para hallar la arctangente con cordic*/
void atan_cordic(int32_t xo, int32_t yo, int32_t *mag, int32_t *atan, int32_t *atanTable)
{
    int32_t x, y, z;
    uint8_t rot_flag;

    // inicialización a 24 bits
    x = xo;
    y = yo;
    z = 0;

    /*Se remapea a ángulos en el I y IV cuadrante, negación de coordenadas x e y */
    // en el cuadrante II -> (x-,0x00FFFFFF y+) en el cuadrante III -> (x-, y-)

    rot_flag = x < 0; // remapeo del ángulo cuando x sea negativo

    // se hace reflexión de las coordenadas para ajuste en I y IV cuadrante
    if (rot_flag)
    {
        x = -x;
        y = -y;
    }

    vectoring_mode(&x, &y, &z, atanTable);

    // se obtienen los resultados después de realizar el modo de rotación

    /*Se refleja de nuevo*/


    if (rot_flag)
    {
        z = (z + 8388608); // se refleja de nuevo

        if (z &(0x800000))  // se verifica el bit de signo a 24 bits
            z = (z & 0x00FFFFFFL) | 0xFF000000L;
            
        else
            z = (z & 0x00FFFFFFL);
            
    }


    *mag = x;
    *atan = z;
}
