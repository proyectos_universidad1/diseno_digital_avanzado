// compilación: gcc cordic.c main.c -lm -o /home/juan/Documentos/JuanK/universidad/profun_2/cordic_c/main
// ejecución: ./main

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "cordic.h"

#ifndef M_PI
  # define M_PI	3.14159265358979323846
#endif

#define ANGLE  M_PI/4  // ángulo en radianes

int main(){

    int32_t table[24];
    int32_t x,y,m,res_atan;
    uint8_t i;

    int32_t angle= (ANGLE / M_PI) * 8388607; // se mapea el angulo a 24 bits

    cordicGenTable(table); // se realiza el cálculo de la tabla para la atan

    /****** se muestra la tabla de la atan en diferentes representaciones ******/

    printf("tabla atan: \n");
    for(i=0;i<24;i++){
        printf("pos %d = 24 bits: %d real: %f: \n", (i+1), table[i], (double) (table[i]*(M_PI / 8388607)));
    }
    printf("\n");

    /***** se muestra la tabla en hexadecimal **********/
    printf("(");
    for (i = 0; i<24; i++){
      printf("X\"%x\"",table[i]);
      if (i != 23) printf(",");
    }
    printf(");");
    printf("\n");

    for(i=0;i<24;i++){
        printf("atanTable[%d] = %d \n", i, table[i]);
    }
    printf("\n");

    // se muestra resultado de seno y coseno por medio de cordic
    cordicCosSin(angle, &x,&y,table);
    printf("angle: %d cos: %d sin: %d\n", angle, x, y);

    // se muestran los resultados para magnitud y atan 
    atan_cordic(x, y, &m, &res_atan, table);
    printf("mag: %d atan: %d", m, res_atan);
    printf("\n");
    
    return 0;
}