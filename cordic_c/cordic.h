#ifndef _CORDIC_
  #define _CORDIC_

  #include <stdint.h>

  void cordicGenTable(int32_t *table);
  void cordicCosSin(int32_t angle, int32_t *c,int32_t *s, int32_t *atanTable);
  void rotationKernel(int32_t *x,int32_t *y,int32_t *z, int32_t *atanTable);
  void vectoring_mode(int32_t *x, int32_t *y, int32_t *z, int32_t *atanTable);
  void atan_cordic(int32_t xo, int32_t yo, int32_t *mag, int32_t *atan, int32_t *atanTable);
#endif
