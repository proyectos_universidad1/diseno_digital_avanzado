// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Mon May  1 15:12:01 2023
// Host        : juan-Inspiron-14-3467 running 64-bit Linux Mint 21.1
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ project_audio_2_design_project_audio_2_ip_0_0_sim_netlist.v
// Design      : project_audio_2_design_project_audio_2_ip_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mux_2_1
   (O,
    \q_reg[7] ,
    \q_reg[11] ,
    \q_reg[15] ,
    \q_reg[19] ,
    \q_reg[23] ,
    \q_reg[23]_0 ,
    D,
    sel0_out,
    A,
    S,
    \q_reg[7]_0 ,
    DI,
    \q_reg[11]_0 ,
    \q_reg[15]_0 ,
    \q_reg[15]_1 ,
    \q_reg[19]_0 ,
    \q_reg[19]_1 ,
    \q_reg[23]_1 ,
    \q_reg[23]_2 ,
    \q_reg[24] ,
    \axi_rdata_reg[3] ,
    \axi_rdata_reg[31] ,
    axi_araddr,
    \axi_rdata_reg[31]_0 ,
    \axi_rdata_reg[7] ,
    \axi_rdata_reg[11] ,
    \axi_rdata_reg[14] ,
    \axi_rdata_reg[19] ,
    \axi_rdata_reg[23] ,
    \axi_rdata_reg[24] );
  output [3:0]O;
  output [3:0]\q_reg[7] ;
  output [3:0]\q_reg[11] ;
  output [3:0]\q_reg[15] ;
  output [3:0]\q_reg[19] ;
  output [3:0]\q_reg[23] ;
  output [0:0]\q_reg[23]_0 ;
  output [25:0]D;
  input sel0_out;
  input [10:0]A;
  input [3:0]S;
  input [3:0]\q_reg[7]_0 ;
  input [1:0]DI;
  input [3:0]\q_reg[11]_0 ;
  input [2:0]\q_reg[15]_0 ;
  input [3:0]\q_reg[15]_1 ;
  input [3:0]\q_reg[19]_0 ;
  input [3:0]\q_reg[19]_1 ;
  input [3:0]\q_reg[23]_1 ;
  input [3:0]\q_reg[23]_2 ;
  input [0:0]\q_reg[24] ;
  input [3:0]\axi_rdata_reg[3] ;
  input [25:0]\axi_rdata_reg[31] ;
  input [1:0]axi_araddr;
  input [25:0]\axi_rdata_reg[31]_0 ;
  input [2:0]\axi_rdata_reg[7] ;
  input [1:0]\axi_rdata_reg[11] ;
  input [2:0]\axi_rdata_reg[14] ;
  input [3:0]\axi_rdata_reg[19] ;
  input [1:0]\axi_rdata_reg[23] ;
  input [0:0]\axi_rdata_reg[24] ;

  wire [10:0]A;
  wire [25:0]D;
  wire [1:0]DI;
  wire [3:0]O;
  wire [3:0]S;
  wire [1:0]axi_araddr;
  wire [1:0]\axi_rdata_reg[11] ;
  wire [2:0]\axi_rdata_reg[14] ;
  wire [3:0]\axi_rdata_reg[19] ;
  wire [1:0]\axi_rdata_reg[23] ;
  wire [0:0]\axi_rdata_reg[24] ;
  wire [25:0]\axi_rdata_reg[31] ;
  wire [25:0]\axi_rdata_reg[31]_0 ;
  wire [3:0]\axi_rdata_reg[3] ;
  wire [2:0]\axi_rdata_reg[7] ;
  wire [3:0]\q_reg[11] ;
  wire [3:0]\q_reg[11]_0 ;
  wire [3:0]\q_reg[15] ;
  wire [2:0]\q_reg[15]_0 ;
  wire [3:0]\q_reg[15]_1 ;
  wire [3:0]\q_reg[19] ;
  wire [3:0]\q_reg[19]_0 ;
  wire [3:0]\q_reg[19]_1 ;
  wire [3:0]\q_reg[23] ;
  wire [0:0]\q_reg[23]_0 ;
  wire [3:0]\q_reg[23]_1 ;
  wire [3:0]\q_reg[23]_2 ;
  wire [0:0]\q_reg[24] ;
  wire [3:0]\q_reg[7] ;
  wire [3:0]\q_reg[7]_0 ;
  wire sel0_out;
  wire y0_carry__0_n_0;
  wire y0_carry__0_n_1;
  wire y0_carry__0_n_2;
  wire y0_carry__0_n_3;
  wire y0_carry__1_n_0;
  wire y0_carry__1_n_1;
  wire y0_carry__1_n_2;
  wire y0_carry__1_n_3;
  wire y0_carry__2_n_0;
  wire y0_carry__2_n_1;
  wire y0_carry__2_n_2;
  wire y0_carry__2_n_3;
  wire y0_carry__3_n_0;
  wire y0_carry__3_n_1;
  wire y0_carry__3_n_2;
  wire y0_carry__3_n_3;
  wire y0_carry__4_n_0;
  wire y0_carry__4_n_1;
  wire y0_carry__4_n_2;
  wire y0_carry__4_n_3;
  wire y0_carry_n_0;
  wire y0_carry_n_1;
  wire y0_carry_n_2;
  wire y0_carry_n_3;
  wire [3:0]NLW_y0_carry__5_CO_UNCONNECTED;
  wire [3:1]NLW_y0_carry__5_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \axi_rdata[0]_i_1 
       (.I0(O[0]),
        .I1(\axi_rdata_reg[3] [0]),
        .I2(\axi_rdata_reg[31] [0]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[31]_0 [0]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \axi_rdata[10]_i_1 
       (.I0(\q_reg[11] [2]),
        .I1(\axi_rdata_reg[11] [0]),
        .I2(\axi_rdata_reg[31] [7]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[31]_0 [7]),
        .O(D[7]));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \axi_rdata[11]_i_1 
       (.I0(\q_reg[11] [3]),
        .I1(\axi_rdata_reg[11] [1]),
        .I2(\axi_rdata_reg[31]_0 [8]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31] [8]),
        .O(D[8]));
  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \axi_rdata[12]_i_1 
       (.I0(\q_reg[15] [0]),
        .I1(\axi_rdata_reg[14] [0]),
        .I2(\axi_rdata_reg[31] [9]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[31]_0 [9]),
        .O(D[9]));
  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \axi_rdata[13]_i_1 
       (.I0(\q_reg[15] [1]),
        .I1(\axi_rdata_reg[14] [1]),
        .I2(\axi_rdata_reg[31] [10]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[31]_0 [10]),
        .O(D[10]));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \axi_rdata[14]_i_1 
       (.I0(\q_reg[15] [2]),
        .I1(\axi_rdata_reg[14] [2]),
        .I2(\axi_rdata_reg[31]_0 [11]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31] [11]),
        .O(D[11]));
  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \axi_rdata[16]_i_1 
       (.I0(\q_reg[19] [0]),
        .I1(\axi_rdata_reg[19] [0]),
        .I2(\axi_rdata_reg[31] [12]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[31]_0 [12]),
        .O(D[12]));
  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \axi_rdata[17]_i_1 
       (.I0(\q_reg[19] [1]),
        .I1(\axi_rdata_reg[19] [1]),
        .I2(\axi_rdata_reg[31] [13]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[31]_0 [13]),
        .O(D[13]));
  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \axi_rdata[18]_i_1 
       (.I0(\q_reg[19] [2]),
        .I1(\axi_rdata_reg[19] [2]),
        .I2(\axi_rdata_reg[31] [14]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[31]_0 [14]),
        .O(D[14]));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \axi_rdata[19]_i_1 
       (.I0(\q_reg[19] [3]),
        .I1(\axi_rdata_reg[19] [3]),
        .I2(\axi_rdata_reg[31]_0 [15]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31] [15]),
        .O(D[15]));
  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \axi_rdata[1]_i_1 
       (.I0(O[1]),
        .I1(\axi_rdata_reg[3] [1]),
        .I2(\axi_rdata_reg[31] [1]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[31]_0 [1]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \axi_rdata[20]_i_1 
       (.I0(\q_reg[23] [0]),
        .I1(\axi_rdata_reg[23] [0]),
        .I2(\axi_rdata_reg[31] [16]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[31]_0 [16]),
        .O(D[16]));
  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \axi_rdata[23]_i_1 
       (.I0(\q_reg[23] [3]),
        .I1(\axi_rdata_reg[23] [1]),
        .I2(\axi_rdata_reg[31] [17]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[31]_0 [17]),
        .O(D[17]));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \axi_rdata[24]_i_1 
       (.I0(\q_reg[23]_0 ),
        .I1(\axi_rdata_reg[24] ),
        .I2(\axi_rdata_reg[31]_0 [18]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31] [18]),
        .O(D[18]));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \axi_rdata[25]_i_1 
       (.I0(\q_reg[23]_0 ),
        .I1(\axi_rdata_reg[24] ),
        .I2(\axi_rdata_reg[31]_0 [19]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31] [19]),
        .O(D[19]));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \axi_rdata[26]_i_1 
       (.I0(\q_reg[23]_0 ),
        .I1(\axi_rdata_reg[24] ),
        .I2(\axi_rdata_reg[31]_0 [20]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31] [20]),
        .O(D[20]));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \axi_rdata[27]_i_1 
       (.I0(\q_reg[23]_0 ),
        .I1(\axi_rdata_reg[24] ),
        .I2(\axi_rdata_reg[31]_0 [21]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31] [21]),
        .O(D[21]));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \axi_rdata[28]_i_1 
       (.I0(\q_reg[23]_0 ),
        .I1(\axi_rdata_reg[24] ),
        .I2(\axi_rdata_reg[31]_0 [22]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31] [22]),
        .O(D[22]));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \axi_rdata[29]_i_1 
       (.I0(\q_reg[23]_0 ),
        .I1(\axi_rdata_reg[24] ),
        .I2(\axi_rdata_reg[31]_0 [23]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31] [23]),
        .O(D[23]));
  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \axi_rdata[2]_i_1 
       (.I0(O[2]),
        .I1(\axi_rdata_reg[3] [2]),
        .I2(\axi_rdata_reg[31] [2]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[31]_0 [2]),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \axi_rdata[30]_i_1 
       (.I0(\q_reg[23]_0 ),
        .I1(\axi_rdata_reg[24] ),
        .I2(\axi_rdata_reg[31]_0 [24]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31] [24]),
        .O(D[24]));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \axi_rdata[31]_i_2 
       (.I0(\q_reg[23]_0 ),
        .I1(\axi_rdata_reg[24] ),
        .I2(\axi_rdata_reg[31]_0 [25]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31] [25]),
        .O(D[25]));
  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \axi_rdata[3]_i_1 
       (.I0(O[3]),
        .I1(\axi_rdata_reg[3] [3]),
        .I2(\axi_rdata_reg[31] [3]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[31]_0 [3]),
        .O(D[3]));
  LUT6 #(
    .INIT(64'hF0FFCCAAF000CCAA)) 
    \axi_rdata[4]_i_1 
       (.I0(\q_reg[7] [0]),
        .I1(\axi_rdata_reg[7] [0]),
        .I2(\axi_rdata_reg[31] [4]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[31]_0 [4]),
        .O(D[4]));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \axi_rdata[6]_i_1 
       (.I0(\q_reg[7] [2]),
        .I1(\axi_rdata_reg[7] [1]),
        .I2(\axi_rdata_reg[31]_0 [5]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31] [5]),
        .O(D[5]));
  LUT6 #(
    .INIT(64'hFFCCF0AA00CCF0AA)) 
    \axi_rdata[7]_i_1 
       (.I0(\q_reg[7] [3]),
        .I1(\axi_rdata_reg[7] [2]),
        .I2(\axi_rdata_reg[31]_0 [6]),
        .I3(axi_araddr[1]),
        .I4(axi_araddr[0]),
        .I5(\axi_rdata_reg[31] [6]),
        .O(D[6]));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry
       (.CI(1'b0),
        .CO({y0_carry_n_0,y0_carry_n_1,y0_carry_n_2,y0_carry_n_3}),
        .CYINIT(sel0_out),
        .DI(A[3:0]),
        .O(O),
        .S(S));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__0
       (.CI(y0_carry_n_0),
        .CO({y0_carry__0_n_0,y0_carry__0_n_1,y0_carry__0_n_2,y0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(A[7:4]),
        .O(\q_reg[7] ),
        .S(\q_reg[7]_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__1
       (.CI(y0_carry__0_n_0),
        .CO({y0_carry__1_n_0,y0_carry__1_n_1,y0_carry__1_n_2,y0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({DI,A[9:8]}),
        .O(\q_reg[11] ),
        .S(\q_reg[11]_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__2
       (.CI(y0_carry__1_n_0),
        .CO({y0_carry__2_n_0,y0_carry__2_n_1,y0_carry__2_n_2,y0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({A[10],\q_reg[15]_0 }),
        .O(\q_reg[15] ),
        .S(\q_reg[15]_1 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__3
       (.CI(y0_carry__2_n_0),
        .CO({y0_carry__3_n_0,y0_carry__3_n_1,y0_carry__3_n_2,y0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[19]_0 ),
        .O(\q_reg[19] ),
        .S(\q_reg[19]_1 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__4
       (.CI(y0_carry__3_n_0),
        .CO({y0_carry__4_n_0,y0_carry__4_n_1,y0_carry__4_n_2,y0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[23]_1 ),
        .O(\q_reg[23] ),
        .S(\q_reg[23]_2 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__5
       (.CI(y0_carry__4_n_0),
        .CO(NLW_y0_carry__5_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_y0_carry__5_O_UNCONNECTED[3:1],\q_reg[23]_0 }),
        .S({1'b0,1'b0,1'b0,\q_reg[24] }));
endmodule

(* ORIG_REF_NAME = "Mux_2_1" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mux_2_1_0
   (\q_reg[23] ,
    O,
    \q_reg[11] ,
    \q_reg[15] ,
    \q_reg[19] ,
    \q_reg[23]_0 ,
    \q_reg[23]_1 ,
    D,
    Q,
    DI,
    S,
    \q_reg[7] ,
    \q_reg[7]_0 ,
    \q_reg[11]_0 ,
    \q_reg[11]_1 ,
    \q_reg[15]_0 ,
    \q_reg[15]_1 ,
    \q_reg[15]_2 ,
    \q_reg[19]_0 ,
    \q_reg[19]_1 ,
    \q_reg[23]_2 ,
    \q_reg[23]_3 ,
    \q_reg[24] ,
    \axi_rdata_reg[22] ,
    \axi_rdata_reg[5] ,
    axi_araddr,
    \axi_rdata_reg[22]_0 ,
    \axi_rdata_reg[9] ,
    \axi_rdata_reg[15] ,
    \axi_rdata_reg[22]_1 );
  output [3:0]\q_reg[23] ;
  output [3:0]O;
  output [3:0]\q_reg[11] ;
  output [3:0]\q_reg[15] ;
  output [3:0]\q_reg[19] ;
  output [3:0]\q_reg[23]_0 ;
  output [0:0]\q_reg[23]_1 ;
  output [5:0]D;
  input [0:0]Q;
  input [3:0]DI;
  input [3:0]S;
  input [3:0]\q_reg[7] ;
  input [3:0]\q_reg[7]_0 ;
  input [3:0]\q_reg[11]_0 ;
  input [3:0]\q_reg[11]_1 ;
  input \q_reg[15]_0 ;
  input [2:0]\q_reg[15]_1 ;
  input [3:0]\q_reg[15]_2 ;
  input [3:0]\q_reg[19]_0 ;
  input [3:0]\q_reg[19]_1 ;
  input [3:0]\q_reg[23]_2 ;
  input [3:0]\q_reg[23]_3 ;
  input [0:0]\q_reg[24] ;
  input [5:0]\axi_rdata_reg[22] ;
  input [0:0]\axi_rdata_reg[5] ;
  input [1:0]axi_araddr;
  input [5:0]\axi_rdata_reg[22]_0 ;
  input [1:0]\axi_rdata_reg[9] ;
  input [0:0]\axi_rdata_reg[15] ;
  input [1:0]\axi_rdata_reg[22]_1 ;

  wire [5:0]D;
  wire [3:0]DI;
  wire [3:0]O;
  wire [0:0]Q;
  wire [3:0]S;
  wire [1:0]axi_araddr;
  wire [0:0]\axi_rdata_reg[15] ;
  wire [5:0]\axi_rdata_reg[22] ;
  wire [5:0]\axi_rdata_reg[22]_0 ;
  wire [1:0]\axi_rdata_reg[22]_1 ;
  wire [0:0]\axi_rdata_reg[5] ;
  wire [1:0]\axi_rdata_reg[9] ;
  wire [3:0]\q_reg[11] ;
  wire [3:0]\q_reg[11]_0 ;
  wire [3:0]\q_reg[11]_1 ;
  wire [3:0]\q_reg[15] ;
  wire \q_reg[15]_0 ;
  wire [2:0]\q_reg[15]_1 ;
  wire [3:0]\q_reg[15]_2 ;
  wire [3:0]\q_reg[19] ;
  wire [3:0]\q_reg[19]_0 ;
  wire [3:0]\q_reg[19]_1 ;
  wire [3:0]\q_reg[23] ;
  wire [3:0]\q_reg[23]_0 ;
  wire [0:0]\q_reg[23]_1 ;
  wire [3:0]\q_reg[23]_2 ;
  wire [3:0]\q_reg[23]_3 ;
  wire [0:0]\q_reg[24] ;
  wire [3:0]\q_reg[7] ;
  wire [3:0]\q_reg[7]_0 ;
  wire y0_carry__0_n_0;
  wire y0_carry__0_n_1;
  wire y0_carry__0_n_2;
  wire y0_carry__0_n_3;
  wire y0_carry__1_n_0;
  wire y0_carry__1_n_1;
  wire y0_carry__1_n_2;
  wire y0_carry__1_n_3;
  wire y0_carry__2_n_0;
  wire y0_carry__2_n_1;
  wire y0_carry__2_n_2;
  wire y0_carry__2_n_3;
  wire y0_carry__3_n_0;
  wire y0_carry__3_n_1;
  wire y0_carry__3_n_2;
  wire y0_carry__3_n_3;
  wire y0_carry__4_n_0;
  wire y0_carry__4_n_1;
  wire y0_carry__4_n_2;
  wire y0_carry__4_n_3;
  wire y0_carry_n_0;
  wire y0_carry_n_1;
  wire y0_carry_n_2;
  wire y0_carry_n_3;
  wire [3:0]NLW_y0_carry__5_CO_UNCONNECTED;
  wire [3:1]NLW_y0_carry__5_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'hFFCCAAF000CCAAF0)) 
    \axi_rdata[15]_i_1 
       (.I0(\q_reg[15] [3]),
        .I1(\axi_rdata_reg[22] [3]),
        .I2(\axi_rdata_reg[15] ),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[22]_0 [3]),
        .O(D[3]));
  LUT6 #(
    .INIT(64'hFFCCAAF000CCAAF0)) 
    \axi_rdata[21]_i_1 
       (.I0(\q_reg[23]_0 [1]),
        .I1(\axi_rdata_reg[22] [4]),
        .I2(\axi_rdata_reg[22]_1 [0]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[22]_0 [4]),
        .O(D[4]));
  LUT6 #(
    .INIT(64'hFFCCAAF000CCAAF0)) 
    \axi_rdata[22]_i_1 
       (.I0(\q_reg[23]_0 [2]),
        .I1(\axi_rdata_reg[22] [5]),
        .I2(\axi_rdata_reg[22]_1 [1]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[22]_0 [5]),
        .O(D[5]));
  LUT6 #(
    .INIT(64'hFFCCAAF000CCAAF0)) 
    \axi_rdata[5]_i_1 
       (.I0(O[1]),
        .I1(\axi_rdata_reg[22] [0]),
        .I2(\axi_rdata_reg[5] ),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[22]_0 [0]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hCCFFAAF0CC00AAF0)) 
    \axi_rdata[8]_i_1 
       (.I0(\q_reg[11] [0]),
        .I1(\axi_rdata_reg[22]_0 [1]),
        .I2(\axi_rdata_reg[9] [0]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[22] [1]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hFFCCAAF000CCAAF0)) 
    \axi_rdata[9]_i_1 
       (.I0(\q_reg[11] [1]),
        .I1(\axi_rdata_reg[22] [2]),
        .I2(\axi_rdata_reg[9] [1]),
        .I3(axi_araddr[0]),
        .I4(axi_araddr[1]),
        .I5(\axi_rdata_reg[22]_0 [2]),
        .O(D[2]));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry
       (.CI(1'b0),
        .CO({y0_carry_n_0,y0_carry_n_1,y0_carry_n_2,y0_carry_n_3}),
        .CYINIT(Q),
        .DI(DI),
        .O(\q_reg[23] ),
        .S(S));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__0
       (.CI(y0_carry_n_0),
        .CO({y0_carry__0_n_0,y0_carry__0_n_1,y0_carry__0_n_2,y0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[7] ),
        .O(O),
        .S(\q_reg[7]_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__1
       (.CI(y0_carry__0_n_0),
        .CO({y0_carry__1_n_0,y0_carry__1_n_1,y0_carry__1_n_2,y0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[11]_0 ),
        .O(\q_reg[11] ),
        .S(\q_reg[11]_1 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__2
       (.CI(y0_carry__1_n_0),
        .CO({y0_carry__2_n_0,y0_carry__2_n_1,y0_carry__2_n_2,y0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({\q_reg[15]_0 ,\q_reg[15]_1 }),
        .O(\q_reg[15] ),
        .S(\q_reg[15]_2 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__3
       (.CI(y0_carry__2_n_0),
        .CO({y0_carry__3_n_0,y0_carry__3_n_1,y0_carry__3_n_2,y0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[19]_0 ),
        .O(\q_reg[19] ),
        .S(\q_reg[19]_1 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__4
       (.CI(y0_carry__3_n_0),
        .CO({y0_carry__4_n_0,y0_carry__4_n_1,y0_carry__4_n_2,y0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[23]_2 ),
        .O(\q_reg[23]_0 ),
        .S(\q_reg[23]_3 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__5
       (.CI(y0_carry__4_n_0),
        .CO(NLW_y0_carry__5_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_y0_carry__5_O_UNCONNECTED[3:1],\q_reg[23]_1 }),
        .S({1'b0,1'b0,1'b0,\q_reg[24] }));
endmodule

(* ORIG_REF_NAME = "Mux_2_1" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mux_2_1__parameterized2
   (\q_reg[3] ,
    \q_reg[7] ,
    \q_reg[11] ,
    \q_reg[15] ,
    \q_reg[19] ,
    O,
    sel0_out,
    Q,
    S,
    \q_reg[7]_0 ,
    \q_reg[11]_0 ,
    \q_reg[15]_0 ,
    \q_reg[19]_0 ,
    \q_reg[23] );
  output [3:0]\q_reg[3] ;
  output [3:0]\q_reg[7] ;
  output [3:0]\q_reg[11] ;
  output [3:0]\q_reg[15] ;
  output [3:0]\q_reg[19] ;
  output [3:0]O;
  input sel0_out;
  input [22:0]Q;
  input [3:0]S;
  input [3:0]\q_reg[7]_0 ;
  input [3:0]\q_reg[11]_0 ;
  input [3:0]\q_reg[15]_0 ;
  input [3:0]\q_reg[19]_0 ;
  input [3:0]\q_reg[23] ;

  wire [3:0]O;
  wire [22:0]Q;
  wire [3:0]S;
  wire [3:0]\q_reg[11] ;
  wire [3:0]\q_reg[11]_0 ;
  wire [3:0]\q_reg[15] ;
  wire [3:0]\q_reg[15]_0 ;
  wire [3:0]\q_reg[19] ;
  wire [3:0]\q_reg[19]_0 ;
  wire [3:0]\q_reg[23] ;
  wire [3:0]\q_reg[3] ;
  wire [3:0]\q_reg[7] ;
  wire [3:0]\q_reg[7]_0 ;
  wire sel0_out;
  wire y0_carry__0_n_0;
  wire y0_carry__0_n_1;
  wire y0_carry__0_n_2;
  wire y0_carry__0_n_3;
  wire y0_carry__1_n_0;
  wire y0_carry__1_n_1;
  wire y0_carry__1_n_2;
  wire y0_carry__1_n_3;
  wire y0_carry__2_n_0;
  wire y0_carry__2_n_1;
  wire y0_carry__2_n_2;
  wire y0_carry__2_n_3;
  wire y0_carry__3_n_0;
  wire y0_carry__3_n_1;
  wire y0_carry__3_n_2;
  wire y0_carry__3_n_3;
  wire y0_carry__4_n_1;
  wire y0_carry__4_n_2;
  wire y0_carry__4_n_3;
  wire y0_carry_n_0;
  wire y0_carry_n_1;
  wire y0_carry_n_2;
  wire y0_carry_n_3;
  wire [3:3]NLW_y0_carry__4_CO_UNCONNECTED;

  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry
       (.CI(1'b0),
        .CO({y0_carry_n_0,y0_carry_n_1,y0_carry_n_2,y0_carry_n_3}),
        .CYINIT(sel0_out),
        .DI(Q[3:0]),
        .O(\q_reg[3] ),
        .S(S));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__0
       (.CI(y0_carry_n_0),
        .CO({y0_carry__0_n_0,y0_carry__0_n_1,y0_carry__0_n_2,y0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(Q[7:4]),
        .O(\q_reg[7] ),
        .S(\q_reg[7]_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__1
       (.CI(y0_carry__0_n_0),
        .CO({y0_carry__1_n_0,y0_carry__1_n_1,y0_carry__1_n_2,y0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(Q[11:8]),
        .O(\q_reg[11] ),
        .S(\q_reg[11]_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__2
       (.CI(y0_carry__1_n_0),
        .CO({y0_carry__2_n_0,y0_carry__2_n_1,y0_carry__2_n_2,y0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(Q[15:12]),
        .O(\q_reg[15] ),
        .S(\q_reg[15]_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__3
       (.CI(y0_carry__2_n_0),
        .CO({y0_carry__3_n_0,y0_carry__3_n_1,y0_carry__3_n_2,y0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(Q[19:16]),
        .O(\q_reg[19] ),
        .S(\q_reg[19]_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__4
       (.CI(y0_carry__3_n_0),
        .CO({NLW_y0_carry__4_CO_UNCONNECTED[3],y0_carry__4_n_1,y0_carry__4_n_2,y0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,Q[22:20]}),
        .O(O),
        .S(\q_reg[23] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ROM
   (out_ROM,
    data_reg_0,
    s00_axi_aclk,
    addr);
  output [22:0]out_ROM;
  output [0:0]data_reg_0;
  input s00_axi_aclk;
  input [4:0]addr;

  wire [4:0]addr;
  wire [0:0]data_reg_0;
  wire [22:0]out_ROM;
  wire s00_axi_aclk;
  wire [15:5]NLW_data_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d5" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "736" *) 
  (* RTL_RAM_NAME = "U0/project_audio_2_ip_v1_0_S00_AXI_inst/cordic_rotation0/data_path0/ROM_1/data_reg" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_ext_slice_begin = "18" *) 
  (* ram_ext_slice_end = "22" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000658),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h005100A20145028B05170A2F145F28BE517CA2F645D78B0D1111FB38E4050000),
    .INIT_01(256'h0000000000000000000000000000000000000000000100020005000A00140028),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000001000200040008),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    data_reg
       (.ADDRARDADDR({1'b0,1'b0,1'b0,1'b0,1'b0,addr,1'b0,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({1'b1,1'b0,1'b0,1'b0,1'b0,addr,1'b0,1'b0,1'b0,1'b0}),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DIADI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(out_ROM[15:0]),
        .DOBDO({NLW_data_reg_DOBDO_UNCONNECTED[15:5],out_ROM[22:18]}),
        .DOPADOP(out_ROM[17:16]),
        .DOPBDOP(NLW_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT1 #(
    .INIT(2'h1)) 
    y0_carry__4_i_1
       (.I0(out_ROM[22]),
        .O(data_reg_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_control_path
   (addr,
    D,
    \FSM_sequential_state_reg[0] ,
    en_reg,
    \q_reg[1] ,
    \q_reg[2] ,
    \q_reg[1]_0 ,
    \q_reg[1]_1 ,
    \q_reg[1]_2 ,
    \q_reg[2]_0 ,
    \q_reg[1]_3 ,
    \q_reg[1]_4 ,
    S,
    \q_reg[14] ,
    \q_reg[19] ,
    \q_reg[22] ,
    \q_reg[11] ,
    \q_reg[14]_0 ,
    \q_reg[19]_0 ,
    \q_reg[22]_0 ,
    \q_reg[0] ,
    p_0_in,
    s00_axi_wdata,
    O,
    \q_reg[19]_1 ,
    \q_reg[15] ,
    \q_reg[11]_0 ,
    \q_reg[7] ,
    \q_reg[3] ,
    y0_carry__1_i_2__0,
    y0_carry__1_i_2__0_0,
    y0_carry__1_i_2__0_1,
    y0_carry__2_i_1__0,
    y0_carry__3_i_2__0,
    y0_carry__4_i_2__0,
    y0_carry__4_i_2__0_0,
    y0_carry__1_i_3__0,
    y0_carry__1_i_3__0_0,
    y0_carry__1_i_3__0_1,
    y0_carry__2_i_3__0,
    y0_carry__3_i_3__0,
    \q_reg[23] ,
    \q_reg[23]_0 ,
    \q_reg[23]_1 ,
    \q_reg[19]_2 ,
    \q_reg[19]_3 ,
    y0_carry__1_i_6,
    y0_carry__1_i_7,
    y0_carry__1_i_2__1,
    y0_carry__1_i_2__1_0,
    y0_carry__1_i_2__1_1,
    y0_carry__2_i_1__1,
    y0_carry__3_i_2__1,
    A,
    y0_carry__1_i_3__1,
    y0_carry__1_i_3__1_0,
    y0_carry__1_i_3__1_1,
    y0_carry__2_i_3__1,
    y0_carry__3_i_3__1,
    Q,
    \q_reg[11]_1 ,
    \q_reg[11]_2 ,
    \q_reg[15]_0 ,
    \q_reg[15]_1 ,
    \q_reg[15]_2 ,
    s00_axi_aclk);
  output [4:0]addr;
  output [23:0]D;
  output \FSM_sequential_state_reg[0] ;
  output en_reg;
  output \q_reg[1] ;
  output \q_reg[2] ;
  output \q_reg[1]_0 ;
  output \q_reg[1]_1 ;
  output \q_reg[1]_2 ;
  output \q_reg[2]_0 ;
  output \q_reg[1]_3 ;
  output \q_reg[1]_4 ;
  output [1:0]S;
  output [2:0]\q_reg[14] ;
  output [1:0]\q_reg[19] ;
  output [2:0]\q_reg[22] ;
  output [1:0]\q_reg[11] ;
  output [2:0]\q_reg[14]_0 ;
  output [1:0]\q_reg[19]_0 ;
  output [2:0]\q_reg[22]_0 ;
  input \q_reg[0] ;
  input [0:0]p_0_in;
  input [23:0]s00_axi_wdata;
  input [3:0]O;
  input [3:0]\q_reg[19]_1 ;
  input [3:0]\q_reg[15] ;
  input [3:0]\q_reg[11]_0 ;
  input [3:0]\q_reg[7] ;
  input [3:0]\q_reg[3] ;
  input y0_carry__1_i_2__0;
  input y0_carry__1_i_2__0_0;
  input y0_carry__1_i_2__0_1;
  input y0_carry__2_i_1__0;
  input y0_carry__3_i_2__0;
  input y0_carry__4_i_2__0;
  input y0_carry__4_i_2__0_0;
  input y0_carry__1_i_3__0;
  input y0_carry__1_i_3__0_0;
  input y0_carry__1_i_3__0_1;
  input y0_carry__2_i_3__0;
  input y0_carry__3_i_3__0;
  input \q_reg[23] ;
  input \q_reg[23]_0 ;
  input \q_reg[23]_1 ;
  input \q_reg[19]_2 ;
  input \q_reg[19]_3 ;
  input y0_carry__1_i_6;
  input y0_carry__1_i_7;
  input y0_carry__1_i_2__1;
  input y0_carry__1_i_2__1_0;
  input y0_carry__1_i_2__1_1;
  input y0_carry__2_i_1__1;
  input y0_carry__3_i_2__1;
  input [13:0]A;
  input y0_carry__1_i_3__1;
  input y0_carry__1_i_3__1_0;
  input y0_carry__1_i_3__1_1;
  input y0_carry__2_i_3__1;
  input y0_carry__3_i_3__1;
  input [0:0]Q;
  input \q_reg[11]_1 ;
  input \q_reg[11]_2 ;
  input \q_reg[15]_0 ;
  input \q_reg[15]_1 ;
  input \q_reg[15]_2 ;
  input s00_axi_aclk;

  wire [13:0]A;
  wire [23:0]D;
  wire \FSM_sequential_state_reg[0] ;
  wire [3:0]O;
  wire [0:0]Q;
  wire [1:0]S;
  wire [4:0]addr;
  wire en_reg;
  wire [0:0]p_0_in;
  wire \q_reg[0] ;
  wire [1:0]\q_reg[11] ;
  wire [3:0]\q_reg[11]_0 ;
  wire \q_reg[11]_1 ;
  wire \q_reg[11]_2 ;
  wire [2:0]\q_reg[14] ;
  wire [2:0]\q_reg[14]_0 ;
  wire [3:0]\q_reg[15] ;
  wire \q_reg[15]_0 ;
  wire \q_reg[15]_1 ;
  wire \q_reg[15]_2 ;
  wire [1:0]\q_reg[19] ;
  wire [1:0]\q_reg[19]_0 ;
  wire [3:0]\q_reg[19]_1 ;
  wire \q_reg[19]_2 ;
  wire \q_reg[19]_3 ;
  wire \q_reg[1] ;
  wire \q_reg[1]_0 ;
  wire \q_reg[1]_1 ;
  wire \q_reg[1]_2 ;
  wire \q_reg[1]_3 ;
  wire \q_reg[1]_4 ;
  wire [2:0]\q_reg[22] ;
  wire [2:0]\q_reg[22]_0 ;
  wire \q_reg[23] ;
  wire \q_reg[23]_0 ;
  wire \q_reg[23]_1 ;
  wire \q_reg[2] ;
  wire \q_reg[2]_0 ;
  wire [3:0]\q_reg[3] ;
  wire [3:0]\q_reg[7] ;
  wire regr_n_0;
  wire s00_axi_aclk;
  wire [23:0]s00_axi_wdata;
  wire y0_carry__1_i_2__0;
  wire y0_carry__1_i_2__0_0;
  wire y0_carry__1_i_2__0_1;
  wire y0_carry__1_i_2__1;
  wire y0_carry__1_i_2__1_0;
  wire y0_carry__1_i_2__1_1;
  wire y0_carry__1_i_3__0;
  wire y0_carry__1_i_3__0_0;
  wire y0_carry__1_i_3__0_1;
  wire y0_carry__1_i_3__1;
  wire y0_carry__1_i_3__1_0;
  wire y0_carry__1_i_3__1_1;
  wire y0_carry__1_i_6;
  wire y0_carry__1_i_7;
  wire y0_carry__2_i_1__0;
  wire y0_carry__2_i_1__1;
  wire y0_carry__2_i_3__0;
  wire y0_carry__2_i_3__1;
  wire y0_carry__3_i_2__0;
  wire y0_carry__3_i_2__1;
  wire y0_carry__3_i_3__0;
  wire y0_carry__3_i_3__1;
  wire y0_carry__4_i_2__0;
  wire y0_carry__4_i_2__0_0;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fsm fsm0
       (.D(D),
        .\FSM_sequential_state_reg[0]_0 (\FSM_sequential_state_reg[0] ),
        .\FSM_sequential_state_reg[0]_1 (en_reg),
        .\FSM_sequential_state_reg[0]_2 (regr_n_0),
        .O(O),
        .p_0_in(p_0_in),
        .\q_reg[0] (\q_reg[0] ),
        .\q_reg[11] (\q_reg[11]_0 ),
        .\q_reg[15] (\q_reg[15] ),
        .\q_reg[19] (\q_reg[19]_1 ),
        .\q_reg[3] (\q_reg[3] ),
        .\q_reg[7] (\q_reg[7] ),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_wdata(s00_axi_wdata));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg regr
       (.A(A),
        .Q(Q),
        .S(S),
        .\q_reg[0]_0 (addr[0]),
        .\q_reg[0]_1 (\FSM_sequential_state_reg[0] ),
        .\q_reg[0]_2 (en_reg),
        .\q_reg[11] (\q_reg[11] ),
        .\q_reg[11]_0 (\q_reg[11]_1 ),
        .\q_reg[11]_1 (\q_reg[11]_2 ),
        .\q_reg[14] (\q_reg[14] ),
        .\q_reg[14]_0 (\q_reg[14]_0 ),
        .\q_reg[15] (\q_reg[15]_0 ),
        .\q_reg[15]_0 (\q_reg[15]_1 ),
        .\q_reg[15]_1 (\q_reg[15]_2 ),
        .\q_reg[19] (\q_reg[19] ),
        .\q_reg[19]_0 (\q_reg[19]_0 ),
        .\q_reg[19]_1 (\q_reg[19]_2 ),
        .\q_reg[19]_2 (\q_reg[19]_3 ),
        .\q_reg[1]_0 (addr[1]),
        .\q_reg[1]_1 (\q_reg[1] ),
        .\q_reg[1]_2 (\q_reg[1]_0 ),
        .\q_reg[1]_3 (\q_reg[1]_1 ),
        .\q_reg[1]_4 (\q_reg[1]_2 ),
        .\q_reg[1]_5 (\q_reg[1]_3 ),
        .\q_reg[1]_6 (\q_reg[1]_4 ),
        .\q_reg[22] (\q_reg[22] ),
        .\q_reg[22]_0 (\q_reg[22]_0 ),
        .\q_reg[23] (\q_reg[23] ),
        .\q_reg[23]_0 (\q_reg[23]_0 ),
        .\q_reg[23]_1 (\q_reg[23]_1 ),
        .\q_reg[2]_0 (addr[2]),
        .\q_reg[2]_1 (\q_reg[2] ),
        .\q_reg[2]_2 (\q_reg[2]_0 ),
        .\q_reg[3]_0 (addr[3]),
        .\q_reg[4]_0 (regr_n_0),
        .\q_reg[4]_1 (addr[4]),
        .s00_axi_aclk(s00_axi_aclk),
        .y0_carry__1_i_2__0_0(y0_carry__1_i_2__0),
        .y0_carry__1_i_2__0_1(y0_carry__1_i_2__0_0),
        .y0_carry__1_i_2__0_2(y0_carry__1_i_2__0_1),
        .y0_carry__1_i_2__1_0(y0_carry__1_i_2__1),
        .y0_carry__1_i_2__1_1(y0_carry__1_i_2__1_0),
        .y0_carry__1_i_2__1_2(y0_carry__1_i_2__1_1),
        .y0_carry__1_i_3__0(y0_carry__1_i_3__0),
        .y0_carry__1_i_3__0_0(y0_carry__1_i_3__0_0),
        .y0_carry__1_i_3__0_1(y0_carry__1_i_3__0_1),
        .y0_carry__1_i_3__1(y0_carry__1_i_3__1),
        .y0_carry__1_i_3__1_0(y0_carry__1_i_3__1_0),
        .y0_carry__1_i_3__1_1(y0_carry__1_i_3__1_1),
        .y0_carry__1_i_6_0(y0_carry__1_i_6),
        .y0_carry__1_i_7_0(y0_carry__1_i_7),
        .y0_carry__2_i_1__0(y0_carry__2_i_1__0),
        .y0_carry__2_i_1__1(y0_carry__2_i_1__1),
        .y0_carry__2_i_3__0_0(y0_carry__2_i_3__0),
        .y0_carry__2_i_3__1_0(y0_carry__2_i_3__1),
        .y0_carry__3_i_2__0_0(y0_carry__3_i_2__0),
        .y0_carry__3_i_2__1_0(y0_carry__3_i_2__1),
        .y0_carry__3_i_3__0(y0_carry__3_i_3__0),
        .y0_carry__3_i_3__1(y0_carry__3_i_3__1),
        .y0_carry__4_i_2__0_0(y0_carry__4_i_2__0),
        .y0_carry__4_i_2__0_1(y0_carry__4_i_2__0_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_cordic_rotation
   (axi_wready_reg,
    D,
    s00_axi_aclk,
    \q_reg[0] ,
    \q_reg[0]_0 ,
    s00_axi_wvalid,
    s00_axi_awvalid,
    p_0_in,
    s00_axi_wdata,
    Q,
    axi_araddr,
    \axi_rdata_reg[31] );
  output axi_wready_reg;
  output [31:0]D;
  input s00_axi_aclk;
  input \q_reg[0] ;
  input \q_reg[0]_0 ;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [1:0]p_0_in;
  input [23:0]s00_axi_wdata;
  input [31:0]Q;
  input [1:0]axi_araddr;
  input [31:0]\axi_rdata_reg[31] ;

  wire [24:10]A;
  wire [31:0]D;
  wire [31:0]Q;
  wire [4:0]addr;
  wire [1:0]axi_araddr;
  wire [31:0]\axi_rdata_reg[31] ;
  wire axi_wready_reg;
  wire control_path0_n_10;
  wire control_path0_n_11;
  wire control_path0_n_12;
  wire control_path0_n_13;
  wire control_path0_n_14;
  wire control_path0_n_15;
  wire control_path0_n_16;
  wire control_path0_n_17;
  wire control_path0_n_18;
  wire control_path0_n_19;
  wire control_path0_n_20;
  wire control_path0_n_21;
  wire control_path0_n_22;
  wire control_path0_n_23;
  wire control_path0_n_24;
  wire control_path0_n_25;
  wire control_path0_n_26;
  wire control_path0_n_27;
  wire control_path0_n_28;
  wire control_path0_n_29;
  wire control_path0_n_31;
  wire control_path0_n_32;
  wire control_path0_n_33;
  wire control_path0_n_34;
  wire control_path0_n_35;
  wire control_path0_n_36;
  wire control_path0_n_37;
  wire control_path0_n_38;
  wire control_path0_n_39;
  wire control_path0_n_40;
  wire control_path0_n_41;
  wire control_path0_n_42;
  wire control_path0_n_43;
  wire control_path0_n_44;
  wire control_path0_n_45;
  wire control_path0_n_46;
  wire control_path0_n_47;
  wire control_path0_n_48;
  wire control_path0_n_49;
  wire control_path0_n_5;
  wire control_path0_n_50;
  wire control_path0_n_51;
  wire control_path0_n_52;
  wire control_path0_n_53;
  wire control_path0_n_54;
  wire control_path0_n_55;
  wire control_path0_n_56;
  wire control_path0_n_57;
  wire control_path0_n_58;
  wire control_path0_n_6;
  wire control_path0_n_7;
  wire control_path0_n_8;
  wire control_path0_n_9;
  wire data_path0_n_0;
  wire data_path0_n_1;
  wire data_path0_n_10;
  wire data_path0_n_11;
  wire data_path0_n_12;
  wire data_path0_n_13;
  wire data_path0_n_14;
  wire data_path0_n_15;
  wire data_path0_n_16;
  wire data_path0_n_17;
  wire data_path0_n_18;
  wire data_path0_n_19;
  wire data_path0_n_2;
  wire data_path0_n_20;
  wire data_path0_n_21;
  wire data_path0_n_22;
  wire data_path0_n_23;
  wire data_path0_n_3;
  wire data_path0_n_38;
  wire data_path0_n_39;
  wire data_path0_n_4;
  wire data_path0_n_40;
  wire data_path0_n_41;
  wire data_path0_n_42;
  wire data_path0_n_43;
  wire data_path0_n_44;
  wire data_path0_n_45;
  wire data_path0_n_46;
  wire data_path0_n_47;
  wire data_path0_n_48;
  wire data_path0_n_49;
  wire data_path0_n_5;
  wire data_path0_n_50;
  wire data_path0_n_51;
  wire data_path0_n_53;
  wire data_path0_n_54;
  wire data_path0_n_55;
  wire data_path0_n_56;
  wire data_path0_n_57;
  wire data_path0_n_58;
  wire data_path0_n_59;
  wire data_path0_n_6;
  wire data_path0_n_60;
  wire data_path0_n_61;
  wire data_path0_n_62;
  wire data_path0_n_63;
  wire data_path0_n_64;
  wire data_path0_n_65;
  wire data_path0_n_66;
  wire data_path0_n_67;
  wire data_path0_n_68;
  wire data_path0_n_69;
  wire data_path0_n_7;
  wire data_path0_n_70;
  wire data_path0_n_71;
  wire data_path0_n_72;
  wire data_path0_n_73;
  wire data_path0_n_8;
  wire data_path0_n_9;
  wire en_reg;
  wire [1:0]p_0_in;
  wire \q_reg[0] ;
  wire \q_reg[0]_0 ;
  wire s00_axi_aclk;
  wire s00_axi_awvalid;
  wire [23:0]s00_axi_wdata;
  wire s00_axi_wvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_control_path control_path0
       (.A({A[24:16],A[14:10]}),
        .D({control_path0_n_5,control_path0_n_6,control_path0_n_7,control_path0_n_8,control_path0_n_9,control_path0_n_10,control_path0_n_11,control_path0_n_12,control_path0_n_13,control_path0_n_14,control_path0_n_15,control_path0_n_16,control_path0_n_17,control_path0_n_18,control_path0_n_19,control_path0_n_20,control_path0_n_21,control_path0_n_22,control_path0_n_23,control_path0_n_24,control_path0_n_25,control_path0_n_26,control_path0_n_27,control_path0_n_28}),
        .\FSM_sequential_state_reg[0] (control_path0_n_29),
        .O({data_path0_n_20,data_path0_n_21,data_path0_n_22,data_path0_n_23}),
        .Q(data_path0_n_38),
        .S({control_path0_n_39,control_path0_n_40}),
        .addr(addr),
        .en_reg(en_reg),
        .p_0_in(p_0_in[1]),
        .\q_reg[0] (axi_wready_reg),
        .\q_reg[11] ({control_path0_n_49,control_path0_n_50}),
        .\q_reg[11]_0 ({data_path0_n_8,data_path0_n_9,data_path0_n_10,data_path0_n_11}),
        .\q_reg[11]_1 (data_path0_n_40),
        .\q_reg[11]_2 (data_path0_n_39),
        .\q_reg[14] ({control_path0_n_41,control_path0_n_42,control_path0_n_43}),
        .\q_reg[14]_0 ({control_path0_n_51,control_path0_n_52,control_path0_n_53}),
        .\q_reg[15] ({data_path0_n_12,data_path0_n_13,data_path0_n_14,data_path0_n_15}),
        .\q_reg[15]_0 (data_path0_n_43),
        .\q_reg[15]_1 (data_path0_n_42),
        .\q_reg[15]_2 (data_path0_n_41),
        .\q_reg[19] ({control_path0_n_44,control_path0_n_45}),
        .\q_reg[19]_0 ({control_path0_n_54,control_path0_n_55}),
        .\q_reg[19]_1 ({data_path0_n_16,data_path0_n_17,data_path0_n_18,data_path0_n_19}),
        .\q_reg[19]_2 (data_path0_n_44),
        .\q_reg[19]_3 (data_path0_n_45),
        .\q_reg[1] (control_path0_n_31),
        .\q_reg[1]_0 (control_path0_n_33),
        .\q_reg[1]_1 (control_path0_n_34),
        .\q_reg[1]_2 (control_path0_n_35),
        .\q_reg[1]_3 (control_path0_n_37),
        .\q_reg[1]_4 (control_path0_n_38),
        .\q_reg[22] ({control_path0_n_46,control_path0_n_47,control_path0_n_48}),
        .\q_reg[22]_0 ({control_path0_n_56,control_path0_n_57,control_path0_n_58}),
        .\q_reg[23] (data_path0_n_49),
        .\q_reg[23]_0 (data_path0_n_50),
        .\q_reg[23]_1 (data_path0_n_51),
        .\q_reg[2] (control_path0_n_32),
        .\q_reg[2]_0 (control_path0_n_36),
        .\q_reg[3] ({data_path0_n_0,data_path0_n_1,data_path0_n_2,data_path0_n_3}),
        .\q_reg[7] ({data_path0_n_4,data_path0_n_5,data_path0_n_6,data_path0_n_7}),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_wdata(s00_axi_wdata),
        .y0_carry__1_i_2__0(data_path0_n_55),
        .y0_carry__1_i_2__0_0(data_path0_n_53),
        .y0_carry__1_i_2__0_1(data_path0_n_54),
        .y0_carry__1_i_2__1(data_path0_n_66),
        .y0_carry__1_i_2__1_0(data_path0_n_64),
        .y0_carry__1_i_2__1_1(data_path0_n_65),
        .y0_carry__1_i_3__0(data_path0_n_61),
        .y0_carry__1_i_3__0_0(data_path0_n_59),
        .y0_carry__1_i_3__0_1(data_path0_n_60),
        .y0_carry__1_i_3__1(data_path0_n_71),
        .y0_carry__1_i_3__1_0(data_path0_n_69),
        .y0_carry__1_i_3__1_1(data_path0_n_70),
        .y0_carry__1_i_6(data_path0_n_46),
        .y0_carry__1_i_7(data_path0_n_47),
        .y0_carry__2_i_1__0(data_path0_n_58),
        .y0_carry__2_i_1__1(data_path0_n_68),
        .y0_carry__2_i_3__0(data_path0_n_63),
        .y0_carry__2_i_3__1(data_path0_n_73),
        .y0_carry__3_i_2__0(data_path0_n_57),
        .y0_carry__3_i_2__1(data_path0_n_67),
        .y0_carry__3_i_3__0(data_path0_n_62),
        .y0_carry__3_i_3__1(data_path0_n_72),
        .y0_carry__4_i_2__0(data_path0_n_56),
        .y0_carry__4_i_2__0_0(data_path0_n_48));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_data_path data_path0
       (.D(D),
        .O({data_path0_n_20,data_path0_n_21,data_path0_n_22,data_path0_n_23}),
        .Q(data_path0_n_38),
        .S({control_path0_n_39,control_path0_n_40}),
        .addr(addr),
        .axi_araddr(axi_araddr),
        .\axi_rdata_reg[31] (Q),
        .\axi_rdata_reg[31]_0 (\axi_rdata_reg[31] ),
        .axi_wready_reg(axi_wready_reg),
        .en_reg(en_reg),
        .p_0_in(p_0_in[0]),
        .\q_reg[0] (\q_reg[0] ),
        .\q_reg[0]_0 (\q_reg[0]_0 ),
        .\q_reg[0]_1 (control_path0_n_29),
        .\q_reg[10] (data_path0_n_40),
        .\q_reg[11] ({data_path0_n_8,data_path0_n_9,data_path0_n_10,data_path0_n_11}),
        .\q_reg[11]_0 (data_path0_n_39),
        .\q_reg[11]_1 ({control_path0_n_49,control_path0_n_50}),
        .\q_reg[11]_2 (control_path0_n_33),
        .\q_reg[11]_3 (control_path0_n_37),
        .\q_reg[12] (data_path0_n_43),
        .\q_reg[13] (data_path0_n_42),
        .\q_reg[14] (data_path0_n_41),
        .\q_reg[15] ({data_path0_n_12,data_path0_n_13,data_path0_n_14,data_path0_n_15}),
        .\q_reg[15]_0 ({control_path0_n_41,control_path0_n_42,control_path0_n_43}),
        .\q_reg[15]_1 ({control_path0_n_51,control_path0_n_52,control_path0_n_53}),
        .\q_reg[15]_2 (control_path0_n_31),
        .\q_reg[15]_3 (control_path0_n_35),
        .\q_reg[16] (data_path0_n_47),
        .\q_reg[17] (data_path0_n_46),
        .\q_reg[18] (data_path0_n_45),
        .\q_reg[18]_0 (data_path0_n_60),
        .\q_reg[18]_1 (data_path0_n_70),
        .\q_reg[19] ({data_path0_n_16,data_path0_n_17,data_path0_n_18,data_path0_n_19}),
        .\q_reg[19]_0 (data_path0_n_44),
        .\q_reg[19]_1 (data_path0_n_54),
        .\q_reg[19]_2 (data_path0_n_65),
        .\q_reg[19]_3 ({control_path0_n_44,control_path0_n_45}),
        .\q_reg[19]_4 ({control_path0_n_54,control_path0_n_55}),
        .\q_reg[19]_5 (control_path0_n_34),
        .\q_reg[19]_6 (control_path0_n_38),
        .\q_reg[20] (data_path0_n_51),
        .\q_reg[20]_0 (data_path0_n_61),
        .\q_reg[20]_1 (data_path0_n_63),
        .\q_reg[20]_2 (data_path0_n_71),
        .\q_reg[20]_3 (data_path0_n_73),
        .\q_reg[21] (data_path0_n_50),
        .\q_reg[21]_0 (data_path0_n_55),
        .\q_reg[21]_1 (data_path0_n_58),
        .\q_reg[21]_2 (data_path0_n_66),
        .\q_reg[21]_3 (data_path0_n_68),
        .\q_reg[22] (data_path0_n_49),
        .\q_reg[22]_0 (data_path0_n_59),
        .\q_reg[22]_1 (data_path0_n_62),
        .\q_reg[22]_2 (data_path0_n_69),
        .\q_reg[22]_3 (data_path0_n_72),
        .\q_reg[23] (data_path0_n_48),
        .\q_reg[23]_0 (data_path0_n_53),
        .\q_reg[23]_1 (data_path0_n_57),
        .\q_reg[23]_2 (data_path0_n_64),
        .\q_reg[23]_3 (data_path0_n_67),
        .\q_reg[23]_4 ({control_path0_n_46,control_path0_n_47,control_path0_n_48}),
        .\q_reg[23]_5 ({control_path0_n_56,control_path0_n_57,control_path0_n_58}),
        .\q_reg[23]_6 (control_path0_n_32),
        .\q_reg[23]_7 (control_path0_n_36),
        .\q_reg[23]_8 ({control_path0_n_5,control_path0_n_6,control_path0_n_7,control_path0_n_8,control_path0_n_9,control_path0_n_10,control_path0_n_11,control_path0_n_12,control_path0_n_13,control_path0_n_14,control_path0_n_15,control_path0_n_16,control_path0_n_17,control_path0_n_18,control_path0_n_19,control_path0_n_20,control_path0_n_21,control_path0_n_22,control_path0_n_23,control_path0_n_24,control_path0_n_25,control_path0_n_26,control_path0_n_27,control_path0_n_28}),
        .\q_reg[24] ({A[24:16],A[14:10]}),
        .\q_reg[24]_0 (data_path0_n_56),
        .\q_reg[3] ({data_path0_n_0,data_path0_n_1,data_path0_n_2,data_path0_n_3}),
        .\q_reg[7] ({data_path0_n_4,data_path0_n_5,data_path0_n_6,data_path0_n_7}),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_data_path
   (\q_reg[3] ,
    \q_reg[7] ,
    \q_reg[11] ,
    \q_reg[15] ,
    \q_reg[19] ,
    O,
    \q_reg[24] ,
    Q,
    \q_reg[11]_0 ,
    \q_reg[10] ,
    \q_reg[14] ,
    \q_reg[13] ,
    \q_reg[12] ,
    \q_reg[19]_0 ,
    \q_reg[18] ,
    \q_reg[17] ,
    \q_reg[16] ,
    \q_reg[23] ,
    \q_reg[22] ,
    \q_reg[21] ,
    \q_reg[20] ,
    axi_wready_reg,
    \q_reg[23]_0 ,
    \q_reg[19]_1 ,
    \q_reg[21]_0 ,
    \q_reg[24]_0 ,
    \q_reg[23]_1 ,
    \q_reg[21]_1 ,
    \q_reg[22]_0 ,
    \q_reg[18]_0 ,
    \q_reg[20]_0 ,
    \q_reg[22]_1 ,
    \q_reg[20]_1 ,
    \q_reg[23]_2 ,
    \q_reg[19]_2 ,
    \q_reg[21]_2 ,
    \q_reg[23]_3 ,
    \q_reg[21]_3 ,
    \q_reg[22]_2 ,
    \q_reg[18]_1 ,
    \q_reg[20]_2 ,
    \q_reg[22]_3 ,
    \q_reg[20]_3 ,
    D,
    s00_axi_aclk,
    addr,
    S,
    \q_reg[15]_0 ,
    \q_reg[19]_3 ,
    \q_reg[23]_4 ,
    \q_reg[11]_1 ,
    \q_reg[15]_1 ,
    \q_reg[19]_4 ,
    \q_reg[23]_5 ,
    \q_reg[0] ,
    \q_reg[0]_0 ,
    s00_axi_wvalid,
    s00_axi_awvalid,
    p_0_in,
    \axi_rdata_reg[31] ,
    axi_araddr,
    \axi_rdata_reg[31]_0 ,
    \q_reg[11]_2 ,
    \q_reg[15]_2 ,
    \q_reg[19]_5 ,
    \q_reg[23]_6 ,
    \q_reg[11]_3 ,
    \q_reg[15]_3 ,
    \q_reg[19]_6 ,
    \q_reg[23]_7 ,
    \q_reg[0]_1 ,
    en_reg,
    \q_reg[23]_8 );
  output [3:0]\q_reg[3] ;
  output [3:0]\q_reg[7] ;
  output [3:0]\q_reg[11] ;
  output [3:0]\q_reg[15] ;
  output [3:0]\q_reg[19] ;
  output [3:0]O;
  output [13:0]\q_reg[24] ;
  output [0:0]Q;
  output \q_reg[11]_0 ;
  output \q_reg[10] ;
  output \q_reg[14] ;
  output \q_reg[13] ;
  output \q_reg[12] ;
  output \q_reg[19]_0 ;
  output \q_reg[18] ;
  output \q_reg[17] ;
  output \q_reg[16] ;
  output \q_reg[23] ;
  output \q_reg[22] ;
  output \q_reg[21] ;
  output \q_reg[20] ;
  output axi_wready_reg;
  output \q_reg[23]_0 ;
  output \q_reg[19]_1 ;
  output \q_reg[21]_0 ;
  output \q_reg[24]_0 ;
  output \q_reg[23]_1 ;
  output \q_reg[21]_1 ;
  output \q_reg[22]_0 ;
  output \q_reg[18]_0 ;
  output \q_reg[20]_0 ;
  output \q_reg[22]_1 ;
  output \q_reg[20]_1 ;
  output \q_reg[23]_2 ;
  output \q_reg[19]_2 ;
  output \q_reg[21]_2 ;
  output \q_reg[23]_3 ;
  output \q_reg[21]_3 ;
  output \q_reg[22]_2 ;
  output \q_reg[18]_1 ;
  output \q_reg[20]_2 ;
  output \q_reg[22]_3 ;
  output \q_reg[20]_3 ;
  output [31:0]D;
  input s00_axi_aclk;
  input [4:0]addr;
  input [1:0]S;
  input [2:0]\q_reg[15]_0 ;
  input [1:0]\q_reg[19]_3 ;
  input [2:0]\q_reg[23]_4 ;
  input [1:0]\q_reg[11]_1 ;
  input [2:0]\q_reg[15]_1 ;
  input [1:0]\q_reg[19]_4 ;
  input [2:0]\q_reg[23]_5 ;
  input \q_reg[0] ;
  input \q_reg[0]_0 ;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [0:0]p_0_in;
  input [31:0]\axi_rdata_reg[31] ;
  input [1:0]axi_araddr;
  input [31:0]\axi_rdata_reg[31]_0 ;
  input \q_reg[11]_2 ;
  input \q_reg[15]_2 ;
  input \q_reg[19]_5 ;
  input \q_reg[23]_6 ;
  input \q_reg[11]_3 ;
  input \q_reg[15]_3 ;
  input \q_reg[19]_6 ;
  input \q_reg[23]_7 ;
  input \q_reg[0]_1 ;
  input en_reg;
  input [23:0]\q_reg[23]_8 ;

  wire [15:0]A;
  wire [31:0]D;
  wire Mux_X_n_0;
  wire Mux_X_n_1;
  wire Mux_X_n_10;
  wire Mux_X_n_11;
  wire Mux_X_n_12;
  wire Mux_X_n_13;
  wire Mux_X_n_14;
  wire Mux_X_n_15;
  wire Mux_X_n_16;
  wire Mux_X_n_17;
  wire Mux_X_n_18;
  wire Mux_X_n_19;
  wire Mux_X_n_2;
  wire Mux_X_n_20;
  wire Mux_X_n_21;
  wire Mux_X_n_22;
  wire Mux_X_n_23;
  wire Mux_X_n_24;
  wire Mux_X_n_3;
  wire Mux_X_n_4;
  wire Mux_X_n_5;
  wire Mux_X_n_6;
  wire Mux_X_n_7;
  wire Mux_X_n_8;
  wire Mux_X_n_9;
  wire Mux_Y_n_0;
  wire Mux_Y_n_1;
  wire Mux_Y_n_10;
  wire Mux_Y_n_11;
  wire Mux_Y_n_12;
  wire Mux_Y_n_13;
  wire Mux_Y_n_14;
  wire Mux_Y_n_15;
  wire Mux_Y_n_16;
  wire Mux_Y_n_17;
  wire Mux_Y_n_18;
  wire Mux_Y_n_19;
  wire Mux_Y_n_2;
  wire Mux_Y_n_20;
  wire Mux_Y_n_21;
  wire Mux_Y_n_22;
  wire Mux_Y_n_23;
  wire Mux_Y_n_24;
  wire Mux_Y_n_3;
  wire Mux_Y_n_4;
  wire Mux_Y_n_5;
  wire Mux_Y_n_6;
  wire Mux_Y_n_7;
  wire Mux_Y_n_8;
  wire Mux_Y_n_9;
  wire [3:0]O;
  wire [0:0]Q;
  wire ROM_1_n_23;
  wire [1:0]S;
  wire [4:0]addr;
  wire [1:0]axi_araddr;
  wire [31:0]\axi_rdata_reg[31] ;
  wire [31:0]\axi_rdata_reg[31]_0 ;
  wire axi_wready_reg;
  wire en_reg;
  wire [22:0]out_ROM;
  wire [0:0]p_0_in;
  wire \q_reg[0] ;
  wire \q_reg[0]_0 ;
  wire \q_reg[0]_1 ;
  wire \q_reg[10] ;
  wire [3:0]\q_reg[11] ;
  wire \q_reg[11]_0 ;
  wire [1:0]\q_reg[11]_1 ;
  wire \q_reg[11]_2 ;
  wire \q_reg[11]_3 ;
  wire \q_reg[12] ;
  wire \q_reg[13] ;
  wire \q_reg[14] ;
  wire [3:0]\q_reg[15] ;
  wire [2:0]\q_reg[15]_0 ;
  wire [2:0]\q_reg[15]_1 ;
  wire \q_reg[15]_2 ;
  wire \q_reg[15]_3 ;
  wire \q_reg[16] ;
  wire \q_reg[17] ;
  wire \q_reg[18] ;
  wire \q_reg[18]_0 ;
  wire \q_reg[18]_1 ;
  wire [3:0]\q_reg[19] ;
  wire \q_reg[19]_0 ;
  wire \q_reg[19]_1 ;
  wire \q_reg[19]_2 ;
  wire [1:0]\q_reg[19]_3 ;
  wire [1:0]\q_reg[19]_4 ;
  wire \q_reg[19]_5 ;
  wire \q_reg[19]_6 ;
  wire \q_reg[20] ;
  wire \q_reg[20]_0 ;
  wire \q_reg[20]_1 ;
  wire \q_reg[20]_2 ;
  wire \q_reg[20]_3 ;
  wire \q_reg[21] ;
  wire \q_reg[21]_0 ;
  wire \q_reg[21]_1 ;
  wire \q_reg[21]_2 ;
  wire \q_reg[21]_3 ;
  wire \q_reg[22] ;
  wire \q_reg[22]_0 ;
  wire \q_reg[22]_1 ;
  wire \q_reg[22]_2 ;
  wire \q_reg[22]_3 ;
  wire \q_reg[23] ;
  wire \q_reg[23]_0 ;
  wire \q_reg[23]_1 ;
  wire \q_reg[23]_2 ;
  wire \q_reg[23]_3 ;
  wire [2:0]\q_reg[23]_4 ;
  wire [2:0]\q_reg[23]_5 ;
  wire \q_reg[23]_6 ;
  wire \q_reg[23]_7 ;
  wire [23:0]\q_reg[23]_8 ;
  wire [13:0]\q_reg[24] ;
  wire \q_reg[24]_0 ;
  wire [3:0]\q_reg[3] ;
  wire [3:0]\q_reg[7] ;
  wire reg_X_n_0;
  wire reg_X_n_20;
  wire reg_X_n_33;
  wire reg_X_n_38;
  wire reg_X_n_39;
  wire reg_X_n_40;
  wire reg_X_n_41;
  wire reg_X_n_42;
  wire reg_X_n_43;
  wire reg_X_n_44;
  wire reg_X_n_45;
  wire reg_X_n_46;
  wire reg_X_n_47;
  wire reg_X_n_48;
  wire reg_X_n_49;
  wire reg_X_n_50;
  wire reg_X_n_51;
  wire reg_Y_n_0;
  wire reg_Y_n_10;
  wire reg_Y_n_23;
  wire reg_Y_n_28;
  wire reg_Y_n_29;
  wire reg_Y_n_30;
  wire reg_Y_n_31;
  wire reg_Y_n_32;
  wire reg_Y_n_33;
  wire reg_Y_n_34;
  wire reg_Y_n_35;
  wire reg_Y_n_36;
  wire reg_Y_n_37;
  wire reg_Y_n_38;
  wire reg_Y_n_39;
  wire reg_Y_n_40;
  wire reg_Y_n_41;
  wire reg_Y_n_42;
  wire reg_Y_n_43;
  wire reg_Y_n_44;
  wire reg_Y_n_45;
  wire reg_Y_n_46;
  wire reg_Y_n_47;
  wire reg_Y_n_48;
  wire reg_Y_n_49;
  wire reg_Y_n_50;
  wire reg_Y_n_51;
  wire reg_Y_n_52;
  wire reg_Y_n_9;
  wire reg_Z_n_10;
  wire reg_Z_n_11;
  wire reg_Z_n_12;
  wire reg_Z_n_13;
  wire reg_Z_n_14;
  wire reg_Z_n_15;
  wire reg_Z_n_16;
  wire reg_Z_n_17;
  wire reg_Z_n_18;
  wire reg_Z_n_19;
  wire reg_Z_n_20;
  wire reg_Z_n_21;
  wire reg_Z_n_22;
  wire reg_Z_n_23;
  wire reg_Z_n_24;
  wire reg_Z_n_25;
  wire reg_Z_n_26;
  wire reg_Z_n_27;
  wire reg_Z_n_28;
  wire reg_Z_n_29;
  wire reg_Z_n_3;
  wire reg_Z_n_30;
  wire reg_Z_n_31;
  wire reg_Z_n_32;
  wire reg_Z_n_33;
  wire reg_Z_n_34;
  wire reg_Z_n_35;
  wire reg_Z_n_36;
  wire reg_Z_n_37;
  wire reg_Z_n_38;
  wire reg_Z_n_39;
  wire reg_Z_n_4;
  wire reg_Z_n_40;
  wire reg_Z_n_41;
  wire reg_Z_n_42;
  wire reg_Z_n_43;
  wire reg_Z_n_44;
  wire reg_Z_n_45;
  wire reg_Z_n_46;
  wire reg_Z_n_47;
  wire reg_Z_n_48;
  wire reg_Z_n_49;
  wire reg_Z_n_5;
  wire reg_Z_n_6;
  wire reg_Z_n_7;
  wire reg_Z_n_8;
  wire reg_Z_n_9;
  wire s00_axi_aclk;
  wire s00_axi_awvalid;
  wire s00_axi_wvalid;
  wire sel0_out;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mux_2_1 Mux_X
       (.A({A[15],A[9:0]}),
        .D({D[31:23],D[20:16],D[14:10],D[7:6],D[4:0]}),
        .DI(\q_reg[24] [1:0]),
        .O({Mux_X_n_0,Mux_X_n_1,Mux_X_n_2,Mux_X_n_3}),
        .S({reg_Y_n_39,reg_Y_n_40,reg_Y_n_41,reg_Y_n_42}),
        .axi_araddr(axi_araddr),
        .\axi_rdata_reg[11] ({Mux_Y_n_8,Mux_Y_n_9}),
        .\axi_rdata_reg[14] ({Mux_Y_n_13,Mux_Y_n_14,Mux_Y_n_15}),
        .\axi_rdata_reg[19] ({Mux_Y_n_16,Mux_Y_n_17,Mux_Y_n_18,Mux_Y_n_19}),
        .\axi_rdata_reg[23] ({Mux_Y_n_20,Mux_Y_n_23}),
        .\axi_rdata_reg[24] (Mux_Y_n_24),
        .\axi_rdata_reg[31] ({\axi_rdata_reg[31] [31:23],\axi_rdata_reg[31] [20:16],\axi_rdata_reg[31] [14:10],\axi_rdata_reg[31] [7:6],\axi_rdata_reg[31] [4:0]}),
        .\axi_rdata_reg[31]_0 ({\axi_rdata_reg[31]_0 [31:23],\axi_rdata_reg[31]_0 [20:16],\axi_rdata_reg[31]_0 [14:10],\axi_rdata_reg[31]_0 [7:6],\axi_rdata_reg[31]_0 [4:0]}),
        .\axi_rdata_reg[3] ({Mux_Y_n_0,Mux_Y_n_1,Mux_Y_n_2,Mux_Y_n_3}),
        .\axi_rdata_reg[7] ({Mux_Y_n_4,Mux_Y_n_5,Mux_Y_n_7}),
        .\q_reg[11] ({Mux_X_n_8,Mux_X_n_9,Mux_X_n_10,Mux_X_n_11}),
        .\q_reg[11]_0 ({S,reg_X_n_38,reg_Y_n_47}),
        .\q_reg[15] ({Mux_X_n_12,Mux_X_n_13,Mux_X_n_14,Mux_X_n_15}),
        .\q_reg[15]_0 (\q_reg[24] [4:2]),
        .\q_reg[15]_1 ({reg_X_n_39,\q_reg[15]_0 }),
        .\q_reg[19] ({Mux_X_n_16,Mux_X_n_17,Mux_X_n_18,Mux_X_n_19}),
        .\q_reg[19]_0 (\q_reg[24] [8:5]),
        .\q_reg[19]_1 ({\q_reg[19]_3 ,reg_X_n_40,reg_Y_n_48}),
        .\q_reg[23] ({Mux_X_n_20,Mux_X_n_21,Mux_X_n_22,Mux_X_n_23}),
        .\q_reg[23]_0 (Mux_X_n_24),
        .\q_reg[23]_1 (\q_reg[24] [12:9]),
        .\q_reg[23]_2 ({reg_X_n_41,\q_reg[23]_4 }),
        .\q_reg[24] (reg_Y_n_38),
        .\q_reg[7] ({Mux_X_n_4,Mux_X_n_5,Mux_X_n_6,Mux_X_n_7}),
        .\q_reg[7]_0 ({reg_Y_n_43,reg_Y_n_44,reg_Y_n_45,reg_Y_n_46}),
        .sel0_out(sel0_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mux_2_1_0 Mux_Y
       (.D({D[22:21],D[15],D[9:8],D[5]}),
        .DI({reg_Y_n_34,reg_Y_n_35,reg_Y_n_36,reg_Y_n_37}),
        .O({Mux_Y_n_4,Mux_Y_n_5,Mux_Y_n_6,Mux_Y_n_7}),
        .Q(Q),
        .S({reg_X_n_42,reg_X_n_43,reg_X_n_44,reg_X_n_45}),
        .axi_araddr(axi_araddr),
        .\axi_rdata_reg[15] (Mux_X_n_12),
        .\axi_rdata_reg[22] ({\axi_rdata_reg[31]_0 [22:21],\axi_rdata_reg[31]_0 [15],\axi_rdata_reg[31]_0 [9:8],\axi_rdata_reg[31]_0 [5]}),
        .\axi_rdata_reg[22]_0 ({\axi_rdata_reg[31] [22:21],\axi_rdata_reg[31] [15],\axi_rdata_reg[31] [9:8],\axi_rdata_reg[31] [5]}),
        .\axi_rdata_reg[22]_1 ({Mux_X_n_21,Mux_X_n_22}),
        .\axi_rdata_reg[5] (Mux_X_n_6),
        .\axi_rdata_reg[9] ({Mux_X_n_10,Mux_X_n_11}),
        .\q_reg[11] ({Mux_Y_n_8,Mux_Y_n_9,Mux_Y_n_10,Mux_Y_n_11}),
        .\q_reg[11]_0 ({\q_reg[11]_0 ,\q_reg[10] ,reg_Y_n_28,reg_Y_n_29}),
        .\q_reg[11]_1 ({\q_reg[11]_1 ,reg_Y_n_49,reg_X_n_50}),
        .\q_reg[15] ({Mux_Y_n_12,Mux_Y_n_13,Mux_Y_n_14,Mux_Y_n_15}),
        .\q_reg[15]_0 (reg_Y_n_9),
        .\q_reg[15]_1 ({\q_reg[14] ,\q_reg[13] ,\q_reg[12] }),
        .\q_reg[15]_2 ({reg_Y_n_50,\q_reg[15]_1 }),
        .\q_reg[19] ({Mux_Y_n_16,Mux_Y_n_17,Mux_Y_n_18,Mux_Y_n_19}),
        .\q_reg[19]_0 ({\q_reg[19]_0 ,\q_reg[18] ,\q_reg[17] ,\q_reg[16] }),
        .\q_reg[19]_1 ({\q_reg[19]_4 ,reg_Y_n_51,reg_X_n_51}),
        .\q_reg[23] ({Mux_Y_n_0,Mux_Y_n_1,Mux_Y_n_2,Mux_Y_n_3}),
        .\q_reg[23]_0 ({Mux_Y_n_20,Mux_Y_n_21,Mux_Y_n_22,Mux_Y_n_23}),
        .\q_reg[23]_1 (Mux_Y_n_24),
        .\q_reg[23]_2 ({\q_reg[23] ,\q_reg[22] ,\q_reg[21] ,\q_reg[20] }),
        .\q_reg[23]_3 ({reg_Y_n_52,\q_reg[23]_5 }),
        .\q_reg[24] (reg_Z_n_26),
        .\q_reg[7] ({reg_Y_n_30,reg_Y_n_31,reg_Y_n_32,reg_Y_n_33}),
        .\q_reg[7]_0 ({reg_X_n_46,reg_X_n_47,reg_X_n_48,reg_X_n_49}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_Mux_2_1__parameterized2 Mux_Z
       (.O(O),
        .Q({reg_Z_n_3,reg_Z_n_4,reg_Z_n_5,reg_Z_n_6,reg_Z_n_7,reg_Z_n_8,reg_Z_n_9,reg_Z_n_10,reg_Z_n_11,reg_Z_n_12,reg_Z_n_13,reg_Z_n_14,reg_Z_n_15,reg_Z_n_16,reg_Z_n_17,reg_Z_n_18,reg_Z_n_19,reg_Z_n_20,reg_Z_n_21,reg_Z_n_22,reg_Z_n_23,reg_Z_n_24,reg_Z_n_25}),
        .S({reg_Z_n_27,reg_Z_n_28,reg_Z_n_29,reg_Z_n_30}),
        .\q_reg[11] (\q_reg[11] ),
        .\q_reg[11]_0 ({reg_Z_n_35,reg_Z_n_36,reg_Z_n_37,reg_Z_n_38}),
        .\q_reg[15] (\q_reg[15] ),
        .\q_reg[15]_0 ({reg_Z_n_39,reg_Z_n_40,reg_Z_n_41,reg_Z_n_42}),
        .\q_reg[19] (\q_reg[19] ),
        .\q_reg[19]_0 ({reg_Z_n_43,reg_Z_n_44,reg_Z_n_45,reg_Z_n_46}),
        .\q_reg[23] ({ROM_1_n_23,reg_Z_n_47,reg_Z_n_48,reg_Z_n_49}),
        .\q_reg[3] (\q_reg[3] ),
        .\q_reg[7] (\q_reg[7] ),
        .\q_reg[7]_0 ({reg_Z_n_31,reg_Z_n_32,reg_Z_n_33,reg_Z_n_34}),
        .sel0_out(sel0_out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ROM ROM_1
       (.addr(addr),
        .data_reg_0(ROM_1_n_23),
        .out_ROM(out_ROM),
        .s00_axi_aclk(s00_axi_aclk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg__parameterized1 reg_X
       (.A({A[15],A[9:0]}),
        .DI(\q_reg[24] [1:0]),
        .O({Mux_X_n_0,Mux_X_n_1,Mux_X_n_2,Mux_X_n_3}),
        .Q(Q),
        .S({reg_X_n_42,reg_X_n_43,reg_X_n_44,reg_X_n_45}),
        .addr(addr),
        .en_reg(en_reg),
        .\q_reg[11]_0 (\q_reg[11]_2 ),
        .\q_reg[11]_1 (reg_Y_n_0),
        .\q_reg[11]_2 (reg_Y_n_29),
        .\q_reg[11]_3 ({Mux_X_n_8,Mux_X_n_9,Mux_X_n_10,Mux_X_n_11}),
        .\q_reg[14]_0 (\q_reg[24] [4:2]),
        .\q_reg[15]_0 (reg_X_n_39),
        .\q_reg[15]_1 (reg_Y_n_23),
        .\q_reg[15]_2 (\q_reg[15]_2 ),
        .\q_reg[15]_3 ({Mux_X_n_12,Mux_X_n_13,Mux_X_n_14,Mux_X_n_15}),
        .\q_reg[16]_0 (reg_X_n_51),
        .\q_reg[17]_0 (reg_X_n_40),
        .\q_reg[18]_0 (\q_reg[18]_1 ),
        .\q_reg[19]_0 (\q_reg[19]_2 ),
        .\q_reg[19]_1 (\q_reg[24] [8:5]),
        .\q_reg[19]_2 (\q_reg[19]_5 ),
        .\q_reg[19]_3 (reg_Y_n_10),
        .\q_reg[19]_4 (\q_reg[16] ),
        .\q_reg[19]_5 ({Mux_X_n_16,Mux_X_n_17,Mux_X_n_18,Mux_X_n_19}),
        .\q_reg[1]_0 (reg_X_n_0),
        .\q_reg[1]_1 (reg_X_n_20),
        .\q_reg[1]_2 (reg_X_n_33),
        .\q_reg[20]_0 (\q_reg[20]_2 ),
        .\q_reg[20]_1 (\q_reg[20]_3 ),
        .\q_reg[21]_0 (\q_reg[21]_2 ),
        .\q_reg[21]_1 (\q_reg[21]_3 ),
        .\q_reg[22]_0 (\q_reg[22]_2 ),
        .\q_reg[22]_1 (\q_reg[22]_3 ),
        .\q_reg[22]_2 (\q_reg[0]_1 ),
        .\q_reg[23]_0 (\q_reg[23]_2 ),
        .\q_reg[23]_1 (\q_reg[24] [12:9]),
        .\q_reg[23]_2 (\q_reg[23]_3 ),
        .\q_reg[23]_3 (reg_X_n_41),
        .\q_reg[23]_4 (\q_reg[23]_6 ),
        .\q_reg[23]_5 (\q_reg[24]_0 ),
        .\q_reg[23]_6 ({Mux_X_n_20,Mux_X_n_21,Mux_X_n_22,Mux_X_n_23}),
        .\q_reg[24]_0 (\q_reg[24] [13]),
        .\q_reg[24]_1 (Mux_X_n_24),
        .\q_reg[3]_0 ({reg_Y_n_34,reg_Y_n_35,reg_Y_n_36,reg_Y_n_37}),
        .\q_reg[7]_0 ({reg_X_n_46,reg_X_n_47,reg_X_n_48,reg_X_n_49}),
        .\q_reg[7]_1 ({reg_Y_n_30,reg_Y_n_31,reg_Y_n_32,reg_Y_n_33}),
        .\q_reg[7]_2 ({Mux_X_n_4,Mux_X_n_5,Mux_X_n_6,Mux_X_n_7}),
        .\q_reg[8]_0 (reg_X_n_50),
        .\q_reg[9]_0 (reg_X_n_38),
        .s00_axi_aclk(s00_axi_aclk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg__parameterized1_1 reg_Y
       (.A(A[8:0]),
        .O({Mux_Y_n_4,Mux_Y_n_5,Mux_Y_n_6,Mux_Y_n_7}),
        .Q(Q),
        .S({reg_Y_n_39,reg_Y_n_40,reg_Y_n_41,reg_Y_n_42}),
        .addr(addr),
        .en_reg(en_reg),
        .\q_reg[0]_0 (\q_reg[0]_1 ),
        .\q_reg[11]_0 ({\q_reg[11]_0 ,\q_reg[10] ,reg_Y_n_28,reg_Y_n_29}),
        .\q_reg[11]_1 (\q_reg[11]_3 ),
        .\q_reg[11]_2 (reg_X_n_0),
        .\q_reg[11]_3 ({Mux_Y_n_8,Mux_Y_n_9,Mux_Y_n_10,Mux_Y_n_11}),
        .\q_reg[14]_0 ({\q_reg[14] ,\q_reg[13] ,\q_reg[12] }),
        .\q_reg[15]_0 (reg_Y_n_9),
        .\q_reg[15]_1 (reg_Y_n_50),
        .\q_reg[15]_2 (reg_X_n_33),
        .\q_reg[15]_3 (\q_reg[15]_3 ),
        .\q_reg[15]_4 ({Mux_Y_n_12,Mux_Y_n_13,Mux_Y_n_14,Mux_Y_n_15}),
        .\q_reg[16]_0 (reg_Y_n_48),
        .\q_reg[17]_0 (reg_Y_n_51),
        .\q_reg[18]_0 (\q_reg[18]_0 ),
        .\q_reg[19]_0 (\q_reg[19]_1 ),
        .\q_reg[19]_1 ({\q_reg[19]_0 ,\q_reg[18] ,\q_reg[17] ,\q_reg[16] }),
        .\q_reg[19]_2 (\q_reg[24] [5]),
        .\q_reg[19]_3 (\q_reg[19]_6 ),
        .\q_reg[19]_4 (reg_X_n_20),
        .\q_reg[19]_5 ({Mux_Y_n_16,Mux_Y_n_17,Mux_Y_n_18,Mux_Y_n_19}),
        .\q_reg[1]_0 (reg_Y_n_0),
        .\q_reg[1]_1 (reg_Y_n_10),
        .\q_reg[1]_2 (reg_Y_n_23),
        .\q_reg[20]_0 (\q_reg[20]_0 ),
        .\q_reg[20]_1 (\q_reg[20]_1 ),
        .\q_reg[21]_0 (\q_reg[21]_0 ),
        .\q_reg[21]_1 (\q_reg[21]_1 ),
        .\q_reg[22]_0 (\q_reg[22]_0 ),
        .\q_reg[22]_1 (\q_reg[22]_1 ),
        .\q_reg[23]_0 (\q_reg[23]_0 ),
        .\q_reg[23]_1 ({\q_reg[23] ,\q_reg[22] ,\q_reg[21] ,\q_reg[20] }),
        .\q_reg[23]_2 (\q_reg[23]_1 ),
        .\q_reg[23]_3 (reg_Y_n_52),
        .\q_reg[23]_4 (\q_reg[23]_7 ),
        .\q_reg[23]_5 ({Mux_Y_n_20,Mux_Y_n_21,Mux_Y_n_22,Mux_Y_n_23}),
        .\q_reg[24]_0 (\q_reg[24]_0 ),
        .\q_reg[24]_1 (reg_Y_n_38),
        .\q_reg[24]_2 (\q_reg[24] [13]),
        .\q_reg[24]_3 (Mux_Y_n_24),
        .\q_reg[3]_0 ({reg_Y_n_34,reg_Y_n_35,reg_Y_n_36,reg_Y_n_37}),
        .\q_reg[3]_1 ({Mux_Y_n_0,Mux_Y_n_1,Mux_Y_n_2,Mux_Y_n_3}),
        .\q_reg[7]_0 ({reg_Y_n_30,reg_Y_n_31,reg_Y_n_32,reg_Y_n_33}),
        .\q_reg[7]_1 ({reg_Y_n_43,reg_Y_n_44,reg_Y_n_45,reg_Y_n_46}),
        .\q_reg[8]_0 (reg_Y_n_47),
        .\q_reg[9]_0 (reg_Y_n_49),
        .s00_axi_aclk(s00_axi_aclk));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg__parameterized3 reg_Z
       (.Q({Q,reg_Z_n_3,reg_Z_n_4,reg_Z_n_5,reg_Z_n_6,reg_Z_n_7,reg_Z_n_8,reg_Z_n_9,reg_Z_n_10,reg_Z_n_11,reg_Z_n_12,reg_Z_n_13,reg_Z_n_14,reg_Z_n_15,reg_Z_n_16,reg_Z_n_17,reg_Z_n_18,reg_Z_n_19,reg_Z_n_20,reg_Z_n_21,reg_Z_n_22,reg_Z_n_23,reg_Z_n_24,reg_Z_n_25}),
        .S({reg_Z_n_27,reg_Z_n_28,reg_Z_n_29,reg_Z_n_30}),
        .axi_wready_reg(axi_wready_reg),
        .en_reg(en_reg),
        .out_ROM(out_ROM),
        .p_0_in(p_0_in),
        .\q_reg[0]_0 (\q_reg[0] ),
        .\q_reg[0]_1 (\q_reg[0]_0 ),
        .\q_reg[11]_0 ({reg_Z_n_35,reg_Z_n_36,reg_Z_n_37,reg_Z_n_38}),
        .\q_reg[15]_0 ({reg_Z_n_39,reg_Z_n_40,reg_Z_n_41,reg_Z_n_42}),
        .\q_reg[19]_0 ({reg_Z_n_43,reg_Z_n_44,reg_Z_n_45,reg_Z_n_46}),
        .\q_reg[22]_0 ({reg_Z_n_47,reg_Z_n_48,reg_Z_n_49}),
        .\q_reg[23]_0 (reg_Z_n_26),
        .\q_reg[23]_1 (\q_reg[23]_8 ),
        .\q_reg[24] (\q_reg[24] [13]),
        .\q_reg[24]_0 (\q_reg[24]_0 ),
        .\q_reg[7]_0 ({reg_Z_n_31,reg_Z_n_32,reg_Z_n_33,reg_Z_n_34}),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_wvalid(s00_axi_wvalid),
        .sel0_out(sel0_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_fsm
   (D,
    \FSM_sequential_state_reg[0]_0 ,
    \FSM_sequential_state_reg[0]_1 ,
    \q_reg[0] ,
    p_0_in,
    \FSM_sequential_state_reg[0]_2 ,
    s00_axi_wdata,
    O,
    \q_reg[19] ,
    \q_reg[15] ,
    \q_reg[11] ,
    \q_reg[7] ,
    \q_reg[3] ,
    s00_axi_aclk);
  output [23:0]D;
  output \FSM_sequential_state_reg[0]_0 ;
  output \FSM_sequential_state_reg[0]_1 ;
  input \q_reg[0] ;
  input [0:0]p_0_in;
  input \FSM_sequential_state_reg[0]_2 ;
  input [23:0]s00_axi_wdata;
  input [3:0]O;
  input [3:0]\q_reg[19] ;
  input [3:0]\q_reg[15] ;
  input [3:0]\q_reg[11] ;
  input [3:0]\q_reg[7] ;
  input [3:0]\q_reg[3] ;
  input s00_axi_aclk;

  wire [23:0]D;
  wire \FSM_sequential_state_reg[0]_0 ;
  wire \FSM_sequential_state_reg[0]_1 ;
  wire \FSM_sequential_state_reg[0]_2 ;
  wire [3:0]O;
  wire [0:0]p_0_in;
  wire \q_reg[0] ;
  wire [3:0]\q_reg[11] ;
  wire [3:0]\q_reg[15] ;
  wire [3:0]\q_reg[19] ;
  wire [3:0]\q_reg[3] ;
  wire [3:0]\q_reg[7] ;
  wire s00_axi_aclk;
  wire [23:0]s00_axi_wdata;
  wire [1:0]state;
  wire [1:0]state__0;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h55040004)) 
    \FSM_sequential_state[0]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(\FSM_sequential_state_reg[0]_2 ),
        .O(state__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_state[1]_i_1 
       (.I0(state[0]),
        .I1(state[1]),
        .O(state__0[1]));
  (* FSM_ENCODED_STATES = "counter_state:10,start_state:00,compute_state:01" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(state__0[0]),
        .Q(state[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "counter_state:10,start_state:00,compute_state:01" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(state__0[1]),
        .Q(state[1]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[0]_i_1__0 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[0]),
        .I5(\q_reg[3] [0]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[10]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[10]),
        .I5(\q_reg[11] [2]),
        .O(D[10]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[11]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[11]),
        .I5(\q_reg[11] [3]),
        .O(D[11]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[12]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[12]),
        .I5(\q_reg[15] [0]),
        .O(D[12]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[13]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[13]),
        .I5(\q_reg[15] [1]),
        .O(D[13]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[14]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[14]),
        .I5(\q_reg[15] [2]),
        .O(D[14]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[15]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[15]),
        .I5(\q_reg[15] [3]),
        .O(D[15]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[16]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[16]),
        .I5(\q_reg[19] [0]),
        .O(D[16]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[17]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[17]),
        .I5(\q_reg[19] [1]),
        .O(D[17]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[18]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[18]),
        .I5(\q_reg[19] [2]),
        .O(D[18]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[19]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[19]),
        .I5(\q_reg[19] [3]),
        .O(D[19]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[1]_i_1__0 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[1]),
        .I5(\q_reg[3] [1]),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[20]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[20]),
        .I5(O[0]),
        .O(D[20]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[21]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[21]),
        .I5(O[1]),
        .O(D[21]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[22]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[22]),
        .I5(O[2]),
        .O(D[22]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[23]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[23]),
        .I5(O[3]),
        .O(D[23]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[2]_i_1__0 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[2]),
        .I5(\q_reg[3] [2]),
        .O(D[2]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[3]_i_1__0 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[3]),
        .I5(\q_reg[3] [3]),
        .O(D[3]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[4]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[4]),
        .I5(\q_reg[7] [0]),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0004)) 
    \q[4]_i_1__0 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .O(\FSM_sequential_state_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h00AE)) 
    \q[4]_i_2 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .O(\FSM_sequential_state_reg[0]_1 ));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[5]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[5]),
        .I5(\q_reg[7] [1]),
        .O(D[5]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[6]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[6]),
        .I5(\q_reg[7] [2]),
        .O(D[6]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[7]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[7]),
        .I5(\q_reg[7] [3]),
        .O(D[7]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[8]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[8]),
        .I5(\q_reg[11] [0]),
        .O(D[8]));
  LUT6 #(
    .INIT(64'hFFFFFFFB00040000)) 
    \q[9]_i_1 
       (.I0(state[0]),
        .I1(\q_reg[0] ),
        .I2(p_0_in),
        .I3(state[1]),
        .I4(s00_axi_wdata[9]),
        .I5(\q_reg[11] [1]),
        .O(D[9]));
endmodule

(* CHECK_LICENSE_TYPE = "project_audio_2_design_project_audio_2_ip_0_0,project_audio_2_ip_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "project_audio_2_ip_v1_0,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN project_audio_2_design_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [3:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [3:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN project_audio_2_design_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_project_audio_2_ip_v1_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[3:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[3:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_project_audio_2_ip_v1_0
   (S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input s00_axi_aclk;
  input [1:0]s00_axi_awaddr;
  input [1:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_project_audio_2_ip_v1_0_S00_AXI project_audio_2_ip_v1_0_S00_AXI_inst
       (.S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_project_audio_2_ip_v1_0_S00_AXI
   (S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input s00_axi_aclk;
  input [1:0]s00_axi_awaddr;
  input [1:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire [3:2]axi_araddr;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire axi_arready0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire cordic_rotation0_n_0;
  wire [1:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [31:0]slv_reg2;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[31]_i_2_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire [31:0]slv_reg3;
  wire slv_reg_rden;

  LUT6 #(
    .INIT(64'hFFFF88880FFF8888)) 
    aw_en_i_1
       (.I0(s00_axi_bready),
        .I1(s00_axi_bvalid),
        .I2(s00_axi_awvalid),
        .I3(s00_axi_wvalid),
        .I4(aw_en_reg_n_0),
        .I5(S_AXI_AWREADY),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[2]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[3]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(axi_araddr[2]),
        .S(axi_awready_i_1_n_0));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(axi_araddr[3]),
        .S(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hEFFFFFFF20000000)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(S_AXI_AWREADY),
        .I2(aw_en_reg_n_0),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_awvalid),
        .I5(p_0_in[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEFFFFFFF20000000)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(S_AXI_AWREADY),
        .I2(aw_en_reg_n_0),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_awvalid),
        .I5(p_0_in[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_0_in[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_0_in[1]),
        .R(axi_awready_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    axi_awready_i_2
       (.I0(S_AXI_AWREADY),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(S_AXI_AWREADY),
        .I3(S_AXI_WREADY),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(axi_awready_i_1_n_0));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    axi_wready_i_1
       (.I0(S_AXI_WREADY),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(axi_awready_i_1_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_cordic_rotation cordic_rotation0
       (.D(reg_data_out),
        .Q(slv_reg3),
        .axi_araddr(axi_araddr),
        .\axi_rdata_reg[31] (slv_reg2),
        .axi_wready_reg(cordic_rotation0_n_0),
        .p_0_in(p_0_in),
        .\q_reg[0] (S_AXI_WREADY),
        .\q_reg[0]_0 (S_AXI_AWREADY),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_wdata(s00_axi_wdata[23:0]),
        .s00_axi_wvalid(s00_axi_wvalid));
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg2[15]_i_1 
       (.I0(s00_axi_wstrb[1]),
        .I1(p_0_in[1]),
        .I2(\slv_reg2[31]_i_2_n_0 ),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg2[23]_i_1 
       (.I0(s00_axi_wstrb[2]),
        .I1(p_0_in[1]),
        .I2(\slv_reg2[31]_i_2_n_0 ),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg2[31]_i_1 
       (.I0(s00_axi_wstrb[3]),
        .I1(p_0_in[1]),
        .I2(\slv_reg2[31]_i_2_n_0 ),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hBFFFFFFF)) 
    \slv_reg2[31]_i_2 
       (.I0(p_0_in[0]),
        .I1(S_AXI_WREADY),
        .I2(S_AXI_AWREADY),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_awvalid),
        .O(\slv_reg2[31]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \slv_reg2[7]_i_1 
       (.I0(s00_axi_wstrb[0]),
        .I1(p_0_in[1]),
        .I2(\slv_reg2[31]_i_2_n_0 ),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg2[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(axi_awready_i_1_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    \slv_reg3[15]_i_1 
       (.I0(cordic_rotation0_n_0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[1]),
        .O(p_1_in[15]));
  LUT3 #(
    .INIT(8'h80)) 
    \slv_reg3[23]_i_1 
       (.I0(cordic_rotation0_n_0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[1]),
        .O(p_1_in[23]));
  LUT3 #(
    .INIT(8'h80)) 
    \slv_reg3[31]_i_1 
       (.I0(cordic_rotation0_n_0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[1]),
        .O(p_1_in[31]));
  LUT3 #(
    .INIT(8'h80)) 
    \slv_reg3[7]_i_1 
       (.I0(cordic_rotation0_n_0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[1]),
        .O(p_1_in[7]));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg3[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg3[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg3[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg3[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg3[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg3[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg3[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg3[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg3[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg3[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg3[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg3[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg3[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg3[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg3[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg3[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg3[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg3[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg3[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg3[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg3[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg3[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg3[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg3[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg3[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg3[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg3[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg3[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg3[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg3[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg3[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg3[9]),
        .R(axi_awready_i_1_n_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg
   (\q_reg[4]_0 ,
    \q_reg[4]_1 ,
    \q_reg[2]_0 ,
    \q_reg[3]_0 ,
    \q_reg[1]_0 ,
    \q_reg[0]_0 ,
    \q_reg[1]_1 ,
    \q_reg[2]_1 ,
    \q_reg[1]_2 ,
    \q_reg[1]_3 ,
    \q_reg[1]_4 ,
    \q_reg[2]_2 ,
    \q_reg[1]_5 ,
    \q_reg[1]_6 ,
    S,
    \q_reg[14] ,
    \q_reg[19] ,
    \q_reg[22] ,
    \q_reg[11] ,
    \q_reg[14]_0 ,
    \q_reg[19]_0 ,
    \q_reg[22]_0 ,
    y0_carry__1_i_2__0_0,
    y0_carry__1_i_2__0_1,
    y0_carry__1_i_2__0_2,
    y0_carry__2_i_1__0,
    y0_carry__3_i_2__0_0,
    y0_carry__4_i_2__0_0,
    y0_carry__4_i_2__0_1,
    y0_carry__1_i_3__0,
    y0_carry__1_i_3__0_0,
    y0_carry__1_i_3__0_1,
    y0_carry__2_i_3__0_0,
    y0_carry__3_i_3__0,
    \q_reg[23] ,
    \q_reg[23]_0 ,
    \q_reg[23]_1 ,
    \q_reg[19]_1 ,
    \q_reg[19]_2 ,
    y0_carry__1_i_6_0,
    y0_carry__1_i_7_0,
    y0_carry__1_i_2__1_0,
    y0_carry__1_i_2__1_1,
    y0_carry__1_i_2__1_2,
    y0_carry__2_i_1__1,
    y0_carry__3_i_2__1_0,
    A,
    y0_carry__1_i_3__1,
    y0_carry__1_i_3__1_0,
    y0_carry__1_i_3__1_1,
    y0_carry__2_i_3__1_0,
    y0_carry__3_i_3__1,
    Q,
    \q_reg[11]_0 ,
    \q_reg[11]_1 ,
    \q_reg[15] ,
    \q_reg[15]_0 ,
    \q_reg[15]_1 ,
    \q_reg[0]_1 ,
    \q_reg[0]_2 ,
    s00_axi_aclk);
  output \q_reg[4]_0 ;
  output \q_reg[4]_1 ;
  output \q_reg[2]_0 ;
  output \q_reg[3]_0 ;
  output \q_reg[1]_0 ;
  output \q_reg[0]_0 ;
  output \q_reg[1]_1 ;
  output \q_reg[2]_1 ;
  output \q_reg[1]_2 ;
  output \q_reg[1]_3 ;
  output \q_reg[1]_4 ;
  output \q_reg[2]_2 ;
  output \q_reg[1]_5 ;
  output \q_reg[1]_6 ;
  output [1:0]S;
  output [2:0]\q_reg[14] ;
  output [1:0]\q_reg[19] ;
  output [2:0]\q_reg[22] ;
  output [1:0]\q_reg[11] ;
  output [2:0]\q_reg[14]_0 ;
  output [1:0]\q_reg[19]_0 ;
  output [2:0]\q_reg[22]_0 ;
  input y0_carry__1_i_2__0_0;
  input y0_carry__1_i_2__0_1;
  input y0_carry__1_i_2__0_2;
  input y0_carry__2_i_1__0;
  input y0_carry__3_i_2__0_0;
  input y0_carry__4_i_2__0_0;
  input y0_carry__4_i_2__0_1;
  input y0_carry__1_i_3__0;
  input y0_carry__1_i_3__0_0;
  input y0_carry__1_i_3__0_1;
  input y0_carry__2_i_3__0_0;
  input y0_carry__3_i_3__0;
  input \q_reg[23] ;
  input \q_reg[23]_0 ;
  input \q_reg[23]_1 ;
  input \q_reg[19]_1 ;
  input \q_reg[19]_2 ;
  input y0_carry__1_i_6_0;
  input y0_carry__1_i_7_0;
  input y0_carry__1_i_2__1_0;
  input y0_carry__1_i_2__1_1;
  input y0_carry__1_i_2__1_2;
  input y0_carry__2_i_1__1;
  input y0_carry__3_i_2__1_0;
  input [13:0]A;
  input y0_carry__1_i_3__1;
  input y0_carry__1_i_3__1_0;
  input y0_carry__1_i_3__1_1;
  input y0_carry__2_i_3__1_0;
  input y0_carry__3_i_3__1;
  input [0:0]Q;
  input \q_reg[11]_0 ;
  input \q_reg[11]_1 ;
  input \q_reg[15] ;
  input \q_reg[15]_0 ;
  input \q_reg[15]_1 ;
  input \q_reg[0]_1 ;
  input \q_reg[0]_2 ;
  input s00_axi_aclk;

  wire [13:0]A;
  wire [0:0]Q;
  wire [1:0]S;
  wire \q[0]_i_1_n_0 ;
  wire \q[1]_i_1_n_0 ;
  wire \q[2]_i_1_n_0 ;
  wire \q[3]_i_1_n_0 ;
  wire \q[4]_i_3_n_0 ;
  wire \q_reg[0]_0 ;
  wire \q_reg[0]_1 ;
  wire \q_reg[0]_2 ;
  wire [1:0]\q_reg[11] ;
  wire \q_reg[11]_0 ;
  wire \q_reg[11]_1 ;
  wire [2:0]\q_reg[14] ;
  wire [2:0]\q_reg[14]_0 ;
  wire \q_reg[15] ;
  wire \q_reg[15]_0 ;
  wire \q_reg[15]_1 ;
  wire [1:0]\q_reg[19] ;
  wire [1:0]\q_reg[19]_0 ;
  wire \q_reg[19]_1 ;
  wire \q_reg[19]_2 ;
  wire \q_reg[1]_0 ;
  wire \q_reg[1]_1 ;
  wire \q_reg[1]_2 ;
  wire \q_reg[1]_3 ;
  wire \q_reg[1]_4 ;
  wire \q_reg[1]_5 ;
  wire \q_reg[1]_6 ;
  wire [2:0]\q_reg[22] ;
  wire [2:0]\q_reg[22]_0 ;
  wire \q_reg[23] ;
  wire \q_reg[23]_0 ;
  wire \q_reg[23]_1 ;
  wire \q_reg[2]_0 ;
  wire \q_reg[2]_1 ;
  wire \q_reg[2]_2 ;
  wire \q_reg[3]_0 ;
  wire \q_reg[4]_0 ;
  wire \q_reg[4]_1 ;
  wire s00_axi_aclk;
  wire y0_carry__1_i_10__0_n_0;
  wire y0_carry__1_i_10_n_0;
  wire y0_carry__1_i_11__0_n_0;
  wire y0_carry__1_i_11_n_0;
  wire y0_carry__1_i_2__0_0;
  wire y0_carry__1_i_2__0_1;
  wire y0_carry__1_i_2__0_2;
  wire y0_carry__1_i_2__1_0;
  wire y0_carry__1_i_2__1_1;
  wire y0_carry__1_i_2__1_2;
  wire y0_carry__1_i_3__0;
  wire y0_carry__1_i_3__0_0;
  wire y0_carry__1_i_3__0_1;
  wire y0_carry__1_i_3__1;
  wire y0_carry__1_i_3__1_0;
  wire y0_carry__1_i_3__1_1;
  wire y0_carry__1_i_5__0_n_0;
  wire y0_carry__1_i_5_n_0;
  wire y0_carry__1_i_6_0;
  wire y0_carry__1_i_6__0_n_0;
  wire y0_carry__1_i_6_n_0;
  wire y0_carry__1_i_7_0;
  wire y0_carry__1_i_9__0_n_0;
  wire y0_carry__1_i_9_n_0;
  wire y0_carry__2_i_12__0_n_0;
  wire y0_carry__2_i_12_n_0;
  wire y0_carry__2_i_1__0;
  wire y0_carry__2_i_1__1;
  wire y0_carry__2_i_3__0_0;
  wire y0_carry__2_i_3__1_0;
  wire y0_carry__2_i_7__0_n_0;
  wire y0_carry__2_i_7_n_0;
  wire y0_carry__2_i_8__0_n_0;
  wire y0_carry__2_i_8_n_0;
  wire y0_carry__3_i_10__0_n_0;
  wire y0_carry__3_i_10_n_0;
  wire y0_carry__3_i_11__0_n_0;
  wire y0_carry__3_i_11_n_0;
  wire y0_carry__3_i_2__0_0;
  wire y0_carry__3_i_2__1_0;
  wire y0_carry__3_i_3__0;
  wire y0_carry__3_i_3__1;
  wire y0_carry__3_i_5__0_n_0;
  wire y0_carry__3_i_5_n_0;
  wire y0_carry__3_i_6__0_n_0;
  wire y0_carry__3_i_6_n_0;
  wire y0_carry__3_i_9__0_n_0;
  wire y0_carry__3_i_9_n_0;
  wire y0_carry__4_i_2__0_0;
  wire y0_carry__4_i_2__0_1;
  wire y0_carry__4_i_6__0_n_0;
  wire y0_carry__4_i_6_n_0;
  wire y0_carry__4_i_7__0_n_0;
  wire y0_carry__4_i_7_n_0;
  wire y0_carry__4_i_8__0_n_0;
  wire y0_carry__4_i_8_n_0;

  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFFFFFFDF)) 
    \FSM_sequential_state[0]_i_3 
       (.I0(\q_reg[4]_1 ),
        .I1(\q_reg[2]_0 ),
        .I2(\q_reg[3]_0 ),
        .I3(\q_reg[1]_0 ),
        .I4(\q_reg[0]_0 ),
        .O(\q_reg[4]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \q[0]_i_1 
       (.I0(\q_reg[0]_0 ),
        .O(\q[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \q[1]_i_1 
       (.I0(\q_reg[0]_0 ),
        .I1(\q_reg[1]_0 ),
        .O(\q[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \q[2]_i_1 
       (.I0(\q_reg[2]_0 ),
        .I1(\q_reg[1]_0 ),
        .I2(\q_reg[0]_0 ),
        .O(\q[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \q[3]_i_1 
       (.I0(\q_reg[3]_0 ),
        .I1(\q_reg[2]_0 ),
        .I2(\q_reg[0]_0 ),
        .I3(\q_reg[1]_0 ),
        .O(\q[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \q[4]_i_3 
       (.I0(\q_reg[4]_1 ),
        .I1(\q_reg[1]_0 ),
        .I2(\q_reg[0]_0 ),
        .I3(\q_reg[2]_0 ),
        .I4(\q_reg[3]_0 ),
        .O(\q[4]_i_3_n_0 ));
  FDRE \q_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\q_reg[0]_2 ),
        .D(\q[0]_i_1_n_0 ),
        .Q(\q_reg[0]_0 ),
        .R(\q_reg[0]_1 ));
  FDRE \q_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\q_reg[0]_2 ),
        .D(\q[1]_i_1_n_0 ),
        .Q(\q_reg[1]_0 ),
        .R(\q_reg[0]_1 ));
  FDRE \q_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\q_reg[0]_2 ),
        .D(\q[2]_i_1_n_0 ),
        .Q(\q_reg[2]_0 ),
        .R(\q_reg[0]_1 ));
  FDRE \q_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\q_reg[0]_2 ),
        .D(\q[3]_i_1_n_0 ),
        .Q(\q_reg[3]_0 ),
        .R(\q_reg[0]_1 ));
  FDRE \q_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\q_reg[0]_2 ),
        .D(\q[4]_i_3_n_0 ),
        .Q(\q_reg[4]_1 ),
        .R(\q_reg[0]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__1_i_10
       (.I0(\q_reg[3]_0 ),
        .I1(y0_carry__4_i_2__0_0),
        .I2(\q_reg[4]_1 ),
        .I3(y0_carry__1_i_7_0),
        .O(y0_carry__1_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__1_i_10__0
       (.I0(\q_reg[3]_0 ),
        .I1(A[13]),
        .I2(\q_reg[4]_1 ),
        .I3(A[5]),
        .O(y0_carry__1_i_10__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__1_i_11
       (.I0(\q_reg[3]_0 ),
        .I1(y0_carry__4_i_2__0_0),
        .I2(\q_reg[4]_1 ),
        .I3(y0_carry__1_i_6_0),
        .O(y0_carry__1_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__1_i_11__0
       (.I0(\q_reg[3]_0 ),
        .I1(A[13]),
        .I2(\q_reg[4]_1 ),
        .I3(A[6]),
        .O(y0_carry__1_i_11__0_n_0));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__1_i_1__0
       (.I0(A[1]),
        .I1(y0_carry__1_i_5_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__1_i_6_n_0),
        .I4(Q),
        .O(S[1]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__1_i_1__1
       (.I0(\q_reg[11]_1 ),
        .I1(y0_carry__1_i_5__0_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__1_i_6__0_n_0),
        .I4(Q),
        .O(\q_reg[11] [1]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__1_i_2__0
       (.I0(A[0]),
        .I1(y0_carry__1_i_6_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(\q_reg[1]_2 ),
        .I4(Q),
        .O(S[0]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__1_i_2__1
       (.I0(\q_reg[11]_0 ),
        .I1(y0_carry__1_i_6__0_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(\q_reg[1]_5 ),
        .I4(Q),
        .O(\q_reg[11] [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_5
       (.I0(y0_carry__1_i_9_n_0),
        .I1(y0_carry__1_i_3__0_0),
        .I2(\q_reg[1]_0 ),
        .I3(y0_carry__1_i_10_n_0),
        .I4(\q_reg[2]_0 ),
        .I5(y0_carry__1_i_3__0),
        .O(y0_carry__1_i_5_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_5__0
       (.I0(y0_carry__1_i_9__0_n_0),
        .I1(y0_carry__1_i_3__1_0),
        .I2(\q_reg[1]_0 ),
        .I3(y0_carry__1_i_10__0_n_0),
        .I4(\q_reg[2]_0 ),
        .I5(y0_carry__1_i_3__1),
        .O(y0_carry__1_i_5__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_6
       (.I0(y0_carry__1_i_11_n_0),
        .I1(y0_carry__1_i_2__0_0),
        .I2(\q_reg[1]_0 ),
        .I3(y0_carry__1_i_2__0_1),
        .I4(\q_reg[2]_0 ),
        .I5(y0_carry__1_i_2__0_2),
        .O(y0_carry__1_i_6_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_6__0
       (.I0(y0_carry__1_i_11__0_n_0),
        .I1(y0_carry__1_i_2__1_0),
        .I2(\q_reg[1]_0 ),
        .I3(y0_carry__1_i_2__1_1),
        .I4(\q_reg[2]_0 ),
        .I5(y0_carry__1_i_2__1_2),
        .O(y0_carry__1_i_6__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_7
       (.I0(y0_carry__1_i_10_n_0),
        .I1(y0_carry__1_i_3__0),
        .I2(\q_reg[1]_0 ),
        .I3(y0_carry__1_i_3__0_0),
        .I4(\q_reg[2]_0 ),
        .I5(y0_carry__1_i_3__0_1),
        .O(\q_reg[1]_2 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_7__0
       (.I0(y0_carry__1_i_10__0_n_0),
        .I1(y0_carry__1_i_3__1),
        .I2(\q_reg[1]_0 ),
        .I3(y0_carry__1_i_3__1_0),
        .I4(\q_reg[2]_0 ),
        .I5(y0_carry__1_i_3__1_1),
        .O(\q_reg[1]_5 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__1_i_9
       (.I0(\q_reg[3]_0 ),
        .I1(y0_carry__4_i_2__0_0),
        .I2(\q_reg[4]_1 ),
        .I3(\q_reg[19]_2 ),
        .O(y0_carry__1_i_9_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__1_i_9__0
       (.I0(\q_reg[3]_0 ),
        .I1(A[13]),
        .I2(\q_reg[4]_1 ),
        .I3(A[7]),
        .O(y0_carry__1_i_9__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__2_i_12
       (.I0(\q_reg[3]_0 ),
        .I1(y0_carry__4_i_2__0_0),
        .I2(\q_reg[4]_1 ),
        .I3(\q_reg[19]_1 ),
        .O(y0_carry__2_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__2_i_12__0
       (.I0(\q_reg[3]_0 ),
        .I1(A[13]),
        .I2(\q_reg[4]_1 ),
        .I3(A[8]),
        .O(y0_carry__2_i_12__0_n_0));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__2_i_2__0
       (.I0(A[4]),
        .I1(\q_reg[1]_1 ),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__2_i_7_n_0),
        .I4(Q),
        .O(\q_reg[14] [2]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__2_i_2__1
       (.I0(\q_reg[15]_1 ),
        .I1(\q_reg[1]_4 ),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__2_i_7__0_n_0),
        .I4(Q),
        .O(\q_reg[14]_0 [2]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__2_i_3__0
       (.I0(A[3]),
        .I1(y0_carry__2_i_7_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__2_i_8_n_0),
        .I4(Q),
        .O(\q_reg[14] [1]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__2_i_3__1
       (.I0(\q_reg[15]_0 ),
        .I1(y0_carry__2_i_7__0_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__2_i_8__0_n_0),
        .I4(Q),
        .O(\q_reg[14]_0 [1]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__2_i_4__0
       (.I0(A[2]),
        .I1(y0_carry__2_i_8_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__1_i_5_n_0),
        .I4(Q),
        .O(\q_reg[14] [0]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__2_i_4__1
       (.I0(\q_reg[15] ),
        .I1(y0_carry__2_i_8__0_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__1_i_5__0_n_0),
        .I4(Q),
        .O(\q_reg[14]_0 [0]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    y0_carry__2_i_6
       (.I0(y0_carry__2_i_1__0),
        .I1(\q_reg[1]_0 ),
        .I2(y0_carry__2_i_12_n_0),
        .I3(\q_reg[2]_0 ),
        .I4(y0_carry__1_i_2__0_1),
        .O(\q_reg[1]_1 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    y0_carry__2_i_6__0
       (.I0(y0_carry__2_i_1__1),
        .I1(\q_reg[1]_0 ),
        .I2(y0_carry__2_i_12__0_n_0),
        .I3(\q_reg[2]_0 ),
        .I4(y0_carry__1_i_2__1_1),
        .O(\q_reg[1]_4 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    y0_carry__2_i_7
       (.I0(y0_carry__2_i_3__0_0),
        .I1(\q_reg[1]_0 ),
        .I2(y0_carry__1_i_9_n_0),
        .I3(\q_reg[2]_0 ),
        .I4(y0_carry__1_i_3__0_0),
        .O(y0_carry__2_i_7_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    y0_carry__2_i_7__0
       (.I0(y0_carry__2_i_3__1_0),
        .I1(\q_reg[1]_0 ),
        .I2(y0_carry__1_i_9__0_n_0),
        .I3(\q_reg[2]_0 ),
        .I4(y0_carry__1_i_3__1_0),
        .O(y0_carry__2_i_7__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__2_i_8
       (.I0(y0_carry__2_i_12_n_0),
        .I1(y0_carry__1_i_2__0_1),
        .I2(\q_reg[1]_0 ),
        .I3(y0_carry__1_i_11_n_0),
        .I4(\q_reg[2]_0 ),
        .I5(y0_carry__1_i_2__0_0),
        .O(y0_carry__2_i_8_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__2_i_8__0
       (.I0(y0_carry__2_i_12__0_n_0),
        .I1(y0_carry__1_i_2__1_1),
        .I2(\q_reg[1]_0 ),
        .I3(y0_carry__1_i_11__0_n_0),
        .I4(\q_reg[2]_0 ),
        .I5(y0_carry__1_i_2__1_0),
        .O(y0_carry__2_i_8__0_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__3_i_10
       (.I0(\q_reg[2]_0 ),
        .I1(\q_reg[3]_0 ),
        .I2(y0_carry__4_i_2__0_0),
        .I3(\q_reg[4]_1 ),
        .I4(\q_reg[23]_1 ),
        .O(y0_carry__3_i_10_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__3_i_10__0
       (.I0(\q_reg[2]_0 ),
        .I1(\q_reg[3]_0 ),
        .I2(A[13]),
        .I3(\q_reg[4]_1 ),
        .I4(A[9]),
        .O(y0_carry__3_i_10__0_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__3_i_11
       (.I0(\q_reg[2]_0 ),
        .I1(\q_reg[3]_0 ),
        .I2(y0_carry__4_i_2__0_0),
        .I3(\q_reg[4]_1 ),
        .I4(\q_reg[23]_0 ),
        .O(y0_carry__3_i_11_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__3_i_11__0
       (.I0(\q_reg[2]_0 ),
        .I1(\q_reg[3]_0 ),
        .I2(A[13]),
        .I3(\q_reg[4]_1 ),
        .I4(A[10]),
        .O(y0_carry__3_i_11__0_n_0));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__3_i_1__0
       (.I0(A[8]),
        .I1(y0_carry__3_i_5_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__3_i_6_n_0),
        .I4(Q),
        .O(\q_reg[19] [1]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__3_i_1__1
       (.I0(\q_reg[19]_1 ),
        .I1(y0_carry__3_i_5__0_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__3_i_6__0_n_0),
        .I4(Q),
        .O(\q_reg[19]_0 [1]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__3_i_2__0
       (.I0(A[7]),
        .I1(y0_carry__3_i_6_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(\q_reg[1]_3 ),
        .I4(Q),
        .O(\q_reg[19] [0]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__3_i_2__1
       (.I0(\q_reg[19]_2 ),
        .I1(y0_carry__3_i_6__0_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(\q_reg[1]_6 ),
        .I4(Q),
        .O(\q_reg[19]_0 [0]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_5
       (.I0(y0_carry__3_i_9_n_0),
        .I1(\q_reg[1]_0 ),
        .I2(y0_carry__3_i_10_n_0),
        .O(y0_carry__3_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_5__0
       (.I0(y0_carry__3_i_9__0_n_0),
        .I1(\q_reg[1]_0 ),
        .I2(y0_carry__3_i_10__0_n_0),
        .O(y0_carry__3_i_5__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_6
       (.I0(y0_carry__3_i_11_n_0),
        .I1(\q_reg[1]_0 ),
        .I2(y0_carry__3_i_2__0_0),
        .O(y0_carry__3_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_6__0
       (.I0(y0_carry__3_i_11__0_n_0),
        .I1(\q_reg[1]_0 ),
        .I2(y0_carry__3_i_2__1_0),
        .O(y0_carry__3_i_6__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_7
       (.I0(y0_carry__3_i_10_n_0),
        .I1(\q_reg[1]_0 ),
        .I2(y0_carry__3_i_3__0),
        .O(\q_reg[1]_3 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_7__0
       (.I0(y0_carry__3_i_10__0_n_0),
        .I1(\q_reg[1]_0 ),
        .I2(y0_carry__3_i_3__1),
        .O(\q_reg[1]_6 ));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__3_i_9
       (.I0(\q_reg[2]_0 ),
        .I1(\q_reg[3]_0 ),
        .I2(y0_carry__4_i_2__0_0),
        .I3(\q_reg[4]_1 ),
        .I4(\q_reg[23] ),
        .O(y0_carry__3_i_9_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__3_i_9__0
       (.I0(\q_reg[2]_0 ),
        .I1(\q_reg[3]_0 ),
        .I2(A[13]),
        .I3(\q_reg[4]_1 ),
        .I4(A[11]),
        .O(y0_carry__3_i_9__0_n_0));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__4_i_2__0
       (.I0(A[11]),
        .I1(y0_carry__4_i_6_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__4_i_7_n_0),
        .I4(Q),
        .O(\q_reg[22] [2]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__4_i_2__1
       (.I0(\q_reg[23] ),
        .I1(y0_carry__4_i_6__0_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__4_i_7__0_n_0),
        .I4(Q),
        .O(\q_reg[22]_0 [2]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__4_i_3__0
       (.I0(A[10]),
        .I1(y0_carry__4_i_7_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__4_i_8_n_0),
        .I4(Q),
        .O(\q_reg[22] [1]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__4_i_3__1
       (.I0(\q_reg[23]_0 ),
        .I1(y0_carry__4_i_7__0_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__4_i_8__0_n_0),
        .I4(Q),
        .O(\q_reg[22]_0 [1]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__4_i_4__0
       (.I0(A[9]),
        .I1(y0_carry__4_i_8_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__3_i_5_n_0),
        .I4(Q),
        .O(\q_reg[22] [0]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__4_i_4__1
       (.I0(\q_reg[23]_1 ),
        .I1(y0_carry__4_i_8__0_n_0),
        .I2(\q_reg[0]_0 ),
        .I3(y0_carry__3_i_5__0_n_0),
        .I4(Q),
        .O(\q_reg[22]_0 [0]));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__4_i_5
       (.I0(\q_reg[2]_0 ),
        .I1(\q_reg[3]_0 ),
        .I2(y0_carry__4_i_2__0_0),
        .I3(\q_reg[4]_1 ),
        .I4(y0_carry__4_i_2__0_1),
        .O(\q_reg[2]_1 ));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__4_i_5__0
       (.I0(\q_reg[2]_0 ),
        .I1(\q_reg[3]_0 ),
        .I2(A[13]),
        .I3(\q_reg[4]_1 ),
        .I4(A[12]),
        .O(\q_reg[2]_2 ));
  LUT6 #(
    .INIT(64'hFF00FF01FF00FE00)) 
    y0_carry__4_i_6
       (.I0(\q_reg[1]_0 ),
        .I1(\q_reg[2]_0 ),
        .I2(\q_reg[3]_0 ),
        .I3(y0_carry__4_i_2__0_0),
        .I4(\q_reg[4]_1 ),
        .I5(y0_carry__4_i_2__0_1),
        .O(y0_carry__4_i_6_n_0));
  LUT6 #(
    .INIT(64'hFF00FF01FF00FE00)) 
    y0_carry__4_i_6__0
       (.I0(\q_reg[1]_0 ),
        .I1(\q_reg[2]_0 ),
        .I2(\q_reg[3]_0 ),
        .I3(A[13]),
        .I4(\q_reg[4]_1 ),
        .I5(A[12]),
        .O(y0_carry__4_i_6__0_n_0));
  LUT6 #(
    .INIT(64'hFF00FF01FF00FE00)) 
    y0_carry__4_i_7
       (.I0(\q_reg[1]_0 ),
        .I1(\q_reg[2]_0 ),
        .I2(\q_reg[3]_0 ),
        .I3(y0_carry__4_i_2__0_0),
        .I4(\q_reg[4]_1 ),
        .I5(\q_reg[23] ),
        .O(y0_carry__4_i_7_n_0));
  LUT6 #(
    .INIT(64'hFF00FF01FF00FE00)) 
    y0_carry__4_i_7__0
       (.I0(\q_reg[1]_0 ),
        .I1(\q_reg[2]_0 ),
        .I2(\q_reg[3]_0 ),
        .I3(A[13]),
        .I4(\q_reg[4]_1 ),
        .I5(A[11]),
        .O(y0_carry__4_i_7__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__4_i_8
       (.I0(\q_reg[2]_1 ),
        .I1(\q_reg[1]_0 ),
        .I2(y0_carry__3_i_11_n_0),
        .O(y0_carry__4_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__4_i_8__0
       (.I0(\q_reg[2]_2 ),
        .I1(\q_reg[1]_0 ),
        .I2(y0_carry__3_i_11__0_n_0),
        .O(y0_carry__4_i_8__0_n_0));
endmodule

(* ORIG_REF_NAME = "reg" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg__parameterized1
   (\q_reg[1]_0 ,
    \q_reg[23]_0 ,
    \q_reg[19]_0 ,
    \q_reg[21]_0 ,
    \q_reg[23]_1 ,
    \q_reg[24]_0 ,
    A,
    \q_reg[1]_1 ,
    \q_reg[23]_2 ,
    \q_reg[21]_1 ,
    \q_reg[19]_1 ,
    \q_reg[22]_0 ,
    \q_reg[18]_0 ,
    \q_reg[20]_0 ,
    \q_reg[14]_0 ,
    \q_reg[1]_2 ,
    \q_reg[22]_1 ,
    \q_reg[20]_1 ,
    DI,
    \q_reg[9]_0 ,
    \q_reg[15]_0 ,
    \q_reg[17]_0 ,
    \q_reg[23]_3 ,
    S,
    \q_reg[7]_0 ,
    \q_reg[8]_0 ,
    \q_reg[16]_0 ,
    addr,
    \q_reg[11]_0 ,
    \q_reg[11]_1 ,
    Q,
    \q_reg[15]_1 ,
    \q_reg[15]_2 ,
    \q_reg[19]_2 ,
    \q_reg[19]_3 ,
    \q_reg[23]_4 ,
    \q_reg[23]_5 ,
    \q_reg[3]_0 ,
    \q_reg[7]_1 ,
    \q_reg[11]_2 ,
    \q_reg[19]_4 ,
    \q_reg[22]_2 ,
    en_reg,
    \q_reg[24]_1 ,
    s00_axi_aclk,
    \q_reg[23]_6 ,
    \q_reg[19]_5 ,
    \q_reg[15]_3 ,
    \q_reg[11]_3 ,
    \q_reg[7]_2 ,
    O);
  output \q_reg[1]_0 ;
  output \q_reg[23]_0 ;
  output \q_reg[19]_0 ;
  output \q_reg[21]_0 ;
  output [3:0]\q_reg[23]_1 ;
  output \q_reg[24]_0 ;
  output [10:0]A;
  output \q_reg[1]_1 ;
  output \q_reg[23]_2 ;
  output \q_reg[21]_1 ;
  output [3:0]\q_reg[19]_1 ;
  output \q_reg[22]_0 ;
  output \q_reg[18]_0 ;
  output \q_reg[20]_0 ;
  output [2:0]\q_reg[14]_0 ;
  output \q_reg[1]_2 ;
  output \q_reg[22]_1 ;
  output \q_reg[20]_1 ;
  output [1:0]DI;
  output [0:0]\q_reg[9]_0 ;
  output [0:0]\q_reg[15]_0 ;
  output [0:0]\q_reg[17]_0 ;
  output [0:0]\q_reg[23]_3 ;
  output [3:0]S;
  output [3:0]\q_reg[7]_0 ;
  output [0:0]\q_reg[8]_0 ;
  output [0:0]\q_reg[16]_0 ;
  input [4:0]addr;
  input \q_reg[11]_0 ;
  input \q_reg[11]_1 ;
  input [0:0]Q;
  input \q_reg[15]_1 ;
  input \q_reg[15]_2 ;
  input \q_reg[19]_2 ;
  input \q_reg[19]_3 ;
  input \q_reg[23]_4 ;
  input \q_reg[23]_5 ;
  input [3:0]\q_reg[3]_0 ;
  input [3:0]\q_reg[7]_1 ;
  input [0:0]\q_reg[11]_2 ;
  input [0:0]\q_reg[19]_4 ;
  input \q_reg[22]_2 ;
  input en_reg;
  input [0:0]\q_reg[24]_1 ;
  input s00_axi_aclk;
  input [3:0]\q_reg[23]_6 ;
  input [3:0]\q_reg[19]_5 ;
  input [3:0]\q_reg[15]_3 ;
  input [3:0]\q_reg[11]_3 ;
  input [3:0]\q_reg[7]_2 ;
  input [3:0]O;

  wire [10:0]A;
  wire [1:0]DI;
  wire [3:0]O;
  wire [0:0]Q;
  wire [3:0]S;
  wire [4:0]addr;
  wire en_reg;
  wire \q_reg[11]_0 ;
  wire \q_reg[11]_1 ;
  wire [0:0]\q_reg[11]_2 ;
  wire [3:0]\q_reg[11]_3 ;
  wire [2:0]\q_reg[14]_0 ;
  wire [0:0]\q_reg[15]_0 ;
  wire \q_reg[15]_1 ;
  wire \q_reg[15]_2 ;
  wire [3:0]\q_reg[15]_3 ;
  wire [0:0]\q_reg[16]_0 ;
  wire [0:0]\q_reg[17]_0 ;
  wire \q_reg[18]_0 ;
  wire \q_reg[19]_0 ;
  wire [3:0]\q_reg[19]_1 ;
  wire \q_reg[19]_2 ;
  wire \q_reg[19]_3 ;
  wire [0:0]\q_reg[19]_4 ;
  wire [3:0]\q_reg[19]_5 ;
  wire \q_reg[1]_0 ;
  wire \q_reg[1]_1 ;
  wire \q_reg[1]_2 ;
  wire \q_reg[20]_0 ;
  wire \q_reg[20]_1 ;
  wire \q_reg[21]_0 ;
  wire \q_reg[21]_1 ;
  wire \q_reg[22]_0 ;
  wire \q_reg[22]_1 ;
  wire \q_reg[22]_2 ;
  wire \q_reg[23]_0 ;
  wire [3:0]\q_reg[23]_1 ;
  wire \q_reg[23]_2 ;
  wire [0:0]\q_reg[23]_3 ;
  wire \q_reg[23]_4 ;
  wire \q_reg[23]_5 ;
  wire [3:0]\q_reg[23]_6 ;
  wire \q_reg[24]_0 ;
  wire [0:0]\q_reg[24]_1 ;
  wire [3:0]\q_reg[3]_0 ;
  wire [3:0]\q_reg[7]_0 ;
  wire [3:0]\q_reg[7]_1 ;
  wire [3:0]\q_reg[7]_2 ;
  wire [0:0]\q_reg[8]_0 ;
  wire [0:0]\q_reg[9]_0 ;
  wire s00_axi_aclk;
  wire y0_carry__0_i_5__0_n_0;
  wire y0_carry__0_i_6__0_n_0;
  wire y0_carry__0_i_7__0_n_0;
  wire y0_carry__0_i_8__0_n_0;
  wire y0_carry_i_11__0_n_0;
  wire y0_carry_i_12__0_n_0;
  wire y0_carry_i_13__0_n_0;
  wire y0_carry_i_14__0_n_0;
  wire y0_carry_i_15__0_n_0;
  wire y0_carry_i_16__0_n_0;
  wire y0_carry_i_17__0_n_0;
  wire y0_carry_i_18__0_n_0;
  wire y0_carry_i_19__0_n_0;
  wire y0_carry_i_20__0_n_0;
  wire y0_carry_i_5__0_n_0;
  wire y0_carry_i_6__0_n_0;
  wire y0_carry_i_7__0_n_0;
  wire y0_carry_i_8__0_n_0;
  wire y0_carry_i_9__0_n_0;

  FDSE \q_reg[0] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(O[0]),
        .Q(A[0]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[10] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[11]_3 [2]),
        .Q(DI[0]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[11] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[11]_3 [3]),
        .Q(DI[1]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[12] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[15]_3 [0]),
        .Q(\q_reg[14]_0 [0]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[13] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[15]_3 [1]),
        .Q(\q_reg[14]_0 [1]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[14] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[15]_3 [2]),
        .Q(\q_reg[14]_0 [2]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[15] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[15]_3 [3]),
        .Q(A[10]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[16] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[19]_5 [0]),
        .Q(\q_reg[19]_1 [0]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[17] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[19]_5 [1]),
        .Q(\q_reg[19]_1 [1]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[18] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[19]_5 [2]),
        .Q(\q_reg[19]_1 [2]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[19] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[19]_5 [3]),
        .Q(\q_reg[19]_1 [3]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[1] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(O[1]),
        .Q(A[1]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[20] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_6 [0]),
        .Q(\q_reg[23]_1 [0]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[21] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_6 [1]),
        .Q(\q_reg[23]_1 [1]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[22] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_6 [2]),
        .Q(\q_reg[23]_1 [2]),
        .S(\q_reg[22]_2 ));
  FDRE \q_reg[23] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_6 [3]),
        .Q(\q_reg[23]_1 [3]),
        .R(\q_reg[22]_2 ));
  FDRE \q_reg[24] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[24]_1 ),
        .Q(\q_reg[24]_0 ),
        .R(\q_reg[22]_2 ));
  FDSE \q_reg[2] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(O[2]),
        .Q(A[2]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[3] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(O[3]),
        .Q(A[3]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[4] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[7]_2 [0]),
        .Q(A[4]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[5] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[7]_2 [1]),
        .Q(A[5]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[6] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[7]_2 [2]),
        .Q(A[6]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[7] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[7]_2 [3]),
        .Q(A[7]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[8] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[11]_3 [0]),
        .Q(A[8]),
        .S(\q_reg[22]_2 ));
  FDSE \q_reg[9] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[11]_3 [1]),
        .Q(A[9]),
        .S(\q_reg[22]_2 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_10__0
       (.I0(\q_reg[23]_1 [0]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(\q_reg[14]_0 [0]),
        .O(\q_reg[20]_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_11__0
       (.I0(\q_reg[23]_1 [1]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(\q_reg[14]_0 [1]),
        .O(\q_reg[21]_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_12__0
       (.I0(\q_reg[19]_1 [3]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(DI[1]),
        .O(\q_reg[19]_0 ));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__0_i_1__1
       (.I0(\q_reg[7]_1 [3]),
        .I1(y0_carry__0_i_5__0_n_0),
        .I2(addr[0]),
        .I3(y0_carry__0_i_6__0_n_0),
        .I4(Q),
        .O(\q_reg[7]_0 [3]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__0_i_2__1
       (.I0(\q_reg[7]_1 [2]),
        .I1(y0_carry__0_i_6__0_n_0),
        .I2(addr[0]),
        .I3(y0_carry__0_i_7__0_n_0),
        .I4(Q),
        .O(\q_reg[7]_0 [2]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__0_i_3__1
       (.I0(\q_reg[7]_1 [1]),
        .I1(y0_carry__0_i_7__0_n_0),
        .I2(addr[0]),
        .I3(y0_carry__0_i_8__0_n_0),
        .I4(Q),
        .O(\q_reg[7]_0 [1]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__0_i_4__1
       (.I0(\q_reg[7]_1 [0]),
        .I1(y0_carry__0_i_8__0_n_0),
        .I2(addr[0]),
        .I3(y0_carry_i_5__0_n_0),
        .I4(Q),
        .O(\q_reg[7]_0 [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_5__0
       (.I0(\q_reg[22]_0 ),
        .I1(\q_reg[18]_0 ),
        .I2(addr[1]),
        .I3(\q_reg[20]_0 ),
        .I4(addr[2]),
        .I5(y0_carry_i_12__0_n_0),
        .O(y0_carry__0_i_5__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_6__0
       (.I0(\q_reg[21]_0 ),
        .I1(y0_carry_i_14__0_n_0),
        .I2(addr[1]),
        .I3(\q_reg[19]_0 ),
        .I4(addr[2]),
        .I5(y0_carry_i_16__0_n_0),
        .O(y0_carry__0_i_6__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_7__0
       (.I0(\q_reg[20]_0 ),
        .I1(y0_carry_i_12__0_n_0),
        .I2(addr[1]),
        .I3(\q_reg[18]_0 ),
        .I4(addr[2]),
        .I5(y0_carry_i_11__0_n_0),
        .O(y0_carry__0_i_7__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_8__0
       (.I0(\q_reg[19]_0 ),
        .I1(y0_carry_i_16__0_n_0),
        .I2(addr[1]),
        .I3(y0_carry_i_14__0_n_0),
        .I4(addr[2]),
        .I5(y0_carry_i_15__0_n_0),
        .O(y0_carry__0_i_8__0_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_9__0
       (.I0(\q_reg[23]_1 [2]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(\q_reg[14]_0 [2]),
        .O(\q_reg[22]_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__1_i_12__0
       (.I0(\q_reg[23]_1 [3]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(A[10]),
        .O(\q_reg[23]_0 ));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__1_i_3__0
       (.I0(A[9]),
        .I1(\q_reg[11]_0 ),
        .I2(addr[0]),
        .I3(\q_reg[11]_1 ),
        .I4(Q),
        .O(\q_reg[9]_0 ));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__1_i_4__1
       (.I0(\q_reg[11]_2 ),
        .I1(\q_reg[1]_0 ),
        .I2(addr[0]),
        .I3(y0_carry__0_i_5__0_n_0),
        .I4(Q),
        .O(\q_reg[8]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_8__0
       (.I0(\q_reg[23]_0 ),
        .I1(\q_reg[19]_0 ),
        .I2(addr[1]),
        .I3(\q_reg[21]_0 ),
        .I4(addr[2]),
        .I5(y0_carry_i_14__0_n_0),
        .O(\q_reg[1]_0 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__2_i_10__0
       (.I0(\q_reg[23]_1 [0]),
        .I1(addr[2]),
        .I2(addr[3]),
        .I3(\q_reg[24]_0 ),
        .I4(addr[4]),
        .I5(\q_reg[19]_1 [0]),
        .O(\q_reg[20]_1 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__2_i_11__0
       (.I0(\q_reg[23]_1 [1]),
        .I1(addr[2]),
        .I2(addr[3]),
        .I3(\q_reg[24]_0 ),
        .I4(addr[4]),
        .I5(\q_reg[19]_1 [1]),
        .O(\q_reg[21]_1 ));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__2_i_1__0
       (.I0(A[10]),
        .I1(\q_reg[15]_1 ),
        .I2(addr[0]),
        .I3(\q_reg[15]_2 ),
        .I4(Q),
        .O(\q_reg[15]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__2_i_5__0
       (.I0(\q_reg[22]_1 ),
        .I1(addr[1]),
        .I2(\q_reg[20]_1 ),
        .O(\q_reg[1]_2 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__2_i_9__0
       (.I0(\q_reg[23]_1 [2]),
        .I1(addr[2]),
        .I2(addr[3]),
        .I3(\q_reg[24]_0 ),
        .I4(addr[4]),
        .I5(\q_reg[19]_1 [2]),
        .O(\q_reg[22]_1 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__3_i_12__0
       (.I0(\q_reg[23]_1 [3]),
        .I1(addr[2]),
        .I2(addr[3]),
        .I3(\q_reg[24]_0 ),
        .I4(addr[4]),
        .I5(\q_reg[19]_1 [3]),
        .O(\q_reg[23]_2 ));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__3_i_3__0
       (.I0(\q_reg[19]_1 [1]),
        .I1(\q_reg[19]_2 ),
        .I2(addr[0]),
        .I3(\q_reg[19]_3 ),
        .I4(Q),
        .O(\q_reg[17]_0 ));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__3_i_4__1
       (.I0(\q_reg[19]_4 ),
        .I1(\q_reg[1]_1 ),
        .I2(addr[0]),
        .I3(\q_reg[1]_2 ),
        .I4(Q),
        .O(\q_reg[16]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_8__0
       (.I0(\q_reg[23]_2 ),
        .I1(addr[1]),
        .I2(\q_reg[21]_1 ),
        .O(\q_reg[1]_1 ));
  LUT6 #(
    .INIT(64'h5556AAA6AAA95559)) 
    y0_carry__4_i_1__0
       (.I0(\q_reg[23]_1 [3]),
        .I1(\q_reg[23]_4 ),
        .I2(addr[0]),
        .I3(addr[1]),
        .I4(\q_reg[23]_5 ),
        .I5(Q),
        .O(\q_reg[23]_3 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry_i_10__0
       (.I0(\q_reg[19]_1 [2]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(DI[0]),
        .O(\q_reg[18]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_11__0
       (.I0(\q_reg[24]_0 ),
        .I1(\q_reg[14]_0 [2]),
        .I2(addr[3]),
        .I3(\q_reg[23]_1 [2]),
        .I4(addr[4]),
        .I5(A[6]),
        .O(y0_carry_i_11__0_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry_i_12__0
       (.I0(\q_reg[19]_1 [0]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(A[8]),
        .O(y0_carry_i_12__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_13__0
       (.I0(\q_reg[24]_0 ),
        .I1(\q_reg[14]_0 [0]),
        .I2(addr[3]),
        .I3(\q_reg[23]_1 [0]),
        .I4(addr[4]),
        .I5(A[4]),
        .O(y0_carry_i_13__0_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry_i_14__0
       (.I0(\q_reg[19]_1 [1]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(A[9]),
        .O(y0_carry_i_14__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_15__0
       (.I0(\q_reg[24]_0 ),
        .I1(\q_reg[14]_0 [1]),
        .I2(addr[3]),
        .I3(\q_reg[23]_1 [1]),
        .I4(addr[4]),
        .I5(A[5]),
        .O(y0_carry_i_15__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_16__0
       (.I0(\q_reg[24]_0 ),
        .I1(A[10]),
        .I2(addr[3]),
        .I3(\q_reg[23]_1 [3]),
        .I4(addr[4]),
        .I5(A[7]),
        .O(y0_carry_i_16__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_17__0
       (.I0(\q_reg[24]_0 ),
        .I1(DI[1]),
        .I2(addr[3]),
        .I3(\q_reg[19]_1 [3]),
        .I4(addr[4]),
        .I5(A[3]),
        .O(y0_carry_i_17__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_18__0
       (.I0(\q_reg[24]_0 ),
        .I1(DI[0]),
        .I2(addr[3]),
        .I3(\q_reg[19]_1 [2]),
        .I4(addr[4]),
        .I5(A[2]),
        .O(y0_carry_i_18__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_19__0
       (.I0(\q_reg[24]_0 ),
        .I1(A[9]),
        .I2(addr[3]),
        .I3(\q_reg[19]_1 [1]),
        .I4(addr[4]),
        .I5(A[1]),
        .O(y0_carry_i_19__0_n_0));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry_i_1__1
       (.I0(\q_reg[3]_0 [3]),
        .I1(y0_carry_i_5__0_n_0),
        .I2(addr[0]),
        .I3(y0_carry_i_6__0_n_0),
        .I4(Q),
        .O(S[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_20__0
       (.I0(\q_reg[24]_0 ),
        .I1(A[8]),
        .I2(addr[3]),
        .I3(\q_reg[19]_1 [0]),
        .I4(addr[4]),
        .I5(A[0]),
        .O(y0_carry_i_20__0_n_0));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry_i_2__1
       (.I0(\q_reg[3]_0 [2]),
        .I1(y0_carry_i_6__0_n_0),
        .I2(addr[0]),
        .I3(y0_carry_i_7__0_n_0),
        .I4(Q),
        .O(S[2]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry_i_3__1
       (.I0(\q_reg[3]_0 [1]),
        .I1(y0_carry_i_7__0_n_0),
        .I2(addr[0]),
        .I3(y0_carry_i_8__0_n_0),
        .I4(Q),
        .O(S[1]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry_i_4__1
       (.I0(\q_reg[3]_0 [0]),
        .I1(y0_carry_i_8__0_n_0),
        .I2(addr[0]),
        .I3(y0_carry_i_9__0_n_0),
        .I4(Q),
        .O(S[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_5__0
       (.I0(\q_reg[18]_0 ),
        .I1(y0_carry_i_11__0_n_0),
        .I2(addr[1]),
        .I3(y0_carry_i_12__0_n_0),
        .I4(addr[2]),
        .I5(y0_carry_i_13__0_n_0),
        .O(y0_carry_i_5__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_6__0
       (.I0(y0_carry_i_14__0_n_0),
        .I1(y0_carry_i_15__0_n_0),
        .I2(addr[1]),
        .I3(y0_carry_i_16__0_n_0),
        .I4(addr[2]),
        .I5(y0_carry_i_17__0_n_0),
        .O(y0_carry_i_6__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_7__0
       (.I0(y0_carry_i_12__0_n_0),
        .I1(y0_carry_i_13__0_n_0),
        .I2(addr[1]),
        .I3(y0_carry_i_11__0_n_0),
        .I4(addr[2]),
        .I5(y0_carry_i_18__0_n_0),
        .O(y0_carry_i_7__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_8__0
       (.I0(y0_carry_i_16__0_n_0),
        .I1(y0_carry_i_17__0_n_0),
        .I2(addr[1]),
        .I3(y0_carry_i_15__0_n_0),
        .I4(addr[2]),
        .I5(y0_carry_i_19__0_n_0),
        .O(y0_carry_i_8__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_9__0
       (.I0(y0_carry_i_11__0_n_0),
        .I1(y0_carry_i_18__0_n_0),
        .I2(addr[1]),
        .I3(y0_carry_i_13__0_n_0),
        .I4(addr[2]),
        .I5(y0_carry_i_20__0_n_0),
        .O(y0_carry_i_9__0_n_0));
endmodule

(* ORIG_REF_NAME = "reg" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg__parameterized1_1
   (\q_reg[1]_0 ,
    \q_reg[23]_0 ,
    \q_reg[19]_0 ,
    \q_reg[21]_0 ,
    \q_reg[23]_1 ,
    \q_reg[24]_0 ,
    \q_reg[15]_0 ,
    \q_reg[1]_1 ,
    \q_reg[23]_2 ,
    \q_reg[21]_1 ,
    \q_reg[19]_1 ,
    \q_reg[22]_0 ,
    \q_reg[18]_0 ,
    \q_reg[20]_0 ,
    \q_reg[14]_0 ,
    \q_reg[1]_2 ,
    \q_reg[22]_1 ,
    \q_reg[20]_1 ,
    \q_reg[11]_0 ,
    \q_reg[7]_0 ,
    \q_reg[3]_0 ,
    \q_reg[24]_1 ,
    S,
    \q_reg[7]_1 ,
    \q_reg[8]_0 ,
    \q_reg[16]_0 ,
    \q_reg[9]_0 ,
    \q_reg[15]_1 ,
    \q_reg[17]_0 ,
    \q_reg[23]_3 ,
    addr,
    \q_reg[24]_2 ,
    Q,
    A,
    \q_reg[19]_2 ,
    \q_reg[11]_1 ,
    \q_reg[11]_2 ,
    \q_reg[15]_2 ,
    \q_reg[15]_3 ,
    \q_reg[19]_3 ,
    \q_reg[19]_4 ,
    \q_reg[23]_4 ,
    \q_reg[0]_0 ,
    en_reg,
    \q_reg[24]_3 ,
    s00_axi_aclk,
    \q_reg[23]_5 ,
    \q_reg[19]_5 ,
    \q_reg[15]_4 ,
    \q_reg[11]_3 ,
    O,
    \q_reg[3]_1 );
  output \q_reg[1]_0 ;
  output \q_reg[23]_0 ;
  output \q_reg[19]_0 ;
  output \q_reg[21]_0 ;
  output [3:0]\q_reg[23]_1 ;
  output \q_reg[24]_0 ;
  output \q_reg[15]_0 ;
  output \q_reg[1]_1 ;
  output \q_reg[23]_2 ;
  output \q_reg[21]_1 ;
  output [3:0]\q_reg[19]_1 ;
  output \q_reg[22]_0 ;
  output \q_reg[18]_0 ;
  output \q_reg[20]_0 ;
  output [2:0]\q_reg[14]_0 ;
  output \q_reg[1]_2 ;
  output \q_reg[22]_1 ;
  output \q_reg[20]_1 ;
  output [3:0]\q_reg[11]_0 ;
  output [3:0]\q_reg[7]_0 ;
  output [3:0]\q_reg[3]_0 ;
  output [0:0]\q_reg[24]_1 ;
  output [3:0]S;
  output [3:0]\q_reg[7]_1 ;
  output [0:0]\q_reg[8]_0 ;
  output [0:0]\q_reg[16]_0 ;
  output [0:0]\q_reg[9]_0 ;
  output [0:0]\q_reg[15]_1 ;
  output [0:0]\q_reg[17]_0 ;
  output [0:0]\q_reg[23]_3 ;
  input [4:0]addr;
  input \q_reg[24]_2 ;
  input [0:0]Q;
  input [8:0]A;
  input [0:0]\q_reg[19]_2 ;
  input \q_reg[11]_1 ;
  input \q_reg[11]_2 ;
  input \q_reg[15]_2 ;
  input \q_reg[15]_3 ;
  input \q_reg[19]_3 ;
  input \q_reg[19]_4 ;
  input \q_reg[23]_4 ;
  input \q_reg[0]_0 ;
  input en_reg;
  input [0:0]\q_reg[24]_3 ;
  input s00_axi_aclk;
  input [3:0]\q_reg[23]_5 ;
  input [3:0]\q_reg[19]_5 ;
  input [3:0]\q_reg[15]_4 ;
  input [3:0]\q_reg[11]_3 ;
  input [3:0]O;
  input [3:0]\q_reg[3]_1 ;

  wire [8:0]A;
  wire [3:0]O;
  wire [0:0]Q;
  wire [3:0]S;
  wire [4:0]addr;
  wire en_reg;
  wire \q_reg[0]_0 ;
  wire [3:0]\q_reg[11]_0 ;
  wire \q_reg[11]_1 ;
  wire \q_reg[11]_2 ;
  wire [3:0]\q_reg[11]_3 ;
  wire [2:0]\q_reg[14]_0 ;
  wire \q_reg[15]_0 ;
  wire [0:0]\q_reg[15]_1 ;
  wire \q_reg[15]_2 ;
  wire \q_reg[15]_3 ;
  wire [3:0]\q_reg[15]_4 ;
  wire [0:0]\q_reg[16]_0 ;
  wire [0:0]\q_reg[17]_0 ;
  wire \q_reg[18]_0 ;
  wire \q_reg[19]_0 ;
  wire [3:0]\q_reg[19]_1 ;
  wire [0:0]\q_reg[19]_2 ;
  wire \q_reg[19]_3 ;
  wire \q_reg[19]_4 ;
  wire [3:0]\q_reg[19]_5 ;
  wire \q_reg[1]_0 ;
  wire \q_reg[1]_1 ;
  wire \q_reg[1]_2 ;
  wire \q_reg[20]_0 ;
  wire \q_reg[20]_1 ;
  wire \q_reg[21]_0 ;
  wire \q_reg[21]_1 ;
  wire \q_reg[22]_0 ;
  wire \q_reg[22]_1 ;
  wire \q_reg[23]_0 ;
  wire [3:0]\q_reg[23]_1 ;
  wire \q_reg[23]_2 ;
  wire [0:0]\q_reg[23]_3 ;
  wire \q_reg[23]_4 ;
  wire [3:0]\q_reg[23]_5 ;
  wire \q_reg[24]_0 ;
  wire [0:0]\q_reg[24]_1 ;
  wire \q_reg[24]_2 ;
  wire [0:0]\q_reg[24]_3 ;
  wire [3:0]\q_reg[3]_0 ;
  wire [3:0]\q_reg[3]_1 ;
  wire [3:0]\q_reg[7]_0 ;
  wire [3:0]\q_reg[7]_1 ;
  wire [0:0]\q_reg[8]_0 ;
  wire [0:0]\q_reg[9]_0 ;
  wire s00_axi_aclk;
  wire y0_carry__0_i_5_n_0;
  wire y0_carry__0_i_6_n_0;
  wire y0_carry__0_i_7_n_0;
  wire y0_carry__0_i_8_n_0;
  wire y0_carry_i_11_n_0;
  wire y0_carry_i_12_n_0;
  wire y0_carry_i_13_n_0;
  wire y0_carry_i_14_n_0;
  wire y0_carry_i_15_n_0;
  wire y0_carry_i_16_n_0;
  wire y0_carry_i_17_n_0;
  wire y0_carry_i_18_n_0;
  wire y0_carry_i_19_n_0;
  wire y0_carry_i_20_n_0;
  wire y0_carry_i_5_n_0;
  wire y0_carry_i_6_n_0;
  wire y0_carry_i_7_n_0;
  wire y0_carry_i_8_n_0;
  wire y0_carry_i_9_n_0;

  FDRE \q_reg[0] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[3]_1 [0]),
        .Q(\q_reg[3]_0 [0]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[10] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[11]_3 [2]),
        .Q(\q_reg[11]_0 [2]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[11] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[11]_3 [3]),
        .Q(\q_reg[11]_0 [3]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[12] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[15]_4 [0]),
        .Q(\q_reg[14]_0 [0]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[13] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[15]_4 [1]),
        .Q(\q_reg[14]_0 [1]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[14] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[15]_4 [2]),
        .Q(\q_reg[14]_0 [2]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[15] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[15]_4 [3]),
        .Q(\q_reg[15]_0 ),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[16] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[19]_5 [0]),
        .Q(\q_reg[19]_1 [0]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[17] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[19]_5 [1]),
        .Q(\q_reg[19]_1 [1]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[18] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[19]_5 [2]),
        .Q(\q_reg[19]_1 [2]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[19] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[19]_5 [3]),
        .Q(\q_reg[19]_1 [3]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[1] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[3]_1 [1]),
        .Q(\q_reg[3]_0 [1]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[20] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_5 [0]),
        .Q(\q_reg[23]_1 [0]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[21] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_5 [1]),
        .Q(\q_reg[23]_1 [1]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[22] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_5 [2]),
        .Q(\q_reg[23]_1 [2]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[23] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_5 [3]),
        .Q(\q_reg[23]_1 [3]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[24] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[24]_3 ),
        .Q(\q_reg[24]_0 ),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[2] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[3]_1 [2]),
        .Q(\q_reg[3]_0 [2]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[3] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[3]_1 [3]),
        .Q(\q_reg[3]_0 [3]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[4] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(O[0]),
        .Q(\q_reg[7]_0 [0]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[5] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(O[1]),
        .Q(\q_reg[7]_0 [1]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[6] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(O[2]),
        .Q(\q_reg[7]_0 [2]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[7] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(O[3]),
        .Q(\q_reg[7]_0 [3]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[8] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[11]_3 [0]),
        .Q(\q_reg[11]_0 [0]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[9] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[11]_3 [1]),
        .Q(\q_reg[11]_0 [1]),
        .R(\q_reg[0]_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_10
       (.I0(\q_reg[23]_1 [0]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(\q_reg[14]_0 [0]),
        .O(\q_reg[20]_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_11
       (.I0(\q_reg[23]_1 [1]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(\q_reg[14]_0 [1]),
        .O(\q_reg[21]_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_12
       (.I0(\q_reg[19]_1 [3]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(\q_reg[11]_0 [3]),
        .O(\q_reg[19]_0 ));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__0_i_1__0
       (.I0(A[7]),
        .I1(y0_carry__0_i_5_n_0),
        .I2(addr[0]),
        .I3(y0_carry__0_i_6_n_0),
        .I4(Q),
        .O(\q_reg[7]_1 [3]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__0_i_2__0
       (.I0(A[6]),
        .I1(y0_carry__0_i_6_n_0),
        .I2(addr[0]),
        .I3(y0_carry__0_i_7_n_0),
        .I4(Q),
        .O(\q_reg[7]_1 [2]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__0_i_3__0
       (.I0(A[5]),
        .I1(y0_carry__0_i_7_n_0),
        .I2(addr[0]),
        .I3(y0_carry__0_i_8_n_0),
        .I4(Q),
        .O(\q_reg[7]_1 [1]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__0_i_4__0
       (.I0(A[4]),
        .I1(y0_carry__0_i_8_n_0),
        .I2(addr[0]),
        .I3(y0_carry_i_5_n_0),
        .I4(Q),
        .O(\q_reg[7]_1 [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_5
       (.I0(\q_reg[22]_0 ),
        .I1(\q_reg[18]_0 ),
        .I2(addr[1]),
        .I3(\q_reg[20]_0 ),
        .I4(addr[2]),
        .I5(y0_carry_i_12_n_0),
        .O(y0_carry__0_i_5_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_6
       (.I0(\q_reg[21]_0 ),
        .I1(y0_carry_i_14_n_0),
        .I2(addr[1]),
        .I3(\q_reg[19]_0 ),
        .I4(addr[2]),
        .I5(y0_carry_i_16_n_0),
        .O(y0_carry__0_i_6_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_7
       (.I0(\q_reg[20]_0 ),
        .I1(y0_carry_i_12_n_0),
        .I2(addr[1]),
        .I3(\q_reg[18]_0 ),
        .I4(addr[2]),
        .I5(y0_carry_i_11_n_0),
        .O(y0_carry__0_i_7_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_8
       (.I0(\q_reg[19]_0 ),
        .I1(y0_carry_i_16_n_0),
        .I2(addr[1]),
        .I3(y0_carry_i_14_n_0),
        .I4(addr[2]),
        .I5(y0_carry_i_15_n_0),
        .O(y0_carry__0_i_8_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_9
       (.I0(\q_reg[23]_1 [2]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(\q_reg[14]_0 [2]),
        .O(\q_reg[22]_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__1_i_12
       (.I0(\q_reg[23]_1 [3]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(\q_reg[15]_0 ),
        .O(\q_reg[23]_0 ));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__1_i_3__1
       (.I0(\q_reg[11]_0 [1]),
        .I1(\q_reg[11]_1 ),
        .I2(addr[0]),
        .I3(\q_reg[11]_2 ),
        .I4(Q),
        .O(\q_reg[9]_0 ));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__1_i_4__0
       (.I0(A[8]),
        .I1(\q_reg[1]_0 ),
        .I2(addr[0]),
        .I3(y0_carry__0_i_5_n_0),
        .I4(Q),
        .O(\q_reg[8]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_8
       (.I0(\q_reg[23]_0 ),
        .I1(\q_reg[19]_0 ),
        .I2(addr[1]),
        .I3(\q_reg[21]_0 ),
        .I4(addr[2]),
        .I5(y0_carry_i_14_n_0),
        .O(\q_reg[1]_0 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__2_i_10
       (.I0(\q_reg[23]_1 [0]),
        .I1(addr[2]),
        .I2(addr[3]),
        .I3(\q_reg[24]_0 ),
        .I4(addr[4]),
        .I5(\q_reg[19]_1 [0]),
        .O(\q_reg[20]_1 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__2_i_11
       (.I0(\q_reg[23]_1 [1]),
        .I1(addr[2]),
        .I2(addr[3]),
        .I3(\q_reg[24]_0 ),
        .I4(addr[4]),
        .I5(\q_reg[19]_1 [1]),
        .O(\q_reg[21]_1 ));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__2_i_1__1
       (.I0(\q_reg[15]_0 ),
        .I1(\q_reg[15]_2 ),
        .I2(addr[0]),
        .I3(\q_reg[15]_3 ),
        .I4(Q),
        .O(\q_reg[15]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__2_i_5
       (.I0(\q_reg[22]_1 ),
        .I1(addr[1]),
        .I2(\q_reg[20]_1 ),
        .O(\q_reg[1]_2 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__2_i_9
       (.I0(\q_reg[23]_1 [2]),
        .I1(addr[2]),
        .I2(addr[3]),
        .I3(\q_reg[24]_0 ),
        .I4(addr[4]),
        .I5(\q_reg[19]_1 [2]),
        .O(\q_reg[22]_1 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__3_i_12
       (.I0(\q_reg[23]_1 [3]),
        .I1(addr[2]),
        .I2(addr[3]),
        .I3(\q_reg[24]_0 ),
        .I4(addr[4]),
        .I5(\q_reg[19]_1 [3]),
        .O(\q_reg[23]_2 ));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__3_i_3__1
       (.I0(\q_reg[19]_1 [1]),
        .I1(\q_reg[19]_3 ),
        .I2(addr[0]),
        .I3(\q_reg[19]_4 ),
        .I4(Q),
        .O(\q_reg[17]_0 ));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__3_i_4__0
       (.I0(\q_reg[19]_2 ),
        .I1(\q_reg[1]_1 ),
        .I2(addr[0]),
        .I3(\q_reg[1]_2 ),
        .I4(Q),
        .O(\q_reg[16]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_8
       (.I0(\q_reg[23]_2 ),
        .I1(addr[1]),
        .I2(\q_reg[21]_1 ),
        .O(\q_reg[1]_1 ));
  LUT6 #(
    .INIT(64'hAAA955595556AAA6)) 
    y0_carry__4_i_1__1
       (.I0(\q_reg[23]_1 [3]),
        .I1(\q_reg[23]_4 ),
        .I2(addr[0]),
        .I3(addr[1]),
        .I4(\q_reg[24]_2 ),
        .I5(Q),
        .O(\q_reg[23]_3 ));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__5_i_1
       (.I0(\q_reg[24]_0 ),
        .I1(\q_reg[24]_2 ),
        .I2(Q),
        .O(\q_reg[24]_1 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry_i_10
       (.I0(\q_reg[19]_1 [2]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(\q_reg[11]_0 [2]),
        .O(\q_reg[18]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_11
       (.I0(\q_reg[24]_0 ),
        .I1(\q_reg[14]_0 [2]),
        .I2(addr[3]),
        .I3(\q_reg[23]_1 [2]),
        .I4(addr[4]),
        .I5(\q_reg[7]_0 [2]),
        .O(y0_carry_i_11_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry_i_12
       (.I0(\q_reg[19]_1 [0]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(\q_reg[11]_0 [0]),
        .O(y0_carry_i_12_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_13
       (.I0(\q_reg[24]_0 ),
        .I1(\q_reg[14]_0 [0]),
        .I2(addr[3]),
        .I3(\q_reg[23]_1 [0]),
        .I4(addr[4]),
        .I5(\q_reg[7]_0 [0]),
        .O(y0_carry_i_13_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry_i_14
       (.I0(\q_reg[19]_1 [1]),
        .I1(addr[3]),
        .I2(\q_reg[24]_0 ),
        .I3(addr[4]),
        .I4(\q_reg[11]_0 [1]),
        .O(y0_carry_i_14_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_15
       (.I0(\q_reg[24]_0 ),
        .I1(\q_reg[14]_0 [1]),
        .I2(addr[3]),
        .I3(\q_reg[23]_1 [1]),
        .I4(addr[4]),
        .I5(\q_reg[7]_0 [1]),
        .O(y0_carry_i_15_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_16
       (.I0(\q_reg[24]_0 ),
        .I1(\q_reg[15]_0 ),
        .I2(addr[3]),
        .I3(\q_reg[23]_1 [3]),
        .I4(addr[4]),
        .I5(\q_reg[7]_0 [3]),
        .O(y0_carry_i_16_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_17
       (.I0(\q_reg[24]_0 ),
        .I1(\q_reg[11]_0 [3]),
        .I2(addr[3]),
        .I3(\q_reg[19]_1 [3]),
        .I4(addr[4]),
        .I5(\q_reg[3]_0 [3]),
        .O(y0_carry_i_17_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_18
       (.I0(\q_reg[24]_0 ),
        .I1(\q_reg[11]_0 [2]),
        .I2(addr[3]),
        .I3(\q_reg[19]_1 [2]),
        .I4(addr[4]),
        .I5(\q_reg[3]_0 [2]),
        .O(y0_carry_i_18_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_19
       (.I0(\q_reg[24]_0 ),
        .I1(\q_reg[11]_0 [1]),
        .I2(addr[3]),
        .I3(\q_reg[19]_1 [1]),
        .I4(addr[4]),
        .I5(\q_reg[3]_0 [1]),
        .O(y0_carry_i_19_n_0));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry_i_1__0
       (.I0(A[3]),
        .I1(y0_carry_i_5_n_0),
        .I2(addr[0]),
        .I3(y0_carry_i_6_n_0),
        .I4(Q),
        .O(S[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_20
       (.I0(\q_reg[24]_0 ),
        .I1(\q_reg[11]_0 [0]),
        .I2(addr[3]),
        .I3(\q_reg[19]_1 [0]),
        .I4(addr[4]),
        .I5(\q_reg[3]_0 [0]),
        .O(y0_carry_i_20_n_0));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry_i_2__0
       (.I0(A[2]),
        .I1(y0_carry_i_6_n_0),
        .I2(addr[0]),
        .I3(y0_carry_i_7_n_0),
        .I4(Q),
        .O(S[2]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry_i_3__0
       (.I0(A[1]),
        .I1(y0_carry_i_7_n_0),
        .I2(addr[0]),
        .I3(y0_carry_i_8_n_0),
        .I4(Q),
        .O(S[1]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry_i_4__0
       (.I0(A[0]),
        .I1(y0_carry_i_8_n_0),
        .I2(addr[0]),
        .I3(y0_carry_i_9_n_0),
        .I4(Q),
        .O(S[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_5
       (.I0(\q_reg[18]_0 ),
        .I1(y0_carry_i_11_n_0),
        .I2(addr[1]),
        .I3(y0_carry_i_12_n_0),
        .I4(addr[2]),
        .I5(y0_carry_i_13_n_0),
        .O(y0_carry_i_5_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_6
       (.I0(y0_carry_i_14_n_0),
        .I1(y0_carry_i_15_n_0),
        .I2(addr[1]),
        .I3(y0_carry_i_16_n_0),
        .I4(addr[2]),
        .I5(y0_carry_i_17_n_0),
        .O(y0_carry_i_6_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_7
       (.I0(y0_carry_i_12_n_0),
        .I1(y0_carry_i_13_n_0),
        .I2(addr[1]),
        .I3(y0_carry_i_11_n_0),
        .I4(addr[2]),
        .I5(y0_carry_i_18_n_0),
        .O(y0_carry_i_7_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_8
       (.I0(y0_carry_i_16_n_0),
        .I1(y0_carry_i_17_n_0),
        .I2(addr[1]),
        .I3(y0_carry_i_15_n_0),
        .I4(addr[2]),
        .I5(y0_carry_i_19_n_0),
        .O(y0_carry_i_8_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_9
       (.I0(y0_carry_i_11_n_0),
        .I1(y0_carry_i_18_n_0),
        .I2(addr[1]),
        .I3(y0_carry_i_13_n_0),
        .I4(addr[2]),
        .I5(y0_carry_i_20_n_0),
        .O(y0_carry_i_9_n_0));
endmodule

(* ORIG_REF_NAME = "reg" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg__parameterized3
   (axi_wready_reg,
    sel0_out,
    Q,
    \q_reg[23]_0 ,
    S,
    \q_reg[7]_0 ,
    \q_reg[11]_0 ,
    \q_reg[15]_0 ,
    \q_reg[19]_0 ,
    \q_reg[22]_0 ,
    \q_reg[0]_0 ,
    \q_reg[0]_1 ,
    s00_axi_wvalid,
    s00_axi_awvalid,
    p_0_in,
    \q_reg[24] ,
    \q_reg[24]_0 ,
    out_ROM,
    en_reg,
    \q_reg[23]_1 ,
    s00_axi_aclk);
  output axi_wready_reg;
  output sel0_out;
  output [23:0]Q;
  output [0:0]\q_reg[23]_0 ;
  output [3:0]S;
  output [3:0]\q_reg[7]_0 ;
  output [3:0]\q_reg[11]_0 ;
  output [3:0]\q_reg[15]_0 ;
  output [3:0]\q_reg[19]_0 ;
  output [2:0]\q_reg[22]_0 ;
  input \q_reg[0]_0 ;
  input \q_reg[0]_1 ;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [0:0]p_0_in;
  input [0:0]\q_reg[24] ;
  input \q_reg[24]_0 ;
  input [22:0]out_ROM;
  input en_reg;
  input [23:0]\q_reg[23]_1 ;
  input s00_axi_aclk;

  wire [23:0]Q;
  wire [3:0]S;
  wire axi_wready_reg;
  wire en_reg;
  wire [22:0]out_ROM;
  wire [0:0]p_0_in;
  wire \q_reg[0]_0 ;
  wire \q_reg[0]_1 ;
  wire [3:0]\q_reg[11]_0 ;
  wire [3:0]\q_reg[15]_0 ;
  wire [3:0]\q_reg[19]_0 ;
  wire [2:0]\q_reg[22]_0 ;
  wire [0:0]\q_reg[23]_0 ;
  wire [23:0]\q_reg[23]_1 ;
  wire [0:0]\q_reg[24] ;
  wire \q_reg[24]_0 ;
  wire [3:0]\q_reg[7]_0 ;
  wire s00_axi_aclk;
  wire s00_axi_awvalid;
  wire s00_axi_wvalid;
  wire sel0_out;

  LUT5 #(
    .INIT(32'h80000000)) 
    \FSM_sequential_state[0]_i_2 
       (.I0(\q_reg[0]_0 ),
        .I1(\q_reg[0]_1 ),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(p_0_in),
        .O(axi_wready_reg));
  FDRE \q_reg[0] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [0]),
        .Q(Q[0]),
        .R(1'b0));
  FDRE \q_reg[10] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [10]),
        .Q(Q[10]),
        .R(1'b0));
  FDRE \q_reg[11] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [11]),
        .Q(Q[11]),
        .R(1'b0));
  FDRE \q_reg[12] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [12]),
        .Q(Q[12]),
        .R(1'b0));
  FDRE \q_reg[13] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [13]),
        .Q(Q[13]),
        .R(1'b0));
  FDRE \q_reg[14] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [14]),
        .Q(Q[14]),
        .R(1'b0));
  FDRE \q_reg[15] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [15]),
        .Q(Q[15]),
        .R(1'b0));
  FDRE \q_reg[16] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [16]),
        .Q(Q[16]),
        .R(1'b0));
  FDRE \q_reg[17] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [17]),
        .Q(Q[17]),
        .R(1'b0));
  FDRE \q_reg[18] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [18]),
        .Q(Q[18]),
        .R(1'b0));
  FDRE \q_reg[19] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [19]),
        .Q(Q[19]),
        .R(1'b0));
  FDRE \q_reg[1] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [1]),
        .Q(Q[1]),
        .R(1'b0));
  FDRE \q_reg[20] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [20]),
        .Q(Q[20]),
        .R(1'b0));
  FDRE \q_reg[21] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [21]),
        .Q(Q[21]),
        .R(1'b0));
  FDRE \q_reg[22] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [22]),
        .Q(Q[22]),
        .R(1'b0));
  FDRE \q_reg[23] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [23]),
        .Q(Q[23]),
        .R(1'b0));
  FDRE \q_reg[2] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [2]),
        .Q(Q[2]),
        .R(1'b0));
  FDRE \q_reg[3] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [3]),
        .Q(Q[3]),
        .R(1'b0));
  FDRE \q_reg[4] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [4]),
        .Q(Q[4]),
        .R(1'b0));
  FDRE \q_reg[5] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [5]),
        .Q(Q[5]),
        .R(1'b0));
  FDRE \q_reg[6] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [6]),
        .Q(Q[6]),
        .R(1'b0));
  FDRE \q_reg[7] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [7]),
        .Q(Q[7]),
        .R(1'b0));
  FDRE \q_reg[8] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [8]),
        .Q(Q[8]),
        .R(1'b0));
  FDRE \q_reg[9] 
       (.C(s00_axi_aclk),
        .CE(en_reg),
        .D(\q_reg[23]_1 [9]),
        .Q(Q[9]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__0_i_1
       (.I0(Q[7]),
        .I1(Q[23]),
        .I2(out_ROM[7]),
        .O(\q_reg[7]_0 [3]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__0_i_2
       (.I0(Q[6]),
        .I1(Q[23]),
        .I2(out_ROM[6]),
        .O(\q_reg[7]_0 [2]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__0_i_3
       (.I0(Q[5]),
        .I1(Q[23]),
        .I2(out_ROM[5]),
        .O(\q_reg[7]_0 [1]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__0_i_4
       (.I0(Q[4]),
        .I1(Q[23]),
        .I2(out_ROM[4]),
        .O(\q_reg[7]_0 [0]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__1_i_1
       (.I0(Q[11]),
        .I1(Q[23]),
        .I2(out_ROM[11]),
        .O(\q_reg[11]_0 [3]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__1_i_2
       (.I0(Q[10]),
        .I1(Q[23]),
        .I2(out_ROM[10]),
        .O(\q_reg[11]_0 [2]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__1_i_3
       (.I0(Q[9]),
        .I1(Q[23]),
        .I2(out_ROM[9]),
        .O(\q_reg[11]_0 [1]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__1_i_4
       (.I0(Q[8]),
        .I1(Q[23]),
        .I2(out_ROM[8]),
        .O(\q_reg[11]_0 [0]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__2_i_1
       (.I0(Q[15]),
        .I1(Q[23]),
        .I2(out_ROM[15]),
        .O(\q_reg[15]_0 [3]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__2_i_2
       (.I0(Q[14]),
        .I1(Q[23]),
        .I2(out_ROM[14]),
        .O(\q_reg[15]_0 [2]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__2_i_3
       (.I0(Q[13]),
        .I1(Q[23]),
        .I2(out_ROM[13]),
        .O(\q_reg[15]_0 [1]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__2_i_4
       (.I0(Q[12]),
        .I1(Q[23]),
        .I2(out_ROM[12]),
        .O(\q_reg[15]_0 [0]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__3_i_1
       (.I0(Q[19]),
        .I1(Q[23]),
        .I2(out_ROM[19]),
        .O(\q_reg[19]_0 [3]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__3_i_2
       (.I0(Q[18]),
        .I1(Q[23]),
        .I2(out_ROM[18]),
        .O(\q_reg[19]_0 [2]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__3_i_3
       (.I0(Q[17]),
        .I1(Q[23]),
        .I2(out_ROM[17]),
        .O(\q_reg[19]_0 [1]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__3_i_4
       (.I0(Q[16]),
        .I1(Q[23]),
        .I2(out_ROM[16]),
        .O(\q_reg[19]_0 [0]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__4_i_2
       (.I0(Q[22]),
        .I1(Q[23]),
        .I2(out_ROM[22]),
        .O(\q_reg[22]_0 [2]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__4_i_3
       (.I0(Q[21]),
        .I1(Q[23]),
        .I2(out_ROM[21]),
        .O(\q_reg[22]_0 [1]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__4_i_4
       (.I0(Q[20]),
        .I1(Q[23]),
        .I2(out_ROM[20]),
        .O(\q_reg[22]_0 [0]));
  LUT3 #(
    .INIT(8'h96)) 
    y0_carry__5_i_1__0
       (.I0(Q[23]),
        .I1(\q_reg[24] ),
        .I2(\q_reg[24]_0 ),
        .O(\q_reg[23]_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    y0_carry_i_1
       (.I0(Q[23]),
        .O(sel0_out));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry_i_2
       (.I0(Q[3]),
        .I1(Q[23]),
        .I2(out_ROM[3]),
        .O(S[3]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry_i_3
       (.I0(Q[2]),
        .I1(Q[23]),
        .I2(out_ROM[2]),
        .O(S[2]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry_i_4
       (.I0(Q[1]),
        .I1(Q[23]),
        .I2(out_ROM[1]),
        .O(S[1]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry_i_5__1
       (.I0(Q[0]),
        .I1(Q[23]),
        .I2(out_ROM[0]),
        .O(S[0]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
