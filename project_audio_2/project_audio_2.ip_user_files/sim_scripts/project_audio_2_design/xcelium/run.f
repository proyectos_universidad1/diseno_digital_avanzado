-makelib xcelium_lib/xilinx_vip -sv \
  "/tools/Xilinx/Vivado/2022.2/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
  "/tools/Xilinx/Vivado/2022.2/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
  "/tools/Xilinx/Vivado/2022.2/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
  "/tools/Xilinx/Vivado/2022.2/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
  "/tools/Xilinx/Vivado/2022.2/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
  "/tools/Xilinx/Vivado/2022.2/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
  "/tools/Xilinx/Vivado/2022.2/data/xilinx_vip/hdl/axi_vip_if.sv" \
  "/tools/Xilinx/Vivado/2022.2/data/xilinx_vip/hdl/clk_vip_if.sv" \
  "/tools/Xilinx/Vivado/2022.2/data/xilinx_vip/hdl/rst_vip_if.sv" \
-endlib
-makelib xcelium_lib/xpm -sv \
  "/tools/Xilinx/Vivado/2022.2/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
  "/tools/Xilinx/Vivado/2022.2/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
-endlib
-makelib xcelium_lib/xpm \
  "/tools/Xilinx/Vivado/2022.2/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib xcelium_lib/axi_infrastructure_v1_1_0 \
  "../../../../project_audio_2.gen/sources_1/bd/project_audio_2_design/ipshared/ec67/hdl/axi_infrastructure_v1_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/axi_vip_v1_1_13 -sv \
  "../../../../project_audio_2.gen/sources_1/bd/project_audio_2_design/ipshared/ffc2/hdl/axi_vip_v1_1_vl_rfs.sv" \
-endlib
-makelib xcelium_lib/processing_system7_vip_v1_0_15 -sv \
  "../../../../project_audio_2.gen/sources_1/bd/project_audio_2_design/ipshared/ee60/hdl/processing_system7_vip_v1_0_vl_rfs.sv" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/project_audio_2_design/ip/project_audio_2_design_processing_system7_0_0/sim/project_audio_2_design_processing_system7_0_0.v" \
-endlib
-makelib xcelium_lib/lib_cdc_v1_0_2 \
  "../../../../project_audio_2.gen/sources_1/bd/project_audio_2_design/ipshared/ef1e/hdl/lib_cdc_v1_0_rfs.vhd" \
-endlib
-makelib xcelium_lib/proc_sys_reset_v5_0_13 \
  "../../../../project_audio_2.gen/sources_1/bd/project_audio_2_design/ipshared/8842/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/project_audio_2_design/ip/project_audio_2_design_rst_ps7_0_100M_0/sim/project_audio_2_design_rst_ps7_0_100M_0.vhd" \
  "../../../bd/project_audio_2_design/ipshared/d48b/src/Mux_2_1.vhd" \
  "../../../bd/project_audio_2_design/ipshared/d48b/src/ROM.vhd" \
  "../../../bd/project_audio_2_design/ipshared/d48b/src/add_sub.vhd" \
  "../../../bd/project_audio_2_design/ipshared/d48b/src/barrel_shifter.vhd" \
  "../../../bd/project_audio_2_design/ipshared/d48b/src/control_path.vhd" \
  "../../../bd/project_audio_2_design/project_audio_2/project_audio_2.srcs/sources_1/new/cordic_rotation.vhd" \
  "../../../bd/project_audio_2_design/ipshared/d48b/src/data_path.vhd" \
  "../../../bd/project_audio_2_design/project_audio_2/project_audio_2.srcs/sources_1/new/fsm.vhd" \
  "../../../bd/project_audio_2_design/ipshared/d48b/hdl/project_audio_2_ip_v1_0_S00_AXI.vhd" \
  "../../../bd/project_audio_2_design/ipshared/d48b/src/reg.vhd" \
  "../../../bd/project_audio_2_design/ipshared/d48b/hdl/project_audio_2_ip_v1_0.vhd" \
  "../../../bd/project_audio_2_design/ip/project_audio_2_design_project_audio_2_ip_0_0/sim/project_audio_2_design_project_audio_2_ip_0_0.vhd" \
-endlib
-makelib xcelium_lib/generic_baseblocks_v2_1_0 \
  "../../../../project_audio_2.gen/sources_1/bd/project_audio_2_design/ipshared/b752/hdl/generic_baseblocks_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_7 \
  "../../../../project_audio_2.gen/sources_1/bd/project_audio_2_design/ipshared/83df/simulation/fifo_generator_vlog_beh.v" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_7 \
  "../../../../project_audio_2.gen/sources_1/bd/project_audio_2_design/ipshared/83df/hdl/fifo_generator_v13_2_rfs.vhd" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_7 \
  "../../../../project_audio_2.gen/sources_1/bd/project_audio_2_design/ipshared/83df/hdl/fifo_generator_v13_2_rfs.v" \
-endlib
-makelib xcelium_lib/axi_data_fifo_v2_1_26 \
  "../../../../project_audio_2.gen/sources_1/bd/project_audio_2_design/ipshared/3111/hdl/axi_data_fifo_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/axi_register_slice_v2_1_27 \
  "../../../../project_audio_2.gen/sources_1/bd/project_audio_2_design/ipshared/f0b4/hdl/axi_register_slice_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/axi_protocol_converter_v2_1_27 \
  "../../../../project_audio_2.gen/sources_1/bd/project_audio_2_design/ipshared/aeb3/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/project_audio_2_design/ip/project_audio_2_design_auto_pc_0/sim/project_audio_2_design_auto_pc_0.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/project_audio_2_design/sim/project_audio_2_design.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  glbl.v
-endlib

