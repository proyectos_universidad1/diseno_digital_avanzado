----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/17/2023 02:24:35 AM
-- Design Name: 
-- Module Name: barrel_shifter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity barrel_shifter is
    generic(
        N_shift : integer := 5;
        N_input : integer := 24
    );
    
    Port ( 
        input : in std_logic_vector(N_input-1 downto 0);
        shift : in std_logic_vector(N_shift-1 downto 0);
        output: out std_logic_vector(N_input-1 downto 0)
    );
end barrel_shifter;

architecture Behavioral of barrel_shifter is
begin
    output <= to_stdlogicvector(to_bitvector(std_logic_vector(input)) sra to_integer(unsigned(shift)));
end Behavioral;
