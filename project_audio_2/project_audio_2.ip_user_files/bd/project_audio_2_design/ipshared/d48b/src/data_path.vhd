----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/25/2023 03:01:52 PM
-- Design Name: 
-- Module Name: data_path - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity data_path is 
    generic (N: integer := 25);
    Port ( 
        sel_mux, clk, en_reg, rst: in std_logic;
        addr: in  std_logic_vector(4 downto 0);
        X0, Y0: in std_logic_vector(N-1 downto 0);
        Z0: in std_logic_vector(N-2 downto 0);
        Xn, Yn: out std_logic_vector(N-1 downto 0);
        Zn: out std_logic_vector(N-2 downto 0)
    );
end data_path;

architecture Behavioral of data_path is

-- Add component for mux 2:1
component Mux_2_1 is 
    generic (N: integer := 8); 

    Port (
        in0, in1: in std_logic_vector(N-1 downto 0);
        sel: in std_logic; 
        y: out std_logic_vector(N-1 downto 0)
    
     );
end component;

-- Add component for register
component reg is
	generic (N	:integer:=8 );
	
    Port(
        clk: in 		std_logic;
        en: in std_logic;
        rst: in           std_logic;
        clr: in           std_logic;
        d: in 		std_logic_vector(N-1 downto 0);
        q: out	std_logic_vector(N-1 downto 0)
        );
end component;

-- Add component for ROM
component ROM is 
    Port(
    clk  : in  std_logic;
    addr : in  std_logic_vector(4 downto 0);
    data : out std_logic_vector(23 downto 0)
);

end component;

-- Add component for add - sub
component add_sub is
    generic(N : integer := 24);
    
    port(
        a, b: in std_logic_vector(N-1 downto 0);
        s : out std_logic_vector(N-1 downto 0);
        sel: in std_logic
    );
     
end component;

-- Add component for barrel_shifter
component barrel_shifter is
    generic(
        N_shift : integer := 5;
        N_input : integer := 24
    );
    
    Port ( 
        input : in std_logic_vector(N_input-1 downto 0);
        shift : in std_logic_vector(N_shift-1 downto 0);
        output: out std_logic_vector(N_input-1 downto 0)
    );
end component;

signal out_mux_x, out_mux_y, out_reg_x, out_reg_y: std_logic_vector(N-1 downto 0);
signal out_barrel_x, out_barrel_y, out_add_sub_x, out_add_sub_y: std_logic_vector(N-1 downto 0);
signal out_mux_z, out_reg_z, out_add_sub_z, out_ROM: std_logic_vector(N-2 downto 0);
signal not_sel_barrel_shifter, sel_barrel_shifter: std_logic;
--signal X0_extend_sign, Y0_extend_sign: std_logic_vector(N-1 downto 0);

begin

-- Instantiate multiplexors to x0, y0, z0 ------------------------
Mux_X : Mux_2_1
generic map(N => N)
Port map(
    in0 => out_add_sub_x,
    in1 => X0,
    sel => sel_mux,
    y => out_mux_x
);

Mux_Y : Mux_2_1
generic map(N => N)
Port map(
    in0 => out_add_sub_y,
    in1 => Y0,
    sel => sel_mux,
    y => out_mux_y
);

Mux_Z : Mux_2_1
generic map(N => N-1)
Port map(
    in0 => out_add_sub_z,
    in1 => Z0,
    sel => sel_mux,
    y => out_mux_z
);
---------------------------------------------------------------------

-- Instantiate registers to x, y, z ---------------------------------

reg_X : reg
generic map(N => N)
Port map(
    clk => clk,
    en => en_reg,
    rst => rst,
    clr => '0',
    d => out_mux_x,
    q => out_reg_x
);

reg_Y : reg
generic map(N => N)
Port map(
    clk => clk,
    en => en_reg,
    rst => rst,
    clr => '0',
    d => out_mux_y,
    q => out_reg_y
);

reg_Z : reg
generic map(N => N-1)
Port map(
    clk => clk,
    en => en_reg,
    rst => rst,
    clr => '0',
    d => out_mux_z,
    q => out_reg_z
);
---------------------------------------------------------------------

-- Intance the ROM---------------------------------------------------

ROM_1: ROM
Port map(
    clk => clk,
    addr => addr,
    data => out_ROM
);
---------------------------------------------------------------------

-- Instance barrel shifter the X and Y ------------------------------
barrel_shifter_X: barrel_shifter
generic map(
        N_shift => 5,
        N_input => N
    )
Port map( 
    input => out_reg_y,
    shift => addr,
    output => out_barrel_x
);

barrel_shifter_Y: barrel_shifter
generic map(
        N_shift => 5,
        N_input => N
    )
Port map( 
    input => out_reg_x,
    shift => addr,
    output => out_barrel_y
);

---------------------------------------------------------------------

-- Instance add sub the X, Y and Z ----------------------------------

add_sub_X: add_sub
generic map(N => N)
port map(
    a => out_reg_x, 
    b => out_barrel_x,
    sel => not_sel_barrel_shifter,
    s => out_add_sub_x
);

add_sub_Y: add_sub
generic map(N => N)
port map(
    a => out_reg_y,
    b => out_barrel_y,
    sel => sel_barrel_shifter,
    s => out_add_sub_y
);

add_sub_Z: add_sub
generic map(N => N-1)
port map(
    a => out_reg_z, 
    b => out_ROM,
    sel => not_sel_barrel_shifter,
    s => out_add_sub_z
);
----------------------------------------------------------------------

-- Asignation of selectors barrel shifter

sel_barrel_shifter <= out_reg_z(N-2);
not_sel_barrel_shifter <= not(sel_barrel_shifter);

-- Asignation to outputs X,Y and Z
Xn <= out_add_sub_x;
Yn <= out_add_sub_y;
Zn <= out_add_sub_z;
----------------------------------------------------------------------
--- sign extension ---- 
--X0_extend_sign(N-1 downto N-2) <= (others => X0(N-2));
--X0_extend_sign(N-2 downto 0) <= X0;
 
--Y0_extend_sign(N-1 downto N-2) <= (others => Y0(N-2));
--Y0_extend_sign(N-2 downto 0) <= Y0; 

end Behavioral;
