# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct /home/juan/Documentos/JuanK/universidad/profun_2/project_audio_2/vitis/project_audio_2_class/platform.tcl
# 
# OR launch xsct and run below command.
# source /home/juan/Documentos/JuanK/universidad/profun_2/project_audio_2/vitis/project_audio_2_class/platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {project_audio_2_class}\
-hw {/home/juan/Documentos/JuanK/universidad/profun_2/project_audio_2/project_audio_2_class.xsa}\
-out {/home/juan/Documentos/JuanK/universidad/profun_2/project_audio_2/vitis}

platform write
domain create -name {standalone_ps7_cortexa9_0} -display-name {standalone_ps7_cortexa9_0} -os {standalone} -proc {ps7_cortexa9_0} -runtime {cpp} -arch {32-bit} -support-app {hello_world}
platform generate -domains 
platform active {project_audio_2_class}
domain active {zynq_fsbl}
domain active {standalone_ps7_cortexa9_0}
platform generate -quick
platform generate
