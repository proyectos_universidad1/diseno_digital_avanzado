# Usage with Vitis IDE:
# In Vitis IDE create a Single Application Debug launch configuration,
# change the debug type to 'Attach to running target' and provide this 
# tcl script in 'Execute Script' option.
# Path of this script: /home/juan/Documentos/JuanK/universidad/profun_2/project_audio_2/vitis/cordic_rotation_system/_ide/scripts/debugger_cordic_rotation-default.tcl
# 
# 
# Usage with xsct:
# To debug using xsct, launch xsct and run below command
# source /home/juan/Documentos/JuanK/universidad/profun_2/project_audio_2/vitis/cordic_rotation_system/_ide/scripts/debugger_cordic_rotation-default.tcl
# 
connect -url tcp:127.0.0.1:3121
targets -set -nocase -filter {name =~"APU*"}
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent Zybo 210279778491A" && level==0 && jtag_device_ctx=="jsn-Zybo-210279778491A-13722093-0"}
fpga -file /home/juan/Documentos/JuanK/universidad/profun_2/project_audio_2/vitis/cordic_rotation/_ide/bitstream/project_audio_2_class.bit
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw /home/juan/Documentos/JuanK/universidad/profun_2/project_audio_2/vitis/project_audio_2_class/export/project_audio_2_class/hw/project_audio_2_class.xsa -mem-ranges [list {0x40000000 0xbfffffff}] -regs
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*"}
source /home/juan/Documentos/JuanK/universidad/profun_2/project_audio_2/vitis/cordic_rotation/_ide/psinit/ps7_init.tcl
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "*A9*#0"}
dow /home/juan/Documentos/JuanK/universidad/profun_2/project_audio_2/vitis/cordic_rotation/Debug/cordic_rotation.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~ "*A9*#0"}
con
