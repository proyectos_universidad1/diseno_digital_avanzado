-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Wed May  3 07:38:58 2023
-- Host        : juan-Inspiron-14-3467 running 64-bit Linux Mint 21.1
-- Command     : write_vhdl -force -mode funcsim
--               /home/juan/Documentos/JuanK/universidad/profun_2/project_audio_2/project_audio_2.gen/sources_1/bd/project_audio_2_design/ip/project_audio_2_design_project_audio_2_ip_0_0/project_audio_2_design_project_audio_2_ip_0_0_sim_netlist.vhdl
-- Design      : project_audio_2_design_project_audio_2_ip_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1 is
  port (
    O : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[23]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[23]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[3]\ : in STD_LOGIC;
    A : in STD_LOGIC_VECTOR ( 10 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    DI : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[11]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[15]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[23]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[23]_2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[24]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1 : entity is "Mux_2_1";
end project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1;

architecture STRUCTURE of project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1 is
  signal \y0_carry__0_n_0\ : STD_LOGIC;
  signal \y0_carry__0_n_1\ : STD_LOGIC;
  signal \y0_carry__0_n_2\ : STD_LOGIC;
  signal \y0_carry__0_n_3\ : STD_LOGIC;
  signal \y0_carry__1_n_0\ : STD_LOGIC;
  signal \y0_carry__1_n_1\ : STD_LOGIC;
  signal \y0_carry__1_n_2\ : STD_LOGIC;
  signal \y0_carry__1_n_3\ : STD_LOGIC;
  signal \y0_carry__2_n_0\ : STD_LOGIC;
  signal \y0_carry__2_n_1\ : STD_LOGIC;
  signal \y0_carry__2_n_2\ : STD_LOGIC;
  signal \y0_carry__2_n_3\ : STD_LOGIC;
  signal \y0_carry__3_n_0\ : STD_LOGIC;
  signal \y0_carry__3_n_1\ : STD_LOGIC;
  signal \y0_carry__3_n_2\ : STD_LOGIC;
  signal \y0_carry__3_n_3\ : STD_LOGIC;
  signal \y0_carry__4_n_0\ : STD_LOGIC;
  signal \y0_carry__4_n_1\ : STD_LOGIC;
  signal \y0_carry__4_n_2\ : STD_LOGIC;
  signal \y0_carry__4_n_3\ : STD_LOGIC;
  signal y0_carry_n_0 : STD_LOGIC;
  signal y0_carry_n_1 : STD_LOGIC;
  signal y0_carry_n_2 : STD_LOGIC;
  signal y0_carry_n_3 : STD_LOGIC;
  signal \NLW_y0_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_y0_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of y0_carry : label is 35;
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of y0_carry : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__0\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__1\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__2\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__2\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__3\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__3\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__4\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__4\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__5\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__5\ : label is "{SYNTH-8 {cell *THIS*}}";
begin
y0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => y0_carry_n_0,
      CO(2) => y0_carry_n_1,
      CO(1) => y0_carry_n_2,
      CO(0) => y0_carry_n_3,
      CYINIT => \q_reg[3]\,
      DI(3 downto 0) => A(3 downto 0),
      O(3 downto 0) => O(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\y0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => y0_carry_n_0,
      CO(3) => \y0_carry__0_n_0\,
      CO(2) => \y0_carry__0_n_1\,
      CO(1) => \y0_carry__0_n_2\,
      CO(0) => \y0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => A(7 downto 4),
      O(3 downto 0) => \q_reg[7]\(3 downto 0),
      S(3 downto 0) => \q_reg[7]_0\(3 downto 0)
    );
\y0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__0_n_0\,
      CO(3) => \y0_carry__1_n_0\,
      CO(2) => \y0_carry__1_n_1\,
      CO(1) => \y0_carry__1_n_2\,
      CO(0) => \y0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => DI(1 downto 0),
      DI(1 downto 0) => A(9 downto 8),
      O(3 downto 0) => \q_reg[11]\(3 downto 0),
      S(3 downto 0) => \q_reg[11]_0\(3 downto 0)
    );
\y0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__1_n_0\,
      CO(3) => \y0_carry__2_n_0\,
      CO(2) => \y0_carry__2_n_1\,
      CO(1) => \y0_carry__2_n_2\,
      CO(0) => \y0_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => A(10),
      DI(2 downto 0) => \q_reg[15]_0\(2 downto 0),
      O(3 downto 0) => \q_reg[15]\(3 downto 0),
      S(3 downto 0) => \q_reg[15]_1\(3 downto 0)
    );
\y0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__2_n_0\,
      CO(3) => \y0_carry__3_n_0\,
      CO(2) => \y0_carry__3_n_1\,
      CO(1) => \y0_carry__3_n_2\,
      CO(0) => \y0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[19]_0\(3 downto 0),
      O(3 downto 0) => \q_reg[19]\(3 downto 0),
      S(3 downto 0) => \q_reg[19]_1\(3 downto 0)
    );
\y0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__3_n_0\,
      CO(3) => \y0_carry__4_n_0\,
      CO(2) => \y0_carry__4_n_1\,
      CO(1) => \y0_carry__4_n_2\,
      CO(0) => \y0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]_1\(3 downto 0),
      O(3 downto 0) => \q_reg[23]\(3 downto 0),
      S(3 downto 0) => \q_reg[23]_2\(3 downto 0)
    );
\y0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__4_n_0\,
      CO(3 downto 0) => \NLW_y0_carry__5_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_y0_carry__5_O_UNCONNECTED\(3 downto 1),
      O(0) => \q_reg[23]_0\(0),
      S(3 downto 1) => B"000",
      S(0) => \q_reg[24]\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1_0 is
  port (
    O : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[23]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[23]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    DI : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]_0\ : in STD_LOGIC;
    \q_reg[15]_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[15]_2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[23]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[23]_2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[24]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_rdata_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \axi_rdata_reg[0]\ : in STD_LOGIC;
    \axi_rdata_reg[3]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[31]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \axi_rdata_reg[23]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[19]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[15]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[11]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \axi_rdata_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1_0 : entity is "Mux_2_1";
end project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1_0;

architecture STRUCTURE of project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1_0 is
  signal \^o\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^q_reg[11]\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^q_reg[15]\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^q_reg[19]\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^q_reg[23]\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^q_reg[23]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^q_reg[7]\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \y0_carry__0_n_0\ : STD_LOGIC;
  signal \y0_carry__0_n_1\ : STD_LOGIC;
  signal \y0_carry__0_n_2\ : STD_LOGIC;
  signal \y0_carry__0_n_3\ : STD_LOGIC;
  signal \y0_carry__1_n_0\ : STD_LOGIC;
  signal \y0_carry__1_n_1\ : STD_LOGIC;
  signal \y0_carry__1_n_2\ : STD_LOGIC;
  signal \y0_carry__1_n_3\ : STD_LOGIC;
  signal \y0_carry__2_n_0\ : STD_LOGIC;
  signal \y0_carry__2_n_1\ : STD_LOGIC;
  signal \y0_carry__2_n_2\ : STD_LOGIC;
  signal \y0_carry__2_n_3\ : STD_LOGIC;
  signal \y0_carry__3_n_0\ : STD_LOGIC;
  signal \y0_carry__3_n_1\ : STD_LOGIC;
  signal \y0_carry__3_n_2\ : STD_LOGIC;
  signal \y0_carry__3_n_3\ : STD_LOGIC;
  signal \y0_carry__4_n_0\ : STD_LOGIC;
  signal \y0_carry__4_n_1\ : STD_LOGIC;
  signal \y0_carry__4_n_2\ : STD_LOGIC;
  signal \y0_carry__4_n_3\ : STD_LOGIC;
  signal y0_carry_n_0 : STD_LOGIC;
  signal y0_carry_n_1 : STD_LOGIC;
  signal y0_carry_n_2 : STD_LOGIC;
  signal y0_carry_n_3 : STD_LOGIC;
  signal \NLW_y0_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_y0_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of y0_carry : label is 35;
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of y0_carry : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__0\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__1\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__2\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__2\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__3\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__3\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__4\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__4\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__5\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__5\ : label is "{SYNTH-8 {cell *THIS*}}";
begin
  O(3 downto 0) <= \^o\(3 downto 0);
  \q_reg[11]\(3 downto 0) <= \^q_reg[11]\(3 downto 0);
  \q_reg[15]\(3 downto 0) <= \^q_reg[15]\(3 downto 0);
  \q_reg[19]\(3 downto 0) <= \^q_reg[19]\(3 downto 0);
  \q_reg[23]\(3 downto 0) <= \^q_reg[23]\(3 downto 0);
  \q_reg[23]_0\(0) <= \^q_reg[23]_0\(0);
  \q_reg[7]\(3 downto 0) <= \^q_reg[7]\(3 downto 0);
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(0),
      I1 => \^o\(0),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[0]\,
      I4 => axi_araddr(1),
      I5 => \axi_rdata_reg[3]\(0),
      O => D(0)
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(10),
      I1 => \^q_reg[11]\(2),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[11]\(2),
      I4 => axi_araddr(1),
      O => D(10)
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(11),
      I1 => \^q_reg[11]\(3),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[11]\(3),
      I4 => axi_araddr(1),
      O => D(11)
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(12),
      I1 => \^q_reg[15]\(0),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[15]\(0),
      I4 => axi_araddr(1),
      O => D(12)
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(13),
      I1 => \^q_reg[15]\(1),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[15]\(1),
      I4 => axi_araddr(1),
      O => D(13)
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(14),
      I1 => \^q_reg[15]\(2),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[15]\(2),
      I4 => axi_araddr(1),
      O => D(14)
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(15),
      I1 => \^q_reg[15]\(3),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[15]\(3),
      I4 => axi_araddr(1),
      O => D(15)
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(16),
      I1 => \^q_reg[19]\(0),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[19]\(0),
      I4 => axi_araddr(1),
      O => D(16)
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(17),
      I1 => \^q_reg[19]\(1),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[19]\(1),
      I4 => axi_araddr(1),
      O => D(17)
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(18),
      I1 => \^q_reg[19]\(2),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[19]\(2),
      I4 => axi_araddr(1),
      O => D(18)
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(19),
      I1 => \^q_reg[19]\(3),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[19]\(3),
      I4 => axi_araddr(1),
      O => D(19)
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(1),
      I1 => \^o\(1),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[3]\(1),
      I4 => axi_araddr(1),
      O => D(1)
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(20),
      I1 => \^q_reg[23]\(0),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[23]\(0),
      I4 => axi_araddr(1),
      O => D(20)
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(21),
      I1 => \^q_reg[23]\(1),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[23]\(1),
      I4 => axi_araddr(1),
      O => D(21)
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(22),
      I1 => \^q_reg[23]\(2),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[23]\(2),
      I4 => axi_araddr(1),
      O => D(22)
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(23),
      I1 => \^q_reg[23]\(3),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[23]\(3),
      I4 => axi_araddr(1),
      O => D(23)
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(24),
      I1 => \^q_reg[23]_0\(0),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[31]_0\(0),
      I4 => axi_araddr(1),
      O => D(24)
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(25),
      I1 => \^q_reg[23]_0\(0),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[31]_0\(0),
      I4 => axi_araddr(1),
      O => D(25)
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(26),
      I1 => \^q_reg[23]_0\(0),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[31]_0\(0),
      I4 => axi_araddr(1),
      O => D(26)
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(27),
      I1 => \^q_reg[23]_0\(0),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[31]_0\(0),
      I4 => axi_araddr(1),
      O => D(27)
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(28),
      I1 => \^q_reg[23]_0\(0),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[31]_0\(0),
      I4 => axi_araddr(1),
      O => D(28)
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(29),
      I1 => \^q_reg[23]_0\(0),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[31]_0\(0),
      I4 => axi_araddr(1),
      O => D(29)
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(2),
      I1 => \^o\(2),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[3]\(2),
      I4 => axi_araddr(1),
      O => D(2)
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(30),
      I1 => \^q_reg[23]_0\(0),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[31]_0\(0),
      I4 => axi_araddr(1),
      O => D(30)
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(31),
      I1 => \^q_reg[23]_0\(0),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[31]_0\(0),
      I4 => axi_araddr(1),
      O => D(31)
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(3),
      I1 => \^o\(3),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[3]\(3),
      I4 => axi_araddr(1),
      O => D(3)
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(4),
      I1 => \^q_reg[7]\(0),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[7]\(0),
      I4 => axi_araddr(1),
      O => D(4)
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(5),
      I1 => \^q_reg[7]\(1),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[7]\(1),
      I4 => axi_araddr(1),
      O => D(5)
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(6),
      I1 => \^q_reg[7]\(2),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[7]\(2),
      I4 => axi_araddr(1),
      O => D(6)
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(7),
      I1 => \^q_reg[7]\(3),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[7]\(3),
      I4 => axi_araddr(1),
      O => D(7)
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(8),
      I1 => \^q_reg[11]\(0),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[11]\(0),
      I4 => axi_araddr(1),
      O => D(8)
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0CFC0"
    )
        port map (
      I0 => \axi_rdata_reg[31]\(9),
      I1 => \^q_reg[11]\(1),
      I2 => axi_araddr(0),
      I3 => \axi_rdata_reg[11]\(1),
      I4 => axi_araddr(1),
      O => D(9)
    );
y0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => y0_carry_n_0,
      CO(2) => y0_carry_n_1,
      CO(1) => y0_carry_n_2,
      CO(0) => y0_carry_n_3,
      CYINIT => Q(0),
      DI(3 downto 0) => DI(3 downto 0),
      O(3 downto 0) => \^o\(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\y0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => y0_carry_n_0,
      CO(3) => \y0_carry__0_n_0\,
      CO(2) => \y0_carry__0_n_1\,
      CO(1) => \y0_carry__0_n_2\,
      CO(0) => \y0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[7]_0\(3 downto 0),
      O(3 downto 0) => \^q_reg[7]\(3 downto 0),
      S(3 downto 0) => \q_reg[7]_1\(3 downto 0)
    );
\y0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__0_n_0\,
      CO(3) => \y0_carry__1_n_0\,
      CO(2) => \y0_carry__1_n_1\,
      CO(1) => \y0_carry__1_n_2\,
      CO(0) => \y0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[11]_0\(3 downto 0),
      O(3 downto 0) => \^q_reg[11]\(3 downto 0),
      S(3 downto 0) => \q_reg[11]_1\(3 downto 0)
    );
\y0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__1_n_0\,
      CO(3) => \y0_carry__2_n_0\,
      CO(2) => \y0_carry__2_n_1\,
      CO(1) => \y0_carry__2_n_2\,
      CO(0) => \y0_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => \q_reg[15]_0\,
      DI(2 downto 0) => \q_reg[15]_1\(2 downto 0),
      O(3 downto 0) => \^q_reg[15]\(3 downto 0),
      S(3 downto 0) => \q_reg[15]_2\(3 downto 0)
    );
\y0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__2_n_0\,
      CO(3) => \y0_carry__3_n_0\,
      CO(2) => \y0_carry__3_n_1\,
      CO(1) => \y0_carry__3_n_2\,
      CO(0) => \y0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[19]_0\(3 downto 0),
      O(3 downto 0) => \^q_reg[19]\(3 downto 0),
      S(3 downto 0) => \q_reg[19]_1\(3 downto 0)
    );
\y0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__3_n_0\,
      CO(3) => \y0_carry__4_n_0\,
      CO(2) => \y0_carry__4_n_1\,
      CO(1) => \y0_carry__4_n_2\,
      CO(0) => \y0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]_1\(3 downto 0),
      O(3 downto 0) => \^q_reg[23]\(3 downto 0),
      S(3 downto 0) => \q_reg[23]_2\(3 downto 0)
    );
\y0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__4_n_0\,
      CO(3 downto 0) => \NLW_y0_carry__5_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_y0_carry__5_O_UNCONNECTED\(3 downto 1),
      O(0) => \^q_reg[23]_0\(0),
      S(3 downto 1) => B"000",
      S(0) => \q_reg[24]\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1__parameterized2\ is
  port (
    \q_reg[3]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    O : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[3]_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 22 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[23]\ : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1__parameterized2\ : entity is "Mux_2_1";
end \project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1__parameterized2\;

architecture STRUCTURE of \project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1__parameterized2\ is
  signal \y0_carry__0_n_0\ : STD_LOGIC;
  signal \y0_carry__0_n_1\ : STD_LOGIC;
  signal \y0_carry__0_n_2\ : STD_LOGIC;
  signal \y0_carry__0_n_3\ : STD_LOGIC;
  signal \y0_carry__1_n_0\ : STD_LOGIC;
  signal \y0_carry__1_n_1\ : STD_LOGIC;
  signal \y0_carry__1_n_2\ : STD_LOGIC;
  signal \y0_carry__1_n_3\ : STD_LOGIC;
  signal \y0_carry__2_n_0\ : STD_LOGIC;
  signal \y0_carry__2_n_1\ : STD_LOGIC;
  signal \y0_carry__2_n_2\ : STD_LOGIC;
  signal \y0_carry__2_n_3\ : STD_LOGIC;
  signal \y0_carry__3_n_0\ : STD_LOGIC;
  signal \y0_carry__3_n_1\ : STD_LOGIC;
  signal \y0_carry__3_n_2\ : STD_LOGIC;
  signal \y0_carry__3_n_3\ : STD_LOGIC;
  signal \y0_carry__4_n_1\ : STD_LOGIC;
  signal \y0_carry__4_n_2\ : STD_LOGIC;
  signal \y0_carry__4_n_3\ : STD_LOGIC;
  signal y0_carry_n_0 : STD_LOGIC;
  signal y0_carry_n_1 : STD_LOGIC;
  signal y0_carry_n_2 : STD_LOGIC;
  signal y0_carry_n_3 : STD_LOGIC;
  signal \NLW_y0_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of y0_carry : label is 35;
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of y0_carry : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__0\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__1\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__2\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__2\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__3\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__3\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__4\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__4\ : label is "{SYNTH-8 {cell *THIS*}}";
begin
y0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => y0_carry_n_0,
      CO(2) => y0_carry_n_1,
      CO(1) => y0_carry_n_2,
      CO(0) => y0_carry_n_3,
      CYINIT => \q_reg[3]_0\,
      DI(3 downto 0) => Q(3 downto 0),
      O(3 downto 0) => \q_reg[3]\(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\y0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => y0_carry_n_0,
      CO(3) => \y0_carry__0_n_0\,
      CO(2) => \y0_carry__0_n_1\,
      CO(1) => \y0_carry__0_n_2\,
      CO(0) => \y0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(7 downto 4),
      O(3 downto 0) => \q_reg[7]\(3 downto 0),
      S(3 downto 0) => \q_reg[7]_0\(3 downto 0)
    );
\y0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__0_n_0\,
      CO(3) => \y0_carry__1_n_0\,
      CO(2) => \y0_carry__1_n_1\,
      CO(1) => \y0_carry__1_n_2\,
      CO(0) => \y0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(11 downto 8),
      O(3 downto 0) => \q_reg[11]\(3 downto 0),
      S(3 downto 0) => \q_reg[11]_0\(3 downto 0)
    );
\y0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__1_n_0\,
      CO(3) => \y0_carry__2_n_0\,
      CO(2) => \y0_carry__2_n_1\,
      CO(1) => \y0_carry__2_n_2\,
      CO(0) => \y0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(15 downto 12),
      O(3 downto 0) => \q_reg[15]\(3 downto 0),
      S(3 downto 0) => \q_reg[15]_0\(3 downto 0)
    );
\y0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__2_n_0\,
      CO(3) => \y0_carry__3_n_0\,
      CO(2) => \y0_carry__3_n_1\,
      CO(1) => \y0_carry__3_n_2\,
      CO(0) => \y0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(19 downto 16),
      O(3 downto 0) => \q_reg[19]\(3 downto 0),
      S(3 downto 0) => \q_reg[19]_0\(3 downto 0)
    );
\y0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__3_n_0\,
      CO(3) => \NLW_y0_carry__4_CO_UNCONNECTED\(3),
      CO(2) => \y0_carry__4_n_1\,
      CO(1) => \y0_carry__4_n_2\,
      CO(0) => \y0_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => Q(22 downto 20),
      O(3 downto 0) => O(3 downto 0),
      S(3 downto 0) => \q_reg[23]\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity project_audio_2_design_project_audio_2_ip_0_0_ROM is
  port (
    out_ROM : out STD_LOGIC_VECTOR ( 22 downto 0 );
    data_reg_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_aclk : in STD_LOGIC;
    addr : in STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of project_audio_2_design_project_audio_2_ip_0_0_ROM : entity is "ROM";
end project_audio_2_design_project_audio_2_ip_0_0_ROM;

architecture STRUCTURE of project_audio_2_design_project_audio_2_ip_0_0_ROM is
  signal \^out_rom\ : STD_LOGIC_VECTOR ( 22 downto 0 );
  signal NLW_data_reg_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 5 );
  signal NLW_data_reg_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of data_reg : label is "p2_d16";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of data_reg : label is "p0_d5";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of data_reg : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of data_reg : label is 736;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of data_reg : label is "U0/project_audio_2_ip_v1_0_S00_AXI_inst/cordic_rotation0/data_path0/ROM_1/data_reg";
  attribute RTL_RAM_TYPE : string;
  attribute RTL_RAM_TYPE of data_reg : label is "RAM_TDP";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of data_reg : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of data_reg : label is 31;
  attribute ram_ext_slice_begin : integer;
  attribute ram_ext_slice_begin of data_reg : label is 18;
  attribute ram_ext_slice_end : integer;
  attribute ram_ext_slice_end of data_reg : label is 22;
  attribute ram_offset : integer;
  attribute ram_offset of data_reg : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of data_reg : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of data_reg : label is 17;
begin
  out_ROM(22 downto 0) <= \^out_rom\(22 downto 0);
data_reg: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000658",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"005100A20145028B05170A2F145F28BE517CA2F645D78B0D1111FB38E4050000",
      INIT_01 => X"0000000000000000000000000000000000000000000100020005000A00140028",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000001000200040008",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"00000",
      INIT_B => X"00000",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(13 downto 9) => B"00000",
      ADDRARDADDR(8 downto 4) => addr(4 downto 0),
      ADDRARDADDR(3 downto 0) => B"0000",
      ADDRBWRADDR(13 downto 9) => B"10000",
      ADDRBWRADDR(8 downto 4) => addr(4 downto 0),
      ADDRBWRADDR(3 downto 0) => B"0000",
      CLKARDCLK => s00_axi_aclk,
      CLKBWRCLK => s00_axi_aclk,
      DIADI(15 downto 0) => B"1111111111111111",
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"11",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 0) => \^out_rom\(15 downto 0),
      DOBDO(15 downto 5) => NLW_data_reg_DOBDO_UNCONNECTED(15 downto 5),
      DOBDO(4 downto 0) => \^out_rom\(22 downto 18),
      DOPADOP(1 downto 0) => \^out_rom\(17 downto 16),
      DOPBDOP(1 downto 0) => NLW_data_reg_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => '1',
      ENBWREN => '1',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1 downto 0) => B"00",
      WEBWE(3 downto 0) => B"0000"
    );
\y0_carry__4_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^out_rom\(22),
      O => data_reg_0(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity project_audio_2_design_project_audio_2_ip_0_0_fsm is
  port (
    \FSM_sequential_state_reg[0]_0\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 23 downto 0 );
    \FSM_sequential_state_reg[0]_1\ : out STD_LOGIC;
    p_0_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \FSM_sequential_state_reg[0]_2\ : in STD_LOGIC;
    \FSM_sequential_state_reg[0]_3\ : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    O : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[3]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of project_audio_2_design_project_audio_2_ip_0_0_fsm : entity is "fsm";
end project_audio_2_design_project_audio_2_ip_0_0_fsm;

architecture STRUCTURE of project_audio_2_design_project_audio_2_ip_0_0_fsm is
  signal \FSM_sequential_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \q[4]_i_4_n_0\ : STD_LOGIC;
  signal state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \state__0\ : STD_LOGIC_VECTOR ( 1 to 1 );
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_state_reg[0]\ : label is "counter_state:10,start_state:00,compute_state:01";
  attribute FSM_ENCODED_STATES of \FSM_sequential_state_reg[1]\ : label is "counter_state:10,start_state:00,compute_state:01";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \q[0]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \q[10]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \q[11]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \q[12]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \q[13]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \q[14]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \q[15]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \q[16]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \q[17]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \q[18]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \q[19]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \q[1]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \q[20]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \q[21]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \q[22]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \q[23]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \q[2]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \q[3]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \q[4]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \q[4]_i_2\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \q[4]_i_4\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \q[5]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \q[6]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \q[7]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \q[8]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \q[9]_i_1\ : label is "soft_lutpair8";
begin
\FSM_sequential_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000001055550010"
    )
        port map (
      I0 => state(0),
      I1 => p_0_in(1),
      I2 => p_0_in(0),
      I3 => \FSM_sequential_state_reg[0]_2\,
      I4 => state(1),
      I5 => \FSM_sequential_state_reg[0]_3\,
      O => \FSM_sequential_state[0]_i_1_n_0\
    );
\FSM_sequential_state[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      O => \state__0\(1)
    );
\FSM_sequential_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \FSM_sequential_state[0]_i_1_n_0\,
      Q => state(0),
      R => '0'
    );
\FSM_sequential_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \state__0\(1),
      Q => state(1),
      R => '0'
    );
\q[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[3]\(0),
      O => D(0)
    );
\q[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(10),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[11]\(2),
      O => D(10)
    );
\q[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(11),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[11]\(3),
      O => D(11)
    );
\q[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(12),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[15]\(0),
      O => D(12)
    );
\q[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(13),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[15]\(1),
      O => D(13)
    );
\q[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(14),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[15]\(2),
      O => D(14)
    );
\q[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(15),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[15]\(3),
      O => D(15)
    );
\q[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(16),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[19]\(0),
      O => D(16)
    );
\q[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(17),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[19]\(1),
      O => D(17)
    );
\q[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(18),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[19]\(2),
      O => D(18)
    );
\q[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(19),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[19]\(3),
      O => D(19)
    );
\q[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(1),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[3]\(1),
      O => D(1)
    );
\q[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(20),
      I1 => \q[4]_i_4_n_0\,
      I2 => O(0),
      O => D(20)
    );
\q[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(21),
      I1 => \q[4]_i_4_n_0\,
      I2 => O(1),
      O => D(21)
    );
\q[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(22),
      I1 => \q[4]_i_4_n_0\,
      I2 => O(2),
      O => D(22)
    );
\q[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(23),
      I1 => \q[4]_i_4_n_0\,
      I2 => O(3),
      O => D(23)
    );
\q[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(2),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[3]\(2),
      O => D(2)
    );
\q[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(3),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[3]\(3),
      O => D(3)
    );
\q[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(4),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[7]\(0),
      O => D(4)
    );
\q[4]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000AABA00000000"
    )
        port map (
      I0 => state(0),
      I1 => \FSM_sequential_state_reg[0]_2\,
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => state(1),
      I5 => \q[4]_i_4_n_0\,
      O => \FSM_sequential_state_reg[0]_0\
    );
\q[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000AABA"
    )
        port map (
      I0 => state(0),
      I1 => \FSM_sequential_state_reg[0]_2\,
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => state(1),
      O => \FSM_sequential_state_reg[0]_1\
    );
\q[4]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => state(0),
      I1 => \FSM_sequential_state_reg[0]_2\,
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => state(1),
      O => \q[4]_i_4_n_0\
    );
\q[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(5),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[7]\(1),
      O => D(5)
    );
\q[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(6),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[7]\(2),
      O => D(6)
    );
\q[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(7),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[7]\(3),
      O => D(7)
    );
\q[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(8),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[11]\(0),
      O => D(8)
    );
\q[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(9),
      I1 => \q[4]_i_4_n_0\,
      I2 => \q_reg[11]\(1),
      O => D(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity project_audio_2_design_project_audio_2_ip_0_0_reg is
  port (
    \q_reg[2]_0\ : out STD_LOGIC;
    \q_reg[1]_0\ : out STD_LOGIC;
    \q_reg[0]_0\ : out STD_LOGIC;
    \q_reg[3]_0\ : out STD_LOGIC;
    \q_reg[1]_1\ : out STD_LOGIC;
    \q_reg[4]_0\ : out STD_LOGIC;
    \q_reg[1]_2\ : out STD_LOGIC;
    \q_reg[2]_1\ : out STD_LOGIC;
    \q_reg[1]_3\ : out STD_LOGIC;
    \q_reg[1]_4\ : out STD_LOGIC;
    \q_reg[1]_5\ : out STD_LOGIC;
    \q_reg[2]_2\ : out STD_LOGIC;
    \q_reg[1]_6\ : out STD_LOGIC;
    \q_reg[1]_7\ : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[14]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[19]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[22]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[11]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[14]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[19]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[22]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \y0_carry__1_i_2__0_0\ : in STD_LOGIC;
    \y0_carry__1_i_2__0_1\ : in STD_LOGIC;
    \y0_carry__1_i_2__0_2\ : in STD_LOGIC;
    \y0_carry__2_i_1__0\ : in STD_LOGIC;
    \y0_carry__3_i_2__0_0\ : in STD_LOGIC;
    \y0_carry__4_i_2__0_0\ : in STD_LOGIC;
    \y0_carry__4_i_2__0_1\ : in STD_LOGIC;
    \y0_carry__1_i_3__0\ : in STD_LOGIC;
    \y0_carry__1_i_3__0_0\ : in STD_LOGIC;
    \y0_carry__1_i_3__0_1\ : in STD_LOGIC;
    \y0_carry__2_i_3__0_0\ : in STD_LOGIC;
    \y0_carry__3_i_3__0\ : in STD_LOGIC;
    \q_reg[23]\ : in STD_LOGIC;
    \q_reg[23]_0\ : in STD_LOGIC;
    \q_reg[23]_1\ : in STD_LOGIC;
    \q_reg[19]_1\ : in STD_LOGIC;
    \q_reg[19]_2\ : in STD_LOGIC;
    \y0_carry__1_i_6_0\ : in STD_LOGIC;
    \y0_carry__1_i_7_0\ : in STD_LOGIC;
    \y0_carry__1_i_2__1_0\ : in STD_LOGIC;
    \y0_carry__1_i_2__1_1\ : in STD_LOGIC;
    \y0_carry__1_i_2__1_2\ : in STD_LOGIC;
    \y0_carry__2_i_1__1\ : in STD_LOGIC;
    \y0_carry__3_i_2__1_0\ : in STD_LOGIC;
    A : in STD_LOGIC_VECTOR ( 13 downto 0 );
    \y0_carry__1_i_3__1\ : in STD_LOGIC;
    \y0_carry__1_i_3__1_0\ : in STD_LOGIC;
    \y0_carry__1_i_3__1_1\ : in STD_LOGIC;
    \y0_carry__2_i_3__1_0\ : in STD_LOGIC;
    \y0_carry__3_i_3__1\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[11]_0\ : in STD_LOGIC;
    \q_reg[11]_1\ : in STD_LOGIC;
    \q_reg[15]\ : in STD_LOGIC;
    \q_reg[15]_0\ : in STD_LOGIC;
    \q_reg[15]_1\ : in STD_LOGIC;
    \q_reg[0]_1\ : in STD_LOGIC;
    \q_reg[0]_2\ : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of project_audio_2_design_project_audio_2_ip_0_0_reg : entity is "reg";
end project_audio_2_design_project_audio_2_ip_0_0_reg;

architecture STRUCTURE of project_audio_2_design_project_audio_2_ip_0_0_reg is
  signal \q[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \q[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \q[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \q[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \q[4]_i_3_n_0\ : STD_LOGIC;
  signal \^q_reg[0]_0\ : STD_LOGIC;
  signal \^q_reg[1]_0\ : STD_LOGIC;
  signal \^q_reg[1]_2\ : STD_LOGIC;
  signal \^q_reg[1]_3\ : STD_LOGIC;
  signal \^q_reg[1]_4\ : STD_LOGIC;
  signal \^q_reg[1]_5\ : STD_LOGIC;
  signal \^q_reg[1]_6\ : STD_LOGIC;
  signal \^q_reg[1]_7\ : STD_LOGIC;
  signal \^q_reg[2]_0\ : STD_LOGIC;
  signal \^q_reg[2]_1\ : STD_LOGIC;
  signal \^q_reg[2]_2\ : STD_LOGIC;
  signal \^q_reg[3]_0\ : STD_LOGIC;
  signal \^q_reg[4]_0\ : STD_LOGIC;
  signal \y0_carry__1_i_10__0_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_10_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_11__0_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_11_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_5__0_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_6__0_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_9__0_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \y0_carry__2_i_12__0_n_0\ : STD_LOGIC;
  signal \y0_carry__2_i_12_n_0\ : STD_LOGIC;
  signal \y0_carry__2_i_7__0_n_0\ : STD_LOGIC;
  signal \y0_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \y0_carry__2_i_8__0_n_0\ : STD_LOGIC;
  signal \y0_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_10__0_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_10_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_11__0_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_11_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_5__0_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_6__0_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_9__0_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_9_n_0\ : STD_LOGIC;
  signal \y0_carry__4_i_6__0_n_0\ : STD_LOGIC;
  signal \y0_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \y0_carry__4_i_7__0_n_0\ : STD_LOGIC;
  signal \y0_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \y0_carry__4_i_8__0_n_0\ : STD_LOGIC;
  signal \y0_carry__4_i_8_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \axi_rdata[0]_i_2\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \q[0]_i_1__0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \q[1]_i_1__0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \q[2]_i_1__0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \q[3]_i_1__0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \q[4]_i_3\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \y0_carry__1_i_10\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \y0_carry__1_i_10__0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \y0_carry__1_i_11\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \y0_carry__1_i_11__0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \y0_carry__1_i_9\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \y0_carry__1_i_9__0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \y0_carry__2_i_12\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \y0_carry__2_i_12__0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \y0_carry__3_i_5\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \y0_carry__3_i_5__0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \y0_carry__3_i_6\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \y0_carry__3_i_6__0\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \y0_carry__3_i_7\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \y0_carry__3_i_7__0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \y0_carry__4_i_8\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \y0_carry__4_i_8__0\ : label is "soft_lutpair21";
begin
  \q_reg[0]_0\ <= \^q_reg[0]_0\;
  \q_reg[1]_0\ <= \^q_reg[1]_0\;
  \q_reg[1]_2\ <= \^q_reg[1]_2\;
  \q_reg[1]_3\ <= \^q_reg[1]_3\;
  \q_reg[1]_4\ <= \^q_reg[1]_4\;
  \q_reg[1]_5\ <= \^q_reg[1]_5\;
  \q_reg[1]_6\ <= \^q_reg[1]_6\;
  \q_reg[1]_7\ <= \^q_reg[1]_7\;
  \q_reg[2]_0\ <= \^q_reg[2]_0\;
  \q_reg[2]_1\ <= \^q_reg[2]_1\;
  \q_reg[2]_2\ <= \^q_reg[2]_2\;
  \q_reg[3]_0\ <= \^q_reg[3]_0\;
  \q_reg[4]_0\ <= \^q_reg[4]_0\;
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00100000"
    )
        port map (
      I0 => \^q_reg[1]_0\,
      I1 => \^q_reg[0]_0\,
      I2 => \^q_reg[4]_0\,
      I3 => \^q_reg[2]_0\,
      I4 => \^q_reg[3]_0\,
      O => \q_reg[1]_1\
    );
\q[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q_reg[0]_0\,
      O => \q[0]_i_1__0_n_0\
    );
\q[1]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q_reg[0]_0\,
      I1 => \^q_reg[1]_0\,
      O => \q[1]_i_1__0_n_0\
    );
\q[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \^q_reg[2]_0\,
      I1 => \^q_reg[1]_0\,
      I2 => \^q_reg[0]_0\,
      O => \q[2]_i_1__0_n_0\
    );
\q[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \^q_reg[3]_0\,
      I1 => \^q_reg[2]_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \^q_reg[1]_0\,
      O => \q[3]_i_1__0_n_0\
    );
\q[4]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \^q_reg[4]_0\,
      I1 => \^q_reg[1]_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \^q_reg[2]_0\,
      I4 => \^q_reg[3]_0\,
      O => \q[4]_i_3_n_0\
    );
\q_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \q_reg[0]_2\,
      D => \q[0]_i_1__0_n_0\,
      Q => \^q_reg[0]_0\,
      R => \q_reg[0]_1\
    );
\q_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \q_reg[0]_2\,
      D => \q[1]_i_1__0_n_0\,
      Q => \^q_reg[1]_0\,
      R => \q_reg[0]_1\
    );
\q_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \q_reg[0]_2\,
      D => \q[2]_i_1__0_n_0\,
      Q => \^q_reg[2]_0\,
      R => \q_reg[0]_1\
    );
\q_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \q_reg[0]_2\,
      D => \q[3]_i_1__0_n_0\,
      Q => \^q_reg[3]_0\,
      R => \q_reg[0]_1\
    );
\q_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \q_reg[0]_2\,
      D => \q[4]_i_3_n_0\,
      Q => \^q_reg[4]_0\,
      R => \q_reg[0]_1\
    );
\y0_carry__1_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q_reg[3]_0\,
      I1 => \y0_carry__4_i_2__0_0\,
      I2 => \^q_reg[4]_0\,
      I3 => \y0_carry__1_i_7_0\,
      O => \y0_carry__1_i_10_n_0\
    );
\y0_carry__1_i_10__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q_reg[3]_0\,
      I1 => A(13),
      I2 => \^q_reg[4]_0\,
      I3 => A(5),
      O => \y0_carry__1_i_10__0_n_0\
    );
\y0_carry__1_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q_reg[3]_0\,
      I1 => \y0_carry__4_i_2__0_0\,
      I2 => \^q_reg[4]_0\,
      I3 => \y0_carry__1_i_6_0\,
      O => \y0_carry__1_i_11_n_0\
    );
\y0_carry__1_i_11__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q_reg[3]_0\,
      I1 => A(13),
      I2 => \^q_reg[4]_0\,
      I3 => A(6),
      O => \y0_carry__1_i_11__0_n_0\
    );
\y0_carry__1_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(1),
      I1 => \y0_carry__1_i_5_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__1_i_6_n_0\,
      I4 => Q(0),
      O => S(1)
    );
\y0_carry__1_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[11]_1\,
      I1 => \y0_carry__1_i_5__0_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__1_i_6__0_n_0\,
      I4 => Q(0),
      O => \q_reg[11]\(1)
    );
\y0_carry__1_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(0),
      I1 => \y0_carry__1_i_6_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \^q_reg[1]_3\,
      I4 => Q(0),
      O => S(0)
    );
\y0_carry__1_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[11]_0\,
      I1 => \y0_carry__1_i_6__0_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \^q_reg[1]_6\,
      I4 => Q(0),
      O => \q_reg[11]\(0)
    );
\y0_carry__1_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__1_i_9_n_0\,
      I1 => \y0_carry__1_i_3__0_0\,
      I2 => \^q_reg[1]_0\,
      I3 => \y0_carry__1_i_10_n_0\,
      I4 => \^q_reg[2]_0\,
      I5 => \y0_carry__1_i_3__0\,
      O => \y0_carry__1_i_5_n_0\
    );
\y0_carry__1_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__1_i_9__0_n_0\,
      I1 => \y0_carry__1_i_3__1_0\,
      I2 => \^q_reg[1]_0\,
      I3 => \y0_carry__1_i_10__0_n_0\,
      I4 => \^q_reg[2]_0\,
      I5 => \y0_carry__1_i_3__1\,
      O => \y0_carry__1_i_5__0_n_0\
    );
\y0_carry__1_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__1_i_11_n_0\,
      I1 => \y0_carry__1_i_2__0_0\,
      I2 => \^q_reg[1]_0\,
      I3 => \y0_carry__1_i_2__0_1\,
      I4 => \^q_reg[2]_0\,
      I5 => \y0_carry__1_i_2__0_2\,
      O => \y0_carry__1_i_6_n_0\
    );
\y0_carry__1_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__1_i_11__0_n_0\,
      I1 => \y0_carry__1_i_2__1_0\,
      I2 => \^q_reg[1]_0\,
      I3 => \y0_carry__1_i_2__1_1\,
      I4 => \^q_reg[2]_0\,
      I5 => \y0_carry__1_i_2__1_2\,
      O => \y0_carry__1_i_6__0_n_0\
    );
\y0_carry__1_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__1_i_10_n_0\,
      I1 => \y0_carry__1_i_3__0\,
      I2 => \^q_reg[1]_0\,
      I3 => \y0_carry__1_i_3__0_0\,
      I4 => \^q_reg[2]_0\,
      I5 => \y0_carry__1_i_3__0_1\,
      O => \^q_reg[1]_3\
    );
\y0_carry__1_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__1_i_10__0_n_0\,
      I1 => \y0_carry__1_i_3__1\,
      I2 => \^q_reg[1]_0\,
      I3 => \y0_carry__1_i_3__1_0\,
      I4 => \^q_reg[2]_0\,
      I5 => \y0_carry__1_i_3__1_1\,
      O => \^q_reg[1]_6\
    );
\y0_carry__1_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q_reg[3]_0\,
      I1 => \y0_carry__4_i_2__0_0\,
      I2 => \^q_reg[4]_0\,
      I3 => \q_reg[19]_2\,
      O => \y0_carry__1_i_9_n_0\
    );
\y0_carry__1_i_9__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q_reg[3]_0\,
      I1 => A(13),
      I2 => \^q_reg[4]_0\,
      I3 => A(7),
      O => \y0_carry__1_i_9__0_n_0\
    );
\y0_carry__2_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q_reg[3]_0\,
      I1 => \y0_carry__4_i_2__0_0\,
      I2 => \^q_reg[4]_0\,
      I3 => \q_reg[19]_1\,
      O => \y0_carry__2_i_12_n_0\
    );
\y0_carry__2_i_12__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q_reg[3]_0\,
      I1 => A(13),
      I2 => \^q_reg[4]_0\,
      I3 => A(8),
      O => \y0_carry__2_i_12__0_n_0\
    );
\y0_carry__2_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(4),
      I1 => \^q_reg[1]_2\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__2_i_7_n_0\,
      I4 => Q(0),
      O => \q_reg[14]\(2)
    );
\y0_carry__2_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[15]_1\,
      I1 => \^q_reg[1]_5\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__2_i_7__0_n_0\,
      I4 => Q(0),
      O => \q_reg[14]_0\(2)
    );
\y0_carry__2_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(3),
      I1 => \y0_carry__2_i_7_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__2_i_8_n_0\,
      I4 => Q(0),
      O => \q_reg[14]\(1)
    );
\y0_carry__2_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[15]_0\,
      I1 => \y0_carry__2_i_7__0_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__2_i_8__0_n_0\,
      I4 => Q(0),
      O => \q_reg[14]_0\(1)
    );
\y0_carry__2_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(2),
      I1 => \y0_carry__2_i_8_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__1_i_5_n_0\,
      I4 => Q(0),
      O => \q_reg[14]\(0)
    );
\y0_carry__2_i_4__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[15]\,
      I1 => \y0_carry__2_i_8__0_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__1_i_5__0_n_0\,
      I4 => Q(0),
      O => \q_reg[14]_0\(0)
    );
\y0_carry__2_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \y0_carry__2_i_1__0\,
      I1 => \^q_reg[1]_0\,
      I2 => \y0_carry__2_i_12_n_0\,
      I3 => \^q_reg[2]_0\,
      I4 => \y0_carry__1_i_2__0_1\,
      O => \^q_reg[1]_2\
    );
\y0_carry__2_i_6__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \y0_carry__2_i_1__1\,
      I1 => \^q_reg[1]_0\,
      I2 => \y0_carry__2_i_12__0_n_0\,
      I3 => \^q_reg[2]_0\,
      I4 => \y0_carry__1_i_2__1_1\,
      O => \^q_reg[1]_5\
    );
\y0_carry__2_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \y0_carry__2_i_3__0_0\,
      I1 => \^q_reg[1]_0\,
      I2 => \y0_carry__1_i_9_n_0\,
      I3 => \^q_reg[2]_0\,
      I4 => \y0_carry__1_i_3__0_0\,
      O => \y0_carry__2_i_7_n_0\
    );
\y0_carry__2_i_7__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \y0_carry__2_i_3__1_0\,
      I1 => \^q_reg[1]_0\,
      I2 => \y0_carry__1_i_9__0_n_0\,
      I3 => \^q_reg[2]_0\,
      I4 => \y0_carry__1_i_3__1_0\,
      O => \y0_carry__2_i_7__0_n_0\
    );
\y0_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__2_i_12_n_0\,
      I1 => \y0_carry__1_i_2__0_1\,
      I2 => \^q_reg[1]_0\,
      I3 => \y0_carry__1_i_11_n_0\,
      I4 => \^q_reg[2]_0\,
      I5 => \y0_carry__1_i_2__0_0\,
      O => \y0_carry__2_i_8_n_0\
    );
\y0_carry__2_i_8__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__2_i_12__0_n_0\,
      I1 => \y0_carry__1_i_2__1_1\,
      I2 => \^q_reg[1]_0\,
      I3 => \y0_carry__1_i_11__0_n_0\,
      I4 => \^q_reg[2]_0\,
      I5 => \y0_carry__1_i_2__1_0\,
      O => \y0_carry__2_i_8__0_n_0\
    );
\y0_carry__3_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q_reg[2]_0\,
      I1 => \^q_reg[3]_0\,
      I2 => \y0_carry__4_i_2__0_0\,
      I3 => \^q_reg[4]_0\,
      I4 => \q_reg[23]_1\,
      O => \y0_carry__3_i_10_n_0\
    );
\y0_carry__3_i_10__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q_reg[2]_0\,
      I1 => \^q_reg[3]_0\,
      I2 => A(13),
      I3 => \^q_reg[4]_0\,
      I4 => A(9),
      O => \y0_carry__3_i_10__0_n_0\
    );
\y0_carry__3_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q_reg[2]_0\,
      I1 => \^q_reg[3]_0\,
      I2 => \y0_carry__4_i_2__0_0\,
      I3 => \^q_reg[4]_0\,
      I4 => \q_reg[23]_0\,
      O => \y0_carry__3_i_11_n_0\
    );
\y0_carry__3_i_11__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q_reg[2]_0\,
      I1 => \^q_reg[3]_0\,
      I2 => A(13),
      I3 => \^q_reg[4]_0\,
      I4 => A(10),
      O => \y0_carry__3_i_11__0_n_0\
    );
\y0_carry__3_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(8),
      I1 => \y0_carry__3_i_5_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__3_i_6_n_0\,
      I4 => Q(0),
      O => \q_reg[19]\(1)
    );
\y0_carry__3_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[19]_1\,
      I1 => \y0_carry__3_i_5__0_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__3_i_6__0_n_0\,
      I4 => Q(0),
      O => \q_reg[19]_0\(1)
    );
\y0_carry__3_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(7),
      I1 => \y0_carry__3_i_6_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \^q_reg[1]_4\,
      I4 => Q(0),
      O => \q_reg[19]\(0)
    );
\y0_carry__3_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[19]_2\,
      I1 => \y0_carry__3_i_6__0_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \^q_reg[1]_7\,
      I4 => Q(0),
      O => \q_reg[19]_0\(0)
    );
\y0_carry__3_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \y0_carry__3_i_9_n_0\,
      I1 => \^q_reg[1]_0\,
      I2 => \y0_carry__3_i_10_n_0\,
      O => \y0_carry__3_i_5_n_0\
    );
\y0_carry__3_i_5__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \y0_carry__3_i_9__0_n_0\,
      I1 => \^q_reg[1]_0\,
      I2 => \y0_carry__3_i_10__0_n_0\,
      O => \y0_carry__3_i_5__0_n_0\
    );
\y0_carry__3_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \y0_carry__3_i_11_n_0\,
      I1 => \^q_reg[1]_0\,
      I2 => \y0_carry__3_i_2__0_0\,
      O => \y0_carry__3_i_6_n_0\
    );
\y0_carry__3_i_6__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \y0_carry__3_i_11__0_n_0\,
      I1 => \^q_reg[1]_0\,
      I2 => \y0_carry__3_i_2__1_0\,
      O => \y0_carry__3_i_6__0_n_0\
    );
\y0_carry__3_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \y0_carry__3_i_10_n_0\,
      I1 => \^q_reg[1]_0\,
      I2 => \y0_carry__3_i_3__0\,
      O => \^q_reg[1]_4\
    );
\y0_carry__3_i_7__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \y0_carry__3_i_10__0_n_0\,
      I1 => \^q_reg[1]_0\,
      I2 => \y0_carry__3_i_3__1\,
      O => \^q_reg[1]_7\
    );
\y0_carry__3_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q_reg[2]_0\,
      I1 => \^q_reg[3]_0\,
      I2 => \y0_carry__4_i_2__0_0\,
      I3 => \^q_reg[4]_0\,
      I4 => \q_reg[23]\,
      O => \y0_carry__3_i_9_n_0\
    );
\y0_carry__3_i_9__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q_reg[2]_0\,
      I1 => \^q_reg[3]_0\,
      I2 => A(13),
      I3 => \^q_reg[4]_0\,
      I4 => A(11),
      O => \y0_carry__3_i_9__0_n_0\
    );
\y0_carry__4_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(11),
      I1 => \y0_carry__4_i_6_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__4_i_7_n_0\,
      I4 => Q(0),
      O => \q_reg[22]\(2)
    );
\y0_carry__4_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[23]\,
      I1 => \y0_carry__4_i_6__0_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__4_i_7__0_n_0\,
      I4 => Q(0),
      O => \q_reg[22]_0\(2)
    );
\y0_carry__4_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(10),
      I1 => \y0_carry__4_i_7_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__4_i_8_n_0\,
      I4 => Q(0),
      O => \q_reg[22]\(1)
    );
\y0_carry__4_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[23]_0\,
      I1 => \y0_carry__4_i_7__0_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__4_i_8__0_n_0\,
      I4 => Q(0),
      O => \q_reg[22]_0\(1)
    );
\y0_carry__4_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(9),
      I1 => \y0_carry__4_i_8_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__3_i_5_n_0\,
      I4 => Q(0),
      O => \q_reg[22]\(0)
    );
\y0_carry__4_i_4__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[23]_1\,
      I1 => \y0_carry__4_i_8__0_n_0\,
      I2 => \^q_reg[0]_0\,
      I3 => \y0_carry__3_i_5__0_n_0\,
      I4 => Q(0),
      O => \q_reg[22]_0\(0)
    );
\y0_carry__4_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q_reg[2]_0\,
      I1 => \^q_reg[3]_0\,
      I2 => \y0_carry__4_i_2__0_0\,
      I3 => \^q_reg[4]_0\,
      I4 => \y0_carry__4_i_2__0_1\,
      O => \^q_reg[2]_1\
    );
\y0_carry__4_i_5__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q_reg[2]_0\,
      I1 => \^q_reg[3]_0\,
      I2 => A(13),
      I3 => \^q_reg[4]_0\,
      I4 => A(12),
      O => \^q_reg[2]_2\
    );
\y0_carry__4_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FF01FF00FE00"
    )
        port map (
      I0 => \^q_reg[1]_0\,
      I1 => \^q_reg[2]_0\,
      I2 => \^q_reg[3]_0\,
      I3 => \y0_carry__4_i_2__0_0\,
      I4 => \^q_reg[4]_0\,
      I5 => \y0_carry__4_i_2__0_1\,
      O => \y0_carry__4_i_6_n_0\
    );
\y0_carry__4_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FF01FF00FE00"
    )
        port map (
      I0 => \^q_reg[1]_0\,
      I1 => \^q_reg[2]_0\,
      I2 => \^q_reg[3]_0\,
      I3 => A(13),
      I4 => \^q_reg[4]_0\,
      I5 => A(12),
      O => \y0_carry__4_i_6__0_n_0\
    );
\y0_carry__4_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FF01FF00FE00"
    )
        port map (
      I0 => \^q_reg[1]_0\,
      I1 => \^q_reg[2]_0\,
      I2 => \^q_reg[3]_0\,
      I3 => \y0_carry__4_i_2__0_0\,
      I4 => \^q_reg[4]_0\,
      I5 => \q_reg[23]\,
      O => \y0_carry__4_i_7_n_0\
    );
\y0_carry__4_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FF01FF00FE00"
    )
        port map (
      I0 => \^q_reg[1]_0\,
      I1 => \^q_reg[2]_0\,
      I2 => \^q_reg[3]_0\,
      I3 => A(13),
      I4 => \^q_reg[4]_0\,
      I5 => A(11),
      O => \y0_carry__4_i_7__0_n_0\
    );
\y0_carry__4_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q_reg[2]_1\,
      I1 => \^q_reg[1]_0\,
      I2 => \y0_carry__3_i_11_n_0\,
      O => \y0_carry__4_i_8_n_0\
    );
\y0_carry__4_i_8__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q_reg[2]_2\,
      I1 => \^q_reg[1]_0\,
      I2 => \y0_carry__3_i_11__0_n_0\,
      O => \y0_carry__4_i_8__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized1\ is
  port (
    s00_axi_awvalid_0 : out STD_LOGIC;
    \q_reg[1]_0\ : out STD_LOGIC;
    \q_reg[23]_0\ : out STD_LOGIC;
    \q_reg[19]_0\ : out STD_LOGIC;
    \q_reg[21]_0\ : out STD_LOGIC;
    \q_reg[23]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[24]_0\ : out STD_LOGIC;
    A : out STD_LOGIC_VECTOR ( 10 downto 0 );
    \q_reg[1]_1\ : out STD_LOGIC;
    \q_reg[23]_2\ : out STD_LOGIC;
    \q_reg[21]_1\ : out STD_LOGIC;
    \q_reg[19]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[22]_0\ : out STD_LOGIC;
    \q_reg[18]_0\ : out STD_LOGIC;
    \q_reg[20]_0\ : out STD_LOGIC;
    \q_reg[14]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[1]_2\ : out STD_LOGIC;
    \q_reg[22]_1\ : out STD_LOGIC;
    \q_reg[20]_1\ : out STD_LOGIC;
    DI : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[9]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[15]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[17]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[23]_3\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[8]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[16]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    \FSM_sequential_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_state_reg[0]_0\ : in STD_LOGIC;
    addr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \q_reg[11]_0\ : in STD_LOGIC;
    \q_reg[11]_1\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[15]_1\ : in STD_LOGIC;
    \q_reg[15]_2\ : in STD_LOGIC;
    \q_reg[19]_2\ : in STD_LOGIC;
    \q_reg[19]_3\ : in STD_LOGIC;
    \q_reg[23]_4\ : in STD_LOGIC;
    \q_reg[23]_5\ : in STD_LOGIC;
    \q_reg[3]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[19]_4\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[22]_2\ : in STD_LOGIC;
    en_reg : in STD_LOGIC;
    \q_reg[24]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_aclk : in STD_LOGIC;
    \q_reg[23]_6\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]_5\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    O : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized1\ : entity is "reg";
end \project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized1\;

architecture STRUCTURE of \project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized1\ is
  signal \^a\ : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal \^di\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^q_reg[14]_0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^q_reg[18]_0\ : STD_LOGIC;
  signal \^q_reg[19]_0\ : STD_LOGIC;
  signal \^q_reg[19]_1\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^q_reg[1]_0\ : STD_LOGIC;
  signal \^q_reg[1]_1\ : STD_LOGIC;
  signal \^q_reg[1]_2\ : STD_LOGIC;
  signal \^q_reg[20]_0\ : STD_LOGIC;
  signal \^q_reg[20]_1\ : STD_LOGIC;
  signal \^q_reg[21]_0\ : STD_LOGIC;
  signal \^q_reg[21]_1\ : STD_LOGIC;
  signal \^q_reg[22]_0\ : STD_LOGIC;
  signal \^q_reg[22]_1\ : STD_LOGIC;
  signal \^q_reg[23]_0\ : STD_LOGIC;
  signal \^q_reg[23]_1\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^q_reg[23]_2\ : STD_LOGIC;
  signal \^q_reg[24]_0\ : STD_LOGIC;
  signal \y0_carry__0_i_5__0_n_0\ : STD_LOGIC;
  signal \y0_carry__0_i_6__0_n_0\ : STD_LOGIC;
  signal \y0_carry__0_i_7__0_n_0\ : STD_LOGIC;
  signal \y0_carry__0_i_8__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_11__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_12__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_13__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_14__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_15__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_16__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_17__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_18__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_19__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_20__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_5__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_6__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_7__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_8__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_9__0_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \y0_carry__2_i_5__0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \y0_carry__3_i_8__0\ : label is "soft_lutpair24";
begin
  A(10 downto 0) <= \^a\(10 downto 0);
  DI(1 downto 0) <= \^di\(1 downto 0);
  \q_reg[14]_0\(2 downto 0) <= \^q_reg[14]_0\(2 downto 0);
  \q_reg[18]_0\ <= \^q_reg[18]_0\;
  \q_reg[19]_0\ <= \^q_reg[19]_0\;
  \q_reg[19]_1\(3 downto 0) <= \^q_reg[19]_1\(3 downto 0);
  \q_reg[1]_0\ <= \^q_reg[1]_0\;
  \q_reg[1]_1\ <= \^q_reg[1]_1\;
  \q_reg[1]_2\ <= \^q_reg[1]_2\;
  \q_reg[20]_0\ <= \^q_reg[20]_0\;
  \q_reg[20]_1\ <= \^q_reg[20]_1\;
  \q_reg[21]_0\ <= \^q_reg[21]_0\;
  \q_reg[21]_1\ <= \^q_reg[21]_1\;
  \q_reg[22]_0\ <= \^q_reg[22]_0\;
  \q_reg[22]_1\ <= \^q_reg[22]_1\;
  \q_reg[23]_0\ <= \^q_reg[23]_0\;
  \q_reg[23]_1\(3 downto 0) <= \^q_reg[23]_1\(3 downto 0);
  \q_reg[23]_2\ <= \^q_reg[23]_2\;
  \q_reg[24]_0\ <= \^q_reg[24]_0\;
\FSM_sequential_state[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \FSM_sequential_state_reg[0]\,
      I3 => \FSM_sequential_state_reg[0]_0\,
      O => s00_axi_awvalid_0
    );
\q_reg[0]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => O(0),
      Q => \^a\(0),
      S => \q_reg[22]_2\
    );
\q_reg[10]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[11]_3\(2),
      Q => \^di\(0),
      S => \q_reg[22]_2\
    );
\q_reg[11]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[11]_3\(3),
      Q => \^di\(1),
      S => \q_reg[22]_2\
    );
\q_reg[12]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[15]_3\(0),
      Q => \^q_reg[14]_0\(0),
      S => \q_reg[22]_2\
    );
\q_reg[13]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[15]_3\(1),
      Q => \^q_reg[14]_0\(1),
      S => \q_reg[22]_2\
    );
\q_reg[14]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[15]_3\(2),
      Q => \^q_reg[14]_0\(2),
      S => \q_reg[22]_2\
    );
\q_reg[15]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[15]_3\(3),
      Q => \^a\(10),
      S => \q_reg[22]_2\
    );
\q_reg[16]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[19]_5\(0),
      Q => \^q_reg[19]_1\(0),
      S => \q_reg[22]_2\
    );
\q_reg[17]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[19]_5\(1),
      Q => \^q_reg[19]_1\(1),
      S => \q_reg[22]_2\
    );
\q_reg[18]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[19]_5\(2),
      Q => \^q_reg[19]_1\(2),
      S => \q_reg[22]_2\
    );
\q_reg[19]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[19]_5\(3),
      Q => \^q_reg[19]_1\(3),
      S => \q_reg[22]_2\
    );
\q_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => O(1),
      Q => \^a\(1),
      S => \q_reg[22]_2\
    );
\q_reg[20]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_6\(0),
      Q => \^q_reg[23]_1\(0),
      S => \q_reg[22]_2\
    );
\q_reg[21]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_6\(1),
      Q => \^q_reg[23]_1\(1),
      S => \q_reg[22]_2\
    );
\q_reg[22]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_6\(2),
      Q => \^q_reg[23]_1\(2),
      S => \q_reg[22]_2\
    );
\q_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_6\(3),
      Q => \^q_reg[23]_1\(3),
      R => \q_reg[22]_2\
    );
\q_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[24]_1\(0),
      Q => \^q_reg[24]_0\,
      R => \q_reg[22]_2\
    );
\q_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => O(2),
      Q => \^a\(2),
      S => \q_reg[22]_2\
    );
\q_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => O(3),
      Q => \^a\(3),
      S => \q_reg[22]_2\
    );
\q_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[7]_2\(0),
      Q => \^a\(4),
      S => \q_reg[22]_2\
    );
\q_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[7]_2\(1),
      Q => \^a\(5),
      S => \q_reg[22]_2\
    );
\q_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[7]_2\(2),
      Q => \^a\(6),
      S => \q_reg[22]_2\
    );
\q_reg[7]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[7]_2\(3),
      Q => \^a\(7),
      S => \q_reg[22]_2\
    );
\q_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[11]_3\(0),
      Q => \^a\(8),
      S => \q_reg[22]_2\
    );
\q_reg[9]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[11]_3\(1),
      Q => \^a\(9),
      S => \q_reg[22]_2\
    );
\y0_carry__0_i_10__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[23]_1\(0),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^q_reg[14]_0\(0),
      O => \^q_reg[20]_0\
    );
\y0_carry__0_i_11__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[23]_1\(1),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^q_reg[14]_0\(1),
      O => \^q_reg[21]_0\
    );
\y0_carry__0_i_12__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[19]_1\(3),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^di\(1),
      O => \^q_reg[19]_0\
    );
\y0_carry__0_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[7]_1\(3),
      I1 => \y0_carry__0_i_5__0_n_0\,
      I2 => addr(0),
      I3 => \y0_carry__0_i_6__0_n_0\,
      I4 => Q(0),
      O => \q_reg[7]_0\(3)
    );
\y0_carry__0_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[7]_1\(2),
      I1 => \y0_carry__0_i_6__0_n_0\,
      I2 => addr(0),
      I3 => \y0_carry__0_i_7__0_n_0\,
      I4 => Q(0),
      O => \q_reg[7]_0\(2)
    );
\y0_carry__0_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[7]_1\(1),
      I1 => \y0_carry__0_i_7__0_n_0\,
      I2 => addr(0),
      I3 => \y0_carry__0_i_8__0_n_0\,
      I4 => Q(0),
      O => \q_reg[7]_0\(1)
    );
\y0_carry__0_i_4__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[7]_1\(0),
      I1 => \y0_carry__0_i_8__0_n_0\,
      I2 => addr(0),
      I3 => \y0_carry_i_5__0_n_0\,
      I4 => Q(0),
      O => \q_reg[7]_0\(0)
    );
\y0_carry__0_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[22]_0\,
      I1 => \^q_reg[18]_0\,
      I2 => addr(1),
      I3 => \^q_reg[20]_0\,
      I4 => addr(2),
      I5 => \y0_carry_i_12__0_n_0\,
      O => \y0_carry__0_i_5__0_n_0\
    );
\y0_carry__0_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[21]_0\,
      I1 => \y0_carry_i_14__0_n_0\,
      I2 => addr(1),
      I3 => \^q_reg[19]_0\,
      I4 => addr(2),
      I5 => \y0_carry_i_16__0_n_0\,
      O => \y0_carry__0_i_6__0_n_0\
    );
\y0_carry__0_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[20]_0\,
      I1 => \y0_carry_i_12__0_n_0\,
      I2 => addr(1),
      I3 => \^q_reg[18]_0\,
      I4 => addr(2),
      I5 => \y0_carry_i_11__0_n_0\,
      O => \y0_carry__0_i_7__0_n_0\
    );
\y0_carry__0_i_8__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[19]_0\,
      I1 => \y0_carry_i_16__0_n_0\,
      I2 => addr(1),
      I3 => \y0_carry_i_14__0_n_0\,
      I4 => addr(2),
      I5 => \y0_carry_i_15__0_n_0\,
      O => \y0_carry__0_i_8__0_n_0\
    );
\y0_carry__0_i_9__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[23]_1\(2),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^q_reg[14]_0\(2),
      O => \^q_reg[22]_0\
    );
\y0_carry__1_i_12__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[23]_1\(3),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^a\(10),
      O => \^q_reg[23]_0\
    );
\y0_carry__1_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \^a\(9),
      I1 => \q_reg[11]_0\,
      I2 => addr(0),
      I3 => \q_reg[11]_1\,
      I4 => Q(0),
      O => \q_reg[9]_0\(0)
    );
\y0_carry__1_i_4__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[11]_2\(0),
      I1 => \^q_reg[1]_0\,
      I2 => addr(0),
      I3 => \y0_carry__0_i_5__0_n_0\,
      I4 => Q(0),
      O => \q_reg[8]_0\(0)
    );
\y0_carry__1_i_8__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[23]_0\,
      I1 => \^q_reg[19]_0\,
      I2 => addr(1),
      I3 => \^q_reg[21]_0\,
      I4 => addr(2),
      I5 => \y0_carry_i_14__0_n_0\,
      O => \^q_reg[1]_0\
    );
\y0_carry__2_i_10__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q_reg[23]_1\(0),
      I1 => addr(2),
      I2 => addr(3),
      I3 => \^q_reg[24]_0\,
      I4 => addr(4),
      I5 => \^q_reg[19]_1\(0),
      O => \^q_reg[20]_1\
    );
\y0_carry__2_i_11__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q_reg[23]_1\(1),
      I1 => addr(2),
      I2 => addr(3),
      I3 => \^q_reg[24]_0\,
      I4 => addr(4),
      I5 => \^q_reg[19]_1\(1),
      O => \^q_reg[21]_1\
    );
\y0_carry__2_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \^a\(10),
      I1 => \q_reg[15]_1\,
      I2 => addr(0),
      I3 => \q_reg[15]_2\,
      I4 => Q(0),
      O => \q_reg[15]_0\(0)
    );
\y0_carry__2_i_5__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q_reg[22]_1\,
      I1 => addr(1),
      I2 => \^q_reg[20]_1\,
      O => \^q_reg[1]_2\
    );
\y0_carry__2_i_9__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q_reg[23]_1\(2),
      I1 => addr(2),
      I2 => addr(3),
      I3 => \^q_reg[24]_0\,
      I4 => addr(4),
      I5 => \^q_reg[19]_1\(2),
      O => \^q_reg[22]_1\
    );
\y0_carry__3_i_12__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q_reg[23]_1\(3),
      I1 => addr(2),
      I2 => addr(3),
      I3 => \^q_reg[24]_0\,
      I4 => addr(4),
      I5 => \^q_reg[19]_1\(3),
      O => \^q_reg[23]_2\
    );
\y0_carry__3_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \^q_reg[19]_1\(1),
      I1 => \q_reg[19]_2\,
      I2 => addr(0),
      I3 => \q_reg[19]_3\,
      I4 => Q(0),
      O => \q_reg[17]_0\(0)
    );
\y0_carry__3_i_4__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[19]_4\(0),
      I1 => \^q_reg[1]_1\,
      I2 => addr(0),
      I3 => \^q_reg[1]_2\,
      I4 => Q(0),
      O => \q_reg[16]_0\(0)
    );
\y0_carry__3_i_8__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q_reg[23]_2\,
      I1 => addr(1),
      I2 => \^q_reg[21]_1\,
      O => \^q_reg[1]_1\
    );
\y0_carry__4_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6665666A999A9995"
    )
        port map (
      I0 => \^q_reg[23]_1\(3),
      I1 => \q_reg[23]_4\,
      I2 => addr(0),
      I3 => addr(1),
      I4 => \q_reg[23]_5\,
      I5 => Q(0),
      O => \q_reg[23]_3\(0)
    );
\y0_carry_i_10__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[19]_1\(2),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^di\(0),
      O => \^q_reg[18]_0\
    );
\y0_carry_i_11__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^q_reg[14]_0\(2),
      I2 => addr(3),
      I3 => \^q_reg[23]_1\(2),
      I4 => addr(4),
      I5 => \^a\(6),
      O => \y0_carry_i_11__0_n_0\
    );
\y0_carry_i_12__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[19]_1\(0),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^a\(8),
      O => \y0_carry_i_12__0_n_0\
    );
\y0_carry_i_13__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^q_reg[14]_0\(0),
      I2 => addr(3),
      I3 => \^q_reg[23]_1\(0),
      I4 => addr(4),
      I5 => \^a\(4),
      O => \y0_carry_i_13__0_n_0\
    );
\y0_carry_i_14__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[19]_1\(1),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^a\(9),
      O => \y0_carry_i_14__0_n_0\
    );
\y0_carry_i_15__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^q_reg[14]_0\(1),
      I2 => addr(3),
      I3 => \^q_reg[23]_1\(1),
      I4 => addr(4),
      I5 => \^a\(5),
      O => \y0_carry_i_15__0_n_0\
    );
\y0_carry_i_16__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^a\(10),
      I2 => addr(3),
      I3 => \^q_reg[23]_1\(3),
      I4 => addr(4),
      I5 => \^a\(7),
      O => \y0_carry_i_16__0_n_0\
    );
\y0_carry_i_17__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^di\(1),
      I2 => addr(3),
      I3 => \^q_reg[19]_1\(3),
      I4 => addr(4),
      I5 => \^a\(3),
      O => \y0_carry_i_17__0_n_0\
    );
\y0_carry_i_18__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^di\(0),
      I2 => addr(3),
      I3 => \^q_reg[19]_1\(2),
      I4 => addr(4),
      I5 => \^a\(2),
      O => \y0_carry_i_18__0_n_0\
    );
\y0_carry_i_19__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^a\(9),
      I2 => addr(3),
      I3 => \^q_reg[19]_1\(1),
      I4 => addr(4),
      I5 => \^a\(1),
      O => \y0_carry_i_19__0_n_0\
    );
\y0_carry_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[3]_0\(3),
      I1 => \y0_carry_i_5__0_n_0\,
      I2 => addr(0),
      I3 => \y0_carry_i_6__0_n_0\,
      I4 => Q(0),
      O => S(3)
    );
\y0_carry_i_20__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^a\(8),
      I2 => addr(3),
      I3 => \^q_reg[19]_1\(0),
      I4 => addr(4),
      I5 => \^a\(0),
      O => \y0_carry_i_20__0_n_0\
    );
\y0_carry_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[3]_0\(2),
      I1 => \y0_carry_i_6__0_n_0\,
      I2 => addr(0),
      I3 => \y0_carry_i_7__0_n_0\,
      I4 => Q(0),
      O => S(2)
    );
\y0_carry_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[3]_0\(1),
      I1 => \y0_carry_i_7__0_n_0\,
      I2 => addr(0),
      I3 => \y0_carry_i_8__0_n_0\,
      I4 => Q(0),
      O => S(1)
    );
\y0_carry_i_4__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \q_reg[3]_0\(0),
      I1 => \y0_carry_i_8__0_n_0\,
      I2 => addr(0),
      I3 => \y0_carry_i_9__0_n_0\,
      I4 => Q(0),
      O => S(0)
    );
\y0_carry_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[18]_0\,
      I1 => \y0_carry_i_11__0_n_0\,
      I2 => addr(1),
      I3 => \y0_carry_i_12__0_n_0\,
      I4 => addr(2),
      I5 => \y0_carry_i_13__0_n_0\,
      O => \y0_carry_i_5__0_n_0\
    );
\y0_carry_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry_i_14__0_n_0\,
      I1 => \y0_carry_i_15__0_n_0\,
      I2 => addr(1),
      I3 => \y0_carry_i_16__0_n_0\,
      I4 => addr(2),
      I5 => \y0_carry_i_17__0_n_0\,
      O => \y0_carry_i_6__0_n_0\
    );
\y0_carry_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry_i_12__0_n_0\,
      I1 => \y0_carry_i_13__0_n_0\,
      I2 => addr(1),
      I3 => \y0_carry_i_11__0_n_0\,
      I4 => addr(2),
      I5 => \y0_carry_i_18__0_n_0\,
      O => \y0_carry_i_7__0_n_0\
    );
\y0_carry_i_8__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry_i_16__0_n_0\,
      I1 => \y0_carry_i_17__0_n_0\,
      I2 => addr(1),
      I3 => \y0_carry_i_15__0_n_0\,
      I4 => addr(2),
      I5 => \y0_carry_i_19__0_n_0\,
      O => \y0_carry_i_8__0_n_0\
    );
\y0_carry_i_9__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry_i_11__0_n_0\,
      I1 => \y0_carry_i_18__0_n_0\,
      I2 => addr(1),
      I3 => \y0_carry_i_13__0_n_0\,
      I4 => addr(2),
      I5 => \y0_carry_i_20__0_n_0\,
      O => \y0_carry_i_9__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized1_1\ is
  port (
    \q_reg[1]_0\ : out STD_LOGIC;
    \q_reg[23]_0\ : out STD_LOGIC;
    \q_reg[19]_0\ : out STD_LOGIC;
    \q_reg[21]_0\ : out STD_LOGIC;
    \q_reg[23]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[24]_0\ : out STD_LOGIC;
    \q_reg[15]_0\ : out STD_LOGIC;
    \q_reg[1]_1\ : out STD_LOGIC;
    \q_reg[23]_2\ : out STD_LOGIC;
    \q_reg[21]_1\ : out STD_LOGIC;
    \q_reg[19]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[22]_0\ : out STD_LOGIC;
    \q_reg[18]_0\ : out STD_LOGIC;
    \q_reg[20]_0\ : out STD_LOGIC;
    \q_reg[14]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[1]_2\ : out STD_LOGIC;
    \q_reg[22]_1\ : out STD_LOGIC;
    \q_reg[20]_1\ : out STD_LOGIC;
    \q_reg[11]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[24]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_1\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[8]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[16]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[9]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[15]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[17]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[23]_3\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    addr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \q_reg[24]_2\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    A : in STD_LOGIC_VECTOR ( 8 downto 0 );
    \q_reg[19]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[11]_1\ : in STD_LOGIC;
    \q_reg[11]_2\ : in STD_LOGIC;
    \q_reg[15]_2\ : in STD_LOGIC;
    \q_reg[15]_3\ : in STD_LOGIC;
    \q_reg[19]_3\ : in STD_LOGIC;
    \q_reg[19]_4\ : in STD_LOGIC;
    \q_reg[23]_4\ : in STD_LOGIC;
    \q_reg[0]_0\ : in STD_LOGIC;
    en_reg : in STD_LOGIC;
    \q_reg[24]_3\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_aclk : in STD_LOGIC;
    \q_reg[23]_5\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]_5\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_2\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    O : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized1_1\ : entity is "reg";
end \project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized1_1\;

architecture STRUCTURE of \project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized1_1\ is
  signal \^q_reg[11]_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^q_reg[14]_0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^q_reg[15]_0\ : STD_LOGIC;
  signal \^q_reg[18]_0\ : STD_LOGIC;
  signal \^q_reg[19]_0\ : STD_LOGIC;
  signal \^q_reg[19]_1\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^q_reg[1]_0\ : STD_LOGIC;
  signal \^q_reg[1]_1\ : STD_LOGIC;
  signal \^q_reg[1]_2\ : STD_LOGIC;
  signal \^q_reg[20]_0\ : STD_LOGIC;
  signal \^q_reg[20]_1\ : STD_LOGIC;
  signal \^q_reg[21]_0\ : STD_LOGIC;
  signal \^q_reg[21]_1\ : STD_LOGIC;
  signal \^q_reg[22]_0\ : STD_LOGIC;
  signal \^q_reg[22]_1\ : STD_LOGIC;
  signal \^q_reg[23]_0\ : STD_LOGIC;
  signal \^q_reg[23]_1\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^q_reg[23]_2\ : STD_LOGIC;
  signal \^q_reg[24]_0\ : STD_LOGIC;
  signal \^q_reg[3]_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \^q_reg[7]_0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \y0_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \y0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \y0_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \y0_carry__0_i_8_n_0\ : STD_LOGIC;
  signal y0_carry_i_11_n_0 : STD_LOGIC;
  signal y0_carry_i_12_n_0 : STD_LOGIC;
  signal y0_carry_i_13_n_0 : STD_LOGIC;
  signal y0_carry_i_14_n_0 : STD_LOGIC;
  signal y0_carry_i_15_n_0 : STD_LOGIC;
  signal y0_carry_i_16_n_0 : STD_LOGIC;
  signal y0_carry_i_17_n_0 : STD_LOGIC;
  signal y0_carry_i_18_n_0 : STD_LOGIC;
  signal y0_carry_i_19_n_0 : STD_LOGIC;
  signal y0_carry_i_20_n_0 : STD_LOGIC;
  signal y0_carry_i_5_n_0 : STD_LOGIC;
  signal y0_carry_i_6_n_0 : STD_LOGIC;
  signal y0_carry_i_7_n_0 : STD_LOGIC;
  signal y0_carry_i_8_n_0 : STD_LOGIC;
  signal y0_carry_i_9_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \y0_carry__2_i_5\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \y0_carry__3_i_8\ : label is "soft_lutpair25";
begin
  \q_reg[11]_0\(3 downto 0) <= \^q_reg[11]_0\(3 downto 0);
  \q_reg[14]_0\(2 downto 0) <= \^q_reg[14]_0\(2 downto 0);
  \q_reg[15]_0\ <= \^q_reg[15]_0\;
  \q_reg[18]_0\ <= \^q_reg[18]_0\;
  \q_reg[19]_0\ <= \^q_reg[19]_0\;
  \q_reg[19]_1\(3 downto 0) <= \^q_reg[19]_1\(3 downto 0);
  \q_reg[1]_0\ <= \^q_reg[1]_0\;
  \q_reg[1]_1\ <= \^q_reg[1]_1\;
  \q_reg[1]_2\ <= \^q_reg[1]_2\;
  \q_reg[20]_0\ <= \^q_reg[20]_0\;
  \q_reg[20]_1\ <= \^q_reg[20]_1\;
  \q_reg[21]_0\ <= \^q_reg[21]_0\;
  \q_reg[21]_1\ <= \^q_reg[21]_1\;
  \q_reg[22]_0\ <= \^q_reg[22]_0\;
  \q_reg[22]_1\ <= \^q_reg[22]_1\;
  \q_reg[23]_0\ <= \^q_reg[23]_0\;
  \q_reg[23]_1\(3 downto 0) <= \^q_reg[23]_1\(3 downto 0);
  \q_reg[23]_2\ <= \^q_reg[23]_2\;
  \q_reg[24]_0\ <= \^q_reg[24]_0\;
  \q_reg[3]_0\(3 downto 0) <= \^q_reg[3]_0\(3 downto 0);
  \q_reg[7]_0\(3 downto 0) <= \^q_reg[7]_0\(3 downto 0);
\q_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => O(0),
      Q => \^q_reg[3]_0\(0),
      R => \q_reg[0]_0\
    );
\q_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[11]_3\(2),
      Q => \^q_reg[11]_0\(2),
      R => \q_reg[0]_0\
    );
\q_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[11]_3\(3),
      Q => \^q_reg[11]_0\(3),
      R => \q_reg[0]_0\
    );
\q_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[15]_4\(0),
      Q => \^q_reg[14]_0\(0),
      R => \q_reg[0]_0\
    );
\q_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[15]_4\(1),
      Q => \^q_reg[14]_0\(1),
      R => \q_reg[0]_0\
    );
\q_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[15]_4\(2),
      Q => \^q_reg[14]_0\(2),
      R => \q_reg[0]_0\
    );
\q_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[15]_4\(3),
      Q => \^q_reg[15]_0\,
      R => \q_reg[0]_0\
    );
\q_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[19]_5\(0),
      Q => \^q_reg[19]_1\(0),
      R => \q_reg[0]_0\
    );
\q_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[19]_5\(1),
      Q => \^q_reg[19]_1\(1),
      R => \q_reg[0]_0\
    );
\q_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[19]_5\(2),
      Q => \^q_reg[19]_1\(2),
      R => \q_reg[0]_0\
    );
\q_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[19]_5\(3),
      Q => \^q_reg[19]_1\(3),
      R => \q_reg[0]_0\
    );
\q_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => O(1),
      Q => \^q_reg[3]_0\(1),
      R => \q_reg[0]_0\
    );
\q_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_5\(0),
      Q => \^q_reg[23]_1\(0),
      R => \q_reg[0]_0\
    );
\q_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_5\(1),
      Q => \^q_reg[23]_1\(1),
      R => \q_reg[0]_0\
    );
\q_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_5\(2),
      Q => \^q_reg[23]_1\(2),
      R => \q_reg[0]_0\
    );
\q_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_5\(3),
      Q => \^q_reg[23]_1\(3),
      R => \q_reg[0]_0\
    );
\q_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[24]_3\(0),
      Q => \^q_reg[24]_0\,
      R => \q_reg[0]_0\
    );
\q_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => O(2),
      Q => \^q_reg[3]_0\(2),
      R => \q_reg[0]_0\
    );
\q_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => O(3),
      Q => \^q_reg[3]_0\(3),
      R => \q_reg[0]_0\
    );
\q_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[7]_2\(0),
      Q => \^q_reg[7]_0\(0),
      R => \q_reg[0]_0\
    );
\q_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[7]_2\(1),
      Q => \^q_reg[7]_0\(1),
      R => \q_reg[0]_0\
    );
\q_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[7]_2\(2),
      Q => \^q_reg[7]_0\(2),
      R => \q_reg[0]_0\
    );
\q_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[7]_2\(3),
      Q => \^q_reg[7]_0\(3),
      R => \q_reg[0]_0\
    );
\q_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[11]_3\(0),
      Q => \^q_reg[11]_0\(0),
      R => \q_reg[0]_0\
    );
\q_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[11]_3\(1),
      Q => \^q_reg[11]_0\(1),
      R => \q_reg[0]_0\
    );
\y0_carry__0_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[23]_1\(0),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^q_reg[14]_0\(0),
      O => \^q_reg[20]_0\
    );
\y0_carry__0_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[23]_1\(1),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^q_reg[14]_0\(1),
      O => \^q_reg[21]_0\
    );
\y0_carry__0_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[19]_1\(3),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^q_reg[11]_0\(3),
      O => \^q_reg[19]_0\
    );
\y0_carry__0_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(7),
      I1 => \y0_carry__0_i_5_n_0\,
      I2 => addr(0),
      I3 => \y0_carry__0_i_6_n_0\,
      I4 => Q(0),
      O => \q_reg[7]_1\(3)
    );
\y0_carry__0_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(6),
      I1 => \y0_carry__0_i_6_n_0\,
      I2 => addr(0),
      I3 => \y0_carry__0_i_7_n_0\,
      I4 => Q(0),
      O => \q_reg[7]_1\(2)
    );
\y0_carry__0_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(5),
      I1 => \y0_carry__0_i_7_n_0\,
      I2 => addr(0),
      I3 => \y0_carry__0_i_8_n_0\,
      I4 => Q(0),
      O => \q_reg[7]_1\(1)
    );
\y0_carry__0_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(4),
      I1 => \y0_carry__0_i_8_n_0\,
      I2 => addr(0),
      I3 => y0_carry_i_5_n_0,
      I4 => Q(0),
      O => \q_reg[7]_1\(0)
    );
\y0_carry__0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[22]_0\,
      I1 => \^q_reg[18]_0\,
      I2 => addr(1),
      I3 => \^q_reg[20]_0\,
      I4 => addr(2),
      I5 => y0_carry_i_12_n_0,
      O => \y0_carry__0_i_5_n_0\
    );
\y0_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[21]_0\,
      I1 => y0_carry_i_14_n_0,
      I2 => addr(1),
      I3 => \^q_reg[19]_0\,
      I4 => addr(2),
      I5 => y0_carry_i_16_n_0,
      O => \y0_carry__0_i_6_n_0\
    );
\y0_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[20]_0\,
      I1 => y0_carry_i_12_n_0,
      I2 => addr(1),
      I3 => \^q_reg[18]_0\,
      I4 => addr(2),
      I5 => y0_carry_i_11_n_0,
      O => \y0_carry__0_i_7_n_0\
    );
\y0_carry__0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[19]_0\,
      I1 => y0_carry_i_16_n_0,
      I2 => addr(1),
      I3 => y0_carry_i_14_n_0,
      I4 => addr(2),
      I5 => y0_carry_i_15_n_0,
      O => \y0_carry__0_i_8_n_0\
    );
\y0_carry__0_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[23]_1\(2),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^q_reg[14]_0\(2),
      O => \^q_reg[22]_0\
    );
\y0_carry__1_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[23]_1\(3),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^q_reg[15]_0\,
      O => \^q_reg[23]_0\
    );
\y0_carry__1_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \^q_reg[11]_0\(1),
      I1 => \q_reg[11]_1\,
      I2 => addr(0),
      I3 => \q_reg[11]_2\,
      I4 => Q(0),
      O => \q_reg[9]_0\(0)
    );
\y0_carry__1_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(8),
      I1 => \^q_reg[1]_0\,
      I2 => addr(0),
      I3 => \y0_carry__0_i_5_n_0\,
      I4 => Q(0),
      O => \q_reg[8]_0\(0)
    );
\y0_carry__1_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[23]_0\,
      I1 => \^q_reg[19]_0\,
      I2 => addr(1),
      I3 => \^q_reg[21]_0\,
      I4 => addr(2),
      I5 => y0_carry_i_14_n_0,
      O => \^q_reg[1]_0\
    );
\y0_carry__2_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q_reg[23]_1\(0),
      I1 => addr(2),
      I2 => addr(3),
      I3 => \^q_reg[24]_0\,
      I4 => addr(4),
      I5 => \^q_reg[19]_1\(0),
      O => \^q_reg[20]_1\
    );
\y0_carry__2_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q_reg[23]_1\(1),
      I1 => addr(2),
      I2 => addr(3),
      I3 => \^q_reg[24]_0\,
      I4 => addr(4),
      I5 => \^q_reg[19]_1\(1),
      O => \^q_reg[21]_1\
    );
\y0_carry__2_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \^q_reg[15]_0\,
      I1 => \q_reg[15]_2\,
      I2 => addr(0),
      I3 => \q_reg[15]_3\,
      I4 => Q(0),
      O => \q_reg[15]_1\(0)
    );
\y0_carry__2_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q_reg[22]_1\,
      I1 => addr(1),
      I2 => \^q_reg[20]_1\,
      O => \^q_reg[1]_2\
    );
\y0_carry__2_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q_reg[23]_1\(2),
      I1 => addr(2),
      I2 => addr(3),
      I3 => \^q_reg[24]_0\,
      I4 => addr(4),
      I5 => \^q_reg[19]_1\(2),
      O => \^q_reg[22]_1\
    );
\y0_carry__3_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q_reg[23]_1\(3),
      I1 => addr(2),
      I2 => addr(3),
      I3 => \^q_reg[24]_0\,
      I4 => addr(4),
      I5 => \^q_reg[19]_1\(3),
      O => \^q_reg[23]_2\
    );
\y0_carry__3_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \^q_reg[19]_1\(1),
      I1 => \q_reg[19]_3\,
      I2 => addr(0),
      I3 => \q_reg[19]_4\,
      I4 => Q(0),
      O => \q_reg[17]_0\(0)
    );
\y0_carry__3_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \q_reg[19]_2\(0),
      I1 => \^q_reg[1]_1\,
      I2 => addr(0),
      I3 => \^q_reg[1]_2\,
      I4 => Q(0),
      O => \q_reg[16]_0\(0)
    );
\y0_carry__3_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q_reg[23]_2\,
      I1 => addr(1),
      I2 => \^q_reg[21]_1\,
      O => \^q_reg[1]_1\
    );
\y0_carry__4_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"999A99956665666A"
    )
        port map (
      I0 => \^q_reg[23]_1\(3),
      I1 => \q_reg[24]_2\,
      I2 => addr(0),
      I3 => addr(1),
      I4 => \q_reg[23]_4\,
      I5 => Q(0),
      O => \q_reg[23]_3\(0)
    );
\y0_carry__5_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \q_reg[24]_2\,
      I2 => Q(0),
      O => \q_reg[24]_1\(0)
    );
y0_carry_i_10: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[19]_1\(2),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^q_reg[11]_0\(2),
      O => \^q_reg[18]_0\
    );
y0_carry_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^q_reg[14]_0\(2),
      I2 => addr(3),
      I3 => \^q_reg[23]_1\(2),
      I4 => addr(4),
      I5 => \^q_reg[7]_0\(2),
      O => y0_carry_i_11_n_0
    );
y0_carry_i_12: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[19]_1\(0),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^q_reg[11]_0\(0),
      O => y0_carry_i_12_n_0
    );
y0_carry_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^q_reg[14]_0\(0),
      I2 => addr(3),
      I3 => \^q_reg[23]_1\(0),
      I4 => addr(4),
      I5 => \^q_reg[7]_0\(0),
      O => y0_carry_i_13_n_0
    );
y0_carry_i_14: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q_reg[19]_1\(1),
      I1 => addr(3),
      I2 => \^q_reg[24]_0\,
      I3 => addr(4),
      I4 => \^q_reg[11]_0\(1),
      O => y0_carry_i_14_n_0
    );
y0_carry_i_15: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^q_reg[14]_0\(1),
      I2 => addr(3),
      I3 => \^q_reg[23]_1\(1),
      I4 => addr(4),
      I5 => \^q_reg[7]_0\(1),
      O => y0_carry_i_15_n_0
    );
y0_carry_i_16: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^q_reg[15]_0\,
      I2 => addr(3),
      I3 => \^q_reg[23]_1\(3),
      I4 => addr(4),
      I5 => \^q_reg[7]_0\(3),
      O => y0_carry_i_16_n_0
    );
y0_carry_i_17: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^q_reg[11]_0\(3),
      I2 => addr(3),
      I3 => \^q_reg[19]_1\(3),
      I4 => addr(4),
      I5 => \^q_reg[3]_0\(3),
      O => y0_carry_i_17_n_0
    );
y0_carry_i_18: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^q_reg[11]_0\(2),
      I2 => addr(3),
      I3 => \^q_reg[19]_1\(2),
      I4 => addr(4),
      I5 => \^q_reg[3]_0\(2),
      O => y0_carry_i_18_n_0
    );
y0_carry_i_19: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^q_reg[11]_0\(1),
      I2 => addr(3),
      I3 => \^q_reg[19]_1\(1),
      I4 => addr(4),
      I5 => \^q_reg[3]_0\(1),
      O => y0_carry_i_19_n_0
    );
\y0_carry_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(3),
      I1 => y0_carry_i_5_n_0,
      I2 => addr(0),
      I3 => y0_carry_i_6_n_0,
      I4 => Q(0),
      O => S(3)
    );
y0_carry_i_20: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[24]_0\,
      I1 => \^q_reg[11]_0\(0),
      I2 => addr(3),
      I3 => \^q_reg[19]_1\(0),
      I4 => addr(4),
      I5 => \^q_reg[3]_0\(0),
      O => y0_carry_i_20_n_0
    );
\y0_carry_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(2),
      I1 => y0_carry_i_6_n_0,
      I2 => addr(0),
      I3 => y0_carry_i_7_n_0,
      I4 => Q(0),
      O => S(2)
    );
\y0_carry_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(1),
      I1 => y0_carry_i_7_n_0,
      I2 => addr(0),
      I3 => y0_carry_i_8_n_0,
      I4 => Q(0),
      O => S(1)
    );
\y0_carry_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => A(0),
      I1 => y0_carry_i_8_n_0,
      I2 => addr(0),
      I3 => y0_carry_i_9_n_0,
      I4 => Q(0),
      O => S(0)
    );
y0_carry_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[18]_0\,
      I1 => y0_carry_i_11_n_0,
      I2 => addr(1),
      I3 => y0_carry_i_12_n_0,
      I4 => addr(2),
      I5 => y0_carry_i_13_n_0,
      O => y0_carry_i_5_n_0
    );
y0_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => y0_carry_i_14_n_0,
      I1 => y0_carry_i_15_n_0,
      I2 => addr(1),
      I3 => y0_carry_i_16_n_0,
      I4 => addr(2),
      I5 => y0_carry_i_17_n_0,
      O => y0_carry_i_6_n_0
    );
y0_carry_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => y0_carry_i_12_n_0,
      I1 => y0_carry_i_13_n_0,
      I2 => addr(1),
      I3 => y0_carry_i_11_n_0,
      I4 => addr(2),
      I5 => y0_carry_i_18_n_0,
      O => y0_carry_i_7_n_0
    );
y0_carry_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => y0_carry_i_16_n_0,
      I1 => y0_carry_i_17_n_0,
      I2 => addr(1),
      I3 => y0_carry_i_15_n_0,
      I4 => addr(2),
      I5 => y0_carry_i_19_n_0,
      O => y0_carry_i_8_n_0
    );
y0_carry_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => y0_carry_i_11_n_0,
      I1 => y0_carry_i_18_n_0,
      I2 => addr(1),
      I3 => y0_carry_i_13_n_0,
      I4 => addr(2),
      I5 => y0_carry_i_20_n_0,
      O => y0_carry_i_9_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized3\ is
  port (
    \q_reg[23]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 23 downto 0 );
    \q_reg[23]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[22]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[24]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[24]_0\ : in STD_LOGIC;
    out_ROM : in STD_LOGIC_VECTOR ( 22 downto 0 );
    en_reg : in STD_LOGIC;
    \q_reg[23]_2\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axi_aclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized3\ : entity is "reg";
end \project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized3\;

architecture STRUCTURE of \project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized3\ is
  signal \^q\ : STD_LOGIC_VECTOR ( 23 downto 0 );
begin
  Q(23 downto 0) <= \^q\(23 downto 0);
\q_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(0),
      Q => \^q\(0),
      R => '0'
    );
\q_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(10),
      Q => \^q\(10),
      R => '0'
    );
\q_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(11),
      Q => \^q\(11),
      R => '0'
    );
\q_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(12),
      Q => \^q\(12),
      R => '0'
    );
\q_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(13),
      Q => \^q\(13),
      R => '0'
    );
\q_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(14),
      Q => \^q\(14),
      R => '0'
    );
\q_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(15),
      Q => \^q\(15),
      R => '0'
    );
\q_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(16),
      Q => \^q\(16),
      R => '0'
    );
\q_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(17),
      Q => \^q\(17),
      R => '0'
    );
\q_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(18),
      Q => \^q\(18),
      R => '0'
    );
\q_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(19),
      Q => \^q\(19),
      R => '0'
    );
\q_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(1),
      Q => \^q\(1),
      R => '0'
    );
\q_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(20),
      Q => \^q\(20),
      R => '0'
    );
\q_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(21),
      Q => \^q\(21),
      R => '0'
    );
\q_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(22),
      Q => \^q\(22),
      R => '0'
    );
\q_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(23),
      Q => \^q\(23),
      R => '0'
    );
\q_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(2),
      Q => \^q\(2),
      R => '0'
    );
\q_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(3),
      Q => \^q\(3),
      R => '0'
    );
\q_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(4),
      Q => \^q\(4),
      R => '0'
    );
\q_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(5),
      Q => \^q\(5),
      R => '0'
    );
\q_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(6),
      Q => \^q\(6),
      R => '0'
    );
\q_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(7),
      Q => \^q\(7),
      R => '0'
    );
\q_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(8),
      Q => \^q\(8),
      R => '0'
    );
\q_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => en_reg,
      D => \q_reg[23]_2\(9),
      Q => \^q\(9),
      R => '0'
    );
\y0_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(7),
      I1 => \^q\(23),
      I2 => out_ROM(7),
      O => \q_reg[7]_0\(3)
    );
\y0_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(6),
      I1 => \^q\(23),
      I2 => out_ROM(6),
      O => \q_reg[7]_0\(2)
    );
\y0_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(23),
      I2 => out_ROM(5),
      O => \q_reg[7]_0\(1)
    );
\y0_carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(23),
      I2 => out_ROM(4),
      O => \q_reg[7]_0\(0)
    );
\y0_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(11),
      I1 => \^q\(23),
      I2 => out_ROM(11),
      O => \q_reg[11]_0\(3)
    );
\y0_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(10),
      I1 => \^q\(23),
      I2 => out_ROM(10),
      O => \q_reg[11]_0\(2)
    );
\y0_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(9),
      I1 => \^q\(23),
      I2 => out_ROM(9),
      O => \q_reg[11]_0\(1)
    );
\y0_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(8),
      I1 => \^q\(23),
      I2 => out_ROM(8),
      O => \q_reg[11]_0\(0)
    );
\y0_carry__2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(15),
      I1 => \^q\(23),
      I2 => out_ROM(15),
      O => \q_reg[15]_0\(3)
    );
\y0_carry__2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(14),
      I1 => \^q\(23),
      I2 => out_ROM(14),
      O => \q_reg[15]_0\(2)
    );
\y0_carry__2_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(13),
      I1 => \^q\(23),
      I2 => out_ROM(13),
      O => \q_reg[15]_0\(1)
    );
\y0_carry__2_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(12),
      I1 => \^q\(23),
      I2 => out_ROM(12),
      O => \q_reg[15]_0\(0)
    );
\y0_carry__3_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(19),
      I1 => \^q\(23),
      I2 => out_ROM(19),
      O => \q_reg[19]_0\(3)
    );
\y0_carry__3_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(18),
      I1 => \^q\(23),
      I2 => out_ROM(18),
      O => \q_reg[19]_0\(2)
    );
\y0_carry__3_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(17),
      I1 => \^q\(23),
      I2 => out_ROM(17),
      O => \q_reg[19]_0\(1)
    );
\y0_carry__3_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(16),
      I1 => \^q\(23),
      I2 => out_ROM(16),
      O => \q_reg[19]_0\(0)
    );
\y0_carry__4_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(22),
      I1 => \^q\(23),
      I2 => out_ROM(22),
      O => \q_reg[22]_0\(2)
    );
\y0_carry__4_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(21),
      I1 => \^q\(23),
      I2 => out_ROM(21),
      O => \q_reg[22]_0\(1)
    );
\y0_carry__4_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(20),
      I1 => \^q\(23),
      I2 => out_ROM(20),
      O => \q_reg[22]_0\(0)
    );
\y0_carry__5_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \^q\(23),
      I1 => \q_reg[24]\(0),
      I2 => \q_reg[24]_0\,
      O => \q_reg[23]_1\(0)
    );
y0_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(23),
      O => \q_reg[23]_0\
    );
y0_carry_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(23),
      I2 => out_ROM(3),
      O => S(3)
    );
y0_carry_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(23),
      I2 => out_ROM(2),
      O => S(2)
    );
y0_carry_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(23),
      I2 => out_ROM(1),
      O => S(1)
    );
\y0_carry_i_5__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(23),
      I2 => out_ROM(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity project_audio_2_design_project_audio_2_ip_0_0_control_path is
  port (
    \q_reg[1]\ : out STD_LOGIC;
    addr : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \FSM_sequential_state_reg[0]\ : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 23 downto 0 );
    en_reg : out STD_LOGIC;
    \q_reg[1]_0\ : out STD_LOGIC;
    \q_reg[2]\ : out STD_LOGIC;
    \q_reg[1]_1\ : out STD_LOGIC;
    \q_reg[1]_2\ : out STD_LOGIC;
    \q_reg[1]_3\ : out STD_LOGIC;
    \q_reg[2]_0\ : out STD_LOGIC;
    \q_reg[1]_4\ : out STD_LOGIC;
    \q_reg[1]_5\ : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[14]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[19]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[22]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[11]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[14]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[19]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[22]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    p_0_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \FSM_sequential_state_reg[0]_0\ : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    O : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[3]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \y0_carry__1_i_2__0\ : in STD_LOGIC;
    \y0_carry__1_i_2__0_0\ : in STD_LOGIC;
    \y0_carry__1_i_2__0_1\ : in STD_LOGIC;
    \y0_carry__2_i_1__0\ : in STD_LOGIC;
    \y0_carry__3_i_2__0\ : in STD_LOGIC;
    \y0_carry__4_i_2__0\ : in STD_LOGIC;
    \y0_carry__4_i_2__0_0\ : in STD_LOGIC;
    \y0_carry__1_i_3__0\ : in STD_LOGIC;
    \y0_carry__1_i_3__0_0\ : in STD_LOGIC;
    \y0_carry__1_i_3__0_1\ : in STD_LOGIC;
    \y0_carry__2_i_3__0\ : in STD_LOGIC;
    \y0_carry__3_i_3__0\ : in STD_LOGIC;
    \q_reg[23]\ : in STD_LOGIC;
    \q_reg[23]_0\ : in STD_LOGIC;
    \q_reg[23]_1\ : in STD_LOGIC;
    \q_reg[19]_2\ : in STD_LOGIC;
    \q_reg[19]_3\ : in STD_LOGIC;
    \y0_carry__1_i_6\ : in STD_LOGIC;
    \y0_carry__1_i_7\ : in STD_LOGIC;
    \y0_carry__1_i_2__1\ : in STD_LOGIC;
    \y0_carry__1_i_2__1_0\ : in STD_LOGIC;
    \y0_carry__1_i_2__1_1\ : in STD_LOGIC;
    \y0_carry__2_i_1__1\ : in STD_LOGIC;
    \y0_carry__3_i_2__1\ : in STD_LOGIC;
    A : in STD_LOGIC_VECTOR ( 13 downto 0 );
    \y0_carry__1_i_3__1\ : in STD_LOGIC;
    \y0_carry__1_i_3__1_0\ : in STD_LOGIC;
    \y0_carry__1_i_3__1_1\ : in STD_LOGIC;
    \y0_carry__2_i_3__1\ : in STD_LOGIC;
    \y0_carry__3_i_3__1\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[11]_1\ : in STD_LOGIC;
    \q_reg[11]_2\ : in STD_LOGIC;
    \q_reg[15]_0\ : in STD_LOGIC;
    \q_reg[15]_1\ : in STD_LOGIC;
    \q_reg[15]_2\ : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of project_audio_2_design_project_audio_2_ip_0_0_control_path : entity is "control_path";
end project_audio_2_design_project_audio_2_ip_0_0_control_path;

architecture STRUCTURE of project_audio_2_design_project_audio_2_ip_0_0_control_path is
  signal \^fsm_sequential_state_reg[0]\ : STD_LOGIC;
  signal \^en_reg\ : STD_LOGIC;
  signal \^q_reg[1]\ : STD_LOGIC;
begin
  \FSM_sequential_state_reg[0]\ <= \^fsm_sequential_state_reg[0]\;
  en_reg <= \^en_reg\;
  \q_reg[1]\ <= \^q_reg[1]\;
fsm0: entity work.project_audio_2_design_project_audio_2_ip_0_0_fsm
     port map (
      D(23 downto 0) => D(23 downto 0),
      \FSM_sequential_state_reg[0]_0\ => \^fsm_sequential_state_reg[0]\,
      \FSM_sequential_state_reg[0]_1\ => \^en_reg\,
      \FSM_sequential_state_reg[0]_2\ => \FSM_sequential_state_reg[0]_0\,
      \FSM_sequential_state_reg[0]_3\ => \^q_reg[1]\,
      O(3 downto 0) => O(3 downto 0),
      p_0_in(1 downto 0) => p_0_in(1 downto 0),
      \q_reg[11]\(3 downto 0) => \q_reg[11]_0\(3 downto 0),
      \q_reg[15]\(3 downto 0) => \q_reg[15]\(3 downto 0),
      \q_reg[19]\(3 downto 0) => \q_reg[19]_1\(3 downto 0),
      \q_reg[3]\(3 downto 0) => \q_reg[3]\(3 downto 0),
      \q_reg[7]\(3 downto 0) => \q_reg[7]\(3 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_wdata(23 downto 0) => s00_axi_wdata(23 downto 0)
    );
regr: entity work.project_audio_2_design_project_audio_2_ip_0_0_reg
     port map (
      A(13 downto 0) => A(13 downto 0),
      Q(0) => Q(0),
      S(1 downto 0) => S(1 downto 0),
      \q_reg[0]_0\ => addr(0),
      \q_reg[0]_1\ => \^fsm_sequential_state_reg[0]\,
      \q_reg[0]_2\ => \^en_reg\,
      \q_reg[11]\(1 downto 0) => \q_reg[11]\(1 downto 0),
      \q_reg[11]_0\ => \q_reg[11]_1\,
      \q_reg[11]_1\ => \q_reg[11]_2\,
      \q_reg[14]\(2 downto 0) => \q_reg[14]\(2 downto 0),
      \q_reg[14]_0\(2 downto 0) => \q_reg[14]_0\(2 downto 0),
      \q_reg[15]\ => \q_reg[15]_0\,
      \q_reg[15]_0\ => \q_reg[15]_1\,
      \q_reg[15]_1\ => \q_reg[15]_2\,
      \q_reg[19]\(1 downto 0) => \q_reg[19]\(1 downto 0),
      \q_reg[19]_0\(1 downto 0) => \q_reg[19]_0\(1 downto 0),
      \q_reg[19]_1\ => \q_reg[19]_2\,
      \q_reg[19]_2\ => \q_reg[19]_3\,
      \q_reg[1]_0\ => addr(1),
      \q_reg[1]_1\ => \^q_reg[1]\,
      \q_reg[1]_2\ => \q_reg[1]_0\,
      \q_reg[1]_3\ => \q_reg[1]_1\,
      \q_reg[1]_4\ => \q_reg[1]_2\,
      \q_reg[1]_5\ => \q_reg[1]_3\,
      \q_reg[1]_6\ => \q_reg[1]_4\,
      \q_reg[1]_7\ => \q_reg[1]_5\,
      \q_reg[22]\(2 downto 0) => \q_reg[22]\(2 downto 0),
      \q_reg[22]_0\(2 downto 0) => \q_reg[22]_0\(2 downto 0),
      \q_reg[23]\ => \q_reg[23]\,
      \q_reg[23]_0\ => \q_reg[23]_0\,
      \q_reg[23]_1\ => \q_reg[23]_1\,
      \q_reg[2]_0\ => addr(2),
      \q_reg[2]_1\ => \q_reg[2]\,
      \q_reg[2]_2\ => \q_reg[2]_0\,
      \q_reg[3]_0\ => addr(3),
      \q_reg[4]_0\ => addr(4),
      s00_axi_aclk => s00_axi_aclk,
      \y0_carry__1_i_2__0_0\ => \y0_carry__1_i_2__0\,
      \y0_carry__1_i_2__0_1\ => \y0_carry__1_i_2__0_0\,
      \y0_carry__1_i_2__0_2\ => \y0_carry__1_i_2__0_1\,
      \y0_carry__1_i_2__1_0\ => \y0_carry__1_i_2__1\,
      \y0_carry__1_i_2__1_1\ => \y0_carry__1_i_2__1_0\,
      \y0_carry__1_i_2__1_2\ => \y0_carry__1_i_2__1_1\,
      \y0_carry__1_i_3__0\ => \y0_carry__1_i_3__0\,
      \y0_carry__1_i_3__0_0\ => \y0_carry__1_i_3__0_0\,
      \y0_carry__1_i_3__0_1\ => \y0_carry__1_i_3__0_1\,
      \y0_carry__1_i_3__1\ => \y0_carry__1_i_3__1\,
      \y0_carry__1_i_3__1_0\ => \y0_carry__1_i_3__1_0\,
      \y0_carry__1_i_3__1_1\ => \y0_carry__1_i_3__1_1\,
      \y0_carry__1_i_6_0\ => \y0_carry__1_i_6\,
      \y0_carry__1_i_7_0\ => \y0_carry__1_i_7\,
      \y0_carry__2_i_1__0\ => \y0_carry__2_i_1__0\,
      \y0_carry__2_i_1__1\ => \y0_carry__2_i_1__1\,
      \y0_carry__2_i_3__0_0\ => \y0_carry__2_i_3__0\,
      \y0_carry__2_i_3__1_0\ => \y0_carry__2_i_3__1\,
      \y0_carry__3_i_2__0_0\ => \y0_carry__3_i_2__0\,
      \y0_carry__3_i_2__1_0\ => \y0_carry__3_i_2__1\,
      \y0_carry__3_i_3__0\ => \y0_carry__3_i_3__0\,
      \y0_carry__3_i_3__1\ => \y0_carry__3_i_3__1\,
      \y0_carry__4_i_2__0_0\ => \y0_carry__4_i_2__0\,
      \y0_carry__4_i_2__0_1\ => \y0_carry__4_i_2__0_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity project_audio_2_design_project_audio_2_ip_0_0_data_path is
  port (
    \q_reg[3]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    O : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[24]\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[11]_0\ : out STD_LOGIC;
    \q_reg[10]\ : out STD_LOGIC;
    \q_reg[14]\ : out STD_LOGIC;
    \q_reg[13]\ : out STD_LOGIC;
    \q_reg[12]\ : out STD_LOGIC;
    \q_reg[19]_0\ : out STD_LOGIC;
    \q_reg[18]\ : out STD_LOGIC;
    \q_reg[17]\ : out STD_LOGIC;
    \q_reg[16]\ : out STD_LOGIC;
    \q_reg[23]\ : out STD_LOGIC;
    \q_reg[22]\ : out STD_LOGIC;
    \q_reg[21]\ : out STD_LOGIC;
    \q_reg[20]\ : out STD_LOGIC;
    s00_axi_awvalid_0 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \q_reg[23]_0\ : out STD_LOGIC;
    \q_reg[19]_1\ : out STD_LOGIC;
    \q_reg[21]_0\ : out STD_LOGIC;
    \q_reg[24]_0\ : out STD_LOGIC;
    \q_reg[23]_1\ : out STD_LOGIC;
    \q_reg[21]_1\ : out STD_LOGIC;
    \q_reg[22]_0\ : out STD_LOGIC;
    \q_reg[18]_0\ : out STD_LOGIC;
    \q_reg[20]_0\ : out STD_LOGIC;
    \q_reg[22]_1\ : out STD_LOGIC;
    \q_reg[20]_1\ : out STD_LOGIC;
    \q_reg[23]_2\ : out STD_LOGIC;
    \q_reg[19]_2\ : out STD_LOGIC;
    \q_reg[21]_2\ : out STD_LOGIC;
    \q_reg[23]_3\ : out STD_LOGIC;
    \q_reg[21]_3\ : out STD_LOGIC;
    \q_reg[22]_2\ : out STD_LOGIC;
    \q_reg[18]_1\ : out STD_LOGIC;
    \q_reg[20]_2\ : out STD_LOGIC;
    \q_reg[22]_3\ : out STD_LOGIC;
    \q_reg[20]_3\ : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    addr : in STD_LOGIC_VECTOR ( 4 downto 0 );
    S : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[15]_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[19]_3\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[23]_4\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[11]_1\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[15]_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[19]_4\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[23]_5\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    \FSM_sequential_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_state_reg[0]_0\ : in STD_LOGIC;
    \axi_rdata_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \axi_rdata_reg[0]\ : in STD_LOGIC;
    \q_reg[11]_2\ : in STD_LOGIC;
    \q_reg[15]_2\ : in STD_LOGIC;
    \q_reg[19]_5\ : in STD_LOGIC;
    \q_reg[23]_6\ : in STD_LOGIC;
    \q_reg[11]_3\ : in STD_LOGIC;
    \q_reg[15]_3\ : in STD_LOGIC;
    \q_reg[19]_6\ : in STD_LOGIC;
    \q_reg[23]_7\ : in STD_LOGIC;
    \q_reg[0]\ : in STD_LOGIC;
    en_reg : in STD_LOGIC;
    \q_reg[23]_8\ : in STD_LOGIC_VECTOR ( 23 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of project_audio_2_design_project_audio_2_ip_0_0_data_path : entity is "data_path";
end project_audio_2_design_project_audio_2_ip_0_0_data_path;

architecture STRUCTURE of project_audio_2_design_project_audio_2_ip_0_0_data_path is
  signal A : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal Mux_X_n_0 : STD_LOGIC;
  signal Mux_X_n_1 : STD_LOGIC;
  signal Mux_X_n_10 : STD_LOGIC;
  signal Mux_X_n_11 : STD_LOGIC;
  signal Mux_X_n_12 : STD_LOGIC;
  signal Mux_X_n_13 : STD_LOGIC;
  signal Mux_X_n_14 : STD_LOGIC;
  signal Mux_X_n_15 : STD_LOGIC;
  signal Mux_X_n_16 : STD_LOGIC;
  signal Mux_X_n_17 : STD_LOGIC;
  signal Mux_X_n_18 : STD_LOGIC;
  signal Mux_X_n_19 : STD_LOGIC;
  signal Mux_X_n_2 : STD_LOGIC;
  signal Mux_X_n_20 : STD_LOGIC;
  signal Mux_X_n_21 : STD_LOGIC;
  signal Mux_X_n_22 : STD_LOGIC;
  signal Mux_X_n_23 : STD_LOGIC;
  signal Mux_X_n_24 : STD_LOGIC;
  signal Mux_X_n_3 : STD_LOGIC;
  signal Mux_X_n_4 : STD_LOGIC;
  signal Mux_X_n_5 : STD_LOGIC;
  signal Mux_X_n_6 : STD_LOGIC;
  signal Mux_X_n_7 : STD_LOGIC;
  signal Mux_X_n_8 : STD_LOGIC;
  signal Mux_X_n_9 : STD_LOGIC;
  signal Mux_Y_n_0 : STD_LOGIC;
  signal Mux_Y_n_1 : STD_LOGIC;
  signal Mux_Y_n_10 : STD_LOGIC;
  signal Mux_Y_n_11 : STD_LOGIC;
  signal Mux_Y_n_12 : STD_LOGIC;
  signal Mux_Y_n_13 : STD_LOGIC;
  signal Mux_Y_n_14 : STD_LOGIC;
  signal Mux_Y_n_15 : STD_LOGIC;
  signal Mux_Y_n_16 : STD_LOGIC;
  signal Mux_Y_n_17 : STD_LOGIC;
  signal Mux_Y_n_18 : STD_LOGIC;
  signal Mux_Y_n_19 : STD_LOGIC;
  signal Mux_Y_n_2 : STD_LOGIC;
  signal Mux_Y_n_20 : STD_LOGIC;
  signal Mux_Y_n_21 : STD_LOGIC;
  signal Mux_Y_n_22 : STD_LOGIC;
  signal Mux_Y_n_23 : STD_LOGIC;
  signal Mux_Y_n_24 : STD_LOGIC;
  signal Mux_Y_n_3 : STD_LOGIC;
  signal Mux_Y_n_4 : STD_LOGIC;
  signal Mux_Y_n_5 : STD_LOGIC;
  signal Mux_Y_n_6 : STD_LOGIC;
  signal Mux_Y_n_7 : STD_LOGIC;
  signal Mux_Y_n_8 : STD_LOGIC;
  signal Mux_Y_n_9 : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal ROM_1_n_23 : STD_LOGIC;
  signal out_ROM : STD_LOGIC_VECTOR ( 22 downto 0 );
  signal \^q_reg[10]\ : STD_LOGIC;
  signal \^q_reg[11]_0\ : STD_LOGIC;
  signal \^q_reg[12]\ : STD_LOGIC;
  signal \^q_reg[13]\ : STD_LOGIC;
  signal \^q_reg[14]\ : STD_LOGIC;
  signal \^q_reg[16]\ : STD_LOGIC;
  signal \^q_reg[17]\ : STD_LOGIC;
  signal \^q_reg[18]\ : STD_LOGIC;
  signal \^q_reg[19]_0\ : STD_LOGIC;
  signal \^q_reg[20]\ : STD_LOGIC;
  signal \^q_reg[21]\ : STD_LOGIC;
  signal \^q_reg[22]\ : STD_LOGIC;
  signal \^q_reg[23]\ : STD_LOGIC;
  signal \^q_reg[24]\ : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \^q_reg[24]_0\ : STD_LOGIC;
  signal reg_X_n_1 : STD_LOGIC;
  signal reg_X_n_21 : STD_LOGIC;
  signal reg_X_n_34 : STD_LOGIC;
  signal reg_X_n_39 : STD_LOGIC;
  signal reg_X_n_40 : STD_LOGIC;
  signal reg_X_n_41 : STD_LOGIC;
  signal reg_X_n_42 : STD_LOGIC;
  signal reg_X_n_43 : STD_LOGIC;
  signal reg_X_n_44 : STD_LOGIC;
  signal reg_X_n_45 : STD_LOGIC;
  signal reg_X_n_46 : STD_LOGIC;
  signal reg_X_n_47 : STD_LOGIC;
  signal reg_X_n_48 : STD_LOGIC;
  signal reg_X_n_49 : STD_LOGIC;
  signal reg_X_n_50 : STD_LOGIC;
  signal reg_X_n_51 : STD_LOGIC;
  signal reg_X_n_52 : STD_LOGIC;
  signal reg_Y_n_0 : STD_LOGIC;
  signal reg_Y_n_10 : STD_LOGIC;
  signal reg_Y_n_23 : STD_LOGIC;
  signal reg_Y_n_28 : STD_LOGIC;
  signal reg_Y_n_29 : STD_LOGIC;
  signal reg_Y_n_30 : STD_LOGIC;
  signal reg_Y_n_31 : STD_LOGIC;
  signal reg_Y_n_32 : STD_LOGIC;
  signal reg_Y_n_33 : STD_LOGIC;
  signal reg_Y_n_34 : STD_LOGIC;
  signal reg_Y_n_35 : STD_LOGIC;
  signal reg_Y_n_36 : STD_LOGIC;
  signal reg_Y_n_37 : STD_LOGIC;
  signal reg_Y_n_38 : STD_LOGIC;
  signal reg_Y_n_39 : STD_LOGIC;
  signal reg_Y_n_40 : STD_LOGIC;
  signal reg_Y_n_41 : STD_LOGIC;
  signal reg_Y_n_42 : STD_LOGIC;
  signal reg_Y_n_43 : STD_LOGIC;
  signal reg_Y_n_44 : STD_LOGIC;
  signal reg_Y_n_45 : STD_LOGIC;
  signal reg_Y_n_46 : STD_LOGIC;
  signal reg_Y_n_47 : STD_LOGIC;
  signal reg_Y_n_48 : STD_LOGIC;
  signal reg_Y_n_49 : STD_LOGIC;
  signal reg_Y_n_50 : STD_LOGIC;
  signal reg_Y_n_51 : STD_LOGIC;
  signal reg_Y_n_52 : STD_LOGIC;
  signal reg_Y_n_9 : STD_LOGIC;
  signal reg_Z_n_0 : STD_LOGIC;
  signal reg_Z_n_10 : STD_LOGIC;
  signal reg_Z_n_11 : STD_LOGIC;
  signal reg_Z_n_12 : STD_LOGIC;
  signal reg_Z_n_13 : STD_LOGIC;
  signal reg_Z_n_14 : STD_LOGIC;
  signal reg_Z_n_15 : STD_LOGIC;
  signal reg_Z_n_16 : STD_LOGIC;
  signal reg_Z_n_17 : STD_LOGIC;
  signal reg_Z_n_18 : STD_LOGIC;
  signal reg_Z_n_19 : STD_LOGIC;
  signal reg_Z_n_2 : STD_LOGIC;
  signal reg_Z_n_20 : STD_LOGIC;
  signal reg_Z_n_21 : STD_LOGIC;
  signal reg_Z_n_22 : STD_LOGIC;
  signal reg_Z_n_23 : STD_LOGIC;
  signal reg_Z_n_24 : STD_LOGIC;
  signal reg_Z_n_25 : STD_LOGIC;
  signal reg_Z_n_26 : STD_LOGIC;
  signal reg_Z_n_27 : STD_LOGIC;
  signal reg_Z_n_28 : STD_LOGIC;
  signal reg_Z_n_29 : STD_LOGIC;
  signal reg_Z_n_3 : STD_LOGIC;
  signal reg_Z_n_30 : STD_LOGIC;
  signal reg_Z_n_31 : STD_LOGIC;
  signal reg_Z_n_32 : STD_LOGIC;
  signal reg_Z_n_33 : STD_LOGIC;
  signal reg_Z_n_34 : STD_LOGIC;
  signal reg_Z_n_35 : STD_LOGIC;
  signal reg_Z_n_36 : STD_LOGIC;
  signal reg_Z_n_37 : STD_LOGIC;
  signal reg_Z_n_38 : STD_LOGIC;
  signal reg_Z_n_39 : STD_LOGIC;
  signal reg_Z_n_4 : STD_LOGIC;
  signal reg_Z_n_40 : STD_LOGIC;
  signal reg_Z_n_41 : STD_LOGIC;
  signal reg_Z_n_42 : STD_LOGIC;
  signal reg_Z_n_43 : STD_LOGIC;
  signal reg_Z_n_44 : STD_LOGIC;
  signal reg_Z_n_45 : STD_LOGIC;
  signal reg_Z_n_46 : STD_LOGIC;
  signal reg_Z_n_47 : STD_LOGIC;
  signal reg_Z_n_48 : STD_LOGIC;
  signal reg_Z_n_5 : STD_LOGIC;
  signal reg_Z_n_6 : STD_LOGIC;
  signal reg_Z_n_7 : STD_LOGIC;
  signal reg_Z_n_8 : STD_LOGIC;
  signal reg_Z_n_9 : STD_LOGIC;
begin
  Q(0) <= \^q\(0);
  \q_reg[10]\ <= \^q_reg[10]\;
  \q_reg[11]_0\ <= \^q_reg[11]_0\;
  \q_reg[12]\ <= \^q_reg[12]\;
  \q_reg[13]\ <= \^q_reg[13]\;
  \q_reg[14]\ <= \^q_reg[14]\;
  \q_reg[16]\ <= \^q_reg[16]\;
  \q_reg[17]\ <= \^q_reg[17]\;
  \q_reg[18]\ <= \^q_reg[18]\;
  \q_reg[19]_0\ <= \^q_reg[19]_0\;
  \q_reg[20]\ <= \^q_reg[20]\;
  \q_reg[21]\ <= \^q_reg[21]\;
  \q_reg[22]\ <= \^q_reg[22]\;
  \q_reg[23]\ <= \^q_reg[23]\;
  \q_reg[24]\(13 downto 0) <= \^q_reg[24]\(13 downto 0);
  \q_reg[24]_0\ <= \^q_reg[24]_0\;
Mux_X: entity work.project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1
     port map (
      A(10) => A(15),
      A(9 downto 0) => A(9 downto 0),
      DI(1 downto 0) => \^q_reg[24]\(1 downto 0),
      O(3) => Mux_X_n_0,
      O(2) => Mux_X_n_1,
      O(1) => Mux_X_n_2,
      O(0) => Mux_X_n_3,
      S(3) => reg_Y_n_39,
      S(2) => reg_Y_n_40,
      S(1) => reg_Y_n_41,
      S(0) => reg_Y_n_42,
      \q_reg[11]\(3) => Mux_X_n_8,
      \q_reg[11]\(2) => Mux_X_n_9,
      \q_reg[11]\(1) => Mux_X_n_10,
      \q_reg[11]\(0) => Mux_X_n_11,
      \q_reg[11]_0\(3 downto 2) => S(1 downto 0),
      \q_reg[11]_0\(1) => reg_X_n_39,
      \q_reg[11]_0\(0) => reg_Y_n_47,
      \q_reg[15]\(3) => Mux_X_n_12,
      \q_reg[15]\(2) => Mux_X_n_13,
      \q_reg[15]\(1) => Mux_X_n_14,
      \q_reg[15]\(0) => Mux_X_n_15,
      \q_reg[15]_0\(2 downto 0) => \^q_reg[24]\(4 downto 2),
      \q_reg[15]_1\(3) => reg_X_n_40,
      \q_reg[15]_1\(2 downto 0) => \q_reg[15]_0\(2 downto 0),
      \q_reg[19]\(3) => Mux_X_n_16,
      \q_reg[19]\(2) => Mux_X_n_17,
      \q_reg[19]\(1) => Mux_X_n_18,
      \q_reg[19]\(0) => Mux_X_n_19,
      \q_reg[19]_0\(3 downto 0) => \^q_reg[24]\(8 downto 5),
      \q_reg[19]_1\(3 downto 2) => \q_reg[19]_3\(1 downto 0),
      \q_reg[19]_1\(1) => reg_X_n_41,
      \q_reg[19]_1\(0) => reg_Y_n_48,
      \q_reg[23]\(3) => Mux_X_n_20,
      \q_reg[23]\(2) => Mux_X_n_21,
      \q_reg[23]\(1) => Mux_X_n_22,
      \q_reg[23]\(0) => Mux_X_n_23,
      \q_reg[23]_0\(0) => Mux_X_n_24,
      \q_reg[23]_1\(3 downto 0) => \^q_reg[24]\(12 downto 9),
      \q_reg[23]_2\(3) => reg_X_n_42,
      \q_reg[23]_2\(2 downto 0) => \q_reg[23]_4\(2 downto 0),
      \q_reg[24]\(0) => reg_Y_n_38,
      \q_reg[3]\ => reg_Z_n_0,
      \q_reg[7]\(3) => Mux_X_n_4,
      \q_reg[7]\(2) => Mux_X_n_5,
      \q_reg[7]\(1) => Mux_X_n_6,
      \q_reg[7]\(0) => Mux_X_n_7,
      \q_reg[7]_0\(3) => reg_Y_n_43,
      \q_reg[7]_0\(2) => reg_Y_n_44,
      \q_reg[7]_0\(1) => reg_Y_n_45,
      \q_reg[7]_0\(0) => reg_Y_n_46
    );
Mux_Y: entity work.project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1_0
     port map (
      D(31 downto 0) => D(31 downto 0),
      DI(3) => reg_Y_n_34,
      DI(2) => reg_Y_n_35,
      DI(1) => reg_Y_n_36,
      DI(0) => reg_Y_n_37,
      O(3) => Mux_Y_n_0,
      O(2) => Mux_Y_n_1,
      O(1) => Mux_Y_n_2,
      O(0) => Mux_Y_n_3,
      Q(0) => \^q\(0),
      S(3) => reg_X_n_43,
      S(2) => reg_X_n_44,
      S(1) => reg_X_n_45,
      S(0) => reg_X_n_46,
      axi_araddr(1 downto 0) => axi_araddr(1 downto 0),
      \axi_rdata_reg[0]\ => \axi_rdata_reg[0]\,
      \axi_rdata_reg[11]\(3) => Mux_X_n_8,
      \axi_rdata_reg[11]\(2) => Mux_X_n_9,
      \axi_rdata_reg[11]\(1) => Mux_X_n_10,
      \axi_rdata_reg[11]\(0) => Mux_X_n_11,
      \axi_rdata_reg[15]\(3) => Mux_X_n_12,
      \axi_rdata_reg[15]\(2) => Mux_X_n_13,
      \axi_rdata_reg[15]\(1) => Mux_X_n_14,
      \axi_rdata_reg[15]\(0) => Mux_X_n_15,
      \axi_rdata_reg[19]\(3) => Mux_X_n_16,
      \axi_rdata_reg[19]\(2) => Mux_X_n_17,
      \axi_rdata_reg[19]\(1) => Mux_X_n_18,
      \axi_rdata_reg[19]\(0) => Mux_X_n_19,
      \axi_rdata_reg[23]\(3) => Mux_X_n_20,
      \axi_rdata_reg[23]\(2) => Mux_X_n_21,
      \axi_rdata_reg[23]\(1) => Mux_X_n_22,
      \axi_rdata_reg[23]\(0) => Mux_X_n_23,
      \axi_rdata_reg[31]\(31 downto 0) => \axi_rdata_reg[31]\(31 downto 0),
      \axi_rdata_reg[31]_0\(0) => Mux_X_n_24,
      \axi_rdata_reg[3]\(3) => Mux_X_n_0,
      \axi_rdata_reg[3]\(2) => Mux_X_n_1,
      \axi_rdata_reg[3]\(1) => Mux_X_n_2,
      \axi_rdata_reg[3]\(0) => Mux_X_n_3,
      \axi_rdata_reg[7]\(3) => Mux_X_n_4,
      \axi_rdata_reg[7]\(2) => Mux_X_n_5,
      \axi_rdata_reg[7]\(1) => Mux_X_n_6,
      \axi_rdata_reg[7]\(0) => Mux_X_n_7,
      \q_reg[11]\(3) => Mux_Y_n_8,
      \q_reg[11]\(2) => Mux_Y_n_9,
      \q_reg[11]\(1) => Mux_Y_n_10,
      \q_reg[11]\(0) => Mux_Y_n_11,
      \q_reg[11]_0\(3) => \^q_reg[11]_0\,
      \q_reg[11]_0\(2) => \^q_reg[10]\,
      \q_reg[11]_0\(1) => reg_Y_n_28,
      \q_reg[11]_0\(0) => reg_Y_n_29,
      \q_reg[11]_1\(3 downto 2) => \q_reg[11]_1\(1 downto 0),
      \q_reg[11]_1\(1) => reg_Y_n_49,
      \q_reg[11]_1\(0) => reg_X_n_51,
      \q_reg[15]\(3) => Mux_Y_n_12,
      \q_reg[15]\(2) => Mux_Y_n_13,
      \q_reg[15]\(1) => Mux_Y_n_14,
      \q_reg[15]\(0) => Mux_Y_n_15,
      \q_reg[15]_0\ => reg_Y_n_9,
      \q_reg[15]_1\(2) => \^q_reg[14]\,
      \q_reg[15]_1\(1) => \^q_reg[13]\,
      \q_reg[15]_1\(0) => \^q_reg[12]\,
      \q_reg[15]_2\(3) => reg_Y_n_50,
      \q_reg[15]_2\(2 downto 0) => \q_reg[15]_1\(2 downto 0),
      \q_reg[19]\(3) => Mux_Y_n_16,
      \q_reg[19]\(2) => Mux_Y_n_17,
      \q_reg[19]\(1) => Mux_Y_n_18,
      \q_reg[19]\(0) => Mux_Y_n_19,
      \q_reg[19]_0\(3) => \^q_reg[19]_0\,
      \q_reg[19]_0\(2) => \^q_reg[18]\,
      \q_reg[19]_0\(1) => \^q_reg[17]\,
      \q_reg[19]_0\(0) => \^q_reg[16]\,
      \q_reg[19]_1\(3 downto 2) => \q_reg[19]_4\(1 downto 0),
      \q_reg[19]_1\(1) => reg_Y_n_51,
      \q_reg[19]_1\(0) => reg_X_n_52,
      \q_reg[23]\(3) => Mux_Y_n_20,
      \q_reg[23]\(2) => Mux_Y_n_21,
      \q_reg[23]\(1) => Mux_Y_n_22,
      \q_reg[23]\(0) => Mux_Y_n_23,
      \q_reg[23]_0\(0) => Mux_Y_n_24,
      \q_reg[23]_1\(3) => \^q_reg[23]\,
      \q_reg[23]_1\(2) => \^q_reg[22]\,
      \q_reg[23]_1\(1) => \^q_reg[21]\,
      \q_reg[23]_1\(0) => \^q_reg[20]\,
      \q_reg[23]_2\(3) => reg_Y_n_52,
      \q_reg[23]_2\(2 downto 0) => \q_reg[23]_5\(2 downto 0),
      \q_reg[24]\(0) => reg_Z_n_25,
      \q_reg[7]\(3) => Mux_Y_n_4,
      \q_reg[7]\(2) => Mux_Y_n_5,
      \q_reg[7]\(1) => Mux_Y_n_6,
      \q_reg[7]\(0) => Mux_Y_n_7,
      \q_reg[7]_0\(3) => reg_Y_n_30,
      \q_reg[7]_0\(2) => reg_Y_n_31,
      \q_reg[7]_0\(1) => reg_Y_n_32,
      \q_reg[7]_0\(0) => reg_Y_n_33,
      \q_reg[7]_1\(3) => reg_X_n_47,
      \q_reg[7]_1\(2) => reg_X_n_48,
      \q_reg[7]_1\(1) => reg_X_n_49,
      \q_reg[7]_1\(0) => reg_X_n_50
    );
Mux_Z: entity work.\project_audio_2_design_project_audio_2_ip_0_0_Mux_2_1__parameterized2\
     port map (
      O(3 downto 0) => O(3 downto 0),
      Q(22) => reg_Z_n_2,
      Q(21) => reg_Z_n_3,
      Q(20) => reg_Z_n_4,
      Q(19) => reg_Z_n_5,
      Q(18) => reg_Z_n_6,
      Q(17) => reg_Z_n_7,
      Q(16) => reg_Z_n_8,
      Q(15) => reg_Z_n_9,
      Q(14) => reg_Z_n_10,
      Q(13) => reg_Z_n_11,
      Q(12) => reg_Z_n_12,
      Q(11) => reg_Z_n_13,
      Q(10) => reg_Z_n_14,
      Q(9) => reg_Z_n_15,
      Q(8) => reg_Z_n_16,
      Q(7) => reg_Z_n_17,
      Q(6) => reg_Z_n_18,
      Q(5) => reg_Z_n_19,
      Q(4) => reg_Z_n_20,
      Q(3) => reg_Z_n_21,
      Q(2) => reg_Z_n_22,
      Q(1) => reg_Z_n_23,
      Q(0) => reg_Z_n_24,
      S(3) => reg_Z_n_26,
      S(2) => reg_Z_n_27,
      S(1) => reg_Z_n_28,
      S(0) => reg_Z_n_29,
      \q_reg[11]\(3 downto 0) => \q_reg[11]\(3 downto 0),
      \q_reg[11]_0\(3) => reg_Z_n_34,
      \q_reg[11]_0\(2) => reg_Z_n_35,
      \q_reg[11]_0\(1) => reg_Z_n_36,
      \q_reg[11]_0\(0) => reg_Z_n_37,
      \q_reg[15]\(3 downto 0) => \q_reg[15]\(3 downto 0),
      \q_reg[15]_0\(3) => reg_Z_n_38,
      \q_reg[15]_0\(2) => reg_Z_n_39,
      \q_reg[15]_0\(1) => reg_Z_n_40,
      \q_reg[15]_0\(0) => reg_Z_n_41,
      \q_reg[19]\(3 downto 0) => \q_reg[19]\(3 downto 0),
      \q_reg[19]_0\(3) => reg_Z_n_42,
      \q_reg[19]_0\(2) => reg_Z_n_43,
      \q_reg[19]_0\(1) => reg_Z_n_44,
      \q_reg[19]_0\(0) => reg_Z_n_45,
      \q_reg[23]\(3) => ROM_1_n_23,
      \q_reg[23]\(2) => reg_Z_n_46,
      \q_reg[23]\(1) => reg_Z_n_47,
      \q_reg[23]\(0) => reg_Z_n_48,
      \q_reg[3]\(3 downto 0) => \q_reg[3]\(3 downto 0),
      \q_reg[3]_0\ => reg_Z_n_0,
      \q_reg[7]\(3 downto 0) => \q_reg[7]\(3 downto 0),
      \q_reg[7]_0\(3) => reg_Z_n_30,
      \q_reg[7]_0\(2) => reg_Z_n_31,
      \q_reg[7]_0\(1) => reg_Z_n_32,
      \q_reg[7]_0\(0) => reg_Z_n_33
    );
ROM_1: entity work.project_audio_2_design_project_audio_2_ip_0_0_ROM
     port map (
      addr(4 downto 0) => addr(4 downto 0),
      data_reg_0(0) => ROM_1_n_23,
      out_ROM(22 downto 0) => out_ROM(22 downto 0),
      s00_axi_aclk => s00_axi_aclk
    );
reg_X: entity work.\project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized1\
     port map (
      A(10) => A(15),
      A(9 downto 0) => A(9 downto 0),
      DI(1 downto 0) => \^q_reg[24]\(1 downto 0),
      \FSM_sequential_state_reg[0]\ => \FSM_sequential_state_reg[0]\,
      \FSM_sequential_state_reg[0]_0\ => \FSM_sequential_state_reg[0]_0\,
      O(3) => Mux_X_n_0,
      O(2) => Mux_X_n_1,
      O(1) => Mux_X_n_2,
      O(0) => Mux_X_n_3,
      Q(0) => \^q\(0),
      S(3) => reg_X_n_43,
      S(2) => reg_X_n_44,
      S(1) => reg_X_n_45,
      S(0) => reg_X_n_46,
      addr(4 downto 0) => addr(4 downto 0),
      en_reg => en_reg,
      \q_reg[11]_0\ => \q_reg[11]_2\,
      \q_reg[11]_1\ => reg_Y_n_0,
      \q_reg[11]_2\(0) => reg_Y_n_29,
      \q_reg[11]_3\(3) => Mux_X_n_8,
      \q_reg[11]_3\(2) => Mux_X_n_9,
      \q_reg[11]_3\(1) => Mux_X_n_10,
      \q_reg[11]_3\(0) => Mux_X_n_11,
      \q_reg[14]_0\(2 downto 0) => \^q_reg[24]\(4 downto 2),
      \q_reg[15]_0\(0) => reg_X_n_40,
      \q_reg[15]_1\ => reg_Y_n_23,
      \q_reg[15]_2\ => \q_reg[15]_2\,
      \q_reg[15]_3\(3) => Mux_X_n_12,
      \q_reg[15]_3\(2) => Mux_X_n_13,
      \q_reg[15]_3\(1) => Mux_X_n_14,
      \q_reg[15]_3\(0) => Mux_X_n_15,
      \q_reg[16]_0\(0) => reg_X_n_52,
      \q_reg[17]_0\(0) => reg_X_n_41,
      \q_reg[18]_0\ => \q_reg[18]_1\,
      \q_reg[19]_0\ => \q_reg[19]_2\,
      \q_reg[19]_1\(3 downto 0) => \^q_reg[24]\(8 downto 5),
      \q_reg[19]_2\ => \q_reg[19]_5\,
      \q_reg[19]_3\ => reg_Y_n_10,
      \q_reg[19]_4\(0) => \^q_reg[16]\,
      \q_reg[19]_5\(3) => Mux_X_n_16,
      \q_reg[19]_5\(2) => Mux_X_n_17,
      \q_reg[19]_5\(1) => Mux_X_n_18,
      \q_reg[19]_5\(0) => Mux_X_n_19,
      \q_reg[1]_0\ => reg_X_n_1,
      \q_reg[1]_1\ => reg_X_n_21,
      \q_reg[1]_2\ => reg_X_n_34,
      \q_reg[20]_0\ => \q_reg[20]_2\,
      \q_reg[20]_1\ => \q_reg[20]_3\,
      \q_reg[21]_0\ => \q_reg[21]_2\,
      \q_reg[21]_1\ => \q_reg[21]_3\,
      \q_reg[22]_0\ => \q_reg[22]_2\,
      \q_reg[22]_1\ => \q_reg[22]_3\,
      \q_reg[22]_2\ => \q_reg[0]\,
      \q_reg[23]_0\ => \q_reg[23]_2\,
      \q_reg[23]_1\(3 downto 0) => \^q_reg[24]\(12 downto 9),
      \q_reg[23]_2\ => \q_reg[23]_3\,
      \q_reg[23]_3\(0) => reg_X_n_42,
      \q_reg[23]_4\ => \^q_reg[24]_0\,
      \q_reg[23]_5\ => \q_reg[23]_6\,
      \q_reg[23]_6\(3) => Mux_X_n_20,
      \q_reg[23]_6\(2) => Mux_X_n_21,
      \q_reg[23]_6\(1) => Mux_X_n_22,
      \q_reg[23]_6\(0) => Mux_X_n_23,
      \q_reg[24]_0\ => \^q_reg[24]\(13),
      \q_reg[24]_1\(0) => Mux_X_n_24,
      \q_reg[3]_0\(3) => reg_Y_n_34,
      \q_reg[3]_0\(2) => reg_Y_n_35,
      \q_reg[3]_0\(1) => reg_Y_n_36,
      \q_reg[3]_0\(0) => reg_Y_n_37,
      \q_reg[7]_0\(3) => reg_X_n_47,
      \q_reg[7]_0\(2) => reg_X_n_48,
      \q_reg[7]_0\(1) => reg_X_n_49,
      \q_reg[7]_0\(0) => reg_X_n_50,
      \q_reg[7]_1\(3) => reg_Y_n_30,
      \q_reg[7]_1\(2) => reg_Y_n_31,
      \q_reg[7]_1\(1) => reg_Y_n_32,
      \q_reg[7]_1\(0) => reg_Y_n_33,
      \q_reg[7]_2\(3) => Mux_X_n_4,
      \q_reg[7]_2\(2) => Mux_X_n_5,
      \q_reg[7]_2\(1) => Mux_X_n_6,
      \q_reg[7]_2\(0) => Mux_X_n_7,
      \q_reg[8]_0\(0) => reg_X_n_51,
      \q_reg[9]_0\(0) => reg_X_n_39,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_awvalid_0 => s00_axi_awvalid_0,
      s00_axi_wvalid => s00_axi_wvalid
    );
reg_Y: entity work.\project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized1_1\
     port map (
      A(8 downto 0) => A(8 downto 0),
      O(3) => Mux_Y_n_0,
      O(2) => Mux_Y_n_1,
      O(1) => Mux_Y_n_2,
      O(0) => Mux_Y_n_3,
      Q(0) => \^q\(0),
      S(3) => reg_Y_n_39,
      S(2) => reg_Y_n_40,
      S(1) => reg_Y_n_41,
      S(0) => reg_Y_n_42,
      addr(4 downto 0) => addr(4 downto 0),
      en_reg => en_reg,
      \q_reg[0]_0\ => \q_reg[0]\,
      \q_reg[11]_0\(3) => \^q_reg[11]_0\,
      \q_reg[11]_0\(2) => \^q_reg[10]\,
      \q_reg[11]_0\(1) => reg_Y_n_28,
      \q_reg[11]_0\(0) => reg_Y_n_29,
      \q_reg[11]_1\ => \q_reg[11]_3\,
      \q_reg[11]_2\ => reg_X_n_1,
      \q_reg[11]_3\(3) => Mux_Y_n_8,
      \q_reg[11]_3\(2) => Mux_Y_n_9,
      \q_reg[11]_3\(1) => Mux_Y_n_10,
      \q_reg[11]_3\(0) => Mux_Y_n_11,
      \q_reg[14]_0\(2) => \^q_reg[14]\,
      \q_reg[14]_0\(1) => \^q_reg[13]\,
      \q_reg[14]_0\(0) => \^q_reg[12]\,
      \q_reg[15]_0\ => reg_Y_n_9,
      \q_reg[15]_1\(0) => reg_Y_n_50,
      \q_reg[15]_2\ => reg_X_n_34,
      \q_reg[15]_3\ => \q_reg[15]_3\,
      \q_reg[15]_4\(3) => Mux_Y_n_12,
      \q_reg[15]_4\(2) => Mux_Y_n_13,
      \q_reg[15]_4\(1) => Mux_Y_n_14,
      \q_reg[15]_4\(0) => Mux_Y_n_15,
      \q_reg[16]_0\(0) => reg_Y_n_48,
      \q_reg[17]_0\(0) => reg_Y_n_51,
      \q_reg[18]_0\ => \q_reg[18]_0\,
      \q_reg[19]_0\ => \q_reg[19]_1\,
      \q_reg[19]_1\(3) => \^q_reg[19]_0\,
      \q_reg[19]_1\(2) => \^q_reg[18]\,
      \q_reg[19]_1\(1) => \^q_reg[17]\,
      \q_reg[19]_1\(0) => \^q_reg[16]\,
      \q_reg[19]_2\(0) => \^q_reg[24]\(5),
      \q_reg[19]_3\ => \q_reg[19]_6\,
      \q_reg[19]_4\ => reg_X_n_21,
      \q_reg[19]_5\(3) => Mux_Y_n_16,
      \q_reg[19]_5\(2) => Mux_Y_n_17,
      \q_reg[19]_5\(1) => Mux_Y_n_18,
      \q_reg[19]_5\(0) => Mux_Y_n_19,
      \q_reg[1]_0\ => reg_Y_n_0,
      \q_reg[1]_1\ => reg_Y_n_10,
      \q_reg[1]_2\ => reg_Y_n_23,
      \q_reg[20]_0\ => \q_reg[20]_0\,
      \q_reg[20]_1\ => \q_reg[20]_1\,
      \q_reg[21]_0\ => \q_reg[21]_0\,
      \q_reg[21]_1\ => \q_reg[21]_1\,
      \q_reg[22]_0\ => \q_reg[22]_0\,
      \q_reg[22]_1\ => \q_reg[22]_1\,
      \q_reg[23]_0\ => \q_reg[23]_0\,
      \q_reg[23]_1\(3) => \^q_reg[23]\,
      \q_reg[23]_1\(2) => \^q_reg[22]\,
      \q_reg[23]_1\(1) => \^q_reg[21]\,
      \q_reg[23]_1\(0) => \^q_reg[20]\,
      \q_reg[23]_2\ => \q_reg[23]_1\,
      \q_reg[23]_3\(0) => reg_Y_n_52,
      \q_reg[23]_4\ => \q_reg[23]_7\,
      \q_reg[23]_5\(3) => Mux_Y_n_20,
      \q_reg[23]_5\(2) => Mux_Y_n_21,
      \q_reg[23]_5\(1) => Mux_Y_n_22,
      \q_reg[23]_5\(0) => Mux_Y_n_23,
      \q_reg[24]_0\ => \^q_reg[24]_0\,
      \q_reg[24]_1\(0) => reg_Y_n_38,
      \q_reg[24]_2\ => \^q_reg[24]\(13),
      \q_reg[24]_3\(0) => Mux_Y_n_24,
      \q_reg[3]_0\(3) => reg_Y_n_34,
      \q_reg[3]_0\(2) => reg_Y_n_35,
      \q_reg[3]_0\(1) => reg_Y_n_36,
      \q_reg[3]_0\(0) => reg_Y_n_37,
      \q_reg[7]_0\(3) => reg_Y_n_30,
      \q_reg[7]_0\(2) => reg_Y_n_31,
      \q_reg[7]_0\(1) => reg_Y_n_32,
      \q_reg[7]_0\(0) => reg_Y_n_33,
      \q_reg[7]_1\(3) => reg_Y_n_43,
      \q_reg[7]_1\(2) => reg_Y_n_44,
      \q_reg[7]_1\(1) => reg_Y_n_45,
      \q_reg[7]_1\(0) => reg_Y_n_46,
      \q_reg[7]_2\(3) => Mux_Y_n_4,
      \q_reg[7]_2\(2) => Mux_Y_n_5,
      \q_reg[7]_2\(1) => Mux_Y_n_6,
      \q_reg[7]_2\(0) => Mux_Y_n_7,
      \q_reg[8]_0\(0) => reg_Y_n_47,
      \q_reg[9]_0\(0) => reg_Y_n_49,
      s00_axi_aclk => s00_axi_aclk
    );
reg_Z: entity work.\project_audio_2_design_project_audio_2_ip_0_0_reg__parameterized3\
     port map (
      Q(23) => \^q\(0),
      Q(22) => reg_Z_n_2,
      Q(21) => reg_Z_n_3,
      Q(20) => reg_Z_n_4,
      Q(19) => reg_Z_n_5,
      Q(18) => reg_Z_n_6,
      Q(17) => reg_Z_n_7,
      Q(16) => reg_Z_n_8,
      Q(15) => reg_Z_n_9,
      Q(14) => reg_Z_n_10,
      Q(13) => reg_Z_n_11,
      Q(12) => reg_Z_n_12,
      Q(11) => reg_Z_n_13,
      Q(10) => reg_Z_n_14,
      Q(9) => reg_Z_n_15,
      Q(8) => reg_Z_n_16,
      Q(7) => reg_Z_n_17,
      Q(6) => reg_Z_n_18,
      Q(5) => reg_Z_n_19,
      Q(4) => reg_Z_n_20,
      Q(3) => reg_Z_n_21,
      Q(2) => reg_Z_n_22,
      Q(1) => reg_Z_n_23,
      Q(0) => reg_Z_n_24,
      S(3) => reg_Z_n_26,
      S(2) => reg_Z_n_27,
      S(1) => reg_Z_n_28,
      S(0) => reg_Z_n_29,
      en_reg => en_reg,
      out_ROM(22 downto 0) => out_ROM(22 downto 0),
      \q_reg[11]_0\(3) => reg_Z_n_34,
      \q_reg[11]_0\(2) => reg_Z_n_35,
      \q_reg[11]_0\(1) => reg_Z_n_36,
      \q_reg[11]_0\(0) => reg_Z_n_37,
      \q_reg[15]_0\(3) => reg_Z_n_38,
      \q_reg[15]_0\(2) => reg_Z_n_39,
      \q_reg[15]_0\(1) => reg_Z_n_40,
      \q_reg[15]_0\(0) => reg_Z_n_41,
      \q_reg[19]_0\(3) => reg_Z_n_42,
      \q_reg[19]_0\(2) => reg_Z_n_43,
      \q_reg[19]_0\(1) => reg_Z_n_44,
      \q_reg[19]_0\(0) => reg_Z_n_45,
      \q_reg[22]_0\(2) => reg_Z_n_46,
      \q_reg[22]_0\(1) => reg_Z_n_47,
      \q_reg[22]_0\(0) => reg_Z_n_48,
      \q_reg[23]_0\ => reg_Z_n_0,
      \q_reg[23]_1\(0) => reg_Z_n_25,
      \q_reg[23]_2\(23 downto 0) => \q_reg[23]_8\(23 downto 0),
      \q_reg[24]\(0) => \^q_reg[24]\(13),
      \q_reg[24]_0\ => \^q_reg[24]_0\,
      \q_reg[7]_0\(3) => reg_Z_n_30,
      \q_reg[7]_0\(2) => reg_Z_n_31,
      \q_reg[7]_0\(1) => reg_Z_n_32,
      \q_reg[7]_0\(0) => reg_Z_n_33,
      s00_axi_aclk => s00_axi_aclk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity project_audio_2_design_project_audio_2_ip_0_0_cordic_rotation is
  port (
    s00_axi_awvalid_0 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    p_0_in : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 23 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    \FSM_sequential_state_reg[0]\ : in STD_LOGIC;
    \FSM_sequential_state_reg[0]_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of project_audio_2_design_project_audio_2_ip_0_0_cordic_rotation : entity is "cordic_rotation";
end project_audio_2_design_project_audio_2_ip_0_0_cordic_rotation;

architecture STRUCTURE of project_audio_2_design_project_audio_2_ip_0_0_cordic_rotation is
  signal A : STD_LOGIC_VECTOR ( 24 downto 10 );
  signal addr : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal control_path0_n_0 : STD_LOGIC;
  signal control_path0_n_10 : STD_LOGIC;
  signal control_path0_n_11 : STD_LOGIC;
  signal control_path0_n_12 : STD_LOGIC;
  signal control_path0_n_13 : STD_LOGIC;
  signal control_path0_n_14 : STD_LOGIC;
  signal control_path0_n_15 : STD_LOGIC;
  signal control_path0_n_16 : STD_LOGIC;
  signal control_path0_n_17 : STD_LOGIC;
  signal control_path0_n_18 : STD_LOGIC;
  signal control_path0_n_19 : STD_LOGIC;
  signal control_path0_n_20 : STD_LOGIC;
  signal control_path0_n_21 : STD_LOGIC;
  signal control_path0_n_22 : STD_LOGIC;
  signal control_path0_n_23 : STD_LOGIC;
  signal control_path0_n_24 : STD_LOGIC;
  signal control_path0_n_25 : STD_LOGIC;
  signal control_path0_n_26 : STD_LOGIC;
  signal control_path0_n_27 : STD_LOGIC;
  signal control_path0_n_28 : STD_LOGIC;
  signal control_path0_n_29 : STD_LOGIC;
  signal control_path0_n_30 : STD_LOGIC;
  signal control_path0_n_32 : STD_LOGIC;
  signal control_path0_n_33 : STD_LOGIC;
  signal control_path0_n_34 : STD_LOGIC;
  signal control_path0_n_35 : STD_LOGIC;
  signal control_path0_n_36 : STD_LOGIC;
  signal control_path0_n_37 : STD_LOGIC;
  signal control_path0_n_38 : STD_LOGIC;
  signal control_path0_n_39 : STD_LOGIC;
  signal control_path0_n_40 : STD_LOGIC;
  signal control_path0_n_41 : STD_LOGIC;
  signal control_path0_n_42 : STD_LOGIC;
  signal control_path0_n_43 : STD_LOGIC;
  signal control_path0_n_44 : STD_LOGIC;
  signal control_path0_n_45 : STD_LOGIC;
  signal control_path0_n_46 : STD_LOGIC;
  signal control_path0_n_47 : STD_LOGIC;
  signal control_path0_n_48 : STD_LOGIC;
  signal control_path0_n_49 : STD_LOGIC;
  signal control_path0_n_50 : STD_LOGIC;
  signal control_path0_n_51 : STD_LOGIC;
  signal control_path0_n_52 : STD_LOGIC;
  signal control_path0_n_53 : STD_LOGIC;
  signal control_path0_n_54 : STD_LOGIC;
  signal control_path0_n_55 : STD_LOGIC;
  signal control_path0_n_56 : STD_LOGIC;
  signal control_path0_n_57 : STD_LOGIC;
  signal control_path0_n_58 : STD_LOGIC;
  signal control_path0_n_59 : STD_LOGIC;
  signal control_path0_n_6 : STD_LOGIC;
  signal control_path0_n_7 : STD_LOGIC;
  signal control_path0_n_8 : STD_LOGIC;
  signal control_path0_n_9 : STD_LOGIC;
  signal data_path0_n_0 : STD_LOGIC;
  signal data_path0_n_1 : STD_LOGIC;
  signal data_path0_n_10 : STD_LOGIC;
  signal data_path0_n_100 : STD_LOGIC;
  signal data_path0_n_101 : STD_LOGIC;
  signal data_path0_n_102 : STD_LOGIC;
  signal data_path0_n_103 : STD_LOGIC;
  signal data_path0_n_104 : STD_LOGIC;
  signal data_path0_n_105 : STD_LOGIC;
  signal data_path0_n_11 : STD_LOGIC;
  signal data_path0_n_12 : STD_LOGIC;
  signal data_path0_n_13 : STD_LOGIC;
  signal data_path0_n_14 : STD_LOGIC;
  signal data_path0_n_15 : STD_LOGIC;
  signal data_path0_n_16 : STD_LOGIC;
  signal data_path0_n_17 : STD_LOGIC;
  signal data_path0_n_18 : STD_LOGIC;
  signal data_path0_n_19 : STD_LOGIC;
  signal data_path0_n_2 : STD_LOGIC;
  signal data_path0_n_20 : STD_LOGIC;
  signal data_path0_n_21 : STD_LOGIC;
  signal data_path0_n_22 : STD_LOGIC;
  signal data_path0_n_23 : STD_LOGIC;
  signal data_path0_n_3 : STD_LOGIC;
  signal data_path0_n_38 : STD_LOGIC;
  signal data_path0_n_39 : STD_LOGIC;
  signal data_path0_n_4 : STD_LOGIC;
  signal data_path0_n_40 : STD_LOGIC;
  signal data_path0_n_41 : STD_LOGIC;
  signal data_path0_n_42 : STD_LOGIC;
  signal data_path0_n_43 : STD_LOGIC;
  signal data_path0_n_44 : STD_LOGIC;
  signal data_path0_n_45 : STD_LOGIC;
  signal data_path0_n_46 : STD_LOGIC;
  signal data_path0_n_47 : STD_LOGIC;
  signal data_path0_n_48 : STD_LOGIC;
  signal data_path0_n_49 : STD_LOGIC;
  signal data_path0_n_5 : STD_LOGIC;
  signal data_path0_n_50 : STD_LOGIC;
  signal data_path0_n_51 : STD_LOGIC;
  signal data_path0_n_6 : STD_LOGIC;
  signal data_path0_n_7 : STD_LOGIC;
  signal data_path0_n_8 : STD_LOGIC;
  signal data_path0_n_85 : STD_LOGIC;
  signal data_path0_n_86 : STD_LOGIC;
  signal data_path0_n_87 : STD_LOGIC;
  signal data_path0_n_88 : STD_LOGIC;
  signal data_path0_n_89 : STD_LOGIC;
  signal data_path0_n_9 : STD_LOGIC;
  signal data_path0_n_90 : STD_LOGIC;
  signal data_path0_n_91 : STD_LOGIC;
  signal data_path0_n_92 : STD_LOGIC;
  signal data_path0_n_93 : STD_LOGIC;
  signal data_path0_n_94 : STD_LOGIC;
  signal data_path0_n_95 : STD_LOGIC;
  signal data_path0_n_96 : STD_LOGIC;
  signal data_path0_n_97 : STD_LOGIC;
  signal data_path0_n_98 : STD_LOGIC;
  signal data_path0_n_99 : STD_LOGIC;
  signal en_reg : STD_LOGIC;
  signal \^s00_axi_awvalid_0\ : STD_LOGIC;
begin
  s00_axi_awvalid_0 <= \^s00_axi_awvalid_0\;
control_path0: entity work.project_audio_2_design_project_audio_2_ip_0_0_control_path
     port map (
      A(13 downto 5) => A(24 downto 16),
      A(4 downto 0) => A(14 downto 10),
      D(23) => control_path0_n_7,
      D(22) => control_path0_n_8,
      D(21) => control_path0_n_9,
      D(20) => control_path0_n_10,
      D(19) => control_path0_n_11,
      D(18) => control_path0_n_12,
      D(17) => control_path0_n_13,
      D(16) => control_path0_n_14,
      D(15) => control_path0_n_15,
      D(14) => control_path0_n_16,
      D(13) => control_path0_n_17,
      D(12) => control_path0_n_18,
      D(11) => control_path0_n_19,
      D(10) => control_path0_n_20,
      D(9) => control_path0_n_21,
      D(8) => control_path0_n_22,
      D(7) => control_path0_n_23,
      D(6) => control_path0_n_24,
      D(5) => control_path0_n_25,
      D(4) => control_path0_n_26,
      D(3) => control_path0_n_27,
      D(2) => control_path0_n_28,
      D(1) => control_path0_n_29,
      D(0) => control_path0_n_30,
      \FSM_sequential_state_reg[0]\ => control_path0_n_6,
      \FSM_sequential_state_reg[0]_0\ => \^s00_axi_awvalid_0\,
      O(3) => data_path0_n_20,
      O(2) => data_path0_n_21,
      O(1) => data_path0_n_22,
      O(0) => data_path0_n_23,
      Q(0) => data_path0_n_38,
      S(1) => control_path0_n_40,
      S(0) => control_path0_n_41,
      addr(4 downto 0) => addr(4 downto 0),
      en_reg => en_reg,
      p_0_in(1 downto 0) => p_0_in(1 downto 0),
      \q_reg[11]\(1) => control_path0_n_50,
      \q_reg[11]\(0) => control_path0_n_51,
      \q_reg[11]_0\(3) => data_path0_n_8,
      \q_reg[11]_0\(2) => data_path0_n_9,
      \q_reg[11]_0\(1) => data_path0_n_10,
      \q_reg[11]_0\(0) => data_path0_n_11,
      \q_reg[11]_1\ => data_path0_n_40,
      \q_reg[11]_2\ => data_path0_n_39,
      \q_reg[14]\(2) => control_path0_n_42,
      \q_reg[14]\(1) => control_path0_n_43,
      \q_reg[14]\(0) => control_path0_n_44,
      \q_reg[14]_0\(2) => control_path0_n_52,
      \q_reg[14]_0\(1) => control_path0_n_53,
      \q_reg[14]_0\(0) => control_path0_n_54,
      \q_reg[15]\(3) => data_path0_n_12,
      \q_reg[15]\(2) => data_path0_n_13,
      \q_reg[15]\(1) => data_path0_n_14,
      \q_reg[15]\(0) => data_path0_n_15,
      \q_reg[15]_0\ => data_path0_n_43,
      \q_reg[15]_1\ => data_path0_n_42,
      \q_reg[15]_2\ => data_path0_n_41,
      \q_reg[19]\(1) => control_path0_n_45,
      \q_reg[19]\(0) => control_path0_n_46,
      \q_reg[19]_0\(1) => control_path0_n_55,
      \q_reg[19]_0\(0) => control_path0_n_56,
      \q_reg[19]_1\(3) => data_path0_n_16,
      \q_reg[19]_1\(2) => data_path0_n_17,
      \q_reg[19]_1\(1) => data_path0_n_18,
      \q_reg[19]_1\(0) => data_path0_n_19,
      \q_reg[19]_2\ => data_path0_n_44,
      \q_reg[19]_3\ => data_path0_n_45,
      \q_reg[1]\ => control_path0_n_0,
      \q_reg[1]_0\ => control_path0_n_32,
      \q_reg[1]_1\ => control_path0_n_34,
      \q_reg[1]_2\ => control_path0_n_35,
      \q_reg[1]_3\ => control_path0_n_36,
      \q_reg[1]_4\ => control_path0_n_38,
      \q_reg[1]_5\ => control_path0_n_39,
      \q_reg[22]\(2) => control_path0_n_47,
      \q_reg[22]\(1) => control_path0_n_48,
      \q_reg[22]\(0) => control_path0_n_49,
      \q_reg[22]_0\(2) => control_path0_n_57,
      \q_reg[22]_0\(1) => control_path0_n_58,
      \q_reg[22]_0\(0) => control_path0_n_59,
      \q_reg[23]\ => data_path0_n_49,
      \q_reg[23]_0\ => data_path0_n_50,
      \q_reg[23]_1\ => data_path0_n_51,
      \q_reg[2]\ => control_path0_n_33,
      \q_reg[2]_0\ => control_path0_n_37,
      \q_reg[3]\(3) => data_path0_n_0,
      \q_reg[3]\(2) => data_path0_n_1,
      \q_reg[3]\(1) => data_path0_n_2,
      \q_reg[3]\(0) => data_path0_n_3,
      \q_reg[7]\(3) => data_path0_n_4,
      \q_reg[7]\(2) => data_path0_n_5,
      \q_reg[7]\(1) => data_path0_n_6,
      \q_reg[7]\(0) => data_path0_n_7,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_wdata(23 downto 0) => s00_axi_wdata(23 downto 0),
      \y0_carry__1_i_2__0\ => data_path0_n_87,
      \y0_carry__1_i_2__0_0\ => data_path0_n_85,
      \y0_carry__1_i_2__0_1\ => data_path0_n_86,
      \y0_carry__1_i_2__1\ => data_path0_n_98,
      \y0_carry__1_i_2__1_0\ => data_path0_n_96,
      \y0_carry__1_i_2__1_1\ => data_path0_n_97,
      \y0_carry__1_i_3__0\ => data_path0_n_93,
      \y0_carry__1_i_3__0_0\ => data_path0_n_91,
      \y0_carry__1_i_3__0_1\ => data_path0_n_92,
      \y0_carry__1_i_3__1\ => data_path0_n_103,
      \y0_carry__1_i_3__1_0\ => data_path0_n_101,
      \y0_carry__1_i_3__1_1\ => data_path0_n_102,
      \y0_carry__1_i_6\ => data_path0_n_46,
      \y0_carry__1_i_7\ => data_path0_n_47,
      \y0_carry__2_i_1__0\ => data_path0_n_90,
      \y0_carry__2_i_1__1\ => data_path0_n_100,
      \y0_carry__2_i_3__0\ => data_path0_n_95,
      \y0_carry__2_i_3__1\ => data_path0_n_105,
      \y0_carry__3_i_2__0\ => data_path0_n_89,
      \y0_carry__3_i_2__1\ => data_path0_n_99,
      \y0_carry__3_i_3__0\ => data_path0_n_94,
      \y0_carry__3_i_3__1\ => data_path0_n_104,
      \y0_carry__4_i_2__0\ => data_path0_n_88,
      \y0_carry__4_i_2__0_0\ => data_path0_n_48
    );
data_path0: entity work.project_audio_2_design_project_audio_2_ip_0_0_data_path
     port map (
      D(31 downto 0) => D(31 downto 0),
      \FSM_sequential_state_reg[0]\ => \FSM_sequential_state_reg[0]\,
      \FSM_sequential_state_reg[0]_0\ => \FSM_sequential_state_reg[0]_0\,
      O(3) => data_path0_n_20,
      O(2) => data_path0_n_21,
      O(1) => data_path0_n_22,
      O(0) => data_path0_n_23,
      Q(0) => data_path0_n_38,
      S(1) => control_path0_n_40,
      S(0) => control_path0_n_41,
      addr(4 downto 0) => addr(4 downto 0),
      axi_araddr(1 downto 0) => axi_araddr(1 downto 0),
      \axi_rdata_reg[0]\ => control_path0_n_0,
      \axi_rdata_reg[31]\(31 downto 0) => Q(31 downto 0),
      en_reg => en_reg,
      \q_reg[0]\ => control_path0_n_6,
      \q_reg[10]\ => data_path0_n_40,
      \q_reg[11]\(3) => data_path0_n_8,
      \q_reg[11]\(2) => data_path0_n_9,
      \q_reg[11]\(1) => data_path0_n_10,
      \q_reg[11]\(0) => data_path0_n_11,
      \q_reg[11]_0\ => data_path0_n_39,
      \q_reg[11]_1\(1) => control_path0_n_50,
      \q_reg[11]_1\(0) => control_path0_n_51,
      \q_reg[11]_2\ => control_path0_n_34,
      \q_reg[11]_3\ => control_path0_n_38,
      \q_reg[12]\ => data_path0_n_43,
      \q_reg[13]\ => data_path0_n_42,
      \q_reg[14]\ => data_path0_n_41,
      \q_reg[15]\(3) => data_path0_n_12,
      \q_reg[15]\(2) => data_path0_n_13,
      \q_reg[15]\(1) => data_path0_n_14,
      \q_reg[15]\(0) => data_path0_n_15,
      \q_reg[15]_0\(2) => control_path0_n_42,
      \q_reg[15]_0\(1) => control_path0_n_43,
      \q_reg[15]_0\(0) => control_path0_n_44,
      \q_reg[15]_1\(2) => control_path0_n_52,
      \q_reg[15]_1\(1) => control_path0_n_53,
      \q_reg[15]_1\(0) => control_path0_n_54,
      \q_reg[15]_2\ => control_path0_n_32,
      \q_reg[15]_3\ => control_path0_n_36,
      \q_reg[16]\ => data_path0_n_47,
      \q_reg[17]\ => data_path0_n_46,
      \q_reg[18]\ => data_path0_n_45,
      \q_reg[18]_0\ => data_path0_n_92,
      \q_reg[18]_1\ => data_path0_n_102,
      \q_reg[19]\(3) => data_path0_n_16,
      \q_reg[19]\(2) => data_path0_n_17,
      \q_reg[19]\(1) => data_path0_n_18,
      \q_reg[19]\(0) => data_path0_n_19,
      \q_reg[19]_0\ => data_path0_n_44,
      \q_reg[19]_1\ => data_path0_n_86,
      \q_reg[19]_2\ => data_path0_n_97,
      \q_reg[19]_3\(1) => control_path0_n_45,
      \q_reg[19]_3\(0) => control_path0_n_46,
      \q_reg[19]_4\(1) => control_path0_n_55,
      \q_reg[19]_4\(0) => control_path0_n_56,
      \q_reg[19]_5\ => control_path0_n_35,
      \q_reg[19]_6\ => control_path0_n_39,
      \q_reg[20]\ => data_path0_n_51,
      \q_reg[20]_0\ => data_path0_n_93,
      \q_reg[20]_1\ => data_path0_n_95,
      \q_reg[20]_2\ => data_path0_n_103,
      \q_reg[20]_3\ => data_path0_n_105,
      \q_reg[21]\ => data_path0_n_50,
      \q_reg[21]_0\ => data_path0_n_87,
      \q_reg[21]_1\ => data_path0_n_90,
      \q_reg[21]_2\ => data_path0_n_98,
      \q_reg[21]_3\ => data_path0_n_100,
      \q_reg[22]\ => data_path0_n_49,
      \q_reg[22]_0\ => data_path0_n_91,
      \q_reg[22]_1\ => data_path0_n_94,
      \q_reg[22]_2\ => data_path0_n_101,
      \q_reg[22]_3\ => data_path0_n_104,
      \q_reg[23]\ => data_path0_n_48,
      \q_reg[23]_0\ => data_path0_n_85,
      \q_reg[23]_1\ => data_path0_n_89,
      \q_reg[23]_2\ => data_path0_n_96,
      \q_reg[23]_3\ => data_path0_n_99,
      \q_reg[23]_4\(2) => control_path0_n_47,
      \q_reg[23]_4\(1) => control_path0_n_48,
      \q_reg[23]_4\(0) => control_path0_n_49,
      \q_reg[23]_5\(2) => control_path0_n_57,
      \q_reg[23]_5\(1) => control_path0_n_58,
      \q_reg[23]_5\(0) => control_path0_n_59,
      \q_reg[23]_6\ => control_path0_n_33,
      \q_reg[23]_7\ => control_path0_n_37,
      \q_reg[23]_8\(23) => control_path0_n_7,
      \q_reg[23]_8\(22) => control_path0_n_8,
      \q_reg[23]_8\(21) => control_path0_n_9,
      \q_reg[23]_8\(20) => control_path0_n_10,
      \q_reg[23]_8\(19) => control_path0_n_11,
      \q_reg[23]_8\(18) => control_path0_n_12,
      \q_reg[23]_8\(17) => control_path0_n_13,
      \q_reg[23]_8\(16) => control_path0_n_14,
      \q_reg[23]_8\(15) => control_path0_n_15,
      \q_reg[23]_8\(14) => control_path0_n_16,
      \q_reg[23]_8\(13) => control_path0_n_17,
      \q_reg[23]_8\(12) => control_path0_n_18,
      \q_reg[23]_8\(11) => control_path0_n_19,
      \q_reg[23]_8\(10) => control_path0_n_20,
      \q_reg[23]_8\(9) => control_path0_n_21,
      \q_reg[23]_8\(8) => control_path0_n_22,
      \q_reg[23]_8\(7) => control_path0_n_23,
      \q_reg[23]_8\(6) => control_path0_n_24,
      \q_reg[23]_8\(5) => control_path0_n_25,
      \q_reg[23]_8\(4) => control_path0_n_26,
      \q_reg[23]_8\(3) => control_path0_n_27,
      \q_reg[23]_8\(2) => control_path0_n_28,
      \q_reg[23]_8\(1) => control_path0_n_29,
      \q_reg[23]_8\(0) => control_path0_n_30,
      \q_reg[24]\(13 downto 5) => A(24 downto 16),
      \q_reg[24]\(4 downto 0) => A(14 downto 10),
      \q_reg[24]_0\ => data_path0_n_88,
      \q_reg[3]\(3) => data_path0_n_0,
      \q_reg[3]\(2) => data_path0_n_1,
      \q_reg[3]\(1) => data_path0_n_2,
      \q_reg[3]\(0) => data_path0_n_3,
      \q_reg[7]\(3) => data_path0_n_4,
      \q_reg[7]\(2) => data_path0_n_5,
      \q_reg[7]\(1) => data_path0_n_6,
      \q_reg[7]\(0) => data_path0_n_7,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_awvalid_0 => \^s00_axi_awvalid_0\,
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity project_audio_2_design_project_audio_2_ip_0_0_project_audio_2_ip_v1_0_S00_AXI is
  port (
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of project_audio_2_design_project_audio_2_ip_0_0_project_audio_2_ip_v1_0_S00_AXI : entity is "project_audio_2_ip_v1_0_S00_AXI";
end project_audio_2_design_project_audio_2_ip_0_0_project_audio_2_ip_v1_0_S00_AXI;

architecture STRUCTURE of project_audio_2_design_project_audio_2_ip_0_0_project_audio_2_ip_v1_0_S00_AXI is
  signal \^s_axi_arready\ : STD_LOGIC;
  signal \^s_axi_awready\ : STD_LOGIC;
  signal \^s_axi_wready\ : STD_LOGIC;
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal axi_araddr : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \axi_araddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal \axi_awaddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[3]_i_1_n_0\ : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_awready_i_1_n_0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal cordic_rotation0_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 7 );
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal slv_reg3 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal slv_reg_rden : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of axi_arready_i_1 : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of axi_awready_i_2 : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of axi_rvalid_i_1 : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair26";
begin
  S_AXI_ARREADY <= \^s_axi_arready\;
  S_AXI_AWREADY <= \^s_axi_awready\;
  S_AXI_WREADY <= \^s_axi_wready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF88880FFF8888"
    )
        port map (
      I0 => s00_axi_bready,
      I1 => \^s00_axi_bvalid\,
      I2 => s00_axi_awvalid,
      I3 => s00_axi_wvalid,
      I4 => aw_en_reg_n_0,
      I5 => \^s_axi_awready\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => axi_awready_i_1_n_0
    );
\axi_araddr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(0),
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => axi_araddr(2),
      O => \axi_araddr[2]_i_1_n_0\
    );
\axi_araddr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(1),
      I1 => s00_axi_arvalid,
      I2 => \^s_axi_arready\,
      I3 => axi_araddr(3),
      O => \axi_araddr[3]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[2]_i_1_n_0\,
      Q => axi_araddr(2),
      S => axi_awready_i_1_n_0
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[3]_i_1_n_0\,
      Q => axi_araddr(3),
      S => axi_awready_i_1_n_0
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s_axi_arready\,
      R => axi_awready_i_1_n_0
    );
\axi_awaddr[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFF20000000"
    )
        port map (
      I0 => s00_axi_awaddr(0),
      I1 => \^s_axi_awready\,
      I2 => aw_en_reg_n_0,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_awvalid,
      I5 => p_0_in(0),
      O => \axi_awaddr[2]_i_1_n_0\
    );
\axi_awaddr[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFF20000000"
    )
        port map (
      I0 => s00_axi_awaddr(1),
      I1 => \^s_axi_awready\,
      I2 => aw_en_reg_n_0,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_awvalid,
      I5 => p_0_in(1),
      O => \axi_awaddr[3]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[2]_i_1_n_0\,
      Q => p_0_in(0),
      R => axi_awready_i_1_n_0
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[3]_i_1_n_0\,
      Q => p_0_in(1),
      R => axi_awready_i_1_n_0
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => axi_awready_i_1_n_0
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \^s_axi_awready\,
      I1 => aw_en_reg_n_0,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s_axi_awready\,
      R => axi_awready_i_1_n_0
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s_axi_awready\,
      I3 => \^s_axi_wready\,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s00_axi_bvalid\,
      R => axi_awready_i_1_n_0
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^s_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      O => slv_reg_rden
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => axi_awready_i_1_n_0
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => slv_reg_rden,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => axi_awready_i_1_n_0
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s_axi_arready\,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s00_axi_rvalid\,
      R => axi_awready_i_1_n_0
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \^s_axi_wready\,
      I1 => aw_en_reg_n_0,
      I2 => s00_axi_wvalid,
      I3 => s00_axi_awvalid,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s_axi_wready\,
      R => axi_awready_i_1_n_0
    );
cordic_rotation0: entity work.project_audio_2_design_project_audio_2_ip_0_0_cordic_rotation
     port map (
      D(31 downto 0) => reg_data_out(31 downto 0),
      \FSM_sequential_state_reg[0]\ => \^s_axi_awready\,
      \FSM_sequential_state_reg[0]_0\ => \^s_axi_wready\,
      Q(31 downto 0) => slv_reg3(31 downto 0),
      axi_araddr(1 downto 0) => axi_araddr(3 downto 2),
      p_0_in(1 downto 0) => p_0_in(1 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_awvalid_0 => cordic_rotation0_n_0,
      s00_axi_wdata(23 downto 0) => s00_axi_wdata(23 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
\slv_reg3[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(0),
      I2 => s00_axi_wstrb(1),
      I3 => cordic_rotation0_n_0,
      O => p_1_in(15)
    );
\slv_reg3[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(0),
      I2 => s00_axi_wstrb(2),
      I3 => cordic_rotation0_n_0,
      O => p_1_in(23)
    );
\slv_reg3[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(0),
      I2 => s00_axi_wstrb(3),
      I3 => cordic_rotation0_n_0,
      O => p_1_in(31)
    );
\slv_reg3[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => p_0_in(1),
      I1 => p_0_in(0),
      I2 => s00_axi_wstrb(0),
      I3 => cordic_rotation0_n_0,
      O => p_1_in(7)
    );
\slv_reg3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(0),
      Q => slv_reg3(0),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(10),
      Q => slv_reg3(10),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(11),
      Q => slv_reg3(11),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(12),
      Q => slv_reg3(12),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(13),
      Q => slv_reg3(13),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(14),
      Q => slv_reg3(14),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(15),
      Q => slv_reg3(15),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(16),
      Q => slv_reg3(16),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(17),
      Q => slv_reg3(17),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(18),
      Q => slv_reg3(18),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(19),
      Q => slv_reg3(19),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(1),
      Q => slv_reg3(1),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(20),
      Q => slv_reg3(20),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(21),
      Q => slv_reg3(21),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(22),
      Q => slv_reg3(22),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(23),
      Q => slv_reg3(23),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(24),
      Q => slv_reg3(24),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(25),
      Q => slv_reg3(25),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(26),
      Q => slv_reg3(26),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(27),
      Q => slv_reg3(27),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(28),
      Q => slv_reg3(28),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(29),
      Q => slv_reg3(29),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(2),
      Q => slv_reg3(2),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(30),
      Q => slv_reg3(30),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(31),
      Q => slv_reg3(31),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(3),
      Q => slv_reg3(3),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(4),
      Q => slv_reg3(4),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(5),
      Q => slv_reg3(5),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(6),
      Q => slv_reg3(6),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(7),
      D => s00_axi_wdata(7),
      Q => slv_reg3(7),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(8),
      Q => slv_reg3(8),
      R => axi_awready_i_1_n_0
    );
\slv_reg3_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(9),
      Q => slv_reg3(9),
      R => axi_awready_i_1_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity project_audio_2_design_project_audio_2_ip_0_0_project_audio_2_ip_v1_0 is
  port (
    S_AXI_WREADY : out STD_LOGIC;
    S_AXI_AWREADY : out STD_LOGIC;
    S_AXI_ARREADY : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_aresetn : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of project_audio_2_design_project_audio_2_ip_0_0_project_audio_2_ip_v1_0 : entity is "project_audio_2_ip_v1_0";
end project_audio_2_design_project_audio_2_ip_0_0_project_audio_2_ip_v1_0;

architecture STRUCTURE of project_audio_2_design_project_audio_2_ip_0_0_project_audio_2_ip_v1_0 is
begin
project_audio_2_ip_v1_0_S00_AXI_inst: entity work.project_audio_2_design_project_audio_2_ip_0_0_project_audio_2_ip_v1_0_S00_AXI
     port map (
      S_AXI_ARREADY => S_AXI_ARREADY,
      S_AXI_AWREADY => S_AXI_AWREADY,
      S_AXI_WREADY => S_AXI_WREADY,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(1 downto 0) => s00_axi_araddr(1 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(1 downto 0) => s00_axi_awaddr(1 downto 0),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity project_audio_2_design_project_audio_2_ip_0_0 is
  port (
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of project_audio_2_design_project_audio_2_ip_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of project_audio_2_design_project_audio_2_ip_0_0 : entity is "project_audio_2_design_project_audio_2_ip_0_0,project_audio_2_ip_v1_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of project_audio_2_design_project_audio_2_ip_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of project_audio_2_design_project_audio_2_ip_0_0 : entity is "project_audio_2_ip_v1_0,Vivado 2022.2";
end project_audio_2_design_project_audio_2_ip_0_0;

architecture STRUCTURE of project_audio_2_design_project_audio_2_ip_0_0 is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN project_audio_2_design_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute x_interface_parameter of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute x_interface_info of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute x_interface_info of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute x_interface_info of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute x_interface_info of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute x_interface_info of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute x_interface_info of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute x_interface_info of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute x_interface_info of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute x_interface_info of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute x_interface_info of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute x_interface_info of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute x_interface_info of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute x_interface_parameter of s00_axi_awaddr : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN project_audio_2_design_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute x_interface_info of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute x_interface_info of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute x_interface_info of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute x_interface_info of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute x_interface_info of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.project_audio_2_design_project_audio_2_ip_0_0_project_audio_2_ip_v1_0
     port map (
      S_AXI_ARREADY => s00_axi_arready,
      S_AXI_AWREADY => s00_axi_awready,
      S_AXI_WREADY => s00_axi_wready,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(1 downto 0) => s00_axi_araddr(3 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(1 downto 0) => s00_axi_awaddr(3 downto 2),
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid
    );
end STRUCTURE;
