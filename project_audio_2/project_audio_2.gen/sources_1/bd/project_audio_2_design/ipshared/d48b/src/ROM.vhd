----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/10/2023 07:38:29 AM
-- Design Name: 
-- Module Name: ROM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


-- ROM Inference on array

-- File: roms_1.vhd

library ieee;

use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

-- ROM memory to save the values of arctan in cordic algorithm
entity ROM is port(
    clk  : in  std_logic;
    addr : in  std_logic_vector(4 downto 0);
    data : out std_logic_vector(23 downto 0)
);

end ROM;

architecture behavioral of ROM is
    type rom_type is array (0 to 31) of std_logic_vector(23 downto 0);
    
    signal ROM_m : rom_type := (
            X"200000",X"12e405",X"09fb38",X"051111",X"028b0d",
            X"0145d7",X"00a2f6",X"00517c",X"0028be",X"00145f",X"000a2f",
            X"000517",X"00028b",X"000145",X"0000a2",X"000051",X"000028",X"000014",X"00000a",
            X"000005",X"000002",X"000001",X"000000",X"000000",X"000000",X"000000",X"000000",X"000000",X"000000",X"000000",X"000000",X"000000"
    );

    attribute rom_style : string;
    attribute rom_style of ROM_m : signal is "block";
begin

process(clk)
begin
    if rising_edge(clk) then
        data <= ROM_m(conv_integer(addr));
    end if;
end process;

end behavioral;
