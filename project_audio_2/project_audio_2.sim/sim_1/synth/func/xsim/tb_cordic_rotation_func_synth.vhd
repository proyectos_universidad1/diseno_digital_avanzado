-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Tue May  2 11:47:27 2023
-- Host        : juan-Inspiron-14-3467 running 64-bit Linux Mint 21.1
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/juan/Documentos/JuanK/universidad/profun_2/project_audio_2/project_audio_2.sim/sim_1/synth/func/xsim/tb_cordic_rotation_func_synth.vhd
-- Design      : cordic_rotation
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Mux_2_1 is
  port (
    cos_OBUF : out STD_LOGIC_VECTOR ( 24 downto 0 );
    D : out STD_LOGIC_VECTOR ( 24 downto 0 );
    \q_reg[3]\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 23 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[23]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[24]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    start_IBUF : in STD_LOGIC;
    \q_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end Mux_2_1;

architecture STRUCTURE of Mux_2_1 is
  signal \^cos_obuf\ : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \y0_carry__0_n_0\ : STD_LOGIC;
  signal \y0_carry__0_n_1\ : STD_LOGIC;
  signal \y0_carry__0_n_2\ : STD_LOGIC;
  signal \y0_carry__0_n_3\ : STD_LOGIC;
  signal \y0_carry__1_n_0\ : STD_LOGIC;
  signal \y0_carry__1_n_1\ : STD_LOGIC;
  signal \y0_carry__1_n_2\ : STD_LOGIC;
  signal \y0_carry__1_n_3\ : STD_LOGIC;
  signal \y0_carry__2_n_0\ : STD_LOGIC;
  signal \y0_carry__2_n_1\ : STD_LOGIC;
  signal \y0_carry__2_n_2\ : STD_LOGIC;
  signal \y0_carry__2_n_3\ : STD_LOGIC;
  signal \y0_carry__3_n_0\ : STD_LOGIC;
  signal \y0_carry__3_n_1\ : STD_LOGIC;
  signal \y0_carry__3_n_2\ : STD_LOGIC;
  signal \y0_carry__3_n_3\ : STD_LOGIC;
  signal \y0_carry__4_n_0\ : STD_LOGIC;
  signal \y0_carry__4_n_1\ : STD_LOGIC;
  signal \y0_carry__4_n_2\ : STD_LOGIC;
  signal \y0_carry__4_n_3\ : STD_LOGIC;
  signal y0_carry_n_0 : STD_LOGIC;
  signal y0_carry_n_1 : STD_LOGIC;
  signal y0_carry_n_2 : STD_LOGIC;
  signal y0_carry_n_3 : STD_LOGIC;
  signal \NLW_y0_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_y0_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \q[10]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \q[11]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \q[12]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \q[13]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \q[14]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \q[15]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \q[16]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \q[17]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \q[18]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \q[19]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \q[1]_i_1__0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \q[20]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \q[21]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \q[22]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \q[23]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \q[24]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \q[2]_i_1__0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \q[3]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \q[4]_i_1__0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \q[5]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \q[6]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \q[7]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \q[8]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \q[9]_i_1\ : label is "soft_lutpair19";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of y0_carry : label is 35;
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of y0_carry : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__0\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__1\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__2\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__2\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__3\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__3\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__4\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__4\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__5\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__5\ : label is "{SYNTH-8 {cell *THIS*}}";
begin
  cos_OBUF(24 downto 0) <= \^cos_obuf\(24 downto 0);
\q[0]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(0),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(0)
    );
\q[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(10),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(10)
    );
\q[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(11),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(11)
    );
\q[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(12),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(12)
    );
\q[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(13),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(13)
    );
\q[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(14),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(14)
    );
\q[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(15),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(15)
    );
\q[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(16),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(16)
    );
\q[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(17),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(17)
    );
\q[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(18),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(18)
    );
\q[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(19),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(19)
    );
\q[1]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(1),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(1)
    );
\q[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(20),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(20)
    );
\q[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(21),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(21)
    );
\q[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(22),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(22)
    );
\q[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^cos_obuf\(23),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(23)
    );
\q[24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^cos_obuf\(24),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(24)
    );
\q[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(2),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(2)
    );
\q[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(3),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(3)
    );
\q[4]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(4),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(4)
    );
\q[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(5),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(5)
    );
\q[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(6),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(6)
    );
\q[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(7),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(7)
    );
\q[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(8),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(8)
    );
\q[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AE"
    )
        port map (
      I0 => \^cos_obuf\(9),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(9)
    );
y0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => y0_carry_n_0,
      CO(2) => y0_carry_n_1,
      CO(1) => y0_carry_n_2,
      CO(0) => y0_carry_n_3,
      CYINIT => \q_reg[3]\,
      DI(3 downto 0) => Q(3 downto 0),
      O(3 downto 0) => \^cos_obuf\(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\y0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => y0_carry_n_0,
      CO(3) => \y0_carry__0_n_0\,
      CO(2) => \y0_carry__0_n_1\,
      CO(1) => \y0_carry__0_n_2\,
      CO(0) => \y0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(7 downto 4),
      O(3 downto 0) => \^cos_obuf\(7 downto 4),
      S(3 downto 0) => \q_reg[7]\(3 downto 0)
    );
\y0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__0_n_0\,
      CO(3) => \y0_carry__1_n_0\,
      CO(2) => \y0_carry__1_n_1\,
      CO(1) => \y0_carry__1_n_2\,
      CO(0) => \y0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(11 downto 8),
      O(3 downto 0) => \^cos_obuf\(11 downto 8),
      S(3 downto 0) => \q_reg[11]\(3 downto 0)
    );
\y0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__1_n_0\,
      CO(3) => \y0_carry__2_n_0\,
      CO(2) => \y0_carry__2_n_1\,
      CO(1) => \y0_carry__2_n_2\,
      CO(0) => \y0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(15 downto 12),
      O(3 downto 0) => \^cos_obuf\(15 downto 12),
      S(3 downto 0) => \q_reg[15]\(3 downto 0)
    );
\y0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__2_n_0\,
      CO(3) => \y0_carry__3_n_0\,
      CO(2) => \y0_carry__3_n_1\,
      CO(1) => \y0_carry__3_n_2\,
      CO(0) => \y0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(19 downto 16),
      O(3 downto 0) => \^cos_obuf\(19 downto 16),
      S(3 downto 0) => \q_reg[19]\(3 downto 0)
    );
\y0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__3_n_0\,
      CO(3) => \y0_carry__4_n_0\,
      CO(2) => \y0_carry__4_n_1\,
      CO(1) => \y0_carry__4_n_2\,
      CO(0) => \y0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(23 downto 20),
      O(3 downto 0) => \^cos_obuf\(23 downto 20),
      S(3 downto 0) => \q_reg[23]\(3 downto 0)
    );
\y0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__4_n_0\,
      CO(3 downto 0) => \NLW_y0_carry__5_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_y0_carry__5_O_UNCONNECTED\(3 downto 1),
      O(0) => \^cos_obuf\(24),
      S(3 downto 1) => B"000",
      S(0) => \q_reg[24]\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Mux_2_1_0 is
  port (
    sin_OBUF : out STD_LOGIC_VECTOR ( 24 downto 0 );
    D : out STD_LOGIC_VECTOR ( 24 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[23]\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[23]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[24]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    start_IBUF : in STD_LOGIC;
    \q_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Mux_2_1_0 : entity is "Mux_2_1";
end Mux_2_1_0;

architecture STRUCTURE of Mux_2_1_0 is
  signal \^sin_obuf\ : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \y0_carry__0_n_0\ : STD_LOGIC;
  signal \y0_carry__0_n_1\ : STD_LOGIC;
  signal \y0_carry__0_n_2\ : STD_LOGIC;
  signal \y0_carry__0_n_3\ : STD_LOGIC;
  signal \y0_carry__1_n_0\ : STD_LOGIC;
  signal \y0_carry__1_n_1\ : STD_LOGIC;
  signal \y0_carry__1_n_2\ : STD_LOGIC;
  signal \y0_carry__1_n_3\ : STD_LOGIC;
  signal \y0_carry__2_n_0\ : STD_LOGIC;
  signal \y0_carry__2_n_1\ : STD_LOGIC;
  signal \y0_carry__2_n_2\ : STD_LOGIC;
  signal \y0_carry__2_n_3\ : STD_LOGIC;
  signal \y0_carry__3_n_0\ : STD_LOGIC;
  signal \y0_carry__3_n_1\ : STD_LOGIC;
  signal \y0_carry__3_n_2\ : STD_LOGIC;
  signal \y0_carry__3_n_3\ : STD_LOGIC;
  signal \y0_carry__4_n_0\ : STD_LOGIC;
  signal \y0_carry__4_n_1\ : STD_LOGIC;
  signal \y0_carry__4_n_2\ : STD_LOGIC;
  signal \y0_carry__4_n_3\ : STD_LOGIC;
  signal y0_carry_n_0 : STD_LOGIC;
  signal y0_carry_n_1 : STD_LOGIC;
  signal y0_carry_n_2 : STD_LOGIC;
  signal y0_carry_n_3 : STD_LOGIC;
  signal \NLW_y0_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_y0_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \q[10]_i_1__1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \q[11]_i_1__1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \q[12]_i_1__1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \q[13]_i_1__1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \q[14]_i_1__1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \q[15]_i_1__1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \q[16]_i_1__1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \q[17]_i_1__1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \q[18]_i_1__1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \q[19]_i_1__1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \q[1]_i_1__2\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \q[20]_i_1__1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \q[21]_i_1__1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \q[22]_i_1__1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \q[23]_i_1__1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \q[24]_i_1__0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \q[2]_i_1__2\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \q[3]_i_1__1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \q[4]_i_1__2\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \q[5]_i_1__1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \q[6]_i_1__1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \q[7]_i_1__1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \q[8]_i_1__1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \q[9]_i_1__1\ : label is "soft_lutpair31";
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of y0_carry : label is 35;
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of y0_carry : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__0\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__1\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__2\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__2\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__3\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__3\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__4\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__4\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__5\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__5\ : label is "{SYNTH-8 {cell *THIS*}}";
begin
  sin_OBUF(24 downto 0) <= \^sin_obuf\(24 downto 0);
\q[0]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(0),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(0)
    );
\q[10]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(10),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(10)
    );
\q[11]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(11),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(11)
    );
\q[12]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(12),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(12)
    );
\q[13]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(13),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(13)
    );
\q[14]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(14),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(14)
    );
\q[15]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(15),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(15)
    );
\q[16]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(16),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(16)
    );
\q[17]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(17),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(17)
    );
\q[18]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(18),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(18)
    );
\q[19]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(19),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(19)
    );
\q[1]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(1),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(1)
    );
\q[20]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(20),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(20)
    );
\q[21]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(21),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(21)
    );
\q[22]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(22),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(22)
    );
\q[23]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(23),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(23)
    );
\q[24]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(24),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(24)
    );
\q[2]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(2),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(2)
    );
\q[3]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(3),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(3)
    );
\q[4]_i_1__2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(4),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(4)
    );
\q[5]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(5),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(5)
    );
\q[6]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(6),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(6)
    );
\q[7]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(7),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(7)
    );
\q[8]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(8),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(8)
    );
\q[9]_i_1__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A2"
    )
        port map (
      I0 => \^sin_obuf\(9),
      I1 => start_IBUF,
      I2 => \q_reg[0]\(0),
      O => D(9)
    );
y0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => y0_carry_n_0,
      CO(2) => y0_carry_n_1,
      CO(1) => y0_carry_n_2,
      CO(0) => y0_carry_n_3,
      CYINIT => Q(0),
      DI(3 downto 0) => \q_reg[23]\(3 downto 0),
      O(3 downto 0) => \^sin_obuf\(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\y0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => y0_carry_n_0,
      CO(3) => \y0_carry__0_n_0\,
      CO(2) => \y0_carry__0_n_1\,
      CO(1) => \y0_carry__0_n_2\,
      CO(0) => \y0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]\(7 downto 4),
      O(3 downto 0) => \^sin_obuf\(7 downto 4),
      S(3 downto 0) => \q_reg[7]\(3 downto 0)
    );
\y0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__0_n_0\,
      CO(3) => \y0_carry__1_n_0\,
      CO(2) => \y0_carry__1_n_1\,
      CO(1) => \y0_carry__1_n_2\,
      CO(0) => \y0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]\(11 downto 8),
      O(3 downto 0) => \^sin_obuf\(11 downto 8),
      S(3 downto 0) => \q_reg[11]\(3 downto 0)
    );
\y0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__1_n_0\,
      CO(3) => \y0_carry__2_n_0\,
      CO(2) => \y0_carry__2_n_1\,
      CO(1) => \y0_carry__2_n_2\,
      CO(0) => \y0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]\(15 downto 12),
      O(3 downto 0) => \^sin_obuf\(15 downto 12),
      S(3 downto 0) => \q_reg[15]\(3 downto 0)
    );
\y0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__2_n_0\,
      CO(3) => \y0_carry__3_n_0\,
      CO(2) => \y0_carry__3_n_1\,
      CO(1) => \y0_carry__3_n_2\,
      CO(0) => \y0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]\(19 downto 16),
      O(3 downto 0) => \^sin_obuf\(19 downto 16),
      S(3 downto 0) => \q_reg[19]\(3 downto 0)
    );
\y0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__3_n_0\,
      CO(3) => \y0_carry__4_n_0\,
      CO(2) => \y0_carry__4_n_1\,
      CO(1) => \y0_carry__4_n_2\,
      CO(0) => \y0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \q_reg[23]\(23 downto 20),
      O(3 downto 0) => \^sin_obuf\(23 downto 20),
      S(3 downto 0) => \q_reg[23]_0\(3 downto 0)
    );
\y0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__4_n_0\,
      CO(3 downto 0) => \NLW_y0_carry__5_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_y0_carry__5_O_UNCONNECTED\(3 downto 1),
      O(0) => \^sin_obuf\(24),
      S(3 downto 1) => B"000",
      S(0) => \q_reg[24]\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \Mux_2_1__parameterized2\ is
  port (
    \q_reg[3]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    O : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[3]_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 22 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[23]\ : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \Mux_2_1__parameterized2\ : entity is "Mux_2_1";
end \Mux_2_1__parameterized2\;

architecture STRUCTURE of \Mux_2_1__parameterized2\ is
  signal \y0_carry__0_n_0\ : STD_LOGIC;
  signal \y0_carry__0_n_1\ : STD_LOGIC;
  signal \y0_carry__0_n_2\ : STD_LOGIC;
  signal \y0_carry__0_n_3\ : STD_LOGIC;
  signal \y0_carry__1_n_0\ : STD_LOGIC;
  signal \y0_carry__1_n_1\ : STD_LOGIC;
  signal \y0_carry__1_n_2\ : STD_LOGIC;
  signal \y0_carry__1_n_3\ : STD_LOGIC;
  signal \y0_carry__2_n_0\ : STD_LOGIC;
  signal \y0_carry__2_n_1\ : STD_LOGIC;
  signal \y0_carry__2_n_2\ : STD_LOGIC;
  signal \y0_carry__2_n_3\ : STD_LOGIC;
  signal \y0_carry__3_n_0\ : STD_LOGIC;
  signal \y0_carry__3_n_1\ : STD_LOGIC;
  signal \y0_carry__3_n_2\ : STD_LOGIC;
  signal \y0_carry__3_n_3\ : STD_LOGIC;
  signal \y0_carry__4_n_1\ : STD_LOGIC;
  signal \y0_carry__4_n_2\ : STD_LOGIC;
  signal \y0_carry__4_n_3\ : STD_LOGIC;
  signal y0_carry_n_0 : STD_LOGIC;
  signal y0_carry_n_1 : STD_LOGIC;
  signal y0_carry_n_2 : STD_LOGIC;
  signal y0_carry_n_3 : STD_LOGIC;
  signal \NLW_y0_carry__4_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of y0_carry : label is 35;
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of y0_carry : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__0\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__0\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__1\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__1\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__2\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__2\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__3\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__3\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute ADDER_THRESHOLD of \y0_carry__4\ : label is 35;
  attribute METHODOLOGY_DRC_VIOS of \y0_carry__4\ : label is "{SYNTH-8 {cell *THIS*}}";
begin
y0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => y0_carry_n_0,
      CO(2) => y0_carry_n_1,
      CO(1) => y0_carry_n_2,
      CO(0) => y0_carry_n_3,
      CYINIT => \q_reg[3]_0\,
      DI(3 downto 0) => Q(3 downto 0),
      O(3 downto 0) => \q_reg[3]\(3 downto 0),
      S(3 downto 0) => S(3 downto 0)
    );
\y0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => y0_carry_n_0,
      CO(3) => \y0_carry__0_n_0\,
      CO(2) => \y0_carry__0_n_1\,
      CO(1) => \y0_carry__0_n_2\,
      CO(0) => \y0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(7 downto 4),
      O(3 downto 0) => \q_reg[7]\(3 downto 0),
      S(3 downto 0) => \q_reg[7]_0\(3 downto 0)
    );
\y0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__0_n_0\,
      CO(3) => \y0_carry__1_n_0\,
      CO(2) => \y0_carry__1_n_1\,
      CO(1) => \y0_carry__1_n_2\,
      CO(0) => \y0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(11 downto 8),
      O(3 downto 0) => \q_reg[11]\(3 downto 0),
      S(3 downto 0) => \q_reg[11]_0\(3 downto 0)
    );
\y0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__1_n_0\,
      CO(3) => \y0_carry__2_n_0\,
      CO(2) => \y0_carry__2_n_1\,
      CO(1) => \y0_carry__2_n_2\,
      CO(0) => \y0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(15 downto 12),
      O(3 downto 0) => \q_reg[15]\(3 downto 0),
      S(3 downto 0) => \q_reg[15]_0\(3 downto 0)
    );
\y0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__2_n_0\,
      CO(3) => \y0_carry__3_n_0\,
      CO(2) => \y0_carry__3_n_1\,
      CO(1) => \y0_carry__3_n_2\,
      CO(0) => \y0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(19 downto 16),
      O(3 downto 0) => \q_reg[19]\(3 downto 0),
      S(3 downto 0) => \q_reg[19]_0\(3 downto 0)
    );
\y0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \y0_carry__3_n_0\,
      CO(3) => \NLW_y0_carry__4_CO_UNCONNECTED\(3),
      CO(2) => \y0_carry__4_n_1\,
      CO(1) => \y0_carry__4_n_2\,
      CO(0) => \y0_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => Q(22 downto 20),
      O(3 downto 0) => O(3 downto 0),
      S(3 downto 0) => \q_reg[23]\(3 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ROM is
  port (
    out_ROM : out STD_LOGIC_VECTOR ( 22 downto 0 );
    data_reg_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    data_reg_1 : in STD_LOGIC_VECTOR ( 4 downto 0 )
  );
end ROM;

architecture STRUCTURE of ROM is
  signal \^out_rom\ : STD_LOGIC_VECTOR ( 22 downto 0 );
  signal NLW_data_reg_DOBDO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 5 );
  signal NLW_data_reg_DOPBDOP_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTA.DATA_BIT_LAYOUT\ of data_reg : label is "p2_d16";
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ : string;
  attribute \MEM.PORTB.DATA_BIT_LAYOUT\ of data_reg : label is "p0_d5";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of data_reg : label is "{SYNTH-6 {cell *THIS*}}";
  attribute RTL_RAM_BITS : integer;
  attribute RTL_RAM_BITS of data_reg : label is 736;
  attribute RTL_RAM_NAME : string;
  attribute RTL_RAM_NAME of data_reg : label is "data_path0/ROM_1/data_reg";
  attribute RTL_RAM_TYPE : string;
  attribute RTL_RAM_TYPE of data_reg : label is "RAM_TDP";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of data_reg : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of data_reg : label is 31;
  attribute ram_ext_slice_begin : integer;
  attribute ram_ext_slice_begin of data_reg : label is 18;
  attribute ram_ext_slice_end : integer;
  attribute ram_ext_slice_end of data_reg : label is 22;
  attribute ram_offset : integer;
  attribute ram_offset of data_reg : label is 0;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of data_reg : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of data_reg : label is 17;
begin
  out_ROM(22 downto 0) <= \^out_rom\(22 downto 0);
data_reg: unisim.vcomponents.RAMB18E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000658",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"005100A20145028B05170A2F145F28BE517CA2F645D78B0D1111FB38E4050000",
      INIT_01 => X"0000000000000000000000000000000000000000000100020005000A00140028",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000001000200040008",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"00000",
      INIT_B => X"00000",
      INIT_FILE => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "DELAYED_WRITE",
      READ_WIDTH_A => 18,
      READ_WIDTH_B => 18,
      RSTREG_PRIORITY_A => "RSTREG",
      RSTREG_PRIORITY_B => "RSTREG",
      SIM_COLLISION_CHECK => "ALL",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"00000",
      SRVAL_B => X"00000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 18,
      WRITE_WIDTH_B => 18
    )
        port map (
      ADDRARDADDR(13 downto 9) => B"00000",
      ADDRARDADDR(8 downto 4) => data_reg_1(4 downto 0),
      ADDRARDADDR(3 downto 0) => B"0000",
      ADDRBWRADDR(13 downto 9) => B"10000",
      ADDRBWRADDR(8 downto 4) => data_reg_1(4 downto 0),
      ADDRBWRADDR(3 downto 0) => B"0000",
      CLKARDCLK => CLK,
      CLKBWRCLK => CLK,
      DIADI(15 downto 0) => B"1111111111111111",
      DIBDI(15 downto 0) => B"1111111111111111",
      DIPADIP(1 downto 0) => B"11",
      DIPBDIP(1 downto 0) => B"11",
      DOADO(15 downto 0) => \^out_rom\(15 downto 0),
      DOBDO(15 downto 5) => NLW_data_reg_DOBDO_UNCONNECTED(15 downto 5),
      DOBDO(4 downto 0) => \^out_rom\(22 downto 18),
      DOPADOP(1 downto 0) => \^out_rom\(17 downto 16),
      DOPBDOP(1 downto 0) => NLW_data_reg_DOPBDOP_UNCONNECTED(1 downto 0),
      ENARDEN => '1',
      ENBWREN => '1',
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      WEA(1 downto 0) => B"00",
      WEBWE(3 downto 0) => B"0000"
    );
\y0_carry__4_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^out_rom\(22),
      O => data_reg_0(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fsm is
  port (
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_sequential_state_reg[0]_0\ : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    D : out STD_LOGIC_VECTOR ( 23 downto 0 );
    start_IBUF : in STD_LOGIC;
    \FSM_sequential_state_reg[0]_1\ : in STD_LOGIC;
    angle_IBUF : in STD_LOGIC_VECTOR ( 23 downto 0 );
    O : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[3]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end fsm;

architecture STRUCTURE of fsm is
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal state : STD_LOGIC_VECTOR ( 1 to 1 );
  signal \state__0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_state[1]_i_1\ : label is "soft_lutpair2";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_state_reg[0]\ : label is "counter_state:10,start_state:00,compute_state:01";
  attribute FSM_ENCODED_STATES of \FSM_sequential_state_reg[1]\ : label is "counter_state:10,start_state:00,compute_state:01";
  attribute SOFT_HLUTNM of \q[21]_i_1__0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \q[22]_i_1__0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \q[23]_i_1__0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \q[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \q[4]_i_3\ : label is "soft_lutpair1";
begin
  Q(0) <= \^q\(0);
\FSM_sequential_state[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3202"
    )
        port map (
      I0 => start_IBUF,
      I1 => \^q\(0),
      I2 => state(1),
      I3 => \FSM_sequential_state_reg[0]_1\,
      O => \state__0\(0)
    );
\FSM_sequential_state[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^q\(0),
      I1 => state(1),
      O => \state__0\(1)
    );
\FSM_sequential_state_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => \state__0\(0),
      Q => \^q\(0)
    );
\FSM_sequential_state_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => '1',
      CLR => AR(0),
      D => \state__0\(1),
      Q => state(1)
    );
\q[0]_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(0),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[3]\(0),
      O => D(0)
    );
\q[10]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(10),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[11]\(2),
      O => D(10)
    );
\q[11]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(11),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[11]\(3),
      O => D(11)
    );
\q[12]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(12),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[15]\(0),
      O => D(12)
    );
\q[13]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(13),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[15]\(1),
      O => D(13)
    );
\q[14]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(14),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[15]\(2),
      O => D(14)
    );
\q[15]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(15),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[15]\(3),
      O => D(15)
    );
\q[16]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(16),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[19]\(0),
      O => D(16)
    );
\q[17]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(17),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[19]\(1),
      O => D(17)
    );
\q[18]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(18),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[19]\(2),
      O => D(18)
    );
\q[19]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(19),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[19]\(3),
      O => D(19)
    );
\q[1]_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(1),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[3]\(1),
      O => D(1)
    );
\q[20]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(20),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => O(0),
      O => D(20)
    );
\q[21]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(21),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => O(1),
      O => D(21)
    );
\q[22]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(22),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => O(2),
      O => D(22)
    );
\q[23]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(23),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => O(3),
      O => D(23)
    );
\q[2]_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(2),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[3]\(2),
      O => D(2)
    );
\q[3]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(3),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[3]\(3),
      O => D(3)
    );
\q[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0E"
    )
        port map (
      I0 => \^q\(0),
      I1 => start_IBUF,
      I2 => state(1),
      O => E(0)
    );
\q[4]_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(4),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[7]\(0),
      O => D(4)
    );
\q[4]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \^q\(0),
      I1 => start_IBUF,
      I2 => state(1),
      O => \FSM_sequential_state_reg[0]_0\
    );
\q[5]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(5),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[7]\(1),
      O => D(5)
    );
\q[6]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(6),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[7]\(2),
      O => D(6)
    );
\q[7]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(7),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[7]\(3),
      O => D(7)
    );
\q[8]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(8),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[11]\(0),
      O => D(8)
    );
\q[9]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EF20"
    )
        port map (
      I0 => angle_IBUF(9),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[11]\(1),
      O => D(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity reg is
  port (
    done_OBUF : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \q_reg[1]_0\ : out STD_LOGIC;
    \q_reg[1]_1\ : out STD_LOGIC;
    \q_reg[2]_0\ : out STD_LOGIC;
    \q_reg[1]_2\ : out STD_LOGIC;
    \q_reg[1]_3\ : out STD_LOGIC;
    \q_reg[1]_4\ : out STD_LOGIC;
    \q_reg[2]_1\ : out STD_LOGIC;
    \q_reg[1]_5\ : out STD_LOGIC;
    \q_reg[1]_6\ : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[14]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[19]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[22]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[11]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[14]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[19]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[22]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \y0_carry__1_i_2__0_0\ : in STD_LOGIC;
    \y0_carry__1_i_2__0_1\ : in STD_LOGIC;
    \y0_carry__1_i_2__0_2\ : in STD_LOGIC;
    \y0_carry__2_i_1__0\ : in STD_LOGIC;
    \y0_carry__3_i_2__0_0\ : in STD_LOGIC;
    \y0_carry__4_i_2__0_0\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    \y0_carry__1_i_3__0\ : in STD_LOGIC;
    \y0_carry__1_i_3__0_0\ : in STD_LOGIC;
    \y0_carry__1_i_3__0_1\ : in STD_LOGIC;
    \y0_carry__2_i_3__0_0\ : in STD_LOGIC;
    \y0_carry__3_i_3__0\ : in STD_LOGIC;
    \y0_carry__1_i_2__1_0\ : in STD_LOGIC;
    \y0_carry__1_i_2__1_1\ : in STD_LOGIC;
    \y0_carry__1_i_2__1_2\ : in STD_LOGIC;
    \y0_carry__2_i_1__1\ : in STD_LOGIC;
    \y0_carry__3_i_2__1_0\ : in STD_LOGIC;
    \y0_carry__4_i_2__1_0\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    \y0_carry__1_i_3__1\ : in STD_LOGIC;
    \y0_carry__1_i_3__1_0\ : in STD_LOGIC;
    \y0_carry__1_i_3__1_1\ : in STD_LOGIC;
    \y0_carry__2_i_3__1_0\ : in STD_LOGIC;
    \y0_carry__3_i_3__1\ : in STD_LOGIC;
    start_IBUF : in STD_LOGIC;
    \q_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \y0_carry__1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[4]_0\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end reg;

architecture STRUCTURE of reg is
  signal \^q\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \q[0]_i_1_n_0\ : STD_LOGIC;
  signal \q[1]_i_1_n_0\ : STD_LOGIC;
  signal \q[2]_i_1_n_0\ : STD_LOGIC;
  signal \q[3]_i_1__2_n_0\ : STD_LOGIC;
  signal \q[4]_i_2_n_0\ : STD_LOGIC;
  signal \^q_reg[1]_1\ : STD_LOGIC;
  signal \^q_reg[1]_2\ : STD_LOGIC;
  signal \^q_reg[1]_3\ : STD_LOGIC;
  signal \^q_reg[1]_4\ : STD_LOGIC;
  signal \^q_reg[1]_5\ : STD_LOGIC;
  signal \^q_reg[1]_6\ : STD_LOGIC;
  signal \^q_reg[2]_0\ : STD_LOGIC;
  signal \^q_reg[2]_1\ : STD_LOGIC;
  signal \y0_carry__1_i_10__0_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_10_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_11__0_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_11_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_5__0_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_5_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_6__0_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_6_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_9__0_n_0\ : STD_LOGIC;
  signal \y0_carry__1_i_9_n_0\ : STD_LOGIC;
  signal \y0_carry__2_i_12__0_n_0\ : STD_LOGIC;
  signal \y0_carry__2_i_12_n_0\ : STD_LOGIC;
  signal \y0_carry__2_i_7__0_n_0\ : STD_LOGIC;
  signal \y0_carry__2_i_7_n_0\ : STD_LOGIC;
  signal \y0_carry__2_i_8__0_n_0\ : STD_LOGIC;
  signal \y0_carry__2_i_8_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_10__0_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_10_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_11__0_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_11_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_5__0_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_5_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_6__0_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_6_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_9__0_n_0\ : STD_LOGIC;
  signal \y0_carry__3_i_9_n_0\ : STD_LOGIC;
  signal \y0_carry__4_i_6__0_n_0\ : STD_LOGIC;
  signal \y0_carry__4_i_6_n_0\ : STD_LOGIC;
  signal \y0_carry__4_i_7__0_n_0\ : STD_LOGIC;
  signal \y0_carry__4_i_7_n_0\ : STD_LOGIC;
  signal \y0_carry__4_i_8__0_n_0\ : STD_LOGIC;
  signal \y0_carry__4_i_8_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_state[0]_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of done_OBUF_inst_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \q[1]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \q[2]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \y0_carry__1_i_10\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \y0_carry__1_i_10__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \y0_carry__1_i_11\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \y0_carry__1_i_11__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \y0_carry__1_i_9\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \y0_carry__1_i_9__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \y0_carry__2_i_12\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \y0_carry__2_i_12__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \y0_carry__3_i_5\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \y0_carry__3_i_5__0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \y0_carry__3_i_6\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \y0_carry__3_i_6__0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \y0_carry__3_i_7\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \y0_carry__3_i_7__0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \y0_carry__4_i_8\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \y0_carry__4_i_8__0\ : label is "soft_lutpair12";
begin
  Q(4 downto 0) <= \^q\(4 downto 0);
  \q_reg[1]_1\ <= \^q_reg[1]_1\;
  \q_reg[1]_2\ <= \^q_reg[1]_2\;
  \q_reg[1]_3\ <= \^q_reg[1]_3\;
  \q_reg[1]_4\ <= \^q_reg[1]_4\;
  \q_reg[1]_5\ <= \^q_reg[1]_5\;
  \q_reg[1]_6\ <= \^q_reg[1]_6\;
  \q_reg[2]_0\ <= \^q_reg[2]_0\;
  \q_reg[2]_1\ <= \^q_reg[2]_1\;
\FSM_sequential_state[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFEFFF"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(4),
      I3 => \^q\(3),
      I4 => \^q\(2),
      O => \q_reg[1]_0\
    );
done_OBUF_inst_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000040"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \^q\(4),
      I3 => \^q\(0),
      I4 => \^q\(1),
      O => done_OBUF
    );
\q[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"51"
    )
        port map (
      I0 => \^q\(0),
      I1 => start_IBUF,
      I2 => \q_reg[0]_0\(0),
      O => \q[0]_i_1_n_0\
    );
\q[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6606"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => start_IBUF,
      I3 => \q_reg[0]_0\(0),
      O => \q[1]_i_1_n_0\
    );
\q[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"78780078"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^q\(2),
      I3 => start_IBUF,
      I4 => \q_reg[0]_0\(0),
      O => \q[2]_i_1_n_0\
    );
\q[3]_i_1__2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"15554000"
    )
        port map (
      I0 => \q_reg[4]_0\,
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => \^q\(3),
      O => \q[3]_i_1__2_n_0\
    );
\q[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1555555540000000"
    )
        port map (
      I0 => \q_reg[4]_0\,
      I1 => \^q\(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => \^q\(3),
      I5 => \^q\(4),
      O => \q[4]_i_2_n_0\
    );
\q_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => \q[0]_i_1_n_0\,
      Q => \^q\(0)
    );
\q_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => \q[1]_i_1_n_0\,
      Q => \^q\(1)
    );
\q_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => \q[2]_i_1_n_0\,
      Q => \^q\(2)
    );
\q_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => \q[3]_i_1__2_n_0\,
      Q => \^q\(3)
    );
\q_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => \q[4]_i_2_n_0\,
      Q => \^q\(4)
    );
\y0_carry__1_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q\(3),
      I1 => \y0_carry__4_i_2__0_0\(13),
      I2 => \^q\(4),
      I3 => \y0_carry__4_i_2__0_0\(5),
      O => \y0_carry__1_i_10_n_0\
    );
\y0_carry__1_i_10__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q\(3),
      I1 => \y0_carry__4_i_2__1_0\(13),
      I2 => \^q\(4),
      I3 => \y0_carry__4_i_2__1_0\(5),
      O => \y0_carry__1_i_10__0_n_0\
    );
\y0_carry__1_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q\(3),
      I1 => \y0_carry__4_i_2__0_0\(13),
      I2 => \^q\(4),
      I3 => \y0_carry__4_i_2__0_0\(6),
      O => \y0_carry__1_i_11_n_0\
    );
\y0_carry__1_i_11__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q\(3),
      I1 => \y0_carry__4_i_2__1_0\(13),
      I2 => \^q\(4),
      I3 => \y0_carry__4_i_2__1_0\(6),
      O => \y0_carry__1_i_11__0_n_0\
    );
\y0_carry__1_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4_i_2__1_0\(1),
      I1 => \y0_carry__1_i_5_n_0\,
      I2 => \^q\(0),
      I3 => \y0_carry__1_i_6_n_0\,
      I4 => \y0_carry__1\(0),
      O => S(1)
    );
\y0_carry__1_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4_i_2__0_0\(1),
      I1 => \y0_carry__1_i_5__0_n_0\,
      I2 => \^q\(0),
      I3 => \y0_carry__1_i_6__0_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[11]\(1)
    );
\y0_carry__1_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4_i_2__1_0\(0),
      I1 => \y0_carry__1_i_6_n_0\,
      I2 => \^q\(0),
      I3 => \^q_reg[1]_2\,
      I4 => \y0_carry__1\(0),
      O => S(0)
    );
\y0_carry__1_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4_i_2__0_0\(0),
      I1 => \y0_carry__1_i_6__0_n_0\,
      I2 => \^q\(0),
      I3 => \^q_reg[1]_5\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[11]\(0)
    );
\y0_carry__1_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__1_i_9_n_0\,
      I1 => \y0_carry__1_i_3__0_0\,
      I2 => \^q\(1),
      I3 => \y0_carry__1_i_10_n_0\,
      I4 => \^q\(2),
      I5 => \y0_carry__1_i_3__0\,
      O => \y0_carry__1_i_5_n_0\
    );
\y0_carry__1_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__1_i_9__0_n_0\,
      I1 => \y0_carry__1_i_3__1_0\,
      I2 => \^q\(1),
      I3 => \y0_carry__1_i_10__0_n_0\,
      I4 => \^q\(2),
      I5 => \y0_carry__1_i_3__1\,
      O => \y0_carry__1_i_5__0_n_0\
    );
\y0_carry__1_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__1_i_11_n_0\,
      I1 => \y0_carry__1_i_2__0_0\,
      I2 => \^q\(1),
      I3 => \y0_carry__1_i_2__0_1\,
      I4 => \^q\(2),
      I5 => \y0_carry__1_i_2__0_2\,
      O => \y0_carry__1_i_6_n_0\
    );
\y0_carry__1_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__1_i_11__0_n_0\,
      I1 => \y0_carry__1_i_2__1_0\,
      I2 => \^q\(1),
      I3 => \y0_carry__1_i_2__1_1\,
      I4 => \^q\(2),
      I5 => \y0_carry__1_i_2__1_2\,
      O => \y0_carry__1_i_6__0_n_0\
    );
\y0_carry__1_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__1_i_10_n_0\,
      I1 => \y0_carry__1_i_3__0\,
      I2 => \^q\(1),
      I3 => \y0_carry__1_i_3__0_0\,
      I4 => \^q\(2),
      I5 => \y0_carry__1_i_3__0_1\,
      O => \^q_reg[1]_2\
    );
\y0_carry__1_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__1_i_10__0_n_0\,
      I1 => \y0_carry__1_i_3__1\,
      I2 => \^q\(1),
      I3 => \y0_carry__1_i_3__1_0\,
      I4 => \^q\(2),
      I5 => \y0_carry__1_i_3__1_1\,
      O => \^q_reg[1]_5\
    );
\y0_carry__1_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q\(3),
      I1 => \y0_carry__4_i_2__0_0\(13),
      I2 => \^q\(4),
      I3 => \y0_carry__4_i_2__0_0\(7),
      O => \y0_carry__1_i_9_n_0\
    );
\y0_carry__1_i_9__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q\(3),
      I1 => \y0_carry__4_i_2__1_0\(13),
      I2 => \^q\(4),
      I3 => \y0_carry__4_i_2__1_0\(7),
      O => \y0_carry__1_i_9__0_n_0\
    );
\y0_carry__2_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q\(3),
      I1 => \y0_carry__4_i_2__0_0\(13),
      I2 => \^q\(4),
      I3 => \y0_carry__4_i_2__0_0\(8),
      O => \y0_carry__2_i_12_n_0\
    );
\y0_carry__2_i_12__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"CDC8"
    )
        port map (
      I0 => \^q\(3),
      I1 => \y0_carry__4_i_2__1_0\(13),
      I2 => \^q\(4),
      I3 => \y0_carry__4_i_2__1_0\(8),
      O => \y0_carry__2_i_12__0_n_0\
    );
\y0_carry__2_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4_i_2__1_0\(4),
      I1 => \^q_reg[1]_1\,
      I2 => \^q\(0),
      I3 => \y0_carry__2_i_7_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[14]\(2)
    );
\y0_carry__2_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4_i_2__0_0\(4),
      I1 => \^q_reg[1]_4\,
      I2 => \^q\(0),
      I3 => \y0_carry__2_i_7__0_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[14]_0\(2)
    );
\y0_carry__2_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4_i_2__1_0\(3),
      I1 => \y0_carry__2_i_7_n_0\,
      I2 => \^q\(0),
      I3 => \y0_carry__2_i_8_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[14]\(1)
    );
\y0_carry__2_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4_i_2__0_0\(3),
      I1 => \y0_carry__2_i_7__0_n_0\,
      I2 => \^q\(0),
      I3 => \y0_carry__2_i_8__0_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[14]_0\(1)
    );
\y0_carry__2_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4_i_2__1_0\(2),
      I1 => \y0_carry__2_i_8_n_0\,
      I2 => \^q\(0),
      I3 => \y0_carry__1_i_5_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[14]\(0)
    );
\y0_carry__2_i_4__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4_i_2__0_0\(2),
      I1 => \y0_carry__2_i_8__0_n_0\,
      I2 => \^q\(0),
      I3 => \y0_carry__1_i_5__0_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[14]_0\(0)
    );
\y0_carry__2_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \y0_carry__2_i_1__0\,
      I1 => \^q\(1),
      I2 => \y0_carry__2_i_12_n_0\,
      I3 => \^q\(2),
      I4 => \y0_carry__1_i_2__0_1\,
      O => \^q_reg[1]_1\
    );
\y0_carry__2_i_6__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \y0_carry__2_i_1__1\,
      I1 => \^q\(1),
      I2 => \y0_carry__2_i_12__0_n_0\,
      I3 => \^q\(2),
      I4 => \y0_carry__1_i_2__1_1\,
      O => \^q_reg[1]_4\
    );
\y0_carry__2_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \y0_carry__2_i_3__0_0\,
      I1 => \^q\(1),
      I2 => \y0_carry__1_i_9_n_0\,
      I3 => \^q\(2),
      I4 => \y0_carry__1_i_3__0_0\,
      O => \y0_carry__2_i_7_n_0\
    );
\y0_carry__2_i_7__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \y0_carry__2_i_3__1_0\,
      I1 => \^q\(1),
      I2 => \y0_carry__1_i_9__0_n_0\,
      I3 => \^q\(2),
      I4 => \y0_carry__1_i_3__1_0\,
      O => \y0_carry__2_i_7__0_n_0\
    );
\y0_carry__2_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__2_i_12_n_0\,
      I1 => \y0_carry__1_i_2__0_1\,
      I2 => \^q\(1),
      I3 => \y0_carry__1_i_11_n_0\,
      I4 => \^q\(2),
      I5 => \y0_carry__1_i_2__0_0\,
      O => \y0_carry__2_i_8_n_0\
    );
\y0_carry__2_i_8__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry__2_i_12__0_n_0\,
      I1 => \y0_carry__1_i_2__1_1\,
      I2 => \^q\(1),
      I3 => \y0_carry__1_i_11__0_n_0\,
      I4 => \^q\(2),
      I5 => \y0_carry__1_i_2__1_0\,
      O => \y0_carry__2_i_8__0_n_0\
    );
\y0_carry__3_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \y0_carry__4_i_2__0_0\(13),
      I3 => \^q\(4),
      I4 => \y0_carry__4_i_2__0_0\(9),
      O => \y0_carry__3_i_10_n_0\
    );
\y0_carry__3_i_10__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \y0_carry__4_i_2__1_0\(13),
      I3 => \^q\(4),
      I4 => \y0_carry__4_i_2__1_0\(9),
      O => \y0_carry__3_i_10__0_n_0\
    );
\y0_carry__3_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \y0_carry__4_i_2__0_0\(13),
      I3 => \^q\(4),
      I4 => \y0_carry__4_i_2__0_0\(10),
      O => \y0_carry__3_i_11_n_0\
    );
\y0_carry__3_i_11__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \y0_carry__4_i_2__1_0\(13),
      I3 => \^q\(4),
      I4 => \y0_carry__4_i_2__1_0\(10),
      O => \y0_carry__3_i_11__0_n_0\
    );
\y0_carry__3_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4_i_2__1_0\(8),
      I1 => \y0_carry__3_i_5_n_0\,
      I2 => \^q\(0),
      I3 => \y0_carry__3_i_6_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[19]\(1)
    );
\y0_carry__3_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4_i_2__0_0\(8),
      I1 => \y0_carry__3_i_5__0_n_0\,
      I2 => \^q\(0),
      I3 => \y0_carry__3_i_6__0_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[19]_0\(1)
    );
\y0_carry__3_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4_i_2__1_0\(7),
      I1 => \y0_carry__3_i_6_n_0\,
      I2 => \^q\(0),
      I3 => \^q_reg[1]_3\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[19]\(0)
    );
\y0_carry__3_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4_i_2__0_0\(7),
      I1 => \y0_carry__3_i_6__0_n_0\,
      I2 => \^q\(0),
      I3 => \^q_reg[1]_6\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[19]_0\(0)
    );
\y0_carry__3_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \y0_carry__3_i_9_n_0\,
      I1 => \^q\(1),
      I2 => \y0_carry__3_i_10_n_0\,
      O => \y0_carry__3_i_5_n_0\
    );
\y0_carry__3_i_5__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \y0_carry__3_i_9__0_n_0\,
      I1 => \^q\(1),
      I2 => \y0_carry__3_i_10__0_n_0\,
      O => \y0_carry__3_i_5__0_n_0\
    );
\y0_carry__3_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \y0_carry__3_i_11_n_0\,
      I1 => \^q\(1),
      I2 => \y0_carry__3_i_2__0_0\,
      O => \y0_carry__3_i_6_n_0\
    );
\y0_carry__3_i_6__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \y0_carry__3_i_11__0_n_0\,
      I1 => \^q\(1),
      I2 => \y0_carry__3_i_2__1_0\,
      O => \y0_carry__3_i_6__0_n_0\
    );
\y0_carry__3_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \y0_carry__3_i_10_n_0\,
      I1 => \^q\(1),
      I2 => \y0_carry__3_i_3__0\,
      O => \^q_reg[1]_3\
    );
\y0_carry__3_i_7__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \y0_carry__3_i_10__0_n_0\,
      I1 => \^q\(1),
      I2 => \y0_carry__3_i_3__1\,
      O => \^q_reg[1]_6\
    );
\y0_carry__3_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \y0_carry__4_i_2__0_0\(13),
      I3 => \^q\(4),
      I4 => \y0_carry__4_i_2__0_0\(11),
      O => \y0_carry__3_i_9_n_0\
    );
\y0_carry__3_i_9__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \y0_carry__4_i_2__1_0\(13),
      I3 => \^q\(4),
      I4 => \y0_carry__4_i_2__1_0\(11),
      O => \y0_carry__3_i_9__0_n_0\
    );
\y0_carry__4_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4_i_2__1_0\(11),
      I1 => \y0_carry__4_i_6_n_0\,
      I2 => \^q\(0),
      I3 => \y0_carry__4_i_7_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[22]\(2)
    );
\y0_carry__4_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4_i_2__0_0\(11),
      I1 => \y0_carry__4_i_6__0_n_0\,
      I2 => \^q\(0),
      I3 => \y0_carry__4_i_7__0_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[22]_0\(2)
    );
\y0_carry__4_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4_i_2__1_0\(10),
      I1 => \y0_carry__4_i_7_n_0\,
      I2 => \^q\(0),
      I3 => \y0_carry__4_i_8_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[22]\(1)
    );
\y0_carry__4_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4_i_2__0_0\(10),
      I1 => \y0_carry__4_i_7__0_n_0\,
      I2 => \^q\(0),
      I3 => \y0_carry__4_i_8__0_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[22]_0\(1)
    );
\y0_carry__4_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4_i_2__1_0\(9),
      I1 => \y0_carry__4_i_8_n_0\,
      I2 => \^q\(0),
      I3 => \y0_carry__3_i_5_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[22]\(0)
    );
\y0_carry__4_i_4__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4_i_2__0_0\(9),
      I1 => \y0_carry__4_i_8__0_n_0\,
      I2 => \^q\(0),
      I3 => \y0_carry__3_i_5__0_n_0\,
      I4 => \y0_carry__1\(0),
      O => \q_reg[22]_0\(0)
    );
\y0_carry__4_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \y0_carry__4_i_2__0_0\(13),
      I3 => \^q\(4),
      I4 => \y0_carry__4_i_2__0_0\(12),
      O => \^q_reg[2]_0\
    );
\y0_carry__4_i_5__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0F1F0E0"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(3),
      I2 => \y0_carry__4_i_2__1_0\(13),
      I3 => \^q\(4),
      I4 => \y0_carry__4_i_2__1_0\(12),
      O => \^q_reg[2]_1\
    );
\y0_carry__4_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FF01FF00FE00"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => \^q\(3),
      I3 => \y0_carry__4_i_2__0_0\(13),
      I4 => \^q\(4),
      I5 => \y0_carry__4_i_2__0_0\(12),
      O => \y0_carry__4_i_6_n_0\
    );
\y0_carry__4_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FF01FF00FE00"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => \^q\(3),
      I3 => \y0_carry__4_i_2__1_0\(13),
      I4 => \^q\(4),
      I5 => \y0_carry__4_i_2__1_0\(12),
      O => \y0_carry__4_i_6__0_n_0\
    );
\y0_carry__4_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FF01FF00FE00"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => \^q\(3),
      I3 => \y0_carry__4_i_2__0_0\(13),
      I4 => \^q\(4),
      I5 => \y0_carry__4_i_2__0_0\(11),
      O => \y0_carry__4_i_7_n_0\
    );
\y0_carry__4_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FF01FF00FE00"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(2),
      I2 => \^q\(3),
      I3 => \y0_carry__4_i_2__1_0\(13),
      I4 => \^q\(4),
      I5 => \y0_carry__4_i_2__1_0\(11),
      O => \y0_carry__4_i_7__0_n_0\
    );
\y0_carry__4_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q_reg[2]_0\,
      I1 => \^q\(1),
      I2 => \y0_carry__3_i_11_n_0\,
      O => \y0_carry__4_i_8_n_0\
    );
\y0_carry__4_i_8__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q_reg[2]_1\,
      I1 => \^q\(1),
      I2 => \y0_carry__3_i_11__0_n_0\,
      O => \y0_carry__4_i_8__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \reg__parameterized1\ is
  port (
    \q_reg[1]_0\ : out STD_LOGIC;
    \q_reg[23]_0\ : out STD_LOGIC;
    \q_reg[19]_0\ : out STD_LOGIC;
    \q_reg[21]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 24 downto 0 );
    \q_reg[1]_1\ : out STD_LOGIC;
    \q_reg[23]_1\ : out STD_LOGIC;
    \q_reg[21]_1\ : out STD_LOGIC;
    \q_reg[22]_0\ : out STD_LOGIC;
    \q_reg[18]_0\ : out STD_LOGIC;
    \q_reg[20]_0\ : out STD_LOGIC;
    \q_reg[1]_2\ : out STD_LOGIC;
    \q_reg[22]_1\ : out STD_LOGIC;
    \q_reg[20]_1\ : out STD_LOGIC;
    \q_reg[24]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[8]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[16]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[9]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[15]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[17]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[23]_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \y0_carry__1_i_6\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \y0_carry__4\ : in STD_LOGIC_VECTOR ( 10 downto 0 );
    y0_carry : in STD_LOGIC_VECTOR ( 0 to 0 );
    \y0_carry__1\ : in STD_LOGIC;
    \y0_carry__1_0\ : in STD_LOGIC;
    \y0_carry__2\ : in STD_LOGIC;
    \y0_carry__2_0\ : in STD_LOGIC;
    \y0_carry__3\ : in STD_LOGIC;
    \y0_carry__3_0\ : in STD_LOGIC;
    \y0_carry__4_0\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 24 downto 0 );
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \reg__parameterized1\ : entity is "reg";
end \reg__parameterized1\;

architecture STRUCTURE of \reg__parameterized1\ is
  signal \^q\ : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \^q_reg[18]_0\ : STD_LOGIC;
  signal \^q_reg[19]_0\ : STD_LOGIC;
  signal \^q_reg[1]_0\ : STD_LOGIC;
  signal \^q_reg[1]_1\ : STD_LOGIC;
  signal \^q_reg[1]_2\ : STD_LOGIC;
  signal \^q_reg[20]_0\ : STD_LOGIC;
  signal \^q_reg[20]_1\ : STD_LOGIC;
  signal \^q_reg[21]_0\ : STD_LOGIC;
  signal \^q_reg[21]_1\ : STD_LOGIC;
  signal \^q_reg[22]_0\ : STD_LOGIC;
  signal \^q_reg[22]_1\ : STD_LOGIC;
  signal \^q_reg[23]_0\ : STD_LOGIC;
  signal \^q_reg[23]_1\ : STD_LOGIC;
  signal \y0_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \y0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \y0_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \y0_carry__0_i_8_n_0\ : STD_LOGIC;
  signal y0_carry_i_11_n_0 : STD_LOGIC;
  signal y0_carry_i_12_n_0 : STD_LOGIC;
  signal y0_carry_i_13_n_0 : STD_LOGIC;
  signal y0_carry_i_14_n_0 : STD_LOGIC;
  signal y0_carry_i_15_n_0 : STD_LOGIC;
  signal y0_carry_i_16_n_0 : STD_LOGIC;
  signal y0_carry_i_17_n_0 : STD_LOGIC;
  signal y0_carry_i_18_n_0 : STD_LOGIC;
  signal y0_carry_i_19_n_0 : STD_LOGIC;
  signal y0_carry_i_20_n_0 : STD_LOGIC;
  signal y0_carry_i_5_n_0 : STD_LOGIC;
  signal y0_carry_i_6_n_0 : STD_LOGIC;
  signal y0_carry_i_7_n_0 : STD_LOGIC;
  signal y0_carry_i_8_n_0 : STD_LOGIC;
  signal y0_carry_i_9_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \y0_carry__2_i_5\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \y0_carry__3_i_8\ : label is "soft_lutpair37";
begin
  Q(24 downto 0) <= \^q\(24 downto 0);
  \q_reg[18]_0\ <= \^q_reg[18]_0\;
  \q_reg[19]_0\ <= \^q_reg[19]_0\;
  \q_reg[1]_0\ <= \^q_reg[1]_0\;
  \q_reg[1]_1\ <= \^q_reg[1]_1\;
  \q_reg[1]_2\ <= \^q_reg[1]_2\;
  \q_reg[20]_0\ <= \^q_reg[20]_0\;
  \q_reg[20]_1\ <= \^q_reg[20]_1\;
  \q_reg[21]_0\ <= \^q_reg[21]_0\;
  \q_reg[21]_1\ <= \^q_reg[21]_1\;
  \q_reg[22]_0\ <= \^q_reg[22]_0\;
  \q_reg[22]_1\ <= \^q_reg[22]_1\;
  \q_reg[23]_0\ <= \^q_reg[23]_0\;
  \q_reg[23]_1\ <= \^q_reg[23]_1\;
\q_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(0),
      Q => \^q\(0)
    );
\q_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(10),
      Q => \^q\(10)
    );
\q_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(11),
      Q => \^q\(11)
    );
\q_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(12),
      Q => \^q\(12)
    );
\q_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(13),
      Q => \^q\(13)
    );
\q_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(14),
      Q => \^q\(14)
    );
\q_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(15),
      Q => \^q\(15)
    );
\q_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(16),
      Q => \^q\(16)
    );
\q_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(17),
      Q => \^q\(17)
    );
\q_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(18),
      Q => \^q\(18)
    );
\q_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(19),
      Q => \^q\(19)
    );
\q_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(1),
      Q => \^q\(1)
    );
\q_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(20),
      Q => \^q\(20)
    );
\q_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(21),
      Q => \^q\(21)
    );
\q_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(22),
      Q => \^q\(22)
    );
\q_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(23),
      Q => \^q\(23)
    );
\q_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(24),
      Q => \^q\(24)
    );
\q_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(2),
      Q => \^q\(2)
    );
\q_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(3),
      Q => \^q\(3)
    );
\q_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(4),
      Q => \^q\(4)
    );
\q_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(5),
      Q => \^q\(5)
    );
\q_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(6),
      Q => \^q\(6)
    );
\q_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(7),
      Q => \^q\(7)
    );
\q_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(8),
      Q => \^q\(8)
    );
\q_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(9),
      Q => \^q\(9)
    );
\y0_carry__0_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(20),
      I1 => \y0_carry__1_i_6\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6\(4),
      I4 => \^q\(12),
      O => \^q_reg[20]_0\
    );
\y0_carry__0_i_11\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(21),
      I1 => \y0_carry__1_i_6\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6\(4),
      I4 => \^q\(13),
      O => \^q_reg[21]_0\
    );
\y0_carry__0_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(19),
      I1 => \y0_carry__1_i_6\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6\(4),
      I4 => \^q\(11),
      O => \^q_reg[19]_0\
    );
\y0_carry__0_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4\(7),
      I1 => \y0_carry__0_i_5_n_0\,
      I2 => \y0_carry__1_i_6\(0),
      I3 => \y0_carry__0_i_6_n_0\,
      I4 => y0_carry(0),
      O => \q_reg[7]_0\(3)
    );
\y0_carry__0_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4\(6),
      I1 => \y0_carry__0_i_6_n_0\,
      I2 => \y0_carry__1_i_6\(0),
      I3 => \y0_carry__0_i_7_n_0\,
      I4 => y0_carry(0),
      O => \q_reg[7]_0\(2)
    );
\y0_carry__0_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4\(5),
      I1 => \y0_carry__0_i_7_n_0\,
      I2 => \y0_carry__1_i_6\(0),
      I3 => \y0_carry__0_i_8_n_0\,
      I4 => y0_carry(0),
      O => \q_reg[7]_0\(1)
    );
\y0_carry__0_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4\(4),
      I1 => \y0_carry__0_i_8_n_0\,
      I2 => \y0_carry__1_i_6\(0),
      I3 => y0_carry_i_5_n_0,
      I4 => y0_carry(0),
      O => \q_reg[7]_0\(0)
    );
\y0_carry__0_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[22]_0\,
      I1 => \^q_reg[18]_0\,
      I2 => \y0_carry__1_i_6\(1),
      I3 => \^q_reg[20]_0\,
      I4 => \y0_carry__1_i_6\(2),
      I5 => y0_carry_i_12_n_0,
      O => \y0_carry__0_i_5_n_0\
    );
\y0_carry__0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[21]_0\,
      I1 => y0_carry_i_14_n_0,
      I2 => \y0_carry__1_i_6\(1),
      I3 => \^q_reg[19]_0\,
      I4 => \y0_carry__1_i_6\(2),
      I5 => y0_carry_i_16_n_0,
      O => \y0_carry__0_i_6_n_0\
    );
\y0_carry__0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[20]_0\,
      I1 => y0_carry_i_12_n_0,
      I2 => \y0_carry__1_i_6\(1),
      I3 => \^q_reg[18]_0\,
      I4 => \y0_carry__1_i_6\(2),
      I5 => y0_carry_i_11_n_0,
      O => \y0_carry__0_i_7_n_0\
    );
\y0_carry__0_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[19]_0\,
      I1 => y0_carry_i_16_n_0,
      I2 => \y0_carry__1_i_6\(1),
      I3 => y0_carry_i_14_n_0,
      I4 => \y0_carry__1_i_6\(2),
      I5 => y0_carry_i_15_n_0,
      O => \y0_carry__0_i_8_n_0\
    );
\y0_carry__0_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(22),
      I1 => \y0_carry__1_i_6\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6\(4),
      I4 => \^q\(14),
      O => \^q_reg[22]_0\
    );
\y0_carry__1_i_12\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(23),
      I1 => \y0_carry__1_i_6\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6\(4),
      I4 => \^q\(15),
      O => \^q_reg[23]_0\
    );
\y0_carry__1_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \^q\(9),
      I1 => \y0_carry__1\,
      I2 => \y0_carry__1_i_6\(0),
      I3 => \y0_carry__1_0\,
      I4 => y0_carry(0),
      O => \q_reg[9]_0\(0)
    );
\y0_carry__1_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4\(8),
      I1 => \^q_reg[1]_0\,
      I2 => \y0_carry__1_i_6\(0),
      I3 => \y0_carry__0_i_5_n_0\,
      I4 => y0_carry(0),
      O => \q_reg[8]_0\(0)
    );
\y0_carry__1_i_8\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[23]_0\,
      I1 => \^q_reg[19]_0\,
      I2 => \y0_carry__1_i_6\(1),
      I3 => \^q_reg[21]_0\,
      I4 => \y0_carry__1_i_6\(2),
      I5 => y0_carry_i_14_n_0,
      O => \^q_reg[1]_0\
    );
\y0_carry__2_i_10\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q\(20),
      I1 => \y0_carry__1_i_6\(2),
      I2 => \y0_carry__1_i_6\(3),
      I3 => \^q\(24),
      I4 => \y0_carry__1_i_6\(4),
      I5 => \^q\(16),
      O => \^q_reg[20]_1\
    );
\y0_carry__2_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q\(21),
      I1 => \y0_carry__1_i_6\(2),
      I2 => \y0_carry__1_i_6\(3),
      I3 => \^q\(24),
      I4 => \y0_carry__1_i_6\(4),
      I5 => \^q\(17),
      O => \^q_reg[21]_1\
    );
\y0_carry__2_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \^q\(15),
      I1 => \y0_carry__2\,
      I2 => \y0_carry__1_i_6\(0),
      I3 => \y0_carry__2_0\,
      I4 => y0_carry(0),
      O => \q_reg[15]_0\(0)
    );
\y0_carry__2_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q_reg[22]_1\,
      I1 => \y0_carry__1_i_6\(1),
      I2 => \^q_reg[20]_1\,
      O => \^q_reg[1]_2\
    );
\y0_carry__2_i_9\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q\(22),
      I1 => \y0_carry__1_i_6\(2),
      I2 => \y0_carry__1_i_6\(3),
      I3 => \^q\(24),
      I4 => \y0_carry__1_i_6\(4),
      I5 => \^q\(18),
      O => \^q_reg[22]_1\
    );
\y0_carry__3_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q\(23),
      I1 => \y0_carry__1_i_6\(2),
      I2 => \y0_carry__1_i_6\(3),
      I3 => \^q\(24),
      I4 => \y0_carry__1_i_6\(4),
      I5 => \^q\(19),
      O => \^q_reg[23]_1\
    );
\y0_carry__3_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \^q\(17),
      I1 => \y0_carry__3\,
      I2 => \y0_carry__1_i_6\(0),
      I3 => \y0_carry__3_0\,
      I4 => y0_carry(0),
      O => \q_reg[17]_0\(0)
    );
\y0_carry__3_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4\(9),
      I1 => \^q_reg[1]_1\,
      I2 => \y0_carry__1_i_6\(0),
      I3 => \^q_reg[1]_2\,
      I4 => y0_carry(0),
      O => \q_reg[16]_0\(0)
    );
\y0_carry__3_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q_reg[23]_1\,
      I1 => \y0_carry__1_i_6\(1),
      I2 => \^q_reg[21]_1\,
      O => \^q_reg[1]_1\
    );
\y0_carry__4_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6665666A999A9995"
    )
        port map (
      I0 => \^q\(23),
      I1 => \y0_carry__4\(10),
      I2 => \y0_carry__1_i_6\(0),
      I3 => \y0_carry__1_i_6\(1),
      I4 => \y0_carry__4_0\,
      I5 => y0_carry(0),
      O => \q_reg[23]_2\(0)
    );
\y0_carry__5_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(24),
      I1 => \y0_carry__4\(10),
      I2 => y0_carry(0),
      O => \q_reg[24]_0\(0)
    );
y0_carry_i_10: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(18),
      I1 => \y0_carry__1_i_6\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6\(4),
      I4 => \^q\(10),
      O => \^q_reg[18]_0\
    );
y0_carry_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(14),
      I2 => \y0_carry__1_i_6\(3),
      I3 => \^q\(22),
      I4 => \y0_carry__1_i_6\(4),
      I5 => \^q\(6),
      O => y0_carry_i_11_n_0
    );
y0_carry_i_12: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(16),
      I1 => \y0_carry__1_i_6\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6\(4),
      I4 => \^q\(8),
      O => y0_carry_i_12_n_0
    );
y0_carry_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(12),
      I2 => \y0_carry__1_i_6\(3),
      I3 => \^q\(20),
      I4 => \y0_carry__1_i_6\(4),
      I5 => \^q\(4),
      O => y0_carry_i_13_n_0
    );
y0_carry_i_14: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(17),
      I1 => \y0_carry__1_i_6\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6\(4),
      I4 => \^q\(9),
      O => y0_carry_i_14_n_0
    );
y0_carry_i_15: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(13),
      I2 => \y0_carry__1_i_6\(3),
      I3 => \^q\(21),
      I4 => \y0_carry__1_i_6\(4),
      I5 => \^q\(5),
      O => y0_carry_i_15_n_0
    );
y0_carry_i_16: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(15),
      I2 => \y0_carry__1_i_6\(3),
      I3 => \^q\(23),
      I4 => \y0_carry__1_i_6\(4),
      I5 => \^q\(7),
      O => y0_carry_i_16_n_0
    );
y0_carry_i_17: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(11),
      I2 => \y0_carry__1_i_6\(3),
      I3 => \^q\(19),
      I4 => \y0_carry__1_i_6\(4),
      I5 => \^q\(3),
      O => y0_carry_i_17_n_0
    );
y0_carry_i_18: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(10),
      I2 => \y0_carry__1_i_6\(3),
      I3 => \^q\(18),
      I4 => \y0_carry__1_i_6\(4),
      I5 => \^q\(2),
      O => y0_carry_i_18_n_0
    );
y0_carry_i_19: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(9),
      I2 => \y0_carry__1_i_6\(3),
      I3 => \^q\(17),
      I4 => \y0_carry__1_i_6\(4),
      I5 => \^q\(1),
      O => y0_carry_i_19_n_0
    );
\y0_carry_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4\(3),
      I1 => y0_carry_i_5_n_0,
      I2 => \y0_carry__1_i_6\(0),
      I3 => y0_carry_i_6_n_0,
      I4 => y0_carry(0),
      O => S(3)
    );
y0_carry_i_20: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(8),
      I2 => \y0_carry__1_i_6\(3),
      I3 => \^q\(16),
      I4 => \y0_carry__1_i_6\(4),
      I5 => \^q\(0),
      O => y0_carry_i_20_n_0
    );
\y0_carry_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4\(2),
      I1 => y0_carry_i_6_n_0,
      I2 => \y0_carry__1_i_6\(0),
      I3 => y0_carry_i_7_n_0,
      I4 => y0_carry(0),
      O => S(2)
    );
\y0_carry_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4\(1),
      I1 => y0_carry_i_7_n_0,
      I2 => \y0_carry__1_i_6\(0),
      I3 => y0_carry_i_8_n_0,
      I4 => y0_carry(0),
      O => S(1)
    );
\y0_carry_i_4__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \y0_carry__4\(0),
      I1 => y0_carry_i_8_n_0,
      I2 => \y0_carry__1_i_6\(0),
      I3 => y0_carry_i_9_n_0,
      I4 => y0_carry(0),
      O => S(0)
    );
y0_carry_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[18]_0\,
      I1 => y0_carry_i_11_n_0,
      I2 => \y0_carry__1_i_6\(1),
      I3 => y0_carry_i_12_n_0,
      I4 => \y0_carry__1_i_6\(2),
      I5 => y0_carry_i_13_n_0,
      O => y0_carry_i_5_n_0
    );
y0_carry_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => y0_carry_i_14_n_0,
      I1 => y0_carry_i_15_n_0,
      I2 => \y0_carry__1_i_6\(1),
      I3 => y0_carry_i_16_n_0,
      I4 => \y0_carry__1_i_6\(2),
      I5 => y0_carry_i_17_n_0,
      O => y0_carry_i_6_n_0
    );
y0_carry_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => y0_carry_i_12_n_0,
      I1 => y0_carry_i_13_n_0,
      I2 => \y0_carry__1_i_6\(1),
      I3 => y0_carry_i_11_n_0,
      I4 => \y0_carry__1_i_6\(2),
      I5 => y0_carry_i_18_n_0,
      O => y0_carry_i_7_n_0
    );
y0_carry_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => y0_carry_i_16_n_0,
      I1 => y0_carry_i_17_n_0,
      I2 => \y0_carry__1_i_6\(1),
      I3 => y0_carry_i_15_n_0,
      I4 => \y0_carry__1_i_6\(2),
      I5 => y0_carry_i_19_n_0,
      O => y0_carry_i_8_n_0
    );
y0_carry_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => y0_carry_i_11_n_0,
      I1 => y0_carry_i_18_n_0,
      I2 => \y0_carry__1_i_6\(1),
      I3 => y0_carry_i_13_n_0,
      I4 => \y0_carry__1_i_6\(2),
      I5 => y0_carry_i_20_n_0,
      O => y0_carry_i_9_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \reg__parameterized1_1\ is
  port (
    \q_reg[1]_0\ : out STD_LOGIC;
    \q_reg[23]_0\ : out STD_LOGIC;
    \q_reg[19]_0\ : out STD_LOGIC;
    \q_reg[21]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 24 downto 0 );
    \q_reg[1]_1\ : out STD_LOGIC;
    \q_reg[23]_1\ : out STD_LOGIC;
    \q_reg[21]_1\ : out STD_LOGIC;
    \q_reg[22]_0\ : out STD_LOGIC;
    \q_reg[18]_0\ : out STD_LOGIC;
    \q_reg[20]_0\ : out STD_LOGIC;
    \q_reg[1]_2\ : out STD_LOGIC;
    \q_reg[22]_1\ : out STD_LOGIC;
    \q_reg[20]_1\ : out STD_LOGIC;
    \q_reg[9]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[15]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[17]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[23]_2\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[8]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[16]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \y0_carry__1_i_6__0\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \y0_carry__1\ : in STD_LOGIC;
    \y0_carry__1_0\ : in STD_LOGIC;
    \y0_carry__1_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \y0_carry__2\ : in STD_LOGIC;
    \y0_carry__2_0\ : in STD_LOGIC;
    \y0_carry__3\ : in STD_LOGIC;
    \y0_carry__3_0\ : in STD_LOGIC;
    \y0_carry__4\ : in STD_LOGIC_VECTOR ( 10 downto 0 );
    \y0_carry__4_0\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 24 downto 0 );
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \reg__parameterized1_1\ : entity is "reg";
end \reg__parameterized1_1\;

architecture STRUCTURE of \reg__parameterized1_1\ is
  signal \^q\ : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \^q_reg[18]_0\ : STD_LOGIC;
  signal \^q_reg[19]_0\ : STD_LOGIC;
  signal \^q_reg[1]_0\ : STD_LOGIC;
  signal \^q_reg[1]_1\ : STD_LOGIC;
  signal \^q_reg[1]_2\ : STD_LOGIC;
  signal \^q_reg[20]_0\ : STD_LOGIC;
  signal \^q_reg[20]_1\ : STD_LOGIC;
  signal \^q_reg[21]_0\ : STD_LOGIC;
  signal \^q_reg[21]_1\ : STD_LOGIC;
  signal \^q_reg[22]_0\ : STD_LOGIC;
  signal \^q_reg[22]_1\ : STD_LOGIC;
  signal \^q_reg[23]_0\ : STD_LOGIC;
  signal \^q_reg[23]_1\ : STD_LOGIC;
  signal \y0_carry__0_i_5__0_n_0\ : STD_LOGIC;
  signal \y0_carry__0_i_6__0_n_0\ : STD_LOGIC;
  signal \y0_carry__0_i_7__0_n_0\ : STD_LOGIC;
  signal \y0_carry__0_i_8__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_11__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_12__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_13__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_14__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_15__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_16__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_17__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_18__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_19__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_20__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_5__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_6__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_7__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_8__0_n_0\ : STD_LOGIC;
  signal \y0_carry_i_9__0_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \y0_carry__2_i_5__0\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \y0_carry__3_i_8__0\ : label is "soft_lutpair38";
begin
  Q(24 downto 0) <= \^q\(24 downto 0);
  \q_reg[18]_0\ <= \^q_reg[18]_0\;
  \q_reg[19]_0\ <= \^q_reg[19]_0\;
  \q_reg[1]_0\ <= \^q_reg[1]_0\;
  \q_reg[1]_1\ <= \^q_reg[1]_1\;
  \q_reg[1]_2\ <= \^q_reg[1]_2\;
  \q_reg[20]_0\ <= \^q_reg[20]_0\;
  \q_reg[20]_1\ <= \^q_reg[20]_1\;
  \q_reg[21]_0\ <= \^q_reg[21]_0\;
  \q_reg[21]_1\ <= \^q_reg[21]_1\;
  \q_reg[22]_0\ <= \^q_reg[22]_0\;
  \q_reg[22]_1\ <= \^q_reg[22]_1\;
  \q_reg[23]_0\ <= \^q_reg[23]_0\;
  \q_reg[23]_1\ <= \^q_reg[23]_1\;
\q_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(0),
      Q => \^q\(0)
    );
\q_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(10),
      Q => \^q\(10)
    );
\q_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(11),
      Q => \^q\(11)
    );
\q_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(12),
      Q => \^q\(12)
    );
\q_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(13),
      Q => \^q\(13)
    );
\q_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(14),
      Q => \^q\(14)
    );
\q_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(15),
      Q => \^q\(15)
    );
\q_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(16),
      Q => \^q\(16)
    );
\q_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(17),
      Q => \^q\(17)
    );
\q_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(18),
      Q => \^q\(18)
    );
\q_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(19),
      Q => \^q\(19)
    );
\q_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(1),
      Q => \^q\(1)
    );
\q_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(20),
      Q => \^q\(20)
    );
\q_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(21),
      Q => \^q\(21)
    );
\q_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(22),
      Q => \^q\(22)
    );
\q_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(23),
      Q => \^q\(23)
    );
\q_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(24),
      Q => \^q\(24)
    );
\q_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(2),
      Q => \^q\(2)
    );
\q_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(3),
      Q => \^q\(3)
    );
\q_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(4),
      Q => \^q\(4)
    );
\q_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(5),
      Q => \^q\(5)
    );
\q_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(6),
      Q => \^q\(6)
    );
\q_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(7),
      Q => \^q\(7)
    );
\q_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(8),
      Q => \^q\(8)
    );
\q_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(9),
      Q => \^q\(9)
    );
\y0_carry__0_i_10__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(20),
      I1 => \y0_carry__1_i_6__0\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6__0\(4),
      I4 => \^q\(12),
      O => \^q_reg[20]_0\
    );
\y0_carry__0_i_11__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(21),
      I1 => \y0_carry__1_i_6__0\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6__0\(4),
      I4 => \^q\(13),
      O => \^q_reg[21]_0\
    );
\y0_carry__0_i_12__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(19),
      I1 => \y0_carry__1_i_6__0\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6__0\(4),
      I4 => \^q\(11),
      O => \^q_reg[19]_0\
    );
\y0_carry__0_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4\(7),
      I1 => \y0_carry__0_i_5__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(0),
      I3 => \y0_carry__0_i_6__0_n_0\,
      I4 => \y0_carry__1_1\(0),
      O => \q_reg[7]_0\(3)
    );
\y0_carry__0_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4\(6),
      I1 => \y0_carry__0_i_6__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(0),
      I3 => \y0_carry__0_i_7__0_n_0\,
      I4 => \y0_carry__1_1\(0),
      O => \q_reg[7]_0\(2)
    );
\y0_carry__0_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4\(5),
      I1 => \y0_carry__0_i_7__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(0),
      I3 => \y0_carry__0_i_8__0_n_0\,
      I4 => \y0_carry__1_1\(0),
      O => \q_reg[7]_0\(1)
    );
\y0_carry__0_i_4__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4\(4),
      I1 => \y0_carry__0_i_8__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(0),
      I3 => \y0_carry_i_5__0_n_0\,
      I4 => \y0_carry__1_1\(0),
      O => \q_reg[7]_0\(0)
    );
\y0_carry__0_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[22]_0\,
      I1 => \^q_reg[18]_0\,
      I2 => \y0_carry__1_i_6__0\(1),
      I3 => \^q_reg[20]_0\,
      I4 => \y0_carry__1_i_6__0\(2),
      I5 => \y0_carry_i_12__0_n_0\,
      O => \y0_carry__0_i_5__0_n_0\
    );
\y0_carry__0_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[21]_0\,
      I1 => \y0_carry_i_14__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(1),
      I3 => \^q_reg[19]_0\,
      I4 => \y0_carry__1_i_6__0\(2),
      I5 => \y0_carry_i_16__0_n_0\,
      O => \y0_carry__0_i_6__0_n_0\
    );
\y0_carry__0_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[20]_0\,
      I1 => \y0_carry_i_12__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(1),
      I3 => \^q_reg[18]_0\,
      I4 => \y0_carry__1_i_6__0\(2),
      I5 => \y0_carry_i_11__0_n_0\,
      O => \y0_carry__0_i_7__0_n_0\
    );
\y0_carry__0_i_8__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[19]_0\,
      I1 => \y0_carry_i_16__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(1),
      I3 => \y0_carry_i_14__0_n_0\,
      I4 => \y0_carry__1_i_6__0\(2),
      I5 => \y0_carry_i_15__0_n_0\,
      O => \y0_carry__0_i_8__0_n_0\
    );
\y0_carry__0_i_9__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(22),
      I1 => \y0_carry__1_i_6__0\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6__0\(4),
      I4 => \^q\(14),
      O => \^q_reg[22]_0\
    );
\y0_carry__1_i_12__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(23),
      I1 => \y0_carry__1_i_6__0\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6__0\(4),
      I4 => \^q\(15),
      O => \^q_reg[23]_0\
    );
\y0_carry__1_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \^q\(9),
      I1 => \y0_carry__1\,
      I2 => \y0_carry__1_i_6__0\(0),
      I3 => \y0_carry__1_0\,
      I4 => \y0_carry__1_1\(0),
      O => \q_reg[9]_0\(0)
    );
\y0_carry__1_i_4__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4\(8),
      I1 => \^q_reg[1]_0\,
      I2 => \y0_carry__1_i_6__0\(0),
      I3 => \y0_carry__0_i_5__0_n_0\,
      I4 => \y0_carry__1_1\(0),
      O => \q_reg[8]_0\(0)
    );
\y0_carry__1_i_8__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[23]_0\,
      I1 => \^q_reg[19]_0\,
      I2 => \y0_carry__1_i_6__0\(1),
      I3 => \^q_reg[21]_0\,
      I4 => \y0_carry__1_i_6__0\(2),
      I5 => \y0_carry_i_14__0_n_0\,
      O => \^q_reg[1]_0\
    );
\y0_carry__2_i_10__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q\(20),
      I1 => \y0_carry__1_i_6__0\(2),
      I2 => \y0_carry__1_i_6__0\(3),
      I3 => \^q\(24),
      I4 => \y0_carry__1_i_6__0\(4),
      I5 => \^q\(16),
      O => \^q_reg[20]_1\
    );
\y0_carry__2_i_11__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q\(21),
      I1 => \y0_carry__1_i_6__0\(2),
      I2 => \y0_carry__1_i_6__0\(3),
      I3 => \^q\(24),
      I4 => \y0_carry__1_i_6__0\(4),
      I5 => \^q\(17),
      O => \^q_reg[21]_1\
    );
\y0_carry__2_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \^q\(15),
      I1 => \y0_carry__2\,
      I2 => \y0_carry__1_i_6__0\(0),
      I3 => \y0_carry__2_0\,
      I4 => \y0_carry__1_1\(0),
      O => \q_reg[15]_0\(0)
    );
\y0_carry__2_i_5__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q_reg[22]_1\,
      I1 => \y0_carry__1_i_6__0\(1),
      I2 => \^q_reg[20]_1\,
      O => \^q_reg[1]_2\
    );
\y0_carry__2_i_9__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q\(22),
      I1 => \y0_carry__1_i_6__0\(2),
      I2 => \y0_carry__1_i_6__0\(3),
      I3 => \^q\(24),
      I4 => \y0_carry__1_i_6__0\(4),
      I5 => \^q\(18),
      O => \^q_reg[22]_1\
    );
\y0_carry__3_i_12__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FB0BFF00F808"
    )
        port map (
      I0 => \^q\(23),
      I1 => \y0_carry__1_i_6__0\(2),
      I2 => \y0_carry__1_i_6__0\(3),
      I3 => \^q\(24),
      I4 => \y0_carry__1_i_6__0\(4),
      I5 => \^q\(19),
      O => \^q_reg[23]_1\
    );
\y0_carry__3_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9A95656A"
    )
        port map (
      I0 => \^q\(17),
      I1 => \y0_carry__3\,
      I2 => \y0_carry__1_i_6__0\(0),
      I3 => \y0_carry__3_0\,
      I4 => \y0_carry__1_1\(0),
      O => \q_reg[17]_0\(0)
    );
\y0_carry__3_i_4__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4\(9),
      I1 => \^q_reg[1]_1\,
      I2 => \y0_carry__1_i_6__0\(0),
      I3 => \^q_reg[1]_2\,
      I4 => \y0_carry__1_1\(0),
      O => \q_reg[16]_0\(0)
    );
\y0_carry__3_i_8__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \^q_reg[23]_1\,
      I1 => \y0_carry__1_i_6__0\(1),
      I2 => \^q_reg[21]_1\,
      O => \^q_reg[1]_1\
    );
\y0_carry__4_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"999A99956665666A"
    )
        port map (
      I0 => \^q\(23),
      I1 => \y0_carry__4\(10),
      I2 => \y0_carry__1_i_6__0\(0),
      I3 => \y0_carry__1_i_6__0\(1),
      I4 => \y0_carry__4_0\,
      I5 => \y0_carry__1_1\(0),
      O => \q_reg[23]_2\(0)
    );
\y0_carry_i_10__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(18),
      I1 => \y0_carry__1_i_6__0\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6__0\(4),
      I4 => \^q\(10),
      O => \^q_reg[18]_0\
    );
\y0_carry_i_11__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(14),
      I2 => \y0_carry__1_i_6__0\(3),
      I3 => \^q\(22),
      I4 => \y0_carry__1_i_6__0\(4),
      I5 => \^q\(6),
      O => \y0_carry_i_11__0_n_0\
    );
\y0_carry_i_12__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(16),
      I1 => \y0_carry__1_i_6__0\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6__0\(4),
      I4 => \^q\(8),
      O => \y0_carry_i_12__0_n_0\
    );
\y0_carry_i_13__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(12),
      I2 => \y0_carry__1_i_6__0\(3),
      I3 => \^q\(20),
      I4 => \y0_carry__1_i_6__0\(4),
      I5 => \^q\(4),
      O => \y0_carry_i_13__0_n_0\
    );
\y0_carry_i_14__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0BBF088"
    )
        port map (
      I0 => \^q\(17),
      I1 => \y0_carry__1_i_6__0\(3),
      I2 => \^q\(24),
      I3 => \y0_carry__1_i_6__0\(4),
      I4 => \^q\(9),
      O => \y0_carry_i_14__0_n_0\
    );
\y0_carry_i_15__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(13),
      I2 => \y0_carry__1_i_6__0\(3),
      I3 => \^q\(21),
      I4 => \y0_carry__1_i_6__0\(4),
      I5 => \^q\(5),
      O => \y0_carry_i_15__0_n_0\
    );
\y0_carry_i_16__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(15),
      I2 => \y0_carry__1_i_6__0\(3),
      I3 => \^q\(23),
      I4 => \y0_carry__1_i_6__0\(4),
      I5 => \^q\(7),
      O => \y0_carry_i_16__0_n_0\
    );
\y0_carry_i_17__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(11),
      I2 => \y0_carry__1_i_6__0\(3),
      I3 => \^q\(19),
      I4 => \y0_carry__1_i_6__0\(4),
      I5 => \^q\(3),
      O => \y0_carry_i_17__0_n_0\
    );
\y0_carry_i_18__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(10),
      I2 => \y0_carry__1_i_6__0\(3),
      I3 => \^q\(18),
      I4 => \y0_carry__1_i_6__0\(4),
      I5 => \^q\(2),
      O => \y0_carry_i_18__0_n_0\
    );
\y0_carry_i_19__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(9),
      I2 => \y0_carry__1_i_6__0\(3),
      I3 => \^q\(17),
      I4 => \y0_carry__1_i_6__0\(4),
      I5 => \^q\(1),
      O => \y0_carry_i_19__0_n_0\
    );
\y0_carry_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4\(3),
      I1 => \y0_carry_i_5__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(0),
      I3 => \y0_carry_i_6__0_n_0\,
      I4 => \y0_carry__1_1\(0),
      O => S(3)
    );
\y0_carry_i_20__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(24),
      I1 => \^q\(8),
      I2 => \y0_carry__1_i_6__0\(3),
      I3 => \^q\(16),
      I4 => \y0_carry__1_i_6__0\(4),
      I5 => \^q\(0),
      O => \y0_carry_i_20__0_n_0\
    );
\y0_carry_i_2__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4\(2),
      I1 => \y0_carry_i_6__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(0),
      I3 => \y0_carry_i_7__0_n_0\,
      I4 => \y0_carry__1_1\(0),
      O => S(2)
    );
\y0_carry_i_3__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4\(1),
      I1 => \y0_carry_i_7__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(0),
      I3 => \y0_carry_i_8__0_n_0\,
      I4 => \y0_carry__1_1\(0),
      O => S(1)
    );
\y0_carry_i_4__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"656A9A95"
    )
        port map (
      I0 => \y0_carry__4\(0),
      I1 => \y0_carry_i_8__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(0),
      I3 => \y0_carry_i_9__0_n_0\,
      I4 => \y0_carry__1_1\(0),
      O => S(0)
    );
\y0_carry_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q_reg[18]_0\,
      I1 => \y0_carry_i_11__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(1),
      I3 => \y0_carry_i_12__0_n_0\,
      I4 => \y0_carry__1_i_6__0\(2),
      I5 => \y0_carry_i_13__0_n_0\,
      O => \y0_carry_i_5__0_n_0\
    );
\y0_carry_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry_i_14__0_n_0\,
      I1 => \y0_carry_i_15__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(1),
      I3 => \y0_carry_i_16__0_n_0\,
      I4 => \y0_carry__1_i_6__0\(2),
      I5 => \y0_carry_i_17__0_n_0\,
      O => \y0_carry_i_6__0_n_0\
    );
\y0_carry_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry_i_12__0_n_0\,
      I1 => \y0_carry_i_13__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(1),
      I3 => \y0_carry_i_11__0_n_0\,
      I4 => \y0_carry__1_i_6__0\(2),
      I5 => \y0_carry_i_18__0_n_0\,
      O => \y0_carry_i_7__0_n_0\
    );
\y0_carry_i_8__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry_i_16__0_n_0\,
      I1 => \y0_carry_i_17__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(1),
      I3 => \y0_carry_i_15__0_n_0\,
      I4 => \y0_carry__1_i_6__0\(2),
      I5 => \y0_carry_i_19__0_n_0\,
      O => \y0_carry_i_8__0_n_0\
    );
\y0_carry_i_9__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \y0_carry_i_11__0_n_0\,
      I1 => \y0_carry_i_18__0_n_0\,
      I2 => \y0_carry__1_i_6__0\(1),
      I3 => \y0_carry_i_13__0_n_0\,
      I4 => \y0_carry__1_i_6__0\(2),
      I5 => \y0_carry_i_20__0_n_0\,
      O => \y0_carry_i_9__0_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \reg__parameterized3\ is
  port (
    \q_reg[23]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 23 downto 0 );
    \q_reg[23]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[22]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \y0_carry__5\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \y0_carry__5_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    out_ROM : in STD_LOGIC_VECTOR ( 22 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 23 downto 0 );
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \reg__parameterized3\ : entity is "reg";
end \reg__parameterized3\;

architecture STRUCTURE of \reg__parameterized3\ is
  signal \^q\ : STD_LOGIC_VECTOR ( 23 downto 0 );
begin
  Q(23 downto 0) <= \^q\(23 downto 0);
\q_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(0),
      Q => \^q\(0)
    );
\q_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(10),
      Q => \^q\(10)
    );
\q_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(11),
      Q => \^q\(11)
    );
\q_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(12),
      Q => \^q\(12)
    );
\q_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(13),
      Q => \^q\(13)
    );
\q_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(14),
      Q => \^q\(14)
    );
\q_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(15),
      Q => \^q\(15)
    );
\q_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(16),
      Q => \^q\(16)
    );
\q_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(17),
      Q => \^q\(17)
    );
\q_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(18),
      Q => \^q\(18)
    );
\q_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(19),
      Q => \^q\(19)
    );
\q_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(1),
      Q => \^q\(1)
    );
\q_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(20),
      Q => \^q\(20)
    );
\q_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(21),
      Q => \^q\(21)
    );
\q_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(22),
      Q => \^q\(22)
    );
\q_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(23),
      Q => \^q\(23)
    );
\q_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(2),
      Q => \^q\(2)
    );
\q_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(3),
      Q => \^q\(3)
    );
\q_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(4),
      Q => \^q\(4)
    );
\q_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(5),
      Q => \^q\(5)
    );
\q_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(6),
      Q => \^q\(6)
    );
\q_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(7),
      Q => \^q\(7)
    );
\q_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(8),
      Q => \^q\(8)
    );
\q_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(9),
      Q => \^q\(9)
    );
\y0_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(7),
      I1 => \^q\(23),
      I2 => out_ROM(7),
      O => \q_reg[7]_0\(3)
    );
\y0_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(6),
      I1 => \^q\(23),
      I2 => out_ROM(6),
      O => \q_reg[7]_0\(2)
    );
\y0_carry__0_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^q\(23),
      I2 => out_ROM(5),
      O => \q_reg[7]_0\(1)
    );
\y0_carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^q\(23),
      I2 => out_ROM(4),
      O => \q_reg[7]_0\(0)
    );
\y0_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(11),
      I1 => \^q\(23),
      I2 => out_ROM(11),
      O => \q_reg[11]_0\(3)
    );
\y0_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(10),
      I1 => \^q\(23),
      I2 => out_ROM(10),
      O => \q_reg[11]_0\(2)
    );
\y0_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(9),
      I1 => \^q\(23),
      I2 => out_ROM(9),
      O => \q_reg[11]_0\(1)
    );
\y0_carry__1_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(8),
      I1 => \^q\(23),
      I2 => out_ROM(8),
      O => \q_reg[11]_0\(0)
    );
\y0_carry__2_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(15),
      I1 => \^q\(23),
      I2 => out_ROM(15),
      O => \q_reg[15]_0\(3)
    );
\y0_carry__2_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(14),
      I1 => \^q\(23),
      I2 => out_ROM(14),
      O => \q_reg[15]_0\(2)
    );
\y0_carry__2_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(13),
      I1 => \^q\(23),
      I2 => out_ROM(13),
      O => \q_reg[15]_0\(1)
    );
\y0_carry__2_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(12),
      I1 => \^q\(23),
      I2 => out_ROM(12),
      O => \q_reg[15]_0\(0)
    );
\y0_carry__3_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(19),
      I1 => \^q\(23),
      I2 => out_ROM(19),
      O => \q_reg[19]_0\(3)
    );
\y0_carry__3_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(18),
      I1 => \^q\(23),
      I2 => out_ROM(18),
      O => \q_reg[19]_0\(2)
    );
\y0_carry__3_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(17),
      I1 => \^q\(23),
      I2 => out_ROM(17),
      O => \q_reg[19]_0\(1)
    );
\y0_carry__3_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(16),
      I1 => \^q\(23),
      I2 => out_ROM(16),
      O => \q_reg[19]_0\(0)
    );
\y0_carry__4_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(22),
      I1 => \^q\(23),
      I2 => out_ROM(22),
      O => \q_reg[22]_0\(2)
    );
\y0_carry__4_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(21),
      I1 => \^q\(23),
      I2 => out_ROM(21),
      O => \q_reg[22]_0\(1)
    );
\y0_carry__4_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(20),
      I1 => \^q\(23),
      I2 => out_ROM(20),
      O => \q_reg[22]_0\(0)
    );
\y0_carry__5_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \^q\(23),
      I1 => \y0_carry__5\(0),
      I2 => \y0_carry__5_0\(0),
      O => \q_reg[23]_1\(0)
    );
y0_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(23),
      O => \q_reg[23]_0\
    );
y0_carry_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(23),
      I2 => out_ROM(3),
      O => S(3)
    );
y0_carry_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(23),
      I2 => out_ROM(2),
      O => S(2)
    );
y0_carry_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(23),
      I2 => out_ROM(1),
      O => S(1)
    );
\y0_carry_i_5__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(23),
      I2 => out_ROM(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity control_path is
  port (
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    done_OBUF : out STD_LOGIC;
    \q_reg[4]\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \q_reg[1]\ : out STD_LOGIC;
    \q_reg[2]\ : out STD_LOGIC;
    \q_reg[1]_0\ : out STD_LOGIC;
    \q_reg[1]_1\ : out STD_LOGIC;
    \q_reg[1]_2\ : out STD_LOGIC;
    \q_reg[2]_0\ : out STD_LOGIC;
    \q_reg[1]_3\ : out STD_LOGIC;
    \q_reg[1]_4\ : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    D : out STD_LOGIC_VECTOR ( 23 downto 0 );
    S : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[14]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[19]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[22]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[11]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[14]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[19]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[22]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    start_IBUF : in STD_LOGIC;
    \y0_carry__1_i_2__0\ : in STD_LOGIC;
    \y0_carry__1_i_2__0_0\ : in STD_LOGIC;
    \y0_carry__1_i_2__0_1\ : in STD_LOGIC;
    \y0_carry__2_i_1__0\ : in STD_LOGIC;
    \y0_carry__3_i_2__0\ : in STD_LOGIC;
    \y0_carry__4_i_2__0\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    \y0_carry__1_i_3__0\ : in STD_LOGIC;
    \y0_carry__1_i_3__0_0\ : in STD_LOGIC;
    \y0_carry__1_i_3__0_1\ : in STD_LOGIC;
    \y0_carry__2_i_3__0\ : in STD_LOGIC;
    \y0_carry__3_i_3__0\ : in STD_LOGIC;
    \y0_carry__1_i_2__1\ : in STD_LOGIC;
    \y0_carry__1_i_2__1_0\ : in STD_LOGIC;
    \y0_carry__1_i_2__1_1\ : in STD_LOGIC;
    \y0_carry__2_i_1__1\ : in STD_LOGIC;
    \y0_carry__3_i_2__1\ : in STD_LOGIC;
    \y0_carry__4_i_2__1\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    \y0_carry__1_i_3__1\ : in STD_LOGIC;
    \y0_carry__1_i_3__1_0\ : in STD_LOGIC;
    \y0_carry__1_i_3__1_1\ : in STD_LOGIC;
    \y0_carry__2_i_3__1\ : in STD_LOGIC;
    \y0_carry__3_i_3__1\ : in STD_LOGIC;
    angle_IBUF : in STD_LOGIC_VECTOR ( 23 downto 0 );
    O : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[3]\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \y0_carry__1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end control_path;

architecture STRUCTURE of control_path is
  signal \^e\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal fsm0_n_1 : STD_LOGIC;
  signal regr_n_6 : STD_LOGIC;
begin
  E(0) <= \^e\(0);
  Q(0) <= \^q\(0);
fsm0: entity work.fsm
     port map (
      AR(0) => AR(0),
      CLK => CLK,
      D(23 downto 0) => D(23 downto 0),
      E(0) => \^e\(0),
      \FSM_sequential_state_reg[0]_0\ => fsm0_n_1,
      \FSM_sequential_state_reg[0]_1\ => regr_n_6,
      O(3 downto 0) => O(3 downto 0),
      Q(0) => \^q\(0),
      angle_IBUF(23 downto 0) => angle_IBUF(23 downto 0),
      \q_reg[11]\(3 downto 0) => \q_reg[11]_0\(3 downto 0),
      \q_reg[15]\(3 downto 0) => \q_reg[15]\(3 downto 0),
      \q_reg[19]\(3 downto 0) => \q_reg[19]_1\(3 downto 0),
      \q_reg[3]\(3 downto 0) => \q_reg[3]\(3 downto 0),
      \q_reg[7]\(3 downto 0) => \q_reg[7]\(3 downto 0),
      start_IBUF => start_IBUF
    );
regr: entity work.reg
     port map (
      AR(0) => AR(0),
      CLK => CLK,
      E(0) => \^e\(0),
      Q(4 downto 0) => \q_reg[4]\(4 downto 0),
      S(1 downto 0) => S(1 downto 0),
      done_OBUF => done_OBUF,
      \q_reg[0]_0\(0) => \^q\(0),
      \q_reg[11]\(1 downto 0) => \q_reg[11]\(1 downto 0),
      \q_reg[14]\(2 downto 0) => \q_reg[14]\(2 downto 0),
      \q_reg[14]_0\(2 downto 0) => \q_reg[14]_0\(2 downto 0),
      \q_reg[19]\(1 downto 0) => \q_reg[19]\(1 downto 0),
      \q_reg[19]_0\(1 downto 0) => \q_reg[19]_0\(1 downto 0),
      \q_reg[1]_0\ => regr_n_6,
      \q_reg[1]_1\ => \q_reg[1]\,
      \q_reg[1]_2\ => \q_reg[1]_0\,
      \q_reg[1]_3\ => \q_reg[1]_1\,
      \q_reg[1]_4\ => \q_reg[1]_2\,
      \q_reg[1]_5\ => \q_reg[1]_3\,
      \q_reg[1]_6\ => \q_reg[1]_4\,
      \q_reg[22]\(2 downto 0) => \q_reg[22]\(2 downto 0),
      \q_reg[22]_0\(2 downto 0) => \q_reg[22]_0\(2 downto 0),
      \q_reg[2]_0\ => \q_reg[2]\,
      \q_reg[2]_1\ => \q_reg[2]_0\,
      \q_reg[4]_0\ => fsm0_n_1,
      start_IBUF => start_IBUF,
      \y0_carry__1\(0) => \y0_carry__1\(0),
      \y0_carry__1_i_2__0_0\ => \y0_carry__1_i_2__0\,
      \y0_carry__1_i_2__0_1\ => \y0_carry__1_i_2__0_0\,
      \y0_carry__1_i_2__0_2\ => \y0_carry__1_i_2__0_1\,
      \y0_carry__1_i_2__1_0\ => \y0_carry__1_i_2__1\,
      \y0_carry__1_i_2__1_1\ => \y0_carry__1_i_2__1_0\,
      \y0_carry__1_i_2__1_2\ => \y0_carry__1_i_2__1_1\,
      \y0_carry__1_i_3__0\ => \y0_carry__1_i_3__0\,
      \y0_carry__1_i_3__0_0\ => \y0_carry__1_i_3__0_0\,
      \y0_carry__1_i_3__0_1\ => \y0_carry__1_i_3__0_1\,
      \y0_carry__1_i_3__1\ => \y0_carry__1_i_3__1\,
      \y0_carry__1_i_3__1_0\ => \y0_carry__1_i_3__1_0\,
      \y0_carry__1_i_3__1_1\ => \y0_carry__1_i_3__1_1\,
      \y0_carry__2_i_1__0\ => \y0_carry__2_i_1__0\,
      \y0_carry__2_i_1__1\ => \y0_carry__2_i_1__1\,
      \y0_carry__2_i_3__0_0\ => \y0_carry__2_i_3__0\,
      \y0_carry__2_i_3__1_0\ => \y0_carry__2_i_3__1\,
      \y0_carry__3_i_2__0_0\ => \y0_carry__3_i_2__0\,
      \y0_carry__3_i_2__1_0\ => \y0_carry__3_i_2__1\,
      \y0_carry__3_i_3__0\ => \y0_carry__3_i_3__0\,
      \y0_carry__3_i_3__1\ => \y0_carry__3_i_3__1\,
      \y0_carry__4_i_2__0_0\(13 downto 0) => \y0_carry__4_i_2__0\(13 downto 0),
      \y0_carry__4_i_2__1_0\(13 downto 0) => \y0_carry__4_i_2__1\(13 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity data_path is
  port (
    \q_reg[3]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    O : out STD_LOGIC_VECTOR ( 3 downto 0 );
    sin_OBUF : out STD_LOGIC_VECTOR ( 24 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[24]\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    cos_OBUF : out STD_LOGIC_VECTOR ( 24 downto 0 );
    \q_reg[24]_0\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \q_reg[23]\ : out STD_LOGIC;
    \q_reg[19]_0\ : out STD_LOGIC;
    \q_reg[21]\ : out STD_LOGIC;
    \q_reg[23]_0\ : out STD_LOGIC;
    \q_reg[21]_0\ : out STD_LOGIC;
    \q_reg[22]\ : out STD_LOGIC;
    \q_reg[18]\ : out STD_LOGIC;
    \q_reg[20]\ : out STD_LOGIC;
    \q_reg[22]_0\ : out STD_LOGIC;
    \q_reg[20]_0\ : out STD_LOGIC;
    \q_reg[23]_1\ : out STD_LOGIC;
    \q_reg[19]_1\ : out STD_LOGIC;
    \q_reg[21]_1\ : out STD_LOGIC;
    \q_reg[23]_2\ : out STD_LOGIC;
    \q_reg[21]_2\ : out STD_LOGIC;
    \q_reg[22]_1\ : out STD_LOGIC;
    \q_reg[18]_0\ : out STD_LOGIC;
    \q_reg[20]_1\ : out STD_LOGIC;
    \q_reg[22]_2\ : out STD_LOGIC;
    \q_reg[20]_2\ : out STD_LOGIC;
    CLK : in STD_LOGIC;
    data_reg : in STD_LOGIC_VECTOR ( 4 downto 0 );
    S : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[15]_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[19]_2\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[23]_3\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[11]_0\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[15]_1\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \q_reg[19]_3\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \q_reg[23]_4\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    start_IBUF : in STD_LOGIC;
    \q_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \y0_carry__1\ : in STD_LOGIC;
    \y0_carry__2\ : in STD_LOGIC;
    \y0_carry__3\ : in STD_LOGIC;
    \y0_carry__4\ : in STD_LOGIC;
    \y0_carry__1_0\ : in STD_LOGIC;
    \y0_carry__2_0\ : in STD_LOGIC;
    \y0_carry__3_0\ : in STD_LOGIC;
    \y0_carry__4_0\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 23 downto 0 )
  );
end data_path;

architecture STRUCTURE of data_path is
  signal Mux_Y_n_25 : STD_LOGIC;
  signal Mux_Y_n_26 : STD_LOGIC;
  signal Mux_Y_n_27 : STD_LOGIC;
  signal Mux_Y_n_28 : STD_LOGIC;
  signal Mux_Y_n_29 : STD_LOGIC;
  signal Mux_Y_n_30 : STD_LOGIC;
  signal Mux_Y_n_31 : STD_LOGIC;
  signal Mux_Y_n_32 : STD_LOGIC;
  signal Mux_Y_n_33 : STD_LOGIC;
  signal Mux_Y_n_34 : STD_LOGIC;
  signal Mux_Y_n_35 : STD_LOGIC;
  signal Mux_Y_n_36 : STD_LOGIC;
  signal Mux_Y_n_37 : STD_LOGIC;
  signal Mux_Y_n_38 : STD_LOGIC;
  signal Mux_Y_n_39 : STD_LOGIC;
  signal Mux_Y_n_40 : STD_LOGIC;
  signal Mux_Y_n_41 : STD_LOGIC;
  signal Mux_Y_n_42 : STD_LOGIC;
  signal Mux_Y_n_43 : STD_LOGIC;
  signal Mux_Y_n_44 : STD_LOGIC;
  signal Mux_Y_n_45 : STD_LOGIC;
  signal Mux_Y_n_46 : STD_LOGIC;
  signal Mux_Y_n_47 : STD_LOGIC;
  signal Mux_Y_n_48 : STD_LOGIC;
  signal Mux_Y_n_49 : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal ROM_1_n_23 : STD_LOGIC;
  signal out_ROM : STD_LOGIC_VECTOR ( 22 downto 0 );
  signal p_0_in : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal \^q_reg[24]\ : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \^q_reg[24]_0\ : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal reg_X_n_0 : STD_LOGIC;
  signal reg_X_n_13 : STD_LOGIC;
  signal reg_X_n_19 : STD_LOGIC;
  signal reg_X_n_20 : STD_LOGIC;
  signal reg_X_n_21 : STD_LOGIC;
  signal reg_X_n_22 : STD_LOGIC;
  signal reg_X_n_23 : STD_LOGIC;
  signal reg_X_n_24 : STD_LOGIC;
  signal reg_X_n_25 : STD_LOGIC;
  signal reg_X_n_26 : STD_LOGIC;
  signal reg_X_n_27 : STD_LOGIC;
  signal reg_X_n_28 : STD_LOGIC;
  signal reg_X_n_29 : STD_LOGIC;
  signal reg_X_n_35 : STD_LOGIC;
  signal reg_X_n_38 : STD_LOGIC;
  signal reg_X_n_39 : STD_LOGIC;
  signal reg_X_n_40 : STD_LOGIC;
  signal reg_X_n_41 : STD_LOGIC;
  signal reg_X_n_42 : STD_LOGIC;
  signal reg_X_n_43 : STD_LOGIC;
  signal reg_X_n_44 : STD_LOGIC;
  signal reg_X_n_45 : STD_LOGIC;
  signal reg_X_n_46 : STD_LOGIC;
  signal reg_X_n_47 : STD_LOGIC;
  signal reg_X_n_48 : STD_LOGIC;
  signal reg_X_n_49 : STD_LOGIC;
  signal reg_X_n_50 : STD_LOGIC;
  signal reg_X_n_51 : STD_LOGIC;
  signal reg_X_n_52 : STD_LOGIC;
  signal reg_Y_n_0 : STD_LOGIC;
  signal reg_Y_n_13 : STD_LOGIC;
  signal reg_Y_n_19 : STD_LOGIC;
  signal reg_Y_n_20 : STD_LOGIC;
  signal reg_Y_n_21 : STD_LOGIC;
  signal reg_Y_n_22 : STD_LOGIC;
  signal reg_Y_n_23 : STD_LOGIC;
  signal reg_Y_n_24 : STD_LOGIC;
  signal reg_Y_n_25 : STD_LOGIC;
  signal reg_Y_n_26 : STD_LOGIC;
  signal reg_Y_n_27 : STD_LOGIC;
  signal reg_Y_n_28 : STD_LOGIC;
  signal reg_Y_n_29 : STD_LOGIC;
  signal reg_Y_n_35 : STD_LOGIC;
  signal reg_Y_n_38 : STD_LOGIC;
  signal reg_Y_n_39 : STD_LOGIC;
  signal reg_Y_n_40 : STD_LOGIC;
  signal reg_Y_n_41 : STD_LOGIC;
  signal reg_Y_n_42 : STD_LOGIC;
  signal reg_Y_n_43 : STD_LOGIC;
  signal reg_Y_n_44 : STD_LOGIC;
  signal reg_Y_n_45 : STD_LOGIC;
  signal reg_Y_n_46 : STD_LOGIC;
  signal reg_Y_n_47 : STD_LOGIC;
  signal reg_Y_n_48 : STD_LOGIC;
  signal reg_Y_n_49 : STD_LOGIC;
  signal reg_Y_n_50 : STD_LOGIC;
  signal reg_Y_n_51 : STD_LOGIC;
  signal reg_Z_n_0 : STD_LOGIC;
  signal reg_Z_n_10 : STD_LOGIC;
  signal reg_Z_n_11 : STD_LOGIC;
  signal reg_Z_n_12 : STD_LOGIC;
  signal reg_Z_n_13 : STD_LOGIC;
  signal reg_Z_n_14 : STD_LOGIC;
  signal reg_Z_n_15 : STD_LOGIC;
  signal reg_Z_n_16 : STD_LOGIC;
  signal reg_Z_n_17 : STD_LOGIC;
  signal reg_Z_n_18 : STD_LOGIC;
  signal reg_Z_n_19 : STD_LOGIC;
  signal reg_Z_n_2 : STD_LOGIC;
  signal reg_Z_n_20 : STD_LOGIC;
  signal reg_Z_n_21 : STD_LOGIC;
  signal reg_Z_n_22 : STD_LOGIC;
  signal reg_Z_n_23 : STD_LOGIC;
  signal reg_Z_n_24 : STD_LOGIC;
  signal reg_Z_n_25 : STD_LOGIC;
  signal reg_Z_n_26 : STD_LOGIC;
  signal reg_Z_n_27 : STD_LOGIC;
  signal reg_Z_n_28 : STD_LOGIC;
  signal reg_Z_n_29 : STD_LOGIC;
  signal reg_Z_n_3 : STD_LOGIC;
  signal reg_Z_n_30 : STD_LOGIC;
  signal reg_Z_n_31 : STD_LOGIC;
  signal reg_Z_n_32 : STD_LOGIC;
  signal reg_Z_n_33 : STD_LOGIC;
  signal reg_Z_n_34 : STD_LOGIC;
  signal reg_Z_n_35 : STD_LOGIC;
  signal reg_Z_n_36 : STD_LOGIC;
  signal reg_Z_n_37 : STD_LOGIC;
  signal reg_Z_n_38 : STD_LOGIC;
  signal reg_Z_n_39 : STD_LOGIC;
  signal reg_Z_n_4 : STD_LOGIC;
  signal reg_Z_n_40 : STD_LOGIC;
  signal reg_Z_n_41 : STD_LOGIC;
  signal reg_Z_n_42 : STD_LOGIC;
  signal reg_Z_n_43 : STD_LOGIC;
  signal reg_Z_n_44 : STD_LOGIC;
  signal reg_Z_n_45 : STD_LOGIC;
  signal reg_Z_n_46 : STD_LOGIC;
  signal reg_Z_n_47 : STD_LOGIC;
  signal reg_Z_n_48 : STD_LOGIC;
  signal reg_Z_n_5 : STD_LOGIC;
  signal reg_Z_n_6 : STD_LOGIC;
  signal reg_Z_n_7 : STD_LOGIC;
  signal reg_Z_n_8 : STD_LOGIC;
  signal reg_Z_n_9 : STD_LOGIC;
begin
  Q(0) <= \^q\(0);
  \q_reg[24]\(13 downto 0) <= \^q_reg[24]\(13 downto 0);
  \q_reg[24]_0\(13 downto 0) <= \^q_reg[24]_0\(13 downto 0);
Mux_X: entity work.Mux_2_1
     port map (
      D(24 downto 0) => p_0_in(24 downto 0),
      Q(23 downto 16) => \^q_reg[24]_0\(12 downto 5),
      Q(15) => reg_X_n_13,
      Q(14 downto 10) => \^q_reg[24]_0\(4 downto 0),
      Q(9) => reg_X_n_19,
      Q(8) => reg_X_n_20,
      Q(7) => reg_X_n_21,
      Q(6) => reg_X_n_22,
      Q(5) => reg_X_n_23,
      Q(4) => reg_X_n_24,
      Q(3) => reg_X_n_25,
      Q(2) => reg_X_n_26,
      Q(1) => reg_X_n_27,
      Q(0) => reg_X_n_28,
      S(3) => reg_Y_n_42,
      S(2) => reg_Y_n_43,
      S(1) => reg_Y_n_44,
      S(0) => reg_Y_n_45,
      cos_OBUF(24 downto 0) => cos_OBUF(24 downto 0),
      \q_reg[0]\(0) => \q_reg[0]\(0),
      \q_reg[11]\(3 downto 2) => \q_reg[11]_0\(1 downto 0),
      \q_reg[11]\(1) => reg_X_n_49,
      \q_reg[11]\(0) => reg_Y_n_50,
      \q_reg[15]\(3) => reg_X_n_50,
      \q_reg[15]\(2 downto 0) => \q_reg[15]_1\(2 downto 0),
      \q_reg[19]\(3 downto 2) => \q_reg[19]_3\(1 downto 0),
      \q_reg[19]\(1) => reg_X_n_51,
      \q_reg[19]\(0) => reg_Y_n_51,
      \q_reg[23]\(3) => reg_X_n_52,
      \q_reg[23]\(2 downto 0) => \q_reg[23]_4\(2 downto 0),
      \q_reg[24]\(0) => reg_X_n_38,
      \q_reg[3]\ => reg_Z_n_0,
      \q_reg[7]\(3) => reg_Y_n_46,
      \q_reg[7]\(2) => reg_Y_n_47,
      \q_reg[7]\(1) => reg_Y_n_48,
      \q_reg[7]\(0) => reg_Y_n_49,
      start_IBUF => start_IBUF
    );
Mux_Y: entity work.Mux_2_1_0
     port map (
      D(24) => Mux_Y_n_25,
      D(23) => Mux_Y_n_26,
      D(22) => Mux_Y_n_27,
      D(21) => Mux_Y_n_28,
      D(20) => Mux_Y_n_29,
      D(19) => Mux_Y_n_30,
      D(18) => Mux_Y_n_31,
      D(17) => Mux_Y_n_32,
      D(16) => Mux_Y_n_33,
      D(15) => Mux_Y_n_34,
      D(14) => Mux_Y_n_35,
      D(13) => Mux_Y_n_36,
      D(12) => Mux_Y_n_37,
      D(11) => Mux_Y_n_38,
      D(10) => Mux_Y_n_39,
      D(9) => Mux_Y_n_40,
      D(8) => Mux_Y_n_41,
      D(7) => Mux_Y_n_42,
      D(6) => Mux_Y_n_43,
      D(5) => Mux_Y_n_44,
      D(4) => Mux_Y_n_45,
      D(3) => Mux_Y_n_46,
      D(2) => Mux_Y_n_47,
      D(1) => Mux_Y_n_48,
      D(0) => Mux_Y_n_49,
      Q(0) => \^q\(0),
      S(3) => reg_X_n_39,
      S(2) => reg_X_n_40,
      S(1) => reg_X_n_41,
      S(0) => reg_X_n_42,
      \q_reg[0]\(0) => \q_reg[0]\(0),
      \q_reg[11]\(3 downto 2) => S(1 downto 0),
      \q_reg[11]\(1) => reg_Y_n_38,
      \q_reg[11]\(0) => reg_X_n_47,
      \q_reg[15]\(3) => reg_Y_n_39,
      \q_reg[15]\(2 downto 0) => \q_reg[15]_0\(2 downto 0),
      \q_reg[19]\(3 downto 2) => \q_reg[19]_2\(1 downto 0),
      \q_reg[19]\(1) => reg_Y_n_40,
      \q_reg[19]\(0) => reg_X_n_48,
      \q_reg[23]\(23 downto 16) => \^q_reg[24]\(12 downto 5),
      \q_reg[23]\(15) => reg_Y_n_13,
      \q_reg[23]\(14 downto 10) => \^q_reg[24]\(4 downto 0),
      \q_reg[23]\(9) => reg_Y_n_19,
      \q_reg[23]\(8) => reg_Y_n_20,
      \q_reg[23]\(7) => reg_Y_n_21,
      \q_reg[23]\(6) => reg_Y_n_22,
      \q_reg[23]\(5) => reg_Y_n_23,
      \q_reg[23]\(4) => reg_Y_n_24,
      \q_reg[23]\(3) => reg_Y_n_25,
      \q_reg[23]\(2) => reg_Y_n_26,
      \q_reg[23]\(1) => reg_Y_n_27,
      \q_reg[23]\(0) => reg_Y_n_28,
      \q_reg[23]_0\(3) => reg_Y_n_41,
      \q_reg[23]_0\(2 downto 0) => \q_reg[23]_3\(2 downto 0),
      \q_reg[24]\(0) => reg_Z_n_25,
      \q_reg[7]\(3) => reg_X_n_43,
      \q_reg[7]\(2) => reg_X_n_44,
      \q_reg[7]\(1) => reg_X_n_45,
      \q_reg[7]\(0) => reg_X_n_46,
      sin_OBUF(24 downto 0) => sin_OBUF(24 downto 0),
      start_IBUF => start_IBUF
    );
Mux_Z: entity work.\Mux_2_1__parameterized2\
     port map (
      O(3 downto 0) => O(3 downto 0),
      Q(22) => reg_Z_n_2,
      Q(21) => reg_Z_n_3,
      Q(20) => reg_Z_n_4,
      Q(19) => reg_Z_n_5,
      Q(18) => reg_Z_n_6,
      Q(17) => reg_Z_n_7,
      Q(16) => reg_Z_n_8,
      Q(15) => reg_Z_n_9,
      Q(14) => reg_Z_n_10,
      Q(13) => reg_Z_n_11,
      Q(12) => reg_Z_n_12,
      Q(11) => reg_Z_n_13,
      Q(10) => reg_Z_n_14,
      Q(9) => reg_Z_n_15,
      Q(8) => reg_Z_n_16,
      Q(7) => reg_Z_n_17,
      Q(6) => reg_Z_n_18,
      Q(5) => reg_Z_n_19,
      Q(4) => reg_Z_n_20,
      Q(3) => reg_Z_n_21,
      Q(2) => reg_Z_n_22,
      Q(1) => reg_Z_n_23,
      Q(0) => reg_Z_n_24,
      S(3) => reg_Z_n_26,
      S(2) => reg_Z_n_27,
      S(1) => reg_Z_n_28,
      S(0) => reg_Z_n_29,
      \q_reg[11]\(3 downto 0) => \q_reg[11]\(3 downto 0),
      \q_reg[11]_0\(3) => reg_Z_n_34,
      \q_reg[11]_0\(2) => reg_Z_n_35,
      \q_reg[11]_0\(1) => reg_Z_n_36,
      \q_reg[11]_0\(0) => reg_Z_n_37,
      \q_reg[15]\(3 downto 0) => \q_reg[15]\(3 downto 0),
      \q_reg[15]_0\(3) => reg_Z_n_38,
      \q_reg[15]_0\(2) => reg_Z_n_39,
      \q_reg[15]_0\(1) => reg_Z_n_40,
      \q_reg[15]_0\(0) => reg_Z_n_41,
      \q_reg[19]\(3 downto 0) => \q_reg[19]\(3 downto 0),
      \q_reg[19]_0\(3) => reg_Z_n_42,
      \q_reg[19]_0\(2) => reg_Z_n_43,
      \q_reg[19]_0\(1) => reg_Z_n_44,
      \q_reg[19]_0\(0) => reg_Z_n_45,
      \q_reg[23]\(3) => ROM_1_n_23,
      \q_reg[23]\(2) => reg_Z_n_46,
      \q_reg[23]\(1) => reg_Z_n_47,
      \q_reg[23]\(0) => reg_Z_n_48,
      \q_reg[3]\(3 downto 0) => \q_reg[3]\(3 downto 0),
      \q_reg[3]_0\ => reg_Z_n_0,
      \q_reg[7]\(3 downto 0) => \q_reg[7]\(3 downto 0),
      \q_reg[7]_0\(3) => reg_Z_n_30,
      \q_reg[7]_0\(2) => reg_Z_n_31,
      \q_reg[7]_0\(1) => reg_Z_n_32,
      \q_reg[7]_0\(0) => reg_Z_n_33
    );
ROM_1: entity work.ROM
     port map (
      CLK => CLK,
      data_reg_0(0) => ROM_1_n_23,
      data_reg_1(4 downto 0) => data_reg(4 downto 0),
      out_ROM(22 downto 0) => out_ROM(22 downto 0)
    );
reg_X: entity work.\reg__parameterized1\
     port map (
      AR(0) => AR(0),
      CLK => CLK,
      D(24 downto 0) => p_0_in(24 downto 0),
      E(0) => E(0),
      Q(24 downto 16) => \^q_reg[24]_0\(13 downto 5),
      Q(15) => reg_X_n_13,
      Q(14 downto 10) => \^q_reg[24]_0\(4 downto 0),
      Q(9) => reg_X_n_19,
      Q(8) => reg_X_n_20,
      Q(7) => reg_X_n_21,
      Q(6) => reg_X_n_22,
      Q(5) => reg_X_n_23,
      Q(4) => reg_X_n_24,
      Q(3) => reg_X_n_25,
      Q(2) => reg_X_n_26,
      Q(1) => reg_X_n_27,
      Q(0) => reg_X_n_28,
      S(3) => reg_X_n_39,
      S(2) => reg_X_n_40,
      S(1) => reg_X_n_41,
      S(0) => reg_X_n_42,
      \q_reg[15]_0\(0) => reg_X_n_50,
      \q_reg[16]_0\(0) => reg_X_n_48,
      \q_reg[17]_0\(0) => reg_X_n_51,
      \q_reg[18]_0\ => \q_reg[18]\,
      \q_reg[19]_0\ => \q_reg[19]_0\,
      \q_reg[1]_0\ => reg_X_n_0,
      \q_reg[1]_1\ => reg_X_n_29,
      \q_reg[1]_2\ => reg_X_n_35,
      \q_reg[20]_0\ => \q_reg[20]\,
      \q_reg[20]_1\ => \q_reg[20]_0\,
      \q_reg[21]_0\ => \q_reg[21]\,
      \q_reg[21]_1\ => \q_reg[21]_0\,
      \q_reg[22]_0\ => \q_reg[22]\,
      \q_reg[22]_1\ => \q_reg[22]_0\,
      \q_reg[23]_0\ => \q_reg[23]\,
      \q_reg[23]_1\ => \q_reg[23]_0\,
      \q_reg[23]_2\(0) => reg_X_n_52,
      \q_reg[24]_0\(0) => reg_X_n_38,
      \q_reg[7]_0\(3) => reg_X_n_43,
      \q_reg[7]_0\(2) => reg_X_n_44,
      \q_reg[7]_0\(1) => reg_X_n_45,
      \q_reg[7]_0\(0) => reg_X_n_46,
      \q_reg[8]_0\(0) => reg_X_n_47,
      \q_reg[9]_0\(0) => reg_X_n_49,
      y0_carry(0) => \^q\(0),
      \y0_carry__1\ => \y0_carry__1_0\,
      \y0_carry__1_0\ => reg_Y_n_0,
      \y0_carry__1_i_6\(4 downto 0) => data_reg(4 downto 0),
      \y0_carry__2\ => reg_Y_n_35,
      \y0_carry__2_0\ => \y0_carry__2_0\,
      \y0_carry__3\ => \y0_carry__3_0\,
      \y0_carry__3_0\ => reg_Y_n_29,
      \y0_carry__4\(10) => \^q_reg[24]\(13),
      \y0_carry__4\(9) => \^q_reg[24]\(5),
      \y0_carry__4\(8) => reg_Y_n_20,
      \y0_carry__4\(7) => reg_Y_n_21,
      \y0_carry__4\(6) => reg_Y_n_22,
      \y0_carry__4\(5) => reg_Y_n_23,
      \y0_carry__4\(4) => reg_Y_n_24,
      \y0_carry__4\(3) => reg_Y_n_25,
      \y0_carry__4\(2) => reg_Y_n_26,
      \y0_carry__4\(1) => reg_Y_n_27,
      \y0_carry__4\(0) => reg_Y_n_28,
      \y0_carry__4_0\ => \y0_carry__4_0\
    );
reg_Y: entity work.\reg__parameterized1_1\
     port map (
      AR(0) => AR(0),
      CLK => CLK,
      D(24) => Mux_Y_n_25,
      D(23) => Mux_Y_n_26,
      D(22) => Mux_Y_n_27,
      D(21) => Mux_Y_n_28,
      D(20) => Mux_Y_n_29,
      D(19) => Mux_Y_n_30,
      D(18) => Mux_Y_n_31,
      D(17) => Mux_Y_n_32,
      D(16) => Mux_Y_n_33,
      D(15) => Mux_Y_n_34,
      D(14) => Mux_Y_n_35,
      D(13) => Mux_Y_n_36,
      D(12) => Mux_Y_n_37,
      D(11) => Mux_Y_n_38,
      D(10) => Mux_Y_n_39,
      D(9) => Mux_Y_n_40,
      D(8) => Mux_Y_n_41,
      D(7) => Mux_Y_n_42,
      D(6) => Mux_Y_n_43,
      D(5) => Mux_Y_n_44,
      D(4) => Mux_Y_n_45,
      D(3) => Mux_Y_n_46,
      D(2) => Mux_Y_n_47,
      D(1) => Mux_Y_n_48,
      D(0) => Mux_Y_n_49,
      E(0) => E(0),
      Q(24 downto 16) => \^q_reg[24]\(13 downto 5),
      Q(15) => reg_Y_n_13,
      Q(14 downto 10) => \^q_reg[24]\(4 downto 0),
      Q(9) => reg_Y_n_19,
      Q(8) => reg_Y_n_20,
      Q(7) => reg_Y_n_21,
      Q(6) => reg_Y_n_22,
      Q(5) => reg_Y_n_23,
      Q(4) => reg_Y_n_24,
      Q(3) => reg_Y_n_25,
      Q(2) => reg_Y_n_26,
      Q(1) => reg_Y_n_27,
      Q(0) => reg_Y_n_28,
      S(3) => reg_Y_n_42,
      S(2) => reg_Y_n_43,
      S(1) => reg_Y_n_44,
      S(0) => reg_Y_n_45,
      \q_reg[15]_0\(0) => reg_Y_n_39,
      \q_reg[16]_0\(0) => reg_Y_n_51,
      \q_reg[17]_0\(0) => reg_Y_n_40,
      \q_reg[18]_0\ => \q_reg[18]_0\,
      \q_reg[19]_0\ => \q_reg[19]_1\,
      \q_reg[1]_0\ => reg_Y_n_0,
      \q_reg[1]_1\ => reg_Y_n_29,
      \q_reg[1]_2\ => reg_Y_n_35,
      \q_reg[20]_0\ => \q_reg[20]_1\,
      \q_reg[20]_1\ => \q_reg[20]_2\,
      \q_reg[21]_0\ => \q_reg[21]_1\,
      \q_reg[21]_1\ => \q_reg[21]_2\,
      \q_reg[22]_0\ => \q_reg[22]_1\,
      \q_reg[22]_1\ => \q_reg[22]_2\,
      \q_reg[23]_0\ => \q_reg[23]_1\,
      \q_reg[23]_1\ => \q_reg[23]_2\,
      \q_reg[23]_2\(0) => reg_Y_n_41,
      \q_reg[7]_0\(3) => reg_Y_n_46,
      \q_reg[7]_0\(2) => reg_Y_n_47,
      \q_reg[7]_0\(1) => reg_Y_n_48,
      \q_reg[7]_0\(0) => reg_Y_n_49,
      \q_reg[8]_0\(0) => reg_Y_n_50,
      \q_reg[9]_0\(0) => reg_Y_n_38,
      \y0_carry__1\ => \y0_carry__1\,
      \y0_carry__1_0\ => reg_X_n_0,
      \y0_carry__1_1\(0) => \^q\(0),
      \y0_carry__1_i_6__0\(4 downto 0) => data_reg(4 downto 0),
      \y0_carry__2\ => reg_X_n_35,
      \y0_carry__2_0\ => \y0_carry__2\,
      \y0_carry__3\ => \y0_carry__3\,
      \y0_carry__3_0\ => reg_X_n_29,
      \y0_carry__4\(10) => \^q_reg[24]_0\(13),
      \y0_carry__4\(9) => \^q_reg[24]_0\(5),
      \y0_carry__4\(8) => reg_X_n_20,
      \y0_carry__4\(7) => reg_X_n_21,
      \y0_carry__4\(6) => reg_X_n_22,
      \y0_carry__4\(5) => reg_X_n_23,
      \y0_carry__4\(4) => reg_X_n_24,
      \y0_carry__4\(3) => reg_X_n_25,
      \y0_carry__4\(2) => reg_X_n_26,
      \y0_carry__4\(1) => reg_X_n_27,
      \y0_carry__4\(0) => reg_X_n_28,
      \y0_carry__4_0\ => \y0_carry__4\
    );
reg_Z: entity work.\reg__parameterized3\
     port map (
      AR(0) => AR(0),
      CLK => CLK,
      D(23 downto 0) => D(23 downto 0),
      E(0) => E(0),
      Q(23) => \^q\(0),
      Q(22) => reg_Z_n_2,
      Q(21) => reg_Z_n_3,
      Q(20) => reg_Z_n_4,
      Q(19) => reg_Z_n_5,
      Q(18) => reg_Z_n_6,
      Q(17) => reg_Z_n_7,
      Q(16) => reg_Z_n_8,
      Q(15) => reg_Z_n_9,
      Q(14) => reg_Z_n_10,
      Q(13) => reg_Z_n_11,
      Q(12) => reg_Z_n_12,
      Q(11) => reg_Z_n_13,
      Q(10) => reg_Z_n_14,
      Q(9) => reg_Z_n_15,
      Q(8) => reg_Z_n_16,
      Q(7) => reg_Z_n_17,
      Q(6) => reg_Z_n_18,
      Q(5) => reg_Z_n_19,
      Q(4) => reg_Z_n_20,
      Q(3) => reg_Z_n_21,
      Q(2) => reg_Z_n_22,
      Q(1) => reg_Z_n_23,
      Q(0) => reg_Z_n_24,
      S(3) => reg_Z_n_26,
      S(2) => reg_Z_n_27,
      S(1) => reg_Z_n_28,
      S(0) => reg_Z_n_29,
      out_ROM(22 downto 0) => out_ROM(22 downto 0),
      \q_reg[11]_0\(3) => reg_Z_n_34,
      \q_reg[11]_0\(2) => reg_Z_n_35,
      \q_reg[11]_0\(1) => reg_Z_n_36,
      \q_reg[11]_0\(0) => reg_Z_n_37,
      \q_reg[15]_0\(3) => reg_Z_n_38,
      \q_reg[15]_0\(2) => reg_Z_n_39,
      \q_reg[15]_0\(1) => reg_Z_n_40,
      \q_reg[15]_0\(0) => reg_Z_n_41,
      \q_reg[19]_0\(3) => reg_Z_n_42,
      \q_reg[19]_0\(2) => reg_Z_n_43,
      \q_reg[19]_0\(1) => reg_Z_n_44,
      \q_reg[19]_0\(0) => reg_Z_n_45,
      \q_reg[22]_0\(2) => reg_Z_n_46,
      \q_reg[22]_0\(1) => reg_Z_n_47,
      \q_reg[22]_0\(0) => reg_Z_n_48,
      \q_reg[23]_0\ => reg_Z_n_0,
      \q_reg[23]_1\(0) => reg_Z_n_25,
      \q_reg[7]_0\(3) => reg_Z_n_30,
      \q_reg[7]_0\(2) => reg_Z_n_31,
      \q_reg[7]_0\(1) => reg_Z_n_32,
      \q_reg[7]_0\(0) => reg_Z_n_33,
      \y0_carry__5\(0) => \^q_reg[24]\(13),
      \y0_carry__5_0\(0) => \^q_reg[24]_0\(13)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity cordic_rotation is
  port (
    angle : in STD_LOGIC_VECTOR ( 23 downto 0 );
    rst : in STD_LOGIC;
    start : in STD_LOGIC;
    clk : in STD_LOGIC;
    cos : out STD_LOGIC_VECTOR ( 24 downto 0 );
    sin : out STD_LOGIC_VECTOR ( 24 downto 0 );
    done : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of cordic_rotation : entity is true;
end cordic_rotation;

architecture STRUCTURE of cordic_rotation is
  signal addr : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal angle_IBUF : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal control_path0_n_10 : STD_LOGIC;
  signal control_path0_n_11 : STD_LOGIC;
  signal control_path0_n_12 : STD_LOGIC;
  signal control_path0_n_13 : STD_LOGIC;
  signal control_path0_n_14 : STD_LOGIC;
  signal control_path0_n_16 : STD_LOGIC;
  signal control_path0_n_17 : STD_LOGIC;
  signal control_path0_n_18 : STD_LOGIC;
  signal control_path0_n_19 : STD_LOGIC;
  signal control_path0_n_20 : STD_LOGIC;
  signal control_path0_n_21 : STD_LOGIC;
  signal control_path0_n_22 : STD_LOGIC;
  signal control_path0_n_23 : STD_LOGIC;
  signal control_path0_n_24 : STD_LOGIC;
  signal control_path0_n_25 : STD_LOGIC;
  signal control_path0_n_26 : STD_LOGIC;
  signal control_path0_n_27 : STD_LOGIC;
  signal control_path0_n_28 : STD_LOGIC;
  signal control_path0_n_29 : STD_LOGIC;
  signal control_path0_n_30 : STD_LOGIC;
  signal control_path0_n_31 : STD_LOGIC;
  signal control_path0_n_32 : STD_LOGIC;
  signal control_path0_n_33 : STD_LOGIC;
  signal control_path0_n_34 : STD_LOGIC;
  signal control_path0_n_35 : STD_LOGIC;
  signal control_path0_n_36 : STD_LOGIC;
  signal control_path0_n_37 : STD_LOGIC;
  signal control_path0_n_38 : STD_LOGIC;
  signal control_path0_n_39 : STD_LOGIC;
  signal control_path0_n_40 : STD_LOGIC;
  signal control_path0_n_41 : STD_LOGIC;
  signal control_path0_n_42 : STD_LOGIC;
  signal control_path0_n_43 : STD_LOGIC;
  signal control_path0_n_44 : STD_LOGIC;
  signal control_path0_n_45 : STD_LOGIC;
  signal control_path0_n_46 : STD_LOGIC;
  signal control_path0_n_47 : STD_LOGIC;
  signal control_path0_n_48 : STD_LOGIC;
  signal control_path0_n_49 : STD_LOGIC;
  signal control_path0_n_50 : STD_LOGIC;
  signal control_path0_n_51 : STD_LOGIC;
  signal control_path0_n_52 : STD_LOGIC;
  signal control_path0_n_53 : STD_LOGIC;
  signal control_path0_n_54 : STD_LOGIC;
  signal control_path0_n_55 : STD_LOGIC;
  signal control_path0_n_56 : STD_LOGIC;
  signal control_path0_n_57 : STD_LOGIC;
  signal control_path0_n_58 : STD_LOGIC;
  signal control_path0_n_59 : STD_LOGIC;
  signal control_path0_n_7 : STD_LOGIC;
  signal control_path0_n_8 : STD_LOGIC;
  signal control_path0_n_9 : STD_LOGIC;
  signal cos_OBUF : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal data_path0_n_0 : STD_LOGIC;
  signal data_path0_n_1 : STD_LOGIC;
  signal data_path0_n_10 : STD_LOGIC;
  signal data_path0_n_100 : STD_LOGIC;
  signal data_path0_n_101 : STD_LOGIC;
  signal data_path0_n_102 : STD_LOGIC;
  signal data_path0_n_103 : STD_LOGIC;
  signal data_path0_n_104 : STD_LOGIC;
  signal data_path0_n_105 : STD_LOGIC;
  signal data_path0_n_106 : STD_LOGIC;
  signal data_path0_n_107 : STD_LOGIC;
  signal data_path0_n_108 : STD_LOGIC;
  signal data_path0_n_109 : STD_LOGIC;
  signal data_path0_n_11 : STD_LOGIC;
  signal data_path0_n_110 : STD_LOGIC;
  signal data_path0_n_111 : STD_LOGIC;
  signal data_path0_n_112 : STD_LOGIC;
  signal data_path0_n_113 : STD_LOGIC;
  signal data_path0_n_114 : STD_LOGIC;
  signal data_path0_n_115 : STD_LOGIC;
  signal data_path0_n_116 : STD_LOGIC;
  signal data_path0_n_117 : STD_LOGIC;
  signal data_path0_n_118 : STD_LOGIC;
  signal data_path0_n_119 : STD_LOGIC;
  signal data_path0_n_12 : STD_LOGIC;
  signal data_path0_n_120 : STD_LOGIC;
  signal data_path0_n_121 : STD_LOGIC;
  signal data_path0_n_122 : STD_LOGIC;
  signal data_path0_n_13 : STD_LOGIC;
  signal data_path0_n_14 : STD_LOGIC;
  signal data_path0_n_15 : STD_LOGIC;
  signal data_path0_n_16 : STD_LOGIC;
  signal data_path0_n_17 : STD_LOGIC;
  signal data_path0_n_18 : STD_LOGIC;
  signal data_path0_n_19 : STD_LOGIC;
  signal data_path0_n_2 : STD_LOGIC;
  signal data_path0_n_20 : STD_LOGIC;
  signal data_path0_n_21 : STD_LOGIC;
  signal data_path0_n_22 : STD_LOGIC;
  signal data_path0_n_23 : STD_LOGIC;
  signal data_path0_n_3 : STD_LOGIC;
  signal data_path0_n_4 : STD_LOGIC;
  signal data_path0_n_49 : STD_LOGIC;
  signal data_path0_n_5 : STD_LOGIC;
  signal data_path0_n_50 : STD_LOGIC;
  signal data_path0_n_51 : STD_LOGIC;
  signal data_path0_n_52 : STD_LOGIC;
  signal data_path0_n_53 : STD_LOGIC;
  signal data_path0_n_54 : STD_LOGIC;
  signal data_path0_n_55 : STD_LOGIC;
  signal data_path0_n_56 : STD_LOGIC;
  signal data_path0_n_57 : STD_LOGIC;
  signal data_path0_n_58 : STD_LOGIC;
  signal data_path0_n_59 : STD_LOGIC;
  signal data_path0_n_6 : STD_LOGIC;
  signal data_path0_n_60 : STD_LOGIC;
  signal data_path0_n_61 : STD_LOGIC;
  signal data_path0_n_62 : STD_LOGIC;
  signal data_path0_n_63 : STD_LOGIC;
  signal data_path0_n_7 : STD_LOGIC;
  signal data_path0_n_8 : STD_LOGIC;
  signal data_path0_n_89 : STD_LOGIC;
  signal data_path0_n_9 : STD_LOGIC;
  signal data_path0_n_90 : STD_LOGIC;
  signal data_path0_n_91 : STD_LOGIC;
  signal data_path0_n_92 : STD_LOGIC;
  signal data_path0_n_93 : STD_LOGIC;
  signal data_path0_n_94 : STD_LOGIC;
  signal data_path0_n_95 : STD_LOGIC;
  signal data_path0_n_96 : STD_LOGIC;
  signal data_path0_n_97 : STD_LOGIC;
  signal data_path0_n_98 : STD_LOGIC;
  signal data_path0_n_99 : STD_LOGIC;
  signal done_OBUF : STD_LOGIC;
  signal en_reg : STD_LOGIC;
  signal \fsm0/state\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal rst_IBUF : STD_LOGIC;
  signal sin_OBUF : STD_LOGIC_VECTOR ( 24 downto 0 );
  signal start_IBUF : STD_LOGIC;
begin
\angle_IBUF[0]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(0),
      O => angle_IBUF(0)
    );
\angle_IBUF[10]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(10),
      O => angle_IBUF(10)
    );
\angle_IBUF[11]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(11),
      O => angle_IBUF(11)
    );
\angle_IBUF[12]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(12),
      O => angle_IBUF(12)
    );
\angle_IBUF[13]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(13),
      O => angle_IBUF(13)
    );
\angle_IBUF[14]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(14),
      O => angle_IBUF(14)
    );
\angle_IBUF[15]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(15),
      O => angle_IBUF(15)
    );
\angle_IBUF[16]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(16),
      O => angle_IBUF(16)
    );
\angle_IBUF[17]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(17),
      O => angle_IBUF(17)
    );
\angle_IBUF[18]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(18),
      O => angle_IBUF(18)
    );
\angle_IBUF[19]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(19),
      O => angle_IBUF(19)
    );
\angle_IBUF[1]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(1),
      O => angle_IBUF(1)
    );
\angle_IBUF[20]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(20),
      O => angle_IBUF(20)
    );
\angle_IBUF[21]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(21),
      O => angle_IBUF(21)
    );
\angle_IBUF[22]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(22),
      O => angle_IBUF(22)
    );
\angle_IBUF[23]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(23),
      O => angle_IBUF(23)
    );
\angle_IBUF[2]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(2),
      O => angle_IBUF(2)
    );
\angle_IBUF[3]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(3),
      O => angle_IBUF(3)
    );
\angle_IBUF[4]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(4),
      O => angle_IBUF(4)
    );
\angle_IBUF[5]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(5),
      O => angle_IBUF(5)
    );
\angle_IBUF[6]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(6),
      O => angle_IBUF(6)
    );
\angle_IBUF[7]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(7),
      O => angle_IBUF(7)
    );
\angle_IBUF[8]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(8),
      O => angle_IBUF(8)
    );
\angle_IBUF[9]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => angle(9),
      O => angle_IBUF(9)
    );
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => clk,
      O => clk_IBUF
    );
control_path0: entity work.control_path
     port map (
      AR(0) => rst_IBUF,
      CLK => clk_IBUF_BUFG,
      D(23) => control_path0_n_16,
      D(22) => control_path0_n_17,
      D(21) => control_path0_n_18,
      D(20) => control_path0_n_19,
      D(19) => control_path0_n_20,
      D(18) => control_path0_n_21,
      D(17) => control_path0_n_22,
      D(16) => control_path0_n_23,
      D(15) => control_path0_n_24,
      D(14) => control_path0_n_25,
      D(13) => control_path0_n_26,
      D(12) => control_path0_n_27,
      D(11) => control_path0_n_28,
      D(10) => control_path0_n_29,
      D(9) => control_path0_n_30,
      D(8) => control_path0_n_31,
      D(7) => control_path0_n_32,
      D(6) => control_path0_n_33,
      D(5) => control_path0_n_34,
      D(4) => control_path0_n_35,
      D(3) => control_path0_n_36,
      D(2) => control_path0_n_37,
      D(1) => control_path0_n_38,
      D(0) => control_path0_n_39,
      E(0) => en_reg,
      O(3) => data_path0_n_20,
      O(2) => data_path0_n_21,
      O(1) => data_path0_n_22,
      O(0) => data_path0_n_23,
      Q(0) => \fsm0/state\(0),
      S(1) => control_path0_n_40,
      S(0) => control_path0_n_41,
      angle_IBUF(23 downto 0) => angle_IBUF(23 downto 0),
      done_OBUF => done_OBUF,
      \q_reg[11]\(1) => control_path0_n_50,
      \q_reg[11]\(0) => control_path0_n_51,
      \q_reg[11]_0\(3) => data_path0_n_8,
      \q_reg[11]_0\(2) => data_path0_n_9,
      \q_reg[11]_0\(1) => data_path0_n_10,
      \q_reg[11]_0\(0) => data_path0_n_11,
      \q_reg[14]\(2) => control_path0_n_42,
      \q_reg[14]\(1) => control_path0_n_43,
      \q_reg[14]\(0) => control_path0_n_44,
      \q_reg[14]_0\(2) => control_path0_n_52,
      \q_reg[14]_0\(1) => control_path0_n_53,
      \q_reg[14]_0\(0) => control_path0_n_54,
      \q_reg[15]\(3) => data_path0_n_12,
      \q_reg[15]\(2) => data_path0_n_13,
      \q_reg[15]\(1) => data_path0_n_14,
      \q_reg[15]\(0) => data_path0_n_15,
      \q_reg[19]\(1) => control_path0_n_45,
      \q_reg[19]\(0) => control_path0_n_46,
      \q_reg[19]_0\(1) => control_path0_n_55,
      \q_reg[19]_0\(0) => control_path0_n_56,
      \q_reg[19]_1\(3) => data_path0_n_16,
      \q_reg[19]_1\(2) => data_path0_n_17,
      \q_reg[19]_1\(1) => data_path0_n_18,
      \q_reg[19]_1\(0) => data_path0_n_19,
      \q_reg[1]\ => control_path0_n_7,
      \q_reg[1]_0\ => control_path0_n_9,
      \q_reg[1]_1\ => control_path0_n_10,
      \q_reg[1]_2\ => control_path0_n_11,
      \q_reg[1]_3\ => control_path0_n_13,
      \q_reg[1]_4\ => control_path0_n_14,
      \q_reg[22]\(2) => control_path0_n_47,
      \q_reg[22]\(1) => control_path0_n_48,
      \q_reg[22]\(0) => control_path0_n_49,
      \q_reg[22]_0\(2) => control_path0_n_57,
      \q_reg[22]_0\(1) => control_path0_n_58,
      \q_reg[22]_0\(0) => control_path0_n_59,
      \q_reg[2]\ => control_path0_n_8,
      \q_reg[2]_0\ => control_path0_n_12,
      \q_reg[3]\(3) => data_path0_n_0,
      \q_reg[3]\(2) => data_path0_n_1,
      \q_reg[3]\(1) => data_path0_n_2,
      \q_reg[3]\(0) => data_path0_n_3,
      \q_reg[4]\(4 downto 0) => addr(4 downto 0),
      \q_reg[7]\(3) => data_path0_n_4,
      \q_reg[7]\(2) => data_path0_n_5,
      \q_reg[7]\(1) => data_path0_n_6,
      \q_reg[7]\(0) => data_path0_n_7,
      start_IBUF => start_IBUF,
      \y0_carry__1\(0) => data_path0_n_49,
      \y0_carry__1_i_2__0\ => data_path0_n_105,
      \y0_carry__1_i_2__0_0\ => data_path0_n_103,
      \y0_carry__1_i_2__0_1\ => data_path0_n_104,
      \y0_carry__1_i_2__1\ => data_path0_n_115,
      \y0_carry__1_i_2__1_0\ => data_path0_n_113,
      \y0_carry__1_i_2__1_1\ => data_path0_n_114,
      \y0_carry__1_i_3__0\ => data_path0_n_110,
      \y0_carry__1_i_3__0_0\ => data_path0_n_108,
      \y0_carry__1_i_3__0_1\ => data_path0_n_109,
      \y0_carry__1_i_3__1\ => data_path0_n_120,
      \y0_carry__1_i_3__1_0\ => data_path0_n_118,
      \y0_carry__1_i_3__1_1\ => data_path0_n_119,
      \y0_carry__2_i_1__0\ => data_path0_n_107,
      \y0_carry__2_i_1__1\ => data_path0_n_117,
      \y0_carry__2_i_3__0\ => data_path0_n_112,
      \y0_carry__2_i_3__1\ => data_path0_n_122,
      \y0_carry__3_i_2__0\ => data_path0_n_106,
      \y0_carry__3_i_2__1\ => data_path0_n_116,
      \y0_carry__3_i_3__0\ => data_path0_n_111,
      \y0_carry__3_i_3__1\ => data_path0_n_121,
      \y0_carry__4_i_2__0\(13) => data_path0_n_89,
      \y0_carry__4_i_2__0\(12) => data_path0_n_90,
      \y0_carry__4_i_2__0\(11) => data_path0_n_91,
      \y0_carry__4_i_2__0\(10) => data_path0_n_92,
      \y0_carry__4_i_2__0\(9) => data_path0_n_93,
      \y0_carry__4_i_2__0\(8) => data_path0_n_94,
      \y0_carry__4_i_2__0\(7) => data_path0_n_95,
      \y0_carry__4_i_2__0\(6) => data_path0_n_96,
      \y0_carry__4_i_2__0\(5) => data_path0_n_97,
      \y0_carry__4_i_2__0\(4) => data_path0_n_98,
      \y0_carry__4_i_2__0\(3) => data_path0_n_99,
      \y0_carry__4_i_2__0\(2) => data_path0_n_100,
      \y0_carry__4_i_2__0\(1) => data_path0_n_101,
      \y0_carry__4_i_2__0\(0) => data_path0_n_102,
      \y0_carry__4_i_2__1\(13) => data_path0_n_50,
      \y0_carry__4_i_2__1\(12) => data_path0_n_51,
      \y0_carry__4_i_2__1\(11) => data_path0_n_52,
      \y0_carry__4_i_2__1\(10) => data_path0_n_53,
      \y0_carry__4_i_2__1\(9) => data_path0_n_54,
      \y0_carry__4_i_2__1\(8) => data_path0_n_55,
      \y0_carry__4_i_2__1\(7) => data_path0_n_56,
      \y0_carry__4_i_2__1\(6) => data_path0_n_57,
      \y0_carry__4_i_2__1\(5) => data_path0_n_58,
      \y0_carry__4_i_2__1\(4) => data_path0_n_59,
      \y0_carry__4_i_2__1\(3) => data_path0_n_60,
      \y0_carry__4_i_2__1\(2) => data_path0_n_61,
      \y0_carry__4_i_2__1\(1) => data_path0_n_62,
      \y0_carry__4_i_2__1\(0) => data_path0_n_63
    );
\cos_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(0),
      O => cos(0)
    );
\cos_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(10),
      O => cos(10)
    );
\cos_OBUF[11]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(11),
      O => cos(11)
    );
\cos_OBUF[12]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(12),
      O => cos(12)
    );
\cos_OBUF[13]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(13),
      O => cos(13)
    );
\cos_OBUF[14]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(14),
      O => cos(14)
    );
\cos_OBUF[15]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(15),
      O => cos(15)
    );
\cos_OBUF[16]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(16),
      O => cos(16)
    );
\cos_OBUF[17]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(17),
      O => cos(17)
    );
\cos_OBUF[18]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(18),
      O => cos(18)
    );
\cos_OBUF[19]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(19),
      O => cos(19)
    );
\cos_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(1),
      O => cos(1)
    );
\cos_OBUF[20]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(20),
      O => cos(20)
    );
\cos_OBUF[21]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(21),
      O => cos(21)
    );
\cos_OBUF[22]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(22),
      O => cos(22)
    );
\cos_OBUF[23]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(23),
      O => cos(23)
    );
\cos_OBUF[24]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(24),
      O => cos(24)
    );
\cos_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(2),
      O => cos(2)
    );
\cos_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(3),
      O => cos(3)
    );
\cos_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(4),
      O => cos(4)
    );
\cos_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(5),
      O => cos(5)
    );
\cos_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(6),
      O => cos(6)
    );
\cos_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(7),
      O => cos(7)
    );
\cos_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(8),
      O => cos(8)
    );
\cos_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cos_OBUF(9),
      O => cos(9)
    );
data_path0: entity work.data_path
     port map (
      AR(0) => rst_IBUF,
      CLK => clk_IBUF_BUFG,
      D(23) => control_path0_n_16,
      D(22) => control_path0_n_17,
      D(21) => control_path0_n_18,
      D(20) => control_path0_n_19,
      D(19) => control_path0_n_20,
      D(18) => control_path0_n_21,
      D(17) => control_path0_n_22,
      D(16) => control_path0_n_23,
      D(15) => control_path0_n_24,
      D(14) => control_path0_n_25,
      D(13) => control_path0_n_26,
      D(12) => control_path0_n_27,
      D(11) => control_path0_n_28,
      D(10) => control_path0_n_29,
      D(9) => control_path0_n_30,
      D(8) => control_path0_n_31,
      D(7) => control_path0_n_32,
      D(6) => control_path0_n_33,
      D(5) => control_path0_n_34,
      D(4) => control_path0_n_35,
      D(3) => control_path0_n_36,
      D(2) => control_path0_n_37,
      D(1) => control_path0_n_38,
      D(0) => control_path0_n_39,
      E(0) => en_reg,
      O(3) => data_path0_n_20,
      O(2) => data_path0_n_21,
      O(1) => data_path0_n_22,
      O(0) => data_path0_n_23,
      Q(0) => data_path0_n_49,
      S(1) => control_path0_n_40,
      S(0) => control_path0_n_41,
      cos_OBUF(24 downto 0) => cos_OBUF(24 downto 0),
      data_reg(4 downto 0) => addr(4 downto 0),
      \q_reg[0]\(0) => \fsm0/state\(0),
      \q_reg[11]\(3) => data_path0_n_8,
      \q_reg[11]\(2) => data_path0_n_9,
      \q_reg[11]\(1) => data_path0_n_10,
      \q_reg[11]\(0) => data_path0_n_11,
      \q_reg[11]_0\(1) => control_path0_n_50,
      \q_reg[11]_0\(0) => control_path0_n_51,
      \q_reg[15]\(3) => data_path0_n_12,
      \q_reg[15]\(2) => data_path0_n_13,
      \q_reg[15]\(1) => data_path0_n_14,
      \q_reg[15]\(0) => data_path0_n_15,
      \q_reg[15]_0\(2) => control_path0_n_42,
      \q_reg[15]_0\(1) => control_path0_n_43,
      \q_reg[15]_0\(0) => control_path0_n_44,
      \q_reg[15]_1\(2) => control_path0_n_52,
      \q_reg[15]_1\(1) => control_path0_n_53,
      \q_reg[15]_1\(0) => control_path0_n_54,
      \q_reg[18]\ => data_path0_n_109,
      \q_reg[18]_0\ => data_path0_n_119,
      \q_reg[19]\(3) => data_path0_n_16,
      \q_reg[19]\(2) => data_path0_n_17,
      \q_reg[19]\(1) => data_path0_n_18,
      \q_reg[19]\(0) => data_path0_n_19,
      \q_reg[19]_0\ => data_path0_n_104,
      \q_reg[19]_1\ => data_path0_n_114,
      \q_reg[19]_2\(1) => control_path0_n_45,
      \q_reg[19]_2\(0) => control_path0_n_46,
      \q_reg[19]_3\(1) => control_path0_n_55,
      \q_reg[19]_3\(0) => control_path0_n_56,
      \q_reg[20]\ => data_path0_n_110,
      \q_reg[20]_0\ => data_path0_n_112,
      \q_reg[20]_1\ => data_path0_n_120,
      \q_reg[20]_2\ => data_path0_n_122,
      \q_reg[21]\ => data_path0_n_105,
      \q_reg[21]_0\ => data_path0_n_107,
      \q_reg[21]_1\ => data_path0_n_115,
      \q_reg[21]_2\ => data_path0_n_117,
      \q_reg[22]\ => data_path0_n_108,
      \q_reg[22]_0\ => data_path0_n_111,
      \q_reg[22]_1\ => data_path0_n_118,
      \q_reg[22]_2\ => data_path0_n_121,
      \q_reg[23]\ => data_path0_n_103,
      \q_reg[23]_0\ => data_path0_n_106,
      \q_reg[23]_1\ => data_path0_n_113,
      \q_reg[23]_2\ => data_path0_n_116,
      \q_reg[23]_3\(2) => control_path0_n_47,
      \q_reg[23]_3\(1) => control_path0_n_48,
      \q_reg[23]_3\(0) => control_path0_n_49,
      \q_reg[23]_4\(2) => control_path0_n_57,
      \q_reg[23]_4\(1) => control_path0_n_58,
      \q_reg[23]_4\(0) => control_path0_n_59,
      \q_reg[24]\(13) => data_path0_n_50,
      \q_reg[24]\(12) => data_path0_n_51,
      \q_reg[24]\(11) => data_path0_n_52,
      \q_reg[24]\(10) => data_path0_n_53,
      \q_reg[24]\(9) => data_path0_n_54,
      \q_reg[24]\(8) => data_path0_n_55,
      \q_reg[24]\(7) => data_path0_n_56,
      \q_reg[24]\(6) => data_path0_n_57,
      \q_reg[24]\(5) => data_path0_n_58,
      \q_reg[24]\(4) => data_path0_n_59,
      \q_reg[24]\(3) => data_path0_n_60,
      \q_reg[24]\(2) => data_path0_n_61,
      \q_reg[24]\(1) => data_path0_n_62,
      \q_reg[24]\(0) => data_path0_n_63,
      \q_reg[24]_0\(13) => data_path0_n_89,
      \q_reg[24]_0\(12) => data_path0_n_90,
      \q_reg[24]_0\(11) => data_path0_n_91,
      \q_reg[24]_0\(10) => data_path0_n_92,
      \q_reg[24]_0\(9) => data_path0_n_93,
      \q_reg[24]_0\(8) => data_path0_n_94,
      \q_reg[24]_0\(7) => data_path0_n_95,
      \q_reg[24]_0\(6) => data_path0_n_96,
      \q_reg[24]_0\(5) => data_path0_n_97,
      \q_reg[24]_0\(4) => data_path0_n_98,
      \q_reg[24]_0\(3) => data_path0_n_99,
      \q_reg[24]_0\(2) => data_path0_n_100,
      \q_reg[24]_0\(1) => data_path0_n_101,
      \q_reg[24]_0\(0) => data_path0_n_102,
      \q_reg[3]\(3) => data_path0_n_0,
      \q_reg[3]\(2) => data_path0_n_1,
      \q_reg[3]\(1) => data_path0_n_2,
      \q_reg[3]\(0) => data_path0_n_3,
      \q_reg[7]\(3) => data_path0_n_4,
      \q_reg[7]\(2) => data_path0_n_5,
      \q_reg[7]\(1) => data_path0_n_6,
      \q_reg[7]\(0) => data_path0_n_7,
      sin_OBUF(24 downto 0) => sin_OBUF(24 downto 0),
      start_IBUF => start_IBUF,
      \y0_carry__1\ => control_path0_n_9,
      \y0_carry__1_0\ => control_path0_n_13,
      \y0_carry__2\ => control_path0_n_7,
      \y0_carry__2_0\ => control_path0_n_11,
      \y0_carry__3\ => control_path0_n_10,
      \y0_carry__3_0\ => control_path0_n_14,
      \y0_carry__4\ => control_path0_n_8,
      \y0_carry__4_0\ => control_path0_n_12
    );
done_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => done_OBUF,
      O => done
    );
rst_IBUF_inst: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => rst,
      O => rst_IBUF
    );
\sin_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(0),
      O => sin(0)
    );
\sin_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(10),
      O => sin(10)
    );
\sin_OBUF[11]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(11),
      O => sin(11)
    );
\sin_OBUF[12]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(12),
      O => sin(12)
    );
\sin_OBUF[13]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(13),
      O => sin(13)
    );
\sin_OBUF[14]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(14),
      O => sin(14)
    );
\sin_OBUF[15]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(15),
      O => sin(15)
    );
\sin_OBUF[16]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(16),
      O => sin(16)
    );
\sin_OBUF[17]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(17),
      O => sin(17)
    );
\sin_OBUF[18]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(18),
      O => sin(18)
    );
\sin_OBUF[19]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(19),
      O => sin(19)
    );
\sin_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(1),
      O => sin(1)
    );
\sin_OBUF[20]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(20),
      O => sin(20)
    );
\sin_OBUF[21]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(21),
      O => sin(21)
    );
\sin_OBUF[22]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(22),
      O => sin(22)
    );
\sin_OBUF[23]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(23),
      O => sin(23)
    );
\sin_OBUF[24]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(24),
      O => sin(24)
    );
\sin_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(2),
      O => sin(2)
    );
\sin_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(3),
      O => sin(3)
    );
\sin_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(4),
      O => sin(4)
    );
\sin_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(5),
      O => sin(5)
    );
\sin_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(6),
      O => sin(6)
    );
\sin_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(7),
      O => sin(7)
    );
\sin_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(8),
      O => sin(8)
    );
\sin_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => sin_OBUF(9),
      O => sin(9)
    );
start_IBUF_inst: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => start,
      O => start_IBUF
    );
end STRUCTURE;
