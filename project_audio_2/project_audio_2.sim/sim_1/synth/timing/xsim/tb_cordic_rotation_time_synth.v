// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Wed May  3 07:36:30 2023
// Host        : juan-Inspiron-14-3467 running 64-bit Linux Mint 21.1
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/juan/Documentos/JuanK/universidad/profun_2/project_audio_2/project_audio_2.sim/sim_1/synth/timing/xsim/tb_cordic_rotation_time_synth.v
// Design      : cordic_rotation
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module Mux_2_1
   (cos_OBUF,
    D,
    \q_reg[3] ,
    Q,
    S,
    \q_reg[7] ,
    \q_reg[11] ,
    \q_reg[15] ,
    \q_reg[19] ,
    \q_reg[23] ,
    \q_reg[24] ,
    start_IBUF,
    \q_reg[0] );
  output [24:0]cos_OBUF;
  output [24:0]D;
  input \q_reg[3] ;
  input [23:0]Q;
  input [3:0]S;
  input [3:0]\q_reg[7] ;
  input [3:0]\q_reg[11] ;
  input [3:0]\q_reg[15] ;
  input [3:0]\q_reg[19] ;
  input [3:0]\q_reg[23] ;
  input [0:0]\q_reg[24] ;
  input start_IBUF;
  input [0:0]\q_reg[0] ;

  wire [24:0]D;
  wire [23:0]Q;
  wire [3:0]S;
  wire [24:0]cos_OBUF;
  wire [0:0]\q_reg[0] ;
  wire [3:0]\q_reg[11] ;
  wire [3:0]\q_reg[15] ;
  wire [3:0]\q_reg[19] ;
  wire [3:0]\q_reg[23] ;
  wire [0:0]\q_reg[24] ;
  wire \q_reg[3] ;
  wire [3:0]\q_reg[7] ;
  wire start_IBUF;
  wire y0_carry__0_n_0;
  wire y0_carry__0_n_1;
  wire y0_carry__0_n_2;
  wire y0_carry__0_n_3;
  wire y0_carry__1_n_0;
  wire y0_carry__1_n_1;
  wire y0_carry__1_n_2;
  wire y0_carry__1_n_3;
  wire y0_carry__2_n_0;
  wire y0_carry__2_n_1;
  wire y0_carry__2_n_2;
  wire y0_carry__2_n_3;
  wire y0_carry__3_n_0;
  wire y0_carry__3_n_1;
  wire y0_carry__3_n_2;
  wire y0_carry__3_n_3;
  wire y0_carry__4_n_0;
  wire y0_carry__4_n_1;
  wire y0_carry__4_n_2;
  wire y0_carry__4_n_3;
  wire y0_carry_n_0;
  wire y0_carry_n_1;
  wire y0_carry_n_2;
  wire y0_carry_n_3;
  wire [3:0]NLW_y0_carry__5_CO_UNCONNECTED;
  wire [3:1]NLW_y0_carry__5_O_UNCONNECTED;

  LUT3 #(
    .INIT(8'hAE)) 
    \q[0]_i_1__0 
       (.I0(cos_OBUF[0]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[10]_i_1 
       (.I0(cos_OBUF[10]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[10]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[11]_i_1 
       (.I0(cos_OBUF[11]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[11]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[12]_i_1 
       (.I0(cos_OBUF[12]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[12]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[13]_i_1 
       (.I0(cos_OBUF[13]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[13]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[14]_i_1 
       (.I0(cos_OBUF[14]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[14]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[15]_i_1 
       (.I0(cos_OBUF[15]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[15]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[16]_i_1 
       (.I0(cos_OBUF[16]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[16]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[17]_i_1 
       (.I0(cos_OBUF[17]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[17]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[18]_i_1 
       (.I0(cos_OBUF[18]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[18]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[19]_i_1 
       (.I0(cos_OBUF[19]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[19]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[1]_i_1__0 
       (.I0(cos_OBUF[1]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[20]_i_1 
       (.I0(cos_OBUF[20]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[20]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[21]_i_1 
       (.I0(cos_OBUF[21]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[21]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[22]_i_1 
       (.I0(cos_OBUF[22]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[22]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[23]_i_1 
       (.I0(cos_OBUF[23]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[23]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[24]_i_1 
       (.I0(cos_OBUF[24]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[24]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[2]_i_1__0 
       (.I0(cos_OBUF[2]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[3]_i_1 
       (.I0(cos_OBUF[3]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[4]_i_1__0 
       (.I0(cos_OBUF[4]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[5]_i_1 
       (.I0(cos_OBUF[5]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[6]_i_1 
       (.I0(cos_OBUF[6]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[7]_i_1 
       (.I0(cos_OBUF[7]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[7]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[8]_i_1 
       (.I0(cos_OBUF[8]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[8]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hAE)) 
    \q[9]_i_1 
       (.I0(cos_OBUF[9]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[9]));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry
       (.CI(1'b0),
        .CO({y0_carry_n_0,y0_carry_n_1,y0_carry_n_2,y0_carry_n_3}),
        .CYINIT(\q_reg[3] ),
        .DI(Q[3:0]),
        .O(cos_OBUF[3:0]),
        .S(S));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__0
       (.CI(y0_carry_n_0),
        .CO({y0_carry__0_n_0,y0_carry__0_n_1,y0_carry__0_n_2,y0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(Q[7:4]),
        .O(cos_OBUF[7:4]),
        .S(\q_reg[7] ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__1
       (.CI(y0_carry__0_n_0),
        .CO({y0_carry__1_n_0,y0_carry__1_n_1,y0_carry__1_n_2,y0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(Q[11:8]),
        .O(cos_OBUF[11:8]),
        .S(\q_reg[11] ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__2
       (.CI(y0_carry__1_n_0),
        .CO({y0_carry__2_n_0,y0_carry__2_n_1,y0_carry__2_n_2,y0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(Q[15:12]),
        .O(cos_OBUF[15:12]),
        .S(\q_reg[15] ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__3
       (.CI(y0_carry__2_n_0),
        .CO({y0_carry__3_n_0,y0_carry__3_n_1,y0_carry__3_n_2,y0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(Q[19:16]),
        .O(cos_OBUF[19:16]),
        .S(\q_reg[19] ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__4
       (.CI(y0_carry__3_n_0),
        .CO({y0_carry__4_n_0,y0_carry__4_n_1,y0_carry__4_n_2,y0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(Q[23:20]),
        .O(cos_OBUF[23:20]),
        .S(\q_reg[23] ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__5
       (.CI(y0_carry__4_n_0),
        .CO(NLW_y0_carry__5_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_y0_carry__5_O_UNCONNECTED[3:1],cos_OBUF[24]}),
        .S({1'b0,1'b0,1'b0,\q_reg[24] }));
endmodule

(* ORIG_REF_NAME = "Mux_2_1" *) 
module Mux_2_1_0
   (sin_OBUF,
    D,
    Q,
    \q_reg[23] ,
    S,
    \q_reg[7] ,
    \q_reg[11] ,
    \q_reg[15] ,
    \q_reg[19] ,
    \q_reg[23]_0 ,
    \q_reg[24] ,
    start_IBUF,
    \q_reg[0] );
  output [24:0]sin_OBUF;
  output [24:0]D;
  input [0:0]Q;
  input [23:0]\q_reg[23] ;
  input [3:0]S;
  input [3:0]\q_reg[7] ;
  input [3:0]\q_reg[11] ;
  input [3:0]\q_reg[15] ;
  input [3:0]\q_reg[19] ;
  input [3:0]\q_reg[23]_0 ;
  input [0:0]\q_reg[24] ;
  input start_IBUF;
  input [0:0]\q_reg[0] ;

  wire [24:0]D;
  wire [0:0]Q;
  wire [3:0]S;
  wire [0:0]\q_reg[0] ;
  wire [3:0]\q_reg[11] ;
  wire [3:0]\q_reg[15] ;
  wire [3:0]\q_reg[19] ;
  wire [23:0]\q_reg[23] ;
  wire [3:0]\q_reg[23]_0 ;
  wire [0:0]\q_reg[24] ;
  wire [3:0]\q_reg[7] ;
  wire [24:0]sin_OBUF;
  wire start_IBUF;
  wire y0_carry__0_n_0;
  wire y0_carry__0_n_1;
  wire y0_carry__0_n_2;
  wire y0_carry__0_n_3;
  wire y0_carry__1_n_0;
  wire y0_carry__1_n_1;
  wire y0_carry__1_n_2;
  wire y0_carry__1_n_3;
  wire y0_carry__2_n_0;
  wire y0_carry__2_n_1;
  wire y0_carry__2_n_2;
  wire y0_carry__2_n_3;
  wire y0_carry__3_n_0;
  wire y0_carry__3_n_1;
  wire y0_carry__3_n_2;
  wire y0_carry__3_n_3;
  wire y0_carry__4_n_0;
  wire y0_carry__4_n_1;
  wire y0_carry__4_n_2;
  wire y0_carry__4_n_3;
  wire y0_carry_n_0;
  wire y0_carry_n_1;
  wire y0_carry_n_2;
  wire y0_carry_n_3;
  wire [3:0]NLW_y0_carry__5_CO_UNCONNECTED;
  wire [3:1]NLW_y0_carry__5_O_UNCONNECTED;

  LUT3 #(
    .INIT(8'hA2)) 
    \q[0]_i_1__2 
       (.I0(sin_OBUF[0]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[10]_i_1__1 
       (.I0(sin_OBUF[10]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[10]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[11]_i_1__1 
       (.I0(sin_OBUF[11]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[11]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[12]_i_1__1 
       (.I0(sin_OBUF[12]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[12]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[13]_i_1__1 
       (.I0(sin_OBUF[13]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[13]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[14]_i_1__1 
       (.I0(sin_OBUF[14]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[14]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[15]_i_1__1 
       (.I0(sin_OBUF[15]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[15]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[16]_i_1__1 
       (.I0(sin_OBUF[16]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[16]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[17]_i_1__1 
       (.I0(sin_OBUF[17]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[17]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[18]_i_1__1 
       (.I0(sin_OBUF[18]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[18]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[19]_i_1__1 
       (.I0(sin_OBUF[19]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[19]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[1]_i_1__2 
       (.I0(sin_OBUF[1]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[20]_i_1__1 
       (.I0(sin_OBUF[20]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[20]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[21]_i_1__1 
       (.I0(sin_OBUF[21]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[21]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[22]_i_1__1 
       (.I0(sin_OBUF[22]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[22]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[23]_i_1__1 
       (.I0(sin_OBUF[23]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[23]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[24]_i_1__0 
       (.I0(sin_OBUF[24]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[24]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[2]_i_1__2 
       (.I0(sin_OBUF[2]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[3]_i_1__1 
       (.I0(sin_OBUF[3]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[4]_i_1__2 
       (.I0(sin_OBUF[4]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[5]_i_1__1 
       (.I0(sin_OBUF[5]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[6]_i_1__1 
       (.I0(sin_OBUF[6]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[7]_i_1__1 
       (.I0(sin_OBUF[7]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[7]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[8]_i_1__1 
       (.I0(sin_OBUF[8]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[8]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hA2)) 
    \q[9]_i_1__1 
       (.I0(sin_OBUF[9]),
        .I1(start_IBUF),
        .I2(\q_reg[0] ),
        .O(D[9]));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry
       (.CI(1'b0),
        .CO({y0_carry_n_0,y0_carry_n_1,y0_carry_n_2,y0_carry_n_3}),
        .CYINIT(Q),
        .DI(\q_reg[23] [3:0]),
        .O(sin_OBUF[3:0]),
        .S(S));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__0
       (.CI(y0_carry_n_0),
        .CO({y0_carry__0_n_0,y0_carry__0_n_1,y0_carry__0_n_2,y0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[23] [7:4]),
        .O(sin_OBUF[7:4]),
        .S(\q_reg[7] ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__1
       (.CI(y0_carry__0_n_0),
        .CO({y0_carry__1_n_0,y0_carry__1_n_1,y0_carry__1_n_2,y0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[23] [11:8]),
        .O(sin_OBUF[11:8]),
        .S(\q_reg[11] ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__2
       (.CI(y0_carry__1_n_0),
        .CO({y0_carry__2_n_0,y0_carry__2_n_1,y0_carry__2_n_2,y0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[23] [15:12]),
        .O(sin_OBUF[15:12]),
        .S(\q_reg[15] ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__3
       (.CI(y0_carry__2_n_0),
        .CO({y0_carry__3_n_0,y0_carry__3_n_1,y0_carry__3_n_2,y0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[23] [19:16]),
        .O(sin_OBUF[19:16]),
        .S(\q_reg[19] ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__4
       (.CI(y0_carry__3_n_0),
        .CO({y0_carry__4_n_0,y0_carry__4_n_1,y0_carry__4_n_2,y0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(\q_reg[23] [23:20]),
        .O(sin_OBUF[23:20]),
        .S(\q_reg[23]_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__5
       (.CI(y0_carry__4_n_0),
        .CO(NLW_y0_carry__5_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_y0_carry__5_O_UNCONNECTED[3:1],sin_OBUF[24]}),
        .S({1'b0,1'b0,1'b0,\q_reg[24] }));
endmodule

(* ORIG_REF_NAME = "Mux_2_1" *) 
module Mux_2_1__parameterized2
   (\q_reg[3] ,
    \q_reg[7] ,
    \q_reg[11] ,
    \q_reg[15] ,
    \q_reg[19] ,
    O,
    \q_reg[3]_0 ,
    Q,
    S,
    \q_reg[7]_0 ,
    \q_reg[11]_0 ,
    \q_reg[15]_0 ,
    \q_reg[19]_0 ,
    \q_reg[23] );
  output [3:0]\q_reg[3] ;
  output [3:0]\q_reg[7] ;
  output [3:0]\q_reg[11] ;
  output [3:0]\q_reg[15] ;
  output [3:0]\q_reg[19] ;
  output [3:0]O;
  input \q_reg[3]_0 ;
  input [22:0]Q;
  input [3:0]S;
  input [3:0]\q_reg[7]_0 ;
  input [3:0]\q_reg[11]_0 ;
  input [3:0]\q_reg[15]_0 ;
  input [3:0]\q_reg[19]_0 ;
  input [3:0]\q_reg[23] ;

  wire [3:0]O;
  wire [22:0]Q;
  wire [3:0]S;
  wire [3:0]\q_reg[11] ;
  wire [3:0]\q_reg[11]_0 ;
  wire [3:0]\q_reg[15] ;
  wire [3:0]\q_reg[15]_0 ;
  wire [3:0]\q_reg[19] ;
  wire [3:0]\q_reg[19]_0 ;
  wire [3:0]\q_reg[23] ;
  wire [3:0]\q_reg[3] ;
  wire \q_reg[3]_0 ;
  wire [3:0]\q_reg[7] ;
  wire [3:0]\q_reg[7]_0 ;
  wire y0_carry__0_n_0;
  wire y0_carry__0_n_1;
  wire y0_carry__0_n_2;
  wire y0_carry__0_n_3;
  wire y0_carry__1_n_0;
  wire y0_carry__1_n_1;
  wire y0_carry__1_n_2;
  wire y0_carry__1_n_3;
  wire y0_carry__2_n_0;
  wire y0_carry__2_n_1;
  wire y0_carry__2_n_2;
  wire y0_carry__2_n_3;
  wire y0_carry__3_n_0;
  wire y0_carry__3_n_1;
  wire y0_carry__3_n_2;
  wire y0_carry__3_n_3;
  wire y0_carry__4_n_1;
  wire y0_carry__4_n_2;
  wire y0_carry__4_n_3;
  wire y0_carry_n_0;
  wire y0_carry_n_1;
  wire y0_carry_n_2;
  wire y0_carry_n_3;
  wire [3:3]NLW_y0_carry__4_CO_UNCONNECTED;

  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry
       (.CI(1'b0),
        .CO({y0_carry_n_0,y0_carry_n_1,y0_carry_n_2,y0_carry_n_3}),
        .CYINIT(\q_reg[3]_0 ),
        .DI(Q[3:0]),
        .O(\q_reg[3] ),
        .S(S));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__0
       (.CI(y0_carry_n_0),
        .CO({y0_carry__0_n_0,y0_carry__0_n_1,y0_carry__0_n_2,y0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(Q[7:4]),
        .O(\q_reg[7] ),
        .S(\q_reg[7]_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__1
       (.CI(y0_carry__0_n_0),
        .CO({y0_carry__1_n_0,y0_carry__1_n_1,y0_carry__1_n_2,y0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(Q[11:8]),
        .O(\q_reg[11] ),
        .S(\q_reg[11]_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__2
       (.CI(y0_carry__1_n_0),
        .CO({y0_carry__2_n_0,y0_carry__2_n_1,y0_carry__2_n_2,y0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(Q[15:12]),
        .O(\q_reg[15] ),
        .S(\q_reg[15]_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__3
       (.CI(y0_carry__2_n_0),
        .CO({y0_carry__3_n_0,y0_carry__3_n_1,y0_carry__3_n_2,y0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(Q[19:16]),
        .O(\q_reg[19] ),
        .S(\q_reg[19]_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 y0_carry__4
       (.CI(y0_carry__3_n_0),
        .CO({NLW_y0_carry__4_CO_UNCONNECTED[3],y0_carry__4_n_1,y0_carry__4_n_2,y0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,Q[22:20]}),
        .O(O),
        .S(\q_reg[23] ));
endmodule

module ROM
   (out_ROM,
    data_reg_0,
    CLK,
    data_reg_1);
  output [22:0]out_ROM;
  output [0:0]data_reg_0;
  input CLK;
  input [4:0]data_reg_1;

  wire CLK;
  wire [0:0]data_reg_0;
  wire [4:0]data_reg_1;
  wire [22:0]out_ROM;
  wire [15:5]NLW_data_reg_DOBDO_UNCONNECTED;
  wire [1:0]NLW_data_reg_DOPBDOP_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p2_d16" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d5" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "736" *) 
  (* RTL_RAM_NAME = "data_path0/ROM_1/data_reg" *) 
  (* RTL_RAM_TYPE = "RAM_TDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "31" *) 
  (* ram_ext_slice_begin = "18" *) 
  (* ram_ext_slice_end = "22" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "17" *) 
  RAMB18E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000658),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h005100A20145028B05170A2F145F28BE517CA2F645D78B0D1111FB38E4050000),
    .INIT_01(256'h0000000000000000000000000000000000000000000100020005000A00140028),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000001000200040008),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(18'h00000),
    .INIT_B(18'h00000),
    .INIT_FILE("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(18),
    .READ_WIDTH_B(18),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(18'h00000),
    .SRVAL_B(18'h00000),
    .WRITE_MODE_A("WRITE_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(18),
    .WRITE_WIDTH_B(18)) 
    data_reg
       (.ADDRARDADDR({1'b0,1'b0,1'b0,1'b0,1'b0,data_reg_1,1'b0,1'b0,1'b0,1'b0}),
        .ADDRBWRADDR({1'b1,1'b0,1'b0,1'b0,1'b0,data_reg_1,1'b0,1'b0,1'b0,1'b0}),
        .CLKARDCLK(CLK),
        .CLKBWRCLK(CLK),
        .DIADI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIBDI({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b1,1'b1}),
        .DIPBDIP({1'b1,1'b1}),
        .DOADO(out_ROM[15:0]),
        .DOBDO({NLW_data_reg_DOBDO_UNCONNECTED[15:5],out_ROM[22:18]}),
        .DOPADOP(out_ROM[17:16]),
        .DOPBDOP(NLW_data_reg_DOPBDOP_UNCONNECTED[1:0]),
        .ENARDEN(1'b1),
        .ENBWREN(1'b1),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .WEA({1'b0,1'b0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0}));
  LUT1 #(
    .INIT(2'h1)) 
    y0_carry__4_i_1
       (.I0(out_ROM[22]),
        .O(data_reg_0));
endmodule

module control_path
   (Q,
    done_OBUF,
    \q_reg[4] ,
    \q_reg[1] ,
    \q_reg[2] ,
    \q_reg[1]_0 ,
    \q_reg[1]_1 ,
    \q_reg[1]_2 ,
    \q_reg[2]_0 ,
    \q_reg[1]_3 ,
    \q_reg[1]_4 ,
    E,
    D,
    S,
    \q_reg[14] ,
    \q_reg[19] ,
    \q_reg[22] ,
    \q_reg[11] ,
    \q_reg[14]_0 ,
    \q_reg[19]_0 ,
    \q_reg[22]_0 ,
    start_IBUF,
    y0_carry__1_i_2__0,
    y0_carry__1_i_2__0_0,
    y0_carry__1_i_2__0_1,
    y0_carry__2_i_1__0,
    y0_carry__3_i_2__0,
    y0_carry__4_i_2__0,
    y0_carry__1_i_3__0,
    y0_carry__1_i_3__0_0,
    y0_carry__1_i_3__0_1,
    y0_carry__2_i_3__0,
    y0_carry__3_i_3__0,
    y0_carry__1_i_2__1,
    y0_carry__1_i_2__1_0,
    y0_carry__1_i_2__1_1,
    y0_carry__2_i_1__1,
    y0_carry__3_i_2__1,
    y0_carry__4_i_2__1,
    y0_carry__1_i_3__1,
    y0_carry__1_i_3__1_0,
    y0_carry__1_i_3__1_1,
    y0_carry__2_i_3__1,
    y0_carry__3_i_3__1,
    angle_IBUF,
    O,
    \q_reg[19]_1 ,
    \q_reg[15] ,
    \q_reg[11]_0 ,
    \q_reg[7] ,
    \q_reg[3] ,
    y0_carry__1,
    CLK,
    AR);
  output [0:0]Q;
  output done_OBUF;
  output [4:0]\q_reg[4] ;
  output \q_reg[1] ;
  output \q_reg[2] ;
  output \q_reg[1]_0 ;
  output \q_reg[1]_1 ;
  output \q_reg[1]_2 ;
  output \q_reg[2]_0 ;
  output \q_reg[1]_3 ;
  output \q_reg[1]_4 ;
  output [0:0]E;
  output [23:0]D;
  output [1:0]S;
  output [2:0]\q_reg[14] ;
  output [1:0]\q_reg[19] ;
  output [2:0]\q_reg[22] ;
  output [1:0]\q_reg[11] ;
  output [2:0]\q_reg[14]_0 ;
  output [1:0]\q_reg[19]_0 ;
  output [2:0]\q_reg[22]_0 ;
  input start_IBUF;
  input y0_carry__1_i_2__0;
  input y0_carry__1_i_2__0_0;
  input y0_carry__1_i_2__0_1;
  input y0_carry__2_i_1__0;
  input y0_carry__3_i_2__0;
  input [13:0]y0_carry__4_i_2__0;
  input y0_carry__1_i_3__0;
  input y0_carry__1_i_3__0_0;
  input y0_carry__1_i_3__0_1;
  input y0_carry__2_i_3__0;
  input y0_carry__3_i_3__0;
  input y0_carry__1_i_2__1;
  input y0_carry__1_i_2__1_0;
  input y0_carry__1_i_2__1_1;
  input y0_carry__2_i_1__1;
  input y0_carry__3_i_2__1;
  input [13:0]y0_carry__4_i_2__1;
  input y0_carry__1_i_3__1;
  input y0_carry__1_i_3__1_0;
  input y0_carry__1_i_3__1_1;
  input y0_carry__2_i_3__1;
  input y0_carry__3_i_3__1;
  input [23:0]angle_IBUF;
  input [3:0]O;
  input [3:0]\q_reg[19]_1 ;
  input [3:0]\q_reg[15] ;
  input [3:0]\q_reg[11]_0 ;
  input [3:0]\q_reg[7] ;
  input [3:0]\q_reg[3] ;
  input [0:0]y0_carry__1;
  input CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire CLK;
  wire [23:0]D;
  wire [0:0]E;
  wire [3:0]O;
  wire [0:0]Q;
  wire [1:0]S;
  wire [23:0]angle_IBUF;
  wire done_OBUF;
  wire fsm0_n_1;
  wire [1:0]\q_reg[11] ;
  wire [3:0]\q_reg[11]_0 ;
  wire [2:0]\q_reg[14] ;
  wire [2:0]\q_reg[14]_0 ;
  wire [3:0]\q_reg[15] ;
  wire [1:0]\q_reg[19] ;
  wire [1:0]\q_reg[19]_0 ;
  wire [3:0]\q_reg[19]_1 ;
  wire \q_reg[1] ;
  wire \q_reg[1]_0 ;
  wire \q_reg[1]_1 ;
  wire \q_reg[1]_2 ;
  wire \q_reg[1]_3 ;
  wire \q_reg[1]_4 ;
  wire [2:0]\q_reg[22] ;
  wire [2:0]\q_reg[22]_0 ;
  wire \q_reg[2] ;
  wire \q_reg[2]_0 ;
  wire [3:0]\q_reg[3] ;
  wire [4:0]\q_reg[4] ;
  wire [3:0]\q_reg[7] ;
  wire regr_n_6;
  wire start_IBUF;
  wire [0:0]y0_carry__1;
  wire y0_carry__1_i_2__0;
  wire y0_carry__1_i_2__0_0;
  wire y0_carry__1_i_2__0_1;
  wire y0_carry__1_i_2__1;
  wire y0_carry__1_i_2__1_0;
  wire y0_carry__1_i_2__1_1;
  wire y0_carry__1_i_3__0;
  wire y0_carry__1_i_3__0_0;
  wire y0_carry__1_i_3__0_1;
  wire y0_carry__1_i_3__1;
  wire y0_carry__1_i_3__1_0;
  wire y0_carry__1_i_3__1_1;
  wire y0_carry__2_i_1__0;
  wire y0_carry__2_i_1__1;
  wire y0_carry__2_i_3__0;
  wire y0_carry__2_i_3__1;
  wire y0_carry__3_i_2__0;
  wire y0_carry__3_i_2__1;
  wire y0_carry__3_i_3__0;
  wire y0_carry__3_i_3__1;
  wire [13:0]y0_carry__4_i_2__0;
  wire [13:0]y0_carry__4_i_2__1;

  fsm fsm0
       (.AR(AR),
        .CLK(CLK),
        .D(D),
        .E(E),
        .\FSM_sequential_state_reg[0]_0 (fsm0_n_1),
        .\FSM_sequential_state_reg[0]_1 (regr_n_6),
        .O(O),
        .Q(Q),
        .angle_IBUF(angle_IBUF),
        .\q_reg[11] (\q_reg[11]_0 ),
        .\q_reg[15] (\q_reg[15] ),
        .\q_reg[19] (\q_reg[19]_1 ),
        .\q_reg[3] (\q_reg[3] ),
        .\q_reg[7] (\q_reg[7] ),
        .start_IBUF(start_IBUF));
  \reg  regr
       (.AR(AR),
        .CLK(CLK),
        .E(E),
        .Q(\q_reg[4] ),
        .S(S),
        .done_OBUF(done_OBUF),
        .\q_reg[0]_0 (Q),
        .\q_reg[11] (\q_reg[11] ),
        .\q_reg[14] (\q_reg[14] ),
        .\q_reg[14]_0 (\q_reg[14]_0 ),
        .\q_reg[19] (\q_reg[19] ),
        .\q_reg[19]_0 (\q_reg[19]_0 ),
        .\q_reg[1]_0 (regr_n_6),
        .\q_reg[1]_1 (\q_reg[1] ),
        .\q_reg[1]_2 (\q_reg[1]_0 ),
        .\q_reg[1]_3 (\q_reg[1]_1 ),
        .\q_reg[1]_4 (\q_reg[1]_2 ),
        .\q_reg[1]_5 (\q_reg[1]_3 ),
        .\q_reg[1]_6 (\q_reg[1]_4 ),
        .\q_reg[22] (\q_reg[22] ),
        .\q_reg[22]_0 (\q_reg[22]_0 ),
        .\q_reg[2]_0 (\q_reg[2] ),
        .\q_reg[2]_1 (\q_reg[2]_0 ),
        .\q_reg[4]_0 (fsm0_n_1),
        .start_IBUF(start_IBUF),
        .y0_carry__1(y0_carry__1),
        .y0_carry__1_i_2__0_0(y0_carry__1_i_2__0),
        .y0_carry__1_i_2__0_1(y0_carry__1_i_2__0_0),
        .y0_carry__1_i_2__0_2(y0_carry__1_i_2__0_1),
        .y0_carry__1_i_2__1_0(y0_carry__1_i_2__1),
        .y0_carry__1_i_2__1_1(y0_carry__1_i_2__1_0),
        .y0_carry__1_i_2__1_2(y0_carry__1_i_2__1_1),
        .y0_carry__1_i_3__0(y0_carry__1_i_3__0),
        .y0_carry__1_i_3__0_0(y0_carry__1_i_3__0_0),
        .y0_carry__1_i_3__0_1(y0_carry__1_i_3__0_1),
        .y0_carry__1_i_3__1(y0_carry__1_i_3__1),
        .y0_carry__1_i_3__1_0(y0_carry__1_i_3__1_0),
        .y0_carry__1_i_3__1_1(y0_carry__1_i_3__1_1),
        .y0_carry__2_i_1__0(y0_carry__2_i_1__0),
        .y0_carry__2_i_1__1(y0_carry__2_i_1__1),
        .y0_carry__2_i_3__0_0(y0_carry__2_i_3__0),
        .y0_carry__2_i_3__1_0(y0_carry__2_i_3__1),
        .y0_carry__3_i_2__0_0(y0_carry__3_i_2__0),
        .y0_carry__3_i_2__1_0(y0_carry__3_i_2__1),
        .y0_carry__3_i_3__0(y0_carry__3_i_3__0),
        .y0_carry__3_i_3__1(y0_carry__3_i_3__1),
        .y0_carry__4_i_2__0_0(y0_carry__4_i_2__0),
        .y0_carry__4_i_2__1_0(y0_carry__4_i_2__1));
endmodule

(* NotValidForBitStream *)
module cordic_rotation
   (angle,
    rst,
    start,
    clk,
    cos,
    sin,
    done);
  input [23:0]angle;
  input rst;
  input start;
  input clk;
  output [24:0]cos;
  output [24:0]sin;
  output done;

  wire [4:0]addr;
  wire [23:0]angle;
  wire [23:0]angle_IBUF;
  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire control_path0_n_10;
  wire control_path0_n_11;
  wire control_path0_n_12;
  wire control_path0_n_13;
  wire control_path0_n_14;
  wire control_path0_n_16;
  wire control_path0_n_17;
  wire control_path0_n_18;
  wire control_path0_n_19;
  wire control_path0_n_20;
  wire control_path0_n_21;
  wire control_path0_n_22;
  wire control_path0_n_23;
  wire control_path0_n_24;
  wire control_path0_n_25;
  wire control_path0_n_26;
  wire control_path0_n_27;
  wire control_path0_n_28;
  wire control_path0_n_29;
  wire control_path0_n_30;
  wire control_path0_n_31;
  wire control_path0_n_32;
  wire control_path0_n_33;
  wire control_path0_n_34;
  wire control_path0_n_35;
  wire control_path0_n_36;
  wire control_path0_n_37;
  wire control_path0_n_38;
  wire control_path0_n_39;
  wire control_path0_n_40;
  wire control_path0_n_41;
  wire control_path0_n_42;
  wire control_path0_n_43;
  wire control_path0_n_44;
  wire control_path0_n_45;
  wire control_path0_n_46;
  wire control_path0_n_47;
  wire control_path0_n_48;
  wire control_path0_n_49;
  wire control_path0_n_50;
  wire control_path0_n_51;
  wire control_path0_n_52;
  wire control_path0_n_53;
  wire control_path0_n_54;
  wire control_path0_n_55;
  wire control_path0_n_56;
  wire control_path0_n_57;
  wire control_path0_n_58;
  wire control_path0_n_59;
  wire control_path0_n_7;
  wire control_path0_n_8;
  wire control_path0_n_9;
  wire [24:0]cos;
  wire [24:0]cos_OBUF;
  wire data_path0_n_0;
  wire data_path0_n_1;
  wire data_path0_n_10;
  wire data_path0_n_100;
  wire data_path0_n_101;
  wire data_path0_n_102;
  wire data_path0_n_103;
  wire data_path0_n_104;
  wire data_path0_n_105;
  wire data_path0_n_106;
  wire data_path0_n_107;
  wire data_path0_n_108;
  wire data_path0_n_109;
  wire data_path0_n_11;
  wire data_path0_n_110;
  wire data_path0_n_111;
  wire data_path0_n_112;
  wire data_path0_n_113;
  wire data_path0_n_114;
  wire data_path0_n_115;
  wire data_path0_n_116;
  wire data_path0_n_117;
  wire data_path0_n_118;
  wire data_path0_n_119;
  wire data_path0_n_12;
  wire data_path0_n_120;
  wire data_path0_n_121;
  wire data_path0_n_122;
  wire data_path0_n_13;
  wire data_path0_n_14;
  wire data_path0_n_15;
  wire data_path0_n_16;
  wire data_path0_n_17;
  wire data_path0_n_18;
  wire data_path0_n_19;
  wire data_path0_n_2;
  wire data_path0_n_20;
  wire data_path0_n_21;
  wire data_path0_n_22;
  wire data_path0_n_23;
  wire data_path0_n_3;
  wire data_path0_n_4;
  wire data_path0_n_49;
  wire data_path0_n_5;
  wire data_path0_n_50;
  wire data_path0_n_51;
  wire data_path0_n_52;
  wire data_path0_n_53;
  wire data_path0_n_54;
  wire data_path0_n_55;
  wire data_path0_n_56;
  wire data_path0_n_57;
  wire data_path0_n_58;
  wire data_path0_n_59;
  wire data_path0_n_6;
  wire data_path0_n_60;
  wire data_path0_n_61;
  wire data_path0_n_62;
  wire data_path0_n_63;
  wire data_path0_n_7;
  wire data_path0_n_8;
  wire data_path0_n_89;
  wire data_path0_n_9;
  wire data_path0_n_90;
  wire data_path0_n_91;
  wire data_path0_n_92;
  wire data_path0_n_93;
  wire data_path0_n_94;
  wire data_path0_n_95;
  wire data_path0_n_96;
  wire data_path0_n_97;
  wire data_path0_n_98;
  wire data_path0_n_99;
  wire done;
  wire done_OBUF;
  wire en_reg;
  wire [0:0]\fsm0/state ;
  wire rst;
  wire rst_IBUF;
  wire [24:0]sin;
  wire [24:0]sin_OBUF;
  wire start;
  wire start_IBUF;

initial begin
 $sdf_annotate("tb_cordic_rotation_time_synth.sdf",,,,"tool_control");
end
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[0]_inst 
       (.I(angle[0]),
        .O(angle_IBUF[0]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[10]_inst 
       (.I(angle[10]),
        .O(angle_IBUF[10]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[11]_inst 
       (.I(angle[11]),
        .O(angle_IBUF[11]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[12]_inst 
       (.I(angle[12]),
        .O(angle_IBUF[12]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[13]_inst 
       (.I(angle[13]),
        .O(angle_IBUF[13]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[14]_inst 
       (.I(angle[14]),
        .O(angle_IBUF[14]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[15]_inst 
       (.I(angle[15]),
        .O(angle_IBUF[15]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[16]_inst 
       (.I(angle[16]),
        .O(angle_IBUF[16]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[17]_inst 
       (.I(angle[17]),
        .O(angle_IBUF[17]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[18]_inst 
       (.I(angle[18]),
        .O(angle_IBUF[18]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[19]_inst 
       (.I(angle[19]),
        .O(angle_IBUF[19]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[1]_inst 
       (.I(angle[1]),
        .O(angle_IBUF[1]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[20]_inst 
       (.I(angle[20]),
        .O(angle_IBUF[20]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[21]_inst 
       (.I(angle[21]),
        .O(angle_IBUF[21]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[22]_inst 
       (.I(angle[22]),
        .O(angle_IBUF[22]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[23]_inst 
       (.I(angle[23]),
        .O(angle_IBUF[23]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[2]_inst 
       (.I(angle[2]),
        .O(angle_IBUF[2]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[3]_inst 
       (.I(angle[3]),
        .O(angle_IBUF[3]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[4]_inst 
       (.I(angle[4]),
        .O(angle_IBUF[4]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[5]_inst 
       (.I(angle[5]),
        .O(angle_IBUF[5]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[6]_inst 
       (.I(angle[6]),
        .O(angle_IBUF[6]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[7]_inst 
       (.I(angle[7]),
        .O(angle_IBUF[7]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[8]_inst 
       (.I(angle[8]),
        .O(angle_IBUF[8]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \angle_IBUF[9]_inst 
       (.I(angle[9]),
        .O(angle_IBUF[9]));
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF #(
    .CCIO_EN("TRUE")) 
    clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  control_path control_path0
       (.AR(rst_IBUF),
        .CLK(clk_IBUF_BUFG),
        .D({control_path0_n_16,control_path0_n_17,control_path0_n_18,control_path0_n_19,control_path0_n_20,control_path0_n_21,control_path0_n_22,control_path0_n_23,control_path0_n_24,control_path0_n_25,control_path0_n_26,control_path0_n_27,control_path0_n_28,control_path0_n_29,control_path0_n_30,control_path0_n_31,control_path0_n_32,control_path0_n_33,control_path0_n_34,control_path0_n_35,control_path0_n_36,control_path0_n_37,control_path0_n_38,control_path0_n_39}),
        .E(en_reg),
        .O({data_path0_n_20,data_path0_n_21,data_path0_n_22,data_path0_n_23}),
        .Q(\fsm0/state ),
        .S({control_path0_n_40,control_path0_n_41}),
        .angle_IBUF(angle_IBUF),
        .done_OBUF(done_OBUF),
        .\q_reg[11] ({control_path0_n_50,control_path0_n_51}),
        .\q_reg[11]_0 ({data_path0_n_8,data_path0_n_9,data_path0_n_10,data_path0_n_11}),
        .\q_reg[14] ({control_path0_n_42,control_path0_n_43,control_path0_n_44}),
        .\q_reg[14]_0 ({control_path0_n_52,control_path0_n_53,control_path0_n_54}),
        .\q_reg[15] ({data_path0_n_12,data_path0_n_13,data_path0_n_14,data_path0_n_15}),
        .\q_reg[19] ({control_path0_n_45,control_path0_n_46}),
        .\q_reg[19]_0 ({control_path0_n_55,control_path0_n_56}),
        .\q_reg[19]_1 ({data_path0_n_16,data_path0_n_17,data_path0_n_18,data_path0_n_19}),
        .\q_reg[1] (control_path0_n_7),
        .\q_reg[1]_0 (control_path0_n_9),
        .\q_reg[1]_1 (control_path0_n_10),
        .\q_reg[1]_2 (control_path0_n_11),
        .\q_reg[1]_3 (control_path0_n_13),
        .\q_reg[1]_4 (control_path0_n_14),
        .\q_reg[22] ({control_path0_n_47,control_path0_n_48,control_path0_n_49}),
        .\q_reg[22]_0 ({control_path0_n_57,control_path0_n_58,control_path0_n_59}),
        .\q_reg[2] (control_path0_n_8),
        .\q_reg[2]_0 (control_path0_n_12),
        .\q_reg[3] ({data_path0_n_0,data_path0_n_1,data_path0_n_2,data_path0_n_3}),
        .\q_reg[4] (addr),
        .\q_reg[7] ({data_path0_n_4,data_path0_n_5,data_path0_n_6,data_path0_n_7}),
        .start_IBUF(start_IBUF),
        .y0_carry__1(data_path0_n_49),
        .y0_carry__1_i_2__0(data_path0_n_105),
        .y0_carry__1_i_2__0_0(data_path0_n_103),
        .y0_carry__1_i_2__0_1(data_path0_n_104),
        .y0_carry__1_i_2__1(data_path0_n_115),
        .y0_carry__1_i_2__1_0(data_path0_n_113),
        .y0_carry__1_i_2__1_1(data_path0_n_114),
        .y0_carry__1_i_3__0(data_path0_n_110),
        .y0_carry__1_i_3__0_0(data_path0_n_108),
        .y0_carry__1_i_3__0_1(data_path0_n_109),
        .y0_carry__1_i_3__1(data_path0_n_120),
        .y0_carry__1_i_3__1_0(data_path0_n_118),
        .y0_carry__1_i_3__1_1(data_path0_n_119),
        .y0_carry__2_i_1__0(data_path0_n_107),
        .y0_carry__2_i_1__1(data_path0_n_117),
        .y0_carry__2_i_3__0(data_path0_n_112),
        .y0_carry__2_i_3__1(data_path0_n_122),
        .y0_carry__3_i_2__0(data_path0_n_106),
        .y0_carry__3_i_2__1(data_path0_n_116),
        .y0_carry__3_i_3__0(data_path0_n_111),
        .y0_carry__3_i_3__1(data_path0_n_121),
        .y0_carry__4_i_2__0({data_path0_n_89,data_path0_n_90,data_path0_n_91,data_path0_n_92,data_path0_n_93,data_path0_n_94,data_path0_n_95,data_path0_n_96,data_path0_n_97,data_path0_n_98,data_path0_n_99,data_path0_n_100,data_path0_n_101,data_path0_n_102}),
        .y0_carry__4_i_2__1({data_path0_n_50,data_path0_n_51,data_path0_n_52,data_path0_n_53,data_path0_n_54,data_path0_n_55,data_path0_n_56,data_path0_n_57,data_path0_n_58,data_path0_n_59,data_path0_n_60,data_path0_n_61,data_path0_n_62,data_path0_n_63}));
  OBUF \cos_OBUF[0]_inst 
       (.I(cos_OBUF[0]),
        .O(cos[0]));
  OBUF \cos_OBUF[10]_inst 
       (.I(cos_OBUF[10]),
        .O(cos[10]));
  OBUF \cos_OBUF[11]_inst 
       (.I(cos_OBUF[11]),
        .O(cos[11]));
  OBUF \cos_OBUF[12]_inst 
       (.I(cos_OBUF[12]),
        .O(cos[12]));
  OBUF \cos_OBUF[13]_inst 
       (.I(cos_OBUF[13]),
        .O(cos[13]));
  OBUF \cos_OBUF[14]_inst 
       (.I(cos_OBUF[14]),
        .O(cos[14]));
  OBUF \cos_OBUF[15]_inst 
       (.I(cos_OBUF[15]),
        .O(cos[15]));
  OBUF \cos_OBUF[16]_inst 
       (.I(cos_OBUF[16]),
        .O(cos[16]));
  OBUF \cos_OBUF[17]_inst 
       (.I(cos_OBUF[17]),
        .O(cos[17]));
  OBUF \cos_OBUF[18]_inst 
       (.I(cos_OBUF[18]),
        .O(cos[18]));
  OBUF \cos_OBUF[19]_inst 
       (.I(cos_OBUF[19]),
        .O(cos[19]));
  OBUF \cos_OBUF[1]_inst 
       (.I(cos_OBUF[1]),
        .O(cos[1]));
  OBUF \cos_OBUF[20]_inst 
       (.I(cos_OBUF[20]),
        .O(cos[20]));
  OBUF \cos_OBUF[21]_inst 
       (.I(cos_OBUF[21]),
        .O(cos[21]));
  OBUF \cos_OBUF[22]_inst 
       (.I(cos_OBUF[22]),
        .O(cos[22]));
  OBUF \cos_OBUF[23]_inst 
       (.I(cos_OBUF[23]),
        .O(cos[23]));
  OBUF \cos_OBUF[24]_inst 
       (.I(cos_OBUF[24]),
        .O(cos[24]));
  OBUF \cos_OBUF[2]_inst 
       (.I(cos_OBUF[2]),
        .O(cos[2]));
  OBUF \cos_OBUF[3]_inst 
       (.I(cos_OBUF[3]),
        .O(cos[3]));
  OBUF \cos_OBUF[4]_inst 
       (.I(cos_OBUF[4]),
        .O(cos[4]));
  OBUF \cos_OBUF[5]_inst 
       (.I(cos_OBUF[5]),
        .O(cos[5]));
  OBUF \cos_OBUF[6]_inst 
       (.I(cos_OBUF[6]),
        .O(cos[6]));
  OBUF \cos_OBUF[7]_inst 
       (.I(cos_OBUF[7]),
        .O(cos[7]));
  OBUF \cos_OBUF[8]_inst 
       (.I(cos_OBUF[8]),
        .O(cos[8]));
  OBUF \cos_OBUF[9]_inst 
       (.I(cos_OBUF[9]),
        .O(cos[9]));
  data_path data_path0
       (.AR(rst_IBUF),
        .CLK(clk_IBUF_BUFG),
        .D({control_path0_n_16,control_path0_n_17,control_path0_n_18,control_path0_n_19,control_path0_n_20,control_path0_n_21,control_path0_n_22,control_path0_n_23,control_path0_n_24,control_path0_n_25,control_path0_n_26,control_path0_n_27,control_path0_n_28,control_path0_n_29,control_path0_n_30,control_path0_n_31,control_path0_n_32,control_path0_n_33,control_path0_n_34,control_path0_n_35,control_path0_n_36,control_path0_n_37,control_path0_n_38,control_path0_n_39}),
        .E(en_reg),
        .O({data_path0_n_20,data_path0_n_21,data_path0_n_22,data_path0_n_23}),
        .Q(data_path0_n_49),
        .S({control_path0_n_40,control_path0_n_41}),
        .cos_OBUF(cos_OBUF),
        .data_reg(addr),
        .\q_reg[0] (\fsm0/state ),
        .\q_reg[11] ({data_path0_n_8,data_path0_n_9,data_path0_n_10,data_path0_n_11}),
        .\q_reg[11]_0 ({control_path0_n_50,control_path0_n_51}),
        .\q_reg[15] ({data_path0_n_12,data_path0_n_13,data_path0_n_14,data_path0_n_15}),
        .\q_reg[15]_0 ({control_path0_n_42,control_path0_n_43,control_path0_n_44}),
        .\q_reg[15]_1 ({control_path0_n_52,control_path0_n_53,control_path0_n_54}),
        .\q_reg[18] (data_path0_n_109),
        .\q_reg[18]_0 (data_path0_n_119),
        .\q_reg[19] ({data_path0_n_16,data_path0_n_17,data_path0_n_18,data_path0_n_19}),
        .\q_reg[19]_0 (data_path0_n_104),
        .\q_reg[19]_1 (data_path0_n_114),
        .\q_reg[19]_2 ({control_path0_n_45,control_path0_n_46}),
        .\q_reg[19]_3 ({control_path0_n_55,control_path0_n_56}),
        .\q_reg[20] (data_path0_n_110),
        .\q_reg[20]_0 (data_path0_n_112),
        .\q_reg[20]_1 (data_path0_n_120),
        .\q_reg[20]_2 (data_path0_n_122),
        .\q_reg[21] (data_path0_n_105),
        .\q_reg[21]_0 (data_path0_n_107),
        .\q_reg[21]_1 (data_path0_n_115),
        .\q_reg[21]_2 (data_path0_n_117),
        .\q_reg[22] (data_path0_n_108),
        .\q_reg[22]_0 (data_path0_n_111),
        .\q_reg[22]_1 (data_path0_n_118),
        .\q_reg[22]_2 (data_path0_n_121),
        .\q_reg[23] (data_path0_n_103),
        .\q_reg[23]_0 (data_path0_n_106),
        .\q_reg[23]_1 (data_path0_n_113),
        .\q_reg[23]_2 (data_path0_n_116),
        .\q_reg[23]_3 ({control_path0_n_47,control_path0_n_48,control_path0_n_49}),
        .\q_reg[23]_4 ({control_path0_n_57,control_path0_n_58,control_path0_n_59}),
        .\q_reg[24] ({data_path0_n_50,data_path0_n_51,data_path0_n_52,data_path0_n_53,data_path0_n_54,data_path0_n_55,data_path0_n_56,data_path0_n_57,data_path0_n_58,data_path0_n_59,data_path0_n_60,data_path0_n_61,data_path0_n_62,data_path0_n_63}),
        .\q_reg[24]_0 ({data_path0_n_89,data_path0_n_90,data_path0_n_91,data_path0_n_92,data_path0_n_93,data_path0_n_94,data_path0_n_95,data_path0_n_96,data_path0_n_97,data_path0_n_98,data_path0_n_99,data_path0_n_100,data_path0_n_101,data_path0_n_102}),
        .\q_reg[3] ({data_path0_n_0,data_path0_n_1,data_path0_n_2,data_path0_n_3}),
        .\q_reg[7] ({data_path0_n_4,data_path0_n_5,data_path0_n_6,data_path0_n_7}),
        .sin_OBUF(sin_OBUF),
        .start_IBUF(start_IBUF),
        .y0_carry__1(control_path0_n_9),
        .y0_carry__1_0(control_path0_n_13),
        .y0_carry__2(control_path0_n_7),
        .y0_carry__2_0(control_path0_n_11),
        .y0_carry__3(control_path0_n_10),
        .y0_carry__3_0(control_path0_n_14),
        .y0_carry__4(control_path0_n_8),
        .y0_carry__4_0(control_path0_n_12));
  OBUF done_OBUF_inst
       (.I(done_OBUF),
        .O(done));
  IBUF #(
    .CCIO_EN("TRUE")) 
    rst_IBUF_inst
       (.I(rst),
        .O(rst_IBUF));
  OBUF \sin_OBUF[0]_inst 
       (.I(sin_OBUF[0]),
        .O(sin[0]));
  OBUF \sin_OBUF[10]_inst 
       (.I(sin_OBUF[10]),
        .O(sin[10]));
  OBUF \sin_OBUF[11]_inst 
       (.I(sin_OBUF[11]),
        .O(sin[11]));
  OBUF \sin_OBUF[12]_inst 
       (.I(sin_OBUF[12]),
        .O(sin[12]));
  OBUF \sin_OBUF[13]_inst 
       (.I(sin_OBUF[13]),
        .O(sin[13]));
  OBUF \sin_OBUF[14]_inst 
       (.I(sin_OBUF[14]),
        .O(sin[14]));
  OBUF \sin_OBUF[15]_inst 
       (.I(sin_OBUF[15]),
        .O(sin[15]));
  OBUF \sin_OBUF[16]_inst 
       (.I(sin_OBUF[16]),
        .O(sin[16]));
  OBUF \sin_OBUF[17]_inst 
       (.I(sin_OBUF[17]),
        .O(sin[17]));
  OBUF \sin_OBUF[18]_inst 
       (.I(sin_OBUF[18]),
        .O(sin[18]));
  OBUF \sin_OBUF[19]_inst 
       (.I(sin_OBUF[19]),
        .O(sin[19]));
  OBUF \sin_OBUF[1]_inst 
       (.I(sin_OBUF[1]),
        .O(sin[1]));
  OBUF \sin_OBUF[20]_inst 
       (.I(sin_OBUF[20]),
        .O(sin[20]));
  OBUF \sin_OBUF[21]_inst 
       (.I(sin_OBUF[21]),
        .O(sin[21]));
  OBUF \sin_OBUF[22]_inst 
       (.I(sin_OBUF[22]),
        .O(sin[22]));
  OBUF \sin_OBUF[23]_inst 
       (.I(sin_OBUF[23]),
        .O(sin[23]));
  OBUF \sin_OBUF[24]_inst 
       (.I(sin_OBUF[24]),
        .O(sin[24]));
  OBUF \sin_OBUF[2]_inst 
       (.I(sin_OBUF[2]),
        .O(sin[2]));
  OBUF \sin_OBUF[3]_inst 
       (.I(sin_OBUF[3]),
        .O(sin[3]));
  OBUF \sin_OBUF[4]_inst 
       (.I(sin_OBUF[4]),
        .O(sin[4]));
  OBUF \sin_OBUF[5]_inst 
       (.I(sin_OBUF[5]),
        .O(sin[5]));
  OBUF \sin_OBUF[6]_inst 
       (.I(sin_OBUF[6]),
        .O(sin[6]));
  OBUF \sin_OBUF[7]_inst 
       (.I(sin_OBUF[7]),
        .O(sin[7]));
  OBUF \sin_OBUF[8]_inst 
       (.I(sin_OBUF[8]),
        .O(sin[8]));
  OBUF \sin_OBUF[9]_inst 
       (.I(sin_OBUF[9]),
        .O(sin[9]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    start_IBUF_inst
       (.I(start),
        .O(start_IBUF));
endmodule

module data_path
   (\q_reg[3] ,
    \q_reg[7] ,
    \q_reg[11] ,
    \q_reg[15] ,
    \q_reg[19] ,
    O,
    sin_OBUF,
    Q,
    \q_reg[24] ,
    cos_OBUF,
    \q_reg[24]_0 ,
    \q_reg[23] ,
    \q_reg[19]_0 ,
    \q_reg[21] ,
    \q_reg[23]_0 ,
    \q_reg[21]_0 ,
    \q_reg[22] ,
    \q_reg[18] ,
    \q_reg[20] ,
    \q_reg[22]_0 ,
    \q_reg[20]_0 ,
    \q_reg[23]_1 ,
    \q_reg[19]_1 ,
    \q_reg[21]_1 ,
    \q_reg[23]_2 ,
    \q_reg[21]_2 ,
    \q_reg[22]_1 ,
    \q_reg[18]_0 ,
    \q_reg[20]_1 ,
    \q_reg[22]_2 ,
    \q_reg[20]_2 ,
    CLK,
    data_reg,
    S,
    \q_reg[15]_0 ,
    \q_reg[19]_2 ,
    \q_reg[23]_3 ,
    \q_reg[11]_0 ,
    \q_reg[15]_1 ,
    \q_reg[19]_3 ,
    \q_reg[23]_4 ,
    start_IBUF,
    \q_reg[0] ,
    y0_carry__1,
    y0_carry__2,
    y0_carry__3,
    y0_carry__4,
    y0_carry__1_0,
    y0_carry__2_0,
    y0_carry__3_0,
    y0_carry__4_0,
    E,
    AR,
    D);
  output [3:0]\q_reg[3] ;
  output [3:0]\q_reg[7] ;
  output [3:0]\q_reg[11] ;
  output [3:0]\q_reg[15] ;
  output [3:0]\q_reg[19] ;
  output [3:0]O;
  output [24:0]sin_OBUF;
  output [0:0]Q;
  output [13:0]\q_reg[24] ;
  output [24:0]cos_OBUF;
  output [13:0]\q_reg[24]_0 ;
  output \q_reg[23] ;
  output \q_reg[19]_0 ;
  output \q_reg[21] ;
  output \q_reg[23]_0 ;
  output \q_reg[21]_0 ;
  output \q_reg[22] ;
  output \q_reg[18] ;
  output \q_reg[20] ;
  output \q_reg[22]_0 ;
  output \q_reg[20]_0 ;
  output \q_reg[23]_1 ;
  output \q_reg[19]_1 ;
  output \q_reg[21]_1 ;
  output \q_reg[23]_2 ;
  output \q_reg[21]_2 ;
  output \q_reg[22]_1 ;
  output \q_reg[18]_0 ;
  output \q_reg[20]_1 ;
  output \q_reg[22]_2 ;
  output \q_reg[20]_2 ;
  input CLK;
  input [4:0]data_reg;
  input [1:0]S;
  input [2:0]\q_reg[15]_0 ;
  input [1:0]\q_reg[19]_2 ;
  input [2:0]\q_reg[23]_3 ;
  input [1:0]\q_reg[11]_0 ;
  input [2:0]\q_reg[15]_1 ;
  input [1:0]\q_reg[19]_3 ;
  input [2:0]\q_reg[23]_4 ;
  input start_IBUF;
  input [0:0]\q_reg[0] ;
  input y0_carry__1;
  input y0_carry__2;
  input y0_carry__3;
  input y0_carry__4;
  input y0_carry__1_0;
  input y0_carry__2_0;
  input y0_carry__3_0;
  input y0_carry__4_0;
  input [0:0]E;
  input [0:0]AR;
  input [23:0]D;

  wire [0:0]AR;
  wire CLK;
  wire [23:0]D;
  wire [0:0]E;
  wire Mux_Y_n_25;
  wire Mux_Y_n_26;
  wire Mux_Y_n_27;
  wire Mux_Y_n_28;
  wire Mux_Y_n_29;
  wire Mux_Y_n_30;
  wire Mux_Y_n_31;
  wire Mux_Y_n_32;
  wire Mux_Y_n_33;
  wire Mux_Y_n_34;
  wire Mux_Y_n_35;
  wire Mux_Y_n_36;
  wire Mux_Y_n_37;
  wire Mux_Y_n_38;
  wire Mux_Y_n_39;
  wire Mux_Y_n_40;
  wire Mux_Y_n_41;
  wire Mux_Y_n_42;
  wire Mux_Y_n_43;
  wire Mux_Y_n_44;
  wire Mux_Y_n_45;
  wire Mux_Y_n_46;
  wire Mux_Y_n_47;
  wire Mux_Y_n_48;
  wire Mux_Y_n_49;
  wire [3:0]O;
  wire [0:0]Q;
  wire ROM_1_n_23;
  wire [1:0]S;
  wire [24:0]cos_OBUF;
  wire [4:0]data_reg;
  wire [22:0]out_ROM;
  wire [24:0]p_0_in;
  wire [0:0]\q_reg[0] ;
  wire [3:0]\q_reg[11] ;
  wire [1:0]\q_reg[11]_0 ;
  wire [3:0]\q_reg[15] ;
  wire [2:0]\q_reg[15]_0 ;
  wire [2:0]\q_reg[15]_1 ;
  wire \q_reg[18] ;
  wire \q_reg[18]_0 ;
  wire [3:0]\q_reg[19] ;
  wire \q_reg[19]_0 ;
  wire \q_reg[19]_1 ;
  wire [1:0]\q_reg[19]_2 ;
  wire [1:0]\q_reg[19]_3 ;
  wire \q_reg[20] ;
  wire \q_reg[20]_0 ;
  wire \q_reg[20]_1 ;
  wire \q_reg[20]_2 ;
  wire \q_reg[21] ;
  wire \q_reg[21]_0 ;
  wire \q_reg[21]_1 ;
  wire \q_reg[21]_2 ;
  wire \q_reg[22] ;
  wire \q_reg[22]_0 ;
  wire \q_reg[22]_1 ;
  wire \q_reg[22]_2 ;
  wire \q_reg[23] ;
  wire \q_reg[23]_0 ;
  wire \q_reg[23]_1 ;
  wire \q_reg[23]_2 ;
  wire [2:0]\q_reg[23]_3 ;
  wire [2:0]\q_reg[23]_4 ;
  wire [13:0]\q_reg[24] ;
  wire [13:0]\q_reg[24]_0 ;
  wire [3:0]\q_reg[3] ;
  wire [3:0]\q_reg[7] ;
  wire reg_X_n_0;
  wire reg_X_n_13;
  wire reg_X_n_19;
  wire reg_X_n_20;
  wire reg_X_n_21;
  wire reg_X_n_22;
  wire reg_X_n_23;
  wire reg_X_n_24;
  wire reg_X_n_25;
  wire reg_X_n_26;
  wire reg_X_n_27;
  wire reg_X_n_28;
  wire reg_X_n_29;
  wire reg_X_n_35;
  wire reg_X_n_38;
  wire reg_X_n_39;
  wire reg_X_n_40;
  wire reg_X_n_41;
  wire reg_X_n_42;
  wire reg_X_n_43;
  wire reg_X_n_44;
  wire reg_X_n_45;
  wire reg_X_n_46;
  wire reg_X_n_47;
  wire reg_X_n_48;
  wire reg_X_n_49;
  wire reg_X_n_50;
  wire reg_X_n_51;
  wire reg_X_n_52;
  wire reg_Y_n_0;
  wire reg_Y_n_13;
  wire reg_Y_n_19;
  wire reg_Y_n_20;
  wire reg_Y_n_21;
  wire reg_Y_n_22;
  wire reg_Y_n_23;
  wire reg_Y_n_24;
  wire reg_Y_n_25;
  wire reg_Y_n_26;
  wire reg_Y_n_27;
  wire reg_Y_n_28;
  wire reg_Y_n_29;
  wire reg_Y_n_35;
  wire reg_Y_n_38;
  wire reg_Y_n_39;
  wire reg_Y_n_40;
  wire reg_Y_n_41;
  wire reg_Y_n_42;
  wire reg_Y_n_43;
  wire reg_Y_n_44;
  wire reg_Y_n_45;
  wire reg_Y_n_46;
  wire reg_Y_n_47;
  wire reg_Y_n_48;
  wire reg_Y_n_49;
  wire reg_Y_n_50;
  wire reg_Y_n_51;
  wire reg_Z_n_0;
  wire reg_Z_n_10;
  wire reg_Z_n_11;
  wire reg_Z_n_12;
  wire reg_Z_n_13;
  wire reg_Z_n_14;
  wire reg_Z_n_15;
  wire reg_Z_n_16;
  wire reg_Z_n_17;
  wire reg_Z_n_18;
  wire reg_Z_n_19;
  wire reg_Z_n_2;
  wire reg_Z_n_20;
  wire reg_Z_n_21;
  wire reg_Z_n_22;
  wire reg_Z_n_23;
  wire reg_Z_n_24;
  wire reg_Z_n_25;
  wire reg_Z_n_26;
  wire reg_Z_n_27;
  wire reg_Z_n_28;
  wire reg_Z_n_29;
  wire reg_Z_n_3;
  wire reg_Z_n_30;
  wire reg_Z_n_31;
  wire reg_Z_n_32;
  wire reg_Z_n_33;
  wire reg_Z_n_34;
  wire reg_Z_n_35;
  wire reg_Z_n_36;
  wire reg_Z_n_37;
  wire reg_Z_n_38;
  wire reg_Z_n_39;
  wire reg_Z_n_4;
  wire reg_Z_n_40;
  wire reg_Z_n_41;
  wire reg_Z_n_42;
  wire reg_Z_n_43;
  wire reg_Z_n_44;
  wire reg_Z_n_45;
  wire reg_Z_n_46;
  wire reg_Z_n_47;
  wire reg_Z_n_48;
  wire reg_Z_n_5;
  wire reg_Z_n_6;
  wire reg_Z_n_7;
  wire reg_Z_n_8;
  wire reg_Z_n_9;
  wire [24:0]sin_OBUF;
  wire start_IBUF;
  wire y0_carry__1;
  wire y0_carry__1_0;
  wire y0_carry__2;
  wire y0_carry__2_0;
  wire y0_carry__3;
  wire y0_carry__3_0;
  wire y0_carry__4;
  wire y0_carry__4_0;

  Mux_2_1 Mux_X
       (.D(p_0_in),
        .Q({\q_reg[24]_0 [12:5],reg_X_n_13,\q_reg[24]_0 [4:0],reg_X_n_19,reg_X_n_20,reg_X_n_21,reg_X_n_22,reg_X_n_23,reg_X_n_24,reg_X_n_25,reg_X_n_26,reg_X_n_27,reg_X_n_28}),
        .S({reg_Y_n_42,reg_Y_n_43,reg_Y_n_44,reg_Y_n_45}),
        .cos_OBUF(cos_OBUF),
        .\q_reg[0] (\q_reg[0] ),
        .\q_reg[11] ({\q_reg[11]_0 ,reg_X_n_49,reg_Y_n_50}),
        .\q_reg[15] ({reg_X_n_50,\q_reg[15]_1 }),
        .\q_reg[19] ({\q_reg[19]_3 ,reg_X_n_51,reg_Y_n_51}),
        .\q_reg[23] ({reg_X_n_52,\q_reg[23]_4 }),
        .\q_reg[24] (reg_X_n_38),
        .\q_reg[3] (reg_Z_n_0),
        .\q_reg[7] ({reg_Y_n_46,reg_Y_n_47,reg_Y_n_48,reg_Y_n_49}),
        .start_IBUF(start_IBUF));
  Mux_2_1_0 Mux_Y
       (.D({Mux_Y_n_25,Mux_Y_n_26,Mux_Y_n_27,Mux_Y_n_28,Mux_Y_n_29,Mux_Y_n_30,Mux_Y_n_31,Mux_Y_n_32,Mux_Y_n_33,Mux_Y_n_34,Mux_Y_n_35,Mux_Y_n_36,Mux_Y_n_37,Mux_Y_n_38,Mux_Y_n_39,Mux_Y_n_40,Mux_Y_n_41,Mux_Y_n_42,Mux_Y_n_43,Mux_Y_n_44,Mux_Y_n_45,Mux_Y_n_46,Mux_Y_n_47,Mux_Y_n_48,Mux_Y_n_49}),
        .Q(Q),
        .S({reg_X_n_39,reg_X_n_40,reg_X_n_41,reg_X_n_42}),
        .\q_reg[0] (\q_reg[0] ),
        .\q_reg[11] ({S,reg_Y_n_38,reg_X_n_47}),
        .\q_reg[15] ({reg_Y_n_39,\q_reg[15]_0 }),
        .\q_reg[19] ({\q_reg[19]_2 ,reg_Y_n_40,reg_X_n_48}),
        .\q_reg[23] ({\q_reg[24] [12:5],reg_Y_n_13,\q_reg[24] [4:0],reg_Y_n_19,reg_Y_n_20,reg_Y_n_21,reg_Y_n_22,reg_Y_n_23,reg_Y_n_24,reg_Y_n_25,reg_Y_n_26,reg_Y_n_27,reg_Y_n_28}),
        .\q_reg[23]_0 ({reg_Y_n_41,\q_reg[23]_3 }),
        .\q_reg[24] (reg_Z_n_25),
        .\q_reg[7] ({reg_X_n_43,reg_X_n_44,reg_X_n_45,reg_X_n_46}),
        .sin_OBUF(sin_OBUF),
        .start_IBUF(start_IBUF));
  Mux_2_1__parameterized2 Mux_Z
       (.O(O),
        .Q({reg_Z_n_2,reg_Z_n_3,reg_Z_n_4,reg_Z_n_5,reg_Z_n_6,reg_Z_n_7,reg_Z_n_8,reg_Z_n_9,reg_Z_n_10,reg_Z_n_11,reg_Z_n_12,reg_Z_n_13,reg_Z_n_14,reg_Z_n_15,reg_Z_n_16,reg_Z_n_17,reg_Z_n_18,reg_Z_n_19,reg_Z_n_20,reg_Z_n_21,reg_Z_n_22,reg_Z_n_23,reg_Z_n_24}),
        .S({reg_Z_n_26,reg_Z_n_27,reg_Z_n_28,reg_Z_n_29}),
        .\q_reg[11] (\q_reg[11] ),
        .\q_reg[11]_0 ({reg_Z_n_34,reg_Z_n_35,reg_Z_n_36,reg_Z_n_37}),
        .\q_reg[15] (\q_reg[15] ),
        .\q_reg[15]_0 ({reg_Z_n_38,reg_Z_n_39,reg_Z_n_40,reg_Z_n_41}),
        .\q_reg[19] (\q_reg[19] ),
        .\q_reg[19]_0 ({reg_Z_n_42,reg_Z_n_43,reg_Z_n_44,reg_Z_n_45}),
        .\q_reg[23] ({ROM_1_n_23,reg_Z_n_46,reg_Z_n_47,reg_Z_n_48}),
        .\q_reg[3] (\q_reg[3] ),
        .\q_reg[3]_0 (reg_Z_n_0),
        .\q_reg[7] (\q_reg[7] ),
        .\q_reg[7]_0 ({reg_Z_n_30,reg_Z_n_31,reg_Z_n_32,reg_Z_n_33}));
  ROM ROM_1
       (.CLK(CLK),
        .data_reg_0(ROM_1_n_23),
        .data_reg_1(data_reg),
        .out_ROM(out_ROM));
  reg__parameterized1 reg_X
       (.AR(AR),
        .CLK(CLK),
        .D(p_0_in),
        .E(E),
        .Q({\q_reg[24]_0 [13:5],reg_X_n_13,\q_reg[24]_0 [4:0],reg_X_n_19,reg_X_n_20,reg_X_n_21,reg_X_n_22,reg_X_n_23,reg_X_n_24,reg_X_n_25,reg_X_n_26,reg_X_n_27,reg_X_n_28}),
        .S({reg_X_n_39,reg_X_n_40,reg_X_n_41,reg_X_n_42}),
        .\q_reg[15]_0 (reg_X_n_50),
        .\q_reg[16]_0 (reg_X_n_48),
        .\q_reg[17]_0 (reg_X_n_51),
        .\q_reg[18]_0 (\q_reg[18] ),
        .\q_reg[19]_0 (\q_reg[19]_0 ),
        .\q_reg[1]_0 (reg_X_n_0),
        .\q_reg[1]_1 (reg_X_n_29),
        .\q_reg[1]_2 (reg_X_n_35),
        .\q_reg[20]_0 (\q_reg[20] ),
        .\q_reg[20]_1 (\q_reg[20]_0 ),
        .\q_reg[21]_0 (\q_reg[21] ),
        .\q_reg[21]_1 (\q_reg[21]_0 ),
        .\q_reg[22]_0 (\q_reg[22] ),
        .\q_reg[22]_1 (\q_reg[22]_0 ),
        .\q_reg[23]_0 (\q_reg[23] ),
        .\q_reg[23]_1 (\q_reg[23]_0 ),
        .\q_reg[23]_2 (reg_X_n_52),
        .\q_reg[24]_0 (reg_X_n_38),
        .\q_reg[7]_0 ({reg_X_n_43,reg_X_n_44,reg_X_n_45,reg_X_n_46}),
        .\q_reg[8]_0 (reg_X_n_47),
        .\q_reg[9]_0 (reg_X_n_49),
        .y0_carry(Q),
        .y0_carry__1(y0_carry__1_0),
        .y0_carry__1_0(reg_Y_n_0),
        .y0_carry__1_i_6(data_reg),
        .y0_carry__2(reg_Y_n_35),
        .y0_carry__2_0(y0_carry__2_0),
        .y0_carry__3(y0_carry__3_0),
        .y0_carry__3_0(reg_Y_n_29),
        .y0_carry__4({\q_reg[24] [13],\q_reg[24] [5],reg_Y_n_20,reg_Y_n_21,reg_Y_n_22,reg_Y_n_23,reg_Y_n_24,reg_Y_n_25,reg_Y_n_26,reg_Y_n_27,reg_Y_n_28}),
        .y0_carry__4_0(y0_carry__4_0));
  reg__parameterized1_1 reg_Y
       (.AR(AR),
        .CLK(CLK),
        .D({Mux_Y_n_25,Mux_Y_n_26,Mux_Y_n_27,Mux_Y_n_28,Mux_Y_n_29,Mux_Y_n_30,Mux_Y_n_31,Mux_Y_n_32,Mux_Y_n_33,Mux_Y_n_34,Mux_Y_n_35,Mux_Y_n_36,Mux_Y_n_37,Mux_Y_n_38,Mux_Y_n_39,Mux_Y_n_40,Mux_Y_n_41,Mux_Y_n_42,Mux_Y_n_43,Mux_Y_n_44,Mux_Y_n_45,Mux_Y_n_46,Mux_Y_n_47,Mux_Y_n_48,Mux_Y_n_49}),
        .E(E),
        .Q({\q_reg[24] [13:5],reg_Y_n_13,\q_reg[24] [4:0],reg_Y_n_19,reg_Y_n_20,reg_Y_n_21,reg_Y_n_22,reg_Y_n_23,reg_Y_n_24,reg_Y_n_25,reg_Y_n_26,reg_Y_n_27,reg_Y_n_28}),
        .S({reg_Y_n_42,reg_Y_n_43,reg_Y_n_44,reg_Y_n_45}),
        .\q_reg[15]_0 (reg_Y_n_39),
        .\q_reg[16]_0 (reg_Y_n_51),
        .\q_reg[17]_0 (reg_Y_n_40),
        .\q_reg[18]_0 (\q_reg[18]_0 ),
        .\q_reg[19]_0 (\q_reg[19]_1 ),
        .\q_reg[1]_0 (reg_Y_n_0),
        .\q_reg[1]_1 (reg_Y_n_29),
        .\q_reg[1]_2 (reg_Y_n_35),
        .\q_reg[20]_0 (\q_reg[20]_1 ),
        .\q_reg[20]_1 (\q_reg[20]_2 ),
        .\q_reg[21]_0 (\q_reg[21]_1 ),
        .\q_reg[21]_1 (\q_reg[21]_2 ),
        .\q_reg[22]_0 (\q_reg[22]_1 ),
        .\q_reg[22]_1 (\q_reg[22]_2 ),
        .\q_reg[23]_0 (\q_reg[23]_1 ),
        .\q_reg[23]_1 (\q_reg[23]_2 ),
        .\q_reg[23]_2 (reg_Y_n_41),
        .\q_reg[7]_0 ({reg_Y_n_46,reg_Y_n_47,reg_Y_n_48,reg_Y_n_49}),
        .\q_reg[8]_0 (reg_Y_n_50),
        .\q_reg[9]_0 (reg_Y_n_38),
        .y0_carry__1(y0_carry__1),
        .y0_carry__1_0(reg_X_n_0),
        .y0_carry__1_1(Q),
        .y0_carry__1_i_6__0(data_reg),
        .y0_carry__2(reg_X_n_35),
        .y0_carry__2_0(y0_carry__2),
        .y0_carry__3(y0_carry__3),
        .y0_carry__3_0(reg_X_n_29),
        .y0_carry__4({\q_reg[24]_0 [13],\q_reg[24]_0 [5],reg_X_n_20,reg_X_n_21,reg_X_n_22,reg_X_n_23,reg_X_n_24,reg_X_n_25,reg_X_n_26,reg_X_n_27,reg_X_n_28}),
        .y0_carry__4_0(y0_carry__4));
  reg__parameterized3 reg_Z
       (.AR(AR),
        .CLK(CLK),
        .D(D),
        .E(E),
        .Q({Q,reg_Z_n_2,reg_Z_n_3,reg_Z_n_4,reg_Z_n_5,reg_Z_n_6,reg_Z_n_7,reg_Z_n_8,reg_Z_n_9,reg_Z_n_10,reg_Z_n_11,reg_Z_n_12,reg_Z_n_13,reg_Z_n_14,reg_Z_n_15,reg_Z_n_16,reg_Z_n_17,reg_Z_n_18,reg_Z_n_19,reg_Z_n_20,reg_Z_n_21,reg_Z_n_22,reg_Z_n_23,reg_Z_n_24}),
        .S({reg_Z_n_26,reg_Z_n_27,reg_Z_n_28,reg_Z_n_29}),
        .out_ROM(out_ROM),
        .\q_reg[11]_0 ({reg_Z_n_34,reg_Z_n_35,reg_Z_n_36,reg_Z_n_37}),
        .\q_reg[15]_0 ({reg_Z_n_38,reg_Z_n_39,reg_Z_n_40,reg_Z_n_41}),
        .\q_reg[19]_0 ({reg_Z_n_42,reg_Z_n_43,reg_Z_n_44,reg_Z_n_45}),
        .\q_reg[22]_0 ({reg_Z_n_46,reg_Z_n_47,reg_Z_n_48}),
        .\q_reg[23]_0 (reg_Z_n_0),
        .\q_reg[23]_1 (reg_Z_n_25),
        .\q_reg[7]_0 ({reg_Z_n_30,reg_Z_n_31,reg_Z_n_32,reg_Z_n_33}),
        .y0_carry__5(\q_reg[24] [13]),
        .y0_carry__5_0(\q_reg[24]_0 [13]));
endmodule

module fsm
   (Q,
    \FSM_sequential_state_reg[0]_0 ,
    E,
    D,
    start_IBUF,
    \FSM_sequential_state_reg[0]_1 ,
    angle_IBUF,
    O,
    \q_reg[19] ,
    \q_reg[15] ,
    \q_reg[11] ,
    \q_reg[7] ,
    \q_reg[3] ,
    CLK,
    AR);
  output [0:0]Q;
  output \FSM_sequential_state_reg[0]_0 ;
  output [0:0]E;
  output [23:0]D;
  input start_IBUF;
  input \FSM_sequential_state_reg[0]_1 ;
  input [23:0]angle_IBUF;
  input [3:0]O;
  input [3:0]\q_reg[19] ;
  input [3:0]\q_reg[15] ;
  input [3:0]\q_reg[11] ;
  input [3:0]\q_reg[7] ;
  input [3:0]\q_reg[3] ;
  input CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire CLK;
  wire [23:0]D;
  wire [0:0]E;
  wire \FSM_sequential_state_reg[0]_0 ;
  wire \FSM_sequential_state_reg[0]_1 ;
  wire [3:0]O;
  wire [0:0]Q;
  wire [23:0]angle_IBUF;
  wire [3:0]\q_reg[11] ;
  wire [3:0]\q_reg[15] ;
  wire [3:0]\q_reg[19] ;
  wire [3:0]\q_reg[3] ;
  wire [3:0]\q_reg[7] ;
  wire start_IBUF;
  wire [1:1]state;
  wire [1:0]state__0;

  LUT4 #(
    .INIT(16'h3202)) 
    \FSM_sequential_state[0]_i_1 
       (.I0(start_IBUF),
        .I1(Q),
        .I2(state),
        .I3(\FSM_sequential_state_reg[0]_1 ),
        .O(state__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_state[1]_i_1 
       (.I0(Q),
        .I1(state),
        .O(state__0[1]));
  (* FSM_ENCODED_STATES = "counter_state:10,start_state:00,compute_state:01" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(AR),
        .D(state__0[0]),
        .Q(Q));
  (* FSM_ENCODED_STATES = "counter_state:10,start_state:00,compute_state:01" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .CLR(AR),
        .D(state__0[1]),
        .Q(state));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[0]_i_1__1 
       (.I0(angle_IBUF[0]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[3] [0]),
        .O(D[0]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[10]_i_1__0 
       (.I0(angle_IBUF[10]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[11] [2]),
        .O(D[10]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[11]_i_1__0 
       (.I0(angle_IBUF[11]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[11] [3]),
        .O(D[11]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[12]_i_1__0 
       (.I0(angle_IBUF[12]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[15] [0]),
        .O(D[12]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[13]_i_1__0 
       (.I0(angle_IBUF[13]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[15] [1]),
        .O(D[13]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[14]_i_1__0 
       (.I0(angle_IBUF[14]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[15] [2]),
        .O(D[14]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[15]_i_1__0 
       (.I0(angle_IBUF[15]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[15] [3]),
        .O(D[15]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[16]_i_1__0 
       (.I0(angle_IBUF[16]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[19] [0]),
        .O(D[16]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[17]_i_1__0 
       (.I0(angle_IBUF[17]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[19] [1]),
        .O(D[17]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[18]_i_1__0 
       (.I0(angle_IBUF[18]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[19] [2]),
        .O(D[18]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[19]_i_1__0 
       (.I0(angle_IBUF[19]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[19] [3]),
        .O(D[19]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[1]_i_1__1 
       (.I0(angle_IBUF[1]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[3] [1]),
        .O(D[1]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[20]_i_1__0 
       (.I0(angle_IBUF[20]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(O[0]),
        .O(D[20]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[21]_i_1__0 
       (.I0(angle_IBUF[21]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(O[1]),
        .O(D[21]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[22]_i_1__0 
       (.I0(angle_IBUF[22]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(O[2]),
        .O(D[22]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[23]_i_1__0 
       (.I0(angle_IBUF[23]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(O[3]),
        .O(D[23]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[2]_i_1__1 
       (.I0(angle_IBUF[2]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[3] [2]),
        .O(D[2]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[3]_i_1__0 
       (.I0(angle_IBUF[3]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[3] [3]),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h0E)) 
    \q[4]_i_1 
       (.I0(Q),
        .I1(start_IBUF),
        .I2(state),
        .O(E));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[4]_i_1__1 
       (.I0(angle_IBUF[4]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[7] [0]),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \q[4]_i_3 
       (.I0(Q),
        .I1(start_IBUF),
        .I2(state),
        .O(\FSM_sequential_state_reg[0]_0 ));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[5]_i_1__0 
       (.I0(angle_IBUF[5]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[7] [1]),
        .O(D[5]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[6]_i_1__0 
       (.I0(angle_IBUF[6]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[7] [2]),
        .O(D[6]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[7]_i_1__0 
       (.I0(angle_IBUF[7]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[7] [3]),
        .O(D[7]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[8]_i_1__0 
       (.I0(angle_IBUF[8]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[11] [0]),
        .O(D[8]));
  LUT4 #(
    .INIT(16'hEF20)) 
    \q[9]_i_1__0 
       (.I0(angle_IBUF[9]),
        .I1(Q),
        .I2(start_IBUF),
        .I3(\q_reg[11] [1]),
        .O(D[9]));
endmodule

module \reg 
   (done_OBUF,
    Q,
    \q_reg[1]_0 ,
    \q_reg[1]_1 ,
    \q_reg[2]_0 ,
    \q_reg[1]_2 ,
    \q_reg[1]_3 ,
    \q_reg[1]_4 ,
    \q_reg[2]_1 ,
    \q_reg[1]_5 ,
    \q_reg[1]_6 ,
    S,
    \q_reg[14] ,
    \q_reg[19] ,
    \q_reg[22] ,
    \q_reg[11] ,
    \q_reg[14]_0 ,
    \q_reg[19]_0 ,
    \q_reg[22]_0 ,
    y0_carry__1_i_2__0_0,
    y0_carry__1_i_2__0_1,
    y0_carry__1_i_2__0_2,
    y0_carry__2_i_1__0,
    y0_carry__3_i_2__0_0,
    y0_carry__4_i_2__0_0,
    y0_carry__1_i_3__0,
    y0_carry__1_i_3__0_0,
    y0_carry__1_i_3__0_1,
    y0_carry__2_i_3__0_0,
    y0_carry__3_i_3__0,
    y0_carry__1_i_2__1_0,
    y0_carry__1_i_2__1_1,
    y0_carry__1_i_2__1_2,
    y0_carry__2_i_1__1,
    y0_carry__3_i_2__1_0,
    y0_carry__4_i_2__1_0,
    y0_carry__1_i_3__1,
    y0_carry__1_i_3__1_0,
    y0_carry__1_i_3__1_1,
    y0_carry__2_i_3__1_0,
    y0_carry__3_i_3__1,
    start_IBUF,
    \q_reg[0]_0 ,
    y0_carry__1,
    \q_reg[4]_0 ,
    E,
    CLK,
    AR);
  output done_OBUF;
  output [4:0]Q;
  output \q_reg[1]_0 ;
  output \q_reg[1]_1 ;
  output \q_reg[2]_0 ;
  output \q_reg[1]_2 ;
  output \q_reg[1]_3 ;
  output \q_reg[1]_4 ;
  output \q_reg[2]_1 ;
  output \q_reg[1]_5 ;
  output \q_reg[1]_6 ;
  output [1:0]S;
  output [2:0]\q_reg[14] ;
  output [1:0]\q_reg[19] ;
  output [2:0]\q_reg[22] ;
  output [1:0]\q_reg[11] ;
  output [2:0]\q_reg[14]_0 ;
  output [1:0]\q_reg[19]_0 ;
  output [2:0]\q_reg[22]_0 ;
  input y0_carry__1_i_2__0_0;
  input y0_carry__1_i_2__0_1;
  input y0_carry__1_i_2__0_2;
  input y0_carry__2_i_1__0;
  input y0_carry__3_i_2__0_0;
  input [13:0]y0_carry__4_i_2__0_0;
  input y0_carry__1_i_3__0;
  input y0_carry__1_i_3__0_0;
  input y0_carry__1_i_3__0_1;
  input y0_carry__2_i_3__0_0;
  input y0_carry__3_i_3__0;
  input y0_carry__1_i_2__1_0;
  input y0_carry__1_i_2__1_1;
  input y0_carry__1_i_2__1_2;
  input y0_carry__2_i_1__1;
  input y0_carry__3_i_2__1_0;
  input [13:0]y0_carry__4_i_2__1_0;
  input y0_carry__1_i_3__1;
  input y0_carry__1_i_3__1_0;
  input y0_carry__1_i_3__1_1;
  input y0_carry__2_i_3__1_0;
  input y0_carry__3_i_3__1;
  input start_IBUF;
  input [0:0]\q_reg[0]_0 ;
  input [0:0]y0_carry__1;
  input \q_reg[4]_0 ;
  input [0:0]E;
  input CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire CLK;
  wire [0:0]E;
  wire [4:0]Q;
  wire [1:0]S;
  wire done_OBUF;
  wire \q[0]_i_1_n_0 ;
  wire \q[1]_i_1_n_0 ;
  wire \q[2]_i_1_n_0 ;
  wire \q[3]_i_1__2_n_0 ;
  wire \q[4]_i_2_n_0 ;
  wire [0:0]\q_reg[0]_0 ;
  wire [1:0]\q_reg[11] ;
  wire [2:0]\q_reg[14] ;
  wire [2:0]\q_reg[14]_0 ;
  wire [1:0]\q_reg[19] ;
  wire [1:0]\q_reg[19]_0 ;
  wire \q_reg[1]_0 ;
  wire \q_reg[1]_1 ;
  wire \q_reg[1]_2 ;
  wire \q_reg[1]_3 ;
  wire \q_reg[1]_4 ;
  wire \q_reg[1]_5 ;
  wire \q_reg[1]_6 ;
  wire [2:0]\q_reg[22] ;
  wire [2:0]\q_reg[22]_0 ;
  wire \q_reg[2]_0 ;
  wire \q_reg[2]_1 ;
  wire \q_reg[4]_0 ;
  wire start_IBUF;
  wire [0:0]y0_carry__1;
  wire y0_carry__1_i_10__0_n_0;
  wire y0_carry__1_i_10_n_0;
  wire y0_carry__1_i_11__0_n_0;
  wire y0_carry__1_i_11_n_0;
  wire y0_carry__1_i_2__0_0;
  wire y0_carry__1_i_2__0_1;
  wire y0_carry__1_i_2__0_2;
  wire y0_carry__1_i_2__1_0;
  wire y0_carry__1_i_2__1_1;
  wire y0_carry__1_i_2__1_2;
  wire y0_carry__1_i_3__0;
  wire y0_carry__1_i_3__0_0;
  wire y0_carry__1_i_3__0_1;
  wire y0_carry__1_i_3__1;
  wire y0_carry__1_i_3__1_0;
  wire y0_carry__1_i_3__1_1;
  wire y0_carry__1_i_5__0_n_0;
  wire y0_carry__1_i_5_n_0;
  wire y0_carry__1_i_6__0_n_0;
  wire y0_carry__1_i_6_n_0;
  wire y0_carry__1_i_9__0_n_0;
  wire y0_carry__1_i_9_n_0;
  wire y0_carry__2_i_12__0_n_0;
  wire y0_carry__2_i_12_n_0;
  wire y0_carry__2_i_1__0;
  wire y0_carry__2_i_1__1;
  wire y0_carry__2_i_3__0_0;
  wire y0_carry__2_i_3__1_0;
  wire y0_carry__2_i_7__0_n_0;
  wire y0_carry__2_i_7_n_0;
  wire y0_carry__2_i_8__0_n_0;
  wire y0_carry__2_i_8_n_0;
  wire y0_carry__3_i_10__0_n_0;
  wire y0_carry__3_i_10_n_0;
  wire y0_carry__3_i_11__0_n_0;
  wire y0_carry__3_i_11_n_0;
  wire y0_carry__3_i_2__0_0;
  wire y0_carry__3_i_2__1_0;
  wire y0_carry__3_i_3__0;
  wire y0_carry__3_i_3__1;
  wire y0_carry__3_i_5__0_n_0;
  wire y0_carry__3_i_5_n_0;
  wire y0_carry__3_i_6__0_n_0;
  wire y0_carry__3_i_6_n_0;
  wire y0_carry__3_i_9__0_n_0;
  wire y0_carry__3_i_9_n_0;
  wire [13:0]y0_carry__4_i_2__0_0;
  wire [13:0]y0_carry__4_i_2__1_0;
  wire y0_carry__4_i_6__0_n_0;
  wire y0_carry__4_i_6_n_0;
  wire y0_carry__4_i_7__0_n_0;
  wire y0_carry__4_i_7_n_0;
  wire y0_carry__4_i_8__0_n_0;
  wire y0_carry__4_i_8_n_0;

  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hFFFFEFFF)) 
    \FSM_sequential_state[0]_i_2 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[4]),
        .I3(Q[3]),
        .I4(Q[2]),
        .O(\q_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h00000040)) 
    done_OBUF_inst_i_1
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[0]),
        .I4(Q[1]),
        .O(done_OBUF));
  LUT3 #(
    .INIT(8'h51)) 
    \q[0]_i_1 
       (.I0(Q[0]),
        .I1(start_IBUF),
        .I2(\q_reg[0]_0 ),
        .O(\q[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h6606)) 
    \q[1]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(start_IBUF),
        .I3(\q_reg[0]_0 ),
        .O(\q[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h78780078)) 
    \q[2]_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(start_IBUF),
        .I4(\q_reg[0]_0 ),
        .O(\q[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h15554000)) 
    \q[3]_i_1__2 
       (.I0(\q_reg[4]_0 ),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Q[3]),
        .O(\q[3]_i_1__2_n_0 ));
  LUT6 #(
    .INIT(64'h1555555540000000)) 
    \q[4]_i_2 
       (.I0(\q_reg[4]_0 ),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(Q[3]),
        .I5(Q[4]),
        .O(\q[4]_i_2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(\q[0]_i_1_n_0 ),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(\q[1]_i_1_n_0 ),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(\q[2]_i_1_n_0 ),
        .Q(Q[2]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(\q[3]_i_1__2_n_0 ),
        .Q(Q[3]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[4] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(\q[4]_i_2_n_0 ),
        .Q(Q[4]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__1_i_10
       (.I0(Q[3]),
        .I1(y0_carry__4_i_2__0_0[13]),
        .I2(Q[4]),
        .I3(y0_carry__4_i_2__0_0[5]),
        .O(y0_carry__1_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__1_i_10__0
       (.I0(Q[3]),
        .I1(y0_carry__4_i_2__1_0[13]),
        .I2(Q[4]),
        .I3(y0_carry__4_i_2__1_0[5]),
        .O(y0_carry__1_i_10__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__1_i_11
       (.I0(Q[3]),
        .I1(y0_carry__4_i_2__0_0[13]),
        .I2(Q[4]),
        .I3(y0_carry__4_i_2__0_0[6]),
        .O(y0_carry__1_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__1_i_11__0
       (.I0(Q[3]),
        .I1(y0_carry__4_i_2__1_0[13]),
        .I2(Q[4]),
        .I3(y0_carry__4_i_2__1_0[6]),
        .O(y0_carry__1_i_11__0_n_0));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__1_i_1__0
       (.I0(y0_carry__4_i_2__1_0[1]),
        .I1(y0_carry__1_i_5_n_0),
        .I2(Q[0]),
        .I3(y0_carry__1_i_6_n_0),
        .I4(y0_carry__1),
        .O(S[1]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__1_i_1__1
       (.I0(y0_carry__4_i_2__0_0[1]),
        .I1(y0_carry__1_i_5__0_n_0),
        .I2(Q[0]),
        .I3(y0_carry__1_i_6__0_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[11] [1]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__1_i_2__0
       (.I0(y0_carry__4_i_2__1_0[0]),
        .I1(y0_carry__1_i_6_n_0),
        .I2(Q[0]),
        .I3(\q_reg[1]_2 ),
        .I4(y0_carry__1),
        .O(S[0]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__1_i_2__1
       (.I0(y0_carry__4_i_2__0_0[0]),
        .I1(y0_carry__1_i_6__0_n_0),
        .I2(Q[0]),
        .I3(\q_reg[1]_5 ),
        .I4(y0_carry__1),
        .O(\q_reg[11] [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_5
       (.I0(y0_carry__1_i_9_n_0),
        .I1(y0_carry__1_i_3__0_0),
        .I2(Q[1]),
        .I3(y0_carry__1_i_10_n_0),
        .I4(Q[2]),
        .I5(y0_carry__1_i_3__0),
        .O(y0_carry__1_i_5_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_5__0
       (.I0(y0_carry__1_i_9__0_n_0),
        .I1(y0_carry__1_i_3__1_0),
        .I2(Q[1]),
        .I3(y0_carry__1_i_10__0_n_0),
        .I4(Q[2]),
        .I5(y0_carry__1_i_3__1),
        .O(y0_carry__1_i_5__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_6
       (.I0(y0_carry__1_i_11_n_0),
        .I1(y0_carry__1_i_2__0_0),
        .I2(Q[1]),
        .I3(y0_carry__1_i_2__0_1),
        .I4(Q[2]),
        .I5(y0_carry__1_i_2__0_2),
        .O(y0_carry__1_i_6_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_6__0
       (.I0(y0_carry__1_i_11__0_n_0),
        .I1(y0_carry__1_i_2__1_0),
        .I2(Q[1]),
        .I3(y0_carry__1_i_2__1_1),
        .I4(Q[2]),
        .I5(y0_carry__1_i_2__1_2),
        .O(y0_carry__1_i_6__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_7
       (.I0(y0_carry__1_i_10_n_0),
        .I1(y0_carry__1_i_3__0),
        .I2(Q[1]),
        .I3(y0_carry__1_i_3__0_0),
        .I4(Q[2]),
        .I5(y0_carry__1_i_3__0_1),
        .O(\q_reg[1]_2 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_7__0
       (.I0(y0_carry__1_i_10__0_n_0),
        .I1(y0_carry__1_i_3__1),
        .I2(Q[1]),
        .I3(y0_carry__1_i_3__1_0),
        .I4(Q[2]),
        .I5(y0_carry__1_i_3__1_1),
        .O(\q_reg[1]_5 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__1_i_9
       (.I0(Q[3]),
        .I1(y0_carry__4_i_2__0_0[13]),
        .I2(Q[4]),
        .I3(y0_carry__4_i_2__0_0[7]),
        .O(y0_carry__1_i_9_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__1_i_9__0
       (.I0(Q[3]),
        .I1(y0_carry__4_i_2__1_0[13]),
        .I2(Q[4]),
        .I3(y0_carry__4_i_2__1_0[7]),
        .O(y0_carry__1_i_9__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__2_i_12
       (.I0(Q[3]),
        .I1(y0_carry__4_i_2__0_0[13]),
        .I2(Q[4]),
        .I3(y0_carry__4_i_2__0_0[8]),
        .O(y0_carry__2_i_12_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hCDC8)) 
    y0_carry__2_i_12__0
       (.I0(Q[3]),
        .I1(y0_carry__4_i_2__1_0[13]),
        .I2(Q[4]),
        .I3(y0_carry__4_i_2__1_0[8]),
        .O(y0_carry__2_i_12__0_n_0));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__2_i_2__0
       (.I0(y0_carry__4_i_2__1_0[4]),
        .I1(\q_reg[1]_1 ),
        .I2(Q[0]),
        .I3(y0_carry__2_i_7_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[14] [2]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__2_i_2__1
       (.I0(y0_carry__4_i_2__0_0[4]),
        .I1(\q_reg[1]_4 ),
        .I2(Q[0]),
        .I3(y0_carry__2_i_7__0_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[14]_0 [2]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__2_i_3__0
       (.I0(y0_carry__4_i_2__1_0[3]),
        .I1(y0_carry__2_i_7_n_0),
        .I2(Q[0]),
        .I3(y0_carry__2_i_8_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[14] [1]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__2_i_3__1
       (.I0(y0_carry__4_i_2__0_0[3]),
        .I1(y0_carry__2_i_7__0_n_0),
        .I2(Q[0]),
        .I3(y0_carry__2_i_8__0_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[14]_0 [1]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__2_i_4__0
       (.I0(y0_carry__4_i_2__1_0[2]),
        .I1(y0_carry__2_i_8_n_0),
        .I2(Q[0]),
        .I3(y0_carry__1_i_5_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[14] [0]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__2_i_4__1
       (.I0(y0_carry__4_i_2__0_0[2]),
        .I1(y0_carry__2_i_8__0_n_0),
        .I2(Q[0]),
        .I3(y0_carry__1_i_5__0_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[14]_0 [0]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    y0_carry__2_i_6
       (.I0(y0_carry__2_i_1__0),
        .I1(Q[1]),
        .I2(y0_carry__2_i_12_n_0),
        .I3(Q[2]),
        .I4(y0_carry__1_i_2__0_1),
        .O(\q_reg[1]_1 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    y0_carry__2_i_6__0
       (.I0(y0_carry__2_i_1__1),
        .I1(Q[1]),
        .I2(y0_carry__2_i_12__0_n_0),
        .I3(Q[2]),
        .I4(y0_carry__1_i_2__1_1),
        .O(\q_reg[1]_4 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    y0_carry__2_i_7
       (.I0(y0_carry__2_i_3__0_0),
        .I1(Q[1]),
        .I2(y0_carry__1_i_9_n_0),
        .I3(Q[2]),
        .I4(y0_carry__1_i_3__0_0),
        .O(y0_carry__2_i_7_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    y0_carry__2_i_7__0
       (.I0(y0_carry__2_i_3__1_0),
        .I1(Q[1]),
        .I2(y0_carry__1_i_9__0_n_0),
        .I3(Q[2]),
        .I4(y0_carry__1_i_3__1_0),
        .O(y0_carry__2_i_7__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__2_i_8
       (.I0(y0_carry__2_i_12_n_0),
        .I1(y0_carry__1_i_2__0_1),
        .I2(Q[1]),
        .I3(y0_carry__1_i_11_n_0),
        .I4(Q[2]),
        .I5(y0_carry__1_i_2__0_0),
        .O(y0_carry__2_i_8_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__2_i_8__0
       (.I0(y0_carry__2_i_12__0_n_0),
        .I1(y0_carry__1_i_2__1_1),
        .I2(Q[1]),
        .I3(y0_carry__1_i_11__0_n_0),
        .I4(Q[2]),
        .I5(y0_carry__1_i_2__1_0),
        .O(y0_carry__2_i_8__0_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__3_i_10
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(y0_carry__4_i_2__0_0[13]),
        .I3(Q[4]),
        .I4(y0_carry__4_i_2__0_0[9]),
        .O(y0_carry__3_i_10_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__3_i_10__0
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(y0_carry__4_i_2__1_0[13]),
        .I3(Q[4]),
        .I4(y0_carry__4_i_2__1_0[9]),
        .O(y0_carry__3_i_10__0_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__3_i_11
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(y0_carry__4_i_2__0_0[13]),
        .I3(Q[4]),
        .I4(y0_carry__4_i_2__0_0[10]),
        .O(y0_carry__3_i_11_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__3_i_11__0
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(y0_carry__4_i_2__1_0[13]),
        .I3(Q[4]),
        .I4(y0_carry__4_i_2__1_0[10]),
        .O(y0_carry__3_i_11__0_n_0));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__3_i_1__0
       (.I0(y0_carry__4_i_2__1_0[8]),
        .I1(y0_carry__3_i_5_n_0),
        .I2(Q[0]),
        .I3(y0_carry__3_i_6_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[19] [1]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__3_i_1__1
       (.I0(y0_carry__4_i_2__0_0[8]),
        .I1(y0_carry__3_i_5__0_n_0),
        .I2(Q[0]),
        .I3(y0_carry__3_i_6__0_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[19]_0 [1]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__3_i_2__0
       (.I0(y0_carry__4_i_2__1_0[7]),
        .I1(y0_carry__3_i_6_n_0),
        .I2(Q[0]),
        .I3(\q_reg[1]_3 ),
        .I4(y0_carry__1),
        .O(\q_reg[19] [0]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__3_i_2__1
       (.I0(y0_carry__4_i_2__0_0[7]),
        .I1(y0_carry__3_i_6__0_n_0),
        .I2(Q[0]),
        .I3(\q_reg[1]_6 ),
        .I4(y0_carry__1),
        .O(\q_reg[19]_0 [0]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_5
       (.I0(y0_carry__3_i_9_n_0),
        .I1(Q[1]),
        .I2(y0_carry__3_i_10_n_0),
        .O(y0_carry__3_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_5__0
       (.I0(y0_carry__3_i_9__0_n_0),
        .I1(Q[1]),
        .I2(y0_carry__3_i_10__0_n_0),
        .O(y0_carry__3_i_5__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_6
       (.I0(y0_carry__3_i_11_n_0),
        .I1(Q[1]),
        .I2(y0_carry__3_i_2__0_0),
        .O(y0_carry__3_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_6__0
       (.I0(y0_carry__3_i_11__0_n_0),
        .I1(Q[1]),
        .I2(y0_carry__3_i_2__1_0),
        .O(y0_carry__3_i_6__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_7
       (.I0(y0_carry__3_i_10_n_0),
        .I1(Q[1]),
        .I2(y0_carry__3_i_3__0),
        .O(\q_reg[1]_3 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_7__0
       (.I0(y0_carry__3_i_10__0_n_0),
        .I1(Q[1]),
        .I2(y0_carry__3_i_3__1),
        .O(\q_reg[1]_6 ));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__3_i_9
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(y0_carry__4_i_2__0_0[13]),
        .I3(Q[4]),
        .I4(y0_carry__4_i_2__0_0[11]),
        .O(y0_carry__3_i_9_n_0));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__3_i_9__0
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(y0_carry__4_i_2__1_0[13]),
        .I3(Q[4]),
        .I4(y0_carry__4_i_2__1_0[11]),
        .O(y0_carry__3_i_9__0_n_0));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__4_i_2__0
       (.I0(y0_carry__4_i_2__1_0[11]),
        .I1(y0_carry__4_i_6_n_0),
        .I2(Q[0]),
        .I3(y0_carry__4_i_7_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[22] [2]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__4_i_2__1
       (.I0(y0_carry__4_i_2__0_0[11]),
        .I1(y0_carry__4_i_6__0_n_0),
        .I2(Q[0]),
        .I3(y0_carry__4_i_7__0_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[22]_0 [2]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__4_i_3__0
       (.I0(y0_carry__4_i_2__1_0[10]),
        .I1(y0_carry__4_i_7_n_0),
        .I2(Q[0]),
        .I3(y0_carry__4_i_8_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[22] [1]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__4_i_3__1
       (.I0(y0_carry__4_i_2__0_0[10]),
        .I1(y0_carry__4_i_7__0_n_0),
        .I2(Q[0]),
        .I3(y0_carry__4_i_8__0_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[22]_0 [1]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__4_i_4__0
       (.I0(y0_carry__4_i_2__1_0[9]),
        .I1(y0_carry__4_i_8_n_0),
        .I2(Q[0]),
        .I3(y0_carry__3_i_5_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[22] [0]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__4_i_4__1
       (.I0(y0_carry__4_i_2__0_0[9]),
        .I1(y0_carry__4_i_8__0_n_0),
        .I2(Q[0]),
        .I3(y0_carry__3_i_5__0_n_0),
        .I4(y0_carry__1),
        .O(\q_reg[22]_0 [0]));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__4_i_5
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(y0_carry__4_i_2__0_0[13]),
        .I3(Q[4]),
        .I4(y0_carry__4_i_2__0_0[12]),
        .O(\q_reg[2]_0 ));
  LUT5 #(
    .INIT(32'hF0F1F0E0)) 
    y0_carry__4_i_5__0
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(y0_carry__4_i_2__1_0[13]),
        .I3(Q[4]),
        .I4(y0_carry__4_i_2__1_0[12]),
        .O(\q_reg[2]_1 ));
  LUT6 #(
    .INIT(64'hFF00FF01FF00FE00)) 
    y0_carry__4_i_6
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(y0_carry__4_i_2__0_0[13]),
        .I4(Q[4]),
        .I5(y0_carry__4_i_2__0_0[12]),
        .O(y0_carry__4_i_6_n_0));
  LUT6 #(
    .INIT(64'hFF00FF01FF00FE00)) 
    y0_carry__4_i_6__0
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(y0_carry__4_i_2__1_0[13]),
        .I4(Q[4]),
        .I5(y0_carry__4_i_2__1_0[12]),
        .O(y0_carry__4_i_6__0_n_0));
  LUT6 #(
    .INIT(64'hFF00FF01FF00FE00)) 
    y0_carry__4_i_7
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(y0_carry__4_i_2__0_0[13]),
        .I4(Q[4]),
        .I5(y0_carry__4_i_2__0_0[11]),
        .O(y0_carry__4_i_7_n_0));
  LUT6 #(
    .INIT(64'hFF00FF01FF00FE00)) 
    y0_carry__4_i_7__0
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(y0_carry__4_i_2__1_0[13]),
        .I4(Q[4]),
        .I5(y0_carry__4_i_2__1_0[11]),
        .O(y0_carry__4_i_7__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__4_i_8
       (.I0(\q_reg[2]_0 ),
        .I1(Q[1]),
        .I2(y0_carry__3_i_11_n_0),
        .O(y0_carry__4_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__4_i_8__0
       (.I0(\q_reg[2]_1 ),
        .I1(Q[1]),
        .I2(y0_carry__3_i_11__0_n_0),
        .O(y0_carry__4_i_8__0_n_0));
endmodule

(* ORIG_REF_NAME = "reg" *) 
module reg__parameterized1
   (\q_reg[1]_0 ,
    \q_reg[23]_0 ,
    \q_reg[19]_0 ,
    \q_reg[21]_0 ,
    Q,
    \q_reg[1]_1 ,
    \q_reg[23]_1 ,
    \q_reg[21]_1 ,
    \q_reg[22]_0 ,
    \q_reg[18]_0 ,
    \q_reg[20]_0 ,
    \q_reg[1]_2 ,
    \q_reg[22]_1 ,
    \q_reg[20]_1 ,
    \q_reg[24]_0 ,
    S,
    \q_reg[7]_0 ,
    \q_reg[8]_0 ,
    \q_reg[16]_0 ,
    \q_reg[9]_0 ,
    \q_reg[15]_0 ,
    \q_reg[17]_0 ,
    \q_reg[23]_2 ,
    y0_carry__1_i_6,
    y0_carry__4,
    y0_carry,
    y0_carry__1,
    y0_carry__1_0,
    y0_carry__2,
    y0_carry__2_0,
    y0_carry__3,
    y0_carry__3_0,
    y0_carry__4_0,
    E,
    D,
    CLK,
    AR);
  output \q_reg[1]_0 ;
  output \q_reg[23]_0 ;
  output \q_reg[19]_0 ;
  output \q_reg[21]_0 ;
  output [24:0]Q;
  output \q_reg[1]_1 ;
  output \q_reg[23]_1 ;
  output \q_reg[21]_1 ;
  output \q_reg[22]_0 ;
  output \q_reg[18]_0 ;
  output \q_reg[20]_0 ;
  output \q_reg[1]_2 ;
  output \q_reg[22]_1 ;
  output \q_reg[20]_1 ;
  output [0:0]\q_reg[24]_0 ;
  output [3:0]S;
  output [3:0]\q_reg[7]_0 ;
  output [0:0]\q_reg[8]_0 ;
  output [0:0]\q_reg[16]_0 ;
  output [0:0]\q_reg[9]_0 ;
  output [0:0]\q_reg[15]_0 ;
  output [0:0]\q_reg[17]_0 ;
  output [0:0]\q_reg[23]_2 ;
  input [4:0]y0_carry__1_i_6;
  input [10:0]y0_carry__4;
  input [0:0]y0_carry;
  input y0_carry__1;
  input y0_carry__1_0;
  input y0_carry__2;
  input y0_carry__2_0;
  input y0_carry__3;
  input y0_carry__3_0;
  input y0_carry__4_0;
  input [0:0]E;
  input [24:0]D;
  input CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire CLK;
  wire [24:0]D;
  wire [0:0]E;
  wire [24:0]Q;
  wire [3:0]S;
  wire [0:0]\q_reg[15]_0 ;
  wire [0:0]\q_reg[16]_0 ;
  wire [0:0]\q_reg[17]_0 ;
  wire \q_reg[18]_0 ;
  wire \q_reg[19]_0 ;
  wire \q_reg[1]_0 ;
  wire \q_reg[1]_1 ;
  wire \q_reg[1]_2 ;
  wire \q_reg[20]_0 ;
  wire \q_reg[20]_1 ;
  wire \q_reg[21]_0 ;
  wire \q_reg[21]_1 ;
  wire \q_reg[22]_0 ;
  wire \q_reg[22]_1 ;
  wire \q_reg[23]_0 ;
  wire \q_reg[23]_1 ;
  wire [0:0]\q_reg[23]_2 ;
  wire [0:0]\q_reg[24]_0 ;
  wire [3:0]\q_reg[7]_0 ;
  wire [0:0]\q_reg[8]_0 ;
  wire [0:0]\q_reg[9]_0 ;
  wire [0:0]y0_carry;
  wire y0_carry__0_i_5_n_0;
  wire y0_carry__0_i_6_n_0;
  wire y0_carry__0_i_7_n_0;
  wire y0_carry__0_i_8_n_0;
  wire y0_carry__1;
  wire y0_carry__1_0;
  wire [4:0]y0_carry__1_i_6;
  wire y0_carry__2;
  wire y0_carry__2_0;
  wire y0_carry__3;
  wire y0_carry__3_0;
  wire [10:0]y0_carry__4;
  wire y0_carry__4_0;
  wire y0_carry_i_11_n_0;
  wire y0_carry_i_12_n_0;
  wire y0_carry_i_13_n_0;
  wire y0_carry_i_14_n_0;
  wire y0_carry_i_15_n_0;
  wire y0_carry_i_16_n_0;
  wire y0_carry_i_17_n_0;
  wire y0_carry_i_18_n_0;
  wire y0_carry_i_19_n_0;
  wire y0_carry_i_20_n_0;
  wire y0_carry_i_5_n_0;
  wire y0_carry_i_6_n_0;
  wire y0_carry_i_7_n_0;
  wire y0_carry_i_8_n_0;
  wire y0_carry_i_9_n_0;

  FDCE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[0]),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[10] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[10]),
        .Q(Q[10]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[11] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[11]),
        .Q(Q[11]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[12] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[12]),
        .Q(Q[12]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[13] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[13]),
        .Q(Q[13]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[14] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[14]),
        .Q(Q[14]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[15] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[15]),
        .Q(Q[15]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[16] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[16]),
        .Q(Q[16]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[17] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[17]),
        .Q(Q[17]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[18] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[18]),
        .Q(Q[18]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[19] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[19]),
        .Q(Q[19]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[1]),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[20] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[20]),
        .Q(Q[20]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[21] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[21]),
        .Q(Q[21]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[22] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[22]),
        .Q(Q[22]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[23] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[23]),
        .Q(Q[23]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[24] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[24]),
        .Q(Q[24]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[2]),
        .Q(Q[2]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[3]),
        .Q(Q[3]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[4] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[4]),
        .Q(Q[4]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[5] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[5]),
        .Q(Q[5]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[6] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[6]),
        .Q(Q[6]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[7] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[7]),
        .Q(Q[7]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[8] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[8]),
        .Q(Q[8]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[9] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[9]),
        .Q(Q[9]));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_10
       (.I0(Q[20]),
        .I1(y0_carry__1_i_6[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6[4]),
        .I4(Q[12]),
        .O(\q_reg[20]_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_11
       (.I0(Q[21]),
        .I1(y0_carry__1_i_6[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6[4]),
        .I4(Q[13]),
        .O(\q_reg[21]_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_12
       (.I0(Q[19]),
        .I1(y0_carry__1_i_6[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6[4]),
        .I4(Q[11]),
        .O(\q_reg[19]_0 ));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__0_i_1__0
       (.I0(y0_carry__4[7]),
        .I1(y0_carry__0_i_5_n_0),
        .I2(y0_carry__1_i_6[0]),
        .I3(y0_carry__0_i_6_n_0),
        .I4(y0_carry),
        .O(\q_reg[7]_0 [3]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__0_i_2__0
       (.I0(y0_carry__4[6]),
        .I1(y0_carry__0_i_6_n_0),
        .I2(y0_carry__1_i_6[0]),
        .I3(y0_carry__0_i_7_n_0),
        .I4(y0_carry),
        .O(\q_reg[7]_0 [2]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__0_i_3__0
       (.I0(y0_carry__4[5]),
        .I1(y0_carry__0_i_7_n_0),
        .I2(y0_carry__1_i_6[0]),
        .I3(y0_carry__0_i_8_n_0),
        .I4(y0_carry),
        .O(\q_reg[7]_0 [1]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__0_i_4__0
       (.I0(y0_carry__4[4]),
        .I1(y0_carry__0_i_8_n_0),
        .I2(y0_carry__1_i_6[0]),
        .I3(y0_carry_i_5_n_0),
        .I4(y0_carry),
        .O(\q_reg[7]_0 [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_5
       (.I0(\q_reg[22]_0 ),
        .I1(\q_reg[18]_0 ),
        .I2(y0_carry__1_i_6[1]),
        .I3(\q_reg[20]_0 ),
        .I4(y0_carry__1_i_6[2]),
        .I5(y0_carry_i_12_n_0),
        .O(y0_carry__0_i_5_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_6
       (.I0(\q_reg[21]_0 ),
        .I1(y0_carry_i_14_n_0),
        .I2(y0_carry__1_i_6[1]),
        .I3(\q_reg[19]_0 ),
        .I4(y0_carry__1_i_6[2]),
        .I5(y0_carry_i_16_n_0),
        .O(y0_carry__0_i_6_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_7
       (.I0(\q_reg[20]_0 ),
        .I1(y0_carry_i_12_n_0),
        .I2(y0_carry__1_i_6[1]),
        .I3(\q_reg[18]_0 ),
        .I4(y0_carry__1_i_6[2]),
        .I5(y0_carry_i_11_n_0),
        .O(y0_carry__0_i_7_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_8
       (.I0(\q_reg[19]_0 ),
        .I1(y0_carry_i_16_n_0),
        .I2(y0_carry__1_i_6[1]),
        .I3(y0_carry_i_14_n_0),
        .I4(y0_carry__1_i_6[2]),
        .I5(y0_carry_i_15_n_0),
        .O(y0_carry__0_i_8_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_9
       (.I0(Q[22]),
        .I1(y0_carry__1_i_6[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6[4]),
        .I4(Q[14]),
        .O(\q_reg[22]_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__1_i_12
       (.I0(Q[23]),
        .I1(y0_carry__1_i_6[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6[4]),
        .I4(Q[15]),
        .O(\q_reg[23]_0 ));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__1_i_3__1
       (.I0(Q[9]),
        .I1(y0_carry__1),
        .I2(y0_carry__1_i_6[0]),
        .I3(y0_carry__1_0),
        .I4(y0_carry),
        .O(\q_reg[9]_0 ));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__1_i_4__0
       (.I0(y0_carry__4[8]),
        .I1(\q_reg[1]_0 ),
        .I2(y0_carry__1_i_6[0]),
        .I3(y0_carry__0_i_5_n_0),
        .I4(y0_carry),
        .O(\q_reg[8]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_8
       (.I0(\q_reg[23]_0 ),
        .I1(\q_reg[19]_0 ),
        .I2(y0_carry__1_i_6[1]),
        .I3(\q_reg[21]_0 ),
        .I4(y0_carry__1_i_6[2]),
        .I5(y0_carry_i_14_n_0),
        .O(\q_reg[1]_0 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__2_i_10
       (.I0(Q[20]),
        .I1(y0_carry__1_i_6[2]),
        .I2(y0_carry__1_i_6[3]),
        .I3(Q[24]),
        .I4(y0_carry__1_i_6[4]),
        .I5(Q[16]),
        .O(\q_reg[20]_1 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__2_i_11
       (.I0(Q[21]),
        .I1(y0_carry__1_i_6[2]),
        .I2(y0_carry__1_i_6[3]),
        .I3(Q[24]),
        .I4(y0_carry__1_i_6[4]),
        .I5(Q[17]),
        .O(\q_reg[21]_1 ));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__2_i_1__1
       (.I0(Q[15]),
        .I1(y0_carry__2),
        .I2(y0_carry__1_i_6[0]),
        .I3(y0_carry__2_0),
        .I4(y0_carry),
        .O(\q_reg[15]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__2_i_5
       (.I0(\q_reg[22]_1 ),
        .I1(y0_carry__1_i_6[1]),
        .I2(\q_reg[20]_1 ),
        .O(\q_reg[1]_2 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__2_i_9
       (.I0(Q[22]),
        .I1(y0_carry__1_i_6[2]),
        .I2(y0_carry__1_i_6[3]),
        .I3(Q[24]),
        .I4(y0_carry__1_i_6[4]),
        .I5(Q[18]),
        .O(\q_reg[22]_1 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__3_i_12
       (.I0(Q[23]),
        .I1(y0_carry__1_i_6[2]),
        .I2(y0_carry__1_i_6[3]),
        .I3(Q[24]),
        .I4(y0_carry__1_i_6[4]),
        .I5(Q[19]),
        .O(\q_reg[23]_1 ));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__3_i_3__1
       (.I0(Q[17]),
        .I1(y0_carry__3),
        .I2(y0_carry__1_i_6[0]),
        .I3(y0_carry__3_0),
        .I4(y0_carry),
        .O(\q_reg[17]_0 ));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__3_i_4__0
       (.I0(y0_carry__4[9]),
        .I1(\q_reg[1]_1 ),
        .I2(y0_carry__1_i_6[0]),
        .I3(\q_reg[1]_2 ),
        .I4(y0_carry),
        .O(\q_reg[16]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_8
       (.I0(\q_reg[23]_1 ),
        .I1(y0_carry__1_i_6[1]),
        .I2(\q_reg[21]_1 ),
        .O(\q_reg[1]_1 ));
  LUT6 #(
    .INIT(64'h6665666A999A9995)) 
    y0_carry__4_i_1__1
       (.I0(Q[23]),
        .I1(y0_carry__4[10]),
        .I2(y0_carry__1_i_6[0]),
        .I3(y0_carry__1_i_6[1]),
        .I4(y0_carry__4_0),
        .I5(y0_carry),
        .O(\q_reg[23]_2 ));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__5_i_1
       (.I0(Q[24]),
        .I1(y0_carry__4[10]),
        .I2(y0_carry),
        .O(\q_reg[24]_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry_i_10
       (.I0(Q[18]),
        .I1(y0_carry__1_i_6[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6[4]),
        .I4(Q[10]),
        .O(\q_reg[18]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_11
       (.I0(Q[24]),
        .I1(Q[14]),
        .I2(y0_carry__1_i_6[3]),
        .I3(Q[22]),
        .I4(y0_carry__1_i_6[4]),
        .I5(Q[6]),
        .O(y0_carry_i_11_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry_i_12
       (.I0(Q[16]),
        .I1(y0_carry__1_i_6[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6[4]),
        .I4(Q[8]),
        .O(y0_carry_i_12_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_13
       (.I0(Q[24]),
        .I1(Q[12]),
        .I2(y0_carry__1_i_6[3]),
        .I3(Q[20]),
        .I4(y0_carry__1_i_6[4]),
        .I5(Q[4]),
        .O(y0_carry_i_13_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry_i_14
       (.I0(Q[17]),
        .I1(y0_carry__1_i_6[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6[4]),
        .I4(Q[9]),
        .O(y0_carry_i_14_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_15
       (.I0(Q[24]),
        .I1(Q[13]),
        .I2(y0_carry__1_i_6[3]),
        .I3(Q[21]),
        .I4(y0_carry__1_i_6[4]),
        .I5(Q[5]),
        .O(y0_carry_i_15_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_16
       (.I0(Q[24]),
        .I1(Q[15]),
        .I2(y0_carry__1_i_6[3]),
        .I3(Q[23]),
        .I4(y0_carry__1_i_6[4]),
        .I5(Q[7]),
        .O(y0_carry_i_16_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_17
       (.I0(Q[24]),
        .I1(Q[11]),
        .I2(y0_carry__1_i_6[3]),
        .I3(Q[19]),
        .I4(y0_carry__1_i_6[4]),
        .I5(Q[3]),
        .O(y0_carry_i_17_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_18
       (.I0(Q[24]),
        .I1(Q[10]),
        .I2(y0_carry__1_i_6[3]),
        .I3(Q[18]),
        .I4(y0_carry__1_i_6[4]),
        .I5(Q[2]),
        .O(y0_carry_i_18_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_19
       (.I0(Q[24]),
        .I1(Q[9]),
        .I2(y0_carry__1_i_6[3]),
        .I3(Q[17]),
        .I4(y0_carry__1_i_6[4]),
        .I5(Q[1]),
        .O(y0_carry_i_19_n_0));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry_i_1__0
       (.I0(y0_carry__4[3]),
        .I1(y0_carry_i_5_n_0),
        .I2(y0_carry__1_i_6[0]),
        .I3(y0_carry_i_6_n_0),
        .I4(y0_carry),
        .O(S[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_20
       (.I0(Q[24]),
        .I1(Q[8]),
        .I2(y0_carry__1_i_6[3]),
        .I3(Q[16]),
        .I4(y0_carry__1_i_6[4]),
        .I5(Q[0]),
        .O(y0_carry_i_20_n_0));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry_i_2__0
       (.I0(y0_carry__4[2]),
        .I1(y0_carry_i_6_n_0),
        .I2(y0_carry__1_i_6[0]),
        .I3(y0_carry_i_7_n_0),
        .I4(y0_carry),
        .O(S[2]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry_i_3__0
       (.I0(y0_carry__4[1]),
        .I1(y0_carry_i_7_n_0),
        .I2(y0_carry__1_i_6[0]),
        .I3(y0_carry_i_8_n_0),
        .I4(y0_carry),
        .O(S[1]));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry_i_4__0
       (.I0(y0_carry__4[0]),
        .I1(y0_carry_i_8_n_0),
        .I2(y0_carry__1_i_6[0]),
        .I3(y0_carry_i_9_n_0),
        .I4(y0_carry),
        .O(S[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_5
       (.I0(\q_reg[18]_0 ),
        .I1(y0_carry_i_11_n_0),
        .I2(y0_carry__1_i_6[1]),
        .I3(y0_carry_i_12_n_0),
        .I4(y0_carry__1_i_6[2]),
        .I5(y0_carry_i_13_n_0),
        .O(y0_carry_i_5_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_6
       (.I0(y0_carry_i_14_n_0),
        .I1(y0_carry_i_15_n_0),
        .I2(y0_carry__1_i_6[1]),
        .I3(y0_carry_i_16_n_0),
        .I4(y0_carry__1_i_6[2]),
        .I5(y0_carry_i_17_n_0),
        .O(y0_carry_i_6_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_7
       (.I0(y0_carry_i_12_n_0),
        .I1(y0_carry_i_13_n_0),
        .I2(y0_carry__1_i_6[1]),
        .I3(y0_carry_i_11_n_0),
        .I4(y0_carry__1_i_6[2]),
        .I5(y0_carry_i_18_n_0),
        .O(y0_carry_i_7_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_8
       (.I0(y0_carry_i_16_n_0),
        .I1(y0_carry_i_17_n_0),
        .I2(y0_carry__1_i_6[1]),
        .I3(y0_carry_i_15_n_0),
        .I4(y0_carry__1_i_6[2]),
        .I5(y0_carry_i_19_n_0),
        .O(y0_carry_i_8_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_9
       (.I0(y0_carry_i_11_n_0),
        .I1(y0_carry_i_18_n_0),
        .I2(y0_carry__1_i_6[1]),
        .I3(y0_carry_i_13_n_0),
        .I4(y0_carry__1_i_6[2]),
        .I5(y0_carry_i_20_n_0),
        .O(y0_carry_i_9_n_0));
endmodule

(* ORIG_REF_NAME = "reg" *) 
module reg__parameterized1_1
   (\q_reg[1]_0 ,
    \q_reg[23]_0 ,
    \q_reg[19]_0 ,
    \q_reg[21]_0 ,
    Q,
    \q_reg[1]_1 ,
    \q_reg[23]_1 ,
    \q_reg[21]_1 ,
    \q_reg[22]_0 ,
    \q_reg[18]_0 ,
    \q_reg[20]_0 ,
    \q_reg[1]_2 ,
    \q_reg[22]_1 ,
    \q_reg[20]_1 ,
    \q_reg[9]_0 ,
    \q_reg[15]_0 ,
    \q_reg[17]_0 ,
    \q_reg[23]_2 ,
    S,
    \q_reg[7]_0 ,
    \q_reg[8]_0 ,
    \q_reg[16]_0 ,
    y0_carry__1_i_6__0,
    y0_carry__1,
    y0_carry__1_0,
    y0_carry__1_1,
    y0_carry__2,
    y0_carry__2_0,
    y0_carry__3,
    y0_carry__3_0,
    y0_carry__4,
    y0_carry__4_0,
    E,
    D,
    CLK,
    AR);
  output \q_reg[1]_0 ;
  output \q_reg[23]_0 ;
  output \q_reg[19]_0 ;
  output \q_reg[21]_0 ;
  output [24:0]Q;
  output \q_reg[1]_1 ;
  output \q_reg[23]_1 ;
  output \q_reg[21]_1 ;
  output \q_reg[22]_0 ;
  output \q_reg[18]_0 ;
  output \q_reg[20]_0 ;
  output \q_reg[1]_2 ;
  output \q_reg[22]_1 ;
  output \q_reg[20]_1 ;
  output [0:0]\q_reg[9]_0 ;
  output [0:0]\q_reg[15]_0 ;
  output [0:0]\q_reg[17]_0 ;
  output [0:0]\q_reg[23]_2 ;
  output [3:0]S;
  output [3:0]\q_reg[7]_0 ;
  output [0:0]\q_reg[8]_0 ;
  output [0:0]\q_reg[16]_0 ;
  input [4:0]y0_carry__1_i_6__0;
  input y0_carry__1;
  input y0_carry__1_0;
  input [0:0]y0_carry__1_1;
  input y0_carry__2;
  input y0_carry__2_0;
  input y0_carry__3;
  input y0_carry__3_0;
  input [10:0]y0_carry__4;
  input y0_carry__4_0;
  input [0:0]E;
  input [24:0]D;
  input CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire CLK;
  wire [24:0]D;
  wire [0:0]E;
  wire [24:0]Q;
  wire [3:0]S;
  wire [0:0]\q_reg[15]_0 ;
  wire [0:0]\q_reg[16]_0 ;
  wire [0:0]\q_reg[17]_0 ;
  wire \q_reg[18]_0 ;
  wire \q_reg[19]_0 ;
  wire \q_reg[1]_0 ;
  wire \q_reg[1]_1 ;
  wire \q_reg[1]_2 ;
  wire \q_reg[20]_0 ;
  wire \q_reg[20]_1 ;
  wire \q_reg[21]_0 ;
  wire \q_reg[21]_1 ;
  wire \q_reg[22]_0 ;
  wire \q_reg[22]_1 ;
  wire \q_reg[23]_0 ;
  wire \q_reg[23]_1 ;
  wire [0:0]\q_reg[23]_2 ;
  wire [3:0]\q_reg[7]_0 ;
  wire [0:0]\q_reg[8]_0 ;
  wire [0:0]\q_reg[9]_0 ;
  wire y0_carry__0_i_5__0_n_0;
  wire y0_carry__0_i_6__0_n_0;
  wire y0_carry__0_i_7__0_n_0;
  wire y0_carry__0_i_8__0_n_0;
  wire y0_carry__1;
  wire y0_carry__1_0;
  wire [0:0]y0_carry__1_1;
  wire [4:0]y0_carry__1_i_6__0;
  wire y0_carry__2;
  wire y0_carry__2_0;
  wire y0_carry__3;
  wire y0_carry__3_0;
  wire [10:0]y0_carry__4;
  wire y0_carry__4_0;
  wire y0_carry_i_11__0_n_0;
  wire y0_carry_i_12__0_n_0;
  wire y0_carry_i_13__0_n_0;
  wire y0_carry_i_14__0_n_0;
  wire y0_carry_i_15__0_n_0;
  wire y0_carry_i_16__0_n_0;
  wire y0_carry_i_17__0_n_0;
  wire y0_carry_i_18__0_n_0;
  wire y0_carry_i_19__0_n_0;
  wire y0_carry_i_20__0_n_0;
  wire y0_carry_i_5__0_n_0;
  wire y0_carry_i_6__0_n_0;
  wire y0_carry_i_7__0_n_0;
  wire y0_carry_i_8__0_n_0;
  wire y0_carry_i_9__0_n_0;

  FDCE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[0]),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[10] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[10]),
        .Q(Q[10]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[11] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[11]),
        .Q(Q[11]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[12] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[12]),
        .Q(Q[12]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[13] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[13]),
        .Q(Q[13]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[14] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[14]),
        .Q(Q[14]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[15] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[15]),
        .Q(Q[15]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[16] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[16]),
        .Q(Q[16]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[17] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[17]),
        .Q(Q[17]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[18] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[18]),
        .Q(Q[18]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[19] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[19]),
        .Q(Q[19]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[1]),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[20] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[20]),
        .Q(Q[20]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[21] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[21]),
        .Q(Q[21]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[22] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[22]),
        .Q(Q[22]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[23] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[23]),
        .Q(Q[23]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[24] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[24]),
        .Q(Q[24]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[2]),
        .Q(Q[2]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[3]),
        .Q(Q[3]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[4] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[4]),
        .Q(Q[4]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[5] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[5]),
        .Q(Q[5]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[6] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[6]),
        .Q(Q[6]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[7] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[7]),
        .Q(Q[7]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[8] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[8]),
        .Q(Q[8]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[9] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[9]),
        .Q(Q[9]));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_10__0
       (.I0(Q[20]),
        .I1(y0_carry__1_i_6__0[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6__0[4]),
        .I4(Q[12]),
        .O(\q_reg[20]_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_11__0
       (.I0(Q[21]),
        .I1(y0_carry__1_i_6__0[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6__0[4]),
        .I4(Q[13]),
        .O(\q_reg[21]_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_12__0
       (.I0(Q[19]),
        .I1(y0_carry__1_i_6__0[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6__0[4]),
        .I4(Q[11]),
        .O(\q_reg[19]_0 ));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__0_i_1__1
       (.I0(y0_carry__4[7]),
        .I1(y0_carry__0_i_5__0_n_0),
        .I2(y0_carry__1_i_6__0[0]),
        .I3(y0_carry__0_i_6__0_n_0),
        .I4(y0_carry__1_1),
        .O(\q_reg[7]_0 [3]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__0_i_2__1
       (.I0(y0_carry__4[6]),
        .I1(y0_carry__0_i_6__0_n_0),
        .I2(y0_carry__1_i_6__0[0]),
        .I3(y0_carry__0_i_7__0_n_0),
        .I4(y0_carry__1_1),
        .O(\q_reg[7]_0 [2]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__0_i_3__1
       (.I0(y0_carry__4[5]),
        .I1(y0_carry__0_i_7__0_n_0),
        .I2(y0_carry__1_i_6__0[0]),
        .I3(y0_carry__0_i_8__0_n_0),
        .I4(y0_carry__1_1),
        .O(\q_reg[7]_0 [1]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__0_i_4__1
       (.I0(y0_carry__4[4]),
        .I1(y0_carry__0_i_8__0_n_0),
        .I2(y0_carry__1_i_6__0[0]),
        .I3(y0_carry_i_5__0_n_0),
        .I4(y0_carry__1_1),
        .O(\q_reg[7]_0 [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_5__0
       (.I0(\q_reg[22]_0 ),
        .I1(\q_reg[18]_0 ),
        .I2(y0_carry__1_i_6__0[1]),
        .I3(\q_reg[20]_0 ),
        .I4(y0_carry__1_i_6__0[2]),
        .I5(y0_carry_i_12__0_n_0),
        .O(y0_carry__0_i_5__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_6__0
       (.I0(\q_reg[21]_0 ),
        .I1(y0_carry_i_14__0_n_0),
        .I2(y0_carry__1_i_6__0[1]),
        .I3(\q_reg[19]_0 ),
        .I4(y0_carry__1_i_6__0[2]),
        .I5(y0_carry_i_16__0_n_0),
        .O(y0_carry__0_i_6__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_7__0
       (.I0(\q_reg[20]_0 ),
        .I1(y0_carry_i_12__0_n_0),
        .I2(y0_carry__1_i_6__0[1]),
        .I3(\q_reg[18]_0 ),
        .I4(y0_carry__1_i_6__0[2]),
        .I5(y0_carry_i_11__0_n_0),
        .O(y0_carry__0_i_7__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__0_i_8__0
       (.I0(\q_reg[19]_0 ),
        .I1(y0_carry_i_16__0_n_0),
        .I2(y0_carry__1_i_6__0[1]),
        .I3(y0_carry_i_14__0_n_0),
        .I4(y0_carry__1_i_6__0[2]),
        .I5(y0_carry_i_15__0_n_0),
        .O(y0_carry__0_i_8__0_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__0_i_9__0
       (.I0(Q[22]),
        .I1(y0_carry__1_i_6__0[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6__0[4]),
        .I4(Q[14]),
        .O(\q_reg[22]_0 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry__1_i_12__0
       (.I0(Q[23]),
        .I1(y0_carry__1_i_6__0[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6__0[4]),
        .I4(Q[15]),
        .O(\q_reg[23]_0 ));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__1_i_3__0
       (.I0(Q[9]),
        .I1(y0_carry__1),
        .I2(y0_carry__1_i_6__0[0]),
        .I3(y0_carry__1_0),
        .I4(y0_carry__1_1),
        .O(\q_reg[9]_0 ));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__1_i_4__1
       (.I0(y0_carry__4[8]),
        .I1(\q_reg[1]_0 ),
        .I2(y0_carry__1_i_6__0[0]),
        .I3(y0_carry__0_i_5__0_n_0),
        .I4(y0_carry__1_1),
        .O(\q_reg[8]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry__1_i_8__0
       (.I0(\q_reg[23]_0 ),
        .I1(\q_reg[19]_0 ),
        .I2(y0_carry__1_i_6__0[1]),
        .I3(\q_reg[21]_0 ),
        .I4(y0_carry__1_i_6__0[2]),
        .I5(y0_carry_i_14__0_n_0),
        .O(\q_reg[1]_0 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__2_i_10__0
       (.I0(Q[20]),
        .I1(y0_carry__1_i_6__0[2]),
        .I2(y0_carry__1_i_6__0[3]),
        .I3(Q[24]),
        .I4(y0_carry__1_i_6__0[4]),
        .I5(Q[16]),
        .O(\q_reg[20]_1 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__2_i_11__0
       (.I0(Q[21]),
        .I1(y0_carry__1_i_6__0[2]),
        .I2(y0_carry__1_i_6__0[3]),
        .I3(Q[24]),
        .I4(y0_carry__1_i_6__0[4]),
        .I5(Q[17]),
        .O(\q_reg[21]_1 ));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__2_i_1__0
       (.I0(Q[15]),
        .I1(y0_carry__2),
        .I2(y0_carry__1_i_6__0[0]),
        .I3(y0_carry__2_0),
        .I4(y0_carry__1_1),
        .O(\q_reg[15]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__2_i_5__0
       (.I0(\q_reg[22]_1 ),
        .I1(y0_carry__1_i_6__0[1]),
        .I2(\q_reg[20]_1 ),
        .O(\q_reg[1]_2 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__2_i_9__0
       (.I0(Q[22]),
        .I1(y0_carry__1_i_6__0[2]),
        .I2(y0_carry__1_i_6__0[3]),
        .I3(Q[24]),
        .I4(y0_carry__1_i_6__0[4]),
        .I5(Q[18]),
        .O(\q_reg[22]_1 ));
  LUT6 #(
    .INIT(64'hFF00FB0BFF00F808)) 
    y0_carry__3_i_12__0
       (.I0(Q[23]),
        .I1(y0_carry__1_i_6__0[2]),
        .I2(y0_carry__1_i_6__0[3]),
        .I3(Q[24]),
        .I4(y0_carry__1_i_6__0[4]),
        .I5(Q[19]),
        .O(\q_reg[23]_1 ));
  LUT5 #(
    .INIT(32'h9A95656A)) 
    y0_carry__3_i_3__0
       (.I0(Q[17]),
        .I1(y0_carry__3),
        .I2(y0_carry__1_i_6__0[0]),
        .I3(y0_carry__3_0),
        .I4(y0_carry__1_1),
        .O(\q_reg[17]_0 ));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry__3_i_4__1
       (.I0(y0_carry__4[9]),
        .I1(\q_reg[1]_1 ),
        .I2(y0_carry__1_i_6__0[0]),
        .I3(\q_reg[1]_2 ),
        .I4(y0_carry__1_1),
        .O(\q_reg[16]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    y0_carry__3_i_8__0
       (.I0(\q_reg[23]_1 ),
        .I1(y0_carry__1_i_6__0[1]),
        .I2(\q_reg[21]_1 ),
        .O(\q_reg[1]_1 ));
  LUT6 #(
    .INIT(64'h999A99956665666A)) 
    y0_carry__4_i_1__0
       (.I0(Q[23]),
        .I1(y0_carry__4[10]),
        .I2(y0_carry__1_i_6__0[0]),
        .I3(y0_carry__1_i_6__0[1]),
        .I4(y0_carry__4_0),
        .I5(y0_carry__1_1),
        .O(\q_reg[23]_2 ));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry_i_10__0
       (.I0(Q[18]),
        .I1(y0_carry__1_i_6__0[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6__0[4]),
        .I4(Q[10]),
        .O(\q_reg[18]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_11__0
       (.I0(Q[24]),
        .I1(Q[14]),
        .I2(y0_carry__1_i_6__0[3]),
        .I3(Q[22]),
        .I4(y0_carry__1_i_6__0[4]),
        .I5(Q[6]),
        .O(y0_carry_i_11__0_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry_i_12__0
       (.I0(Q[16]),
        .I1(y0_carry__1_i_6__0[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6__0[4]),
        .I4(Q[8]),
        .O(y0_carry_i_12__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_13__0
       (.I0(Q[24]),
        .I1(Q[12]),
        .I2(y0_carry__1_i_6__0[3]),
        .I3(Q[20]),
        .I4(y0_carry__1_i_6__0[4]),
        .I5(Q[4]),
        .O(y0_carry_i_13__0_n_0));
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    y0_carry_i_14__0
       (.I0(Q[17]),
        .I1(y0_carry__1_i_6__0[3]),
        .I2(Q[24]),
        .I3(y0_carry__1_i_6__0[4]),
        .I4(Q[9]),
        .O(y0_carry_i_14__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_15__0
       (.I0(Q[24]),
        .I1(Q[13]),
        .I2(y0_carry__1_i_6__0[3]),
        .I3(Q[21]),
        .I4(y0_carry__1_i_6__0[4]),
        .I5(Q[5]),
        .O(y0_carry_i_15__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_16__0
       (.I0(Q[24]),
        .I1(Q[15]),
        .I2(y0_carry__1_i_6__0[3]),
        .I3(Q[23]),
        .I4(y0_carry__1_i_6__0[4]),
        .I5(Q[7]),
        .O(y0_carry_i_16__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_17__0
       (.I0(Q[24]),
        .I1(Q[11]),
        .I2(y0_carry__1_i_6__0[3]),
        .I3(Q[19]),
        .I4(y0_carry__1_i_6__0[4]),
        .I5(Q[3]),
        .O(y0_carry_i_17__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_18__0
       (.I0(Q[24]),
        .I1(Q[10]),
        .I2(y0_carry__1_i_6__0[3]),
        .I3(Q[18]),
        .I4(y0_carry__1_i_6__0[4]),
        .I5(Q[2]),
        .O(y0_carry_i_18__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_19__0
       (.I0(Q[24]),
        .I1(Q[9]),
        .I2(y0_carry__1_i_6__0[3]),
        .I3(Q[17]),
        .I4(y0_carry__1_i_6__0[4]),
        .I5(Q[1]),
        .O(y0_carry_i_19__0_n_0));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry_i_1__1
       (.I0(y0_carry__4[3]),
        .I1(y0_carry_i_5__0_n_0),
        .I2(y0_carry__1_i_6__0[0]),
        .I3(y0_carry_i_6__0_n_0),
        .I4(y0_carry__1_1),
        .O(S[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_20__0
       (.I0(Q[24]),
        .I1(Q[8]),
        .I2(y0_carry__1_i_6__0[3]),
        .I3(Q[16]),
        .I4(y0_carry__1_i_6__0[4]),
        .I5(Q[0]),
        .O(y0_carry_i_20__0_n_0));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry_i_2__1
       (.I0(y0_carry__4[2]),
        .I1(y0_carry_i_6__0_n_0),
        .I2(y0_carry__1_i_6__0[0]),
        .I3(y0_carry_i_7__0_n_0),
        .I4(y0_carry__1_1),
        .O(S[2]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry_i_3__1
       (.I0(y0_carry__4[1]),
        .I1(y0_carry_i_7__0_n_0),
        .I2(y0_carry__1_i_6__0[0]),
        .I3(y0_carry_i_8__0_n_0),
        .I4(y0_carry__1_1),
        .O(S[1]));
  LUT5 #(
    .INIT(32'h656A9A95)) 
    y0_carry_i_4__1
       (.I0(y0_carry__4[0]),
        .I1(y0_carry_i_8__0_n_0),
        .I2(y0_carry__1_i_6__0[0]),
        .I3(y0_carry_i_9__0_n_0),
        .I4(y0_carry__1_1),
        .O(S[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_5__0
       (.I0(\q_reg[18]_0 ),
        .I1(y0_carry_i_11__0_n_0),
        .I2(y0_carry__1_i_6__0[1]),
        .I3(y0_carry_i_12__0_n_0),
        .I4(y0_carry__1_i_6__0[2]),
        .I5(y0_carry_i_13__0_n_0),
        .O(y0_carry_i_5__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_6__0
       (.I0(y0_carry_i_14__0_n_0),
        .I1(y0_carry_i_15__0_n_0),
        .I2(y0_carry__1_i_6__0[1]),
        .I3(y0_carry_i_16__0_n_0),
        .I4(y0_carry__1_i_6__0[2]),
        .I5(y0_carry_i_17__0_n_0),
        .O(y0_carry_i_6__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_7__0
       (.I0(y0_carry_i_12__0_n_0),
        .I1(y0_carry_i_13__0_n_0),
        .I2(y0_carry__1_i_6__0[1]),
        .I3(y0_carry_i_11__0_n_0),
        .I4(y0_carry__1_i_6__0[2]),
        .I5(y0_carry_i_18__0_n_0),
        .O(y0_carry_i_7__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_8__0
       (.I0(y0_carry_i_16__0_n_0),
        .I1(y0_carry_i_17__0_n_0),
        .I2(y0_carry__1_i_6__0[1]),
        .I3(y0_carry_i_15__0_n_0),
        .I4(y0_carry__1_i_6__0[2]),
        .I5(y0_carry_i_19__0_n_0),
        .O(y0_carry_i_8__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    y0_carry_i_9__0
       (.I0(y0_carry_i_11__0_n_0),
        .I1(y0_carry_i_18__0_n_0),
        .I2(y0_carry__1_i_6__0[1]),
        .I3(y0_carry_i_13__0_n_0),
        .I4(y0_carry__1_i_6__0[2]),
        .I5(y0_carry_i_20__0_n_0),
        .O(y0_carry_i_9__0_n_0));
endmodule

(* ORIG_REF_NAME = "reg" *) 
module reg__parameterized3
   (\q_reg[23]_0 ,
    Q,
    \q_reg[23]_1 ,
    S,
    \q_reg[7]_0 ,
    \q_reg[11]_0 ,
    \q_reg[15]_0 ,
    \q_reg[19]_0 ,
    \q_reg[22]_0 ,
    y0_carry__5,
    y0_carry__5_0,
    out_ROM,
    E,
    D,
    CLK,
    AR);
  output \q_reg[23]_0 ;
  output [23:0]Q;
  output [0:0]\q_reg[23]_1 ;
  output [3:0]S;
  output [3:0]\q_reg[7]_0 ;
  output [3:0]\q_reg[11]_0 ;
  output [3:0]\q_reg[15]_0 ;
  output [3:0]\q_reg[19]_0 ;
  output [2:0]\q_reg[22]_0 ;
  input [0:0]y0_carry__5;
  input [0:0]y0_carry__5_0;
  input [22:0]out_ROM;
  input [0:0]E;
  input [23:0]D;
  input CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire CLK;
  wire [23:0]D;
  wire [0:0]E;
  wire [23:0]Q;
  wire [3:0]S;
  wire [22:0]out_ROM;
  wire [3:0]\q_reg[11]_0 ;
  wire [3:0]\q_reg[15]_0 ;
  wire [3:0]\q_reg[19]_0 ;
  wire [2:0]\q_reg[22]_0 ;
  wire \q_reg[23]_0 ;
  wire [0:0]\q_reg[23]_1 ;
  wire [3:0]\q_reg[7]_0 ;
  wire [0:0]y0_carry__5;
  wire [0:0]y0_carry__5_0;

  FDCE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[0]),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[10] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[10]),
        .Q(Q[10]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[11] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[11]),
        .Q(Q[11]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[12] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[12]),
        .Q(Q[12]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[13] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[13]),
        .Q(Q[13]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[14] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[14]),
        .Q(Q[14]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[15] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[15]),
        .Q(Q[15]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[16] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[16]),
        .Q(Q[16]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[17] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[17]),
        .Q(Q[17]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[18] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[18]),
        .Q(Q[18]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[19] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[19]),
        .Q(Q[19]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[1]),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[20] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[20]),
        .Q(Q[20]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[21] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[21]),
        .Q(Q[21]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[22] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[22]),
        .Q(Q[22]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[23] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[23]),
        .Q(Q[23]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[2]),
        .Q(Q[2]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[3]),
        .Q(Q[3]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[4] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[4]),
        .Q(Q[4]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[5] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[5]),
        .Q(Q[5]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[6] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[6]),
        .Q(Q[6]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[7] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[7]),
        .Q(Q[7]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[8] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[8]),
        .Q(Q[8]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[9] 
       (.C(CLK),
        .CE(E),
        .CLR(AR),
        .D(D[9]),
        .Q(Q[9]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__0_i_1
       (.I0(Q[7]),
        .I1(Q[23]),
        .I2(out_ROM[7]),
        .O(\q_reg[7]_0 [3]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__0_i_2
       (.I0(Q[6]),
        .I1(Q[23]),
        .I2(out_ROM[6]),
        .O(\q_reg[7]_0 [2]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__0_i_3
       (.I0(Q[5]),
        .I1(Q[23]),
        .I2(out_ROM[5]),
        .O(\q_reg[7]_0 [1]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__0_i_4
       (.I0(Q[4]),
        .I1(Q[23]),
        .I2(out_ROM[4]),
        .O(\q_reg[7]_0 [0]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__1_i_1
       (.I0(Q[11]),
        .I1(Q[23]),
        .I2(out_ROM[11]),
        .O(\q_reg[11]_0 [3]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__1_i_2
       (.I0(Q[10]),
        .I1(Q[23]),
        .I2(out_ROM[10]),
        .O(\q_reg[11]_0 [2]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__1_i_3
       (.I0(Q[9]),
        .I1(Q[23]),
        .I2(out_ROM[9]),
        .O(\q_reg[11]_0 [1]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__1_i_4
       (.I0(Q[8]),
        .I1(Q[23]),
        .I2(out_ROM[8]),
        .O(\q_reg[11]_0 [0]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__2_i_1
       (.I0(Q[15]),
        .I1(Q[23]),
        .I2(out_ROM[15]),
        .O(\q_reg[15]_0 [3]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__2_i_2
       (.I0(Q[14]),
        .I1(Q[23]),
        .I2(out_ROM[14]),
        .O(\q_reg[15]_0 [2]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__2_i_3
       (.I0(Q[13]),
        .I1(Q[23]),
        .I2(out_ROM[13]),
        .O(\q_reg[15]_0 [1]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__2_i_4
       (.I0(Q[12]),
        .I1(Q[23]),
        .I2(out_ROM[12]),
        .O(\q_reg[15]_0 [0]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__3_i_1
       (.I0(Q[19]),
        .I1(Q[23]),
        .I2(out_ROM[19]),
        .O(\q_reg[19]_0 [3]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__3_i_2
       (.I0(Q[18]),
        .I1(Q[23]),
        .I2(out_ROM[18]),
        .O(\q_reg[19]_0 [2]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__3_i_3
       (.I0(Q[17]),
        .I1(Q[23]),
        .I2(out_ROM[17]),
        .O(\q_reg[19]_0 [1]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__3_i_4
       (.I0(Q[16]),
        .I1(Q[23]),
        .I2(out_ROM[16]),
        .O(\q_reg[19]_0 [0]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__4_i_2
       (.I0(Q[22]),
        .I1(Q[23]),
        .I2(out_ROM[22]),
        .O(\q_reg[22]_0 [2]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__4_i_3
       (.I0(Q[21]),
        .I1(Q[23]),
        .I2(out_ROM[21]),
        .O(\q_reg[22]_0 [1]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry__4_i_4
       (.I0(Q[20]),
        .I1(Q[23]),
        .I2(out_ROM[20]),
        .O(\q_reg[22]_0 [0]));
  LUT3 #(
    .INIT(8'h96)) 
    y0_carry__5_i_1__0
       (.I0(Q[23]),
        .I1(y0_carry__5),
        .I2(y0_carry__5_0),
        .O(\q_reg[23]_1 ));
  LUT1 #(
    .INIT(2'h1)) 
    y0_carry_i_1
       (.I0(Q[23]),
        .O(\q_reg[23]_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry_i_2
       (.I0(Q[3]),
        .I1(Q[23]),
        .I2(out_ROM[3]),
        .O(S[3]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry_i_3
       (.I0(Q[2]),
        .I1(Q[23]),
        .I2(out_ROM[2]),
        .O(S[2]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry_i_4
       (.I0(Q[1]),
        .I1(Q[23]),
        .I2(out_ROM[1]),
        .O(S[1]));
  LUT3 #(
    .INIT(8'h69)) 
    y0_carry_i_5__1
       (.I0(Q[0]),
        .I1(Q[23]),
        .I2(out_ROM[0]),
        .O(S[0]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
