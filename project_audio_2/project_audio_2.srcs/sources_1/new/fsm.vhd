----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/25/2023 04:17:30 PM
-- Design Name: 
-- Module Name: fsm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm is
    Port (
        clk, rst, start, ovf: in std_logic; 
        en_counter, clr_counter: out std_logic;
        sel_mux, en_reg : out std_logic
    );
end fsm;

architecture Behavioral of fsm is
    -- signals for state machine
    type state_type is (start_state,compute_state,counter_state);
	signal state: state_type;
begin

    -- next state logic
    process(clk, rst,state,start,ovf)
    begin
        -- rst of fsm 
        if rst = '1' then
            state <= start_state;
        elsif rising_edge(clk) then
           case state is 
            
            when start_state =>
              if start = '1' then
                state <= compute_state;
              else
                state <= start_state;
              end if;
              
            when compute_state => 
                state <= counter_state;
            
            when counter_state =>
                if ovf = '0' then
                 state <= compute_state; 
                else 
                 state <= start_state;
                end if;
            
            when others =>
                state <= start_state;
          end case;      
        end if;
    end process;
    
        -- Output logic
    process(state,start,ovf) 
    begin
        
     case state is
     
      when start_state =>           
        if (start='1' ) then
          clr_counter<='1';        
          en_counter<='1';
          sel_mux <= '1';
          en_reg <= '1';  
        else 
          clr_counter<='0';        
          en_counter<='0';
          sel_mux <= '0';
          en_reg <= '0';  
        end if;
        
        when compute_state =>
          clr_counter<='0';        
          en_counter<='1';
          sel_mux <= '0';
          en_reg <= '1';
          
        when counter_state =>
          clr_counter<='0';        
          en_counter<='0';
          sel_mux <= '0';
          en_reg <= '0';
            
        when others =>
          clr_counter<='0';        
          en_counter<='0';
          sel_mux <= '0';
          en_reg <= '0';  
        end case;
    end process;
    
end Behavioral;






