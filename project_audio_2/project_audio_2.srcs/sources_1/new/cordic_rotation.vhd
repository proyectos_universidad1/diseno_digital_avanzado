----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/25/2023 05:53:47 PM
-- Design Name: 
-- Module Name: cordic_rotation - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cordic_rotation is
   Port (     
       angle: in std_logic_vector (23 downto 0);
       rst, start, clk: in std_logic;
       cos, sin: out std_logic_vector (24 downto 0);
       done : out std_logic
   );
end cordic_rotation;

architecture Behavioral of cordic_rotation is
    -- component for control path
    component control_path is
       Port ( 
           clk, rst, start : in std_logic;
           sel_mux, en_reg, done : out std_logic;
           counter_out: out std_logic_vector(4 downto 0)
       );
    end component;
    
    -- component for data path
    component data_path is 
        generic (N: integer := 25);
        Port ( 
            sel_mux, clk, en_reg, rst: in std_logic;
            addr: in  std_logic_vector(4 downto 0);
            X0, Y0: in std_logic_vector(N-1 downto 0);
            Z0: in std_logic_vector(N-2 downto 0);
            Xn, Yn: out std_logic_vector(N-1 downto 0);
            Zn: out std_logic_vector(N-2 downto 0)
        );
    end component;
    
    constant X0: std_logic_vector (23 downto 0) := std_logic_vector(to_signed(integer(8388607),24));
    constant Y0 : std_logic_vector (23 downto 0) := (others => '0');
    
    signal sel_mux, en_reg: std_logic;
    signal addr: std_logic_vector(4 downto 0);
    signal X0_extend_sign, Y0_extend_sign: std_logic_vector(24 downto 0);
    signal z: std_logic_vector (23 downto 0);
begin
    
     -- sign extension
    X0_extend_sign(24) <= X0(23);
    X0_extend_sign(23 downto 0) <= X0;
     
    Y0_extend_sign(24) <= Y0(23);
    Y0_extend_sign(23 downto 0) <= Y0;
    
    -- instance control path
    control_path0: control_path port map(
        clk => clk,
        rst => rst,
        start => start,
        sel_mux => sel_mux,
        en_reg => en_reg,
        done => done,
        counter_out => addr
    );
    
    -- instance data path
    data_path0: data_path
    generic map (N => 25)
    port map (
       sel_mux => sel_mux,
       clk => clk,
       en_reg => en_reg,
       rst => rst,
       addr => addr,
       X0 => X0_extend_sign, 
       Y0 => Y0_extend_sign,
       Z0 => angle,
       Xn => cos, 
       Yn => sin,
       Zn => z
    );
    
end Behavioral;
