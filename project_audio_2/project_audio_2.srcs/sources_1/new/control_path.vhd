----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/25/2023 05:21:10 PM
-- Design Name: 
-- Module Name: control_path - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity control_path is
   Port ( 
       clk, rst, start : in std_logic;
       sel_mux, en_reg, done : out std_logic;
       counter_out: out std_logic_vector(4 downto 0)
   );
end control_path;

architecture Behavioral of control_path is
    -- component to state machine
    component fsm is
        Port (
            clk, rst, start, ovf: in std_logic; 
            en_counter, clr_counter: out std_logic;
            sel_mux, en_reg : out std_logic
        );
    end component;
    
    -- component to register out
    component reg is
	   generic (N	:integer:=8 );
	   port(
            clk: in 		std_logic;
            en: in std_logic;
            rst: in           std_logic;
            clr: in           std_logic;
            d: in 		std_logic_vector(N-1 downto 0);
            q: out	std_logic_vector(N-1 downto 0)
       );
    end component;
    
    signal ovf, en_counter, clr_counter: std_logic;
    signal adder_out: std_logic_vector(4 downto 0);
    signal counter_out_signal: std_logic_vector(4 downto 0);
begin
    
    -- instance state machine of control path
    fsm0: fsm port map(
        clk => clk, 
        rst => rst,
        start => start,
        ovf => ovf,
        en_counter => en_counter,
        clr_counter => clr_counter,
        sel_mux => sel_mux,
        en_reg => en_reg
    );
    
    -- instance of register out of control path
    -- The counter register
    regr: reg 
    generic map(N=>5)
    port map(
        clk=>clk,
        rst=>rst,
        clr=>clr_counter,
        en=>en_counter,
        d=>adder_out,
        q=>counter_out_signal
    );
    
    -- adder of counter
    adder_out<=std_logic_vector(unsigned(counter_out_signal)+to_unsigned(1,5));
    counter_out <= counter_out_signal;
    
    -- comparator of count
    ovf<='1' when counter_out_signal = std_logic_vector(to_unsigned(24,5)) else '0';    
    done<=ovf;  
    
end Behavioral;




