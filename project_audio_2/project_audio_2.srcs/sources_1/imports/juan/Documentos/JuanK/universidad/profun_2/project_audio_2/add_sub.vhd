----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/23/2023 03:26:42 PM
-- Design Name: 
-- Module Name: add_sub - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- sum or rest when sel take a value 0 or 1

entity add_sub is
    generic(N : integer := 24);
    
    port(
        a, b: in std_logic_vector(N-1 downto 0);
        s : out std_logic_vector(N-1 downto 0);
        sel: in std_logic
    );
     
end add_sub;

architecture Behavioral of add_sub is
begin
   -- if sel = 1 sum, if sel = 0 substract 
   process(a, b, sel)
   begin
        if sel = '0' then
            s <= std_logic_vector(signed(a) + signed(b));
        else
            s<= std_logic_vector(signed(a) - signed(b));
        end if;
   end process;      
end Behavioral;
