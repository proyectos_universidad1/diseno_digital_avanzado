----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/25/2023 06:16:32 PM
-- Design Name: 
-- Module Name: tb_cordic_rotation - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_cordic_rotation is
--  Port ( );
end tb_cordic_rotation;

architecture Behavioral of tb_cordic_rotation is
    -- component for cordic rotation
    component cordic_rotation is
       Port (     
           angle : in std_logic_vector (23 downto 0);
           rst,start, clk: in std_logic;
           cos, sin: out std_logic_vector (24 downto 0);
           done : out std_logic
       );
    end component;
    
   signal angle: std_logic_vector (23 downto 0);
   signal rst,start, clk:  std_logic;
   signal cos, sin: std_logic_vector (24 downto 0);
   signal done :  std_logic;
   
   -- Clock period for simulation
   constant clk_period : time := 10 ns;
begin

-- EUT instance
cordic_rotation0: cordic_rotation port map(
    angle => angle,
    start => start,
    clk => clk,
    rst=>rst,
    cos => cos,
    sin => sin,
    done => done 
); 

process
begin
    rst<='1';
    start<='0';
    angle<=(others=>'0');
    wait for 20*clk_period; -- To wait 100ns reset during timing simulation
    rst<='0';
    angle <= std_logic_vector(to_signed(integer(-2097151),24));
    start<='1';
    wait for clk_period;
    start<='0';
    
wait;
end process;

process
begin
    clk<='0';
    wait for clk_period/2;
    clk<='1';
    wait for clk_period/2;
end process;

end Behavioral;
