----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/15/2023 09:37:17 AM
-- Design Name: 
-- Module Name: tb_project_part1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_project_part1 is
end tb_project_part1;

architecture Behavioral of tb_project_part1 is
    component project_part1 is Port( 
        -- ff tipo D 1
        in_data: in std_logic_vector(3 downto 0);
        en: in std_logic;
        clk: in std_logic;
        leds: out std_logic_vector(3 downto 0);
        -----------------------------------
        
        -- ff tipo D 2
        out_data: out std_logic_vector(3 downto 0);
        buttons: in std_logic_vector(3 downto 0)
    );
    end component;
    
    signal in_data, leds, out_data, buttons: std_logic_vector(3 downto 0);
    signal  en, clk: std_logic;
    constant clk_period : time := 400 ns;
    
begin

    proj: project_part1 port map(
        in_data => in_data,
        leds => leds,
        out_data => out_data,
        buttons => buttons,
        en => en,
        clk => clk
    );
    
    process
    begin
            clk<='0';           
            wait for clk_period/2;
            clk <= '1';            
            wait for clk_period/2;     
     
    end process;
    
    process
    begin
        en<='1';
        wait for clk_period;
        en <= '0';
        wait for clk_period;
        en<='1';
        wait;
    end process;
    
    process 
		begin 
		 in_data <= "1111";
		 buttons <= "0010";
		 wait for clk_period; 
		 in_data <= "0011";
		 buttons <= "0111";
		 wait for clk_period;
		 in_data <= "1011";
		 buttons <= "0110";
		 wait for clk_period;
		 
		wait;
	end process;


end Behavioral;
