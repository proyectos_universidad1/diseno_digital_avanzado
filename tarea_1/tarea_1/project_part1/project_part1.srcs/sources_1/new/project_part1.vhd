----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/13/2023 07:24:19 AM
-- Design Name: 
-- Module Name: project_part1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity project_part1 is Port( 
    -- ff tipo D 1
    in_data: in std_logic_vector(3 downto 0);
    en: in std_logic;
    clk: in std_logic;
    leds: out std_logic_vector(3 downto 0);
    -----------------------------------
    
    -- ff tipo D 2
    out_data: out std_logic_vector(3 downto 0);
    buttons: in std_logic_vector(3 downto 0)
);
end project_part1;

architecture Behavioral of project_part1 is
begin
   -- ff tipo D  1
   process (en, clk)
   begin 
    if rising_edge(clk) then
     if en = '1' then
        leds <= in_data;
    end if;
   end if; 
   end process;
   -------------------
   
   -- ff tipo D 2
   out_data <= buttons when rising_edge(clk);
   
end Behavioral;
