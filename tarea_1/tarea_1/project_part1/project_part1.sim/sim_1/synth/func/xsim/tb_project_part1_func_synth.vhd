-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Sat Feb 25 17:59:53 2023
-- Host        : juan-Inspiron-14-3467 running 64-bit Linux Mint 21.1
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/juan/Documentos/JuanK/universidad/profun_2/tarea_1/project_part1/project_part1.sim/sim_1/synth/func/xsim/tb_project_part1_func_synth.vhd
-- Design      : project_part1
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity project_part1 is
  port (
    in_data : in STD_LOGIC_VECTOR ( 3 downto 0 );
    en : in STD_LOGIC;
    clk : in STD_LOGIC;
    leds : out STD_LOGIC_VECTOR ( 3 downto 0 );
    out_data : out STD_LOGIC_VECTOR ( 3 downto 0 );
    buttons : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of project_part1 : entity is true;
end project_part1;

architecture STRUCTURE of project_part1 is
  signal buttons_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal en_IBUF : STD_LOGIC;
  signal in_data_IBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal leds_OBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal out_data_OBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
\buttons_IBUF[0]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => buttons(0),
      O => buttons_IBUF(0)
    );
\buttons_IBUF[1]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => buttons(1),
      O => buttons_IBUF(1)
    );
\buttons_IBUF[2]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => buttons(2),
      O => buttons_IBUF(2)
    );
\buttons_IBUF[3]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => buttons(3),
      O => buttons_IBUF(3)
    );
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => clk,
      O => clk_IBUF
    );
en_IBUF_inst: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => en,
      O => en_IBUF
    );
\in_data_IBUF[0]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => in_data(0),
      O => in_data_IBUF(0)
    );
\in_data_IBUF[1]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => in_data(1),
      O => in_data_IBUF(1)
    );
\in_data_IBUF[2]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => in_data(2),
      O => in_data_IBUF(2)
    );
\in_data_IBUF[3]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => in_data(3),
      O => in_data_IBUF(3)
    );
\leds_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => leds_OBUF(0),
      O => leds(0)
    );
\leds_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => leds_OBUF(1),
      O => leds(1)
    );
\leds_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => leds_OBUF(2),
      O => leds(2)
    );
\leds_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => leds_OBUF(3),
      O => leds(3)
    );
\leds_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_IBUF,
      D => in_data_IBUF(0),
      Q => leds_OBUF(0),
      R => '0'
    );
\leds_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_IBUF,
      D => in_data_IBUF(1),
      Q => leds_OBUF(1),
      R => '0'
    );
\leds_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_IBUF,
      D => in_data_IBUF(2),
      Q => leds_OBUF(2),
      R => '0'
    );
\leds_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_IBUF,
      D => in_data_IBUF(3),
      Q => leds_OBUF(3),
      R => '0'
    );
\out_data_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => out_data_OBUF(0),
      O => out_data(0)
    );
\out_data_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => out_data_OBUF(1),
      O => out_data(1)
    );
\out_data_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => out_data_OBUF(2),
      O => out_data(2)
    );
\out_data_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => out_data_OBUF(3),
      O => out_data(3)
    );
\out_data_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => buttons_IBUF(0),
      Q => out_data_OBUF(0),
      R => '0'
    );
\out_data_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => buttons_IBUF(1),
      Q => out_data_OBUF(1),
      R => '0'
    );
\out_data_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => buttons_IBUF(2),
      Q => out_data_OBUF(2),
      R => '0'
    );
\out_data_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => buttons_IBUF(3),
      Q => out_data_OBUF(3),
      R => '0'
    );
end STRUCTURE;
