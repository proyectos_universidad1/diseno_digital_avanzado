----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/26/2023 11:19:05 AM
-- Design Name: 
-- Module Name: tb_mux_2_1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_mux_2_1 is
--  Port ( );
end tb_mux_2_1;

architecture Behavioral of tb_mux_2_1 is

    constant mux_width : integer := 8;
    signal sel: std_logic;
    signal in0, in1, y: std_logic_vector(mux_width downto 0);
    
    component Mux_2_1 is 
    --generic (N: integer := 8); 
    Port (
        in0, in1: in std_logic_vector(mux_width downto 0);
        sel: in std_logic; 
        y: out std_logic_vector(mux_width downto 0)
    
     );
     end component;
    
begin

    mux1: Mux_2_1
    --generic map(N => mux_width)
    port map(in0=>in0, in1=>in1,  sel=>sel, y=>y);
    
    process 
		begin 
		 in0 <= "000001111";
		 in1 <= (others => '0');
		 sel <= '0';
		 wait for 1us;  
		 sel <= '1';
		 wait for 1us;
		 in0 <= "100000011";
		 in1 <= "111111111";
		 sel <= '0';
		 wait for 1 us;
		 sel <= '1';
		 wait for 1us;
		 
		wait;
	end process;

end Behavioral;
