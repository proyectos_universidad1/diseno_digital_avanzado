----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/25/2023 06:29:39 PM
-- Design Name: 
-- Module Name: tb_counter_struct - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_counter_struct is
--  Port ( );
end tb_counter_struct;

architecture Behavioral of tb_counter_struct is
    constant clk_period : time := 400 ns;
    constant counter_width : integer := 8;
    signal clk, en, ovf: std_logic;
    signal count: std_logic_vector(counter_width-1 downto 0);
    
    signal aux: std_logic_vector(3 downto 0);
    
    component counter_struct is
    --generic (N: integer := 8); 
    Port ( 
        en, clk: in std_logic;
        count: out std_logic_vector(counter_width-1 downto 0);
        ovf: out std_logic
        );
    end component counter_struct;
begin
    
    counter1: counter_struct 
    --generic map(N => register_width)
    port map(clk=>clk, en=>en, count => count, ovf => ovf);

    process
    begin
            clk<='0';           
            wait for clk_period/2;
            clk <= '1';            
            wait for clk_period/2;     
     
    end process;
    
    process
    begin
        en<='1';
        aux <= std_logic_vector(to_unsigned(17, aux'length) + to_unsigned(1, aux'length));
        wait;
    end process;
	
end Behavioral;
