----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/26/2023 10:39:32 AM
-- Design Name: 
-- Module Name: Mux_2_1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Mux_2_1 is 
    generic (N: integer := 8); 

    Port (
        in0, in1: in std_logic_vector(N downto 0);
        sel: in std_logic; 
        y: out std_logic_vector(N downto 0)
    
     );
end Mux_2_1;

architecture Behavioral of Mux_2_1 is

begin
    with sel select
        y <= in0 when '0',
             in1 when others;
end Behavioral;
