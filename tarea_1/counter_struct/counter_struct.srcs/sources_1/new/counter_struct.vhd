----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/25/2023 06:24:31 PM
-- Design Name: 
-- Module Name: counter_struct - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter_struct is
    generic (N: integer := 8); 
    Port ( 
        en, clk: in std_logic;
        count: out std_logic_vector(N-1 downto 0);
        ovf: out std_logic
        );
end counter_struct;

architecture Behavioral of counter_struct is

    component reg_d is
        generic (N: integer := 8); 
        Port (
            en, clk: in std_logic;
            D: in std_logic_vector(N downto 0);
            Q: out std_logic_vector(N downto 0)
        );
    end component reg_d;
    
    component Mux_2_1 is 
        generic (N: integer := 8); 
        Port (
            in0, in1: in std_logic_vector(N downto 0);
            sel: in std_logic; 
            y: out std_logic_vector(N downto 0)
         );
    end component Mux_2_1;
    
    component sum is
        generic (N	:integer:=8 );
        Port ( 
            a, b: in std_logic_vector(N downto 0);
            s: out std_logic_vector(N downto 0)
        );
    end component sum;
    
    signal adder_out, inD1, scount: std_logic_vector(N downto 0);
begin
    
    sum1: sum 
    generic map (N => N)
    port map(a => scount, b => "000000001", s => adder_out);
    
    mux: Mux_2_1 
    generic map (N => N)
    port map(in0 => adder_out, in1 => (others => '0'), sel => adder_out(8), y => inD1 );

    reg1: reg_d 
    generic map (N => N)
    port map(clk => clk, en => en, D => inD1, Q => scount);
    
    reg2: reg_d 
    generic map (N => N-8)
    port map(clk => clk, en => en, D(0) => adder_out(8), Q(0) => ovf);
    
    count <= scount(7 downto 0);
    
    
end Behavioral;
