-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Tue Mar  7 23:22:01 2023
-- Host        : juan-Inspiron-14-3467 running 64-bit Linux Mint 21.1
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/juan/Documentos/JuanK/universidad/profun_2/tarea_1/counter_struct/counter_struct.sim/sim_1/synth/func/xsim/tb_counter_struct_func_synth.vhd
-- Design      : counter_struct
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity reg_d is
  port (
    count : out STD_LOGIC_VECTOR ( 7 downto 0 );
    D : out STD_LOGIC;
    en : in STD_LOGIC;
    clk : in STD_LOGIC
  );
end reg_d;

architecture STRUCTURE of reg_d is
  signal \D__0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \Q[0]_i_2_n_0\ : STD_LOGIC;
  signal \^count\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Q[0]_i_1__0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \Q[0]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \Q[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \Q[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \Q[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \Q[4]_i_1\ : label is "soft_lutpair0";
begin
  count(7 downto 0) <= \^count\(7 downto 0);
\Q[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \^count\(7),
      I1 => \^count\(5),
      I2 => \^count\(3),
      I3 => \Q[0]_i_2_n_0\,
      I4 => \^count\(4),
      I5 => \^count\(6),
      O => D
    );
\Q[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^count\(0),
      O => \D__0\(0)
    );
\Q[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^count\(2),
      I1 => \^count\(1),
      I2 => \^count\(0),
      O => \Q[0]_i_2_n_0\
    );
\Q[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^count\(0),
      I1 => \^count\(1),
      O => \D__0\(1)
    );
\Q[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^count\(0),
      I1 => \^count\(1),
      I2 => \^count\(2),
      O => \D__0\(2)
    );
\Q[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => \^count\(3),
      I1 => \^count\(0),
      I2 => \^count\(1),
      I3 => \^count\(2),
      O => \D__0\(3)
    );
\Q[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \^count\(4),
      I1 => \^count\(2),
      I2 => \^count\(1),
      I3 => \^count\(0),
      I4 => \^count\(3),
      O => \D__0\(4)
    );
\Q[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^count\(5),
      I1 => \^count\(3),
      I2 => \^count\(0),
      I3 => \^count\(1),
      I4 => \^count\(2),
      I5 => \^count\(4),
      O => \D__0\(5)
    );
\Q[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => \^count\(6),
      I1 => \^count\(4),
      I2 => \Q[0]_i_2_n_0\,
      I3 => \^count\(3),
      I4 => \^count\(5),
      O => \D__0\(6)
    );
\Q[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^count\(7),
      I1 => \^count\(5),
      I2 => \^count\(3),
      I3 => \Q[0]_i_2_n_0\,
      I4 => \^count\(4),
      I5 => \^count\(6),
      O => \D__0\(7)
    );
\Q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \D__0\(0),
      Q => \^count\(0),
      R => '0'
    );
\Q_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \D__0\(1),
      Q => \^count\(1),
      R => '0'
    );
\Q_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \D__0\(2),
      Q => \^count\(2),
      R => '0'
    );
\Q_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \D__0\(3),
      Q => \^count\(3),
      R => '0'
    );
\Q_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \D__0\(4),
      Q => \^count\(4),
      R => '0'
    );
\Q_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \D__0\(5),
      Q => \^count\(5),
      R => '0'
    );
\Q_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \D__0\(6),
      Q => \^count\(6),
      R => '0'
    );
\Q_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => \D__0\(7),
      Q => \^count\(7),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \reg_d__parameterized1\ is
  port (
    ovf : out STD_LOGIC;
    en : in STD_LOGIC;
    D : in STD_LOGIC;
    clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \reg_d__parameterized1\ : entity is "reg_d";
end \reg_d__parameterized1\;

architecture STRUCTURE of \reg_d__parameterized1\ is
begin
\Q_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => en,
      D => D,
      Q => ovf,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity counter_struct is
  port (
    en : in STD_LOGIC;
    clk : in STD_LOGIC;
    count : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ovf : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of counter_struct : entity is true;
  attribute N : integer;
  attribute N of counter_struct : entity is 8;
end counter_struct;

architecture STRUCTURE of counter_struct is
  signal D : STD_LOGIC;
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal count_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal en_IBUF : STD_LOGIC;
  signal ovf_OBUF : STD_LOGIC;
begin
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => clk,
      O => clk_IBUF
    );
\count_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_OBUF(0),
      O => count(0)
    );
\count_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_OBUF(1),
      O => count(1)
    );
\count_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_OBUF(2),
      O => count(2)
    );
\count_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_OBUF(3),
      O => count(3)
    );
\count_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_OBUF(4),
      O => count(4)
    );
\count_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_OBUF(5),
      O => count(5)
    );
\count_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_OBUF(6),
      O => count(6)
    );
\count_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => count_OBUF(7),
      O => count(7)
    );
en_IBUF_inst: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => en,
      O => en_IBUF
    );
ovf_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => ovf_OBUF,
      O => ovf
    );
reg1: entity work.reg_d
     port map (
      D => D,
      clk => clk_IBUF_BUFG,
      count(7 downto 0) => count_OBUF(7 downto 0),
      en => en_IBUF
    );
reg2: entity work.\reg_d__parameterized1\
     port map (
      D => D,
      clk => clk_IBUF_BUFG,
      en => en_IBUF,
      ovf => ovf_OBUF
    );
end STRUCTURE;
