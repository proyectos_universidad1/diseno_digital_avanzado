----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/15/2023 08:20:12 AM
-- Design Name: 
-- Module Name: counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter is
 Port ( en : in STD_LOGIC;
           clk : in STD_LOGIC;
           count : out STD_LOGIC_VECTOR (7 downto 0);
           ovf : out STD_LOGIC);
end counter;

architecture Behavioral of counter is
    signal adder_out, reg_input, scount: std_logic_vector(8 downto 0);
begin
    -- se define el registro de manera secuencial con process
    process(en, clk, reg_input)
    begin 
        if(rising_edge(clk)) then
            if (en = '1') then
               scount <= reg_input;
               --overflow del contador
               ovf <= adder_out(8);
            end if;
        end if;
    end process;
    
    -- multiplexor  (concurrencia)
    reg_input <= adder_out when (adder_out(8) = '0') else (others=>'0');
    
    -- sumador (concurrencia)
    adder_out <= std_logic_vector(unsigned(scount) + to_unsigned(1, 9));
    
    -- conectar la salida
    count <= scount(7 downto 0);

end Behavioral;
