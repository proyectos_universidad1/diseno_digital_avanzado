----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.08.2022 21:15:07
-- Design Name: 
-- Module Name: tb_counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_counter is
--  Port ( );
end tb_counter;

architecture Behavioral of tb_counter is

component counter is port(
clk: in std_logic;
en: in std_logic;
count: out std_logic_vector(7 downto 0);
ovf: out std_logic
);
end component;

signal clk : std_logic;
signal en: std_logic;
signal count: std_logic_vector(7 downto 0);
signal ovf: std_logic;
constant clk_period : time := 400 ns;

begin

counter0: counter port map(
clk=>clk,
en=>en,
count=>count,
ovf=>ovf
);

    process
    begin
            clk<='0';           
            wait for clk_period/2;
            clk <= '1';            
            wait for clk_period/2;     
     
    end process;
    
    process
    begin
        en<='1';
        wait;
    end process;
    
    
--    process
--    begin
--        reset<='1';
--        wait for clk_period;
--        reset<='0';
--        wait;
--    end process;

end architecture;
