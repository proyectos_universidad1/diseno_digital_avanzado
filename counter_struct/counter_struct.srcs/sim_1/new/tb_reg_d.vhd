----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/25/2023 08:50:27 PM
-- Design Name: 
-- Module Name: tb_reg_d - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_reg_d is
--  Port ( );
end tb_reg_d;

architecture Behavioral of tb_reg_d is
    constant clk_period : time := 400 ns;
    constant register_width : integer := 8;
    signal clk, en: std_logic;
    signal D, Q: std_logic_vector(register_width downto 0);
    
    component reg_d is
     --generic (N: integer := 8);
     Port ( 
        en, clk: in std_logic;
        D: in std_logic_vector(register_width downto 0);
        Q: out std_logic_vector(register_width downto 0)
     );
    end component;
begin
    
     reg1: reg_d 
    --generic map(N => register_width)
    port map(clk=>clk, en=>en, D=>D, Q=>Q);

    process
    begin
            clk<='0';           
            wait for clk_period/2;
            clk <= '1';            
            wait for clk_period/2;     
     
    end process;
    
    process
    begin
        en<='1';
        wait for clk_period;
        en <= '0';
        wait for clk_period;
        en<='1';
        wait;
    end process;
    
    process 
		begin 
		 D <= "000001111";
		 wait for clk_period; 
		 D <= "000000011";
		 wait for clk_period;
		 D <= "000001011";
		 wait for clk_period;
		 
		wait;
	end process;

end Behavioral;
