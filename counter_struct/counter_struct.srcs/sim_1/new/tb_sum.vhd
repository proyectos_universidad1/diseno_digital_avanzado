----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/26/2023 11:59:08 AM
-- Design Name: 
-- Module Name: tb_sum - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_sum is
--  Port ( );
end tb_sum;

architecture Behavioral of tb_sum is

    constant sum_width : integer := 8;
    signal a, b, s: std_logic_vector(sum_width downto 0);

    component sum is
        --generic (N	:integer:=8 );
    Port ( 
        a, b: in std_logic_vector(sum_width downto 0);
        s: out std_logic_vector(sum_width downto 0)
    );
    end component;

begin

    sum1: sum
    --generic map(N => mux_width)
    port map(a=>a, b=>b, s=>s);

    process 
		begin 
		 a <= "000001111";
		 b <= "000000001";
		 wait for 1us;
		 a <= "100000011";
		 b <= "000000000";
		 wait for 1us;
		 a <= "011001111";
		 b <= "000000001";
		 wait for 1us;
		 
		wait;
	end process;

end Behavioral;
