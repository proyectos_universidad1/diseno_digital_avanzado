// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Wed Apr 26 09:27:30 2023
// Host        : juan-Inspiron-14-3467 running 64-bit Linux Mint 21.1
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/juan/Documentos/JuanK/universidad/profun_2/digit_serial_divider_mealy/digit_serial_divider_mealy.sim/sim_1/synth/timing/xsim/tb_digit_serial_divider_time_synth.v
// Design      : digit_serial_divider
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module controlpath
   (en_shift,
    E,
    done_OBUF,
    D,
    state_reg,
    clk_IBUF_BUFG,
    AR,
    start_IBUF,
    x_IBUF);
  output en_shift;
  output [0:0]E;
  output done_OBUF;
  output [0:0]D;
  output [0:0]state_reg;
  input clk_IBUF_BUFG;
  input [0:0]AR;
  input start_IBUF;
  input [0:0]x_IBUF;

  wire [0:0]AR;
  wire [0:0]D;
  wire [0:0]E;
  wire clk_IBUF_BUFG;
  wire [0:0]counter_out;
  wire done_OBUF;
  wire en_shift;
  wire fsm0_n_3;
  wire regr_n_0;
  wire start_IBUF;
  wire [0:0]state_reg;
  wire [0:0]x_IBUF;

  fsm fsm0
       (.AR(AR),
        .D(D),
        .E(E),
        .Q(counter_out),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .start_IBUF(start_IBUF),
        .state_reg_0(en_shift),
        .state_reg_1(fsm0_n_3),
        .state_reg_2(state_reg),
        .state_reg_3(regr_n_0),
        .x_IBUF(x_IBUF));
  reg__parameterized2 regr
       (.AR(AR),
        .D(fsm0_n_3),
        .E(E),
        .Q(counter_out),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .done_OBUF(done_OBUF),
        .\q_reg[1]_0 (en_shift),
        .\q_reg[2]_0 (regr_n_0),
        .\q_reg[4]_0 (state_reg),
        .start_IBUF(start_IBUF));
endmodule

module datapath
   (Q,
    x_IBUF,
    start_IBUF,
    en_shift,
    E,
    D,
    clk_IBUF_BUFG,
    AR,
    \q_reg[24]_inv ,
    \q_reg[0] );
  output [23:0]Q;
  input [22:0]x_IBUF;
  input start_IBUF;
  input en_shift;
  input [0:0]E;
  input [23:0]D;
  input clk_IBUF_BUFG;
  input [0:0]AR;
  input [0:0]\q_reg[24]_inv ;
  input [0:0]\q_reg[0] ;

  wire [0:0]AR;
  wire [23:0]D;
  wire [0:0]E;
  wire [23:0]Q;
  wire clk_IBUF_BUFG;
  wire en_shift;
  wire [0:0]\q_reg[0] ;
  wire [0:0]\q_reg[24]_inv ;
  wire regr_n_0;
  wire regr_n_1;
  wire regr_n_10;
  wire regr_n_11;
  wire regr_n_12;
  wire regr_n_13;
  wire regr_n_14;
  wire regr_n_15;
  wire regr_n_16;
  wire regr_n_17;
  wire regr_n_18;
  wire regr_n_19;
  wire regr_n_2;
  wire regr_n_20;
  wire regr_n_21;
  wire regr_n_22;
  wire regr_n_23;
  wire regr_n_24;
  wire regr_n_25;
  wire regr_n_26;
  wire regr_n_27;
  wire regr_n_28;
  wire regr_n_29;
  wire regr_n_3;
  wire regr_n_30;
  wire regr_n_31;
  wire regr_n_32;
  wire regr_n_33;
  wire regr_n_34;
  wire regr_n_35;
  wire regr_n_36;
  wire regr_n_37;
  wire regr_n_38;
  wire regr_n_39;
  wire regr_n_4;
  wire regr_n_40;
  wire regr_n_41;
  wire regr_n_42;
  wire regr_n_43;
  wire regr_n_44;
  wire regr_n_45;
  wire regr_n_46;
  wire regr_n_47;
  wire regr_n_48;
  wire regr_n_5;
  wire regr_n_6;
  wire regr_n_7;
  wire regr_n_8;
  wire regr_n_9;
  wire regy_n_0;
  wire regy_n_1;
  wire regy_n_10;
  wire regy_n_11;
  wire regy_n_12;
  wire regy_n_13;
  wire regy_n_14;
  wire regy_n_15;
  wire regy_n_16;
  wire regy_n_17;
  wire regy_n_18;
  wire regy_n_19;
  wire regy_n_2;
  wire regy_n_20;
  wire regy_n_21;
  wire regy_n_22;
  wire regy_n_23;
  wire regy_n_3;
  wire regy_n_4;
  wire regy_n_5;
  wire regy_n_6;
  wire regy_n_7;
  wire regy_n_8;
  wire regy_n_9;
  wire start_IBUF;
  wire sub_out_carry__0_n_0;
  wire sub_out_carry__0_n_1;
  wire sub_out_carry__0_n_2;
  wire sub_out_carry__0_n_3;
  wire sub_out_carry__0_n_4;
  wire sub_out_carry__0_n_5;
  wire sub_out_carry__0_n_6;
  wire sub_out_carry__0_n_7;
  wire sub_out_carry__1_n_0;
  wire sub_out_carry__1_n_1;
  wire sub_out_carry__1_n_2;
  wire sub_out_carry__1_n_3;
  wire sub_out_carry__1_n_4;
  wire sub_out_carry__1_n_5;
  wire sub_out_carry__1_n_6;
  wire sub_out_carry__1_n_7;
  wire sub_out_carry__2_n_0;
  wire sub_out_carry__2_n_1;
  wire sub_out_carry__2_n_2;
  wire sub_out_carry__2_n_3;
  wire sub_out_carry__2_n_4;
  wire sub_out_carry__2_n_5;
  wire sub_out_carry__2_n_6;
  wire sub_out_carry__2_n_7;
  wire sub_out_carry__3_n_0;
  wire sub_out_carry__3_n_1;
  wire sub_out_carry__3_n_2;
  wire sub_out_carry__3_n_3;
  wire sub_out_carry__3_n_4;
  wire sub_out_carry__3_n_5;
  wire sub_out_carry__3_n_6;
  wire sub_out_carry__3_n_7;
  wire sub_out_carry__4_n_0;
  wire sub_out_carry__4_n_1;
  wire sub_out_carry__4_n_2;
  wire sub_out_carry__4_n_3;
  wire sub_out_carry__4_n_4;
  wire sub_out_carry__4_n_5;
  wire sub_out_carry__4_n_6;
  wire sub_out_carry__4_n_7;
  wire sub_out_carry__5_n_7;
  wire sub_out_carry_n_0;
  wire sub_out_carry_n_1;
  wire sub_out_carry_n_2;
  wire sub_out_carry_n_3;
  wire sub_out_carry_n_4;
  wire sub_out_carry_n_5;
  wire sub_out_carry_n_6;
  wire sub_out_carry_n_7;
  wire [22:0]x_IBUF;
  wire [3:0]NLW_sub_out_carry__5_CO_UNCONNECTED;
  wire [3:1]NLW_sub_out_carry__5_O_UNCONNECTED;

  \reg  regr
       (.AR(AR),
        .O(sub_out_carry__5_n_7),
        .Q({regr_n_0,regr_n_1,regr_n_2,regr_n_3,regr_n_4,regr_n_5,regr_n_6,regr_n_7,regr_n_8,regr_n_9,regr_n_10,regr_n_11,regr_n_12,regr_n_13,regr_n_14,regr_n_15,regr_n_16,regr_n_17,regr_n_18,regr_n_19,regr_n_20,regr_n_21,regr_n_22,regr_n_23}),
        .S({regr_n_24,regr_n_25,regr_n_26,regr_n_27}),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .en_shift(en_shift),
        .\q_reg[0]_0 (\q_reg[0] ),
        .\q_reg[11]_0 ({regr_n_32,regr_n_33,regr_n_34,regr_n_35}),
        .\q_reg[12]_0 ({sub_out_carry__1_n_4,sub_out_carry__1_n_5,sub_out_carry__1_n_6,sub_out_carry__1_n_7}),
        .\q_reg[15]_0 ({regr_n_36,regr_n_37,regr_n_38,regr_n_39}),
        .\q_reg[16]_0 ({sub_out_carry__2_n_4,sub_out_carry__2_n_5,sub_out_carry__2_n_6,sub_out_carry__2_n_7}),
        .\q_reg[19]_0 ({regr_n_40,regr_n_41,regr_n_42,regr_n_43}),
        .\q_reg[20]_0 ({sub_out_carry__3_n_4,sub_out_carry__3_n_5,sub_out_carry__3_n_6,sub_out_carry__3_n_7}),
        .\q_reg[23]_0 ({regr_n_44,regr_n_45,regr_n_46,regr_n_47}),
        .\q_reg[24]_inv_0 (regr_n_48),
        .\q_reg[24]_inv_1 ({sub_out_carry__4_n_4,sub_out_carry__4_n_5,sub_out_carry__4_n_6,sub_out_carry__4_n_7}),
        .\q_reg[24]_inv_2 (\q_reg[24]_inv ),
        .\q_reg[4]_0 ({sub_out_carry_n_4,sub_out_carry_n_5,sub_out_carry_n_6,sub_out_carry_n_7}),
        .\q_reg[7]_0 ({regr_n_28,regr_n_29,regr_n_30,regr_n_31}),
        .\q_reg[8]_0 ({sub_out_carry__0_n_4,sub_out_carry__0_n_5,sub_out_carry__0_n_6,sub_out_carry__0_n_7}),
        .start_IBUF(start_IBUF),
        .sub_out_carry__4({regy_n_0,regy_n_1,regy_n_2,regy_n_3,regy_n_4,regy_n_5,regy_n_6,regy_n_7,regy_n_8,regy_n_9,regy_n_10,regy_n_11,regy_n_12,regy_n_13,regy_n_14,regy_n_15,regy_n_16,regy_n_17,regy_n_18,regy_n_19,regy_n_20,regy_n_21,regy_n_22,regy_n_23}),
        .x_IBUF(x_IBUF));
  reg_0 regy
       (.AR(AR),
        .D(D),
        .E(E),
        .Q({regy_n_0,regy_n_1,regy_n_2,regy_n_3,regy_n_4,regy_n_5,regy_n_6,regy_n_7,regy_n_8,regy_n_9,regy_n_10,regy_n_11,regy_n_12,regy_n_13,regy_n_14,regy_n_15,regy_n_16,regy_n_17,regy_n_18,regy_n_19,regy_n_20,regy_n_21,regy_n_22,regy_n_23}),
        .clk_IBUF_BUFG(clk_IBUF_BUFG));
  shift_reg shift_reg0
       (.AR(AR),
        .O(sub_out_carry__5_n_7),
        .Q(Q),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .en_shift(en_shift));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sub_out_carry
       (.CI(1'b0),
        .CO({sub_out_carry_n_0,sub_out_carry_n_1,sub_out_carry_n_2,sub_out_carry_n_3}),
        .CYINIT(1'b1),
        .DI({regr_n_20,regr_n_21,regr_n_22,regr_n_23}),
        .O({sub_out_carry_n_4,sub_out_carry_n_5,sub_out_carry_n_6,sub_out_carry_n_7}),
        .S({regr_n_24,regr_n_25,regr_n_26,regr_n_27}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sub_out_carry__0
       (.CI(sub_out_carry_n_0),
        .CO({sub_out_carry__0_n_0,sub_out_carry__0_n_1,sub_out_carry__0_n_2,sub_out_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({regr_n_16,regr_n_17,regr_n_18,regr_n_19}),
        .O({sub_out_carry__0_n_4,sub_out_carry__0_n_5,sub_out_carry__0_n_6,sub_out_carry__0_n_7}),
        .S({regr_n_28,regr_n_29,regr_n_30,regr_n_31}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sub_out_carry__1
       (.CI(sub_out_carry__0_n_0),
        .CO({sub_out_carry__1_n_0,sub_out_carry__1_n_1,sub_out_carry__1_n_2,sub_out_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({regr_n_12,regr_n_13,regr_n_14,regr_n_15}),
        .O({sub_out_carry__1_n_4,sub_out_carry__1_n_5,sub_out_carry__1_n_6,sub_out_carry__1_n_7}),
        .S({regr_n_32,regr_n_33,regr_n_34,regr_n_35}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sub_out_carry__2
       (.CI(sub_out_carry__1_n_0),
        .CO({sub_out_carry__2_n_0,sub_out_carry__2_n_1,sub_out_carry__2_n_2,sub_out_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({regr_n_8,regr_n_9,regr_n_10,regr_n_11}),
        .O({sub_out_carry__2_n_4,sub_out_carry__2_n_5,sub_out_carry__2_n_6,sub_out_carry__2_n_7}),
        .S({regr_n_36,regr_n_37,regr_n_38,regr_n_39}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sub_out_carry__3
       (.CI(sub_out_carry__2_n_0),
        .CO({sub_out_carry__3_n_0,sub_out_carry__3_n_1,sub_out_carry__3_n_2,sub_out_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({regr_n_4,regr_n_5,regr_n_6,regr_n_7}),
        .O({sub_out_carry__3_n_4,sub_out_carry__3_n_5,sub_out_carry__3_n_6,sub_out_carry__3_n_7}),
        .S({regr_n_40,regr_n_41,regr_n_42,regr_n_43}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sub_out_carry__4
       (.CI(sub_out_carry__3_n_0),
        .CO({sub_out_carry__4_n_0,sub_out_carry__4_n_1,sub_out_carry__4_n_2,sub_out_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({regr_n_0,regr_n_1,regr_n_2,regr_n_3}),
        .O({sub_out_carry__4_n_4,sub_out_carry__4_n_5,sub_out_carry__4_n_6,sub_out_carry__4_n_7}),
        .S({regr_n_44,regr_n_45,regr_n_46,regr_n_47}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 sub_out_carry__5
       (.CI(sub_out_carry__4_n_0),
        .CO(NLW_sub_out_carry__5_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_sub_out_carry__5_O_UNCONNECTED[3:1],sub_out_carry__5_n_7}),
        .S({1'b0,1'b0,1'b0,regr_n_48}));
endmodule

(* NotValidForBitStream *)
module digit_serial_divider
   (x,
    y,
    q,
    start,
    done,
    clk,
    rst);
  input [23:0]x;
  input [23:0]y;
  output [23:0]q;
  input start;
  output done;
  input clk;
  input rst;

  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire controlpath0_n_3;
  wire controlpath0_n_4;
  wire done;
  wire done_OBUF;
  wire en_r;
  wire en_shift;
  wire [23:0]q;
  wire [23:0]q_OBUF;
  wire rst;
  wire rst_IBUF;
  wire start;
  wire start_IBUF;
  wire [23:0]x;
  wire [23:0]x_IBUF;
  wire [23:0]y;
  wire [23:0]y_IBUF;

initial begin
 $sdf_annotate("tb_digit_serial_divider_time_synth.sdf",,,,"tool_control");
end
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF #(
    .CCIO_EN("TRUE")) 
    clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  controlpath controlpath0
       (.AR(rst_IBUF),
        .D(controlpath0_n_3),
        .E(en_r),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .done_OBUF(done_OBUF),
        .en_shift(en_shift),
        .start_IBUF(start_IBUF),
        .state_reg(controlpath0_n_4),
        .x_IBUF(x_IBUF[0]));
  datapath datapath0
       (.AR(rst_IBUF),
        .D(y_IBUF),
        .E(controlpath0_n_4),
        .Q(q_OBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .en_shift(en_shift),
        .\q_reg[0] (controlpath0_n_3),
        .\q_reg[24]_inv (en_r),
        .start_IBUF(start_IBUF),
        .x_IBUF(x_IBUF[23:1]));
  OBUF done_OBUF_inst
       (.I(done_OBUF),
        .O(done));
  OBUF \q_OBUF[0]_inst 
       (.I(q_OBUF[0]),
        .O(q[0]));
  OBUF \q_OBUF[10]_inst 
       (.I(q_OBUF[10]),
        .O(q[10]));
  OBUF \q_OBUF[11]_inst 
       (.I(q_OBUF[11]),
        .O(q[11]));
  OBUF \q_OBUF[12]_inst 
       (.I(q_OBUF[12]),
        .O(q[12]));
  OBUF \q_OBUF[13]_inst 
       (.I(q_OBUF[13]),
        .O(q[13]));
  OBUF \q_OBUF[14]_inst 
       (.I(q_OBUF[14]),
        .O(q[14]));
  OBUF \q_OBUF[15]_inst 
       (.I(q_OBUF[15]),
        .O(q[15]));
  OBUF \q_OBUF[16]_inst 
       (.I(q_OBUF[16]),
        .O(q[16]));
  OBUF \q_OBUF[17]_inst 
       (.I(q_OBUF[17]),
        .O(q[17]));
  OBUF \q_OBUF[18]_inst 
       (.I(q_OBUF[18]),
        .O(q[18]));
  OBUF \q_OBUF[19]_inst 
       (.I(q_OBUF[19]),
        .O(q[19]));
  OBUF \q_OBUF[1]_inst 
       (.I(q_OBUF[1]),
        .O(q[1]));
  OBUF \q_OBUF[20]_inst 
       (.I(q_OBUF[20]),
        .O(q[20]));
  OBUF \q_OBUF[21]_inst 
       (.I(q_OBUF[21]),
        .O(q[21]));
  OBUF \q_OBUF[22]_inst 
       (.I(q_OBUF[22]),
        .O(q[22]));
  OBUF \q_OBUF[23]_inst 
       (.I(q_OBUF[23]),
        .O(q[23]));
  OBUF \q_OBUF[2]_inst 
       (.I(q_OBUF[2]),
        .O(q[2]));
  OBUF \q_OBUF[3]_inst 
       (.I(q_OBUF[3]),
        .O(q[3]));
  OBUF \q_OBUF[4]_inst 
       (.I(q_OBUF[4]),
        .O(q[4]));
  OBUF \q_OBUF[5]_inst 
       (.I(q_OBUF[5]),
        .O(q[5]));
  OBUF \q_OBUF[6]_inst 
       (.I(q_OBUF[6]),
        .O(q[6]));
  OBUF \q_OBUF[7]_inst 
       (.I(q_OBUF[7]),
        .O(q[7]));
  OBUF \q_OBUF[8]_inst 
       (.I(q_OBUF[8]),
        .O(q[8]));
  OBUF \q_OBUF[9]_inst 
       (.I(q_OBUF[9]),
        .O(q[9]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    rst_IBUF_inst
       (.I(rst),
        .O(rst_IBUF));
  IBUF #(
    .CCIO_EN("TRUE")) 
    start_IBUF_inst
       (.I(start),
        .O(start_IBUF));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[0]_inst 
       (.I(x[0]),
        .O(x_IBUF[0]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[10]_inst 
       (.I(x[10]),
        .O(x_IBUF[10]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[11]_inst 
       (.I(x[11]),
        .O(x_IBUF[11]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[12]_inst 
       (.I(x[12]),
        .O(x_IBUF[12]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[13]_inst 
       (.I(x[13]),
        .O(x_IBUF[13]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[14]_inst 
       (.I(x[14]),
        .O(x_IBUF[14]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[15]_inst 
       (.I(x[15]),
        .O(x_IBUF[15]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[16]_inst 
       (.I(x[16]),
        .O(x_IBUF[16]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[17]_inst 
       (.I(x[17]),
        .O(x_IBUF[17]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[18]_inst 
       (.I(x[18]),
        .O(x_IBUF[18]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[19]_inst 
       (.I(x[19]),
        .O(x_IBUF[19]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[1]_inst 
       (.I(x[1]),
        .O(x_IBUF[1]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[20]_inst 
       (.I(x[20]),
        .O(x_IBUF[20]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[21]_inst 
       (.I(x[21]),
        .O(x_IBUF[21]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[22]_inst 
       (.I(x[22]),
        .O(x_IBUF[22]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[23]_inst 
       (.I(x[23]),
        .O(x_IBUF[23]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[2]_inst 
       (.I(x[2]),
        .O(x_IBUF[2]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[3]_inst 
       (.I(x[3]),
        .O(x_IBUF[3]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[4]_inst 
       (.I(x[4]),
        .O(x_IBUF[4]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[5]_inst 
       (.I(x[5]),
        .O(x_IBUF[5]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[6]_inst 
       (.I(x[6]),
        .O(x_IBUF[6]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[7]_inst 
       (.I(x[7]),
        .O(x_IBUF[7]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[8]_inst 
       (.I(x[8]),
        .O(x_IBUF[8]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \x_IBUF[9]_inst 
       (.I(x[9]),
        .O(x_IBUF[9]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[0]_inst 
       (.I(y[0]),
        .O(y_IBUF[0]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[10]_inst 
       (.I(y[10]),
        .O(y_IBUF[10]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[11]_inst 
       (.I(y[11]),
        .O(y_IBUF[11]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[12]_inst 
       (.I(y[12]),
        .O(y_IBUF[12]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[13]_inst 
       (.I(y[13]),
        .O(y_IBUF[13]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[14]_inst 
       (.I(y[14]),
        .O(y_IBUF[14]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[15]_inst 
       (.I(y[15]),
        .O(y_IBUF[15]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[16]_inst 
       (.I(y[16]),
        .O(y_IBUF[16]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[17]_inst 
       (.I(y[17]),
        .O(y_IBUF[17]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[18]_inst 
       (.I(y[18]),
        .O(y_IBUF[18]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[19]_inst 
       (.I(y[19]),
        .O(y_IBUF[19]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[1]_inst 
       (.I(y[1]),
        .O(y_IBUF[1]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[20]_inst 
       (.I(y[20]),
        .O(y_IBUF[20]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[21]_inst 
       (.I(y[21]),
        .O(y_IBUF[21]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[22]_inst 
       (.I(y[22]),
        .O(y_IBUF[22]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[23]_inst 
       (.I(y[23]),
        .O(y_IBUF[23]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[2]_inst 
       (.I(y[2]),
        .O(y_IBUF[2]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[3]_inst 
       (.I(y[3]),
        .O(y_IBUF[3]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[4]_inst 
       (.I(y[4]),
        .O(y_IBUF[4]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[5]_inst 
       (.I(y[5]),
        .O(y_IBUF[5]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[6]_inst 
       (.I(y[6]),
        .O(y_IBUF[6]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[7]_inst 
       (.I(y[7]),
        .O(y_IBUF[7]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[8]_inst 
       (.I(y[8]),
        .O(y_IBUF[8]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    \y_IBUF[9]_inst 
       (.I(y[9]),
        .O(y_IBUF[9]));
endmodule

module fsm
   (state_reg_0,
    E,
    D,
    state_reg_1,
    state_reg_2,
    state_reg_3,
    clk_IBUF_BUFG,
    AR,
    start_IBUF,
    x_IBUF,
    Q);
  output state_reg_0;
  output [0:0]E;
  output [0:0]D;
  output [0:0]state_reg_1;
  output [0:0]state_reg_2;
  input state_reg_3;
  input clk_IBUF_BUFG;
  input [0:0]AR;
  input start_IBUF;
  input [0:0]x_IBUF;
  input [0:0]Q;

  wire [0:0]AR;
  wire [0:0]D;
  wire [0:0]E;
  wire [0:0]Q;
  wire clk_IBUF_BUFG;
  wire start_IBUF;
  wire state_reg_0;
  wire [0:0]state_reg_1;
  wire [0:0]state_reg_2;
  wire state_reg_3;
  wire [0:0]x_IBUF;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \q[0]_i_1 
       (.I0(state_reg_0),
        .I1(start_IBUF),
        .I2(x_IBUF),
        .O(D));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h0B)) 
    \q[0]_i_1__0 
       (.I0(state_reg_0),
        .I1(start_IBUF),
        .I2(Q),
        .O(state_reg_1));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \q[23]_i_1__0 
       (.I0(start_IBUF),
        .I1(state_reg_0),
        .O(state_reg_2));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \q[4]_i_1 
       (.I0(state_reg_0),
        .I1(start_IBUF),
        .O(E));
  FDCE #(
    .INIT(1'b0)) 
    state_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(AR),
        .D(state_reg_3),
        .Q(state_reg_0));
endmodule

module \reg 
   (Q,
    S,
    \q_reg[7]_0 ,
    \q_reg[11]_0 ,
    \q_reg[15]_0 ,
    \q_reg[19]_0 ,
    \q_reg[23]_0 ,
    \q_reg[24]_inv_0 ,
    x_IBUF,
    start_IBUF,
    en_shift,
    O,
    \q_reg[4]_0 ,
    \q_reg[8]_0 ,
    \q_reg[12]_0 ,
    \q_reg[16]_0 ,
    \q_reg[20]_0 ,
    \q_reg[24]_inv_1 ,
    sub_out_carry__4,
    \q_reg[24]_inv_2 ,
    clk_IBUF_BUFG,
    AR,
    \q_reg[0]_0 );
  output [23:0]Q;
  output [3:0]S;
  output [3:0]\q_reg[7]_0 ;
  output [3:0]\q_reg[11]_0 ;
  output [3:0]\q_reg[15]_0 ;
  output [3:0]\q_reg[19]_0 ;
  output [3:0]\q_reg[23]_0 ;
  output [0:0]\q_reg[24]_inv_0 ;
  input [22:0]x_IBUF;
  input start_IBUF;
  input en_shift;
  input [0:0]O;
  input [3:0]\q_reg[4]_0 ;
  input [3:0]\q_reg[8]_0 ;
  input [3:0]\q_reg[12]_0 ;
  input [3:0]\q_reg[16]_0 ;
  input [3:0]\q_reg[20]_0 ;
  input [3:0]\q_reg[24]_inv_1 ;
  input [23:0]sub_out_carry__4;
  input [0:0]\q_reg[24]_inv_2 ;
  input clk_IBUF_BUFG;
  input [0:0]AR;
  input [0:0]\q_reg[0]_0 ;

  wire [0:0]AR;
  wire [0:0]O;
  wire [23:0]Q;
  wire [3:0]S;
  wire clk_IBUF_BUFG;
  wire en_shift;
  wire \q[10]_i_1_n_0 ;
  wire \q[11]_i_1_n_0 ;
  wire \q[12]_i_1_n_0 ;
  wire \q[13]_i_1_n_0 ;
  wire \q[14]_i_1_n_0 ;
  wire \q[15]_i_1_n_0 ;
  wire \q[16]_i_1_n_0 ;
  wire \q[17]_i_1_n_0 ;
  wire \q[18]_i_1_n_0 ;
  wire \q[19]_i_1_n_0 ;
  wire \q[1]_i_1_n_0 ;
  wire \q[20]_i_1_n_0 ;
  wire \q[21]_i_1_n_0 ;
  wire \q[22]_i_1_n_0 ;
  wire \q[23]_i_1_n_0 ;
  wire \q[24]_inv_i_1_n_0 ;
  wire \q[2]_i_1_n_0 ;
  wire \q[3]_i_1_n_0 ;
  wire \q[4]_i_1__0_n_0 ;
  wire \q[5]_i_1_n_0 ;
  wire \q[6]_i_1_n_0 ;
  wire \q[7]_i_1_n_0 ;
  wire \q[8]_i_1_n_0 ;
  wire \q[9]_i_1_n_0 ;
  wire [0:0]\q_reg[0]_0 ;
  wire [3:0]\q_reg[11]_0 ;
  wire [3:0]\q_reg[12]_0 ;
  wire [3:0]\q_reg[15]_0 ;
  wire [3:0]\q_reg[16]_0 ;
  wire [3:0]\q_reg[19]_0 ;
  wire [3:0]\q_reg[20]_0 ;
  wire [3:0]\q_reg[23]_0 ;
  wire [0:0]\q_reg[24]_inv_0 ;
  wire [3:0]\q_reg[24]_inv_1 ;
  wire [0:0]\q_reg[24]_inv_2 ;
  wire [3:0]\q_reg[4]_0 ;
  wire [3:0]\q_reg[7]_0 ;
  wire [3:0]\q_reg[8]_0 ;
  wire start_IBUF;
  wire [23:0]sub_out_carry__4;
  wire [22:0]x_IBUF;

  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[10]_i_1 
       (.I0(x_IBUF[9]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[9]),
        .I4(O),
        .I5(\q_reg[12]_0 [1]),
        .O(\q[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[11]_i_1 
       (.I0(x_IBUF[10]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[10]),
        .I4(O),
        .I5(\q_reg[12]_0 [2]),
        .O(\q[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[12]_i_1 
       (.I0(x_IBUF[11]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[11]),
        .I4(O),
        .I5(\q_reg[12]_0 [3]),
        .O(\q[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[13]_i_1 
       (.I0(x_IBUF[12]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[12]),
        .I4(O),
        .I5(\q_reg[16]_0 [0]),
        .O(\q[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[14]_i_1 
       (.I0(x_IBUF[13]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[13]),
        .I4(O),
        .I5(\q_reg[16]_0 [1]),
        .O(\q[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[15]_i_1 
       (.I0(x_IBUF[14]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[14]),
        .I4(O),
        .I5(\q_reg[16]_0 [2]),
        .O(\q[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[16]_i_1 
       (.I0(x_IBUF[15]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[15]),
        .I4(O),
        .I5(\q_reg[16]_0 [3]),
        .O(\q[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[17]_i_1 
       (.I0(x_IBUF[16]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[16]),
        .I4(O),
        .I5(\q_reg[20]_0 [0]),
        .O(\q[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[18]_i_1 
       (.I0(x_IBUF[17]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[17]),
        .I4(O),
        .I5(\q_reg[20]_0 [1]),
        .O(\q[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[19]_i_1 
       (.I0(x_IBUF[18]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[18]),
        .I4(O),
        .I5(\q_reg[20]_0 [2]),
        .O(\q[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[1]_i_1 
       (.I0(x_IBUF[0]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[0]),
        .I4(O),
        .I5(\q_reg[4]_0 [0]),
        .O(\q[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[20]_i_1 
       (.I0(x_IBUF[19]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[19]),
        .I4(O),
        .I5(\q_reg[20]_0 [3]),
        .O(\q[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[21]_i_1 
       (.I0(x_IBUF[20]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[20]),
        .I4(O),
        .I5(\q_reg[24]_inv_1 [0]),
        .O(\q[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[22]_i_1 
       (.I0(x_IBUF[21]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[21]),
        .I4(O),
        .I5(\q_reg[24]_inv_1 [1]),
        .O(\q[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[23]_i_1 
       (.I0(x_IBUF[22]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[22]),
        .I4(O),
        .I5(\q_reg[24]_inv_1 [2]),
        .O(\q[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h1DFF1D1D)) 
    \q[24]_inv_i_1 
       (.I0(\q_reg[24]_inv_1 [3]),
        .I1(O),
        .I2(Q[23]),
        .I3(en_shift),
        .I4(start_IBUF),
        .O(\q[24]_inv_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[2]_i_1 
       (.I0(x_IBUF[1]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[1]),
        .I4(O),
        .I5(\q_reg[4]_0 [1]),
        .O(\q[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[3]_i_1 
       (.I0(x_IBUF[2]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[2]),
        .I4(O),
        .I5(\q_reg[4]_0 [2]),
        .O(\q[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[4]_i_1__0 
       (.I0(x_IBUF[3]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[3]),
        .I4(O),
        .I5(\q_reg[4]_0 [3]),
        .O(\q[4]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[5]_i_1 
       (.I0(x_IBUF[4]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[4]),
        .I4(O),
        .I5(\q_reg[8]_0 [0]),
        .O(\q[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[6]_i_1 
       (.I0(x_IBUF[5]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[5]),
        .I4(O),
        .I5(\q_reg[8]_0 [1]),
        .O(\q[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[7]_i_1 
       (.I0(x_IBUF[6]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[6]),
        .I4(O),
        .I5(\q_reg[8]_0 [2]),
        .O(\q[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[8]_i_1 
       (.I0(x_IBUF[7]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[7]),
        .I4(O),
        .I5(\q_reg[8]_0 [3]),
        .O(\q[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFB08FBFBFB080808)) 
    \q[9]_i_1 
       (.I0(x_IBUF[8]),
        .I1(start_IBUF),
        .I2(en_shift),
        .I3(Q[8]),
        .I4(O),
        .I5(\q_reg[12]_0 [0]),
        .O(\q[9]_i_1_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q_reg[0]_0 ),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[10]_i_1_n_0 ),
        .Q(Q[10]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[11]_i_1_n_0 ),
        .Q(Q[11]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[12]_i_1_n_0 ),
        .Q(Q[12]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[13]_i_1_n_0 ),
        .Q(Q[13]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[14]_i_1_n_0 ),
        .Q(Q[14]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[15]_i_1_n_0 ),
        .Q(Q[15]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[16]_i_1_n_0 ),
        .Q(Q[16]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[17]_i_1_n_0 ),
        .Q(Q[17]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[18]_i_1_n_0 ),
        .Q(Q[18]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[19]_i_1_n_0 ),
        .Q(Q[19]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[1]_i_1_n_0 ),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[20]_i_1_n_0 ),
        .Q(Q[20]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[21]_i_1_n_0 ),
        .Q(Q[21]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[22]_i_1_n_0 ),
        .Q(Q[22]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[23]_i_1_n_0 ),
        .Q(Q[23]));
  (* inverted = "yes" *) 
  FDPE #(
    .INIT(1'b1)) 
    \q_reg[24]_inv 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .D(\q[24]_inv_i_1_n_0 ),
        .PRE(AR),
        .Q(\q_reg[24]_inv_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[2]_i_1_n_0 ),
        .Q(Q[2]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[3]_i_1_n_0 ),
        .Q(Q[3]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[4]_i_1__0_n_0 ),
        .Q(Q[4]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[5]_i_1_n_0 ),
        .Q(Q[5]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[6]_i_1_n_0 ),
        .Q(Q[6]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[7]_i_1_n_0 ),
        .Q(Q[7]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[8]_i_1_n_0 ),
        .Q(Q[8]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(\q_reg[24]_inv_2 ),
        .CLR(AR),
        .D(\q[9]_i_1_n_0 ),
        .Q(Q[9]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__0_i_1
       (.I0(Q[7]),
        .I1(sub_out_carry__4[7]),
        .O(\q_reg[7]_0 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__0_i_2
       (.I0(Q[6]),
        .I1(sub_out_carry__4[6]),
        .O(\q_reg[7]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__0_i_3
       (.I0(Q[5]),
        .I1(sub_out_carry__4[5]),
        .O(\q_reg[7]_0 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__0_i_4
       (.I0(Q[4]),
        .I1(sub_out_carry__4[4]),
        .O(\q_reg[7]_0 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__1_i_1
       (.I0(Q[11]),
        .I1(sub_out_carry__4[11]),
        .O(\q_reg[11]_0 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__1_i_2
       (.I0(Q[10]),
        .I1(sub_out_carry__4[10]),
        .O(\q_reg[11]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__1_i_3
       (.I0(Q[9]),
        .I1(sub_out_carry__4[9]),
        .O(\q_reg[11]_0 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__1_i_4
       (.I0(Q[8]),
        .I1(sub_out_carry__4[8]),
        .O(\q_reg[11]_0 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__2_i_1
       (.I0(Q[15]),
        .I1(sub_out_carry__4[15]),
        .O(\q_reg[15]_0 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__2_i_2
       (.I0(Q[14]),
        .I1(sub_out_carry__4[14]),
        .O(\q_reg[15]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__2_i_3
       (.I0(Q[13]),
        .I1(sub_out_carry__4[13]),
        .O(\q_reg[15]_0 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__2_i_4
       (.I0(Q[12]),
        .I1(sub_out_carry__4[12]),
        .O(\q_reg[15]_0 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__3_i_1
       (.I0(Q[19]),
        .I1(sub_out_carry__4[19]),
        .O(\q_reg[19]_0 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__3_i_2
       (.I0(Q[18]),
        .I1(sub_out_carry__4[18]),
        .O(\q_reg[19]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__3_i_3
       (.I0(Q[17]),
        .I1(sub_out_carry__4[17]),
        .O(\q_reg[19]_0 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__3_i_4
       (.I0(Q[16]),
        .I1(sub_out_carry__4[16]),
        .O(\q_reg[19]_0 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__4_i_1
       (.I0(Q[23]),
        .I1(sub_out_carry__4[23]),
        .O(\q_reg[23]_0 [3]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__4_i_2
       (.I0(Q[22]),
        .I1(sub_out_carry__4[22]),
        .O(\q_reg[23]_0 [2]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__4_i_3
       (.I0(Q[21]),
        .I1(sub_out_carry__4[21]),
        .O(\q_reg[23]_0 [1]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry__4_i_4
       (.I0(Q[20]),
        .I1(sub_out_carry__4[20]),
        .O(\q_reg[23]_0 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry_i_1
       (.I0(Q[3]),
        .I1(sub_out_carry__4[3]),
        .O(S[3]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry_i_2
       (.I0(Q[2]),
        .I1(sub_out_carry__4[2]),
        .O(S[2]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry_i_3
       (.I0(Q[1]),
        .I1(sub_out_carry__4[1]),
        .O(S[1]));
  LUT2 #(
    .INIT(4'h9)) 
    sub_out_carry_i_4
       (.I0(Q[0]),
        .I1(sub_out_carry__4[0]),
        .O(S[0]));
endmodule

(* ORIG_REF_NAME = "reg" *) 
module reg_0
   (Q,
    E,
    D,
    clk_IBUF_BUFG,
    AR);
  output [23:0]Q;
  input [0:0]E;
  input [23:0]D;
  input clk_IBUF_BUFG;
  input [0:0]AR;

  wire [0:0]AR;
  wire [23:0]D;
  wire [0:0]E;
  wire [23:0]Q;
  wire clk_IBUF_BUFG;

  FDCE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[0]),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[10]),
        .Q(Q[10]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[11]),
        .Q(Q[11]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[12]),
        .Q(Q[12]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[13]),
        .Q(Q[13]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[14]),
        .Q(Q[14]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[15]),
        .Q(Q[15]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[16]),
        .Q(Q[16]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[17]),
        .Q(Q[17]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[18]),
        .Q(Q[18]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[19]),
        .Q(Q[19]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[1]),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[20]),
        .Q(Q[20]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[21]),
        .Q(Q[21]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[22]),
        .Q(Q[22]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[23]),
        .Q(Q[23]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[2]),
        .Q(Q[2]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[3]),
        .Q(Q[3]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[4]),
        .Q(Q[4]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[5]),
        .Q(Q[5]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[6]),
        .Q(Q[6]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[7]),
        .Q(Q[7]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[8]),
        .Q(Q[8]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D[9]),
        .Q(Q[9]));
endmodule

(* ORIG_REF_NAME = "reg" *) 
module reg__parameterized2
   (\q_reg[2]_0 ,
    Q,
    done_OBUF,
    \q_reg[1]_0 ,
    start_IBUF,
    \q_reg[4]_0 ,
    E,
    clk_IBUF_BUFG,
    AR,
    D);
  output \q_reg[2]_0 ;
  output [0:0]Q;
  output done_OBUF;
  input \q_reg[1]_0 ;
  input start_IBUF;
  input [0:0]\q_reg[4]_0 ;
  input [0:0]E;
  input clk_IBUF_BUFG;
  input [0:0]AR;
  input [0:0]D;

  wire [0:0]AR;
  wire [0:0]D;
  wire [0:0]E;
  wire [0:0]Q;
  wire clk_IBUF_BUFG;
  wire [4:1]counter_out;
  wire done_OBUF;
  wire \q[1]_i_1__0_n_0 ;
  wire \q[2]_i_1__0_n_0 ;
  wire \q[3]_i_1__0_n_0 ;
  wire \q[4]_i_2_n_0 ;
  wire \q_reg[1]_0 ;
  wire \q_reg[2]_0 ;
  wire [0:0]\q_reg[4]_0 ;
  wire start_IBUF;
  wire state_i_2_n_0;

  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h40000000)) 
    done_OBUF_inst_i_1
       (.I0(counter_out[3]),
        .I1(counter_out[1]),
        .I2(Q),
        .I3(counter_out[4]),
        .I4(counter_out[2]),
        .O(done_OBUF));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h6066)) 
    \q[1]_i_1__0 
       (.I0(counter_out[1]),
        .I1(Q),
        .I2(\q_reg[1]_0 ),
        .I3(start_IBUF),
        .O(\q[1]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h6A006A6A)) 
    \q[2]_i_1__0 
       (.I0(counter_out[2]),
        .I1(counter_out[1]),
        .I2(Q),
        .I3(\q_reg[1]_0 ),
        .I4(start_IBUF),
        .O(\q[2]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h6AAA00006AAA6AAA)) 
    \q[3]_i_1__0 
       (.I0(counter_out[3]),
        .I1(counter_out[2]),
        .I2(Q),
        .I3(counter_out[1]),
        .I4(\q_reg[1]_0 ),
        .I5(start_IBUF),
        .O(\q[3]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'h000000006AAAAAAA)) 
    \q[4]_i_2 
       (.I0(counter_out[4]),
        .I1(counter_out[3]),
        .I2(counter_out[1]),
        .I3(Q),
        .I4(counter_out[2]),
        .I5(\q_reg[4]_0 ),
        .O(\q[4]_i_2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(D),
        .Q(Q));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(\q[1]_i_1__0_n_0 ),
        .Q(counter_out[1]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(\q[2]_i_1__0_n_0 ),
        .Q(counter_out[2]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(\q[3]_i_1__0_n_0 ),
        .Q(counter_out[3]));
  FDCE #(
    .INIT(1'b0)) 
    \q_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .CLR(AR),
        .D(\q[4]_i_2_n_0 ),
        .Q(counter_out[4]));
  LUT6 #(
    .INIT(64'hFFF7FFFFFFF70000)) 
    state_i_1
       (.I0(counter_out[2]),
        .I1(counter_out[4]),
        .I2(state_i_2_n_0),
        .I3(counter_out[3]),
        .I4(\q_reg[1]_0 ),
        .I5(start_IBUF),
        .O(\q_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h7)) 
    state_i_2
       (.I0(counter_out[1]),
        .I1(Q),
        .O(state_i_2_n_0));
endmodule

module shift_reg
   (Q,
    O,
    en_shift,
    clk_IBUF_BUFG,
    AR);
  output [23:0]Q;
  input [0:0]O;
  input en_shift;
  input clk_IBUF_BUFG;
  input [0:0]AR;

  wire [0:0]AR;
  wire [0:0]O;
  wire [23:0]Q;
  wire clk_IBUF_BUFG;
  wire en_shift;
  wire serial_in;

  LUT1 #(
    .INIT(2'h1)) 
    \internal_q[0]_i_1 
       (.I0(O),
        .O(serial_in));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(serial_in),
        .Q(Q[0]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[9]),
        .Q(Q[10]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[10]),
        .Q(Q[11]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[11]),
        .Q(Q[12]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[12]),
        .Q(Q[13]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[13]),
        .Q(Q[14]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[14]),
        .Q(Q[15]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[15]),
        .Q(Q[16]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[16]),
        .Q(Q[17]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[17]),
        .Q(Q[18]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[18]),
        .Q(Q[19]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[0]),
        .Q(Q[1]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[19]),
        .Q(Q[20]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[20]),
        .Q(Q[21]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[21]),
        .Q(Q[22]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[22]),
        .Q(Q[23]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[1]),
        .Q(Q[2]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[2]),
        .Q(Q[3]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[3]),
        .Q(Q[4]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[4]),
        .Q(Q[5]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[5]),
        .Q(Q[6]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[6]),
        .Q(Q[7]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[7]),
        .Q(Q[8]));
  FDCE #(
    .INIT(1'b0)) 
    \internal_q_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(en_shift),
        .CLR(AR),
        .D(Q[8]),
        .Q(Q[9]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
