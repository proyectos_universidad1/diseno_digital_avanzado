-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Wed Apr 26 09:04:11 2023
-- Host        : juan-Inspiron-14-3467 running 64-bit Linux Mint 21.1
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/juan/Documentos/JuanK/universidad/profun_2/digit_serial_divider_mealy/digit_serial_divider_mealy.sim/sim_1/synth/func/xsim/tb_digit_serial_divider_func_synth.vhd
-- Design      : digit_serial_divider
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity fsm is
  port (
    state_reg_0 : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    state_reg_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    state_reg_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    state_reg_3 : in STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    start_IBUF : in STD_LOGIC;
    x_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end fsm;

architecture STRUCTURE of fsm is
  signal \^state_reg_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \q[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \q[0]_i_1__0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \q[23]_i_1__0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \q[4]_i_1\ : label is "soft_lutpair0";
begin
  state_reg_0 <= \^state_reg_0\;
\q[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \^state_reg_0\,
      I1 => start_IBUF,
      I2 => x_IBUF(0),
      O => D(0)
    );
\q[0]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0B"
    )
        port map (
      I0 => \^state_reg_0\,
      I1 => start_IBUF,
      I2 => Q(0),
      O => state_reg_1(0)
    );
\q[23]_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => start_IBUF,
      I1 => \^state_reg_0\,
      O => state_reg_2(0)
    );
\q[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^state_reg_0\,
      I1 => start_IBUF,
      O => E(0)
    );
state_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => AR(0),
      D => state_reg_3,
      Q => \^state_reg_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity reg is
  port (
    Q : out STD_LOGIC_VECTOR ( 23 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[7]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[11]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[15]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[19]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[23]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[24]_inv_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    x_IBUF : in STD_LOGIC_VECTOR ( 22 downto 0 );
    start_IBUF : in STD_LOGIC;
    en_shift : in STD_LOGIC;
    O : in STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[4]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[8]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[12]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[16]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[20]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \q_reg[24]_inv_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \sub_out_carry__4\ : in STD_LOGIC_VECTOR ( 23 downto 0 );
    \q_reg[24]_inv_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end reg;

architecture STRUCTURE of reg is
  signal \^q\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \q[10]_i_1_n_0\ : STD_LOGIC;
  signal \q[11]_i_1_n_0\ : STD_LOGIC;
  signal \q[12]_i_1_n_0\ : STD_LOGIC;
  signal \q[13]_i_1_n_0\ : STD_LOGIC;
  signal \q[14]_i_1_n_0\ : STD_LOGIC;
  signal \q[15]_i_1_n_0\ : STD_LOGIC;
  signal \q[16]_i_1_n_0\ : STD_LOGIC;
  signal \q[17]_i_1_n_0\ : STD_LOGIC;
  signal \q[18]_i_1_n_0\ : STD_LOGIC;
  signal \q[19]_i_1_n_0\ : STD_LOGIC;
  signal \q[1]_i_1_n_0\ : STD_LOGIC;
  signal \q[20]_i_1_n_0\ : STD_LOGIC;
  signal \q[21]_i_1_n_0\ : STD_LOGIC;
  signal \q[22]_i_1_n_0\ : STD_LOGIC;
  signal \q[23]_i_1_n_0\ : STD_LOGIC;
  signal \q[24]_inv_i_1_n_0\ : STD_LOGIC;
  signal \q[2]_i_1_n_0\ : STD_LOGIC;
  signal \q[3]_i_1_n_0\ : STD_LOGIC;
  signal \q[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \q[5]_i_1_n_0\ : STD_LOGIC;
  signal \q[6]_i_1_n_0\ : STD_LOGIC;
  signal \q[7]_i_1_n_0\ : STD_LOGIC;
  signal \q[8]_i_1_n_0\ : STD_LOGIC;
  signal \q[9]_i_1_n_0\ : STD_LOGIC;
  attribute inverted : string;
  attribute inverted of \q_reg[24]_inv\ : label is "yes";
begin
  Q(23 downto 0) <= \^q\(23 downto 0);
\q[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(9),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(9),
      I4 => O(0),
      I5 => \q_reg[12]_0\(1),
      O => \q[10]_i_1_n_0\
    );
\q[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(10),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(10),
      I4 => O(0),
      I5 => \q_reg[12]_0\(2),
      O => \q[11]_i_1_n_0\
    );
\q[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(11),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(11),
      I4 => O(0),
      I5 => \q_reg[12]_0\(3),
      O => \q[12]_i_1_n_0\
    );
\q[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(12),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(12),
      I4 => O(0),
      I5 => \q_reg[16]_0\(0),
      O => \q[13]_i_1_n_0\
    );
\q[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(13),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(13),
      I4 => O(0),
      I5 => \q_reg[16]_0\(1),
      O => \q[14]_i_1_n_0\
    );
\q[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(14),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(14),
      I4 => O(0),
      I5 => \q_reg[16]_0\(2),
      O => \q[15]_i_1_n_0\
    );
\q[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(15),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(15),
      I4 => O(0),
      I5 => \q_reg[16]_0\(3),
      O => \q[16]_i_1_n_0\
    );
\q[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(16),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(16),
      I4 => O(0),
      I5 => \q_reg[20]_0\(0),
      O => \q[17]_i_1_n_0\
    );
\q[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(17),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(17),
      I4 => O(0),
      I5 => \q_reg[20]_0\(1),
      O => \q[18]_i_1_n_0\
    );
\q[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(18),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(18),
      I4 => O(0),
      I5 => \q_reg[20]_0\(2),
      O => \q[19]_i_1_n_0\
    );
\q[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(0),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(0),
      I4 => O(0),
      I5 => \q_reg[4]_0\(0),
      O => \q[1]_i_1_n_0\
    );
\q[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(19),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(19),
      I4 => O(0),
      I5 => \q_reg[20]_0\(3),
      O => \q[20]_i_1_n_0\
    );
\q[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(20),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(20),
      I4 => O(0),
      I5 => \q_reg[24]_inv_1\(0),
      O => \q[21]_i_1_n_0\
    );
\q[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(21),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(21),
      I4 => O(0),
      I5 => \q_reg[24]_inv_1\(1),
      O => \q[22]_i_1_n_0\
    );
\q[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(22),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(22),
      I4 => O(0),
      I5 => \q_reg[24]_inv_1\(2),
      O => \q[23]_i_1_n_0\
    );
\q[24]_inv_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"1DFF1D1D"
    )
        port map (
      I0 => \q_reg[24]_inv_1\(3),
      I1 => O(0),
      I2 => \^q\(23),
      I3 => en_shift,
      I4 => start_IBUF,
      O => \q[24]_inv_i_1_n_0\
    );
\q[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(1),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(1),
      I4 => O(0),
      I5 => \q_reg[4]_0\(1),
      O => \q[2]_i_1_n_0\
    );
\q[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(2),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(2),
      I4 => O(0),
      I5 => \q_reg[4]_0\(2),
      O => \q[3]_i_1_n_0\
    );
\q[4]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(3),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(3),
      I4 => O(0),
      I5 => \q_reg[4]_0\(3),
      O => \q[4]_i_1__0_n_0\
    );
\q[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(4),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(4),
      I4 => O(0),
      I5 => \q_reg[8]_0\(0),
      O => \q[5]_i_1_n_0\
    );
\q[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(5),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(5),
      I4 => O(0),
      I5 => \q_reg[8]_0\(1),
      O => \q[6]_i_1_n_0\
    );
\q[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(6),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(6),
      I4 => O(0),
      I5 => \q_reg[8]_0\(2),
      O => \q[7]_i_1_n_0\
    );
\q[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(7),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(7),
      I4 => O(0),
      I5 => \q_reg[8]_0\(3),
      O => \q[8]_i_1_n_0\
    );
\q[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FB08FBFBFB080808"
    )
        port map (
      I0 => x_IBUF(8),
      I1 => start_IBUF,
      I2 => en_shift,
      I3 => \^q\(8),
      I4 => O(0),
      I5 => \q_reg[12]_0\(0),
      O => \q[9]_i_1_n_0\
    );
\q_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q_reg[0]_0\(0),
      Q => \^q\(0)
    );
\q_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[10]_i_1_n_0\,
      Q => \^q\(10)
    );
\q_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[11]_i_1_n_0\,
      Q => \^q\(11)
    );
\q_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[12]_i_1_n_0\,
      Q => \^q\(12)
    );
\q_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[13]_i_1_n_0\,
      Q => \^q\(13)
    );
\q_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[14]_i_1_n_0\,
      Q => \^q\(14)
    );
\q_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[15]_i_1_n_0\,
      Q => \^q\(15)
    );
\q_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[16]_i_1_n_0\,
      Q => \^q\(16)
    );
\q_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[17]_i_1_n_0\,
      Q => \^q\(17)
    );
\q_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[18]_i_1_n_0\,
      Q => \^q\(18)
    );
\q_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[19]_i_1_n_0\,
      Q => \^q\(19)
    );
\q_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[1]_i_1_n_0\,
      Q => \^q\(1)
    );
\q_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[20]_i_1_n_0\,
      Q => \^q\(20)
    );
\q_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[21]_i_1_n_0\,
      Q => \^q\(21)
    );
\q_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[22]_i_1_n_0\,
      Q => \^q\(22)
    );
\q_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[23]_i_1_n_0\,
      Q => \^q\(23)
    );
\q_reg[24]_inv\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      D => \q[24]_inv_i_1_n_0\,
      PRE => AR(0),
      Q => \q_reg[24]_inv_0\(0)
    );
\q_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[2]_i_1_n_0\,
      Q => \^q\(2)
    );
\q_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[3]_i_1_n_0\,
      Q => \^q\(3)
    );
\q_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[4]_i_1__0_n_0\,
      Q => \^q\(4)
    );
\q_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[5]_i_1_n_0\,
      Q => \^q\(5)
    );
\q_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[6]_i_1_n_0\,
      Q => \^q\(6)
    );
\q_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[7]_i_1_n_0\,
      Q => \^q\(7)
    );
\q_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[8]_i_1_n_0\,
      Q => \^q\(8)
    );
\q_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => \q_reg[24]_inv_2\(0),
      CLR => AR(0),
      D => \q[9]_i_1_n_0\,
      Q => \^q\(9)
    );
\sub_out_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(7),
      I1 => \sub_out_carry__4\(7),
      O => \q_reg[7]_0\(3)
    );
\sub_out_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(6),
      I1 => \sub_out_carry__4\(6),
      O => \q_reg[7]_0\(2)
    );
\sub_out_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(5),
      I1 => \sub_out_carry__4\(5),
      O => \q_reg[7]_0\(1)
    );
\sub_out_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(4),
      I1 => \sub_out_carry__4\(4),
      O => \q_reg[7]_0\(0)
    );
\sub_out_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(11),
      I1 => \sub_out_carry__4\(11),
      O => \q_reg[11]_0\(3)
    );
\sub_out_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(10),
      I1 => \sub_out_carry__4\(10),
      O => \q_reg[11]_0\(2)
    );
\sub_out_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(9),
      I1 => \sub_out_carry__4\(9),
      O => \q_reg[11]_0\(1)
    );
\sub_out_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(8),
      I1 => \sub_out_carry__4\(8),
      O => \q_reg[11]_0\(0)
    );
\sub_out_carry__2_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(15),
      I1 => \sub_out_carry__4\(15),
      O => \q_reg[15]_0\(3)
    );
\sub_out_carry__2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(14),
      I1 => \sub_out_carry__4\(14),
      O => \q_reg[15]_0\(2)
    );
\sub_out_carry__2_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(13),
      I1 => \sub_out_carry__4\(13),
      O => \q_reg[15]_0\(1)
    );
\sub_out_carry__2_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(12),
      I1 => \sub_out_carry__4\(12),
      O => \q_reg[15]_0\(0)
    );
\sub_out_carry__3_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(19),
      I1 => \sub_out_carry__4\(19),
      O => \q_reg[19]_0\(3)
    );
\sub_out_carry__3_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(18),
      I1 => \sub_out_carry__4\(18),
      O => \q_reg[19]_0\(2)
    );
\sub_out_carry__3_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(17),
      I1 => \sub_out_carry__4\(17),
      O => \q_reg[19]_0\(1)
    );
\sub_out_carry__3_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(16),
      I1 => \sub_out_carry__4\(16),
      O => \q_reg[19]_0\(0)
    );
\sub_out_carry__4_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(23),
      I1 => \sub_out_carry__4\(23),
      O => \q_reg[23]_0\(3)
    );
\sub_out_carry__4_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(22),
      I1 => \sub_out_carry__4\(22),
      O => \q_reg[23]_0\(2)
    );
\sub_out_carry__4_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(21),
      I1 => \sub_out_carry__4\(21),
      O => \q_reg[23]_0\(1)
    );
\sub_out_carry__4_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(20),
      I1 => \sub_out_carry__4\(20),
      O => \q_reg[23]_0\(0)
    );
sub_out_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(3),
      I1 => \sub_out_carry__4\(3),
      O => S(3)
    );
sub_out_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(2),
      I1 => \sub_out_carry__4\(2),
      O => S(2)
    );
sub_out_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(1),
      I1 => \sub_out_carry__4\(1),
      O => S(1)
    );
sub_out_carry_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^q\(0),
      I1 => \sub_out_carry__4\(0),
      O => S(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity reg_0 is
  port (
    Q : out STD_LOGIC_VECTOR ( 23 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 23 downto 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of reg_0 : entity is "reg";
end reg_0;

architecture STRUCTURE of reg_0 is
begin
\q_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(0),
      Q => Q(0)
    );
\q_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(10),
      Q => Q(10)
    );
\q_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(11),
      Q => Q(11)
    );
\q_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(12),
      Q => Q(12)
    );
\q_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(13),
      Q => Q(13)
    );
\q_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(14),
      Q => Q(14)
    );
\q_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(15),
      Q => Q(15)
    );
\q_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(16),
      Q => Q(16)
    );
\q_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(17),
      Q => Q(17)
    );
\q_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(18),
      Q => Q(18)
    );
\q_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(19),
      Q => Q(19)
    );
\q_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(1),
      Q => Q(1)
    );
\q_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(20),
      Q => Q(20)
    );
\q_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(21),
      Q => Q(21)
    );
\q_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(22),
      Q => Q(22)
    );
\q_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(23),
      Q => Q(23)
    );
\q_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(2),
      Q => Q(2)
    );
\q_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(3),
      Q => Q(3)
    );
\q_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(4),
      Q => Q(4)
    );
\q_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(5),
      Q => Q(5)
    );
\q_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(6),
      Q => Q(6)
    );
\q_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(7),
      Q => Q(7)
    );
\q_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(8),
      Q => Q(8)
    );
\q_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(9),
      Q => Q(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \reg__parameterized2\ is
  port (
    \q_reg[2]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 0 to 0 );
    done_OBUF : out STD_LOGIC;
    \q_reg[1]_0\ : in STD_LOGIC;
    start_IBUF : in STD_LOGIC;
    \q_reg[4]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \reg__parameterized2\ : entity is "reg";
end \reg__parameterized2\;

architecture STRUCTURE of \reg__parameterized2\ is
  signal \^q\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal counter_out : STD_LOGIC_VECTOR ( 4 downto 1 );
  signal \q[1]_i_1__0_n_0\ : STD_LOGIC;
  signal \q[2]_i_1__0_n_0\ : STD_LOGIC;
  signal \q[3]_i_1__0_n_0\ : STD_LOGIC;
  signal \q[4]_i_2_n_0\ : STD_LOGIC;
  signal state_i_2_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of done_OBUF_inst_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \q[1]_i_1__0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \q[2]_i_1__0\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of state_i_2 : label is "soft_lutpair3";
begin
  Q(0) <= \^q\(0);
done_OBUF_inst_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40000000"
    )
        port map (
      I0 => counter_out(3),
      I1 => counter_out(1),
      I2 => \^q\(0),
      I3 => counter_out(4),
      I4 => counter_out(2),
      O => done_OBUF
    );
\q[1]_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6066"
    )
        port map (
      I0 => counter_out(1),
      I1 => \^q\(0),
      I2 => \q_reg[1]_0\,
      I3 => start_IBUF,
      O => \q[1]_i_1__0_n_0\
    );
\q[2]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6A006A6A"
    )
        port map (
      I0 => counter_out(2),
      I1 => counter_out(1),
      I2 => \^q\(0),
      I3 => \q_reg[1]_0\,
      I4 => start_IBUF,
      O => \q[2]_i_1__0_n_0\
    );
\q[3]_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAA00006AAA6AAA"
    )
        port map (
      I0 => counter_out(3),
      I1 => counter_out(2),
      I2 => \^q\(0),
      I3 => counter_out(1),
      I4 => \q_reg[1]_0\,
      I5 => start_IBUF,
      O => \q[3]_i_1__0_n_0\
    );
\q[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000006AAAAAAA"
    )
        port map (
      I0 => counter_out(4),
      I1 => counter_out(3),
      I2 => counter_out(1),
      I3 => \^q\(0),
      I4 => counter_out(2),
      I5 => \q_reg[4]_0\(0),
      O => \q[4]_i_2_n_0\
    );
\q_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => D(0),
      Q => \^q\(0)
    );
\q_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => \q[1]_i_1__0_n_0\,
      Q => counter_out(1)
    );
\q_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => \q[2]_i_1__0_n_0\,
      Q => counter_out(2)
    );
\q_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => \q[3]_i_1__0_n_0\,
      Q => counter_out(3)
    );
\q_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => E(0),
      CLR => AR(0),
      D => \q[4]_i_2_n_0\,
      Q => counter_out(4)
    );
state_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF7FFFFFFF70000"
    )
        port map (
      I0 => counter_out(2),
      I1 => counter_out(4),
      I2 => state_i_2_n_0,
      I3 => counter_out(3),
      I4 => \q_reg[1]_0\,
      I5 => start_IBUF,
      O => \q_reg[2]_0\
    );
state_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => counter_out(1),
      I1 => \^q\(0),
      O => state_i_2_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity shift_reg is
  port (
    Q : out STD_LOGIC_VECTOR ( 23 downto 0 );
    O : in STD_LOGIC_VECTOR ( 0 to 0 );
    en_shift : in STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end shift_reg;

architecture STRUCTURE of shift_reg is
  signal \^q\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal serial_in : STD_LOGIC;
begin
  Q(23 downto 0) <= \^q\(23 downto 0);
\internal_q[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => O(0),
      O => serial_in
    );
\internal_q_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => serial_in,
      Q => \^q\(0)
    );
\internal_q_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(9),
      Q => \^q\(10)
    );
\internal_q_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(10),
      Q => \^q\(11)
    );
\internal_q_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(11),
      Q => \^q\(12)
    );
\internal_q_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(12),
      Q => \^q\(13)
    );
\internal_q_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(13),
      Q => \^q\(14)
    );
\internal_q_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(14),
      Q => \^q\(15)
    );
\internal_q_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(15),
      Q => \^q\(16)
    );
\internal_q_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(16),
      Q => \^q\(17)
    );
\internal_q_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(17),
      Q => \^q\(18)
    );
\internal_q_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(18),
      Q => \^q\(19)
    );
\internal_q_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(0),
      Q => \^q\(1)
    );
\internal_q_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(19),
      Q => \^q\(20)
    );
\internal_q_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(20),
      Q => \^q\(21)
    );
\internal_q_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(21),
      Q => \^q\(22)
    );
\internal_q_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(22),
      Q => \^q\(23)
    );
\internal_q_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(1),
      Q => \^q\(2)
    );
\internal_q_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(2),
      Q => \^q\(3)
    );
\internal_q_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(3),
      Q => \^q\(4)
    );
\internal_q_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(4),
      Q => \^q\(5)
    );
\internal_q_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(5),
      Q => \^q\(6)
    );
\internal_q_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(6),
      Q => \^q\(7)
    );
\internal_q_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(7),
      Q => \^q\(8)
    );
\internal_q_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => en_shift,
      CLR => AR(0),
      D => \^q\(8),
      Q => \^q\(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity controlpath is
  port (
    en_shift : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    done_OBUF : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 0 to 0 );
    state_reg : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    start_IBUF : in STD_LOGIC;
    x_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end controlpath;

architecture STRUCTURE of controlpath is
  signal \^e\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal counter_out : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^en_shift\ : STD_LOGIC;
  signal fsm0_n_3 : STD_LOGIC;
  signal regr_n_0 : STD_LOGIC;
  signal \^state_reg\ : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  E(0) <= \^e\(0);
  en_shift <= \^en_shift\;
  state_reg(0) <= \^state_reg\(0);
fsm0: entity work.fsm
     port map (
      AR(0) => AR(0),
      D(0) => D(0),
      E(0) => \^e\(0),
      Q(0) => counter_out(0),
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      start_IBUF => start_IBUF,
      state_reg_0 => \^en_shift\,
      state_reg_1(0) => fsm0_n_3,
      state_reg_2(0) => \^state_reg\(0),
      state_reg_3 => regr_n_0,
      x_IBUF(0) => x_IBUF(0)
    );
regr: entity work.\reg__parameterized2\
     port map (
      AR(0) => AR(0),
      D(0) => fsm0_n_3,
      E(0) => \^e\(0),
      Q(0) => counter_out(0),
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      done_OBUF => done_OBUF,
      \q_reg[1]_0\ => \^en_shift\,
      \q_reg[2]_0\ => regr_n_0,
      \q_reg[4]_0\(0) => \^state_reg\(0),
      start_IBUF => start_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity datapath is
  port (
    Q : out STD_LOGIC_VECTOR ( 23 downto 0 );
    x_IBUF : in STD_LOGIC_VECTOR ( 22 downto 0 );
    start_IBUF : in STD_LOGIC;
    en_shift : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 23 downto 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[24]_inv\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \q_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end datapath;

architecture STRUCTURE of datapath is
  signal regr_n_0 : STD_LOGIC;
  signal regr_n_1 : STD_LOGIC;
  signal regr_n_10 : STD_LOGIC;
  signal regr_n_11 : STD_LOGIC;
  signal regr_n_12 : STD_LOGIC;
  signal regr_n_13 : STD_LOGIC;
  signal regr_n_14 : STD_LOGIC;
  signal regr_n_15 : STD_LOGIC;
  signal regr_n_16 : STD_LOGIC;
  signal regr_n_17 : STD_LOGIC;
  signal regr_n_18 : STD_LOGIC;
  signal regr_n_19 : STD_LOGIC;
  signal regr_n_2 : STD_LOGIC;
  signal regr_n_20 : STD_LOGIC;
  signal regr_n_21 : STD_LOGIC;
  signal regr_n_22 : STD_LOGIC;
  signal regr_n_23 : STD_LOGIC;
  signal regr_n_24 : STD_LOGIC;
  signal regr_n_25 : STD_LOGIC;
  signal regr_n_26 : STD_LOGIC;
  signal regr_n_27 : STD_LOGIC;
  signal regr_n_28 : STD_LOGIC;
  signal regr_n_29 : STD_LOGIC;
  signal regr_n_3 : STD_LOGIC;
  signal regr_n_30 : STD_LOGIC;
  signal regr_n_31 : STD_LOGIC;
  signal regr_n_32 : STD_LOGIC;
  signal regr_n_33 : STD_LOGIC;
  signal regr_n_34 : STD_LOGIC;
  signal regr_n_35 : STD_LOGIC;
  signal regr_n_36 : STD_LOGIC;
  signal regr_n_37 : STD_LOGIC;
  signal regr_n_38 : STD_LOGIC;
  signal regr_n_39 : STD_LOGIC;
  signal regr_n_4 : STD_LOGIC;
  signal regr_n_40 : STD_LOGIC;
  signal regr_n_41 : STD_LOGIC;
  signal regr_n_42 : STD_LOGIC;
  signal regr_n_43 : STD_LOGIC;
  signal regr_n_44 : STD_LOGIC;
  signal regr_n_45 : STD_LOGIC;
  signal regr_n_46 : STD_LOGIC;
  signal regr_n_47 : STD_LOGIC;
  signal regr_n_48 : STD_LOGIC;
  signal regr_n_5 : STD_LOGIC;
  signal regr_n_6 : STD_LOGIC;
  signal regr_n_7 : STD_LOGIC;
  signal regr_n_8 : STD_LOGIC;
  signal regr_n_9 : STD_LOGIC;
  signal regy_n_0 : STD_LOGIC;
  signal regy_n_1 : STD_LOGIC;
  signal regy_n_10 : STD_LOGIC;
  signal regy_n_11 : STD_LOGIC;
  signal regy_n_12 : STD_LOGIC;
  signal regy_n_13 : STD_LOGIC;
  signal regy_n_14 : STD_LOGIC;
  signal regy_n_15 : STD_LOGIC;
  signal regy_n_16 : STD_LOGIC;
  signal regy_n_17 : STD_LOGIC;
  signal regy_n_18 : STD_LOGIC;
  signal regy_n_19 : STD_LOGIC;
  signal regy_n_2 : STD_LOGIC;
  signal regy_n_20 : STD_LOGIC;
  signal regy_n_21 : STD_LOGIC;
  signal regy_n_22 : STD_LOGIC;
  signal regy_n_23 : STD_LOGIC;
  signal regy_n_3 : STD_LOGIC;
  signal regy_n_4 : STD_LOGIC;
  signal regy_n_5 : STD_LOGIC;
  signal regy_n_6 : STD_LOGIC;
  signal regy_n_7 : STD_LOGIC;
  signal regy_n_8 : STD_LOGIC;
  signal regy_n_9 : STD_LOGIC;
  signal \sub_out_carry__0_n_0\ : STD_LOGIC;
  signal \sub_out_carry__0_n_1\ : STD_LOGIC;
  signal \sub_out_carry__0_n_2\ : STD_LOGIC;
  signal \sub_out_carry__0_n_3\ : STD_LOGIC;
  signal \sub_out_carry__0_n_4\ : STD_LOGIC;
  signal \sub_out_carry__0_n_5\ : STD_LOGIC;
  signal \sub_out_carry__0_n_6\ : STD_LOGIC;
  signal \sub_out_carry__0_n_7\ : STD_LOGIC;
  signal \sub_out_carry__1_n_0\ : STD_LOGIC;
  signal \sub_out_carry__1_n_1\ : STD_LOGIC;
  signal \sub_out_carry__1_n_2\ : STD_LOGIC;
  signal \sub_out_carry__1_n_3\ : STD_LOGIC;
  signal \sub_out_carry__1_n_4\ : STD_LOGIC;
  signal \sub_out_carry__1_n_5\ : STD_LOGIC;
  signal \sub_out_carry__1_n_6\ : STD_LOGIC;
  signal \sub_out_carry__1_n_7\ : STD_LOGIC;
  signal \sub_out_carry__2_n_0\ : STD_LOGIC;
  signal \sub_out_carry__2_n_1\ : STD_LOGIC;
  signal \sub_out_carry__2_n_2\ : STD_LOGIC;
  signal \sub_out_carry__2_n_3\ : STD_LOGIC;
  signal \sub_out_carry__2_n_4\ : STD_LOGIC;
  signal \sub_out_carry__2_n_5\ : STD_LOGIC;
  signal \sub_out_carry__2_n_6\ : STD_LOGIC;
  signal \sub_out_carry__2_n_7\ : STD_LOGIC;
  signal \sub_out_carry__3_n_0\ : STD_LOGIC;
  signal \sub_out_carry__3_n_1\ : STD_LOGIC;
  signal \sub_out_carry__3_n_2\ : STD_LOGIC;
  signal \sub_out_carry__3_n_3\ : STD_LOGIC;
  signal \sub_out_carry__3_n_4\ : STD_LOGIC;
  signal \sub_out_carry__3_n_5\ : STD_LOGIC;
  signal \sub_out_carry__3_n_6\ : STD_LOGIC;
  signal \sub_out_carry__3_n_7\ : STD_LOGIC;
  signal \sub_out_carry__4_n_0\ : STD_LOGIC;
  signal \sub_out_carry__4_n_1\ : STD_LOGIC;
  signal \sub_out_carry__4_n_2\ : STD_LOGIC;
  signal \sub_out_carry__4_n_3\ : STD_LOGIC;
  signal \sub_out_carry__4_n_4\ : STD_LOGIC;
  signal \sub_out_carry__4_n_5\ : STD_LOGIC;
  signal \sub_out_carry__4_n_6\ : STD_LOGIC;
  signal \sub_out_carry__4_n_7\ : STD_LOGIC;
  signal \sub_out_carry__5_n_7\ : STD_LOGIC;
  signal sub_out_carry_n_0 : STD_LOGIC;
  signal sub_out_carry_n_1 : STD_LOGIC;
  signal sub_out_carry_n_2 : STD_LOGIC;
  signal sub_out_carry_n_3 : STD_LOGIC;
  signal sub_out_carry_n_4 : STD_LOGIC;
  signal sub_out_carry_n_5 : STD_LOGIC;
  signal sub_out_carry_n_6 : STD_LOGIC;
  signal sub_out_carry_n_7 : STD_LOGIC;
  signal \NLW_sub_out_carry__5_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_sub_out_carry__5_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of sub_out_carry : label is 35;
  attribute ADDER_THRESHOLD of \sub_out_carry__0\ : label is 35;
  attribute ADDER_THRESHOLD of \sub_out_carry__1\ : label is 35;
  attribute ADDER_THRESHOLD of \sub_out_carry__2\ : label is 35;
  attribute ADDER_THRESHOLD of \sub_out_carry__3\ : label is 35;
  attribute ADDER_THRESHOLD of \sub_out_carry__4\ : label is 35;
  attribute ADDER_THRESHOLD of \sub_out_carry__5\ : label is 35;
begin
regr: entity work.reg
     port map (
      AR(0) => AR(0),
      O(0) => \sub_out_carry__5_n_7\,
      Q(23) => regr_n_0,
      Q(22) => regr_n_1,
      Q(21) => regr_n_2,
      Q(20) => regr_n_3,
      Q(19) => regr_n_4,
      Q(18) => regr_n_5,
      Q(17) => regr_n_6,
      Q(16) => regr_n_7,
      Q(15) => regr_n_8,
      Q(14) => regr_n_9,
      Q(13) => regr_n_10,
      Q(12) => regr_n_11,
      Q(11) => regr_n_12,
      Q(10) => regr_n_13,
      Q(9) => regr_n_14,
      Q(8) => regr_n_15,
      Q(7) => regr_n_16,
      Q(6) => regr_n_17,
      Q(5) => regr_n_18,
      Q(4) => regr_n_19,
      Q(3) => regr_n_20,
      Q(2) => regr_n_21,
      Q(1) => regr_n_22,
      Q(0) => regr_n_23,
      S(3) => regr_n_24,
      S(2) => regr_n_25,
      S(1) => regr_n_26,
      S(0) => regr_n_27,
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      en_shift => en_shift,
      \q_reg[0]_0\(0) => \q_reg[0]\(0),
      \q_reg[11]_0\(3) => regr_n_32,
      \q_reg[11]_0\(2) => regr_n_33,
      \q_reg[11]_0\(1) => regr_n_34,
      \q_reg[11]_0\(0) => regr_n_35,
      \q_reg[12]_0\(3) => \sub_out_carry__1_n_4\,
      \q_reg[12]_0\(2) => \sub_out_carry__1_n_5\,
      \q_reg[12]_0\(1) => \sub_out_carry__1_n_6\,
      \q_reg[12]_0\(0) => \sub_out_carry__1_n_7\,
      \q_reg[15]_0\(3) => regr_n_36,
      \q_reg[15]_0\(2) => regr_n_37,
      \q_reg[15]_0\(1) => regr_n_38,
      \q_reg[15]_0\(0) => regr_n_39,
      \q_reg[16]_0\(3) => \sub_out_carry__2_n_4\,
      \q_reg[16]_0\(2) => \sub_out_carry__2_n_5\,
      \q_reg[16]_0\(1) => \sub_out_carry__2_n_6\,
      \q_reg[16]_0\(0) => \sub_out_carry__2_n_7\,
      \q_reg[19]_0\(3) => regr_n_40,
      \q_reg[19]_0\(2) => regr_n_41,
      \q_reg[19]_0\(1) => regr_n_42,
      \q_reg[19]_0\(0) => regr_n_43,
      \q_reg[20]_0\(3) => \sub_out_carry__3_n_4\,
      \q_reg[20]_0\(2) => \sub_out_carry__3_n_5\,
      \q_reg[20]_0\(1) => \sub_out_carry__3_n_6\,
      \q_reg[20]_0\(0) => \sub_out_carry__3_n_7\,
      \q_reg[23]_0\(3) => regr_n_44,
      \q_reg[23]_0\(2) => regr_n_45,
      \q_reg[23]_0\(1) => regr_n_46,
      \q_reg[23]_0\(0) => regr_n_47,
      \q_reg[24]_inv_0\(0) => regr_n_48,
      \q_reg[24]_inv_1\(3) => \sub_out_carry__4_n_4\,
      \q_reg[24]_inv_1\(2) => \sub_out_carry__4_n_5\,
      \q_reg[24]_inv_1\(1) => \sub_out_carry__4_n_6\,
      \q_reg[24]_inv_1\(0) => \sub_out_carry__4_n_7\,
      \q_reg[24]_inv_2\(0) => \q_reg[24]_inv\(0),
      \q_reg[4]_0\(3) => sub_out_carry_n_4,
      \q_reg[4]_0\(2) => sub_out_carry_n_5,
      \q_reg[4]_0\(1) => sub_out_carry_n_6,
      \q_reg[4]_0\(0) => sub_out_carry_n_7,
      \q_reg[7]_0\(3) => regr_n_28,
      \q_reg[7]_0\(2) => regr_n_29,
      \q_reg[7]_0\(1) => regr_n_30,
      \q_reg[7]_0\(0) => regr_n_31,
      \q_reg[8]_0\(3) => \sub_out_carry__0_n_4\,
      \q_reg[8]_0\(2) => \sub_out_carry__0_n_5\,
      \q_reg[8]_0\(1) => \sub_out_carry__0_n_6\,
      \q_reg[8]_0\(0) => \sub_out_carry__0_n_7\,
      start_IBUF => start_IBUF,
      \sub_out_carry__4\(23) => regy_n_0,
      \sub_out_carry__4\(22) => regy_n_1,
      \sub_out_carry__4\(21) => regy_n_2,
      \sub_out_carry__4\(20) => regy_n_3,
      \sub_out_carry__4\(19) => regy_n_4,
      \sub_out_carry__4\(18) => regy_n_5,
      \sub_out_carry__4\(17) => regy_n_6,
      \sub_out_carry__4\(16) => regy_n_7,
      \sub_out_carry__4\(15) => regy_n_8,
      \sub_out_carry__4\(14) => regy_n_9,
      \sub_out_carry__4\(13) => regy_n_10,
      \sub_out_carry__4\(12) => regy_n_11,
      \sub_out_carry__4\(11) => regy_n_12,
      \sub_out_carry__4\(10) => regy_n_13,
      \sub_out_carry__4\(9) => regy_n_14,
      \sub_out_carry__4\(8) => regy_n_15,
      \sub_out_carry__4\(7) => regy_n_16,
      \sub_out_carry__4\(6) => regy_n_17,
      \sub_out_carry__4\(5) => regy_n_18,
      \sub_out_carry__4\(4) => regy_n_19,
      \sub_out_carry__4\(3) => regy_n_20,
      \sub_out_carry__4\(2) => regy_n_21,
      \sub_out_carry__4\(1) => regy_n_22,
      \sub_out_carry__4\(0) => regy_n_23,
      x_IBUF(22 downto 0) => x_IBUF(22 downto 0)
    );
regy: entity work.reg_0
     port map (
      AR(0) => AR(0),
      D(23 downto 0) => D(23 downto 0),
      E(0) => E(0),
      Q(23) => regy_n_0,
      Q(22) => regy_n_1,
      Q(21) => regy_n_2,
      Q(20) => regy_n_3,
      Q(19) => regy_n_4,
      Q(18) => regy_n_5,
      Q(17) => regy_n_6,
      Q(16) => regy_n_7,
      Q(15) => regy_n_8,
      Q(14) => regy_n_9,
      Q(13) => regy_n_10,
      Q(12) => regy_n_11,
      Q(11) => regy_n_12,
      Q(10) => regy_n_13,
      Q(9) => regy_n_14,
      Q(8) => regy_n_15,
      Q(7) => regy_n_16,
      Q(6) => regy_n_17,
      Q(5) => regy_n_18,
      Q(4) => regy_n_19,
      Q(3) => regy_n_20,
      Q(2) => regy_n_21,
      Q(1) => regy_n_22,
      Q(0) => regy_n_23,
      clk_IBUF_BUFG => clk_IBUF_BUFG
    );
shift_reg0: entity work.shift_reg
     port map (
      AR(0) => AR(0),
      O(0) => \sub_out_carry__5_n_7\,
      Q(23 downto 0) => Q(23 downto 0),
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      en_shift => en_shift
    );
sub_out_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => sub_out_carry_n_0,
      CO(2) => sub_out_carry_n_1,
      CO(1) => sub_out_carry_n_2,
      CO(0) => sub_out_carry_n_3,
      CYINIT => '1',
      DI(3) => regr_n_20,
      DI(2) => regr_n_21,
      DI(1) => regr_n_22,
      DI(0) => regr_n_23,
      O(3) => sub_out_carry_n_4,
      O(2) => sub_out_carry_n_5,
      O(1) => sub_out_carry_n_6,
      O(0) => sub_out_carry_n_7,
      S(3) => regr_n_24,
      S(2) => regr_n_25,
      S(1) => regr_n_26,
      S(0) => regr_n_27
    );
\sub_out_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => sub_out_carry_n_0,
      CO(3) => \sub_out_carry__0_n_0\,
      CO(2) => \sub_out_carry__0_n_1\,
      CO(1) => \sub_out_carry__0_n_2\,
      CO(0) => \sub_out_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => regr_n_16,
      DI(2) => regr_n_17,
      DI(1) => regr_n_18,
      DI(0) => regr_n_19,
      O(3) => \sub_out_carry__0_n_4\,
      O(2) => \sub_out_carry__0_n_5\,
      O(1) => \sub_out_carry__0_n_6\,
      O(0) => \sub_out_carry__0_n_7\,
      S(3) => regr_n_28,
      S(2) => regr_n_29,
      S(1) => regr_n_30,
      S(0) => regr_n_31
    );
\sub_out_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \sub_out_carry__0_n_0\,
      CO(3) => \sub_out_carry__1_n_0\,
      CO(2) => \sub_out_carry__1_n_1\,
      CO(1) => \sub_out_carry__1_n_2\,
      CO(0) => \sub_out_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => regr_n_12,
      DI(2) => regr_n_13,
      DI(1) => regr_n_14,
      DI(0) => regr_n_15,
      O(3) => \sub_out_carry__1_n_4\,
      O(2) => \sub_out_carry__1_n_5\,
      O(1) => \sub_out_carry__1_n_6\,
      O(0) => \sub_out_carry__1_n_7\,
      S(3) => regr_n_32,
      S(2) => regr_n_33,
      S(1) => regr_n_34,
      S(0) => regr_n_35
    );
\sub_out_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \sub_out_carry__1_n_0\,
      CO(3) => \sub_out_carry__2_n_0\,
      CO(2) => \sub_out_carry__2_n_1\,
      CO(1) => \sub_out_carry__2_n_2\,
      CO(0) => \sub_out_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => regr_n_8,
      DI(2) => regr_n_9,
      DI(1) => regr_n_10,
      DI(0) => regr_n_11,
      O(3) => \sub_out_carry__2_n_4\,
      O(2) => \sub_out_carry__2_n_5\,
      O(1) => \sub_out_carry__2_n_6\,
      O(0) => \sub_out_carry__2_n_7\,
      S(3) => regr_n_36,
      S(2) => regr_n_37,
      S(1) => regr_n_38,
      S(0) => regr_n_39
    );
\sub_out_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \sub_out_carry__2_n_0\,
      CO(3) => \sub_out_carry__3_n_0\,
      CO(2) => \sub_out_carry__3_n_1\,
      CO(1) => \sub_out_carry__3_n_2\,
      CO(0) => \sub_out_carry__3_n_3\,
      CYINIT => '0',
      DI(3) => regr_n_4,
      DI(2) => regr_n_5,
      DI(1) => regr_n_6,
      DI(0) => regr_n_7,
      O(3) => \sub_out_carry__3_n_4\,
      O(2) => \sub_out_carry__3_n_5\,
      O(1) => \sub_out_carry__3_n_6\,
      O(0) => \sub_out_carry__3_n_7\,
      S(3) => regr_n_40,
      S(2) => regr_n_41,
      S(1) => regr_n_42,
      S(0) => regr_n_43
    );
\sub_out_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \sub_out_carry__3_n_0\,
      CO(3) => \sub_out_carry__4_n_0\,
      CO(2) => \sub_out_carry__4_n_1\,
      CO(1) => \sub_out_carry__4_n_2\,
      CO(0) => \sub_out_carry__4_n_3\,
      CYINIT => '0',
      DI(3) => regr_n_0,
      DI(2) => regr_n_1,
      DI(1) => regr_n_2,
      DI(0) => regr_n_3,
      O(3) => \sub_out_carry__4_n_4\,
      O(2) => \sub_out_carry__4_n_5\,
      O(1) => \sub_out_carry__4_n_6\,
      O(0) => \sub_out_carry__4_n_7\,
      S(3) => regr_n_44,
      S(2) => regr_n_45,
      S(1) => regr_n_46,
      S(0) => regr_n_47
    );
\sub_out_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \sub_out_carry__4_n_0\,
      CO(3 downto 0) => \NLW_sub_out_carry__5_CO_UNCONNECTED\(3 downto 0),
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 1) => \NLW_sub_out_carry__5_O_UNCONNECTED\(3 downto 1),
      O(0) => \sub_out_carry__5_n_7\,
      S(3 downto 1) => B"000",
      S(0) => regr_n_48
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity digit_serial_divider is
  port (
    x : in STD_LOGIC_VECTOR ( 23 downto 0 );
    y : in STD_LOGIC_VECTOR ( 23 downto 0 );
    q : out STD_LOGIC_VECTOR ( 23 downto 0 );
    start : in STD_LOGIC;
    done : out STD_LOGIC;
    clk : in STD_LOGIC;
    rst : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of digit_serial_divider : entity is true;
end digit_serial_divider;

architecture STRUCTURE of digit_serial_divider is
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal controlpath0_n_3 : STD_LOGIC;
  signal controlpath0_n_4 : STD_LOGIC;
  signal done_OBUF : STD_LOGIC;
  signal en_r : STD_LOGIC;
  signal en_shift : STD_LOGIC;
  signal q_OBUF : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal rst_IBUF : STD_LOGIC;
  signal start_IBUF : STD_LOGIC;
  signal x_IBUF : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal y_IBUF : STD_LOGIC_VECTOR ( 23 downto 0 );
begin
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => clk,
      O => clk_IBUF
    );
controlpath0: entity work.controlpath
     port map (
      AR(0) => rst_IBUF,
      D(0) => controlpath0_n_3,
      E(0) => en_r,
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      done_OBUF => done_OBUF,
      en_shift => en_shift,
      start_IBUF => start_IBUF,
      state_reg(0) => controlpath0_n_4,
      x_IBUF(0) => x_IBUF(0)
    );
datapath0: entity work.datapath
     port map (
      AR(0) => rst_IBUF,
      D(23 downto 0) => y_IBUF(23 downto 0),
      E(0) => controlpath0_n_4,
      Q(23 downto 0) => q_OBUF(23 downto 0),
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      en_shift => en_shift,
      \q_reg[0]\(0) => controlpath0_n_3,
      \q_reg[24]_inv\(0) => en_r,
      start_IBUF => start_IBUF,
      x_IBUF(22 downto 0) => x_IBUF(23 downto 1)
    );
done_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => done_OBUF,
      O => done
    );
\q_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(0),
      O => q(0)
    );
\q_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(10),
      O => q(10)
    );
\q_OBUF[11]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(11),
      O => q(11)
    );
\q_OBUF[12]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(12),
      O => q(12)
    );
\q_OBUF[13]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(13),
      O => q(13)
    );
\q_OBUF[14]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(14),
      O => q(14)
    );
\q_OBUF[15]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(15),
      O => q(15)
    );
\q_OBUF[16]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(16),
      O => q(16)
    );
\q_OBUF[17]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(17),
      O => q(17)
    );
\q_OBUF[18]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(18),
      O => q(18)
    );
\q_OBUF[19]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(19),
      O => q(19)
    );
\q_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(1),
      O => q(1)
    );
\q_OBUF[20]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(20),
      O => q(20)
    );
\q_OBUF[21]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(21),
      O => q(21)
    );
\q_OBUF[22]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(22),
      O => q(22)
    );
\q_OBUF[23]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(23),
      O => q(23)
    );
\q_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(2),
      O => q(2)
    );
\q_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(3),
      O => q(3)
    );
\q_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(4),
      O => q(4)
    );
\q_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(5),
      O => q(5)
    );
\q_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(6),
      O => q(6)
    );
\q_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(7),
      O => q(7)
    );
\q_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(8),
      O => q(8)
    );
\q_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => q_OBUF(9),
      O => q(9)
    );
rst_IBUF_inst: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => rst,
      O => rst_IBUF
    );
start_IBUF_inst: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => start,
      O => start_IBUF
    );
\x_IBUF[0]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(0),
      O => x_IBUF(0)
    );
\x_IBUF[10]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(10),
      O => x_IBUF(10)
    );
\x_IBUF[11]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(11),
      O => x_IBUF(11)
    );
\x_IBUF[12]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(12),
      O => x_IBUF(12)
    );
\x_IBUF[13]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(13),
      O => x_IBUF(13)
    );
\x_IBUF[14]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(14),
      O => x_IBUF(14)
    );
\x_IBUF[15]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(15),
      O => x_IBUF(15)
    );
\x_IBUF[16]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(16),
      O => x_IBUF(16)
    );
\x_IBUF[17]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(17),
      O => x_IBUF(17)
    );
\x_IBUF[18]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(18),
      O => x_IBUF(18)
    );
\x_IBUF[19]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(19),
      O => x_IBUF(19)
    );
\x_IBUF[1]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(1),
      O => x_IBUF(1)
    );
\x_IBUF[20]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(20),
      O => x_IBUF(20)
    );
\x_IBUF[21]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(21),
      O => x_IBUF(21)
    );
\x_IBUF[22]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(22),
      O => x_IBUF(22)
    );
\x_IBUF[23]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(23),
      O => x_IBUF(23)
    );
\x_IBUF[2]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(2),
      O => x_IBUF(2)
    );
\x_IBUF[3]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(3),
      O => x_IBUF(3)
    );
\x_IBUF[4]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(4),
      O => x_IBUF(4)
    );
\x_IBUF[5]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(5),
      O => x_IBUF(5)
    );
\x_IBUF[6]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(6),
      O => x_IBUF(6)
    );
\x_IBUF[7]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(7),
      O => x_IBUF(7)
    );
\x_IBUF[8]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(8),
      O => x_IBUF(8)
    );
\x_IBUF[9]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => x(9),
      O => x_IBUF(9)
    );
\y_IBUF[0]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(0),
      O => y_IBUF(0)
    );
\y_IBUF[10]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(10),
      O => y_IBUF(10)
    );
\y_IBUF[11]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(11),
      O => y_IBUF(11)
    );
\y_IBUF[12]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(12),
      O => y_IBUF(12)
    );
\y_IBUF[13]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(13),
      O => y_IBUF(13)
    );
\y_IBUF[14]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(14),
      O => y_IBUF(14)
    );
\y_IBUF[15]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(15),
      O => y_IBUF(15)
    );
\y_IBUF[16]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(16),
      O => y_IBUF(16)
    );
\y_IBUF[17]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(17),
      O => y_IBUF(17)
    );
\y_IBUF[18]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(18),
      O => y_IBUF(18)
    );
\y_IBUF[19]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(19),
      O => y_IBUF(19)
    );
\y_IBUF[1]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(1),
      O => y_IBUF(1)
    );
\y_IBUF[20]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(20),
      O => y_IBUF(20)
    );
\y_IBUF[21]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(21),
      O => y_IBUF(21)
    );
\y_IBUF[22]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(22),
      O => y_IBUF(22)
    );
\y_IBUF[23]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(23),
      O => y_IBUF(23)
    );
\y_IBUF[2]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(2),
      O => y_IBUF(2)
    );
\y_IBUF[3]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(3),
      O => y_IBUF(3)
    );
\y_IBUF[4]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(4),
      O => y_IBUF(4)
    );
\y_IBUF[5]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(5),
      O => y_IBUF(5)
    );
\y_IBUF[6]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(6),
      O => y_IBUF(6)
    );
\y_IBUF[7]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(7),
      O => y_IBUF(7)
    );
\y_IBUF[8]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(8),
      O => y_IBUF(8)
    );
\y_IBUF[9]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => y(9),
      O => y_IBUF(9)
    );
end STRUCTURE;
