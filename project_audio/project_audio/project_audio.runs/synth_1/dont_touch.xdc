# This file is automatically generated.
# It contains project source information necessary for synthesis and implementation.

# Block Designs: bd/project_audio_design/project_audio_design.bd
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==project_audio_design || ORIG_REF_NAME==project_audio_design} -quiet] -quiet

# IP: bd/project_audio_design/ip/project_audio_design_processing_system7_0_0/project_audio_design_processing_system7_0_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==project_audio_design_processing_system7_0_0 || ORIG_REF_NAME==project_audio_design_processing_system7_0_0} -quiet] -quiet

# IP: bd/project_audio_design/ip/project_audio_design_ps7_0_axi_periph_0/project_audio_design_ps7_0_axi_periph_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==project_audio_design_ps7_0_axi_periph_0 || ORIG_REF_NAME==project_audio_design_ps7_0_axi_periph_0} -quiet] -quiet

# IP: bd/project_audio_design/ip/project_audio_design_rst_ps7_0_100M_0/project_audio_design_rst_ps7_0_100M_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==project_audio_design_rst_ps7_0_100M_0 || ORIG_REF_NAME==project_audio_design_rst_ps7_0_100M_0} -quiet] -quiet

# IP: bd/project_audio_design/ip/project_audio_design_project_audio_ip_0_0/project_audio_design_project_audio_ip_0_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==project_audio_design_project_audio_ip_0_0 || ORIG_REF_NAME==project_audio_design_project_audio_ip_0_0} -quiet] -quiet

# IP: bd/project_audio_design/ip/project_audio_design_auto_pc_0/project_audio_design_auto_pc_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==project_audio_design_auto_pc_0 || ORIG_REF_NAME==project_audio_design_auto_pc_0} -quiet] -quiet

# XDC: /home/juan/Documentos/JuanK/universidad/profun_2/project_audio/project_audio/project_audio.gen/sources_1/bd/project_audio_design/project_audio_design_ooc.xdc
