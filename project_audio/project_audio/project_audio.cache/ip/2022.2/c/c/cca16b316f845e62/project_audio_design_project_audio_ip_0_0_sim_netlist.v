// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Wed Mar 22 08:36:44 2023
// Host        : juan-Inspiron-14-3467 running 64-bit Linux Mint 21.1
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ project_audio_design_project_audio_ip_0_0_sim_netlist.v
// Design      : project_audio_design_project_audio_ip_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_circle_buffer
   (dout_b,
    s00_axi_aclk,
    p_0_in_0,
    s00_axi_wvalid,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    s00_axi_awvalid,
    s00_axi_wdata);
  output [23:0]dout_b;
  input s00_axi_aclk;
  input [1:0]p_0_in_0;
  input s00_axi_wvalid;
  input S_AXI_WREADY;
  input S_AXI_AWREADY;
  input s00_axi_awvalid;
  input [23:0]s00_axi_wdata;

  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire [23:0]dout_b;
  wire en_a;
  wire end_v0_carry__0_n_0;
  wire end_v0_carry__0_n_1;
  wire end_v0_carry__0_n_2;
  wire end_v0_carry__0_n_3;
  wire end_v0_carry__1_n_0;
  wire end_v0_carry__1_n_1;
  wire end_v0_carry__1_n_2;
  wire end_v0_carry__1_n_3;
  wire end_v0_carry__2_n_2;
  wire end_v0_carry__2_n_3;
  wire end_v0_carry_n_0;
  wire end_v0_carry_n_1;
  wire end_v0_carry_n_2;
  wire end_v0_carry_n_3;
  wire \end_v_reg[0]_rep__0_n_0 ;
  wire \end_v_reg[0]_rep__10_n_0 ;
  wire \end_v_reg[0]_rep__11_n_0 ;
  wire \end_v_reg[0]_rep__12_n_0 ;
  wire \end_v_reg[0]_rep__13_n_0 ;
  wire \end_v_reg[0]_rep__14_n_0 ;
  wire \end_v_reg[0]_rep__15_n_0 ;
  wire \end_v_reg[0]_rep__16_n_0 ;
  wire \end_v_reg[0]_rep__17_n_0 ;
  wire \end_v_reg[0]_rep__18_n_0 ;
  wire \end_v_reg[0]_rep__19_n_0 ;
  wire \end_v_reg[0]_rep__1_n_0 ;
  wire \end_v_reg[0]_rep__20_n_0 ;
  wire \end_v_reg[0]_rep__21_n_0 ;
  wire \end_v_reg[0]_rep__22_n_0 ;
  wire \end_v_reg[0]_rep__2_n_0 ;
  wire \end_v_reg[0]_rep__3_n_0 ;
  wire \end_v_reg[0]_rep__4_n_0 ;
  wire \end_v_reg[0]_rep__5_n_0 ;
  wire \end_v_reg[0]_rep__6_n_0 ;
  wire \end_v_reg[0]_rep__7_n_0 ;
  wire \end_v_reg[0]_rep__8_n_0 ;
  wire \end_v_reg[0]_rep__9_n_0 ;
  wire \end_v_reg[0]_rep_n_0 ;
  wire \end_v_reg[10]_rep__0_n_0 ;
  wire \end_v_reg[10]_rep__10_n_0 ;
  wire \end_v_reg[10]_rep__11_n_0 ;
  wire \end_v_reg[10]_rep__12_n_0 ;
  wire \end_v_reg[10]_rep__13_n_0 ;
  wire \end_v_reg[10]_rep__14_n_0 ;
  wire \end_v_reg[10]_rep__15_n_0 ;
  wire \end_v_reg[10]_rep__16_n_0 ;
  wire \end_v_reg[10]_rep__17_n_0 ;
  wire \end_v_reg[10]_rep__18_n_0 ;
  wire \end_v_reg[10]_rep__19_n_0 ;
  wire \end_v_reg[10]_rep__1_n_0 ;
  wire \end_v_reg[10]_rep__20_n_0 ;
  wire \end_v_reg[10]_rep__21_n_0 ;
  wire \end_v_reg[10]_rep__22_n_0 ;
  wire \end_v_reg[10]_rep__2_n_0 ;
  wire \end_v_reg[10]_rep__3_n_0 ;
  wire \end_v_reg[10]_rep__4_n_0 ;
  wire \end_v_reg[10]_rep__5_n_0 ;
  wire \end_v_reg[10]_rep__6_n_0 ;
  wire \end_v_reg[10]_rep__7_n_0 ;
  wire \end_v_reg[10]_rep__8_n_0 ;
  wire \end_v_reg[10]_rep__9_n_0 ;
  wire \end_v_reg[10]_rep_n_0 ;
  wire \end_v_reg[11]_rep__0_n_0 ;
  wire \end_v_reg[11]_rep__10_n_0 ;
  wire \end_v_reg[11]_rep__11_n_0 ;
  wire \end_v_reg[11]_rep__12_n_0 ;
  wire \end_v_reg[11]_rep__13_n_0 ;
  wire \end_v_reg[11]_rep__14_n_0 ;
  wire \end_v_reg[11]_rep__15_n_0 ;
  wire \end_v_reg[11]_rep__16_n_0 ;
  wire \end_v_reg[11]_rep__17_n_0 ;
  wire \end_v_reg[11]_rep__18_n_0 ;
  wire \end_v_reg[11]_rep__19_n_0 ;
  wire \end_v_reg[11]_rep__1_n_0 ;
  wire \end_v_reg[11]_rep__20_n_0 ;
  wire \end_v_reg[11]_rep__21_n_0 ;
  wire \end_v_reg[11]_rep__22_n_0 ;
  wire \end_v_reg[11]_rep__2_n_0 ;
  wire \end_v_reg[11]_rep__3_n_0 ;
  wire \end_v_reg[11]_rep__4_n_0 ;
  wire \end_v_reg[11]_rep__5_n_0 ;
  wire \end_v_reg[11]_rep__6_n_0 ;
  wire \end_v_reg[11]_rep__7_n_0 ;
  wire \end_v_reg[11]_rep__8_n_0 ;
  wire \end_v_reg[11]_rep__9_n_0 ;
  wire \end_v_reg[11]_rep_n_0 ;
  wire \end_v_reg[12]_rep__0_n_0 ;
  wire \end_v_reg[12]_rep__10_n_0 ;
  wire \end_v_reg[12]_rep__11_n_0 ;
  wire \end_v_reg[12]_rep__12_n_0 ;
  wire \end_v_reg[12]_rep__13_n_0 ;
  wire \end_v_reg[12]_rep__14_n_0 ;
  wire \end_v_reg[12]_rep__15_n_0 ;
  wire \end_v_reg[12]_rep__16_n_0 ;
  wire \end_v_reg[12]_rep__17_n_0 ;
  wire \end_v_reg[12]_rep__18_n_0 ;
  wire \end_v_reg[12]_rep__19_n_0 ;
  wire \end_v_reg[12]_rep__1_n_0 ;
  wire \end_v_reg[12]_rep__20_n_0 ;
  wire \end_v_reg[12]_rep__21_n_0 ;
  wire \end_v_reg[12]_rep__22_n_0 ;
  wire \end_v_reg[12]_rep__2_n_0 ;
  wire \end_v_reg[12]_rep__3_n_0 ;
  wire \end_v_reg[12]_rep__4_n_0 ;
  wire \end_v_reg[12]_rep__5_n_0 ;
  wire \end_v_reg[12]_rep__6_n_0 ;
  wire \end_v_reg[12]_rep__7_n_0 ;
  wire \end_v_reg[12]_rep__8_n_0 ;
  wire \end_v_reg[12]_rep__9_n_0 ;
  wire \end_v_reg[12]_rep_n_0 ;
  wire \end_v_reg[13]_rep__0_n_0 ;
  wire \end_v_reg[13]_rep__10_n_0 ;
  wire \end_v_reg[13]_rep__11_n_0 ;
  wire \end_v_reg[13]_rep__12_n_0 ;
  wire \end_v_reg[13]_rep__13_n_0 ;
  wire \end_v_reg[13]_rep__14_n_0 ;
  wire \end_v_reg[13]_rep__15_n_0 ;
  wire \end_v_reg[13]_rep__16_n_0 ;
  wire \end_v_reg[13]_rep__17_n_0 ;
  wire \end_v_reg[13]_rep__18_n_0 ;
  wire \end_v_reg[13]_rep__19_n_0 ;
  wire \end_v_reg[13]_rep__1_n_0 ;
  wire \end_v_reg[13]_rep__20_n_0 ;
  wire \end_v_reg[13]_rep__21_n_0 ;
  wire \end_v_reg[13]_rep__22_n_0 ;
  wire \end_v_reg[13]_rep__2_n_0 ;
  wire \end_v_reg[13]_rep__3_n_0 ;
  wire \end_v_reg[13]_rep__4_n_0 ;
  wire \end_v_reg[13]_rep__5_n_0 ;
  wire \end_v_reg[13]_rep__6_n_0 ;
  wire \end_v_reg[13]_rep__7_n_0 ;
  wire \end_v_reg[13]_rep__8_n_0 ;
  wire \end_v_reg[13]_rep__9_n_0 ;
  wire \end_v_reg[13]_rep_n_0 ;
  wire \end_v_reg[14]_rep__0_n_0 ;
  wire \end_v_reg[14]_rep__10_n_0 ;
  wire \end_v_reg[14]_rep__11_n_0 ;
  wire \end_v_reg[14]_rep__12_n_0 ;
  wire \end_v_reg[14]_rep__13_n_0 ;
  wire \end_v_reg[14]_rep__14_n_0 ;
  wire \end_v_reg[14]_rep__15_n_0 ;
  wire \end_v_reg[14]_rep__16_n_0 ;
  wire \end_v_reg[14]_rep__17_n_0 ;
  wire \end_v_reg[14]_rep__18_n_0 ;
  wire \end_v_reg[14]_rep__19_n_0 ;
  wire \end_v_reg[14]_rep__1_n_0 ;
  wire \end_v_reg[14]_rep__20_n_0 ;
  wire \end_v_reg[14]_rep__21_n_0 ;
  wire \end_v_reg[14]_rep__22_n_0 ;
  wire \end_v_reg[14]_rep__2_n_0 ;
  wire \end_v_reg[14]_rep__3_n_0 ;
  wire \end_v_reg[14]_rep__4_n_0 ;
  wire \end_v_reg[14]_rep__5_n_0 ;
  wire \end_v_reg[14]_rep__6_n_0 ;
  wire \end_v_reg[14]_rep__7_n_0 ;
  wire \end_v_reg[14]_rep__8_n_0 ;
  wire \end_v_reg[14]_rep__9_n_0 ;
  wire \end_v_reg[14]_rep_n_0 ;
  wire \end_v_reg[1]_rep__0_n_0 ;
  wire \end_v_reg[1]_rep__10_n_0 ;
  wire \end_v_reg[1]_rep__11_n_0 ;
  wire \end_v_reg[1]_rep__12_n_0 ;
  wire \end_v_reg[1]_rep__13_n_0 ;
  wire \end_v_reg[1]_rep__14_n_0 ;
  wire \end_v_reg[1]_rep__15_n_0 ;
  wire \end_v_reg[1]_rep__16_n_0 ;
  wire \end_v_reg[1]_rep__17_n_0 ;
  wire \end_v_reg[1]_rep__18_n_0 ;
  wire \end_v_reg[1]_rep__19_n_0 ;
  wire \end_v_reg[1]_rep__1_n_0 ;
  wire \end_v_reg[1]_rep__20_n_0 ;
  wire \end_v_reg[1]_rep__21_n_0 ;
  wire \end_v_reg[1]_rep__22_n_0 ;
  wire \end_v_reg[1]_rep__2_n_0 ;
  wire \end_v_reg[1]_rep__3_n_0 ;
  wire \end_v_reg[1]_rep__4_n_0 ;
  wire \end_v_reg[1]_rep__5_n_0 ;
  wire \end_v_reg[1]_rep__6_n_0 ;
  wire \end_v_reg[1]_rep__7_n_0 ;
  wire \end_v_reg[1]_rep__8_n_0 ;
  wire \end_v_reg[1]_rep__9_n_0 ;
  wire \end_v_reg[1]_rep_n_0 ;
  wire \end_v_reg[2]_rep__0_n_0 ;
  wire \end_v_reg[2]_rep__10_n_0 ;
  wire \end_v_reg[2]_rep__11_n_0 ;
  wire \end_v_reg[2]_rep__12_n_0 ;
  wire \end_v_reg[2]_rep__13_n_0 ;
  wire \end_v_reg[2]_rep__14_n_0 ;
  wire \end_v_reg[2]_rep__15_n_0 ;
  wire \end_v_reg[2]_rep__16_n_0 ;
  wire \end_v_reg[2]_rep__17_n_0 ;
  wire \end_v_reg[2]_rep__18_n_0 ;
  wire \end_v_reg[2]_rep__19_n_0 ;
  wire \end_v_reg[2]_rep__1_n_0 ;
  wire \end_v_reg[2]_rep__20_n_0 ;
  wire \end_v_reg[2]_rep__21_n_0 ;
  wire \end_v_reg[2]_rep__22_n_0 ;
  wire \end_v_reg[2]_rep__2_n_0 ;
  wire \end_v_reg[2]_rep__3_n_0 ;
  wire \end_v_reg[2]_rep__4_n_0 ;
  wire \end_v_reg[2]_rep__5_n_0 ;
  wire \end_v_reg[2]_rep__6_n_0 ;
  wire \end_v_reg[2]_rep__7_n_0 ;
  wire \end_v_reg[2]_rep__8_n_0 ;
  wire \end_v_reg[2]_rep__9_n_0 ;
  wire \end_v_reg[2]_rep_n_0 ;
  wire \end_v_reg[3]_rep__0_n_0 ;
  wire \end_v_reg[3]_rep__10_n_0 ;
  wire \end_v_reg[3]_rep__11_n_0 ;
  wire \end_v_reg[3]_rep__12_n_0 ;
  wire \end_v_reg[3]_rep__13_n_0 ;
  wire \end_v_reg[3]_rep__14_n_0 ;
  wire \end_v_reg[3]_rep__15_n_0 ;
  wire \end_v_reg[3]_rep__16_n_0 ;
  wire \end_v_reg[3]_rep__17_n_0 ;
  wire \end_v_reg[3]_rep__18_n_0 ;
  wire \end_v_reg[3]_rep__19_n_0 ;
  wire \end_v_reg[3]_rep__1_n_0 ;
  wire \end_v_reg[3]_rep__20_n_0 ;
  wire \end_v_reg[3]_rep__21_n_0 ;
  wire \end_v_reg[3]_rep__22_n_0 ;
  wire \end_v_reg[3]_rep__2_n_0 ;
  wire \end_v_reg[3]_rep__3_n_0 ;
  wire \end_v_reg[3]_rep__4_n_0 ;
  wire \end_v_reg[3]_rep__5_n_0 ;
  wire \end_v_reg[3]_rep__6_n_0 ;
  wire \end_v_reg[3]_rep__7_n_0 ;
  wire \end_v_reg[3]_rep__8_n_0 ;
  wire \end_v_reg[3]_rep__9_n_0 ;
  wire \end_v_reg[3]_rep_n_0 ;
  wire \end_v_reg[4]_rep__0_n_0 ;
  wire \end_v_reg[4]_rep__10_n_0 ;
  wire \end_v_reg[4]_rep__11_n_0 ;
  wire \end_v_reg[4]_rep__12_n_0 ;
  wire \end_v_reg[4]_rep__13_n_0 ;
  wire \end_v_reg[4]_rep__14_n_0 ;
  wire \end_v_reg[4]_rep__15_n_0 ;
  wire \end_v_reg[4]_rep__16_n_0 ;
  wire \end_v_reg[4]_rep__17_n_0 ;
  wire \end_v_reg[4]_rep__18_n_0 ;
  wire \end_v_reg[4]_rep__19_n_0 ;
  wire \end_v_reg[4]_rep__1_n_0 ;
  wire \end_v_reg[4]_rep__20_n_0 ;
  wire \end_v_reg[4]_rep__21_n_0 ;
  wire \end_v_reg[4]_rep__22_n_0 ;
  wire \end_v_reg[4]_rep__2_n_0 ;
  wire \end_v_reg[4]_rep__3_n_0 ;
  wire \end_v_reg[4]_rep__4_n_0 ;
  wire \end_v_reg[4]_rep__5_n_0 ;
  wire \end_v_reg[4]_rep__6_n_0 ;
  wire \end_v_reg[4]_rep__7_n_0 ;
  wire \end_v_reg[4]_rep__8_n_0 ;
  wire \end_v_reg[4]_rep__9_n_0 ;
  wire \end_v_reg[4]_rep_n_0 ;
  wire \end_v_reg[5]_rep__0_n_0 ;
  wire \end_v_reg[5]_rep__10_n_0 ;
  wire \end_v_reg[5]_rep__11_n_0 ;
  wire \end_v_reg[5]_rep__12_n_0 ;
  wire \end_v_reg[5]_rep__13_n_0 ;
  wire \end_v_reg[5]_rep__14_n_0 ;
  wire \end_v_reg[5]_rep__15_n_0 ;
  wire \end_v_reg[5]_rep__16_n_0 ;
  wire \end_v_reg[5]_rep__17_n_0 ;
  wire \end_v_reg[5]_rep__18_n_0 ;
  wire \end_v_reg[5]_rep__19_n_0 ;
  wire \end_v_reg[5]_rep__1_n_0 ;
  wire \end_v_reg[5]_rep__20_n_0 ;
  wire \end_v_reg[5]_rep__21_n_0 ;
  wire \end_v_reg[5]_rep__22_n_0 ;
  wire \end_v_reg[5]_rep__2_n_0 ;
  wire \end_v_reg[5]_rep__3_n_0 ;
  wire \end_v_reg[5]_rep__4_n_0 ;
  wire \end_v_reg[5]_rep__5_n_0 ;
  wire \end_v_reg[5]_rep__6_n_0 ;
  wire \end_v_reg[5]_rep__7_n_0 ;
  wire \end_v_reg[5]_rep__8_n_0 ;
  wire \end_v_reg[5]_rep__9_n_0 ;
  wire \end_v_reg[5]_rep_n_0 ;
  wire \end_v_reg[6]_rep__0_n_0 ;
  wire \end_v_reg[6]_rep__10_n_0 ;
  wire \end_v_reg[6]_rep__11_n_0 ;
  wire \end_v_reg[6]_rep__12_n_0 ;
  wire \end_v_reg[6]_rep__13_n_0 ;
  wire \end_v_reg[6]_rep__14_n_0 ;
  wire \end_v_reg[6]_rep__15_n_0 ;
  wire \end_v_reg[6]_rep__16_n_0 ;
  wire \end_v_reg[6]_rep__17_n_0 ;
  wire \end_v_reg[6]_rep__18_n_0 ;
  wire \end_v_reg[6]_rep__19_n_0 ;
  wire \end_v_reg[6]_rep__1_n_0 ;
  wire \end_v_reg[6]_rep__20_n_0 ;
  wire \end_v_reg[6]_rep__21_n_0 ;
  wire \end_v_reg[6]_rep__22_n_0 ;
  wire \end_v_reg[6]_rep__2_n_0 ;
  wire \end_v_reg[6]_rep__3_n_0 ;
  wire \end_v_reg[6]_rep__4_n_0 ;
  wire \end_v_reg[6]_rep__5_n_0 ;
  wire \end_v_reg[6]_rep__6_n_0 ;
  wire \end_v_reg[6]_rep__7_n_0 ;
  wire \end_v_reg[6]_rep__8_n_0 ;
  wire \end_v_reg[6]_rep__9_n_0 ;
  wire \end_v_reg[6]_rep_n_0 ;
  wire \end_v_reg[7]_rep__0_n_0 ;
  wire \end_v_reg[7]_rep__10_n_0 ;
  wire \end_v_reg[7]_rep__11_n_0 ;
  wire \end_v_reg[7]_rep__12_n_0 ;
  wire \end_v_reg[7]_rep__13_n_0 ;
  wire \end_v_reg[7]_rep__14_n_0 ;
  wire \end_v_reg[7]_rep__15_n_0 ;
  wire \end_v_reg[7]_rep__16_n_0 ;
  wire \end_v_reg[7]_rep__17_n_0 ;
  wire \end_v_reg[7]_rep__18_n_0 ;
  wire \end_v_reg[7]_rep__19_n_0 ;
  wire \end_v_reg[7]_rep__1_n_0 ;
  wire \end_v_reg[7]_rep__20_n_0 ;
  wire \end_v_reg[7]_rep__21_n_0 ;
  wire \end_v_reg[7]_rep__22_n_0 ;
  wire \end_v_reg[7]_rep__2_n_0 ;
  wire \end_v_reg[7]_rep__3_n_0 ;
  wire \end_v_reg[7]_rep__4_n_0 ;
  wire \end_v_reg[7]_rep__5_n_0 ;
  wire \end_v_reg[7]_rep__6_n_0 ;
  wire \end_v_reg[7]_rep__7_n_0 ;
  wire \end_v_reg[7]_rep__8_n_0 ;
  wire \end_v_reg[7]_rep__9_n_0 ;
  wire \end_v_reg[7]_rep_n_0 ;
  wire \end_v_reg[8]_rep__0_n_0 ;
  wire \end_v_reg[8]_rep__10_n_0 ;
  wire \end_v_reg[8]_rep__11_n_0 ;
  wire \end_v_reg[8]_rep__12_n_0 ;
  wire \end_v_reg[8]_rep__13_n_0 ;
  wire \end_v_reg[8]_rep__14_n_0 ;
  wire \end_v_reg[8]_rep__15_n_0 ;
  wire \end_v_reg[8]_rep__16_n_0 ;
  wire \end_v_reg[8]_rep__17_n_0 ;
  wire \end_v_reg[8]_rep__18_n_0 ;
  wire \end_v_reg[8]_rep__19_n_0 ;
  wire \end_v_reg[8]_rep__1_n_0 ;
  wire \end_v_reg[8]_rep__20_n_0 ;
  wire \end_v_reg[8]_rep__21_n_0 ;
  wire \end_v_reg[8]_rep__22_n_0 ;
  wire \end_v_reg[8]_rep__2_n_0 ;
  wire \end_v_reg[8]_rep__3_n_0 ;
  wire \end_v_reg[8]_rep__4_n_0 ;
  wire \end_v_reg[8]_rep__5_n_0 ;
  wire \end_v_reg[8]_rep__6_n_0 ;
  wire \end_v_reg[8]_rep__7_n_0 ;
  wire \end_v_reg[8]_rep__8_n_0 ;
  wire \end_v_reg[8]_rep__9_n_0 ;
  wire \end_v_reg[8]_rep_n_0 ;
  wire \end_v_reg[9]_rep__0_n_0 ;
  wire \end_v_reg[9]_rep__10_n_0 ;
  wire \end_v_reg[9]_rep__11_n_0 ;
  wire \end_v_reg[9]_rep__12_n_0 ;
  wire \end_v_reg[9]_rep__13_n_0 ;
  wire \end_v_reg[9]_rep__14_n_0 ;
  wire \end_v_reg[9]_rep__15_n_0 ;
  wire \end_v_reg[9]_rep__16_n_0 ;
  wire \end_v_reg[9]_rep__17_n_0 ;
  wire \end_v_reg[9]_rep__18_n_0 ;
  wire \end_v_reg[9]_rep__19_n_0 ;
  wire \end_v_reg[9]_rep__1_n_0 ;
  wire \end_v_reg[9]_rep__20_n_0 ;
  wire \end_v_reg[9]_rep__21_n_0 ;
  wire \end_v_reg[9]_rep__22_n_0 ;
  wire \end_v_reg[9]_rep__2_n_0 ;
  wire \end_v_reg[9]_rep__3_n_0 ;
  wire \end_v_reg[9]_rep__4_n_0 ;
  wire \end_v_reg[9]_rep__5_n_0 ;
  wire \end_v_reg[9]_rep__6_n_0 ;
  wire \end_v_reg[9]_rep__7_n_0 ;
  wire \end_v_reg[9]_rep__8_n_0 ;
  wire \end_v_reg[9]_rep__9_n_0 ;
  wire \end_v_reg[9]_rep_n_0 ;
  wire [14:0]p_0_in;
  wire [1:0]p_0_in_0;
  wire reg_d0_n_0;
  wire reg_d0_n_1;
  wire reg_d0_n_10;
  wire reg_d0_n_11;
  wire reg_d0_n_12;
  wire reg_d0_n_13;
  wire reg_d0_n_14;
  wire reg_d0_n_2;
  wire reg_d0_n_3;
  wire reg_d0_n_4;
  wire reg_d0_n_5;
  wire reg_d0_n_6;
  wire reg_d0_n_7;
  wire reg_d0_n_8;
  wire reg_d0_n_9;
  wire s00_axi_aclk;
  wire s00_axi_awvalid;
  wire [23:0]s00_axi_wdata;
  wire s00_axi_wvalid;
  wire \start_v[0]_i_3_n_0 ;
  wire \start_v[0]_i_4_n_0 ;
  wire \start_v[0]_i_5_n_0 ;
  wire \start_v[0]_i_6_n_0 ;
  wire \start_v[12]_i_2_n_0 ;
  wire \start_v[12]_i_3_n_0 ;
  wire \start_v[12]_i_4_n_0 ;
  wire \start_v[4]_i_2_n_0 ;
  wire \start_v[4]_i_3_n_0 ;
  wire \start_v[4]_i_4_n_0 ;
  wire \start_v[4]_i_5_n_0 ;
  wire \start_v[8]_i_2_n_0 ;
  wire \start_v[8]_i_3_n_0 ;
  wire \start_v[8]_i_4_n_0 ;
  wire \start_v[8]_i_5_n_0 ;
  wire [14:0]start_v_reg;
  wire \start_v_reg[0]_i_2_n_0 ;
  wire \start_v_reg[0]_i_2_n_1 ;
  wire \start_v_reg[0]_i_2_n_2 ;
  wire \start_v_reg[0]_i_2_n_3 ;
  wire \start_v_reg[0]_i_2_n_4 ;
  wire \start_v_reg[0]_i_2_n_5 ;
  wire \start_v_reg[0]_i_2_n_6 ;
  wire \start_v_reg[0]_i_2_n_7 ;
  wire \start_v_reg[0]_rep__0_n_0 ;
  wire \start_v_reg[0]_rep__10_n_0 ;
  wire \start_v_reg[0]_rep__11_n_0 ;
  wire \start_v_reg[0]_rep__12_n_0 ;
  wire \start_v_reg[0]_rep__13_n_0 ;
  wire \start_v_reg[0]_rep__14_n_0 ;
  wire \start_v_reg[0]_rep__15_n_0 ;
  wire \start_v_reg[0]_rep__16_n_0 ;
  wire \start_v_reg[0]_rep__17_n_0 ;
  wire \start_v_reg[0]_rep__18_n_0 ;
  wire \start_v_reg[0]_rep__19_n_0 ;
  wire \start_v_reg[0]_rep__1_n_0 ;
  wire \start_v_reg[0]_rep__20_n_0 ;
  wire \start_v_reg[0]_rep__21_n_0 ;
  wire \start_v_reg[0]_rep__22_n_0 ;
  wire \start_v_reg[0]_rep__23_n_0 ;
  wire \start_v_reg[0]_rep__2_n_0 ;
  wire \start_v_reg[0]_rep__3_n_0 ;
  wire \start_v_reg[0]_rep__4_n_0 ;
  wire \start_v_reg[0]_rep__5_n_0 ;
  wire \start_v_reg[0]_rep__6_n_0 ;
  wire \start_v_reg[0]_rep__7_n_0 ;
  wire \start_v_reg[0]_rep__8_n_0 ;
  wire \start_v_reg[0]_rep__9_n_0 ;
  wire \start_v_reg[0]_rep_n_0 ;
  wire \start_v_reg[10]_rep__0_n_0 ;
  wire \start_v_reg[10]_rep__10_n_0 ;
  wire \start_v_reg[10]_rep__11_n_0 ;
  wire \start_v_reg[10]_rep__12_n_0 ;
  wire \start_v_reg[10]_rep__13_n_0 ;
  wire \start_v_reg[10]_rep__14_n_0 ;
  wire \start_v_reg[10]_rep__15_n_0 ;
  wire \start_v_reg[10]_rep__16_n_0 ;
  wire \start_v_reg[10]_rep__17_n_0 ;
  wire \start_v_reg[10]_rep__18_n_0 ;
  wire \start_v_reg[10]_rep__19_n_0 ;
  wire \start_v_reg[10]_rep__1_n_0 ;
  wire \start_v_reg[10]_rep__20_n_0 ;
  wire \start_v_reg[10]_rep__21_n_0 ;
  wire \start_v_reg[10]_rep__22_n_0 ;
  wire \start_v_reg[10]_rep__23_n_0 ;
  wire \start_v_reg[10]_rep__2_n_0 ;
  wire \start_v_reg[10]_rep__3_n_0 ;
  wire \start_v_reg[10]_rep__4_n_0 ;
  wire \start_v_reg[10]_rep__5_n_0 ;
  wire \start_v_reg[10]_rep__6_n_0 ;
  wire \start_v_reg[10]_rep__7_n_0 ;
  wire \start_v_reg[10]_rep__8_n_0 ;
  wire \start_v_reg[10]_rep__9_n_0 ;
  wire \start_v_reg[10]_rep_n_0 ;
  wire \start_v_reg[11]_rep__0_n_0 ;
  wire \start_v_reg[11]_rep__10_n_0 ;
  wire \start_v_reg[11]_rep__11_n_0 ;
  wire \start_v_reg[11]_rep__12_n_0 ;
  wire \start_v_reg[11]_rep__13_n_0 ;
  wire \start_v_reg[11]_rep__14_n_0 ;
  wire \start_v_reg[11]_rep__15_n_0 ;
  wire \start_v_reg[11]_rep__16_n_0 ;
  wire \start_v_reg[11]_rep__17_n_0 ;
  wire \start_v_reg[11]_rep__18_n_0 ;
  wire \start_v_reg[11]_rep__19_n_0 ;
  wire \start_v_reg[11]_rep__1_n_0 ;
  wire \start_v_reg[11]_rep__20_n_0 ;
  wire \start_v_reg[11]_rep__21_n_0 ;
  wire \start_v_reg[11]_rep__22_n_0 ;
  wire \start_v_reg[11]_rep__23_n_0 ;
  wire \start_v_reg[11]_rep__2_n_0 ;
  wire \start_v_reg[11]_rep__3_n_0 ;
  wire \start_v_reg[11]_rep__4_n_0 ;
  wire \start_v_reg[11]_rep__5_n_0 ;
  wire \start_v_reg[11]_rep__6_n_0 ;
  wire \start_v_reg[11]_rep__7_n_0 ;
  wire \start_v_reg[11]_rep__8_n_0 ;
  wire \start_v_reg[11]_rep__9_n_0 ;
  wire \start_v_reg[11]_rep_n_0 ;
  wire \start_v_reg[12]_i_1_n_2 ;
  wire \start_v_reg[12]_i_1_n_3 ;
  wire \start_v_reg[12]_i_1_n_5 ;
  wire \start_v_reg[12]_i_1_n_6 ;
  wire \start_v_reg[12]_i_1_n_7 ;
  wire \start_v_reg[12]_rep__0_n_0 ;
  wire \start_v_reg[12]_rep__10_n_0 ;
  wire \start_v_reg[12]_rep__11_n_0 ;
  wire \start_v_reg[12]_rep__12_n_0 ;
  wire \start_v_reg[12]_rep__13_n_0 ;
  wire \start_v_reg[12]_rep__14_n_0 ;
  wire \start_v_reg[12]_rep__15_n_0 ;
  wire \start_v_reg[12]_rep__16_n_0 ;
  wire \start_v_reg[12]_rep__17_n_0 ;
  wire \start_v_reg[12]_rep__18_n_0 ;
  wire \start_v_reg[12]_rep__19_n_0 ;
  wire \start_v_reg[12]_rep__1_n_0 ;
  wire \start_v_reg[12]_rep__20_n_0 ;
  wire \start_v_reg[12]_rep__21_n_0 ;
  wire \start_v_reg[12]_rep__22_n_0 ;
  wire \start_v_reg[12]_rep__23_n_0 ;
  wire \start_v_reg[12]_rep__2_n_0 ;
  wire \start_v_reg[12]_rep__3_n_0 ;
  wire \start_v_reg[12]_rep__4_n_0 ;
  wire \start_v_reg[12]_rep__5_n_0 ;
  wire \start_v_reg[12]_rep__6_n_0 ;
  wire \start_v_reg[12]_rep__7_n_0 ;
  wire \start_v_reg[12]_rep__8_n_0 ;
  wire \start_v_reg[12]_rep__9_n_0 ;
  wire \start_v_reg[12]_rep_n_0 ;
  wire \start_v_reg[13]_rep__0_n_0 ;
  wire \start_v_reg[13]_rep__10_n_0 ;
  wire \start_v_reg[13]_rep__11_n_0 ;
  wire \start_v_reg[13]_rep__12_n_0 ;
  wire \start_v_reg[13]_rep__13_n_0 ;
  wire \start_v_reg[13]_rep__14_n_0 ;
  wire \start_v_reg[13]_rep__15_n_0 ;
  wire \start_v_reg[13]_rep__16_n_0 ;
  wire \start_v_reg[13]_rep__17_n_0 ;
  wire \start_v_reg[13]_rep__18_n_0 ;
  wire \start_v_reg[13]_rep__19_n_0 ;
  wire \start_v_reg[13]_rep__1_n_0 ;
  wire \start_v_reg[13]_rep__20_n_0 ;
  wire \start_v_reg[13]_rep__21_n_0 ;
  wire \start_v_reg[13]_rep__22_n_0 ;
  wire \start_v_reg[13]_rep__23_n_0 ;
  wire \start_v_reg[13]_rep__2_n_0 ;
  wire \start_v_reg[13]_rep__3_n_0 ;
  wire \start_v_reg[13]_rep__4_n_0 ;
  wire \start_v_reg[13]_rep__5_n_0 ;
  wire \start_v_reg[13]_rep__6_n_0 ;
  wire \start_v_reg[13]_rep__7_n_0 ;
  wire \start_v_reg[13]_rep__8_n_0 ;
  wire \start_v_reg[13]_rep__9_n_0 ;
  wire \start_v_reg[13]_rep_n_0 ;
  wire \start_v_reg[14]_rep__0_n_0 ;
  wire \start_v_reg[14]_rep__10_n_0 ;
  wire \start_v_reg[14]_rep__11_n_0 ;
  wire \start_v_reg[14]_rep__12_n_0 ;
  wire \start_v_reg[14]_rep__13_n_0 ;
  wire \start_v_reg[14]_rep__14_n_0 ;
  wire \start_v_reg[14]_rep__15_n_0 ;
  wire \start_v_reg[14]_rep__16_n_0 ;
  wire \start_v_reg[14]_rep__17_n_0 ;
  wire \start_v_reg[14]_rep__18_n_0 ;
  wire \start_v_reg[14]_rep__19_n_0 ;
  wire \start_v_reg[14]_rep__1_n_0 ;
  wire \start_v_reg[14]_rep__20_n_0 ;
  wire \start_v_reg[14]_rep__21_n_0 ;
  wire \start_v_reg[14]_rep__22_n_0 ;
  wire \start_v_reg[14]_rep__2_n_0 ;
  wire \start_v_reg[14]_rep__3_n_0 ;
  wire \start_v_reg[14]_rep__4_n_0 ;
  wire \start_v_reg[14]_rep__5_n_0 ;
  wire \start_v_reg[14]_rep__6_n_0 ;
  wire \start_v_reg[14]_rep__7_n_0 ;
  wire \start_v_reg[14]_rep__8_n_0 ;
  wire \start_v_reg[14]_rep__9_n_0 ;
  wire \start_v_reg[14]_rep_n_0 ;
  wire \start_v_reg[1]_rep__0_n_0 ;
  wire \start_v_reg[1]_rep__10_n_0 ;
  wire \start_v_reg[1]_rep__11_n_0 ;
  wire \start_v_reg[1]_rep__12_n_0 ;
  wire \start_v_reg[1]_rep__13_n_0 ;
  wire \start_v_reg[1]_rep__14_n_0 ;
  wire \start_v_reg[1]_rep__15_n_0 ;
  wire \start_v_reg[1]_rep__16_n_0 ;
  wire \start_v_reg[1]_rep__17_n_0 ;
  wire \start_v_reg[1]_rep__18_n_0 ;
  wire \start_v_reg[1]_rep__19_n_0 ;
  wire \start_v_reg[1]_rep__1_n_0 ;
  wire \start_v_reg[1]_rep__20_n_0 ;
  wire \start_v_reg[1]_rep__21_n_0 ;
  wire \start_v_reg[1]_rep__22_n_0 ;
  wire \start_v_reg[1]_rep__23_n_0 ;
  wire \start_v_reg[1]_rep__2_n_0 ;
  wire \start_v_reg[1]_rep__3_n_0 ;
  wire \start_v_reg[1]_rep__4_n_0 ;
  wire \start_v_reg[1]_rep__5_n_0 ;
  wire \start_v_reg[1]_rep__6_n_0 ;
  wire \start_v_reg[1]_rep__7_n_0 ;
  wire \start_v_reg[1]_rep__8_n_0 ;
  wire \start_v_reg[1]_rep__9_n_0 ;
  wire \start_v_reg[1]_rep_n_0 ;
  wire \start_v_reg[2]_rep__0_n_0 ;
  wire \start_v_reg[2]_rep__10_n_0 ;
  wire \start_v_reg[2]_rep__11_n_0 ;
  wire \start_v_reg[2]_rep__12_n_0 ;
  wire \start_v_reg[2]_rep__13_n_0 ;
  wire \start_v_reg[2]_rep__14_n_0 ;
  wire \start_v_reg[2]_rep__15_n_0 ;
  wire \start_v_reg[2]_rep__16_n_0 ;
  wire \start_v_reg[2]_rep__17_n_0 ;
  wire \start_v_reg[2]_rep__18_n_0 ;
  wire \start_v_reg[2]_rep__19_n_0 ;
  wire \start_v_reg[2]_rep__1_n_0 ;
  wire \start_v_reg[2]_rep__20_n_0 ;
  wire \start_v_reg[2]_rep__21_n_0 ;
  wire \start_v_reg[2]_rep__22_n_0 ;
  wire \start_v_reg[2]_rep__23_n_0 ;
  wire \start_v_reg[2]_rep__2_n_0 ;
  wire \start_v_reg[2]_rep__3_n_0 ;
  wire \start_v_reg[2]_rep__4_n_0 ;
  wire \start_v_reg[2]_rep__5_n_0 ;
  wire \start_v_reg[2]_rep__6_n_0 ;
  wire \start_v_reg[2]_rep__7_n_0 ;
  wire \start_v_reg[2]_rep__8_n_0 ;
  wire \start_v_reg[2]_rep__9_n_0 ;
  wire \start_v_reg[2]_rep_n_0 ;
  wire \start_v_reg[3]_rep__0_n_0 ;
  wire \start_v_reg[3]_rep__10_n_0 ;
  wire \start_v_reg[3]_rep__11_n_0 ;
  wire \start_v_reg[3]_rep__12_n_0 ;
  wire \start_v_reg[3]_rep__13_n_0 ;
  wire \start_v_reg[3]_rep__14_n_0 ;
  wire \start_v_reg[3]_rep__15_n_0 ;
  wire \start_v_reg[3]_rep__16_n_0 ;
  wire \start_v_reg[3]_rep__17_n_0 ;
  wire \start_v_reg[3]_rep__18_n_0 ;
  wire \start_v_reg[3]_rep__19_n_0 ;
  wire \start_v_reg[3]_rep__1_n_0 ;
  wire \start_v_reg[3]_rep__20_n_0 ;
  wire \start_v_reg[3]_rep__21_n_0 ;
  wire \start_v_reg[3]_rep__22_n_0 ;
  wire \start_v_reg[3]_rep__23_n_0 ;
  wire \start_v_reg[3]_rep__2_n_0 ;
  wire \start_v_reg[3]_rep__3_n_0 ;
  wire \start_v_reg[3]_rep__4_n_0 ;
  wire \start_v_reg[3]_rep__5_n_0 ;
  wire \start_v_reg[3]_rep__6_n_0 ;
  wire \start_v_reg[3]_rep__7_n_0 ;
  wire \start_v_reg[3]_rep__8_n_0 ;
  wire \start_v_reg[3]_rep__9_n_0 ;
  wire \start_v_reg[3]_rep_n_0 ;
  wire \start_v_reg[4]_i_1_n_0 ;
  wire \start_v_reg[4]_i_1_n_1 ;
  wire \start_v_reg[4]_i_1_n_2 ;
  wire \start_v_reg[4]_i_1_n_3 ;
  wire \start_v_reg[4]_i_1_n_4 ;
  wire \start_v_reg[4]_i_1_n_5 ;
  wire \start_v_reg[4]_i_1_n_6 ;
  wire \start_v_reg[4]_i_1_n_7 ;
  wire \start_v_reg[4]_rep__0_n_0 ;
  wire \start_v_reg[4]_rep__10_n_0 ;
  wire \start_v_reg[4]_rep__11_n_0 ;
  wire \start_v_reg[4]_rep__12_n_0 ;
  wire \start_v_reg[4]_rep__13_n_0 ;
  wire \start_v_reg[4]_rep__14_n_0 ;
  wire \start_v_reg[4]_rep__15_n_0 ;
  wire \start_v_reg[4]_rep__16_n_0 ;
  wire \start_v_reg[4]_rep__17_n_0 ;
  wire \start_v_reg[4]_rep__18_n_0 ;
  wire \start_v_reg[4]_rep__19_n_0 ;
  wire \start_v_reg[4]_rep__1_n_0 ;
  wire \start_v_reg[4]_rep__20_n_0 ;
  wire \start_v_reg[4]_rep__21_n_0 ;
  wire \start_v_reg[4]_rep__22_n_0 ;
  wire \start_v_reg[4]_rep__23_n_0 ;
  wire \start_v_reg[4]_rep__2_n_0 ;
  wire \start_v_reg[4]_rep__3_n_0 ;
  wire \start_v_reg[4]_rep__4_n_0 ;
  wire \start_v_reg[4]_rep__5_n_0 ;
  wire \start_v_reg[4]_rep__6_n_0 ;
  wire \start_v_reg[4]_rep__7_n_0 ;
  wire \start_v_reg[4]_rep__8_n_0 ;
  wire \start_v_reg[4]_rep__9_n_0 ;
  wire \start_v_reg[4]_rep_n_0 ;
  wire \start_v_reg[5]_rep__0_n_0 ;
  wire \start_v_reg[5]_rep__10_n_0 ;
  wire \start_v_reg[5]_rep__11_n_0 ;
  wire \start_v_reg[5]_rep__12_n_0 ;
  wire \start_v_reg[5]_rep__13_n_0 ;
  wire \start_v_reg[5]_rep__14_n_0 ;
  wire \start_v_reg[5]_rep__15_n_0 ;
  wire \start_v_reg[5]_rep__16_n_0 ;
  wire \start_v_reg[5]_rep__17_n_0 ;
  wire \start_v_reg[5]_rep__18_n_0 ;
  wire \start_v_reg[5]_rep__19_n_0 ;
  wire \start_v_reg[5]_rep__1_n_0 ;
  wire \start_v_reg[5]_rep__20_n_0 ;
  wire \start_v_reg[5]_rep__21_n_0 ;
  wire \start_v_reg[5]_rep__22_n_0 ;
  wire \start_v_reg[5]_rep__23_n_0 ;
  wire \start_v_reg[5]_rep__2_n_0 ;
  wire \start_v_reg[5]_rep__3_n_0 ;
  wire \start_v_reg[5]_rep__4_n_0 ;
  wire \start_v_reg[5]_rep__5_n_0 ;
  wire \start_v_reg[5]_rep__6_n_0 ;
  wire \start_v_reg[5]_rep__7_n_0 ;
  wire \start_v_reg[5]_rep__8_n_0 ;
  wire \start_v_reg[5]_rep__9_n_0 ;
  wire \start_v_reg[5]_rep_n_0 ;
  wire \start_v_reg[6]_rep__0_n_0 ;
  wire \start_v_reg[6]_rep__10_n_0 ;
  wire \start_v_reg[6]_rep__11_n_0 ;
  wire \start_v_reg[6]_rep__12_n_0 ;
  wire \start_v_reg[6]_rep__13_n_0 ;
  wire \start_v_reg[6]_rep__14_n_0 ;
  wire \start_v_reg[6]_rep__15_n_0 ;
  wire \start_v_reg[6]_rep__16_n_0 ;
  wire \start_v_reg[6]_rep__17_n_0 ;
  wire \start_v_reg[6]_rep__18_n_0 ;
  wire \start_v_reg[6]_rep__19_n_0 ;
  wire \start_v_reg[6]_rep__1_n_0 ;
  wire \start_v_reg[6]_rep__20_n_0 ;
  wire \start_v_reg[6]_rep__21_n_0 ;
  wire \start_v_reg[6]_rep__22_n_0 ;
  wire \start_v_reg[6]_rep__23_n_0 ;
  wire \start_v_reg[6]_rep__2_n_0 ;
  wire \start_v_reg[6]_rep__3_n_0 ;
  wire \start_v_reg[6]_rep__4_n_0 ;
  wire \start_v_reg[6]_rep__5_n_0 ;
  wire \start_v_reg[6]_rep__6_n_0 ;
  wire \start_v_reg[6]_rep__7_n_0 ;
  wire \start_v_reg[6]_rep__8_n_0 ;
  wire \start_v_reg[6]_rep__9_n_0 ;
  wire \start_v_reg[6]_rep_n_0 ;
  wire \start_v_reg[7]_rep__0_n_0 ;
  wire \start_v_reg[7]_rep__10_n_0 ;
  wire \start_v_reg[7]_rep__11_n_0 ;
  wire \start_v_reg[7]_rep__12_n_0 ;
  wire \start_v_reg[7]_rep__13_n_0 ;
  wire \start_v_reg[7]_rep__14_n_0 ;
  wire \start_v_reg[7]_rep__15_n_0 ;
  wire \start_v_reg[7]_rep__16_n_0 ;
  wire \start_v_reg[7]_rep__17_n_0 ;
  wire \start_v_reg[7]_rep__18_n_0 ;
  wire \start_v_reg[7]_rep__19_n_0 ;
  wire \start_v_reg[7]_rep__1_n_0 ;
  wire \start_v_reg[7]_rep__20_n_0 ;
  wire \start_v_reg[7]_rep__21_n_0 ;
  wire \start_v_reg[7]_rep__22_n_0 ;
  wire \start_v_reg[7]_rep__23_n_0 ;
  wire \start_v_reg[7]_rep__2_n_0 ;
  wire \start_v_reg[7]_rep__3_n_0 ;
  wire \start_v_reg[7]_rep__4_n_0 ;
  wire \start_v_reg[7]_rep__5_n_0 ;
  wire \start_v_reg[7]_rep__6_n_0 ;
  wire \start_v_reg[7]_rep__7_n_0 ;
  wire \start_v_reg[7]_rep__8_n_0 ;
  wire \start_v_reg[7]_rep__9_n_0 ;
  wire \start_v_reg[7]_rep_n_0 ;
  wire \start_v_reg[8]_i_1_n_0 ;
  wire \start_v_reg[8]_i_1_n_1 ;
  wire \start_v_reg[8]_i_1_n_2 ;
  wire \start_v_reg[8]_i_1_n_3 ;
  wire \start_v_reg[8]_i_1_n_4 ;
  wire \start_v_reg[8]_i_1_n_5 ;
  wire \start_v_reg[8]_i_1_n_6 ;
  wire \start_v_reg[8]_i_1_n_7 ;
  wire \start_v_reg[8]_rep__0_n_0 ;
  wire \start_v_reg[8]_rep__10_n_0 ;
  wire \start_v_reg[8]_rep__11_n_0 ;
  wire \start_v_reg[8]_rep__12_n_0 ;
  wire \start_v_reg[8]_rep__13_n_0 ;
  wire \start_v_reg[8]_rep__14_n_0 ;
  wire \start_v_reg[8]_rep__15_n_0 ;
  wire \start_v_reg[8]_rep__16_n_0 ;
  wire \start_v_reg[8]_rep__17_n_0 ;
  wire \start_v_reg[8]_rep__18_n_0 ;
  wire \start_v_reg[8]_rep__19_n_0 ;
  wire \start_v_reg[8]_rep__1_n_0 ;
  wire \start_v_reg[8]_rep__20_n_0 ;
  wire \start_v_reg[8]_rep__21_n_0 ;
  wire \start_v_reg[8]_rep__22_n_0 ;
  wire \start_v_reg[8]_rep__23_n_0 ;
  wire \start_v_reg[8]_rep__2_n_0 ;
  wire \start_v_reg[8]_rep__3_n_0 ;
  wire \start_v_reg[8]_rep__4_n_0 ;
  wire \start_v_reg[8]_rep__5_n_0 ;
  wire \start_v_reg[8]_rep__6_n_0 ;
  wire \start_v_reg[8]_rep__7_n_0 ;
  wire \start_v_reg[8]_rep__8_n_0 ;
  wire \start_v_reg[8]_rep__9_n_0 ;
  wire \start_v_reg[8]_rep_n_0 ;
  wire \start_v_reg[9]_rep__0_n_0 ;
  wire \start_v_reg[9]_rep__10_n_0 ;
  wire \start_v_reg[9]_rep__11_n_0 ;
  wire \start_v_reg[9]_rep__12_n_0 ;
  wire \start_v_reg[9]_rep__13_n_0 ;
  wire \start_v_reg[9]_rep__14_n_0 ;
  wire \start_v_reg[9]_rep__15_n_0 ;
  wire \start_v_reg[9]_rep__16_n_0 ;
  wire \start_v_reg[9]_rep__17_n_0 ;
  wire \start_v_reg[9]_rep__18_n_0 ;
  wire \start_v_reg[9]_rep__19_n_0 ;
  wire \start_v_reg[9]_rep__1_n_0 ;
  wire \start_v_reg[9]_rep__20_n_0 ;
  wire \start_v_reg[9]_rep__21_n_0 ;
  wire \start_v_reg[9]_rep__22_n_0 ;
  wire \start_v_reg[9]_rep__23_n_0 ;
  wire \start_v_reg[9]_rep__2_n_0 ;
  wire \start_v_reg[9]_rep__3_n_0 ;
  wire \start_v_reg[9]_rep__4_n_0 ;
  wire \start_v_reg[9]_rep__5_n_0 ;
  wire \start_v_reg[9]_rep__6_n_0 ;
  wire \start_v_reg[9]_rep__7_n_0 ;
  wire \start_v_reg[9]_rep__8_n_0 ;
  wire \start_v_reg[9]_rep__9_n_0 ;
  wire \start_v_reg[9]_rep_n_0 ;
  wire [3:2]NLW_end_v0_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_end_v0_carry__2_O_UNCONNECTED;
  wire [3:2]\NLW_start_v_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_start_v_reg[12]_i_1_O_UNCONNECTED ;

  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 end_v0_carry
       (.CI(1'b0),
        .CO({end_v0_carry_n_0,end_v0_carry_n_1,end_v0_carry_n_2,end_v0_carry_n_3}),
        .CYINIT(1'b0),
        .DI(start_v_reg[3:0]),
        .O(p_0_in[3:0]),
        .S({reg_d0_n_3,reg_d0_n_4,reg_d0_n_5,reg_d0_n_6}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 end_v0_carry__0
       (.CI(end_v0_carry_n_0),
        .CO({end_v0_carry__0_n_0,end_v0_carry__0_n_1,end_v0_carry__0_n_2,end_v0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(start_v_reg[7:4]),
        .O(p_0_in[7:4]),
        .S({reg_d0_n_7,reg_d0_n_8,reg_d0_n_9,reg_d0_n_10}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 end_v0_carry__1
       (.CI(end_v0_carry__0_n_0),
        .CO({end_v0_carry__1_n_0,end_v0_carry__1_n_1,end_v0_carry__1_n_2,end_v0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(start_v_reg[11:8]),
        .O(p_0_in[11:8]),
        .S({reg_d0_n_11,reg_d0_n_12,reg_d0_n_13,reg_d0_n_14}));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 end_v0_carry__2
       (.CI(end_v0_carry__1_n_0),
        .CO({NLW_end_v0_carry__2_CO_UNCONNECTED[3:2],end_v0_carry__2_n_2,end_v0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,start_v_reg[13:12]}),
        .O({NLW_end_v0_carry__2_O_UNCONNECTED[3],p_0_in[14:12]}),
        .S({1'b0,reg_d0_n_0,reg_d0_n_1,reg_d0_n_2}));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[0]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(\end_v_reg[0]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[10]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[10]),
        .Q(\end_v_reg[10]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[11]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[11]),
        .Q(\end_v_reg[11]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[12]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[12]),
        .Q(\end_v_reg[12]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[13]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[13]),
        .Q(\end_v_reg[13]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[14]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[14]),
        .Q(\end_v_reg[14]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[1]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(\end_v_reg[1]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[2]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(\end_v_reg[2]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[3]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(\end_v_reg[3]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[4]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(\end_v_reg[4]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[5]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(\end_v_reg[5]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[6]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(\end_v_reg[6]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[7]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(\end_v_reg[7]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[8]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[8]),
        .Q(\end_v_reg[8]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__0 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__1 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__10 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__11 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__12 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__13 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__14 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__15 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__16 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__17 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__18 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__19 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__2 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__20 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__21 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__22 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__3 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__4 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__5 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__6 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__7 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__8 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "end_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b1)) 
    \end_v_reg[9]_rep__9 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(p_0_in[9]),
        .Q(\end_v_reg[9]_rep__9_n_0 ),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dual_port_RAM ram0
       (.ADDRARDADDR({\start_v_reg[14]_rep_n_0 ,\start_v_reg[13]_rep__0_n_0 ,\start_v_reg[12]_rep__0_n_0 ,\start_v_reg[11]_rep__0_n_0 ,\start_v_reg[10]_rep__0_n_0 ,\start_v_reg[9]_rep__0_n_0 ,\start_v_reg[8]_rep__0_n_0 ,\start_v_reg[7]_rep__0_n_0 ,\start_v_reg[6]_rep__0_n_0 ,\start_v_reg[5]_rep__0_n_0 ,\start_v_reg[4]_rep__0_n_0 ,\start_v_reg[3]_rep__0_n_0 ,\start_v_reg[2]_rep__0_n_0 ,\start_v_reg[1]_rep__0_n_0 ,\start_v_reg[0]_rep__0_n_0 }),
        .ADDRBWRADDR({\end_v_reg[14]_rep_n_0 ,\end_v_reg[13]_rep_n_0 ,\end_v_reg[12]_rep_n_0 ,\end_v_reg[11]_rep_n_0 ,\end_v_reg[10]_rep_n_0 ,\end_v_reg[9]_rep_n_0 ,\end_v_reg[8]_rep_n_0 ,\end_v_reg[7]_rep_n_0 ,\end_v_reg[6]_rep_n_0 ,\end_v_reg[5]_rep_n_0 ,\end_v_reg[4]_rep_n_0 ,\end_v_reg[3]_rep_n_0 ,\end_v_reg[2]_rep_n_0 ,\end_v_reg[1]_rep_n_0 ,\end_v_reg[0]_rep_n_0 }),
        .RAM_reg_0_10_0({\start_v_reg[14]_rep__9_n_0 ,\start_v_reg[13]_rep__10_n_0 ,\start_v_reg[12]_rep__10_n_0 ,\start_v_reg[11]_rep__10_n_0 ,\start_v_reg[10]_rep__10_n_0 ,\start_v_reg[9]_rep__10_n_0 ,\start_v_reg[8]_rep__10_n_0 ,\start_v_reg[7]_rep__10_n_0 ,\start_v_reg[6]_rep__10_n_0 ,\start_v_reg[5]_rep__10_n_0 ,\start_v_reg[4]_rep__10_n_0 ,\start_v_reg[3]_rep__10_n_0 ,\start_v_reg[2]_rep__10_n_0 ,\start_v_reg[1]_rep__10_n_0 ,\start_v_reg[0]_rep__10_n_0 }),
        .RAM_reg_0_10_1({\end_v_reg[14]_rep__9_n_0 ,\end_v_reg[13]_rep__9_n_0 ,\end_v_reg[12]_rep__9_n_0 ,\end_v_reg[11]_rep__9_n_0 ,\end_v_reg[10]_rep__9_n_0 ,\end_v_reg[9]_rep__9_n_0 ,\end_v_reg[8]_rep__9_n_0 ,\end_v_reg[7]_rep__9_n_0 ,\end_v_reg[6]_rep__9_n_0 ,\end_v_reg[5]_rep__9_n_0 ,\end_v_reg[4]_rep__9_n_0 ,\end_v_reg[3]_rep__9_n_0 ,\end_v_reg[2]_rep__9_n_0 ,\end_v_reg[1]_rep__9_n_0 ,\end_v_reg[0]_rep__9_n_0 }),
        .RAM_reg_0_11_0({\start_v_reg[14]_rep__10_n_0 ,\start_v_reg[13]_rep__11_n_0 ,\start_v_reg[12]_rep__11_n_0 ,\start_v_reg[11]_rep__11_n_0 ,\start_v_reg[10]_rep__11_n_0 ,\start_v_reg[9]_rep__11_n_0 ,\start_v_reg[8]_rep__11_n_0 ,\start_v_reg[7]_rep__11_n_0 ,\start_v_reg[6]_rep__11_n_0 ,\start_v_reg[5]_rep__11_n_0 ,\start_v_reg[4]_rep__11_n_0 ,\start_v_reg[3]_rep__11_n_0 ,\start_v_reg[2]_rep__11_n_0 ,\start_v_reg[1]_rep__11_n_0 ,\start_v_reg[0]_rep__11_n_0 }),
        .RAM_reg_0_11_1({\end_v_reg[14]_rep__10_n_0 ,\end_v_reg[13]_rep__10_n_0 ,\end_v_reg[12]_rep__10_n_0 ,\end_v_reg[11]_rep__10_n_0 ,\end_v_reg[10]_rep__10_n_0 ,\end_v_reg[9]_rep__10_n_0 ,\end_v_reg[8]_rep__10_n_0 ,\end_v_reg[7]_rep__10_n_0 ,\end_v_reg[6]_rep__10_n_0 ,\end_v_reg[5]_rep__10_n_0 ,\end_v_reg[4]_rep__10_n_0 ,\end_v_reg[3]_rep__10_n_0 ,\end_v_reg[2]_rep__10_n_0 ,\end_v_reg[1]_rep__10_n_0 ,\end_v_reg[0]_rep__10_n_0 }),
        .RAM_reg_0_12_0({\start_v_reg[14]_rep__11_n_0 ,\start_v_reg[13]_rep__12_n_0 ,\start_v_reg[12]_rep__12_n_0 ,\start_v_reg[11]_rep__12_n_0 ,\start_v_reg[10]_rep__12_n_0 ,\start_v_reg[9]_rep__12_n_0 ,\start_v_reg[8]_rep__12_n_0 ,\start_v_reg[7]_rep__12_n_0 ,\start_v_reg[6]_rep__12_n_0 ,\start_v_reg[5]_rep__12_n_0 ,\start_v_reg[4]_rep__12_n_0 ,\start_v_reg[3]_rep__12_n_0 ,\start_v_reg[2]_rep__12_n_0 ,\start_v_reg[1]_rep__12_n_0 ,\start_v_reg[0]_rep__12_n_0 }),
        .RAM_reg_0_12_1({\end_v_reg[14]_rep__11_n_0 ,\end_v_reg[13]_rep__11_n_0 ,\end_v_reg[12]_rep__11_n_0 ,\end_v_reg[11]_rep__11_n_0 ,\end_v_reg[10]_rep__11_n_0 ,\end_v_reg[9]_rep__11_n_0 ,\end_v_reg[8]_rep__11_n_0 ,\end_v_reg[7]_rep__11_n_0 ,\end_v_reg[6]_rep__11_n_0 ,\end_v_reg[5]_rep__11_n_0 ,\end_v_reg[4]_rep__11_n_0 ,\end_v_reg[3]_rep__11_n_0 ,\end_v_reg[2]_rep__11_n_0 ,\end_v_reg[1]_rep__11_n_0 ,\end_v_reg[0]_rep__11_n_0 }),
        .RAM_reg_0_13_0({\start_v_reg[14]_rep__12_n_0 ,\start_v_reg[13]_rep__13_n_0 ,\start_v_reg[12]_rep__13_n_0 ,\start_v_reg[11]_rep__13_n_0 ,\start_v_reg[10]_rep__13_n_0 ,\start_v_reg[9]_rep__13_n_0 ,\start_v_reg[8]_rep__13_n_0 ,\start_v_reg[7]_rep__13_n_0 ,\start_v_reg[6]_rep__13_n_0 ,\start_v_reg[5]_rep__13_n_0 ,\start_v_reg[4]_rep__13_n_0 ,\start_v_reg[3]_rep__13_n_0 ,\start_v_reg[2]_rep__13_n_0 ,\start_v_reg[1]_rep__13_n_0 ,\start_v_reg[0]_rep__13_n_0 }),
        .RAM_reg_0_13_1({\end_v_reg[14]_rep__12_n_0 ,\end_v_reg[13]_rep__12_n_0 ,\end_v_reg[12]_rep__12_n_0 ,\end_v_reg[11]_rep__12_n_0 ,\end_v_reg[10]_rep__12_n_0 ,\end_v_reg[9]_rep__12_n_0 ,\end_v_reg[8]_rep__12_n_0 ,\end_v_reg[7]_rep__12_n_0 ,\end_v_reg[6]_rep__12_n_0 ,\end_v_reg[5]_rep__12_n_0 ,\end_v_reg[4]_rep__12_n_0 ,\end_v_reg[3]_rep__12_n_0 ,\end_v_reg[2]_rep__12_n_0 ,\end_v_reg[1]_rep__12_n_0 ,\end_v_reg[0]_rep__12_n_0 }),
        .RAM_reg_0_14_0({\start_v_reg[14]_rep__13_n_0 ,\start_v_reg[13]_rep__14_n_0 ,\start_v_reg[12]_rep__14_n_0 ,\start_v_reg[11]_rep__14_n_0 ,\start_v_reg[10]_rep__14_n_0 ,\start_v_reg[9]_rep__14_n_0 ,\start_v_reg[8]_rep__14_n_0 ,\start_v_reg[7]_rep__14_n_0 ,\start_v_reg[6]_rep__14_n_0 ,\start_v_reg[5]_rep__14_n_0 ,\start_v_reg[4]_rep__14_n_0 ,\start_v_reg[3]_rep__14_n_0 ,\start_v_reg[2]_rep__14_n_0 ,\start_v_reg[1]_rep__14_n_0 ,\start_v_reg[0]_rep__14_n_0 }),
        .RAM_reg_0_14_1({\end_v_reg[14]_rep__13_n_0 ,\end_v_reg[13]_rep__13_n_0 ,\end_v_reg[12]_rep__13_n_0 ,\end_v_reg[11]_rep__13_n_0 ,\end_v_reg[10]_rep__13_n_0 ,\end_v_reg[9]_rep__13_n_0 ,\end_v_reg[8]_rep__13_n_0 ,\end_v_reg[7]_rep__13_n_0 ,\end_v_reg[6]_rep__13_n_0 ,\end_v_reg[5]_rep__13_n_0 ,\end_v_reg[4]_rep__13_n_0 ,\end_v_reg[3]_rep__13_n_0 ,\end_v_reg[2]_rep__13_n_0 ,\end_v_reg[1]_rep__13_n_0 ,\end_v_reg[0]_rep__13_n_0 }),
        .RAM_reg_0_15_0({\start_v_reg[14]_rep__14_n_0 ,\start_v_reg[13]_rep__15_n_0 ,\start_v_reg[12]_rep__15_n_0 ,\start_v_reg[11]_rep__15_n_0 ,\start_v_reg[10]_rep__15_n_0 ,\start_v_reg[9]_rep__15_n_0 ,\start_v_reg[8]_rep__15_n_0 ,\start_v_reg[7]_rep__15_n_0 ,\start_v_reg[6]_rep__15_n_0 ,\start_v_reg[5]_rep__15_n_0 ,\start_v_reg[4]_rep__15_n_0 ,\start_v_reg[3]_rep__15_n_0 ,\start_v_reg[2]_rep__15_n_0 ,\start_v_reg[1]_rep__15_n_0 ,\start_v_reg[0]_rep__15_n_0 }),
        .RAM_reg_0_15_1({\end_v_reg[14]_rep__14_n_0 ,\end_v_reg[13]_rep__14_n_0 ,\end_v_reg[12]_rep__14_n_0 ,\end_v_reg[11]_rep__14_n_0 ,\end_v_reg[10]_rep__14_n_0 ,\end_v_reg[9]_rep__14_n_0 ,\end_v_reg[8]_rep__14_n_0 ,\end_v_reg[7]_rep__14_n_0 ,\end_v_reg[6]_rep__14_n_0 ,\end_v_reg[5]_rep__14_n_0 ,\end_v_reg[4]_rep__14_n_0 ,\end_v_reg[3]_rep__14_n_0 ,\end_v_reg[2]_rep__14_n_0 ,\end_v_reg[1]_rep__14_n_0 ,\end_v_reg[0]_rep__14_n_0 }),
        .RAM_reg_0_16_0({\start_v_reg[14]_rep__15_n_0 ,\start_v_reg[13]_rep__16_n_0 ,\start_v_reg[12]_rep__16_n_0 ,\start_v_reg[11]_rep__16_n_0 ,\start_v_reg[10]_rep__16_n_0 ,\start_v_reg[9]_rep__16_n_0 ,\start_v_reg[8]_rep__16_n_0 ,\start_v_reg[7]_rep__16_n_0 ,\start_v_reg[6]_rep__16_n_0 ,\start_v_reg[5]_rep__16_n_0 ,\start_v_reg[4]_rep__16_n_0 ,\start_v_reg[3]_rep__16_n_0 ,\start_v_reg[2]_rep__16_n_0 ,\start_v_reg[1]_rep__16_n_0 ,\start_v_reg[0]_rep__16_n_0 }),
        .RAM_reg_0_16_1({\end_v_reg[14]_rep__15_n_0 ,\end_v_reg[13]_rep__15_n_0 ,\end_v_reg[12]_rep__15_n_0 ,\end_v_reg[11]_rep__15_n_0 ,\end_v_reg[10]_rep__15_n_0 ,\end_v_reg[9]_rep__15_n_0 ,\end_v_reg[8]_rep__15_n_0 ,\end_v_reg[7]_rep__15_n_0 ,\end_v_reg[6]_rep__15_n_0 ,\end_v_reg[5]_rep__15_n_0 ,\end_v_reg[4]_rep__15_n_0 ,\end_v_reg[3]_rep__15_n_0 ,\end_v_reg[2]_rep__15_n_0 ,\end_v_reg[1]_rep__15_n_0 ,\end_v_reg[0]_rep__15_n_0 }),
        .RAM_reg_0_17_0({\start_v_reg[14]_rep__16_n_0 ,\start_v_reg[13]_rep__17_n_0 ,\start_v_reg[12]_rep__17_n_0 ,\start_v_reg[11]_rep__17_n_0 ,\start_v_reg[10]_rep__17_n_0 ,\start_v_reg[9]_rep__17_n_0 ,\start_v_reg[8]_rep__17_n_0 ,\start_v_reg[7]_rep__17_n_0 ,\start_v_reg[6]_rep__17_n_0 ,\start_v_reg[5]_rep__17_n_0 ,\start_v_reg[4]_rep__17_n_0 ,\start_v_reg[3]_rep__17_n_0 ,\start_v_reg[2]_rep__17_n_0 ,\start_v_reg[1]_rep__17_n_0 ,\start_v_reg[0]_rep__17_n_0 }),
        .RAM_reg_0_17_1({\end_v_reg[14]_rep__16_n_0 ,\end_v_reg[13]_rep__16_n_0 ,\end_v_reg[12]_rep__16_n_0 ,\end_v_reg[11]_rep__16_n_0 ,\end_v_reg[10]_rep__16_n_0 ,\end_v_reg[9]_rep__16_n_0 ,\end_v_reg[8]_rep__16_n_0 ,\end_v_reg[7]_rep__16_n_0 ,\end_v_reg[6]_rep__16_n_0 ,\end_v_reg[5]_rep__16_n_0 ,\end_v_reg[4]_rep__16_n_0 ,\end_v_reg[3]_rep__16_n_0 ,\end_v_reg[2]_rep__16_n_0 ,\end_v_reg[1]_rep__16_n_0 ,\end_v_reg[0]_rep__16_n_0 }),
        .RAM_reg_0_18_0({\start_v_reg[14]_rep__17_n_0 ,\start_v_reg[13]_rep__18_n_0 ,\start_v_reg[12]_rep__18_n_0 ,\start_v_reg[11]_rep__18_n_0 ,\start_v_reg[10]_rep__18_n_0 ,\start_v_reg[9]_rep__18_n_0 ,\start_v_reg[8]_rep__18_n_0 ,\start_v_reg[7]_rep__18_n_0 ,\start_v_reg[6]_rep__18_n_0 ,\start_v_reg[5]_rep__18_n_0 ,\start_v_reg[4]_rep__18_n_0 ,\start_v_reg[3]_rep__18_n_0 ,\start_v_reg[2]_rep__18_n_0 ,\start_v_reg[1]_rep__18_n_0 ,\start_v_reg[0]_rep__18_n_0 }),
        .RAM_reg_0_18_1({\end_v_reg[14]_rep__17_n_0 ,\end_v_reg[13]_rep__17_n_0 ,\end_v_reg[12]_rep__17_n_0 ,\end_v_reg[11]_rep__17_n_0 ,\end_v_reg[10]_rep__17_n_0 ,\end_v_reg[9]_rep__17_n_0 ,\end_v_reg[8]_rep__17_n_0 ,\end_v_reg[7]_rep__17_n_0 ,\end_v_reg[6]_rep__17_n_0 ,\end_v_reg[5]_rep__17_n_0 ,\end_v_reg[4]_rep__17_n_0 ,\end_v_reg[3]_rep__17_n_0 ,\end_v_reg[2]_rep__17_n_0 ,\end_v_reg[1]_rep__17_n_0 ,\end_v_reg[0]_rep__17_n_0 }),
        .RAM_reg_0_19_0({\start_v_reg[14]_rep__18_n_0 ,\start_v_reg[13]_rep__19_n_0 ,\start_v_reg[12]_rep__19_n_0 ,\start_v_reg[11]_rep__19_n_0 ,\start_v_reg[10]_rep__19_n_0 ,\start_v_reg[9]_rep__19_n_0 ,\start_v_reg[8]_rep__19_n_0 ,\start_v_reg[7]_rep__19_n_0 ,\start_v_reg[6]_rep__19_n_0 ,\start_v_reg[5]_rep__19_n_0 ,\start_v_reg[4]_rep__19_n_0 ,\start_v_reg[3]_rep__19_n_0 ,\start_v_reg[2]_rep__19_n_0 ,\start_v_reg[1]_rep__19_n_0 ,\start_v_reg[0]_rep__19_n_0 }),
        .RAM_reg_0_19_1({\end_v_reg[14]_rep__18_n_0 ,\end_v_reg[13]_rep__18_n_0 ,\end_v_reg[12]_rep__18_n_0 ,\end_v_reg[11]_rep__18_n_0 ,\end_v_reg[10]_rep__18_n_0 ,\end_v_reg[9]_rep__18_n_0 ,\end_v_reg[8]_rep__18_n_0 ,\end_v_reg[7]_rep__18_n_0 ,\end_v_reg[6]_rep__18_n_0 ,\end_v_reg[5]_rep__18_n_0 ,\end_v_reg[4]_rep__18_n_0 ,\end_v_reg[3]_rep__18_n_0 ,\end_v_reg[2]_rep__18_n_0 ,\end_v_reg[1]_rep__18_n_0 ,\end_v_reg[0]_rep__18_n_0 }),
        .RAM_reg_0_1_0({\start_v_reg[14]_rep__0_n_0 ,\start_v_reg[13]_rep__1_n_0 ,\start_v_reg[12]_rep__1_n_0 ,\start_v_reg[11]_rep__1_n_0 ,\start_v_reg[10]_rep__1_n_0 ,\start_v_reg[9]_rep__1_n_0 ,\start_v_reg[8]_rep__1_n_0 ,\start_v_reg[7]_rep__1_n_0 ,\start_v_reg[6]_rep__1_n_0 ,\start_v_reg[5]_rep__1_n_0 ,\start_v_reg[4]_rep__1_n_0 ,\start_v_reg[3]_rep__1_n_0 ,\start_v_reg[2]_rep__1_n_0 ,\start_v_reg[1]_rep__1_n_0 ,\start_v_reg[0]_rep__1_n_0 }),
        .RAM_reg_0_1_1({\end_v_reg[14]_rep__0_n_0 ,\end_v_reg[13]_rep__0_n_0 ,\end_v_reg[12]_rep__0_n_0 ,\end_v_reg[11]_rep__0_n_0 ,\end_v_reg[10]_rep__0_n_0 ,\end_v_reg[9]_rep__0_n_0 ,\end_v_reg[8]_rep__0_n_0 ,\end_v_reg[7]_rep__0_n_0 ,\end_v_reg[6]_rep__0_n_0 ,\end_v_reg[5]_rep__0_n_0 ,\end_v_reg[4]_rep__0_n_0 ,\end_v_reg[3]_rep__0_n_0 ,\end_v_reg[2]_rep__0_n_0 ,\end_v_reg[1]_rep__0_n_0 ,\end_v_reg[0]_rep__0_n_0 }),
        .RAM_reg_0_20_0({\start_v_reg[14]_rep__19_n_0 ,\start_v_reg[13]_rep__20_n_0 ,\start_v_reg[12]_rep__20_n_0 ,\start_v_reg[11]_rep__20_n_0 ,\start_v_reg[10]_rep__20_n_0 ,\start_v_reg[9]_rep__20_n_0 ,\start_v_reg[8]_rep__20_n_0 ,\start_v_reg[7]_rep__20_n_0 ,\start_v_reg[6]_rep__20_n_0 ,\start_v_reg[5]_rep__20_n_0 ,\start_v_reg[4]_rep__20_n_0 ,\start_v_reg[3]_rep__20_n_0 ,\start_v_reg[2]_rep__20_n_0 ,\start_v_reg[1]_rep__20_n_0 ,\start_v_reg[0]_rep__20_n_0 }),
        .RAM_reg_0_20_1({\end_v_reg[14]_rep__19_n_0 ,\end_v_reg[13]_rep__19_n_0 ,\end_v_reg[12]_rep__19_n_0 ,\end_v_reg[11]_rep__19_n_0 ,\end_v_reg[10]_rep__19_n_0 ,\end_v_reg[9]_rep__19_n_0 ,\end_v_reg[8]_rep__19_n_0 ,\end_v_reg[7]_rep__19_n_0 ,\end_v_reg[6]_rep__19_n_0 ,\end_v_reg[5]_rep__19_n_0 ,\end_v_reg[4]_rep__19_n_0 ,\end_v_reg[3]_rep__19_n_0 ,\end_v_reg[2]_rep__19_n_0 ,\end_v_reg[1]_rep__19_n_0 ,\end_v_reg[0]_rep__19_n_0 }),
        .RAM_reg_0_21_0({\start_v_reg[14]_rep__20_n_0 ,\start_v_reg[13]_rep__21_n_0 ,\start_v_reg[12]_rep__21_n_0 ,\start_v_reg[11]_rep__21_n_0 ,\start_v_reg[10]_rep__21_n_0 ,\start_v_reg[9]_rep__21_n_0 ,\start_v_reg[8]_rep__21_n_0 ,\start_v_reg[7]_rep__21_n_0 ,\start_v_reg[6]_rep__21_n_0 ,\start_v_reg[5]_rep__21_n_0 ,\start_v_reg[4]_rep__21_n_0 ,\start_v_reg[3]_rep__21_n_0 ,\start_v_reg[2]_rep__21_n_0 ,\start_v_reg[1]_rep__21_n_0 ,\start_v_reg[0]_rep__21_n_0 }),
        .RAM_reg_0_21_1({\end_v_reg[14]_rep__20_n_0 ,\end_v_reg[13]_rep__20_n_0 ,\end_v_reg[12]_rep__20_n_0 ,\end_v_reg[11]_rep__20_n_0 ,\end_v_reg[10]_rep__20_n_0 ,\end_v_reg[9]_rep__20_n_0 ,\end_v_reg[8]_rep__20_n_0 ,\end_v_reg[7]_rep__20_n_0 ,\end_v_reg[6]_rep__20_n_0 ,\end_v_reg[5]_rep__20_n_0 ,\end_v_reg[4]_rep__20_n_0 ,\end_v_reg[3]_rep__20_n_0 ,\end_v_reg[2]_rep__20_n_0 ,\end_v_reg[1]_rep__20_n_0 ,\end_v_reg[0]_rep__20_n_0 }),
        .RAM_reg_0_22_0({\start_v_reg[14]_rep__21_n_0 ,\start_v_reg[13]_rep__22_n_0 ,\start_v_reg[12]_rep__22_n_0 ,\start_v_reg[11]_rep__22_n_0 ,\start_v_reg[10]_rep__22_n_0 ,\start_v_reg[9]_rep__22_n_0 ,\start_v_reg[8]_rep__22_n_0 ,\start_v_reg[7]_rep__22_n_0 ,\start_v_reg[6]_rep__22_n_0 ,\start_v_reg[5]_rep__22_n_0 ,\start_v_reg[4]_rep__22_n_0 ,\start_v_reg[3]_rep__22_n_0 ,\start_v_reg[2]_rep__22_n_0 ,\start_v_reg[1]_rep__22_n_0 ,\start_v_reg[0]_rep__22_n_0 }),
        .RAM_reg_0_22_1({\end_v_reg[14]_rep__21_n_0 ,\end_v_reg[13]_rep__21_n_0 ,\end_v_reg[12]_rep__21_n_0 ,\end_v_reg[11]_rep__21_n_0 ,\end_v_reg[10]_rep__21_n_0 ,\end_v_reg[9]_rep__21_n_0 ,\end_v_reg[8]_rep__21_n_0 ,\end_v_reg[7]_rep__21_n_0 ,\end_v_reg[6]_rep__21_n_0 ,\end_v_reg[5]_rep__21_n_0 ,\end_v_reg[4]_rep__21_n_0 ,\end_v_reg[3]_rep__21_n_0 ,\end_v_reg[2]_rep__21_n_0 ,\end_v_reg[1]_rep__21_n_0 ,\end_v_reg[0]_rep__21_n_0 }),
        .RAM_reg_0_2_0({\start_v_reg[14]_rep__1_n_0 ,\start_v_reg[13]_rep__2_n_0 ,\start_v_reg[12]_rep__2_n_0 ,\start_v_reg[11]_rep__2_n_0 ,\start_v_reg[10]_rep__2_n_0 ,\start_v_reg[9]_rep__2_n_0 ,\start_v_reg[8]_rep__2_n_0 ,\start_v_reg[7]_rep__2_n_0 ,\start_v_reg[6]_rep__2_n_0 ,\start_v_reg[5]_rep__2_n_0 ,\start_v_reg[4]_rep__2_n_0 ,\start_v_reg[3]_rep__2_n_0 ,\start_v_reg[2]_rep__2_n_0 ,\start_v_reg[1]_rep__2_n_0 ,\start_v_reg[0]_rep__2_n_0 }),
        .RAM_reg_0_2_1({\end_v_reg[14]_rep__1_n_0 ,\end_v_reg[13]_rep__1_n_0 ,\end_v_reg[12]_rep__1_n_0 ,\end_v_reg[11]_rep__1_n_0 ,\end_v_reg[10]_rep__1_n_0 ,\end_v_reg[9]_rep__1_n_0 ,\end_v_reg[8]_rep__1_n_0 ,\end_v_reg[7]_rep__1_n_0 ,\end_v_reg[6]_rep__1_n_0 ,\end_v_reg[5]_rep__1_n_0 ,\end_v_reg[4]_rep__1_n_0 ,\end_v_reg[3]_rep__1_n_0 ,\end_v_reg[2]_rep__1_n_0 ,\end_v_reg[1]_rep__1_n_0 ,\end_v_reg[0]_rep__1_n_0 }),
        .RAM_reg_0_3_0({\start_v_reg[14]_rep__2_n_0 ,\start_v_reg[13]_rep__3_n_0 ,\start_v_reg[12]_rep__3_n_0 ,\start_v_reg[11]_rep__3_n_0 ,\start_v_reg[10]_rep__3_n_0 ,\start_v_reg[9]_rep__3_n_0 ,\start_v_reg[8]_rep__3_n_0 ,\start_v_reg[7]_rep__3_n_0 ,\start_v_reg[6]_rep__3_n_0 ,\start_v_reg[5]_rep__3_n_0 ,\start_v_reg[4]_rep__3_n_0 ,\start_v_reg[3]_rep__3_n_0 ,\start_v_reg[2]_rep__3_n_0 ,\start_v_reg[1]_rep__3_n_0 ,\start_v_reg[0]_rep__3_n_0 }),
        .RAM_reg_0_3_1({\end_v_reg[14]_rep__2_n_0 ,\end_v_reg[13]_rep__2_n_0 ,\end_v_reg[12]_rep__2_n_0 ,\end_v_reg[11]_rep__2_n_0 ,\end_v_reg[10]_rep__2_n_0 ,\end_v_reg[9]_rep__2_n_0 ,\end_v_reg[8]_rep__2_n_0 ,\end_v_reg[7]_rep__2_n_0 ,\end_v_reg[6]_rep__2_n_0 ,\end_v_reg[5]_rep__2_n_0 ,\end_v_reg[4]_rep__2_n_0 ,\end_v_reg[3]_rep__2_n_0 ,\end_v_reg[2]_rep__2_n_0 ,\end_v_reg[1]_rep__2_n_0 ,\end_v_reg[0]_rep__2_n_0 }),
        .RAM_reg_0_4_0({\start_v_reg[14]_rep__3_n_0 ,\start_v_reg[13]_rep__4_n_0 ,\start_v_reg[12]_rep__4_n_0 ,\start_v_reg[11]_rep__4_n_0 ,\start_v_reg[10]_rep__4_n_0 ,\start_v_reg[9]_rep__4_n_0 ,\start_v_reg[8]_rep__4_n_0 ,\start_v_reg[7]_rep__4_n_0 ,\start_v_reg[6]_rep__4_n_0 ,\start_v_reg[5]_rep__4_n_0 ,\start_v_reg[4]_rep__4_n_0 ,\start_v_reg[3]_rep__4_n_0 ,\start_v_reg[2]_rep__4_n_0 ,\start_v_reg[1]_rep__4_n_0 ,\start_v_reg[0]_rep__4_n_0 }),
        .RAM_reg_0_4_1({\end_v_reg[14]_rep__3_n_0 ,\end_v_reg[13]_rep__3_n_0 ,\end_v_reg[12]_rep__3_n_0 ,\end_v_reg[11]_rep__3_n_0 ,\end_v_reg[10]_rep__3_n_0 ,\end_v_reg[9]_rep__3_n_0 ,\end_v_reg[8]_rep__3_n_0 ,\end_v_reg[7]_rep__3_n_0 ,\end_v_reg[6]_rep__3_n_0 ,\end_v_reg[5]_rep__3_n_0 ,\end_v_reg[4]_rep__3_n_0 ,\end_v_reg[3]_rep__3_n_0 ,\end_v_reg[2]_rep__3_n_0 ,\end_v_reg[1]_rep__3_n_0 ,\end_v_reg[0]_rep__3_n_0 }),
        .RAM_reg_0_5_0({\start_v_reg[14]_rep__4_n_0 ,\start_v_reg[13]_rep__5_n_0 ,\start_v_reg[12]_rep__5_n_0 ,\start_v_reg[11]_rep__5_n_0 ,\start_v_reg[10]_rep__5_n_0 ,\start_v_reg[9]_rep__5_n_0 ,\start_v_reg[8]_rep__5_n_0 ,\start_v_reg[7]_rep__5_n_0 ,\start_v_reg[6]_rep__5_n_0 ,\start_v_reg[5]_rep__5_n_0 ,\start_v_reg[4]_rep__5_n_0 ,\start_v_reg[3]_rep__5_n_0 ,\start_v_reg[2]_rep__5_n_0 ,\start_v_reg[1]_rep__5_n_0 ,\start_v_reg[0]_rep__5_n_0 }),
        .RAM_reg_0_5_1({\end_v_reg[14]_rep__4_n_0 ,\end_v_reg[13]_rep__4_n_0 ,\end_v_reg[12]_rep__4_n_0 ,\end_v_reg[11]_rep__4_n_0 ,\end_v_reg[10]_rep__4_n_0 ,\end_v_reg[9]_rep__4_n_0 ,\end_v_reg[8]_rep__4_n_0 ,\end_v_reg[7]_rep__4_n_0 ,\end_v_reg[6]_rep__4_n_0 ,\end_v_reg[5]_rep__4_n_0 ,\end_v_reg[4]_rep__4_n_0 ,\end_v_reg[3]_rep__4_n_0 ,\end_v_reg[2]_rep__4_n_0 ,\end_v_reg[1]_rep__4_n_0 ,\end_v_reg[0]_rep__4_n_0 }),
        .RAM_reg_0_6_0({\start_v_reg[14]_rep__5_n_0 ,\start_v_reg[13]_rep__6_n_0 ,\start_v_reg[12]_rep__6_n_0 ,\start_v_reg[11]_rep__6_n_0 ,\start_v_reg[10]_rep__6_n_0 ,\start_v_reg[9]_rep__6_n_0 ,\start_v_reg[8]_rep__6_n_0 ,\start_v_reg[7]_rep__6_n_0 ,\start_v_reg[6]_rep__6_n_0 ,\start_v_reg[5]_rep__6_n_0 ,\start_v_reg[4]_rep__6_n_0 ,\start_v_reg[3]_rep__6_n_0 ,\start_v_reg[2]_rep__6_n_0 ,\start_v_reg[1]_rep__6_n_0 ,\start_v_reg[0]_rep__6_n_0 }),
        .RAM_reg_0_6_1({\end_v_reg[14]_rep__5_n_0 ,\end_v_reg[13]_rep__5_n_0 ,\end_v_reg[12]_rep__5_n_0 ,\end_v_reg[11]_rep__5_n_0 ,\end_v_reg[10]_rep__5_n_0 ,\end_v_reg[9]_rep__5_n_0 ,\end_v_reg[8]_rep__5_n_0 ,\end_v_reg[7]_rep__5_n_0 ,\end_v_reg[6]_rep__5_n_0 ,\end_v_reg[5]_rep__5_n_0 ,\end_v_reg[4]_rep__5_n_0 ,\end_v_reg[3]_rep__5_n_0 ,\end_v_reg[2]_rep__5_n_0 ,\end_v_reg[1]_rep__5_n_0 ,\end_v_reg[0]_rep__5_n_0 }),
        .RAM_reg_0_7_0({\start_v_reg[14]_rep__6_n_0 ,\start_v_reg[13]_rep__7_n_0 ,\start_v_reg[12]_rep__7_n_0 ,\start_v_reg[11]_rep__7_n_0 ,\start_v_reg[10]_rep__7_n_0 ,\start_v_reg[9]_rep__7_n_0 ,\start_v_reg[8]_rep__7_n_0 ,\start_v_reg[7]_rep__7_n_0 ,\start_v_reg[6]_rep__7_n_0 ,\start_v_reg[5]_rep__7_n_0 ,\start_v_reg[4]_rep__7_n_0 ,\start_v_reg[3]_rep__7_n_0 ,\start_v_reg[2]_rep__7_n_0 ,\start_v_reg[1]_rep__7_n_0 ,\start_v_reg[0]_rep__7_n_0 }),
        .RAM_reg_0_7_1({\end_v_reg[14]_rep__6_n_0 ,\end_v_reg[13]_rep__6_n_0 ,\end_v_reg[12]_rep__6_n_0 ,\end_v_reg[11]_rep__6_n_0 ,\end_v_reg[10]_rep__6_n_0 ,\end_v_reg[9]_rep__6_n_0 ,\end_v_reg[8]_rep__6_n_0 ,\end_v_reg[7]_rep__6_n_0 ,\end_v_reg[6]_rep__6_n_0 ,\end_v_reg[5]_rep__6_n_0 ,\end_v_reg[4]_rep__6_n_0 ,\end_v_reg[3]_rep__6_n_0 ,\end_v_reg[2]_rep__6_n_0 ,\end_v_reg[1]_rep__6_n_0 ,\end_v_reg[0]_rep__6_n_0 }),
        .RAM_reg_0_8_0({\start_v_reg[14]_rep__7_n_0 ,\start_v_reg[13]_rep__8_n_0 ,\start_v_reg[12]_rep__8_n_0 ,\start_v_reg[11]_rep__8_n_0 ,\start_v_reg[10]_rep__8_n_0 ,\start_v_reg[9]_rep__8_n_0 ,\start_v_reg[8]_rep__8_n_0 ,\start_v_reg[7]_rep__8_n_0 ,\start_v_reg[6]_rep__8_n_0 ,\start_v_reg[5]_rep__8_n_0 ,\start_v_reg[4]_rep__8_n_0 ,\start_v_reg[3]_rep__8_n_0 ,\start_v_reg[2]_rep__8_n_0 ,\start_v_reg[1]_rep__8_n_0 ,\start_v_reg[0]_rep__8_n_0 }),
        .RAM_reg_0_8_1({\end_v_reg[14]_rep__7_n_0 ,\end_v_reg[13]_rep__7_n_0 ,\end_v_reg[12]_rep__7_n_0 ,\end_v_reg[11]_rep__7_n_0 ,\end_v_reg[10]_rep__7_n_0 ,\end_v_reg[9]_rep__7_n_0 ,\end_v_reg[8]_rep__7_n_0 ,\end_v_reg[7]_rep__7_n_0 ,\end_v_reg[6]_rep__7_n_0 ,\end_v_reg[5]_rep__7_n_0 ,\end_v_reg[4]_rep__7_n_0 ,\end_v_reg[3]_rep__7_n_0 ,\end_v_reg[2]_rep__7_n_0 ,\end_v_reg[1]_rep__7_n_0 ,\end_v_reg[0]_rep__7_n_0 }),
        .RAM_reg_0_9_0({\start_v_reg[14]_rep__8_n_0 ,\start_v_reg[13]_rep__9_n_0 ,\start_v_reg[12]_rep__9_n_0 ,\start_v_reg[11]_rep__9_n_0 ,\start_v_reg[10]_rep__9_n_0 ,\start_v_reg[9]_rep__9_n_0 ,\start_v_reg[8]_rep__9_n_0 ,\start_v_reg[7]_rep__9_n_0 ,\start_v_reg[6]_rep__9_n_0 ,\start_v_reg[5]_rep__9_n_0 ,\start_v_reg[4]_rep__9_n_0 ,\start_v_reg[3]_rep__9_n_0 ,\start_v_reg[2]_rep__9_n_0 ,\start_v_reg[1]_rep__9_n_0 ,\start_v_reg[0]_rep__9_n_0 }),
        .RAM_reg_0_9_1({\end_v_reg[14]_rep__8_n_0 ,\end_v_reg[13]_rep__8_n_0 ,\end_v_reg[12]_rep__8_n_0 ,\end_v_reg[11]_rep__8_n_0 ,\end_v_reg[10]_rep__8_n_0 ,\end_v_reg[9]_rep__8_n_0 ,\end_v_reg[8]_rep__8_n_0 ,\end_v_reg[7]_rep__8_n_0 ,\end_v_reg[6]_rep__8_n_0 ,\end_v_reg[5]_rep__8_n_0 ,\end_v_reg[4]_rep__8_n_0 ,\end_v_reg[3]_rep__8_n_0 ,\end_v_reg[2]_rep__8_n_0 ,\end_v_reg[1]_rep__8_n_0 ,\end_v_reg[0]_rep__8_n_0 }),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .addr_a({\start_v_reg[14]_rep__22_n_0 ,\start_v_reg[13]_rep__23_n_0 ,\start_v_reg[12]_rep__23_n_0 ,\start_v_reg[11]_rep__23_n_0 ,\start_v_reg[10]_rep__23_n_0 ,\start_v_reg[9]_rep__23_n_0 ,\start_v_reg[8]_rep__23_n_0 ,\start_v_reg[7]_rep__23_n_0 ,\start_v_reg[6]_rep__23_n_0 ,\start_v_reg[5]_rep__23_n_0 ,\start_v_reg[4]_rep__23_n_0 ,\start_v_reg[3]_rep__23_n_0 ,\start_v_reg[2]_rep__23_n_0 ,\start_v_reg[1]_rep__23_n_0 ,\start_v_reg[0]_rep__23_n_0 }),
        .addr_b({\end_v_reg[14]_rep__22_n_0 ,\end_v_reg[13]_rep__22_n_0 ,\end_v_reg[12]_rep__22_n_0 ,\end_v_reg[11]_rep__22_n_0 ,\end_v_reg[10]_rep__22_n_0 ,\end_v_reg[9]_rep__22_n_0 ,\end_v_reg[8]_rep__22_n_0 ,\end_v_reg[7]_rep__22_n_0 ,\end_v_reg[6]_rep__22_n_0 ,\end_v_reg[5]_rep__22_n_0 ,\end_v_reg[4]_rep__22_n_0 ,\end_v_reg[3]_rep__22_n_0 ,\end_v_reg[2]_rep__22_n_0 ,\end_v_reg[1]_rep__22_n_0 ,\end_v_reg[0]_rep__22_n_0 }),
        .dout_b(dout_b),
        .p_0_in_0(p_0_in_0),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wvalid(s00_axi_wvalid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg_d reg_d0
       (.S({reg_d0_n_0,reg_d0_n_1,reg_d0_n_2}),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .\end_v_reg[11]_rep__22 (\start_v_reg[8]_rep_n_0 ),
        .\end_v_reg[11]_rep__22_0 (\start_v_reg[9]_rep_n_0 ),
        .\end_v_reg[11]_rep__22_1 (\start_v_reg[10]_rep_n_0 ),
        .\end_v_reg[11]_rep__22_2 (\start_v_reg[11]_rep_n_0 ),
        .\end_v_reg[14]_rep__22 (\start_v_reg[12]_rep_n_0 ),
        .\end_v_reg[14]_rep__22_0 (\start_v_reg[13]_rep_n_0 ),
        .\end_v_reg[3]_rep__22 (\start_v_reg[0]_rep_n_0 ),
        .\end_v_reg[3]_rep__22_0 (\start_v_reg[1]_rep_n_0 ),
        .\end_v_reg[3]_rep__22_1 (\start_v_reg[2]_rep_n_0 ),
        .\end_v_reg[3]_rep__22_2 (\start_v_reg[3]_rep_n_0 ),
        .\end_v_reg[7]_rep__22 (\start_v_reg[4]_rep_n_0 ),
        .\end_v_reg[7]_rep__22_0 (\start_v_reg[5]_rep_n_0 ),
        .\end_v_reg[7]_rep__22_1 (\start_v_reg[6]_rep_n_0 ),
        .\end_v_reg[7]_rep__22_2 (\start_v_reg[7]_rep_n_0 ),
        .p_0_in_0(p_0_in_0),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_wdata(s00_axi_wdata[14:0]),
        .s00_axi_wvalid(s00_axi_wvalid),
        .start_v_reg(start_v_reg[14]),
        .\start_v_reg[11]_rep ({reg_d0_n_11,reg_d0_n_12,reg_d0_n_13,reg_d0_n_14}),
        .\start_v_reg[3]_rep ({reg_d0_n_3,reg_d0_n_4,reg_d0_n_5,reg_d0_n_6}),
        .\start_v_reg[7]_rep ({reg_d0_n_7,reg_d0_n_8,reg_d0_n_9,reg_d0_n_10}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    \start_v[0]_i_1 
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(en_a));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[0]_i_3 
       (.I0(\start_v_reg[3]_rep_n_0 ),
        .O(\start_v[0]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[0]_i_4 
       (.I0(\start_v_reg[2]_rep_n_0 ),
        .O(\start_v[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[0]_i_5 
       (.I0(\start_v_reg[1]_rep_n_0 ),
        .O(\start_v[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[0]_i_6 
       (.I0(\start_v_reg[0]_rep_n_0 ),
        .O(\start_v[0]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[12]_i_2 
       (.I0(start_v_reg[14]),
        .O(\start_v[12]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[12]_i_3 
       (.I0(\start_v_reg[13]_rep_n_0 ),
        .O(\start_v[12]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[12]_i_4 
       (.I0(\start_v_reg[12]_rep_n_0 ),
        .O(\start_v[12]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[4]_i_2 
       (.I0(\start_v_reg[7]_rep_n_0 ),
        .O(\start_v[4]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[4]_i_3 
       (.I0(\start_v_reg[6]_rep_n_0 ),
        .O(\start_v[4]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[4]_i_4 
       (.I0(\start_v_reg[5]_rep_n_0 ),
        .O(\start_v[4]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[4]_i_5 
       (.I0(\start_v_reg[4]_rep_n_0 ),
        .O(\start_v[4]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[8]_i_2 
       (.I0(\start_v_reg[11]_rep_n_0 ),
        .O(\start_v[8]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[8]_i_3 
       (.I0(\start_v_reg[10]_rep_n_0 ),
        .O(\start_v[8]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[8]_i_4 
       (.I0(\start_v_reg[9]_rep_n_0 ),
        .O(\start_v[8]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \start_v[8]_i_5 
       (.I0(\start_v_reg[8]_rep_n_0 ),
        .O(\start_v[8]_i_5_n_0 ));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(start_v_reg[0]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \start_v_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\start_v_reg[0]_i_2_n_0 ,\start_v_reg[0]_i_2_n_1 ,\start_v_reg[0]_i_2_n_2 ,\start_v_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O({\start_v_reg[0]_i_2_n_4 ,\start_v_reg[0]_i_2_n_5 ,\start_v_reg[0]_i_2_n_6 ,\start_v_reg[0]_i_2_n_7 }),
        .S({\start_v[0]_i_3_n_0 ,\start_v[0]_i_4_n_0 ,\start_v[0]_i_5_n_0 ,\start_v[0]_i_6_n_0 }));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__23 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__23_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[0]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[0]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_7 ),
        .Q(\start_v_reg[0]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(start_v_reg[10]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__23 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__23_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[10]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[10]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_5 ),
        .Q(\start_v_reg[10]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(start_v_reg[11]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__23 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__23_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[11]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[11]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_4 ),
        .Q(\start_v_reg[11]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(start_v_reg[12]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \start_v_reg[12]_i_1 
       (.CI(\start_v_reg[8]_i_1_n_0 ),
        .CO({\NLW_start_v_reg[12]_i_1_CO_UNCONNECTED [3:2],\start_v_reg[12]_i_1_n_2 ,\start_v_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b1}),
        .O({\NLW_start_v_reg[12]_i_1_O_UNCONNECTED [3],\start_v_reg[12]_i_1_n_5 ,\start_v_reg[12]_i_1_n_6 ,\start_v_reg[12]_i_1_n_7 }),
        .S({1'b0,\start_v[12]_i_2_n_0 ,\start_v[12]_i_3_n_0 ,\start_v[12]_i_4_n_0 }));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__23 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__23_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[12]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[12]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_7 ),
        .Q(\start_v_reg[12]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(start_v_reg[13]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__23 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__23_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[13]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[13]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_6 ),
        .Q(\start_v_reg[13]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(start_v_reg[14]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[14]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[14]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[12]_i_1_n_5 ),
        .Q(\start_v_reg[14]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(start_v_reg[1]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__23 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__23_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[1]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[1]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_6 ),
        .Q(\start_v_reg[1]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(start_v_reg[2]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__23 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__23_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[2]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[2]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_5 ),
        .Q(\start_v_reg[2]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(start_v_reg[3]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__23 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__23_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[3]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[3]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[0]_i_2_n_4 ),
        .Q(\start_v_reg[3]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(start_v_reg[4]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \start_v_reg[4]_i_1 
       (.CI(\start_v_reg[0]_i_2_n_0 ),
        .CO({\start_v_reg[4]_i_1_n_0 ,\start_v_reg[4]_i_1_n_1 ,\start_v_reg[4]_i_1_n_2 ,\start_v_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O({\start_v_reg[4]_i_1_n_4 ,\start_v_reg[4]_i_1_n_5 ,\start_v_reg[4]_i_1_n_6 ,\start_v_reg[4]_i_1_n_7 }),
        .S({\start_v[4]_i_2_n_0 ,\start_v[4]_i_3_n_0 ,\start_v[4]_i_4_n_0 ,\start_v[4]_i_5_n_0 }));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__23 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__23_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[4]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[4]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_7 ),
        .Q(\start_v_reg[4]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(start_v_reg[5]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__23 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__23_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[5]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[5]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_6 ),
        .Q(\start_v_reg[5]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(start_v_reg[6]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__23 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__23_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[6]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[6]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_5 ),
        .Q(\start_v_reg[6]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(start_v_reg[7]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__23 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__23_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[7]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[7]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[4]_i_1_n_4 ),
        .Q(\start_v_reg[7]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(start_v_reg[8]),
        .R(1'b0));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \start_v_reg[8]_i_1 
       (.CI(\start_v_reg[4]_i_1_n_0 ),
        .CO({\start_v_reg[8]_i_1_n_0 ,\start_v_reg[8]_i_1_n_1 ,\start_v_reg[8]_i_1_n_2 ,\start_v_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O({\start_v_reg[8]_i_1_n_4 ,\start_v_reg[8]_i_1_n_5 ,\start_v_reg[8]_i_1_n_6 ,\start_v_reg[8]_i_1_n_7 }),
        .S({\start_v[8]_i_2_n_0 ,\start_v[8]_i_3_n_0 ,\start_v[8]_i_4_n_0 ,\start_v[8]_i_5_n_0 }));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__23 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__23_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[8]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[8]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_7 ),
        .Q(\start_v_reg[8]_rep__9_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9] 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(start_v_reg[9]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__0 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__0_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__1 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__1_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__10 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__10_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__11 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__11_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__12 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__12_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__13 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__13_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__14 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__14_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__15 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__15_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__16 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__16_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__17 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__17_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__18 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__18_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__19 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__19_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__2 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__2_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__20 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__20_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__21 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__21_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__22 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__22_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__23 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__23_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__3 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__3_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__4 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__4_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__5 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__5_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__6 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__6_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__7 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__7_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__8 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__8_n_0 ),
        .R(1'b0));
  (* ORIG_CELL_NAME = "start_v_reg[9]" *) 
  FDRE #(
    .INIT(1'b0)) 
    \start_v_reg[9]_rep__9 
       (.C(s00_axi_aclk),
        .CE(en_a),
        .D(\start_v_reg[8]_i_1_n_6 ),
        .Q(\start_v_reg[9]_rep__9_n_0 ),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dual_port_RAM
   (dout_b,
    p_0_in_0,
    s00_axi_wvalid,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    s00_axi_awvalid,
    s00_axi_aclk,
    ADDRARDADDR,
    ADDRBWRADDR,
    s00_axi_wdata,
    RAM_reg_0_1_0,
    RAM_reg_0_1_1,
    RAM_reg_0_2_0,
    RAM_reg_0_2_1,
    RAM_reg_0_3_0,
    RAM_reg_0_3_1,
    RAM_reg_0_4_0,
    RAM_reg_0_4_1,
    RAM_reg_0_5_0,
    RAM_reg_0_5_1,
    RAM_reg_0_6_0,
    RAM_reg_0_6_1,
    RAM_reg_0_7_0,
    RAM_reg_0_7_1,
    RAM_reg_0_8_0,
    RAM_reg_0_8_1,
    RAM_reg_0_9_0,
    RAM_reg_0_9_1,
    RAM_reg_0_10_0,
    RAM_reg_0_10_1,
    RAM_reg_0_11_0,
    RAM_reg_0_11_1,
    RAM_reg_0_12_0,
    RAM_reg_0_12_1,
    RAM_reg_0_13_0,
    RAM_reg_0_13_1,
    RAM_reg_0_14_0,
    RAM_reg_0_14_1,
    RAM_reg_0_15_0,
    RAM_reg_0_15_1,
    RAM_reg_0_16_0,
    RAM_reg_0_16_1,
    RAM_reg_0_17_0,
    RAM_reg_0_17_1,
    RAM_reg_0_18_0,
    RAM_reg_0_18_1,
    RAM_reg_0_19_0,
    RAM_reg_0_19_1,
    RAM_reg_0_20_0,
    RAM_reg_0_20_1,
    RAM_reg_0_21_0,
    RAM_reg_0_21_1,
    RAM_reg_0_22_0,
    RAM_reg_0_22_1,
    addr_a,
    addr_b);
  output [23:0]dout_b;
  input [1:0]p_0_in_0;
  input s00_axi_wvalid;
  input S_AXI_WREADY;
  input S_AXI_AWREADY;
  input s00_axi_awvalid;
  input s00_axi_aclk;
  input [14:0]ADDRARDADDR;
  input [14:0]ADDRBWRADDR;
  input [23:0]s00_axi_wdata;
  input [14:0]RAM_reg_0_1_0;
  input [14:0]RAM_reg_0_1_1;
  input [14:0]RAM_reg_0_2_0;
  input [14:0]RAM_reg_0_2_1;
  input [14:0]RAM_reg_0_3_0;
  input [14:0]RAM_reg_0_3_1;
  input [14:0]RAM_reg_0_4_0;
  input [14:0]RAM_reg_0_4_1;
  input [14:0]RAM_reg_0_5_0;
  input [14:0]RAM_reg_0_5_1;
  input [14:0]RAM_reg_0_6_0;
  input [14:0]RAM_reg_0_6_1;
  input [14:0]RAM_reg_0_7_0;
  input [14:0]RAM_reg_0_7_1;
  input [14:0]RAM_reg_0_8_0;
  input [14:0]RAM_reg_0_8_1;
  input [14:0]RAM_reg_0_9_0;
  input [14:0]RAM_reg_0_9_1;
  input [14:0]RAM_reg_0_10_0;
  input [14:0]RAM_reg_0_10_1;
  input [14:0]RAM_reg_0_11_0;
  input [14:0]RAM_reg_0_11_1;
  input [14:0]RAM_reg_0_12_0;
  input [14:0]RAM_reg_0_12_1;
  input [14:0]RAM_reg_0_13_0;
  input [14:0]RAM_reg_0_13_1;
  input [14:0]RAM_reg_0_14_0;
  input [14:0]RAM_reg_0_14_1;
  input [14:0]RAM_reg_0_15_0;
  input [14:0]RAM_reg_0_15_1;
  input [14:0]RAM_reg_0_16_0;
  input [14:0]RAM_reg_0_16_1;
  input [14:0]RAM_reg_0_17_0;
  input [14:0]RAM_reg_0_17_1;
  input [14:0]RAM_reg_0_18_0;
  input [14:0]RAM_reg_0_18_1;
  input [14:0]RAM_reg_0_19_0;
  input [14:0]RAM_reg_0_19_1;
  input [14:0]RAM_reg_0_20_0;
  input [14:0]RAM_reg_0_20_1;
  input [14:0]RAM_reg_0_21_0;
  input [14:0]RAM_reg_0_21_1;
  input [14:0]RAM_reg_0_22_0;
  input [14:0]RAM_reg_0_22_1;
  input [14:0]addr_a;
  input [14:0]addr_b;

  wire [14:0]ADDRARDADDR;
  wire [14:0]ADDRBWRADDR;
  wire RAM_reg_0_0_i_1_n_0;
  wire [14:0]RAM_reg_0_10_0;
  wire [14:0]RAM_reg_0_10_1;
  wire RAM_reg_0_10_i_1_n_0;
  wire [14:0]RAM_reg_0_11_0;
  wire [14:0]RAM_reg_0_11_1;
  wire RAM_reg_0_11_i_1_n_0;
  wire [14:0]RAM_reg_0_12_0;
  wire [14:0]RAM_reg_0_12_1;
  wire RAM_reg_0_12_i_1_n_0;
  wire [14:0]RAM_reg_0_13_0;
  wire [14:0]RAM_reg_0_13_1;
  wire RAM_reg_0_13_i_1_n_0;
  wire [14:0]RAM_reg_0_14_0;
  wire [14:0]RAM_reg_0_14_1;
  wire RAM_reg_0_14_i_1_n_0;
  wire [14:0]RAM_reg_0_15_0;
  wire [14:0]RAM_reg_0_15_1;
  wire RAM_reg_0_15_i_1_n_0;
  wire [14:0]RAM_reg_0_16_0;
  wire [14:0]RAM_reg_0_16_1;
  wire RAM_reg_0_16_i_1_n_0;
  wire [14:0]RAM_reg_0_17_0;
  wire [14:0]RAM_reg_0_17_1;
  wire RAM_reg_0_17_i_1_n_0;
  wire [14:0]RAM_reg_0_18_0;
  wire [14:0]RAM_reg_0_18_1;
  wire RAM_reg_0_18_i_1_n_0;
  wire [14:0]RAM_reg_0_19_0;
  wire [14:0]RAM_reg_0_19_1;
  wire RAM_reg_0_19_i_1_n_0;
  wire [14:0]RAM_reg_0_1_0;
  wire [14:0]RAM_reg_0_1_1;
  wire RAM_reg_0_1_i_1_n_0;
  wire [14:0]RAM_reg_0_20_0;
  wire [14:0]RAM_reg_0_20_1;
  wire RAM_reg_0_20_i_1_n_0;
  wire [14:0]RAM_reg_0_21_0;
  wire [14:0]RAM_reg_0_21_1;
  wire RAM_reg_0_21_i_1_n_0;
  wire [14:0]RAM_reg_0_22_0;
  wire [14:0]RAM_reg_0_22_1;
  wire RAM_reg_0_22_i_1_n_0;
  wire RAM_reg_0_23_i_1_n_0;
  wire [14:0]RAM_reg_0_2_0;
  wire [14:0]RAM_reg_0_2_1;
  wire RAM_reg_0_2_i_1_n_0;
  wire [14:0]RAM_reg_0_3_0;
  wire [14:0]RAM_reg_0_3_1;
  wire RAM_reg_0_3_i_1_n_0;
  wire [14:0]RAM_reg_0_4_0;
  wire [14:0]RAM_reg_0_4_1;
  wire RAM_reg_0_4_i_1_n_0;
  wire [14:0]RAM_reg_0_5_0;
  wire [14:0]RAM_reg_0_5_1;
  wire RAM_reg_0_5_i_1_n_0;
  wire [14:0]RAM_reg_0_6_0;
  wire [14:0]RAM_reg_0_6_1;
  wire RAM_reg_0_6_i_1_n_0;
  wire [14:0]RAM_reg_0_7_0;
  wire [14:0]RAM_reg_0_7_1;
  wire RAM_reg_0_7_i_1_n_0;
  wire [14:0]RAM_reg_0_8_0;
  wire [14:0]RAM_reg_0_8_1;
  wire RAM_reg_0_8_i_1_n_0;
  wire [14:0]RAM_reg_0_9_0;
  wire [14:0]RAM_reg_0_9_1;
  wire RAM_reg_0_9_i_1_n_0;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire [14:0]addr_a;
  wire [14:0]addr_b;
  wire [23:0]dout_b;
  wire [1:0]p_0_in_0;
  wire s00_axi_aclk;
  wire s00_axi_awvalid;
  wire [23:0]s00_axi_wdata;
  wire s00_axi_wvalid;
  wire NLW_RAM_reg_0_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_0_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_0_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_0_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_0_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_0_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_1_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_1_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_1_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_1_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_1_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_1_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_1_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_1_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_1_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_1_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_1_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_1_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_10_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_10_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_10_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_10_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_10_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_10_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_10_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_10_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_10_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_10_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_10_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_10_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_11_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_11_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_11_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_11_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_11_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_11_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_11_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_11_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_11_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_11_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_11_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_11_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_12_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_12_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_12_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_12_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_12_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_12_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_12_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_12_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_12_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_12_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_12_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_12_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_13_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_13_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_13_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_13_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_13_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_13_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_13_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_13_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_13_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_13_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_13_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_13_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_14_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_14_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_14_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_14_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_14_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_14_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_14_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_14_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_14_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_14_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_14_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_14_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_15_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_15_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_15_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_15_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_15_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_15_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_15_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_15_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_15_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_15_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_15_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_15_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_16_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_16_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_16_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_16_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_16_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_16_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_16_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_16_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_16_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_16_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_16_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_16_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_17_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_17_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_17_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_17_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_17_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_17_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_17_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_17_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_17_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_17_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_17_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_17_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_18_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_18_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_18_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_18_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_18_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_18_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_18_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_18_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_18_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_18_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_18_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_18_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_19_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_19_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_19_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_19_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_19_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_19_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_19_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_19_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_19_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_19_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_19_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_19_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_2_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_2_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_2_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_2_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_2_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_2_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_2_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_2_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_2_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_2_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_2_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_2_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_20_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_20_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_20_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_20_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_20_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_20_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_20_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_20_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_20_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_20_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_20_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_20_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_21_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_21_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_21_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_21_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_21_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_21_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_21_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_21_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_21_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_21_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_21_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_21_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_22_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_22_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_22_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_22_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_22_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_22_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_22_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_22_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_22_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_22_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_22_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_22_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_23_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_23_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_23_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_23_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_23_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_23_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_23_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_23_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_23_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_23_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_23_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_23_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_3_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_3_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_3_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_3_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_3_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_3_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_3_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_3_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_3_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_3_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_3_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_3_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_4_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_4_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_4_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_4_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_4_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_4_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_4_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_4_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_4_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_4_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_4_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_4_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_5_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_5_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_5_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_5_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_5_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_5_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_5_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_5_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_5_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_5_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_5_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_5_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_6_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_6_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_6_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_6_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_6_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_6_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_6_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_6_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_6_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_6_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_6_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_6_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_7_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_7_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_7_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_7_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_7_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_7_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_7_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_7_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_7_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_7_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_7_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_7_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_8_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_8_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_8_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_8_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_8_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_8_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_8_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_8_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_8_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_8_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_8_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_8_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_0_9_CASCADEOUTA_UNCONNECTED;
  wire NLW_RAM_reg_0_9_CASCADEOUTB_UNCONNECTED;
  wire NLW_RAM_reg_0_9_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_9_INJECTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_9_INJECTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_0_9_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_0_9_DOADO_UNCONNECTED;
  wire [31:1]NLW_RAM_reg_0_9_DOBDO_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_9_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_0_9_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_0_9_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_0_9_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_0" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "0" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR}),
        .ADDRBWRADDR({1'b1,ADDRBWRADDR}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[0]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_0_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_0_DOBDO_UNCONNECTED[31:1],dout_b[0]}),
        .DOPADOP(NLW_RAM_reg_0_0_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_0_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_0_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_0_i_1_n_0,RAM_reg_0_0_i_1_n_0,RAM_reg_0_0_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_0_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_0_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_1" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "1" *) 
  (* ram_slice_end = "1" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_1
       (.ADDRARDADDR({1'b1,RAM_reg_0_1_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_1_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_1_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_1_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_1_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[1]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_1_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_1_DOBDO_UNCONNECTED[31:1],dout_b[1]}),
        .DOPADOP(NLW_RAM_reg_0_1_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_1_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_1_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_1_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_1_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_1_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_1_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_1_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_1_i_1_n_0,RAM_reg_0_1_i_1_n_0,RAM_reg_0_1_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_10" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "10" *) 
  (* ram_slice_end = "10" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_10
       (.ADDRARDADDR({1'b1,RAM_reg_0_10_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_10_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_10_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_10_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_10_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[10]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_10_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_10_DOBDO_UNCONNECTED[31:1],dout_b[10]}),
        .DOPADOP(NLW_RAM_reg_0_10_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_10_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_10_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_10_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_10_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_10_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_10_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_10_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_10_i_1_n_0,RAM_reg_0_10_i_1_n_0,RAM_reg_0_10_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_10_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_10_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_11" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "11" *) 
  (* ram_slice_end = "11" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_11
       (.ADDRARDADDR({1'b1,RAM_reg_0_11_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_11_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_11_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_11_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_11_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[11]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_11_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_11_DOBDO_UNCONNECTED[31:1],dout_b[11]}),
        .DOPADOP(NLW_RAM_reg_0_11_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_11_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_11_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_11_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_11_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_11_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_11_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_11_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_11_i_1_n_0,RAM_reg_0_11_i_1_n_0,RAM_reg_0_11_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_11_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_11_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_12" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "12" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_12
       (.ADDRARDADDR({1'b1,RAM_reg_0_12_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_12_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_12_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_12_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_12_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[12]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_12_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_12_DOBDO_UNCONNECTED[31:1],dout_b[12]}),
        .DOPADOP(NLW_RAM_reg_0_12_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_12_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_12_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_12_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_12_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_12_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_12_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_12_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_12_i_1_n_0,RAM_reg_0_12_i_1_n_0,RAM_reg_0_12_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_12_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_12_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_13" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "13" *) 
  (* ram_slice_end = "13" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_13
       (.ADDRARDADDR({1'b1,RAM_reg_0_13_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_13_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_13_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_13_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_13_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[13]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_13_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_13_DOBDO_UNCONNECTED[31:1],dout_b[13]}),
        .DOPADOP(NLW_RAM_reg_0_13_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_13_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_13_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_13_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_13_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_13_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_13_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_13_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_13_i_1_n_0,RAM_reg_0_13_i_1_n_0,RAM_reg_0_13_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_13_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_13_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_14" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "14" *) 
  (* ram_slice_end = "14" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_14
       (.ADDRARDADDR({1'b1,RAM_reg_0_14_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_14_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_14_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_14_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_14_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[14]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_14_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_14_DOBDO_UNCONNECTED[31:1],dout_b[14]}),
        .DOPADOP(NLW_RAM_reg_0_14_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_14_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_14_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_14_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_14_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_14_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_14_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_14_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_14_i_1_n_0,RAM_reg_0_14_i_1_n_0,RAM_reg_0_14_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_14_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_14_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_15" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "15" *) 
  (* ram_slice_end = "15" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_15
       (.ADDRARDADDR({1'b1,RAM_reg_0_15_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_15_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_15_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_15_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_15_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[15]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_15_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_15_DOBDO_UNCONNECTED[31:1],dout_b[15]}),
        .DOPADOP(NLW_RAM_reg_0_15_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_15_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_15_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_15_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_15_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_15_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_15_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_15_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_15_i_1_n_0,RAM_reg_0_15_i_1_n_0,RAM_reg_0_15_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_15_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_15_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_16" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "16" *) 
  (* ram_slice_end = "16" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_16
       (.ADDRARDADDR({1'b1,RAM_reg_0_16_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_16_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_16_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_16_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_16_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[16]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_16_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_16_DOBDO_UNCONNECTED[31:1],dout_b[16]}),
        .DOPADOP(NLW_RAM_reg_0_16_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_16_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_16_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_16_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_16_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_16_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_16_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_16_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_16_i_1_n_0,RAM_reg_0_16_i_1_n_0,RAM_reg_0_16_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_16_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_16_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_17" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "17" *) 
  (* ram_slice_end = "17" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_17
       (.ADDRARDADDR({1'b1,RAM_reg_0_17_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_17_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_17_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_17_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_17_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[17]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_17_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_17_DOBDO_UNCONNECTED[31:1],dout_b[17]}),
        .DOPADOP(NLW_RAM_reg_0_17_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_17_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_17_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_17_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_17_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_17_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_17_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_17_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_17_i_1_n_0,RAM_reg_0_17_i_1_n_0,RAM_reg_0_17_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_17_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_17_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_18" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "18" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_18
       (.ADDRARDADDR({1'b1,RAM_reg_0_18_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_18_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_18_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_18_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_18_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[18]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_18_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_18_DOBDO_UNCONNECTED[31:1],dout_b[18]}),
        .DOPADOP(NLW_RAM_reg_0_18_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_18_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_18_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_18_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_18_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_18_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_18_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_18_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_18_i_1_n_0,RAM_reg_0_18_i_1_n_0,RAM_reg_0_18_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_18_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_18_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_19" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "19" *) 
  (* ram_slice_end = "19" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_19
       (.ADDRARDADDR({1'b1,RAM_reg_0_19_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_19_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_19_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_19_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_19_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[19]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_19_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_19_DOBDO_UNCONNECTED[31:1],dout_b[19]}),
        .DOPADOP(NLW_RAM_reg_0_19_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_19_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_19_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_19_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_19_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_19_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_19_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_19_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_19_i_1_n_0,RAM_reg_0_19_i_1_n_0,RAM_reg_0_19_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_19_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_19_i_1_n_0));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_1_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_1_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_2" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "2" *) 
  (* ram_slice_end = "2" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_2
       (.ADDRARDADDR({1'b1,RAM_reg_0_2_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_2_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_2_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_2_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_2_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[2]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_2_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_2_DOBDO_UNCONNECTED[31:1],dout_b[2]}),
        .DOPADOP(NLW_RAM_reg_0_2_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_2_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_2_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_2_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_2_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_2_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_2_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_2_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_2_i_1_n_0,RAM_reg_0_2_i_1_n_0,RAM_reg_0_2_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_20" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "20" *) 
  (* ram_slice_end = "20" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_20
       (.ADDRARDADDR({1'b1,RAM_reg_0_20_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_20_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_20_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_20_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_20_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[20]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_20_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_20_DOBDO_UNCONNECTED[31:1],dout_b[20]}),
        .DOPADOP(NLW_RAM_reg_0_20_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_20_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_20_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_20_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_20_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_20_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_20_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_20_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_20_i_1_n_0,RAM_reg_0_20_i_1_n_0,RAM_reg_0_20_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_20_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_20_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_21" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "21" *) 
  (* ram_slice_end = "21" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_21
       (.ADDRARDADDR({1'b1,RAM_reg_0_21_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_21_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_21_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_21_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_21_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[21]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_21_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_21_DOBDO_UNCONNECTED[31:1],dout_b[21]}),
        .DOPADOP(NLW_RAM_reg_0_21_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_21_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_21_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_21_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_21_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_21_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_21_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_21_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_21_i_1_n_0,RAM_reg_0_21_i_1_n_0,RAM_reg_0_21_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_21_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_21_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_22" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "22" *) 
  (* ram_slice_end = "22" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_22
       (.ADDRARDADDR({1'b1,RAM_reg_0_22_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_22_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_22_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_22_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_22_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[22]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_22_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_22_DOBDO_UNCONNECTED[31:1],dout_b[22]}),
        .DOPADOP(NLW_RAM_reg_0_22_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_22_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_22_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_22_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_22_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_22_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_22_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_22_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_22_i_1_n_0,RAM_reg_0_22_i_1_n_0,RAM_reg_0_22_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_22_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_22_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_23" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "23" *) 
  (* ram_slice_end = "23" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_23
       (.ADDRARDADDR({1'b1,addr_a}),
        .ADDRBWRADDR({1'b1,addr_b}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_23_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_23_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_23_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[23]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_23_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_23_DOBDO_UNCONNECTED[31:1],dout_b[23]}),
        .DOPADOP(NLW_RAM_reg_0_23_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_23_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_23_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_23_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_23_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_23_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_23_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_23_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_23_i_1_n_0,RAM_reg_0_23_i_1_n_0,RAM_reg_0_23_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_23_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_23_i_1_n_0));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_2_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_2_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_3" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "3" *) 
  (* ram_slice_end = "3" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_3
       (.ADDRARDADDR({1'b1,RAM_reg_0_3_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_3_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_3_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_3_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_3_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[3]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_3_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_3_DOBDO_UNCONNECTED[31:1],dout_b[3]}),
        .DOPADOP(NLW_RAM_reg_0_3_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_3_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_3_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_3_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_3_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_3_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_3_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_3_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_3_i_1_n_0,RAM_reg_0_3_i_1_n_0,RAM_reg_0_3_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_3_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_3_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_4" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "4" *) 
  (* ram_slice_end = "4" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_4
       (.ADDRARDADDR({1'b1,RAM_reg_0_4_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_4_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_4_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_4_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_4_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[4]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_4_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_4_DOBDO_UNCONNECTED[31:1],dout_b[4]}),
        .DOPADOP(NLW_RAM_reg_0_4_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_4_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_4_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_4_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_4_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_4_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_4_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_4_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_4_i_1_n_0,RAM_reg_0_4_i_1_n_0,RAM_reg_0_4_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_4_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_4_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_5" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "5" *) 
  (* ram_slice_end = "5" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_5
       (.ADDRARDADDR({1'b1,RAM_reg_0_5_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_5_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_5_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_5_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_5_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[5]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_5_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_5_DOBDO_UNCONNECTED[31:1],dout_b[5]}),
        .DOPADOP(NLW_RAM_reg_0_5_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_5_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_5_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_5_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_5_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_5_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_5_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_5_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_5_i_1_n_0,RAM_reg_0_5_i_1_n_0,RAM_reg_0_5_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_5_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_5_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_6" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "6" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_6
       (.ADDRARDADDR({1'b1,RAM_reg_0_6_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_6_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_6_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_6_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_6_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[6]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_6_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_6_DOBDO_UNCONNECTED[31:1],dout_b[6]}),
        .DOPADOP(NLW_RAM_reg_0_6_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_6_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_6_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_6_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_6_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_6_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_6_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_6_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_6_i_1_n_0,RAM_reg_0_6_i_1_n_0,RAM_reg_0_6_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_6_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_6_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_7" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "7" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_7
       (.ADDRARDADDR({1'b1,RAM_reg_0_7_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_7_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_7_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_7_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_7_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[7]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_7_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_7_DOBDO_UNCONNECTED[31:1],dout_b[7]}),
        .DOPADOP(NLW_RAM_reg_0_7_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_7_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_7_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_7_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_7_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_7_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_7_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_7_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_7_i_1_n_0,RAM_reg_0_7_i_1_n_0,RAM_reg_0_7_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_7_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_7_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_8" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "8" *) 
  (* ram_slice_end = "8" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_8
       (.ADDRARDADDR({1'b1,RAM_reg_0_8_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_8_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_8_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_8_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_8_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[8]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_8_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_8_DOBDO_UNCONNECTED[31:1],dout_b[8]}),
        .DOPADOP(NLW_RAM_reg_0_8_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_8_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_8_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_8_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_8_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_8_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_8_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_8_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_8_i_1_n_0,RAM_reg_0_8_i_1_n_0,RAM_reg_0_8_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_8_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_8_i_1_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d1" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "786432" *) 
  (* RTL_RAM_NAME = "U0/project_audio_ip_v1_0_S00_AXI_inst/project_audio0/circle_buffer0/ram0/RAM_reg_0_9" *) 
  (* RTL_RAM_TYPE = "RAM_SDP" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "9" *) 
  (* ram_slice_end = "9" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(1),
    .READ_WIDTH_B(1),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(1),
    .WRITE_WIDTH_B(1)) 
    RAM_reg_0_9
       (.ADDRARDADDR({1'b1,RAM_reg_0_9_0}),
        .ADDRBWRADDR({1'b1,RAM_reg_0_9_1}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_RAM_reg_0_9_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_RAM_reg_0_9_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(s00_axi_aclk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_0_9_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,s00_axi_wdata[9]}),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_RAM_reg_0_9_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_RAM_reg_0_9_DOBDO_UNCONNECTED[31:1],dout_b[9]}),
        .DOPADOP(NLW_RAM_reg_0_9_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_RAM_reg_0_9_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_0_9_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(RAM_reg_0_9_i_1_n_0),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_RAM_reg_0_9_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_RAM_reg_0_9_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_RAM_reg_0_9_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_0_9_SBITERR_UNCONNECTED),
        .WEA({RAM_reg_0_9_i_1_n_0,RAM_reg_0_9_i_1_n_0,RAM_reg_0_9_i_1_n_0,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT6 #(
    .INIT(64'h4000000000000000)) 
    RAM_reg_0_9_i_1
       (.I0(p_0_in_0[1]),
        .I1(p_0_in_0[0]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(RAM_reg_0_9_i_1_n_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_project_audio
   (dout_b,
    s00_axi_aclk,
    p_0_in_0,
    s00_axi_wvalid,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    s00_axi_awvalid,
    s00_axi_wdata);
  output [23:0]dout_b;
  input s00_axi_aclk;
  input [1:0]p_0_in_0;
  input s00_axi_wvalid;
  input S_AXI_WREADY;
  input S_AXI_AWREADY;
  input s00_axi_awvalid;
  input [23:0]s00_axi_wdata;

  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire [23:0]dout_b;
  wire [1:0]p_0_in_0;
  wire s00_axi_aclk;
  wire s00_axi_awvalid;
  wire [23:0]s00_axi_wdata;
  wire s00_axi_wvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_circle_buffer circle_buffer0
       (.S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .dout_b(dout_b),
        .p_0_in_0(p_0_in_0),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

(* CHECK_LICENSE_TYPE = "project_audio_design_project_audio_ip_0_0,project_audio_ip_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "project_audio_ip_v1_0,Vivado 2022.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 4, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.0, CLK_DOMAIN project_audio_design_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [3:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [3:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN project_audio_design_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_project_audio_ip_v1_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[3:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[3:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_project_audio_ip_v1_0
   (S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_wdata,
    s00_axi_awaddr,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [31:0]s00_axi_wdata;
  input [1:0]s00_axi_awaddr;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input [1:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_project_audio_ip_v1_0_S00_AXI project_audio_ip_v1_0_S00_AXI_inst
       (.S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_project_audio_ip_v1_0_S00_AXI
   (S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    s00_axi_aclk,
    s00_axi_wdata,
    s00_axi_awaddr,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_araddr,
    s00_axi_arvalid,
    s00_axi_wstrb,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input s00_axi_aclk;
  input [31:0]s00_axi_wdata;
  input [1:0]s00_axi_awaddr;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input [1:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire [3:2]axi_araddr;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire axi_arready0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire [23:0]out_project_audio;
  wire [1:0]p_0_in_0;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [1:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [1:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [31:0]slv_reg1;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [31:0]slv_reg2;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire [31:0]slv_reg3;
  wire slv_reg_rden;
  wire slv_reg_wren__2;

  LUT6 #(
    .INIT(64'hBFFF8CCC8CCC8CCC)) 
    aw_en_i_1
       (.I0(S_AXI_AWREADY),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_wvalid),
        .I3(s00_axi_awvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[2]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[3]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(axi_araddr[2]),
        .S(axi_awready_i_1_n_0));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(axi_araddr[3]),
        .S(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wvalid),
        .I3(aw_en_reg_n_0),
        .I4(S_AXI_AWREADY),
        .I5(p_0_in_0[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wvalid),
        .I3(aw_en_reg_n_0),
        .I4(S_AXI_AWREADY),
        .I5(p_0_in_0[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_0_in_0[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_0_in_0[1]),
        .R(axi_awready_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(aw_en_reg_n_0),
        .I3(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[0]_i_1 
       (.I0(slv_reg1[0]),
        .I1(out_project_audio[0]),
        .I2(slv_reg3[0]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[0]),
        .O(reg_data_out[0]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[10]_i_1 
       (.I0(slv_reg1[10]),
        .I1(out_project_audio[10]),
        .I2(slv_reg3[10]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[10]),
        .O(reg_data_out[10]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[11]_i_1 
       (.I0(slv_reg1[11]),
        .I1(out_project_audio[11]),
        .I2(slv_reg3[11]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[11]),
        .O(reg_data_out[11]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[12]_i_1 
       (.I0(slv_reg1[12]),
        .I1(out_project_audio[12]),
        .I2(slv_reg3[12]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[12]),
        .O(reg_data_out[12]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[13]_i_1 
       (.I0(slv_reg1[13]),
        .I1(out_project_audio[13]),
        .I2(slv_reg3[13]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[13]),
        .O(reg_data_out[13]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[14]_i_1 
       (.I0(slv_reg1[14]),
        .I1(out_project_audio[14]),
        .I2(slv_reg3[14]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[14]),
        .O(reg_data_out[14]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[15]_i_1 
       (.I0(slv_reg1[15]),
        .I1(out_project_audio[15]),
        .I2(slv_reg3[15]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[15]),
        .O(reg_data_out[15]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[16]_i_1 
       (.I0(slv_reg1[16]),
        .I1(out_project_audio[16]),
        .I2(slv_reg3[16]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[16]),
        .O(reg_data_out[16]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[17]_i_1 
       (.I0(slv_reg1[17]),
        .I1(out_project_audio[17]),
        .I2(slv_reg3[17]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[17]),
        .O(reg_data_out[17]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[18]_i_1 
       (.I0(slv_reg1[18]),
        .I1(out_project_audio[18]),
        .I2(slv_reg3[18]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[18]),
        .O(reg_data_out[18]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[19]_i_1 
       (.I0(slv_reg1[19]),
        .I1(out_project_audio[19]),
        .I2(slv_reg3[19]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[19]),
        .O(reg_data_out[19]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[1]_i_1 
       (.I0(slv_reg1[1]),
        .I1(out_project_audio[1]),
        .I2(slv_reg3[1]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[1]),
        .O(reg_data_out[1]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[20]_i_1 
       (.I0(slv_reg1[20]),
        .I1(out_project_audio[20]),
        .I2(slv_reg3[20]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[20]),
        .O(reg_data_out[20]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[21]_i_1 
       (.I0(slv_reg1[21]),
        .I1(out_project_audio[21]),
        .I2(slv_reg3[21]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[21]),
        .O(reg_data_out[21]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[22]_i_1 
       (.I0(slv_reg1[22]),
        .I1(out_project_audio[22]),
        .I2(slv_reg3[22]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[22]),
        .O(reg_data_out[22]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[23]_i_1 
       (.I0(slv_reg1[23]),
        .I1(out_project_audio[23]),
        .I2(slv_reg3[23]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[23]),
        .O(reg_data_out[23]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[24]_i_1 
       (.I0(slv_reg1[24]),
        .I1(slv_reg3[24]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[24]),
        .O(reg_data_out[24]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[25]_i_1 
       (.I0(slv_reg1[25]),
        .I1(slv_reg3[25]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[25]),
        .O(reg_data_out[25]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[26]_i_1 
       (.I0(slv_reg1[26]),
        .I1(slv_reg3[26]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[26]),
        .O(reg_data_out[26]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[27]_i_1 
       (.I0(slv_reg1[27]),
        .I1(slv_reg3[27]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[27]),
        .O(reg_data_out[27]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[28]_i_1 
       (.I0(slv_reg1[28]),
        .I1(slv_reg3[28]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[28]),
        .O(reg_data_out[28]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[29]_i_1 
       (.I0(slv_reg1[29]),
        .I1(slv_reg3[29]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[29]),
        .O(reg_data_out[29]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[2]_i_1 
       (.I0(slv_reg1[2]),
        .I1(out_project_audio[2]),
        .I2(slv_reg3[2]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[2]),
        .O(reg_data_out[2]));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[30]_i_1 
       (.I0(slv_reg1[30]),
        .I1(slv_reg3[30]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[30]),
        .O(reg_data_out[30]));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden));
  LUT5 #(
    .INIT(32'hCAF0CA00)) 
    \axi_rdata[31]_i_2 
       (.I0(slv_reg1[31]),
        .I1(slv_reg3[31]),
        .I2(axi_araddr[3]),
        .I3(axi_araddr[2]),
        .I4(slv_reg2[31]),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[3]_i_1 
       (.I0(slv_reg1[3]),
        .I1(out_project_audio[3]),
        .I2(slv_reg3[3]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[3]),
        .O(reg_data_out[3]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[4]_i_1 
       (.I0(slv_reg1[4]),
        .I1(out_project_audio[4]),
        .I2(slv_reg3[4]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[4]),
        .O(reg_data_out[4]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[5]_i_1 
       (.I0(slv_reg1[5]),
        .I1(out_project_audio[5]),
        .I2(slv_reg3[5]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[5]),
        .O(reg_data_out[5]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[6]_i_1 
       (.I0(slv_reg1[6]),
        .I1(out_project_audio[6]),
        .I2(slv_reg3[6]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[6]),
        .O(reg_data_out[6]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[7]_i_1 
       (.I0(slv_reg1[7]),
        .I1(out_project_audio[7]),
        .I2(slv_reg3[7]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[7]),
        .O(reg_data_out[7]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[8]_i_1 
       (.I0(slv_reg1[8]),
        .I1(out_project_audio[8]),
        .I2(slv_reg3[8]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[8]),
        .O(reg_data_out[8]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[9]_i_1 
       (.I0(slv_reg1[9]),
        .I1(out_project_audio[9]),
        .I2(slv_reg3[9]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[9]),
        .O(reg_data_out[9]));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(aw_en_reg_n_0),
        .I3(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(axi_awready_i_1_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_project_audio project_audio0
       (.S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .dout_b(out_project_audio),
        .p_0_in_0(p_0_in_0),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_wdata(s00_axi_wdata[23:0]),
        .s00_axi_wvalid(s00_axi_wvalid));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in_0[0]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in_0[0]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in_0[0]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in_0[0]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in_0[0]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in_0[0]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in_0[0]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in_0[1]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in_0[0]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg2[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in_0[0]),
        .I3(p_0_in_0[1]),
        .O(p_1_in[15]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in_0[0]),
        .I3(p_0_in_0[1]),
        .O(p_1_in[23]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in_0[0]),
        .I3(p_0_in_0[1]),
        .O(p_1_in[31]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[31]_i_2 
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__2));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in_0[0]),
        .I3(p_0_in_0[1]),
        .O(p_1_in[7]));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg3[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg3[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg3[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg3[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg3[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg3[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg3[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg3[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg3[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg3[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg3[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg3[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg3[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg3[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg3[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg3[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg3[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg3[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg3[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg3[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg3[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg3[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg3[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg3[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg3[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg3[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg3[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg3[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg3[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg3[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg3[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg3[9]),
        .R(axi_awready_i_1_n_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_reg_d
   (S,
    \start_v_reg[3]_rep ,
    \start_v_reg[7]_rep ,
    \start_v_reg[11]_rep ,
    p_0_in_0,
    s00_axi_wvalid,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    s00_axi_awvalid,
    start_v_reg,
    \end_v_reg[3]_rep__22 ,
    \end_v_reg[3]_rep__22_0 ,
    \end_v_reg[3]_rep__22_1 ,
    \end_v_reg[3]_rep__22_2 ,
    \end_v_reg[7]_rep__22 ,
    \end_v_reg[7]_rep__22_0 ,
    \end_v_reg[7]_rep__22_1 ,
    \end_v_reg[7]_rep__22_2 ,
    \end_v_reg[11]_rep__22 ,
    \end_v_reg[11]_rep__22_0 ,
    \end_v_reg[11]_rep__22_1 ,
    \end_v_reg[11]_rep__22_2 ,
    \end_v_reg[14]_rep__22 ,
    \end_v_reg[14]_rep__22_0 ,
    s00_axi_wdata,
    s00_axi_aclk);
  output [2:0]S;
  output [3:0]\start_v_reg[3]_rep ;
  output [3:0]\start_v_reg[7]_rep ;
  output [3:0]\start_v_reg[11]_rep ;
  input [1:0]p_0_in_0;
  input s00_axi_wvalid;
  input S_AXI_WREADY;
  input S_AXI_AWREADY;
  input s00_axi_awvalid;
  input [0:0]start_v_reg;
  input \end_v_reg[3]_rep__22 ;
  input \end_v_reg[3]_rep__22_0 ;
  input \end_v_reg[3]_rep__22_1 ;
  input \end_v_reg[3]_rep__22_2 ;
  input \end_v_reg[7]_rep__22 ;
  input \end_v_reg[7]_rep__22_0 ;
  input \end_v_reg[7]_rep__22_1 ;
  input \end_v_reg[7]_rep__22_2 ;
  input \end_v_reg[11]_rep__22 ;
  input \end_v_reg[11]_rep__22_0 ;
  input \end_v_reg[11]_rep__22_1 ;
  input \end_v_reg[11]_rep__22_2 ;
  input \end_v_reg[14]_rep__22 ;
  input \end_v_reg[14]_rep__22_0 ;
  input [14:0]s00_axi_wdata;
  input s00_axi_aclk;

  wire [14:0]Q;
  wire [2:0]S;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire en_echo2_out;
  wire \end_v_reg[11]_rep__22 ;
  wire \end_v_reg[11]_rep__22_0 ;
  wire \end_v_reg[11]_rep__22_1 ;
  wire \end_v_reg[11]_rep__22_2 ;
  wire \end_v_reg[14]_rep__22 ;
  wire \end_v_reg[14]_rep__22_0 ;
  wire \end_v_reg[3]_rep__22 ;
  wire \end_v_reg[3]_rep__22_0 ;
  wire \end_v_reg[3]_rep__22_1 ;
  wire \end_v_reg[3]_rep__22_2 ;
  wire \end_v_reg[7]_rep__22 ;
  wire \end_v_reg[7]_rep__22_0 ;
  wire \end_v_reg[7]_rep__22_1 ;
  wire \end_v_reg[7]_rep__22_2 ;
  wire [1:0]p_0_in_0;
  wire s00_axi_aclk;
  wire s00_axi_awvalid;
  wire [14:0]s00_axi_wdata;
  wire s00_axi_wvalid;
  wire [0:0]start_v_reg;
  wire [3:0]\start_v_reg[11]_rep ;
  wire [3:0]\start_v_reg[3]_rep ;
  wire [3:0]\start_v_reg[7]_rep ;

  LUT6 #(
    .INIT(64'h1000000000000000)) 
    \Q[14]_i_1 
       (.I0(p_0_in_0[0]),
        .I1(p_0_in_0[1]),
        .I2(s00_axi_wvalid),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_AWREADY),
        .I5(s00_axi_awvalid),
        .O(en_echo2_out));
  FDRE \Q_reg[0] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[0]),
        .Q(Q[0]),
        .R(1'b0));
  FDRE \Q_reg[10] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[10]),
        .Q(Q[10]),
        .R(1'b0));
  FDRE \Q_reg[11] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[11]),
        .Q(Q[11]),
        .R(1'b0));
  FDRE \Q_reg[12] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[12]),
        .Q(Q[12]),
        .R(1'b0));
  FDRE \Q_reg[13] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[13]),
        .Q(Q[13]),
        .R(1'b0));
  FDRE \Q_reg[14] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[14]),
        .Q(Q[14]),
        .R(1'b0));
  FDRE \Q_reg[1] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[1]),
        .Q(Q[1]),
        .R(1'b0));
  FDRE \Q_reg[2] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[2]),
        .Q(Q[2]),
        .R(1'b0));
  FDRE \Q_reg[3] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[3]),
        .Q(Q[3]),
        .R(1'b0));
  FDRE \Q_reg[4] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[4]),
        .Q(Q[4]),
        .R(1'b0));
  FDRE \Q_reg[5] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[5]),
        .Q(Q[5]),
        .R(1'b0));
  FDRE \Q_reg[6] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[6]),
        .Q(Q[6]),
        .R(1'b0));
  FDRE \Q_reg[7] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[7]),
        .Q(Q[7]),
        .R(1'b0));
  FDRE \Q_reg[8] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[8]),
        .Q(Q[8]),
        .R(1'b0));
  FDRE \Q_reg[9] 
       (.C(s00_axi_aclk),
        .CE(en_echo2_out),
        .D(s00_axi_wdata[9]),
        .Q(Q[9]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry__0_i_1
       (.I0(\end_v_reg[7]_rep__22_2 ),
        .I1(Q[7]),
        .O(\start_v_reg[7]_rep [3]));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry__0_i_2
       (.I0(\end_v_reg[7]_rep__22_1 ),
        .I1(Q[6]),
        .O(\start_v_reg[7]_rep [2]));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry__0_i_3
       (.I0(\end_v_reg[7]_rep__22_0 ),
        .I1(Q[5]),
        .O(\start_v_reg[7]_rep [1]));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry__0_i_4
       (.I0(\end_v_reg[7]_rep__22 ),
        .I1(Q[4]),
        .O(\start_v_reg[7]_rep [0]));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry__1_i_1
       (.I0(\end_v_reg[11]_rep__22_2 ),
        .I1(Q[11]),
        .O(\start_v_reg[11]_rep [3]));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry__1_i_2
       (.I0(\end_v_reg[11]_rep__22_1 ),
        .I1(Q[10]),
        .O(\start_v_reg[11]_rep [2]));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry__1_i_3
       (.I0(\end_v_reg[11]_rep__22_0 ),
        .I1(Q[9]),
        .O(\start_v_reg[11]_rep [1]));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry__1_i_4
       (.I0(\end_v_reg[11]_rep__22 ),
        .I1(Q[8]),
        .O(\start_v_reg[11]_rep [0]));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry__2_i_1
       (.I0(start_v_reg),
        .I1(Q[14]),
        .O(S[2]));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry__2_i_2
       (.I0(\end_v_reg[14]_rep__22_0 ),
        .I1(Q[13]),
        .O(S[1]));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry__2_i_3
       (.I0(\end_v_reg[14]_rep__22 ),
        .I1(Q[12]),
        .O(S[0]));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry_i_1
       (.I0(\end_v_reg[3]_rep__22_2 ),
        .I1(Q[3]),
        .O(\start_v_reg[3]_rep [3]));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry_i_2
       (.I0(\end_v_reg[3]_rep__22_1 ),
        .I1(Q[2]),
        .O(\start_v_reg[3]_rep [2]));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry_i_3
       (.I0(\end_v_reg[3]_rep__22_0 ),
        .I1(Q[1]),
        .O(\start_v_reg[3]_rep [1]));
  LUT2 #(
    .INIT(4'h6)) 
    end_v0_carry_i_4
       (.I0(\end_v_reg[3]_rep__22 ),
        .I1(Q[0]),
        .O(\start_v_reg[3]_rep [0]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
