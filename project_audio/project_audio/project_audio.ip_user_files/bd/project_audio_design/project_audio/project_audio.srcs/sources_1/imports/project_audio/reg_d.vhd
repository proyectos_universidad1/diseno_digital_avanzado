----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/25/2023 08:47:16 PM
-- Design Name: 
-- Module Name: reg_d - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity reg_d is
    generic (N: integer := 8); 
    Port (
        en, clk: in std_logic;
        D: in std_logic_vector(N-1 downto 0);
        Q: out std_logic_vector(N-1 downto 0)
    );
end reg_d;

architecture Behavioral of reg_d is

begin
    process (clk, D, en) 
    begin
        if en = '1' then
            if rising_edge(clk) then
                Q <= D;
            end if;
        end if;        
    end process;

end Behavioral;
