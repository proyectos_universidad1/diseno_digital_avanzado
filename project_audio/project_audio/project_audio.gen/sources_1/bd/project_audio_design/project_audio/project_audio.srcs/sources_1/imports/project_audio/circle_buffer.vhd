----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/06/2023 07:42:29 AM
-- Design Name: 
-- Module Name: circle_buffer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity circle_buffer is
  generic (n_data: integer := 24; n_addr: integer := 15);
  Port (
      clk, en_a, en_b, w_a, en_echo: in std_logic;
      in_data: in std_logic_vector(n_data-1 downto 0);
      out_data: out std_logic_vector(n_data-1 downto 0)
  );
end circle_buffer;

architecture Behavioral of circle_buffer is
    
    -- component for ram with dual port and one clock-----------------
    component dual_port_RAM is
        generic( 
            n_data: integer := 24;
            n_addr: integer := 15 
        );
        
        Port (
            clk, en_a, en_b, w_a: in std_logic;
            addr_a, addr_b: in std_logic_vector(n_addr-1 downto 0);
            din_a: in std_logic_vector(n_data-1 downto 0);
            dout_b: out std_logic_vector(n_data-1 downto 0)
        );
    end component;
    -----------------------------------------------------------------
    
    --component for register D
    component reg_d is
        generic (N: integer := 8); 
        Port (
            en, clk: in std_logic;
            D: in std_logic_vector(N-1 downto 0);
            Q: out std_logic_vector(N-1 downto 0)
        );
    end component;
    ------------------------------------------------------------------
    
    --signals for dual_port_RAM-----------------------------------------
    signal start_v: std_logic_vector(n_addr-1 downto 0) := (others=>'0');
    signal end_v: std_logic_vector(n_addr-1 downto 0) := (others => '1');
    ---------------------------------------------------------------------
    
    -- signals for register d ----------------------------------------------
    signal n_echo: std_logic_vector(n_addr-1 downto 0);
    ----------------------------------------------------------------------
begin
    
    -- implementation of dual_port_RAM ----------------
    ram0: dual_port_RAM
    generic map(n_data => n_data, n_addr => n_addr)
    port map(
        clk=>clk, en_a=>en_a, en_b =>en_b, 
        w_a=>w_a, addr_a=>start_v, addr_b=>end_v,
        din_a=>in_data, dout_b=>out_data
    );
    ----------------------------------------------------
    
    --implementation of register D--------------------------------------------------
    reg_d0: reg_d
    generic map(N => n_addr)
    port map(en => en_echo, clk=>clk, D => in_data(n_addr-1 downto 0), Q => n_echo);
    ---------------------------------------------------------------------------------
    
    
    -- update the sections register in ram with modular 
    process(clk, en_a, w_a, en_echo, start_v) 
    begin 
    if rising_edge(clk) then
        if en_a = '1' then
            if w_a = '1' then
                start_v <= std_logic_vector(unsigned(start_v) - to_unsigned(1, start_v'length));
            end if;
        end if;
        
        end_v <= std_logic_vector((unsigned(start_v)) + unsigned(n_echo));
    end if;    
    end process;     
   
end Behavioral;
