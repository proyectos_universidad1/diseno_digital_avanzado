----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/12/2023 01:01:45 PM
-- Design Name: 
-- Module Name: tb_project_audio - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use std.textio.all;
use ieee.std_logic_textio.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_project_audio is
--  Port ( );
end tb_project_audio;

architecture Behavioral of tb_project_audio is

    -- component of project audio -----------------------------
    component project_audio is
        --generic(n_data: integer := 24; n_addr: integer := 15);   
        Port (
            clk, en_a, en_b, w_a, en_echo : in std_logic;
            in_data: in std_logic_vector (23 downto 0);
            out_data: out std_logic_vector(23 downto 0)
        );
    end component;
    ------------------------------------------------------------
    constant clk_period : time := 10ns;  
    
    file input_buf: text;
    
    signal clk, en_a, en_b, w_a: std_logic;
    signal en_echo: std_logic := '1';
    signal in_data: std_logic_vector(23 downto 0);
    signal out_data: std_logic_vector(23 downto 0);
begin
    
    -- implementation of component project audio ----------
    project_audio0: project_audio 
    port map(
        clk=>clk, en_a=>en_a, en_b=>en_b, w_a=>w_a,
        in_data => in_data, out_data=>out_data, en_echo => en_echo
    );
    -------------------------------------------------------
    
    -- read file and update input data----------------------------
    process
        variable data_buf : line;
        variable value: integer;
    begin
        -- select delay of signal
        en_a <= '0';
        en_b <= '0';
        w_a <= '0';
        
        clk<='0';           
        wait for clk_period/2;
        clk <= '1';            
        wait for clk_period/2;
        
        en_echo <= '1';
        in_data <= std_logic_vector(to_unsigned(9000, in_data'length));
        
        clk<='0';           
        wait for clk_period/2;
        clk <= '1';            
        wait for clk_period/2;
        
        en_echo <= '0';
        
        clk<='0';           
        wait for clk_period/2;
        clk <= '1';            
        wait for clk_period/2;
        
        en_a <= '1';
        en_b <= '1';
        w_a <= '1';
        
        --open data file
        file_open(input_buf, "signal_test.txt", read_mode);
        
        -- read lines of text file
        while not(endfile(input_buf)) loop
            --read line
            readline(input_buf, data_buf);
            -- read value
            read(data_buf, value);
            -- put data
            in_data <= std_logic_vector(to_signed(value, 24));
            
            -- signal clock
            clk<='0';           
            wait for clk_period/2;
            clk <= '1';            
            wait for clk_period/2;
        end loop;   
    end process;
    ------------------------------------------------------------------------------------

end Behavioral;







