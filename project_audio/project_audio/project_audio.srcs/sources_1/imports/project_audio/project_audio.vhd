----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/05/2023 08:32:55 PM
-- Design Name: 
-- Module Name: project_audio - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- project effects of audio
entity project_audio is
    generic(n_data: integer := 24; n_addr: integer := 15);   
    Port (
        clk, en_a, en_b, w_a, en_echo : in std_logic;
        in_data: in std_logic_vector (n_data-1 downto 0);
        out_data: out std_logic_vector(n_data-1 downto 0)
    );
end project_audio;

architecture Behavioral of project_audio is
    
    -- component for circle buffer ----------------------------
    component circle_buffer is
      generic(n_data: integer := 24; n_addr: integer := 15);
      Port (
          clk, en_a, en_b, w_a, en_echo: in std_logic;
          in_data: in std_logic_vector(n_data-1 downto 0);
          out_data: out std_logic_vector(n_data-1 downto 0)
      );
    end component;
    ----------------------------------------------------------- 
begin

    -- implementation for circle buffer -----------------------
    circle_buffer0: circle_buffer
    generic map (n_data=>n_data, n_addr=>n_addr)
    port map(
        clk=>clk, en_a=>en_a, en_b=>en_b, w_a=>w_a,
        in_data=>in_data, out_data=>out_data, en_echo=>en_echo
    );    
    ----------------------------------------------------------
end Behavioral;






