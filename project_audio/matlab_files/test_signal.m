clc;
clear all;
close all;

% sen(2*pi*f1*t) + sen(2*pi*f2*t)

% frec of signals
f1 = 3000;
f2 = 5000; % max frec of f1

% amplitudes of signals
a1 = 1000;
a2 = 2000;

% sample frec of signals
fs1 = 20*f1;
fs2 = 20*f2;

t = 0:1/fs2:(10/f2);

% vectors time
y1 = (a1*sin(2*pi*f1*t));
y2 = (a2*sin(2*pi*f2*t));
yt = y1 + y2;

figure;
plot(y1)
hold on;
plot(y2);

figure;
plot(yt)

f = fopen('signal_test.txt', 'w');
fprintf(f, '%f\n', yt);
fclose(f);

