clc;
clear all;
close all;
T=5;
T_echo=341e-3;

[x,fs]=audioread('anita.wav');

N_echo=round(fs*T_echo)-1;
N_buffer=32768;
buffer=zeros(1,N_buffer);
buffer_start=0;
buffer_end = N_echo;

y=zeros(1,numel(x));
y_des = zeros(1, numel(x));

for n=0:numel(x)-1
 buffer(buffer_start+1)=x(n+1);   
 y(n+1)=buffer(buffer_start+1)+buffer(buffer_end+1);
 y_des(n+1)=buffer(buffer_end+1);
 
 buffer_start=mod(buffer_start-1,N_buffer);
 buffer_end=mod(buffer_start+N_echo,N_buffer);
    
end

dt = 1/fs;
t = 0:dt:((length(y)-1)*dt);

figure;
plot(t, x)
figure;
plot(t, y_des)

f = fopen('sound.txt', 'w');
fprintf(f, '%f\n', x*10000);
fclose(f);

figure;
plot(t, y);
soundsc(y, fs);



