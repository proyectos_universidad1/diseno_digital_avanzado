clc;
clear all;
close all;
T=5;
fc=500;


[x,fs]=audioread('anita.wav');


y=zeros(1,numel(x));

Omc=2*pi*fc/fs;

phase=0;

for n=0:numel(x)-1
y(n+1)=x(n+1)*cos(phase);
phase=phase+Omc;
end


soundsc(y,fs);