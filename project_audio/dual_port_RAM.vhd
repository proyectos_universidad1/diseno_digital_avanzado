----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/05/2023 08:53:47 PM
-- Design Name: 
-- Module Name: dual_port_RAM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dual_port_RAM is
    generic( 
        n_data: integer := 24;
        n_addr: integer := 15 
    );
    Port (
        clk, en_a, en_b, w_a: in std_logic;
        addr_a, addr_b: in std_logic_vector(n_addr-1 downto 0);
        din_a: in std_logic_vector(n_data-1 downto 0);
        dout_b: out std_logic_vector(n_data-1 downto 0)
    );
end dual_port_RAM;

architecture Behavioral of dual_port_RAM is
    -- definitions of array to ram memory
    type ram_type is array ((2**n_addr)-1 downto 0) of std_logic_vector(n_data-1 downto 0);
    shared variable RAM : ram_type;
begin
    -- write of RAM memory in port a when rising_edge of clk and en_a = 1 and w_a = 1
    process(clk)
    begin
    if rising_edge(clk) then
    if en_a = '1' then
    if w_a = '1' then
        RAM(conv_integer(addr_a)) := din_a;
    end if;
    end if;
    end if;
    end process;

    -- read of RAM memory in port b when rising edge of clk and en_b = '1'
    process(clk)
    begin
    if rising_edge(clk) then
    if en_b = '1' then
        dout_b <= RAM(conv_integer(addr_b));
    end if;
    end if;
    end process;

end Behavioral;