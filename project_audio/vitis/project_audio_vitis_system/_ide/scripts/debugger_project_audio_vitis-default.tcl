# Usage with Vitis IDE:
# In Vitis IDE create a Single Application Debug launch configuration,
# change the debug type to 'Attach to running target' and provide this 
# tcl script in 'Execute Script' option.
# Path of this script: /home/juan/Documentos/JuanK/universidad/profun_2/project_audio/vitis/project_audio_vitis_system/_ide/scripts/debugger_project_audio_vitis-default.tcl
# 
# 
# Usage with xsct:
# To debug using xsct, launch xsct and run below command
# source /home/juan/Documentos/JuanK/universidad/profun_2/project_audio/vitis/project_audio_vitis_system/_ide/scripts/debugger_project_audio_vitis-default.tcl
# 
connect -url tcp:127.0.0.1:3121
targets -set -nocase -filter {name =~"APU*"}
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent Zybo 210279786950A" && level==0 && jtag_device_ctx=="jsn-Zybo-210279786950A-13722093-0"}
fpga -file /home/juan/Documentos/JuanK/universidad/profun_2/project_audio/vitis/project_audio_vitis/_ide/bitstream/project_audio_design_wrapper.bit
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw /home/juan/Documentos/JuanK/universidad/profun_2/project_audio/vitis/project_audio_design_wrapper/export/project_audio_design_wrapper/hw/project_audio_design_wrapper.xsa -mem-ranges [list {0x40000000 0xbfffffff}] -regs
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*"}
source /home/juan/Documentos/JuanK/universidad/profun_2/project_audio/vitis/project_audio_vitis/_ide/psinit/ps7_init.tcl
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "*A9*#0"}
dow /home/juan/Documentos/JuanK/universidad/profun_2/project_audio/vitis/project_audio_vitis/Debug/project_audio_vitis.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~ "*A9*#0"}
con
