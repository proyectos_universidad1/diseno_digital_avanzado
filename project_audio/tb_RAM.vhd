----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/05/2023 09:03:42 PM
-- Design Name: 
-- Module Name: tb_RAM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_RAM is
--  Port ( );
end tb_RAM;

architecture Behavioral of tb_RAM is
    constant clk_period: time := 100ns;

    component dual_port_RAM is
    Port (
        clk, en_a, en_b, w_a: in std_logic;
        addr_a, addr_b: in std_logic_vector(14 downto 0);
        din_a: in std_logic_vector(15 downto 0);
        dout_b: out std_logic_vector(15 downto 0)
    );
    end component;
    
    signal clk, en_a, en_b, w_a: std_logic;
    signal addr_a, addr_b: std_logic_vector(14 downto 0);
    signal din_a, dout_b: std_logic_vector(15 downto 0);
begin
    
    ram0: dual_port_RAM 
    port map(
       clk=>clk, en_a=>en_a, en_b=>en_b, w_a=>w_a,addr_a=>addr_a,
       addr_b=>addr_b, din_a=>din_a,dout_b=>dout_b
    );
    
    -- clock
    process 
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
    end process;
    
    -- signals of enable_ and enable_b
    process
    begin 
        en_a <= '1';
        en_b <= '1';
        wait for clk_period/4;
        en_a <= '0';
        en_b <= '0';
        wait for clk_period/4;
    end process;
    
    -- signals of test
    process 
    begin 
        -- save data on memory RAM
          w_a <= '0';
          addr_a <= (others => '0');
          din_a <= std_logic_vector(to_unsigned(7, din_a'length));
          wait for clk_period/4;
          w_a <= '1';
          wait for 3*clk_period/4;
          w_a <= '0';
          addr_a <= std_logic_vector(to_unsigned(10, addr_a'length));
          din_a <= std_logic_vector(to_unsigned(100, din_a'length));
          wait for clk_period/4;
          w_a <= '1';
          wait for 3*clk_period/4;
          w_a <= '0';
          addr_a <= std_logic_vector(to_unsigned(15, addr_a'length));
          din_a <= std_logic_vector(to_unsigned(200, din_a'length));
          wait for clk_period/4;
          w_a <= '1';
          wait for 3*clk_period/4;
          w_a <= '0';
          addr_b <= (others => '0');
          wait for clk_period;
          addr_b <= std_logic_vector(to_unsigned(10, addr_b'length));
          wait for clk_period;
          addr_b <= std_logic_vector(to_unsigned(15, addr_b'length));
          wait for clk_period;
          
          wait;
        --------------------------
    end process;

end Behavioral;



