-- Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
-- Date        : Sun Mar 12 06:36:46 2023
-- Host        : juan-Inspiron-14-3467 running 64-bit Linux Mint 21.1
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               /home/juan/Documentos/JuanK/universidad/profun_2/test_mixer/test_mixer/test_mixer.sim/sim_1/synth/func/xsim/tb_mixer_func_synth.vhd
-- Design      : mixer
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity mixer is
  port (
    clk : in STD_LOGIC;
    carrier_signal : in STD_LOGIC_VECTOR ( 15 downto 0 );
    mod_signal : in STD_LOGIC_VECTOR ( 15 downto 0 );
    if_signal : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of mixer : entity is true;
end mixer;

architecture STRUCTURE of mixer is
  signal carrier_signal_IBUF : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal if_signal_OBUF : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal if_signal_reg_n_100 : STD_LOGIC;
  signal if_signal_reg_n_101 : STD_LOGIC;
  signal if_signal_reg_n_102 : STD_LOGIC;
  signal if_signal_reg_n_103 : STD_LOGIC;
  signal if_signal_reg_n_104 : STD_LOGIC;
  signal if_signal_reg_n_105 : STD_LOGIC;
  signal if_signal_reg_n_90 : STD_LOGIC;
  signal if_signal_reg_n_91 : STD_LOGIC;
  signal if_signal_reg_n_92 : STD_LOGIC;
  signal if_signal_reg_n_93 : STD_LOGIC;
  signal if_signal_reg_n_94 : STD_LOGIC;
  signal if_signal_reg_n_95 : STD_LOGIC;
  signal if_signal_reg_n_96 : STD_LOGIC;
  signal if_signal_reg_n_97 : STD_LOGIC;
  signal if_signal_reg_n_98 : STD_LOGIC;
  signal if_signal_reg_n_99 : STD_LOGIC;
  signal mod_signal_IBUF : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_if_signal_reg_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_if_signal_reg_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_if_signal_reg_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_if_signal_reg_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_if_signal_reg_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_if_signal_reg_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_if_signal_reg_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_if_signal_reg_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_if_signal_reg_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_if_signal_reg_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 32 );
  signal NLW_if_signal_reg_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
begin
\carrier_signal_IBUF[0]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(0),
      O => carrier_signal_IBUF(0)
    );
\carrier_signal_IBUF[10]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(10),
      O => carrier_signal_IBUF(10)
    );
\carrier_signal_IBUF[11]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(11),
      O => carrier_signal_IBUF(11)
    );
\carrier_signal_IBUF[12]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(12),
      O => carrier_signal_IBUF(12)
    );
\carrier_signal_IBUF[13]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(13),
      O => carrier_signal_IBUF(13)
    );
\carrier_signal_IBUF[14]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(14),
      O => carrier_signal_IBUF(14)
    );
\carrier_signal_IBUF[15]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(15),
      O => carrier_signal_IBUF(15)
    );
\carrier_signal_IBUF[1]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(1),
      O => carrier_signal_IBUF(1)
    );
\carrier_signal_IBUF[2]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(2),
      O => carrier_signal_IBUF(2)
    );
\carrier_signal_IBUF[3]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(3),
      O => carrier_signal_IBUF(3)
    );
\carrier_signal_IBUF[4]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(4),
      O => carrier_signal_IBUF(4)
    );
\carrier_signal_IBUF[5]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(5),
      O => carrier_signal_IBUF(5)
    );
\carrier_signal_IBUF[6]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(6),
      O => carrier_signal_IBUF(6)
    );
\carrier_signal_IBUF[7]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(7),
      O => carrier_signal_IBUF(7)
    );
\carrier_signal_IBUF[8]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(8),
      O => carrier_signal_IBUF(8)
    );
\carrier_signal_IBUF[9]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => carrier_signal(9),
      O => carrier_signal_IBUF(9)
    );
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => clk,
      O => clk_IBUF
    );
\if_signal_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(0),
      O => if_signal(0)
    );
\if_signal_OBUF[10]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(10),
      O => if_signal(10)
    );
\if_signal_OBUF[11]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(11),
      O => if_signal(11)
    );
\if_signal_OBUF[12]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(12),
      O => if_signal(12)
    );
\if_signal_OBUF[13]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(13),
      O => if_signal(13)
    );
\if_signal_OBUF[14]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(14),
      O => if_signal(14)
    );
\if_signal_OBUF[15]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(15),
      O => if_signal(15)
    );
\if_signal_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(1),
      O => if_signal(1)
    );
\if_signal_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(2),
      O => if_signal(2)
    );
\if_signal_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(3),
      O => if_signal(3)
    );
\if_signal_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(4),
      O => if_signal(4)
    );
\if_signal_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(5),
      O => if_signal(5)
    );
\if_signal_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(6),
      O => if_signal(6)
    );
\if_signal_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(7),
      O => if_signal(7)
    );
\if_signal_OBUF[8]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(8),
      O => if_signal(8)
    );
\if_signal_OBUF[9]_inst\: unisim.vcomponents.OBUF
     port map (
      I => if_signal_OBUF(9),
      O => if_signal(9)
    );
if_signal_reg: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 1,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 1,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 1,
      BREG => 1,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 0,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 1,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => mod_signal_IBUF(15),
      A(28) => mod_signal_IBUF(15),
      A(27) => mod_signal_IBUF(15),
      A(26) => mod_signal_IBUF(15),
      A(25) => mod_signal_IBUF(15),
      A(24) => mod_signal_IBUF(15),
      A(23) => mod_signal_IBUF(15),
      A(22) => mod_signal_IBUF(15),
      A(21) => mod_signal_IBUF(15),
      A(20) => mod_signal_IBUF(15),
      A(19) => mod_signal_IBUF(15),
      A(18) => mod_signal_IBUF(15),
      A(17) => mod_signal_IBUF(15),
      A(16) => mod_signal_IBUF(15),
      A(15 downto 0) => mod_signal_IBUF(15 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_if_signal_reg_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => carrier_signal_IBUF(15),
      B(16) => carrier_signal_IBUF(15),
      B(15 downto 0) => carrier_signal_IBUF(15 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_if_signal_reg_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_if_signal_reg_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_if_signal_reg_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '1',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '1',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '1',
      CLK => clk_IBUF_BUFG,
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_if_signal_reg_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_if_signal_reg_OVERFLOW_UNCONNECTED,
      P(47 downto 32) => NLW_if_signal_reg_P_UNCONNECTED(47 downto 32),
      P(31 downto 16) => if_signal_OBUF(15 downto 0),
      P(15) => if_signal_reg_n_90,
      P(14) => if_signal_reg_n_91,
      P(13) => if_signal_reg_n_92,
      P(12) => if_signal_reg_n_93,
      P(11) => if_signal_reg_n_94,
      P(10) => if_signal_reg_n_95,
      P(9) => if_signal_reg_n_96,
      P(8) => if_signal_reg_n_97,
      P(7) => if_signal_reg_n_98,
      P(6) => if_signal_reg_n_99,
      P(5) => if_signal_reg_n_100,
      P(4) => if_signal_reg_n_101,
      P(3) => if_signal_reg_n_102,
      P(2) => if_signal_reg_n_103,
      P(1) => if_signal_reg_n_104,
      P(0) => if_signal_reg_n_105,
      PATTERNBDETECT => NLW_if_signal_reg_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_if_signal_reg_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_if_signal_reg_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_if_signal_reg_UNDERFLOW_UNCONNECTED
    );
\mod_signal_IBUF[0]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(0),
      O => mod_signal_IBUF(0)
    );
\mod_signal_IBUF[10]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(10),
      O => mod_signal_IBUF(10)
    );
\mod_signal_IBUF[11]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(11),
      O => mod_signal_IBUF(11)
    );
\mod_signal_IBUF[12]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(12),
      O => mod_signal_IBUF(12)
    );
\mod_signal_IBUF[13]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(13),
      O => mod_signal_IBUF(13)
    );
\mod_signal_IBUF[14]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(14),
      O => mod_signal_IBUF(14)
    );
\mod_signal_IBUF[15]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(15),
      O => mod_signal_IBUF(15)
    );
\mod_signal_IBUF[1]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(1),
      O => mod_signal_IBUF(1)
    );
\mod_signal_IBUF[2]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(2),
      O => mod_signal_IBUF(2)
    );
\mod_signal_IBUF[3]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(3),
      O => mod_signal_IBUF(3)
    );
\mod_signal_IBUF[4]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(4),
      O => mod_signal_IBUF(4)
    );
\mod_signal_IBUF[5]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(5),
      O => mod_signal_IBUF(5)
    );
\mod_signal_IBUF[6]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(6),
      O => mod_signal_IBUF(6)
    );
\mod_signal_IBUF[7]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(7),
      O => mod_signal_IBUF(7)
    );
\mod_signal_IBUF[8]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(8),
      O => mod_signal_IBUF(8)
    );
\mod_signal_IBUF[9]_inst\: unisim.vcomponents.IBUF
    generic map(
      CCIO_EN => "TRUE"
    )
        port map (
      I => mod_signal(9),
      O => mod_signal_IBUF(9)
    );
end STRUCTURE;
