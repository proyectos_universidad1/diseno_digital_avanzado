----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07.03.2023 14:03:55
-- Design Name: 
-- Module Name: mixer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mixer is
    Port ( 
           clk: in std_logic;
           carrier_signal : in STD_LOGIC_VECTOR (15 downto 0);
           mod_signal : in STD_LOGIC_VECTOR (15 downto 0);
           if_signal : out STD_LOGIC_VECTOR (15 downto 0));
end entity;

architecture rtl of mixer is

signal mult_out:std_logic_vector(31 downto 0);
signal reg_carrier,reg_mod:std_logic_vector(15 downto 0);

begin

reg_carrier<=carrier_signal when (rising_edge(clk));
reg_mod<=mod_signal when (rising_edge(clk));

mult_out<=std_logic_vector(signed(reg_mod)*signed(reg_carrier));

if_signal<=mult_out(31 downto 16) when rising_edge(clk);
end architecture;
