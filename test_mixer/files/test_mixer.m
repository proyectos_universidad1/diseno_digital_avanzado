clc;
clear all;
close all;

N=256;
Om1=0.1*pi;
Om2=0.4*pi;
A=2^15-1;
n=0:(N-1);

carrier_signal=round(A*cos(Om1*n));
mod_signal=round(A*cos(Om2*n));
if_signal=carrier_signal.*mod_signal;




figure;
plot(carrier_signal);
hold on;
plot(mod_signal);

figure;
plot(if_signal);

f=fopen('mod_signal.txt','w');
fprintf(f,'%d\n',mod_signal);
fclose(f);

f=fopen('carrier_signal.txt','w');
fprintf(f,'%d\n',carrier_signal);
fclose(f);







