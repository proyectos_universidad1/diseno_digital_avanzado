----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.08.2022 16:54:02
-- Design Name: 
-- Module Name: tb_mac - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use std.textio.all;
use ieee.std_logic_textio.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_mixer is
--  Port ( );
end tb_mixer;

architecture rtl of tb_mixer is
component mixer is
    Port ( 
           clk: in std_logic;
           carrier_signal : in STD_LOGIC_VECTOR (15 downto 0);
           mod_signal : in STD_LOGIC_VECTOR (15 downto 0);
           if_signal : out STD_LOGIC_VECTOR (15 downto 0));
end component;

-- Signals for entity under test

signal if_signal,carrier_signal,mod_signal: std_logic_vector(15 downto 0);
signal clk: std_logic;

-- For text files
file input_buf1,input_buf2: text;

-- Clock period for simulation
constant clk_period : time := 1 ns;

begin

-- EUT instance
mixer0: mixer
port map(
clk=>clk,
carrier_signal=>carrier_signal,
mod_signal=>mod_signal,
if_signal=>if_signal);

-- Process for data and clock generation
    process
    variable data_buf : line;
    variable value: integer;
    begin     
   
        -- Open data files
        file_open(input_buf1, "mod_signal.txt",  read_mode);
        file_open(input_buf2, "carrier_signal.txt",  read_mode);     
     
                   
         while not (endfile(input_buf1) or endfile(input_buf2) ) loop
           -- Read line
           readline(input_buf1, data_buf);           
           --Read value
           read(data_buf,value);           
           -- Put data in port a
           carrier_signal<=std_logic_vector(to_signed(value,16));
           
            -- Read line
           readline(input_buf2, data_buf);           
           --Read value
           read(data_buf,value);           
           -- Put data in port a
           mod_signal<=std_logic_vector(to_signed(value,16));
           
         
           
           -- Clock pulse
            clk<='0';           
            wait for clk_period/2;
            clk <= '1';            
            wait for clk_period/2;
            
        end loop;        
    end process;
    
   



end architecture;
