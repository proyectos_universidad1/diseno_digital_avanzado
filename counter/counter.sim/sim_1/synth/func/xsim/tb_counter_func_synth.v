// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2022.2 (lin64) Build 3671981 Fri Oct 14 04:59:54 MDT 2022
// Date        : Wed Feb 15 09:29:14 2023
// Host        : juan-Inspiron-14-3467 running 64-bit Linux Mint 21.1
// Command     : write_verilog -mode funcsim -nolib -force -file
//               /home/juan/Documentos/JuanK/Profun2/counter/counter.sim/sim_1/synth/func/xsim/tb_counter_func_synth.v
// Design      : counter
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* NotValidForBitStream *)
module counter
   (en,
    clk,
    count,
    ovf);
  input en;
  input clk;
  output [7:0]count;
  output ovf;

  wire [5:2]adder_out;
  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire [7:0]count;
  wire [7:0]count_OBUF;
  wire en;
  wire en_IBUF;
  wire ovf;
  wire ovf_OBUF;
  wire ovf_i_2_n_0;
  wire p_0_in;
  wire scount;
  wire \scount[0]_i_1_n_0 ;
  wire \scount[1]_i_1_n_0 ;
  wire \scount[6]_i_1_n_0 ;
  wire \scount[7]_i_1_n_0 ;
  wire [8:8]scount_reg;

  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF #(
    .CCIO_EN("TRUE")) 
    clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  OBUF \count_OBUF[0]_inst 
       (.I(count_OBUF[0]),
        .O(count[0]));
  OBUF \count_OBUF[1]_inst 
       (.I(count_OBUF[1]),
        .O(count[1]));
  OBUF \count_OBUF[2]_inst 
       (.I(count_OBUF[2]),
        .O(count[2]));
  OBUF \count_OBUF[3]_inst 
       (.I(count_OBUF[3]),
        .O(count[3]));
  OBUF \count_OBUF[4]_inst 
       (.I(count_OBUF[4]),
        .O(count[4]));
  OBUF \count_OBUF[5]_inst 
       (.I(count_OBUF[5]),
        .O(count[5]));
  OBUF \count_OBUF[6]_inst 
       (.I(count_OBUF[6]),
        .O(count[6]));
  OBUF \count_OBUF[7]_inst 
       (.I(count_OBUF[7]),
        .O(count[7]));
  IBUF #(
    .CCIO_EN("TRUE")) 
    en_IBUF_inst
       (.I(en),
        .O(en_IBUF));
  OBUF ovf_OBUF_inst
       (.I(ovf_OBUF),
        .O(ovf));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    ovf_i_1
       (.I0(count_OBUF[6]),
        .I1(ovf_i_2_n_0),
        .I2(count_OBUF[7]),
        .I3(scount_reg),
        .O(p_0_in));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    ovf_i_2
       (.I0(count_OBUF[5]),
        .I1(count_OBUF[3]),
        .I2(count_OBUF[1]),
        .I3(count_OBUF[0]),
        .I4(count_OBUF[2]),
        .I5(count_OBUF[4]),
        .O(ovf_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    ovf_reg
       (.C(clk_IBUF_BUFG),
        .CE(en_IBUF),
        .D(p_0_in),
        .Q(ovf_OBUF),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00009555)) 
    \scount[0]_i_1 
       (.I0(scount_reg),
        .I1(count_OBUF[7]),
        .I2(ovf_i_2_n_0),
        .I3(count_OBUF[6]),
        .I4(count_OBUF[0]),
        .O(\scount[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000955595550000)) 
    \scount[1]_i_1 
       (.I0(scount_reg),
        .I1(count_OBUF[7]),
        .I2(ovf_i_2_n_0),
        .I3(count_OBUF[6]),
        .I4(count_OBUF[0]),
        .I5(count_OBUF[1]),
        .O(\scount[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h78)) 
    \scount[2]_i_1 
       (.I0(count_OBUF[0]),
        .I1(count_OBUF[1]),
        .I2(count_OBUF[2]),
        .O(adder_out[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \scount[3]_i_1 
       (.I0(count_OBUF[1]),
        .I1(count_OBUF[0]),
        .I2(count_OBUF[2]),
        .I3(count_OBUF[3]),
        .O(adder_out[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \scount[4]_i_1 
       (.I0(count_OBUF[2]),
        .I1(count_OBUF[0]),
        .I2(count_OBUF[1]),
        .I3(count_OBUF[3]),
        .I4(count_OBUF[4]),
        .O(adder_out[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \scount[5]_i_1 
       (.I0(count_OBUF[3]),
        .I1(count_OBUF[1]),
        .I2(count_OBUF[0]),
        .I3(count_OBUF[2]),
        .I4(count_OBUF[4]),
        .I5(count_OBUF[5]),
        .O(adder_out[5]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h14)) 
    \scount[6]_i_1 
       (.I0(scount_reg),
        .I1(ovf_i_2_n_0),
        .I2(count_OBUF[6]),
        .O(\scount[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h1444)) 
    \scount[7]_i_1 
       (.I0(scount_reg),
        .I1(count_OBUF[7]),
        .I2(ovf_i_2_n_0),
        .I3(count_OBUF[6]),
        .O(\scount[7]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h28888888)) 
    \scount[8]_i_1 
       (.I0(en_IBUF),
        .I1(scount_reg),
        .I2(count_OBUF[7]),
        .I3(ovf_i_2_n_0),
        .I4(count_OBUF[6]),
        .O(scount));
  FDRE #(
    .INIT(1'b0)) 
    \scount_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(en_IBUF),
        .D(\scount[0]_i_1_n_0 ),
        .Q(count_OBUF[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \scount_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(en_IBUF),
        .D(\scount[1]_i_1_n_0 ),
        .Q(count_OBUF[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \scount_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(en_IBUF),
        .D(adder_out[2]),
        .Q(count_OBUF[2]),
        .R(scount));
  FDRE #(
    .INIT(1'b0)) 
    \scount_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(en_IBUF),
        .D(adder_out[3]),
        .Q(count_OBUF[3]),
        .R(scount));
  FDRE #(
    .INIT(1'b0)) 
    \scount_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(en_IBUF),
        .D(adder_out[4]),
        .Q(count_OBUF[4]),
        .R(scount));
  FDRE #(
    .INIT(1'b0)) 
    \scount_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(en_IBUF),
        .D(adder_out[5]),
        .Q(count_OBUF[5]),
        .R(scount));
  FDRE #(
    .INIT(1'b0)) 
    \scount_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(en_IBUF),
        .D(\scount[6]_i_1_n_0 ),
        .Q(count_OBUF[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \scount_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(en_IBUF),
        .D(\scount[7]_i_1_n_0 ),
        .Q(count_OBUF[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \scount_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(en_IBUF),
        .D(p_0_in),
        .Q(scount_reg),
        .R(scount));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
