----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.02.2023 08:19:58
-- Design Name: 
-- Module Name: counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter is
    Port ( en : in STD_LOGIC;
           clk : in STD_LOGIC;
           count : out STD_LOGIC_VECTOR (7 downto 0);
           ovf : out STD_LOGIC);
end counter;

architecture Behavioral of counter is

signal adder_out,reg_input,scount: std_logic_vector(8 downto 0);


begin

process(en,clk,reg_input)

begin

-- The registers
if(rising_edge(clk)) then
if(en='1') then
scount<=reg_input;
ovf<=adder_out(8);
end if;
end if;
end process;

-- The multiplexer
reg_input<= adder_out when(adder_out(8)='0') else (others=>'0');

-- The adder
adder_out<= std_logic_vector(unsigned(scount) + to_unsigned(1,9)); 

-- Connect output
count<=scount(7 downto 0);

end Behavioral;
